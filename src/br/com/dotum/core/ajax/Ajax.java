package br.com.dotum.core.ajax;

import java.util.HashMap;
import java.util.Map;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.StringUtils;

public class Ajax {
	private static Log LOG = LogFactory.getLog(Ajax.class);

	private boolean debug = false;
	private boolean reloadPage = false;
	private boolean message = false;
	private Map<String, Object> paramMap = new HashMap<String, Object>();
	private Map<String, Object> propertyMap = new HashMap<String, Object>();

	private FormHtml compForm;
	private String formId;
	private String jsObjForm;
	private boolean form = false;

	private String scriptBeforeSend;
	private StringBuilder scriptSuccess = new StringBuilder();
	private String scriptComplete;

	private String nameVar;
	private int timeOut = 0;

	public Ajax() {
		propertyMap.put("url", FWConstants.URL_DO);
		propertyMap.put("type", "POST");
	}
	public Ajax(Class<?> classe, String method) throws DeveloperException {
		this();

		if (classe == null) 
			throw new DeveloperException("Não foi informado a \"classe\" para usar a acao ajax.");
		if (method == null) 
			throw new DeveloperException("Não foi informado o \"method\" para usar a acao ajax.");


		paramMap.put(FWConstants.PARAM_FW_CLASS, ClassUtils.getCodeTask(classe.getName()));
		paramMap.put(FWConstants.PARAM_FW_METHOD, method);

		if ((method.equals("doFilterClean") == false) && (method.equals("doData") == false)) {
			try {	
				classe.getDeclaredMethod(method);
			} catch (NoSuchMethodException e) {
				LOG.warn("Não existe o metodo \"" + method + "\" na classe \"" + classe.getName() + "\".");
			}
		}
	}

	public void setForm(boolean form) {
		this.form = form;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public void setForm(FormHtml compForm) {
		this.compForm = compForm;
	}
	public String getJsObjForm() {
		return jsObjForm;
	}
	public void setJsObjForm(String jsObjForm) {
		this.jsObjForm = jsObjForm;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public void setReloadPage(boolean reloadPage) {
		this.reloadPage = reloadPage;
	}
	public void setMessage(boolean message) {
		this.message = message;
	}

	public void addParam(String key, Object value) {
		addParam(key, value, false);
	}
	public void addParamMaster(FormHtml form) {
		addParamJs(FWConstants.PARAM_FW_MASTER_ID, "$(\"#"+form.getId()+" input[name='"+ FWConstants.PARAM_FW_MASTER_ID +"']\").val()" + ResultHtml.PL);
	}

	public void addParamComponent(ComponentHtml comp) {
		TableHtml x = (TableHtml)comp;
		
		addParamJs(x.getName(), TableHtml.createVarResultFromTable(x.getId(), true));
	}
	public void addParamJs(String key, String value) {
		addParam(key, value, true);
	}
	private void addParam(String key, Object value, boolean varJs) {
		if (varJs == false) {
			this.paramMap.put(key, value);
		} else {
			this.paramMap.put(key, "\"+"+value+"+\"");
		}
	}

	public String getComponent() {
		StringBuilder sb = new StringBuilder();

		boolean origemForm = false;
		sb.append("var data = null;");
		if (compForm != null) {
			sb.append("data = new FormData($('#"+ compForm.getId() +"'));");
			origemForm = true;
		} else if (StringUtils.hasValue(jsObjForm)) {
			sb.append("data = new FormData("+ jsObjForm +");");
			origemForm = true;
		} else if (formId != null) {
			sb.append("data = new FormData($('#"+ formId +"'));");
			origemForm = true;
		} else if (form == true) {
			sb.append("data = new FormData(this);");
			origemForm = true;
		}

		// nao tem else pois posso passar um form e mais alguma coisa!!!
		if (origemForm == true) {
			for (String key : paramMap.keySet()) {
				sb.append("data.append(\""+ key +"\", \""+ paramMap.get(key) +"\");");
			}
		} else {
			sb.append("data = {" + mapToJson(paramMap) + "};");
		}

		if (StringUtils.hasValue(nameVar)) {
			addProperty("async", false);
		}


		String prop = mapToJson(propertyMap);

		if (StringUtils.hasValue(nameVar)) sb.append("var " + nameVar + " = null;");

		//		if (compTable != null) sb.append( TableHtml.createScriptDataRow(false, compTable.getId(), tableMulteSelected));
		sb.append("$.ajax({");
		sb.append("headers: {\""+ FWConstants.PARAM_FW_TOKEN_AUTHORIZATION +"\": localStorage.getItem('"+ FWConstants.PARAM_FW_TOKEN_AUTHORIZATION +"')},");
		sb.append( prop + ",");
		sb.append("\"data\": data,");

		if (timeOut > 0 )
			sb.append("\"timeout\":"+ timeOut +",");

		if (StringUtils.hasValue(scriptBeforeSend)) {
			sb.append("\"beforeSend\":function(data){");
			sb.append( scriptBeforeSend );
			sb.append("},");
		}


		sb.append("\"success\":function(data, textStatus, request){");
		if (this.debug) sb.append("console.log('Deu certo');");
		if (this.debug) sb.append("console.log(data);");


		sb.append("localStorage.setItem('"+ FWConstants.PARAM_FW_TOKEN_AUTHORIZATION +"', request.getResponseHeader('"+ FWConstants.PARAM_FW_TOKEN_AUTHORIZATION +"'));");

		if (StringUtils.hasValue(nameVar)) sb.append( nameVar + " = data;");
		String mensagem = MessageHtml.createScript(null, "'+data.type+'", "'+data.message+'");
		if (this.message) sb.append(mensagem);
		if (StringUtils.hasValue(this.scriptSuccess.toString())) sb.append(scriptSuccess.toString());
		if (this.reloadPage) sb.append("location.reload();");

		sb.append("},");
		sb.append("\"error\": function(request, textStatus, errorThrown) {");

		if (this.debug) sb.append("console.log('Deu erro');");
		if (this.debug) sb.append("console.log(request);");
		if (this.debug) sb.append("console.log(errorThrown);");
		mensagem = MessageHtml.createScript(null, "warning", "Ops! Não foi possível realizar esta operação!");
		sb.append(mensagem);
		sb.append("}");


		if (StringUtils.hasValue(scriptComplete)) {
			sb.append(",");
			sb.append("\"complete\":function(data){");
			sb.append( scriptComplete );
			sb.append("}");
		}
		sb.append("});" + ResultHtml.PL);

		return sb.toString();
	}

	public static String mapToJson(Map<String, Object> paramMap) {
		StringBuilder paramSB = new StringBuilder();

		String temp = "";
		for (String key : paramMap.keySet()) {
			Object value = paramMap.get(key);

			paramSB.append(temp);
			if (value instanceof Integer) {
				paramSB.append("\"" + key + "\":" + value);
			} else if (value instanceof Boolean) {
				paramSB.append("\"" + key + "\":" + value);
			} else {
				paramSB.append("\"" + key + "\":\"" + value + "\"");
			}

			temp = ", ";
		}
		return paramSB.toString();
	}

	public static String mapToStr(Map<String, String> paramMap) {
		StringBuilder paramSB = new StringBuilder();


		String temp = "";
		for (String key : paramMap.keySet()) {
			String value = paramMap.get(key);
			paramSB.append( temp );
			paramSB.append(key + "=" + value);
			temp = "&";
		}
		return paramSB.toString();
	}

	public void setBeforeSend(String beforeSend) {
		this.scriptBeforeSend = beforeSend;
	}
	public void addSuccess(String success) {
		this.scriptSuccess.append( success );
	}
	public void setSuccess(String success) {
		this.scriptSuccess = new StringBuilder();
		this.scriptSuccess.append( success );
	}
	public void setSuccessOrWarningMessage(String part) {
		this.scriptSuccess = new StringBuilder();
		this.scriptSuccess.append("if(data.type=='success'){");
		this.scriptSuccess.append( part );
		this.scriptSuccess.append("}else{");
		String mensagem = MessageHtml.createScript(null, "'+data.type+'", "'+data.message+'");
		this.scriptSuccess.append( mensagem );
		this.scriptSuccess.append("}");

	}
	public void setComplete(String complete) {
		this.scriptComplete = complete;
	}
	public void addProperty(String key, Object value) {
		this.propertyMap.put(key, value);
	}
	public void setNameVar(String nameVar) {
		this.nameVar = nameVar;

	}
	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}
}

