package br.com.dotum.core.basic998;

import java.util.Date;

import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.ComponentHelper;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Task;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.HtmlEditorFieldHtml;
import br.com.dotum.core.servlet.ServletHelper;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.UserContext;

@Menu(label="Inserir relatorio (DSR)", selecteds=Menu.ZERO, icon=CssIconEnum.INSERIR, color=CssColorEnum.BLUE)
@Task({Task.DEV_CODE})
public class Cpt010Inserir extends AbstractAction {

	public void doScreen() throws Exception {
		FormHtml form = new FormHtml();
		//
		HtmlEditorFieldHtml f1 = new HtmlEditorFieldHtml("XmlCustomizado", null, null, true);
		form.addGroup(null, f1);
		form.addButton("doProcess", "Salvar");

		super.addBody(form);
	}

	public void doProcess(Integer id) throws Exception {
		TransactionDB trans = super.getTransaction();
		UserContext userContext = super.getUserContext();
		Integer uniId = userContext.getUnidade().getId();
		Date dataAtual = trans.getDataAtual();
		String horaAtual = trans.getHoraAtual();

		
		String xml = super.getParamString("XmlCustomizado"); // f4
		ComponentHelper.insertReport(trans, getRequest().getServletContext(),xml);

		super.setMessageSuccess("Registro alterado com sucesso!");
	}
}
