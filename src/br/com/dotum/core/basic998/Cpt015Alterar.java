package br.com.dotum.core.basic998;

import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.ComponentHelper;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Task;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.HtmlEditorFieldHtml;
import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.bean.ComponenteBean;
import br.com.dotum.jedi.core.db.bean.TarefaBean;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.util.StringUtils;

@Menu(label="Alterar", selecteds=Menu.ONE, icon=CssIconEnum.ALTERAR, color=CssColorEnum.BLUE)
@Task({Task.DEV_CODE})
public class Cpt015Alterar extends AbstractAction {

	public void doScreen() throws Exception {
		FormHtml form = new FormHtml();
		
		HtmlEditorFieldHtml f1 = new HtmlEditorFieldHtml("XmlCustomizado", null, null, true);
		form.addGroup(null, f1);
		form.addButton("doProcess", "Salvar");
		
		super.addBody(form);
	}
	public Object doData(Integer id) throws Exception {
		TransactionDB trans = super.getTransaction();
		ComponenteBean bean = trans.selectById(ComponenteBean.class, id);
		
		if (bean.getComponenteTipo().getId().equals(ComponentHelper.CPTT_DOTUMREPORT) == false)
			throw new WarningException("Atualmente so é permitido atualizar Relatorio");
		
		if (StringUtils.hasValue(bean.getXmlCustomizado()) == false)
			bean.setXmlCustomizado(bean.getXml());
		
		return bean;
	}

	public void doProcess(Integer id) throws Exception {
		TransactionDB trans = super.getTransaction();

		String xml = super.getParamString("xmlCustomizado"); // f4


		DotumSmartReport report = DotumSmartReportBuilder.prepare(xml);

		
		ComponenteBean bean = trans.selectById(ComponenteBean.class, id);
		bean.setDescricao(report.getTitle());
		bean.setXmlCustomizado(xml);
		trans.update(bean);
		
		TarefaBean tarBean = trans.selectById(TarefaBean.class, id);
		tarBean.setId(id);
		tarBean.setDescricao(report.getTitle());
		trans.update(tarBean);
		

		//
		// MELHORAR ISSO AQUI
		//
		ComponentHelper.doCacheCustom(trans, getRequest().getServletContext(), bean.getCodigo(), xml);
		super.setMessageSuccess("Registro alterado com sucesso!");
	}
}
