package br.com.dotum.core.basic998;

import br.com.dotum.core.component.AbstractView;
import br.com.dotum.core.component.ComponentHelper;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Task;
import br.com.dotum.core.html.form.field.ComboNumberFieldHtml;
import br.com.dotum.core.html.form.field.TextFieldHtml;
import br.com.dotum.jedi.core.db.ORM;
import br.com.dotum.jedi.core.db.WhereDB.Condition;

@Menu(group="Cadastro", label="Componente", inMenu=Menu.POSITION_2_0_USER)
@Task({Task.DEV_CODE})
public class ScreenView extends AbstractView {

	public void doExecute() throws Exception {
		super.addTableColumn("Componente Tipo", "componenteTipoDescricao");
		super.addTableColumn("Codigo", "codigo");
		super.addTableColumn("Descrição", "descricao");
	}

	public void doFilter() throws Exception {
		TextFieldHtml f1 = new TextFieldHtml("descricao", "Descrição", null, false);
		super.addFilterField(Condition.LIKE, f1);

		ComboNumberFieldHtml f2 = new ComboNumberFieldHtml("componenteTipoId", "Tipo", null, false);
		f2.addOption(ComponentHelper.CPTT_VIEW, "ScreenView");
		f2.addOption(ComponentHelper.CPTT_DOTUMREPORT, "DotumSmartReport");
		super.addFilterField(Condition.EQUALS, f2);
	}


	public Object[] doData() throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("          select cpt.cpt_id,                                                                                        ");
		sb.append("                 cpt.cpt_descricao,                                                                                 ");
		sb.append("                 cpt.cpt_codigo,                                                                                    ");
		sb.append("                 cpt.cpt_xml,                                                                                       ");
		sb.append("                 cptt.cptt_id,                                                                                      ");
		sb.append("                 cptt.cptt_descricao                                                                                ");
		sb.append("            from componente cpt                                                                                     ");
		sb.append("       left join componentetipo cptt on (cptt.cptt_id = cpt.cptt_id)                                                ");
		sb.append("           where cptt.cptt_id in ("+ ComponentHelper.CPTT_DOTUMREPORT +", "+ ComponentHelper.CPTT_VIEW +")          ");


		ORM orm = new ORM();
		orm.add(Integer.class,      "id",                                   "cpt_id");
		orm.add(String.class,       "descricao",                            "cpt_descricao");
		orm.add(String.class,       "codigo",                               "cpt_codigo");
		orm.add(Integer.class,      "componenteTipoId",                     "cptt_id");
		orm.add(String.class,       "componenteTipoDescricao",              "cptt_descricao");
		orm.add(String.class,       "xml",                                  "cpt_xml");

		return new Object[] {orm, sb.toString()};
	}
}

