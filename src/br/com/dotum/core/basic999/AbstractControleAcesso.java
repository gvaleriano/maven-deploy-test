package br.com.dotum.core.basic999;

import java.util.List;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.bean.GrupoAcessoBean;
import br.com.dotum.jedi.core.db.bean.PessoaTarefaBean;
import br.com.dotum.jedi.core.db.bean.TarefaBean;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;

public abstract class AbstractControleAcesso {


	public abstract List<Bean> doDataUsuarioList(TransactionDB trans, UserContext userContext) throws Exception;
	public abstract Bean doDataUsuario(TransactionDB trans, Integer id) throws Exception;

	public abstract List<Bean> doDataUsuarioTarefaList(TransactionDB trans, Integer id) throws Exception;
	public abstract Bean doDataUsuarioTarefa(TransactionDB trans, Integer usuId, Integer tarefaId) throws Exception;


	public abstract void doProcessUsuarioInserir(TransactionDB trans, Integer pessoaId, Integer tarefaId) throws Exception;
	public abstract void doProcessUsuarioDeletar(TransactionDB trans, Integer pessoaId, Integer ... tarefaId) throws Exception;

}
