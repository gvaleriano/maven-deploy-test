package br.com.dotum.core.basic999;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.db.bean.PessoaBean;
import br.com.dotum.jedi.core.db.bean.PessoaTarefaBean;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;

public class ControleAcessoImpl extends AbstractControleAcesso {

	public List<Bean> doDataUsuarioList(TransactionDB trans, UserContext userContext) throws Exception {

		List<Bean> pesList = null;
		try {

			OrderDB order = new OrderDB();
			order.add("NomeRazao");

			WhereDB where = new WhereDB();
			where.add("Unidade.Id", Condition.IN, userContext.getUnidadeArray());
			where.add("Senha", Condition.ISNOTNULL, null);
			pesList = trans.select(PessoaBean.class, where, order);

			for (Bean pesBean : pesList) {
				pesBean.set("Nome", pesBean.get("NomeRazao"));
				pesBean.set("Login", pesBean.get("Usuario"));
			}

		} catch (NotFoundException e) {
			pesList = new ArrayList<Bean>();
		}

		return pesList;
	}
	public List<Bean> doDataUsuarioTarefaList(TransactionDB trans, Integer id) throws Exception {
		List<Bean> ustaList = null;
		try {
			WhereDB where = new WhereDB();
			where.add("Pessoa.Id", Condition.EQUALS, id);
			ustaList = trans.select(PessoaTarefaBean.class, where);
		} catch (NotFoundException e) {
			ustaList = new ArrayList<Bean>();
		}

		return ustaList;
	}
	public Bean doDataUsuario(TransactionDB trans, Integer id) throws Exception {
		PessoaBean pesBean = trans.selectById(PessoaBean.class, id);
		pesBean.set("Nome", pesBean.getNomeRazao());
		pesBean.set("Login", pesBean.getUsuario());

		return pesBean;
	}
	public Bean doDataUsuarioTarefa(TransactionDB trans, Integer usuId, Integer tarefaId) throws Exception {

		WhereDB where = new WhereDB();
		where.add("Pessoa.Id", Condition.EQUALS, usuId);
		where.add("Tarefa.Id", Condition.EQUALS, tarefaId);
		List<PessoaTarefaBean> ustaList = trans.select(PessoaTarefaBean.class, where);
		PessoaTarefaBean petaBean = ustaList.get(0);

		return petaBean;

	}

	public void doProcessUsuarioInserir(TransactionDB trans, Integer usuId, Integer tarefaId) throws Exception {
		PessoaTarefaBean uspeBean = new PessoaTarefaBean();
		uspeBean.getPessoa().setId(usuId);
		uspeBean.getTarefa().setId(tarefaId);
		trans.insert(uspeBean);
	}

	public void doProcessUsuarioDeletar(TransactionDB trans, Integer usuId, Integer ... tarefaIdArray) throws Exception {
		WhereDB where = new WhereDB();
		where.add("Pessoa.Id", Condition.EQUALS, usuId);
		where.add("Tarefa.Id", Condition.IN, tarefaIdArray);
		trans.delete(PessoaTarefaBean.class, where);
	}
}
