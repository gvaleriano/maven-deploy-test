package br.com.dotum.core.basic999;

import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.PasswordFieldHtml;
import br.com.dotum.core.html.other.ColumnHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.MessageHtml.Type;
import br.com.dotum.jedi.core.db.ListDD;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

@Menu(label="Install", publico=true)
public class Controller001Install extends AbstractController {
	private static Log LOG = LogFactory.getLog(Controller001Install.class);

	public void doScreen() throws Exception {

		ColumnHtml column = new ColumnHtml(4, 4, 4);
		
		
		FormHtml form = new FormHtml();
		form.setAjax(false);

		PasswordFieldHtml f1 = new PasswordFieldHtml("senha", "Senha", null, true);

		form.addGroup("Acessar o instalador", f1);
		form.addButton("doProcess", "Acessar");
		column.addContent(1,  new Html(""));
		column.addContent(2,  form);
		column.addContent(3,  new Html(""));
		
		super.addBody(column);
	}

	public void doProcess(Integer id) throws Exception {
		TransactionDB trans = super.getTransaction();
		UserContext userContext = super.getUserContext();

		// pegando os dados
		String senha = getParamString("senha");

		if (senha.equals("2") == false) {

			super.setMessageWarning("A senha informada não é valida");
			doScreen();

		} else {
			// criando as tabelas no banco de dados
			try {
				MessageHtml message = new MessageHtml(Type.INFO);
				message.setLabel("Criando as tabelas no banco de dados");
				String resultado = trans.createDataBase();
				resultado = resultado.replaceAll("\n", "<br />");

				if (resultado.contains("[ERRO]")) message.setType(Type.ERROR);
				message.addContent(resultado);
				super.addBody(message);
			} catch (Exception e) {
				MessageHtml message = new MessageHtml(Type.ERROR);
				message.addContent("[ERRO] " + e.getMessage());
				super.addBody(message);
			}


			// criando as funcoes padroes da dotum!!!
			try {
				MessageHtml message = new MessageHtml(Type.INFO);
				message.setLabel("Criando as funcoes padroes da dotum");
				
				String resultado = trans.createFunction();
				resultado = resultado.replaceAll("\n", "<br />");

				if (resultado.contains("[ERRO]")) message.setType(Type.ERROR);
				message.addContent(resultado);
				super.addBody(message);
			} catch (Exception e) {
				MessageHtml message = new MessageHtml(Type.ERROR);
				message.addContent("[ERRO] " + e.getMessage());
				super.addBody(message);
			}


			// atualizado o DD
			try {
				MessageHtml message = new MessageHtml(Type.INFO);
				message.setLabel("Atualizado o DD");

				Class<?> classe = Class.forName(TransactionDB.PACKAGE_DD + ".ApplicationDD");
				ListDD listDd = (ListDD)classe.newInstance();
				String resultado = listDd.run(trans);
				resultado = resultado.replaceAll("\n", "<br />");

				if (resultado.contains("[ERRO]")) message.setType(Type.ERROR);
				message.addContent(resultado);
				super.addBody(message);
			} catch (Exception e) {
				MessageHtml message = new MessageHtml(Type.ERROR);
				message.addContent("[ERRO] " + e.getMessage());
				super.addBody(message);
			}

//			// atualizando os componente para estatisticas
//			try {
//				MessageHtml message = new MessageHtml(Type.INFO);
//				message.setLabel("Atualizando os componente para estatisticas");
//
//				Map<String, Class<?>> classeFullMap = (Map<String, Class<?>>)getRequest().getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_FULL_MAP);
//				ComponentHelper.mergeComponente(trans, classeFullMap);
//
//				message.addContent("Componentes atualizado com sucesso!");
//				super.addBody(message);
//			} catch (Exception e) {
//				MessageHtml message = new MessageHtml(Type.ERROR);
//				message.addContent("[ERRO] " + e.getMessage());
//				super.addBody(message);
//			}
			
		}
	}
}
