package br.com.dotum.core.basic999;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.ComponentHelper;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Task;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.CheckboxFieldHtml;
import br.com.dotum.core.html.form.field.HiddenFieldHtml;
import br.com.dotum.core.html.form.field.HtmlEditorFieldHtml;
import br.com.dotum.core.html.form.field.TextFieldHtml;
import br.com.dotum.core.html.other.ColumnHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.MessageHtml.Type;
import br.com.dotum.core.html.other.TabHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.core.html.view.FieldViewHtml;
import br.com.dotum.core.html.view.ViewHtml;
import br.com.dotum.jedi.component.xml.DotumReleaseBuilder;
import br.com.dotum.jedi.component.xml.release.model.DotumRelease;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.ListDD;
import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.db.bean.FwReleaseBean;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.gen.GeneratorModel;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.protocol.FileTransferProtocol;
import br.com.dotum.jedi.security.SecurityUtil;
import br.com.dotum.jedi.util.FileUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;

@Menu(label="Setup", inMenu=Menu.POSITION_2_0_USER, icon=CssIconEnum.EXCLAMATION_TRIANGLE, color=CssColorEnum.WHITE)
@Task({Task.DEV_CODE, Task.ADMIN_CODE})
public class Controller005Setup extends AbstractController {
	private static Log LOG = LogFactory.getLog(Controller005Setup.class);

	private static final String KEY = "ta5b6l8e7Dbgig6ffSc22sriptDB_";

	public void doScreen() throws Exception {
		super.addBody(createHtmlTelaToda(null, null));
	}

	public TabHtml createHtmlTelaToda(String queryText, String querySelecionada) throws Exception {
		FormHtml formDB = createFormBancoDados(queryText, querySelecionada);
		MessageHtml messageLog = createMessageLog();
		ColumnHtml column = createColumnUpdadeAplication();
		ComponentHtml tableDB = createColumnDiffDataBase();
		ViewHtml icones = createColumnIcones();


		TabHtml tab = new TabHtml();
		tab.add("Consultar DB", formDB);
		tab.add("Diferença DB", tableDB);
		tab.add("Log do servidor", messageLog);
		tab.add("Atualização", column, true);
		tab.add("Icones" , icones);

		return tab;
	}


	private ComponentHtml createColumnDiffDataBase() throws Exception {

		TableHtml table = new TableHtml("doDataTableDiff");
		table.addColumn("#", "id");
		table.addColumn("Tipo", "tipo");
		table.addColumn("Comando", "comando");
		table.addButton(getClass(), "doProcessTableDiffRun").label("Executar").icon(CssIconEnum.REFRESH).dataComponent(table);

		return table;
	}
	private MessageHtml createMessageLog() {
		MessageHtml message = new MessageHtml(Type.WARNING);
		try {
			FileMemory fm = new FileMemory(PropertiesUtils.getString(PropertiesConstants.APPLICATION_LOG_PATH));
			String text = fm.getText();
			int max = 120000;
			int size = text.length();
			if (size > max) {
				text = text.substring(text.length() - max);
			}
			text = text.replaceAll("\\<", "&lt;"); 
			text = text.replaceAll("\\>", "&gt;"); 
			text = text.replaceAll("\n", "<br />");
			message.addContent(text);
		} catch (Exception e) {
			message.addContent("Não foi encontrato o arquivo de log. Provavelmente voce ira precisar reiniciar o sistema");
			LOG.error(e.getLocalizedMessage(), e);
		}

		return message;
	}
	private ColumnHtml createColumnUpdadeAplication() throws DeveloperException {
		FormHtml formB = new FormHtml(null);
		//
		CheckboxFieldHtml f1 = new CheckboxFieldHtml("defaultData", "DD", 0, true);
		f1.addOption(1, null);
		//
		CheckboxFieldHtml f2 = new CheckboxFieldHtml("componente", "Componente", 0, true);
		f2.addOption(1, null);
		//
		CheckboxFieldHtml f3 = new CheckboxFieldHtml("buscarAtualizacao", "Buscar atualização", 0, true);
		f3.addOption(1, null);
		//
		TextFieldHtml f4 = new TextFieldHtml("codigoAtualizacao", "Codigo", "Kjh34gG6C$%c", true);
		//
		formB.addGroup(null, f3, f1, f2, f4);
		formB.addButton("doProcessAtualizaDD", "Ok");




		TableHtml table = new TableHtml("doDataTableRelease");
		table.addColumn("#", "Id");
		table.addColumn("Tipo", "tipo");
		table.addColumn("Descrição", "descricao");
		table.addColumn("Programador", "programador");
		table.addColumn("Kb", "tamanho");
		table.addColumn("Dep.", "releaseDependente");
		table.addButton(getClass(), "doProccessUpdate").icon(CssIconEnum.DOWNLOAD).label("Atualizar");



		//		FormHtml formA = new FormHtml(null);
		//		TextFieldHtml fB1 = new TextFieldHtml("codigoAtualizacao", "Codigo", "Kjh34gG6C$%c", true);
		//		formA.addGroup("Buscar atualização", fB1);
		//		formA.addButton("doProcessBuscarAtualizacao", "Ok");
		formB.setDataComponent(table);


		ColumnHtml column = new ColumnHtml();
		column.add(1, formB);
		column.add(1, table);

		return column;
	}
	private FormHtml createFormBancoDados(String query, String querySelecionada) throws Exception {

		FormHtml form = new FormHtml();
		form.setAjax(false);

		//FIELDs
		HiddenFieldHtml fa0 = new HiddenFieldHtml("querySelecionada", querySelecionada);
		//
		HtmlEditorFieldHtml fa1 = new HtmlEditorFieldHtml("query", null, query, true);
		fa1.setFieldSelectedContent(fa0);
		fa1.setTypeEditor(HtmlEditorFieldHtml.SQL);

		//
		form.addGroup(null, 1, fa0, fa1);
		//BUTTON
		form.addButton("doProcessBancoDados", "Executar");


		return form;
	}
	private ViewHtml createColumnIcones() throws DeveloperException {
		List<CssIconEnum> iconsList = Arrays.asList(CssIconEnum.values());
		ViewHtml viewIcons = new ViewHtml("doDataTableIcone");
		for(CssIconEnum b : iconsList) {
			viewIcons.addGroup("Icon <a href=\"https://fontawesome.com/v4.7.0/icons/\" target=\"_blank\">Link</a>", 5, new FieldViewHtml(b.name(), b.name()));
		}

		return viewIcons;
	}


	public Object doDataTableRelease(Integer id) throws NotFoundException, Exception {
		TransactionDB trans = getTransaction();

		WhereDB where = new WhereDB();
		where.add("Aplicado", Condition.EQUALS, FWConstants.NAO);
		List<FwReleaseBean> fwrList = trans.select(FwReleaseBean.class, where);

		return fwrList;
	}
	public Object doDataTableDiff(Integer id) throws Exception {
		TransactionDB trans = getTransaction();

		List<Bean> list = new ArrayList<Bean>();

		Map<String, List<String>> map = GeneratorModel.diffDB(trans, null, "/*dotum*/");


		int i = 0;
		for (String key : map.keySet()) {
			for (String comando : map.get(key)) {

				Bean b = new Bean();
				b.set("id", i++);
				b.set("tipo", key);
				b.set("comando", comando);
				list.add(b);
			}
		}
		super.setObjectSession(KEY, list);

		return list;
	}
	public Object doDataTableIcone(Integer id) {
		List<CssIconEnum> iconsList = Arrays.asList(CssIconEnum.values());

		Bean bean = new Bean();
		for(CssIconEnum icon : iconsList) {
			bean.set(icon.name(), "<i class=\" "+icon.getDescricao()+" green bigger-150 \"/>");
		}

		return bean;
	}

	public void doProcessAtualizaDD(Integer id) throws Exception {
		TransactionDB trans = super.getTransaction();
		UserContext userContext = super.getUserContext();
		String message = "";
		boolean errorDD = false;

		// pegando os dados
		Boolean componente = getParamBoolean("componente");
		Boolean defaultData = getParamBoolean("defaultData");
		Boolean buscarAtualizacao = getParamBoolean("buscarAtualizacao");

		// atualizado o DD
		if (defaultData == true) {
			Class<?> classe = Class.forName(TransactionDB.PACKAGE_DD + ".ApplicationDD");
			ListDD listDd = (ListDD)classe.newInstance();
			String result = listDd.run(trans);

			if (result.contains("[ERRO]")) {
				errorDD = true;

				super.logError(result);
				message += "Problema para atualizar (s) DD(s)</br>";
			} else {
				message += "Atualizado o(s) DD(s)</br>";
			}
		}

		if (componente == true) { 
			try {
				// atualizando os componentes
				String preFixPackage = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);

				
				ComponentHelper.doCacheClass(trans, getRequest().getServletContext(), preFixPackage, true);
				Map<String, Class<?>> classeFullMap = (Map<String, Class<?>>)getRequest().getServletContext().getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_FULL_MAP);
				Map<String, DotumSmartReport> classeSmartReportMap = ComponentHelper.getCacheDotumSmartReport(getRequest());
				ComponentHelper.mergeComponente(trans, classeFullMap, classeSmartReportMap);
				ComponentHelper.doCacheCustom(trans, getRequest().getServletContext());
				
				
			} catch (WarningException e) {
				super.setMessageWarning(e.getMessage());
			}
			message += "Atualizado o(s) componente(s)<br/>";
		}

		if (buscarAtualizacao == true) {
			message += doProcessBuscarAtualizacao()+"<br/>";
		}

		if (errorDD == true)
			super.setMessageWarning( message );
		super.setMessageSuccess( message );

	}
	public void doProcessBancoDados() throws DeveloperException, Exception {
		TransactionDB trans = super.getTransaction();
		UserContext userContext = super.getUserContext();
		//
		String queryText = getParamString("query");
		String querySelecionada = getParamString("querySelecionada");
		String query = queryText;
		if (StringUtils.hasValue(querySelecionada)) query = querySelecionada;

		String tipoQuery = (query.toLowerCase().trim().startsWith("select") ? "query" : "command" ); 

		TabHtml tab = createHtmlTelaToda(queryText, querySelecionada);
		ResultSet rs = null;
		try{
			boolean insert = query.toLowerCase().contains("insert");
			boolean update = query.toLowerCase().contains("update");
			boolean delete = query.toLowerCase().contains("delete");
			boolean create = query.toLowerCase().contains("create");
			boolean alter = query.toLowerCase().contains("alter");
			boolean drop = query.toLowerCase().contains("drop");
			boolean tables = query.toLowerCase().contains("table");

			if (tipoQuery.equals("query")) {

				if(insert || update || alter || delete || drop || create || tables){
					throw new Exception("A query contém um comando não permitido.");
				}

				String style1 = "background:#ebebeb;color:#888888;";
				String style2 = "border-right:1px solid #9f9f9f;";
				String style3 = "text-align:right;";
				int maximoRegistros = 5000;
				String sql = "select tb1.* from (" + query + ") tb1 where rownum <= " + maximoRegistros;

				StringBuilder sb = new StringBuilder();
				sb.append("<table class=\"table table-striped table-hover dataTable no-footer\">");


				List<String> attrubuteList = new ArrayList<String>();
				List<Bean> list = new ArrayList<Bean>();

				rs = trans.execQuery( sql );
				int line = 0;
				while (rs.next()) {
					ResultSetMetaData rsData = rs.getMetaData();
					int count = rsData.getColumnCount();

					Bean bean = new Bean();
					list.add(bean);

					sb.append("<tr>");
					for (int i = 1; i <= count; i++) {
						String label = rsData.getColumnLabel(i);
						Object value = rs.getObject(i);


						if (value == null) {
							value = "<b>null</b>";
						} else if(value instanceof BigDecimal){
							value = ((BigDecimal)value).toString();
						} else if(value instanceof java.sql.Timestamp){
							Date d = new Date(((java.sql.Timestamp)value).getTime());
							value = FormatUtils.formatDate(d);
						} else if(value instanceof oracle.sql.BLOB){
							value = "<b>BLOB</b>";
						} else {
							value = rs.getString(i);
						}

						if(line == 0){
							if ((line == 0) && (i == 1)) sb.append("<th style=\""+ style1 + style2 +"\">&nbsp;</th>");
							sb.append("<th style=\""+ style1 +"\">");
							sb.append( label );
							sb.append("</th>");

							attrubuteList.add(label);
						}
						bean.setAttribute(label, value);
					}
					sb.append("</tr>");
					line++;
				}


				line = 0;
				for (Bean b : list) {
					line++;
					sb.append("<tr>");
					sb.append("<td style=\""+ style1 + style2 + style3 +"\">" + line + "</td>");
					for (String x : attrubuteList) {
						sb.append("<td>");
						sb.append( (String)b.get(x) );
						sb.append("</td>");
					}
					sb.append("</tr>");
				}
				sb.append("</table>");
				tab.add("Consultar DB", new Html("<div class=\"col-sm-12\">"));
				tab.add("Consultar DB", new Html( (line >= maximoRegistros ? maximoRegistros + " de muitos" : line) + " registro(s)"));
				tab.add("Consultar DB", new Html(sb.toString()));
				tab.add("Consultar DB", new Html("</div>"));

			} else if (tipoQuery.equals("command")) {

				if(drop||tables){
					throw new Exception("A função contém um comando não permitido. Favor volte a mesa de planejamento.");
				}

				if (userContext.getUser().hasAccessTaskByCode(Task.ADMIN_CODE) == false) {
					throw new WarningException("Só desenvolvedores Dotum tem permissão de executar funções!!!");
				}

				String[] commands = query.split("\\/\\*dotum\\*\\/");
				String[] msgs = new String[commands.length];

				int countError = 0;
				boolean commit = false;
				for (int i = 0; i < commands.length; i++) {
					String command = commands[i];
					if (command.trim().equalsIgnoreCase("commit")) {
						commit = true;
						break;
					}
					try {
						Integer size = trans.execUpdate(command);
						msgs[i] = "[OK] Linhas afetadas " + size + "";
					} catch (Exception e) {
						countError++;
						msgs[i] = "[ERRO] " + e.getMessage();
					}
				}
				if ((commit == true) && (countError == 0)) trans.commit();
				if ((commit == true) && (countError > 0))  trans.rollback();
				if (commit == false) trans.rollback();

				//
				MessageHtml message = new MessageHtml(Type.WARNING);
				message.addBreakLine();
				if ((commit == true) && (countError == 0)) {
					message.setType(Type.SUCCESS);
					message.addContent("Alteração efetuada com sucesso!");


					// so vamos colocar auditoria quando realmente houver commit;
					// assim saberemos quem fez a operação!!!
					StringBuilder log = new StringBuilder();
					for (int i = 0; i < commands.length; i++) {
						String command = commands[i];
						String msg = msgs[i];
						if (command.trim().equalsIgnoreCase("commit")) break;

						log.append(""+command+"\n");
						log.append(msg +".\n");
					}
				}
				if ((commit == true) && (countError > 0)) message.addContent("Existem erros no script o commit não será executado!!!");
				if (commit == false) message.addContent("Não foi comitado pois não foi usado o comando \"COMMIT\" no fim do script!");
				message.addContent("Total de erros: " + countError);
				message.addBreakLine();

				for (int i = 0; i < commands.length; i++) {
					String command = commands[i];
					String msg = msgs[i];
					if (command.trim().equalsIgnoreCase("commit")) break;

					message.addContent(command);
					message.addContent(msg +".\n");
					message.addBreakLine();
				}
				tab.add("Consultar DB", message);
			}
		} catch(WarningException e) {
			trans.rollback();
			MessageHtml message = new MessageHtml(Type.WARNING);
			message.addContent(e.getMessage());
			tab.add("Consultar DB", message);
		} catch(Exception e) {
			trans.rollback();
			LOG.error(e.getMessage(), e);
			MessageHtml message = new MessageHtml(Type.ERROR);
			message.addContent(e.getMessage());
			tab.add("Consultar DB", message);
		} finally{
			trans.closeQuery();
		}

		super.addBody(tab);
	}
	public String doProcessBuscarAtualizacao() throws DeveloperException, Exception {
		TransactionDB trans = super.getTransaction();
		UserContext userContext = super.getUserContext();
		//
		String codigoAtualizacao = getParamString("codigoAtualizacao");

		String ftpHost = PropertiesUtils.getString(PropertiesConstants.FTP_SERVER);
		String ftpUserName = PropertiesUtils.getString(PropertiesConstants.FTP_USERNAME);
		String ftpPassword = PropertiesUtils.getString(PropertiesConstants.FTP_PASSWORD);
		ftpPassword = SecurityUtil.decrypt(SecurityUtil.MOD1, ftpPassword);
		if (StringUtils.hasValue(ftpPassword) == false) throw new WarningException("Atualmente é necessario cripitografar do a senha do FTP no properties");
		//		String ftpFolder = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);

		/**
		 * 
		 * Carissimo Keynes, favor deixar este if como está
		 * 
		 */
		String ftpFolder = "";
		if (PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE).equals("sicoob")) {
			ftpFolder = FWConstants.PATH_RELEASE ;
		} else {
			ftpFolder = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
		}

		List<FwReleaseBean> list = null;
		try {
			OrderDB order = new OrderDB();
			order.add("Id");

			list = trans.select(FwReleaseBean.class, order);
		} catch (NotFoundException n) {
			list = new ArrayList<FwReleaseBean>(); 
		}
		String localDiretory = getPathServer() + FWConstants.PATH_RELEASE + "/";



		FileTransferProtocol ftp = null;
		String[] files = null;
		int quant = 0;
		try {
			ftp = new FileTransferProtocol(ftpHost, ftpUserName, ftpPassword);
			ftp.connect();
			ftp.changeDir(ftpFolder);
			files = ftp.list("./");
		} catch (Exception e) {
			ftp.disconnect();
			throw new WarningException(e.getMessage());
		}


		// aqui serve para pular a release se ela ja estiver no banco de dados
		proximaRelease:
			for (String fileName : files) {
				String numberReleaseStr = StringUtils.getNumberOnly(fileName);
				if (StringUtils.hasValue(numberReleaseStr) == false) {
					throw new WarningException("Existe uma release com problema no Servidor de atualização (FTP). Solicite reparo.");
				}
				Integer numberRelease = FormatUtils.parseInt(numberReleaseStr);
				String unzipFolder = localDiretory + numberRelease + "/";

				for (FwReleaseBean bean : list) {
					if (bean.getId().equals(numberRelease) == false) continue;

					continue proximaRelease;
				}




				FileMemory fm = ftp.receiveFile(fileName);
				fm.setPath(unzipFolder);
				fm.createFile();
				FileUtils.unzip(fm.getParent() + fm.getName(), unzipFolder);

				String deleteFile = null;
				String scriptFile = null;
				String scriptName = null;
				try {
					FileMemory deleteFM = new FileMemory(unzipFolder + "/" + "filetodelete.txt");
					deleteFile = deleteFM.getText();
				} catch (Exception e) {
				}

				try {
					FileMemory scriptFM = new FileMemory(unzipFolder + "/" + "script.sql");
					scriptFile = scriptFM.getText();
					String temp = scriptFM.getRows()[0];
					scriptName = temp.substring(temp.indexOf('\'')+1, temp.lastIndexOf('\''));

				} catch (Exception e) {
				}

				DotumReleaseBuilder dotumRelease = new DotumReleaseBuilder();
				dotumRelease.prepareDotumRelease(new File(unzipFolder + "release.xml"));
				DotumRelease release = dotumRelease.getRelease();

				FwReleaseBean dtrBean = new FwReleaseBean();
				dtrBean.setId(numberRelease);

				Blob b = new Blob();
				b.setData(fm.getBytes());
				dtrBean.setArquivo(b);
				dtrBean.setDescricao(release.getDescription());
				dtrBean.setDataCadastro(release.getCreate());
				dtrBean.setTipo(release.getType());
				dtrBean.setVersao(release.getVersion());
				dtrBean.setProgramador(release.getDeveloperBy());
				dtrBean.setTamanho(fm.sizeByte() / 1024 + " KB");

				dtrBean.setVisivel(FWConstants.SIM);
				dtrBean.setAplicado(FWConstants.NAO);
				//			dtrBean.setArquivoAlterado( release.getChangedFile() );
				//			dtrBean.setArquivoExcluido( release.getDeletedFile() );
				//			dtrBean.setArquivoScript( release.getScriptFile() );
				dtrBean.setReleaseDependente( release.getDependentRelease() );
				dtrBean.setDescricao( release.getObservation() );
				trans.insert(dtrBean);


				FileUtils.deleteDirectory(unzipFolder);

				//; esse commit fica pois caso caia a conexao com o FTP continua de onde parou!!!
				// ja foi implementado uma ideia para continuar a vida com inteligencia!!!
				trans.commit();
				quant++;
			}
		ftp.disconnect();

		if (quant == 0) {
			return "Não há mais releases disponíveis no servidor de atualização.";
		} else {
			return "Novas releases encontradas: " + quant;
		}
	}
	public void doProccessUpdate(Integer id) throws Exception {
		TransactionDB trans = super.getTransaction();
		UserContext userContext = super.getUserContext();
		//
		id = super.getParamInteger("id");
		//
		MessageHtml message = new MessageHtml(MessageHtml.Type.SUCCESS);
		message.setLabel("Relatório das releases");


		int countErro = 0;
		int countSucesso = 0;
		String temp = "";
		StringBuilder sb = new StringBuilder();
		StringBuilder sbError = new StringBuilder();

		List<FwReleaseBean> fwrAscFullList = null;
		List<FwReleaseBean> fwrDescFullList = new ArrayList<FwReleaseBean>();
		List<FwReleaseBean> fwrList = new ArrayList<FwReleaseBean>();

		try {
			OrderDB order = new OrderDB();
			order.add("Id");

			WhereDB where = new WhereDB();
			where.add("Aplicado", Condition.EQUALS, FWConstants.NAO);
			where.add("Id", Condition.LESSOREQUALS, id);
			fwrAscFullList = trans.select(FwReleaseBean.class, where, order);

		} catch (NotFoundException nfe) {
			throw new WarningException("Não há atualizações para aplicar.");
		}
		for (int i = fwrAscFullList.size(); i > 0; i--) {
			fwrDescFullList.add(fwrAscFullList.get((i-1)));
		}


		List<Integer> releaseIdList = new ArrayList<Integer>();
		releaseIdList.add(id);

		for (int i = 0; i < fwrDescFullList.size(); i++) {
			FwReleaseBean fwrBean = fwrDescFullList.get(i);
			if (releaseIdList.contains(fwrBean.getId()) == false) continue;

			String releaseDependente = fwrBean.getReleaseDependente();
			if (StringUtils.hasValue(releaseDependente) == false) continue;

			String[] parts = releaseDependente.split(",");
			for (String part : parts) {
				String xxx = part.split(":")[1];
				releaseIdList.add(NumberUtils.parseInt(xxx));
			}
		}
		Collections.sort(releaseIdList);
		for (Integer fwrIdX : releaseIdList) {
			for (FwReleaseBean fwr : fwrAscFullList) {
				if (fwrIdX.equals(fwr.getId()) == false) continue;

				fwrList.add(fwr);
			}
		}

		for (int i = 0; i < fwrList.size(); i++) {
			StringBuilder logToDB = new StringBuilder();

			FwReleaseBean dtrBean = fwrList.get(i);
			temp = "Atualizando relase " + dtrBean.getId() + ": " + dtrBean.getDescricao() + ".";
			logToDB.append(temp + "<br/>");
			logToDB.append("Programador: "+ dtrBean.getProgramador() + "<br/>");

			String path = getPathServer() + FWConstants.PATH_RELEASE +"/" + dtrBean.getId() + "/";
			String fileNameMD5 = "md5.txt";
			String fileNameScript = "script.sql";
			String fileNameDeleted = "filetodelete.txt";
			String fileNamePackage = "release" + dtrBean.getId() + ".zip";
			String fullNameMD5 = path + fileNameMD5;
			String fullNameScript = path + fileNameScript;
			String fullNameDeleted = path + fileNameDeleted;
			String fullNamePackage = path + fileNamePackage;

			File fileOut = new File(fullNamePackage);
			FileUtils.createFileFromBytes(fileOut, dtrBean.getArquivo().getData());
			// XXX verificar este zip
			FileUtils.unzip(fullNamePackage, path);
			FileUtils.deleteFile(fullNamePackage);

			// aqui atualiza o banco de dados...
			List<String> xxx = updateDataBase((Connection)trans.getConnection(), fullNameScript);
			for (String x : xxx) {
				temp = x;
				logToDB.append(temp + "<br/>");
				if(x.startsWith("[ERRO-")) {
					countErro++;
					sbError.append(temp + "<br/>");
				}
				if(x.startsWith("[OK]")) countSucesso++;
			}


			// aqui é para apagar os arquivos que não estam mais sendo usados;
			// o programador coloca o arquivo em uma lista
			try {
				FileMemory fm = new FileMemory(fullNameDeleted, true);
				String[] yyy = fm.getRows();
				Integer count = 0;
				if (yyy != null) {
					for (String file : yyy) {
						if (StringUtils.hasValue(file) == false) continue;

						String arquivo = getPathServer() + "/" + file;
						boolean result = FileUtils.deleteFile(arquivo);
						if (result == true) {
							countSucesso++;
							logToDB.append("[OK] ");
						} else {
							countErro++;
							sbError.append(temp + "<br/>");
							logToDB.append("[ERRO-3] ");
						}
						logToDB.append(file + " <br/>");
						count++;
					}
				}
				if (count > 0) {
					temp = "[OK] Excluido "+ count +" arquivo(s) em desuso com sucesso.";
					countSucesso++;
					logToDB.append(temp + " <br/><br/>");
				}
			} catch (FileNotFoundException e) {
				// nao faz nada
			} catch (Exception e) {
				LOG.error(e.getMessage());
			}

			//
			// aqui é para copiar os arquivo para a aplicacao
			//
			File releaseDir = new File(path);
			FileUtils.copyDirectory(releaseDir, new File(getPathServer()));
			List<File> yyy = FileUtils.listPath(null, releaseDir);
			for (File f : yyy) {
				logToDB.append("[OK] " + f.getPath() + " <br/>");
			}
			temp = "[OK] Copiado " + yyy.size() + " arquivo(s) com sucesso.";
			countSucesso++;
			logToDB.append(temp + "<br/><br/>");
			// apagando as coisas que não vao usar mais...
			FileUtils.deleteDirectory(path);

			String log = logToDB.toString();
			sb.append(log);

			log = SQLUtils.formatPartStringToShow(log, 3990);
			dtrBean.setAplicado(FWConstants.SIM);
			dtrBean.setLog(log);
			dtrBean.setArquivo(null);
			dtrBean.setDataProcessamento(trans.getDataAtual());
			trans.update(dtrBean);

			// aqui preciso aplicar o commit...
			// pois se o tomcat pensar em reiniciar o processo de atualizacao no meio do caminho
			// ferra comigo
			// entao tem que dar o COMMIT!!!
			trans.commit();
		}

		StringBuilder xxx = new StringBuilder();
		xxx.append("Foram aplicadas " + fwrList.size() + " releases.<br/>");
		xxx.append("Linhas sem erro: " + countSucesso+"<br/>");
		xxx.append("Linhas com erro: " + countErro+"<br/>");
		if (countErro > 0) {
			message.setType(Type.WARNING);
			xxx.append( sbError.toString() );
			super.setMessageWarning( xxx.toString() );
		} else {
			super.setMessageSuccess( xxx.toString() );
		}

		if (fwrList.size() == 1) {
			message.addContent("Foi aplicado 1 release.");
			message.addContent("Linhas sem erro: " + countSucesso);
			message.addContent("Linhas com erro: " + countErro);
			message.addBreakLine();
		} else {
			message.addContent("Foram aplicadas " + fwrList.size() + " releases.");
			message.addContent("Linhas sem erro: " + countSucesso);
			message.addContent("Linhas com erro: " + countErro);
			message.addBreakLine();
		}

		message.addContent(sb.toString());
	}
	public void doProcessTableDiffRun(Integer id) throws Exception {
		TransactionDB trans = super.getTransaction();
		UserContext userContext = super.getUserContext();
		//
		id = super.getParamInteger("id");
		List<Bean> list = super.getObjectSession(KEY);
		
		
		List<String> result = null; 
		for (Bean b : list) {
			if (b.get("id").equals(id) == false) continue;

			String command = (String)b.get("comando");
			if (command.contains("drop"))
				command = "--" + command;
				
			FileMemory fm = new FileMemory(command, "temp.txt");

			result = updateDataBase(trans.getConnection(), fm);
			break;
		}

		StringBuilder sb = new StringBuilder();
		boolean error = false;
		for (String msg : result) {
			if (msg.startsWith("[OK]") == false)
				error = true;
			
			sb.append(msg + "<br />");
		}
		
		if (error)
			super.setMessageWarning(sb.toString());
		else 
			super.setMessageSuccess(sb.toString());
	}


	// HELPER
	public static List<String> updateDataBase(Connection conn, String scriptNameFile) throws Exception {
		File f = new File(scriptNameFile);
		if (f.exists() == false) return new ArrayList<String>(); 
		
		return updateDataBase(conn, new FileMemory(f));
	}
	public static List<String> updateDataBase(Connection conn, FileMemory fm) throws Exception {
		List<String> result = new ArrayList<String>();
		if (fm.getText() != null) {

			String[] sql = fm.getText().split("/\\*dotum\\*/");

			for (String sql_part : sql) {
				if (sql_part == null) continue;

				sql_part = sql_part.trim();
				if (StringUtils.hasValue( sql_part ) == false) continue;
				if (sql_part.toLowerCase().startsWith("execute")) {
					try {
						String proc = sql_part.substring(8);
						CallableStatement cs = conn.prepareCall("{ call "+proc+" }");
						cs.execute();

						result.add("[OK] " + sql_part);
					} catch (Exception ex) {
						result.add("[ERRO-1] " + sql_part + "Erro: " + ex.getMessage());
					}
				} else {
					try {
						Statement stmt = conn.createStatement();
						stmt.executeUpdate(sql_part);

						result.add("[OK] " + sql_part);
					} catch (Exception ex) {
						result.add("[ERRO-2] " + sql_part + "Erro: " + ex.getMessage());
					}
				}
			}
		}
		return result;
	}


}
