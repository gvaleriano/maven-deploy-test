package br.com.dotum.core.basic999;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Task;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.TextFieldHtml;
import br.com.dotum.core.html.other.ColumnHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.ORM;
import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.db.bean.GrupoAcessoBean;
import br.com.dotum.jedi.core.db.bean.GrupoAcessoTarefaBean;
import br.com.dotum.jedi.core.db.bean.PessoaBean;
import br.com.dotum.jedi.core.db.bean.PessoaTarefaBean;
import br.com.dotum.jedi.core.db.bean.TarefaBean;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.BeanUtils;
import br.com.dotum.jedi.util.PropertiesUtils;

@Menu(label="Controle de acesso", inMenu=Menu.POSITION_2_0_USER, icon=CssIconEnum.KEY)
@Task({Task.DEV_CODE, Task.ADMIN_CODE})
public class Controller010ControleAcesso extends AbstractController {

	private static final String PES_SELECTED = "_$jk&ub_PESSOA_SELECIONADA_NJK76";
	private static final String GRUA_SELECTED = "_$jk&ub_GRUPO_ACESSO_SELECIONADA_NJK76";

	public void doScreen() throws Exception {
		ColumnHtml column = new ColumnHtml();


		TableHtml acaoTable = new TableHtml("doDataTableTarefaBotao");
		acaoTable.setCheckbok(true);
		acaoTable.setLabel("Botões da tela");
		acaoTable.addColumn("#", "codigo");
		acaoTable.addColumn("Tela", "descricao");
		acaoTable.addColumn("Tem", "Tem");
		acaoTable.addButton(getClass(), "doProcessAtivarDesativarTarefa").icon(CssIconEnum.EXCHANGE).color(CssColorEnum.BLACK).label("Ativar/Desativar");
		acaoTable.addButton(getClass(), "doProcessAtivarDesativarTarefa").icon(CssIconEnum.EXCHANGE).color(CssColorEnum.BLUE).label("Ativar/Desativar").selecteds(Menu.MANY);


		TableHtml perfilTable = new TableHtml("doDataTableTarefaTela");
		perfilTable.setCheckbok(true);
		perfilTable.setLabel("Telas");
		perfilTable.addColumn("#", "codigo");
		perfilTable.addColumn("Tela", "descricao");
		perfilTable.addColumn("Tem", "Tem");
		perfilTable.addButton(getClass(), "doProcessAtivarDesativarTarefa").icon(CssIconEnum.EXCHANGE).color(CssColorEnum.BLACK).label("Ativar/Desativar").dataComponent(acaoTable);
		perfilTable.addButton(getClass(), "doProcessVerAcoes").icon(CssIconEnum.EYE).color(CssColorEnum.BLACK).label("Ver ações").dataComponent(acaoTable);
		perfilTable.addButton(getClass(), "doProcessAtivarDesativarTarefa").icon(CssIconEnum.EXCHANGE).color(CssColorEnum.BLUE).label("Ativar/Desativar").dataComponent(acaoTable).selecteds(Menu.MANY);

		FormHtml form = createFormInserirGrupoAcesso();

		TableHtml grupoAcessoTable = new TableHtml("doDataTableGrupoTarefa");
		grupoAcessoTable.setLabel("Grupos de Acesso");
		grupoAcessoTable.addColumn("#", "Id");
		grupoAcessoTable.addColumn("Descrição", "descricao");
		grupoAcessoTable.addColumn("Selecionado", "selecionado");
		grupoAcessoTable.addButton(getClass(), null).icon(CssIconEnum.INSERIR).color(CssColorEnum.GREEN).label("Inserir").createWindow(form).selecteds(Menu.ZERO);
		form.setDataComponent(grupoAcessoTable);


		TableHtml usuarioTable = new TableHtml("doDataTableUsuario");
		usuarioTable.setLabel("Usuários");
		usuarioTable.addColumn("#", "id");
		usuarioTable.addColumn("Nome", "nome");
		usuarioTable.addColumn("Login", "login");
		usuarioTable.addColumn("Selecionado", "selecionado");


		usuarioTable.addButton(getClass(), "doProcessSelecionarUsuario").icon(CssIconEnum.ARROW_RIGHT).color(CssColorEnum.BLACK).label("Selecionar").dataComponent(grupoAcessoTable, usuarioTable, perfilTable, acaoTable);
		grupoAcessoTable.addButton(getClass(), "doProcessSelecionarGrupoAcesso").icon(CssIconEnum.ARROW_RIGHT).color(CssColorEnum.BLACK).label("Selecionar").dataComponent(grupoAcessoTable, usuarioTable, perfilTable, acaoTable);


		column.addContent(1, grupoAcessoTable);
		column.addContent(1, usuarioTable);
		column.addContent(2, perfilTable);
		column.addContent(3, acaoTable);

		super.addBody(column);
	}

	public Object doDataTableGrupoTarefa() throws Exception {
		TransactionDB trans = super.getTransaction();
		List<GrupoAcessoBean> gruaList = trans.select(GrupoAcessoBean.class);

		GrupoAcessoBean gruaBean = super.getObjectSession(GRUA_SELECTED);
		if (gruaBean != null) {
			for (GrupoAcessoBean grua : gruaList) {
				grua.set("Selecionado", null);
				if (grua.getId().equals(gruaBean.getId())) grua.set("Selecionado", "X");
			}
		}

		return gruaList;
	}
	public Object doDataTableUsuario() throws Exception {
		TransactionDB trans = getTransaction();
		UserContext userContext = super.getUserContext();

		AbstractControleAcesso o = getObj();
		List<Bean> pesList = o.doDataUsuarioList(trans, userContext);

		Bean pesBean = super.getObjectSession(PES_SELECTED);
		if (pesBean != null) {
			for (Bean pes : pesList) {
				pes.set("Selecionado", null);
				if (pes.get("Id").equals(pesBean.get("Id"))) pes.set("Selecionado", "X");
			}
		}

		return pesList;
	}
	public Object doDataTableTarefaTela() throws Exception {
		TransactionDB trans = super.getTransaction();

		List<TarefaBean> tarList = doDataTarefa(trans, new Integer[] {1, 3, 4, 6});
		Bean pesBean = super.getObjectSession(PES_SELECTED);
		GrupoAcessoBean gruaBean = super.getObjectSession(GRUA_SELECTED);

		if ((pesBean != null) && (pesBean.get("Id") != null)) {

			AbstractControleAcesso o = getObj();
			List<Bean> petaList = o.doDataUsuarioTarefaList(trans, pesBean.get("Id"));

			setTemOnTarefa(tarList, petaList);

		} else if(gruaBean != null) {

			List<Bean> grtaList = null;
			try {
				WhereDB where = new WhereDB();
				where.add("GrupoAcesso.Id", Condition.EQUALS, gruaBean.getId());
				grtaList = trans.select(GrupoAcessoTarefaBean.class, where);
			} catch (NotFoundException e) {
				grtaList = new ArrayList<Bean>();
			}

			setTemOnTarefa(tarList, grtaList);
		}

		return tarList;
	}
	public Object doDataTableTarefaBotao() throws Exception {
		TransactionDB trans = super.getTransaction();

		TarefaBean tarBean = super.getObjectSession("tarefaFiltro");

		List<TarefaBean> tarList = null;
		if (tarBean != null) {
			WhereDB where = new WhereDB();
			where.add("Id", Condition.GREAT, 9999);
			where.add("Codigo", Condition.LIKEBEGIN, tarBean.getCodigo());
			tarList = trans.select(TarefaBean.class, where);
			for (TarefaBean b : tarList) {
				b.set("Tem", null);
			}

			Bean pesBean = super.getObjectSession(PES_SELECTED);
			GrupoAcessoBean gruaBean = super.getObjectSession(GRUA_SELECTED);

			if (pesBean != null) {


				AbstractControleAcesso o = getObj();
				List<Bean> petaList = o.doDataUsuarioTarefaList(trans, pesBean.get("Id"));


				setTemOnTarefa(tarList, petaList);

			}else if(gruaBean != null) {

				List<Bean> grtaList = null;
				try {
					where = new WhereDB();
					where.add("GrupoAcesso.Id", Condition.EQUALS, gruaBean.getId());
					grtaList = trans.select(GrupoAcessoTarefaBean.class, where);
				} catch (NotFoundException e) {
					grtaList = new ArrayList<Bean>();
				}

				setTemOnTarefa(tarList, grtaList);
			}
		}

		return tarList;
	}


	public void doProcessSelecionarUsuario(Integer usuId) throws Exception {
		TransactionDB trans = super.getTransaction();

		usuId = super.getParamInteger("id");


		AbstractControleAcesso o = getObj();
		Bean pesBean = o.doDataUsuario(trans, usuId);

		super.setObjectSession(PES_SELECTED, pesBean);
		super.setObjectSession(GRUA_SELECTED, new GrupoAcessoBean());
		super.setMessageSuccess("Usuario "+ pesBean.get("Nome") +" selecionado.");
	}
	public void doProcessSelecionarGrupoAcesso(Integer gruaId) throws Exception {
		TransactionDB trans = super.getTransaction();

		Integer id = super.getParamInteger("id");

		GrupoAcessoBean gruaBean = trans.selectById(GrupoAcessoBean.class, id);
		super.setObjectSession(GRUA_SELECTED, gruaBean);
		super.setObjectSession(PES_SELECTED, new PessoaBean());
		super.setMessageSuccess("Grupo de Acesso " + gruaBean.getDescricao() + " selecionado.");
	}
	public void doProcessVerAcoes(Integer tarId) throws Exception {
		TransactionDB trans = super.getTransaction();

		Integer id = super.getParamInteger("id");

		TarefaBean tarBean = trans.selectById(TarefaBean.class, id);
		super.setObjectSession("tarefaFiltro", tarBean);
		super.setMessageSuccess("Filtro realizado com sucesso");
	}
	public void doProcessAtivarDesativarTarefa(Integer ... tarefaIdArray) throws Exception {
		TransactionDB trans = super.getTransaction();

		Integer[] ids = super.getParamIntegerArray("id");

		Bean pesBean = super.getObjectSession(PES_SELECTED);
		GrupoAcessoBean gruaBean = super.getObjectSession(GRUA_SELECTED);

		AbstractControleAcesso o = getObj();


		if ((pesBean != null) && (pesBean.get("Id") != null)) {
			for (Integer tarefaId : ids) {

				TarefaBean tarBean = trans.selectById(TarefaBean.class, tarefaId);
				if (tarBean.getCodigo().length() <= 5)
					super.setObjectSession("tarefaFiltro", tarBean);

				try {
					o.doDataUsuarioTarefa(trans, pesBean.get("Id"), tarefaId);

					try {
						WhereDB where = new WhereDB();
						where.add("Codigo", Condition.LIKEBEGIN, tarBean.getCodigo());
						List<TarefaBean> tarList = trans.select(TarefaBean.class, where);

						Integer[] idArray = BeanUtils.getPropertieByListToArray(tarList, "Id");
						o.doProcessUsuarioDeletar(trans, pesBean.get("Id"), idArray);

					} catch (NotFoundException e) {
					}

					super.setMessageSuccess("Tarefa REMOVIDA do \"" + pesBean.get("Nome") + "\".");
				} catch (NotFoundException e) {

					o.doProcessUsuarioInserir(trans, pesBean.get("Id"), tarefaId);

					super.setMessageSuccess("Tarefa ADICIONADA para o \"" + pesBean.get("Nome") + "\".");

				}
			} 
		}else if (gruaBean != null) {
			for (Integer tarefaId : ids) {

				TarefaBean tarBean = trans.selectById(TarefaBean.class, tarefaId);
				if (tarBean.getCodigo().length() <= 5)
					super.setObjectSession("tarefaFiltro", tarBean);


				try {

					WhereDB where = new WhereDB();
					where.add("GrupoAcesso.Id", Condition.EQUALS, gruaBean.getId());
					where.add("Tarefa.Id", Condition.EQUALS, tarefaId);
					trans.select(GrupoAcessoTarefaBean.class, where);

					try {
						where = new WhereDB();
						where.add("Codigo", Condition.LIKEBEGIN, tarBean.getCodigo());
						List<TarefaBean> tarList = trans.select(TarefaBean.class, where);


						Integer[] idArray = BeanUtils.getPropertieByListToArray(tarList, "Id");

						where = new WhereDB();
						where.add("GrupoAcesso.Id", Condition.EQUALS, gruaBean.getId());
						where.add("Tarefa.Id", Condition.IN, idArray);
						trans.delete(GrupoAcessoTarefaBean.class, where);
					} catch (NotFoundException e) {

					}

					super.setMessageSuccess("Tarefa REMOVIDA do grupo \"" + gruaBean.getDescricao() + "\".");
				} catch (NotFoundException e) {

					GrupoAcessoTarefaBean uspeBean = new GrupoAcessoTarefaBean();
					uspeBean.getGrupoAcesso().setId(gruaBean.getId());
					uspeBean.getTarefa().setId(tarefaId);
					trans.insert(uspeBean);

					super.setMessageSuccess("Tarefa ADICIONADA para o grupo \"" + gruaBean.getDescricao() + "\".");
				}
			}
		} else {
			super.setMessageWarning("Selecione um usuario ou grupo antes de usar a ação de liberar/bloquear o perfil");
		}
	}
	public void doProcessInserirGrupoAcesso(Integer id) throws DeveloperException, Exception {
		TransactionDB trans = super.getTransaction();

		String descricao = super.getParamString("descricao");

		GrupoAcessoBean gruaBean = new GrupoAcessoBean();
		gruaBean.setDescricao(descricao);
		trans.insert(gruaBean);
	}

	private FormHtml createFormInserirGrupoAcesso() throws DeveloperException {

		FormHtml form = new FormHtml();

		TextFieldHtml f1 = new TextFieldHtml("descricao", "Descrição", null, true);

		form.addGroup(null, f1);
		form.addButton("doProcessInserirGrupoAcesso", "Salvar");

		return form;
	}

	private AbstractControleAcesso getObj() throws Exception {

		AbstractControleAcesso o = null;
		try {
			String pack = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
			Class c = Class.forName(pack + ".control.ControleAcessoImpl");
			o = (AbstractControleAcesso)c.newInstance();

		} catch (ClassNotFoundException e) {
		
			o = ControleAcessoImpl.class.newInstance();
			
		}

		return o;
	}
	private List<TarefaBean> doDataTarefa(TransactionDB trans, Integer[] cpttIdArray) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("        select tar.*                                                                                     ");
		sb.append("          From tarefa tar                                                                                ");
		sb.append("          join componente cpt on (cpt.cpt_id = tar.tar_id                                                ");
		sb.append("                              and cpt.cptt_id in "+ ArrayUtils.stringToIn(cpttIdArray) + ")                      ");

		ORM orm = new ORM();
		orm.add(Integer.class, "id", "tar_id");
		orm.add(String.class, "descricao", "tar_descricao");
		orm.add(String.class, "codigo", "tar_codigo");

		List<TarefaBean> bList = trans.select(TarefaBean.class, orm, sb.toString());
		for (TarefaBean b : bList) {
			b.set("Tem", null);
		}

		return bList;
	}
	private void setTemOnTarefa(List<TarefaBean> bList, List<Bean> petaList) {
		for (Bean petaBean : petaList) {
			for (TarefaBean b : bList) {
				if (b.getId().equals(petaBean.get("Tarefa.Id")) == false) continue;
				b.set("Tem", "X");

				break;
			}
		}
	}

}
