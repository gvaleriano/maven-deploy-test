package br.com.dotum.core.basic999;

import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.util.PropertiesUtils;

@Menu(label="Erro 403", icon=CssIconEnum.HOME, publico=true)
public class Controller403Forbidden extends AbstractController {

	public void doScreen() throws Exception {
		
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"row\">");
		sb.append("<div class=\"col-xs-2\">");
		sb.append("</div>");
		
		sb.append("	<div class=\"col-xs-8\">");
		sb.append("		<div class=\"error-container\">");
		sb.append("			<div class=\"well\">");
		sb.append("				<h1 class=\"grey lighter smaller\">");
		sb.append("					<span class=\"blue bigger-125\">");
		sb.append("						<i class=\"ace-icon fa fa-lock\"></i>");
		sb.append("						403");
		sb.append("					</span>");
		sb.append("					Acesso negado");
		sb.append("				</h1>");
		sb.append("				<hr />");
		sb.append("				<h3 class=\"lighter smaller\">Você não tem permissão para acessar este endereço.</h3>");
		sb.append("				<div>");

		sb.append("					<div class=\"space-10\"></div>");
		sb.append("					<h3 class=\"lighter smaller\">Tente uma das seguintes opções:</h3>");

		sb.append("					<ul class=\"list-unstyled spaced inline bigger-110 margin-15\">");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Revise com qual usuario você esta acessando.");
		sb.append("						</li>");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		
		
		UrlHtml url = new UrlHtml();
		url.setLabel("Clique aqui");
		String urlStr = PropertiesUtils.getString(PropertiesConstants.SYSTEM_LINK_LOGIN);
		url.setLink(urlStr);
		
		sb.append("							Vá para a tela de login " + url.getFullLink() +".");
		sb.append("						</li>");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Solicite a uma pessoa que autorize o acesso.");
		sb.append("						</li>");
		sb.append("					</ul>");
		sb.append("				</div>");
		sb.append("				<hr />");
		sb.append("				<div class=\"space\"></div>");

		sb.append("			</div>");
		sb.append("		</div>");
		sb.append("	</div>");
		sb.append("<div class=\"col-xs-2\">");
		sb.append("</div>");
		sb.append("	</div>");
		Html comp = new Html(sb.toString());
		super.addBody(comp);
	}
}


