package br.com.dotum.core.basic999;

import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.other.Html;

@Menu(label="Erro 404", icon=CssIconEnum.HOME, publico=true)
public class Controller404Error extends AbstractController {

	public void doScreen() throws Exception {
		
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"row\">");
		sb.append("<div class=\"col-xs-2\">");
		sb.append("</div>");
		
		sb.append("	<div class=\"col-xs-8\">");
		sb.append("		<div class=\"error-container\">");
		sb.append("			<div class=\"well\">");
		sb.append("				<h1 class=\"grey lighter smaller\">");
		sb.append("					<span class=\"blue bigger-125\">");
		sb.append("						<i class=\"ace-icon fa fa-sitemap\"></i>");
		sb.append("						404");
		sb.append("					</span>");
		sb.append("					Opz, página não encontrado");
		sb.append("				</h1>");
		sb.append("				<hr />");
		sb.append("				<h3 class=\"lighter smaller\">A url que está tenteando acessar não existe.</h3>");
		sb.append("				<div>");

		sb.append("					<div class=\"space-10\"></div>");
		sb.append("					<h3 class=\"lighter smaller\">Tente uma das seguintes opções:</h3>");

		sb.append("					<ul class=\"list-unstyled spaced inline bigger-110 margin-15\">");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Verifique a url que está tentando acessar.");
		sb.append("						</li>");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Revise o arquivo config.properties.");
		sb.append("						</li>");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Se você chegou aqui através um link favorito, remova o e adicione novamente");
		sb.append("						</li>");
		sb.append("					</ul>");
		sb.append("				</div>");
		sb.append("				<hr />");
		sb.append("				<div class=\"space\"></div>");

		sb.append("			</div>");
		sb.append("		</div>");
		sb.append("	</div>");
		sb.append("<div class=\"col-xs-2\">");
		sb.append("</div>");
		sb.append("	</div>");
		Html comp = new Html(sb.toString());
		super.addBody(comp);
	}
}


