package br.com.dotum.core.basic999;

import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.util.PropertiesUtils;

@Menu(label="Erro 500", icon=CssIconEnum.HOME, publico=true)
public class Controller500Error extends AbstractController {

	public void doScreen() throws Exception {
		
		String type = PropertiesUtils.getString(PropertiesConstants.DATABASE_TYPE);
		String ip = PropertiesUtils.getString(PropertiesConstants.DATABASE_IP);
		String port = PropertiesUtils.getString(PropertiesConstants.DATABASE_PORT);
		String name = PropertiesUtils.getString(PropertiesConstants.DATABASE_NAME);
		String userName = PropertiesUtils.getString(PropertiesConstants.DATABASE_USARNAME);

		
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"row\">");
		sb.append("<div class=\"col-xs-2\">");
		sb.append("</div>");
		
		sb.append("	<div class=\"col-xs-8\">");
		sb.append("		<div class=\"error-container\">");
		sb.append("			<div class=\"well\">");
		sb.append("				<h1 class=\"grey lighter smaller\">");
		sb.append("					<span class=\"blue bigger-125\">");
		sb.append("						<i class=\"ace-icon fa fa-sitemap\"></i>");
		sb.append("						500");
		sb.append("					</span>");
		sb.append("					Opz, erro interno no servidor");
		sb.append("				</h1>");
		sb.append("				<hr />");
		sb.append("				<h3 class=\"lighter smaller\">Não foi possivel acessar o banco de dados.</h3>");
		sb.append("				<div>");

		sb.append("					<div class=\"space-10\"></div>");
		sb.append("					<h3 class=\"lighter smaller\">Verifique o arquivo de propriedade. Ele esta com os seguintes parametros:</h3>");

		sb.append("					<ul class=\"list-unstyled spaced inline bigger-110 margin-15\">");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Tipo: " + type);
		sb.append("						</li>");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Endereço: " + ip + ":" + port);
		sb.append("						</li>");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Banco: " + name);
		sb.append("						</li>");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Usuario: " + userName);
		sb.append("						</li>");
		sb.append("						<li>");
		sb.append("							<i class=\"ace-icon fa fa-hand-o-right blue\"></i>");
		sb.append("							Senha: **************");
		sb.append("						</li>");
		sb.append("					</ul>");
		sb.append("				</div>");
		sb.append("				<hr />");
		sb.append("				<div class=\"space\"></div>");

		sb.append("			</div>");
		sb.append("		</div>");
		sb.append("	</div>");
		sb.append("<div class=\"col-xs-2\">");
		sb.append("</div>");
		sb.append("	</div>");
		Html comp = new Html(sb.toString());
		super.addBody(comp);
	}
}


