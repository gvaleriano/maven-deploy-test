package br.com.dotum.core.basic999;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.AbstractReport;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.form.field.ComboTextFieldHtml;
import br.com.dotum.core.html.form.field.FieldFactory;
import br.com.dotum.core.html.form.field.HiddenFieldHtml;
import br.com.dotum.core.html.other.ColumnHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.MessageHtml.Type;
import br.com.dotum.core.html.other.WidgetHtml;
import br.com.dotum.core.servlet.DoServlet;
import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumReportInfo;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.component.xml.smartreport.model.Parm;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.StringUtils;

@Menu(group="Relatório", label="DotumSmartReport", icon=CssIconEnum.PRINT, publico=true)
public class Controller900Report extends AbstractController {

	private static final String TYPE = "type";
	
	public void doScreen() throws Exception {
		UserContext userContext = super.getUserContext();
		TransactionDB trans = super.getTransaction();

		boolean emitir = false;
		String codigoReport = super.getParamString(FWConstants.PARAM_FW_CLASS);
		if (codigoReport.equals( ClassUtils.getCodeTask(getClass()) )) {

			emitir = true;
			codigoReport = super.getParamString(FWConstants.PARAM_FW_REPORT_CODE);
		}
		
		Map<String, DotumSmartReport> classeFullMap = (Map<String, DotumSmartReport>)getRequest().getServletContext().getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_SMARTREPORT_MAP);
		DotumSmartReport report = classeFullMap.get(codigoReport);
		super.getResultHtml().setTitle(report.getMenu().getGroup() + ": " + report.getTitle());
		String type = super.getParamString(TYPE);
		if (type == null) type = "";

		ComponentHtml wid1 = createHtmlForm(userContext, report, codigoReport);
		ComponentHtml wid2 = doProcess(userContext, trans, report, codigoReport, emitir, type);
		ColumnHtml column = createHtml(codigoReport, wid1, wid2, emitir);

		
		if (StringUtils.hasValue(type) && type.equals("HTML") == false) {
			Map<String, Object> parmsMap = new HashMap<String, Object>();
			for (Parm parm : report.getParms()) {
				String v = super.getParamString( parm.getName() );
				parmsMap.put(parm.getName(), v);
			}
			report.setType(type);
			DoServlet.exportReportDSR(getResponse(), getTransaction(), getUserContext(), report, parmsMap, type, null);
		} else {
			super.addBody(column);
		}
		
	}
	public static WidgetHtml createHtmlFormFilter(FormHtml form) throws Exception {
		WidgetHtml wid = new WidgetHtml("Filtro do relatorio", CssIconEnum.PRINT);
		wid.add(form);

		return wid;
	}


	public static ColumnHtml createHtml(String codigoReport, ComponentHtml formFilter1, ComponentHtml comp2, boolean emitir) {
		ColumnHtml column = new ColumnHtml(3, 9);
		column.addContent(1, formFilter1);
		column.addContent(2, comp2);

		return column;
	}
	public static WidgetHtml createHtmlForm(AbstractReport report, String codigoReport) throws Exception {
		if (report.getField().size() == 0)
			report.doExecute();

		FormHtml form = new FormHtml();
		form.setAjax(false);

		List<AbstractFieldHtml> fields = report.getField();
		form.addGroup(null, fields.toArray(new AbstractFieldHtml[fields.size()]));
		form.addButton(report.getClass(), "doExportar", "Emitir");


		return createHtmlFormFilter(form);
	}
	public WidgetHtml createHtmlForm(UserContext userContext, DotumSmartReport report, String codigoReport) throws Exception {
		Parm[] parmsArray = report.getParms();

		FormHtml form = new FormHtml(null);
		form.setAjax(false);

		int i = 0;
		for (Parm parm : parmsArray) {
			String value = super.getParamString(parm.getName());
			if (StringUtils.hasValue(value) == false) value = parm.getValue();
			
			
			AbstractFieldHtml field = FieldFactory.createField(userContext, parm.getName(), parm.getLabel(), value, parm.isRequired(), parm.getChave());
			Object obj = FieldFactory.parseValue(field, value);
			field.setValue(obj);
			
			
			field.setValue(value);
			form.addInLine(++i, 12, field);
		}
		ComboTextFieldHtml field = new ComboTextFieldHtml(TYPE, "Formato", "HTML", false);
		field.addOption("HTML", "HTML");
		field.addOption("PDF", "PDF");
		field.addOption("XLS", "XLS");
		field.addOption("DOC", "DOC");		
		form.addInLine(++i, 12, field);
		
		form.addGroup(null, new HiddenFieldHtml(FWConstants.PARAM_FW_REPORT_CODE, codigoReport));
		form.addButton("doScreen", "Emitir");

		return createHtmlFormFilter(form);
	}


	public ComponentHtml doProcess(UserContext userContext, TransactionDB trans, Object obj, String codigoReport, boolean emitir, String type) {
		ComponentHtml resultado = null;
		try {
			if ((emitir == true) && (type.equals("HTML"))) {
				if ((obj instanceof DotumSmartReport)) {
					resultado = doProcessSmartReport(userContext, trans, (DotumSmartReport)obj);
				} else if ((obj instanceof AbstractReport)) {
					resultado = doProcessJasper(userContext, trans, (AbstractReport)obj);
				} else {
					MessageHtml msg = createHtmlMessageReportDesconhecido();

					resultado = msg;
				}
			} else {
				MessageHtml msg = createHtmlMessageFiltarAoLado();

				resultado = msg;
			}
		} catch (Exception e) {
			MessageHtml msg = new MessageHtml(Type.WARNING);
			msg.setLabel("Atenção");

			msg.addContent("Não foi possivel emitir este relatorio.");
			msg.addContent("Entre em contato com sua equipe de TI e informe a mensagem a seguir:");
			msg.addBreakLine();
			msg.addContent("<b>" + codigoReport + ": </b>" + e.getMessage());

			resultado = msg;
		}

		return resultado;
	}
	public static MessageHtml createHtmlMessageFiltarAoLado() {
		MessageHtml msg = new MessageHtml(Type.SUCCESS);
		msg.setLabel("Atenção");
		msg.addContent("Preencha o formulario ao lado para emitir o relatorio.");

		return msg;
	}
	public static MessageHtml createHtmlMessageReportDesconhecido() {
		MessageHtml msg = new MessageHtml(Type.WARNING);
		msg.setLabel("Atenção");
		msg.addContent("Padrão deste relatorio desconhecido entre em contato com sua equipe de TI.");


		return msg;
	}
	public ComponentHtml doProcessJasper(UserContext userContext, TransactionDB trans, AbstractReport report) throws Exception {

		return null;
	}
	public ComponentHtml doProcessSmartReport(UserContext userContext, TransactionDB trans, DotumSmartReport report) throws Exception {
		report.setType("HTML");
		DotumSmartReportBuilder reportBuilder = new DotumSmartReportBuilder(trans);
		reportBuilder.setUserContext(userContext);

		String codigo = "";

		DotumReportInfo dri = new DotumReportInfo(codigo.toUpperCase(), "DotumErp", report.getVersion());
		reportBuilder.setReportInfo(dri);

		// mapa de parametros
		Map<String, Object> parmsMap = new HashMap<String, Object>();
		for (Parm parm : report.getParms()) {
			String v = super.getParamString( parm.getName() );
			parmsMap.put(parm.getName(), v);
		}

		HashMap<String, String> parmsStr = new HashMap<String, String>();
		reportBuilder.setParmsValues( parmsMap );
		reportBuilder.setParmsStr( parmsStr );

		reportBuilder.builder(codigo, report);

		FileMemory resultFm = reportBuilder.getFileMemory();

		return new Html(resultFm.getText());
	}
}

