package br.com.dotum.core.basic999;

import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.core.servlet.ServletHelper;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.util.PropertiesUtils;

@Menu(label="Sair", inMenu=Menu.POSITION_2_0_USER, icon=CssIconEnum.POWER_OFF, publico=true)
public class Controller999Exit extends AbstractController {

	public void doScreen() throws Exception {
		ServletHelper.setUsuarioSession(getRequest(), null);
		super.getRequest().getSession().invalidate();

		UrlHtml url = new UrlHtml();
		url.setLink(PropertiesUtils.getString(PropertiesConstants.SYSTEM_LINK_LOGIN));
		
		super.redirect(url);
	}
}

