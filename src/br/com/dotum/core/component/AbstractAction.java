package br.com.dotum.core.component;

import java.util.List;

import br.com.dotum.jedi.core.table.Bean;

public abstract class AbstractAction extends AbstractCommon {
	private AbstractView view;

	public abstract void doScreen() throws Exception;
	
	public void setView(AbstractView view) {
		this.view = view ;
	}

	protected AbstractView getView() {
		return view;
	}

	public void setAba(String abaPendente) {
		List<Bean> abaList = view.getAbas();

		for (Bean aba : abaList) {
			if (aba.getAttribute("label").equals(abaPendente) == false) continue;
			view.setObjectSession(view.getNameSessionFilterAbaProperty(), aba.getAttribute("property"));
			view.setObjectSession(view.getNameSessionFilterAbaValue(), aba.getAttribute("value"));
			break;
		}
	}

}
