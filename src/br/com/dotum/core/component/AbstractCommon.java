package br.com.dotum.core.component;

import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.form.ButtonHtml;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.core.json.JsonToBean;
import br.com.dotum.core.servlet.DispacherReport;
import br.com.dotum.core.servlet.DoServlet;
import br.com.dotum.core.servlet.ServletHelper;
import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;

public class AbstractCommon {

	private HttpServletRequest request;
	private HttpServletResponse response;
	private Map<String, Object> list;
	private UserContext userContext;
	private TransactionDB trans;
	private ResultHtml html;
	private List<ComponentHtml> bodyCompList = new ArrayList<ComponentHtml>();
	private boolean api;
	private Map<String, Object> tokenParam = new HashMap<String, Object>();


	private List<Boolean> downloadRequiredList = new ArrayList<Boolean>();
	private List<Integer> downloadSizeList = new ArrayList<Integer>();
	private List<FileMemory> downloadFileMemoryList = new ArrayList<FileMemory>();



	private String messageSuccess;
	private String messageWarning;
	private String messageError;

	/**
	 * Metodo que irá preparar uma acao para o usuario...
	 * @param servletContext
	 * @param request
	 * @param response
	 * @param list
	 * @throws Exception
	 */
	public void prepare(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, ResultHtml html, Map<String, Object> list) throws Exception {
		this.request = request;
		this.response = response;
		this.trans = trans;
		this.html = html;
		this.list = list;

		this.userContext = ServletHelper.getUsuarioSession(request);
		this.html.setUserContext(getUserContext());
	}


	public HttpServletRequest getRequest() {
		return request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public Map<String, Object> getList() {
		return list;
	}
	public UserContext getUserContext() {
		return userContext;
	}
	public TransactionDB getTransaction() {
		return trans;
	}
	public ResultHtml getResultHtml() {
		return html;
	}

	
	public String getMessageSuccess() {
		return messageSuccess;
	}
	public String getMessageWarning() {
		return messageWarning;
	}
	public String getMessageError() {
		return messageError;
	}


	public List<Boolean> getDownloadRequiredList() {
		return downloadRequiredList;
	}
	public List<Integer> getDownloadSizeList() {
		return downloadSizeList;
	}
	public List<FileMemory> getDownloadFileMemoryList() {
		return downloadFileMemoryList;
	}


	
	private String getPathServer(String param) {
		return getRequest().getRealPath(param);
	}
	public String getPathServer() {
		return getPathServer("/").replaceAll("\\\\", "/");
	}


	//
	// TRATAMENTO DAS MSGS
	//
	public void setMessageSuccess(AbstractMessage message, Object ... args) {
		this.messageSuccess = String.format(message.getDescricao(), args);
	}
	public void setMessageWarning(AbstractMessage message, Object ... args) {
		this.messageWarning = String.format(message.getDescricao(), args);
	}
	public void setMessageError(AbstractMessage message, Object ... args) {
		this.messageError = String.format(message.getDescricao(), args);
	}
	public void setMessageSuccess(String messageSuccess) {
		this.messageSuccess = messageSuccess;
	}
	public void setMessageWarning(String messageWarning) {
		this.messageWarning = messageWarning;
	}
	public void setMessageError(String messageError) {
		this.messageError = messageError;
	}

	//
	// SESSAO
	//
	public void setObjectSessionApplication(String key, Object value) {
		ServletHelper.setSessionApplication(getRequest(), key, value);
	}
	public <T> T getObjectSessionApplication(String key) {
		return (T) ServletHelper.getSessionApplication(getRequest(), key);
	}
	public void setObjectSession(String key, Object value) {
		ServletHelper.setSession(getRequest(), key, value);
	}
	public <T> T getObjectSession(String key) {
		return ServletHelper.getSession(getRequest(), key);
	}
	public void setObjectSessionScreen(String key, Object value) {
		ServletHelper.setSession(getRequest(), getClass().getName()+"_"+key, value);
	}
	public <T> T getObjectSessionScreen(String key) {
		return ServletHelper.getSession(getRequest(), getClass().getName()+"_"+key);
	}
	
	//
	// PARAM
	//
	// get Params
	private String prepareParam(String param) {
		return param.toLowerCase();
	}
	public Bean getParamBean(String param) throws Exception {
		String value = (String)ServletHelper.getParameter(getRequest(), getList(), prepareParam(param+"Json"), false);
		JsonToBean json = new JsonToBean(value);
		return json.getBean();
	}
	public List<Bean> getParamBeanList(String param) throws Exception {
		String value = (String)ServletHelper.getParameter(getRequest(), getList(), prepareParam(param+"Json"), false);
		JsonToBean json = new JsonToBean(value);
		return json.getBeanList();
	}
	public String getParamString(String param) throws Exception {
		return getParamString(param, false);
	}
	public String getParamString(String param, boolean required) throws Exception {
		String value = (String)ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), false);
		if (StringUtils.hasValue(value)) value = value.trim();
		if (required == true && value == null)
			throw new WarningException("Não foi informado o parametro \"" + param + "\". Favor verificar.");

		return value;
	}
	public String[] getParamStringArray(String param) throws Exception {
		Object obj = ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), true);
		if (obj == null) return new String[0];

		String[] partArray = null;
		if (obj instanceof String) {
			partArray = new String[]{(String)obj};
		} else if (obj instanceof String[]) {
			partArray = (String[])obj;
		}

		for (int i = 0; i < partArray.length; i++) {
			String part = partArray[i];
			if (StringUtils.hasValue(part)) partArray[i] = part.trim();
		}
		return partArray;
	}
	public Integer getParamInteger(String param) throws Exception {
		return getParamInteger(param, false);
	}
	public Integer getParamInteger(String param, boolean required) throws Exception {
		Object obj = ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), false);
		Integer value = null;
		if (obj instanceof String[]) {
			throw new DeveloperException("Estava esperando um Integer e veio um array com os seguintes valores: " + SQLUtils.strToIn((String[])obj));
		} else {
			value = NumberUtils.parseInt((String)obj);
		}
		if (required == true && value == null)
			throw new WarningException("Não foi informado o parametro \"" + param + "\". Favor verificar.");

		return value;
	}
	public Integer[] getParamIntegerArray(String param) throws Exception {
		Object obj = ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), true);
		if (obj == null) return new Integer[0];

		String[] array = null;
		if (obj instanceof String) {
			String objStr = (String)obj;
			
			//isso por que pode vir de uma table
			if (objStr.contains(",")) {
				array = objStr.split(",");
			} else {
				array = new String[]{objStr};
			}
		} else if (obj instanceof String[]) {
			array = (String[])obj;
			if (array.length == 1)
				array = array[0].split(",");
		}
		if (array == null) array = new String[0];
		Integer[] result = new Integer[array.length];
		for (int i = 0; i < array.length; i++) {
			
			
			// nao sei por que estava fazendo primeiro como BigDecimal
			// e depois o parse padrao!!!
			// removi e deixou o sistema 0,045s mais rapido!!!
//			BigDecimal xxx = NumberUtils.parseBigDecimal(array[i], ',');
//			if (xxx != null) result[i] = xxx.intValue();
			result[i] = NumberUtils.parseInt(array[i]);
		}
		return result;
	}
	public BigDecimal getParamBigDecimal(String param) throws Exception {
		return getParamBigDecimal(param, false);
	}
	public BigDecimal getParamBigDecimal(String param, boolean required) throws Exception {
		BigDecimal value = NumberUtils.parseBigDecimal((String)ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), false), ',');
		if (required == true && value == null)
			throw new WarningException("Não foi informado o parametro \"" + param + "\". Favor verificar.");

		return value;
	}
	public BigDecimal[] getParamBigDecimalArray(String param) throws Exception {
		Object obj = ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), true);
		if (obj == null) return new BigDecimal[0];

		String[] array = null;
		if (obj instanceof String) {
			array = new String[]{(String)obj};
		} else if (obj instanceof String[]) {
			array = (String[])obj;
		}
		if (array == null) array = new String[0];
		BigDecimal[] result = new BigDecimal[array.length];
		for (int i = 0; i < array.length; i++) {
			result[i] = NumberUtils.parseBigDecimal(array[i]);
		}
		return result;
	}
	public Boolean getParamBoolean(String param) throws Exception {
		return getParamBoolean(param, false);
	}
	public Boolean getParamBoolean(String param, boolean required) throws Exception {
		String value = (String)ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), false);
		if (required == true && value == null)
			throw new WarningException("Não foi informado o parametro \"" + param + "\". Favor verificar.");

		return (value != null && value.equals("1"));
	}
	public Date getParamDate(String param) throws Exception {
		return getParamDate(param, false);
	}
	public Date getParamDate(String param, boolean required) throws Exception {
		Date value = FormatUtils.parseDate((String)ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), false));
		if (required == true && value == null)
			throw new WarningException("Não foi informado o parametro \"" + param + "\". Favor verificar.");

		return value;
	}
	public FileMemory getParamFile(String param) throws Exception {
		return (FileMemory)ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), false);
	}
	public FileMemory[] getParamFileArray(String param) throws Exception {
		return (FileMemory[])ServletHelper.getParameter(getRequest(), getList(), prepareParam(param), true);
	}
	public <T> T getParamBean(Class<? extends Bean> classBean) throws Exception {

		Bean b = classBean.newInstance();
		Field[] fieldArray = classBean.getDeclaredFields();
		for (Field field : fieldArray) {
			String fieldName = field.getName();
			String param = field.getName().toLowerCase();

			if (field.getType().getSuperclass().equals(Bean.class)) {
				Integer value = getParamInteger( param+"id" );
				b.set(fieldName, value);
			} else if (field.getType().equals(Integer.class)) {
				Integer value = getParamInteger( param );
				b.set(fieldName, value);
			} else if (field.getType().equals(String.class)) {
				String value = getParamString( param );
				b.set(fieldName, value);
			} else if (field.getType().equals(Date.class)) {
				Date value = getParamDate( param );
				b.set(fieldName, value);
			} else if (field.getType().equals(BigDecimal.class)) {
				BigDecimal value = getParamBigDecimal( param );
				b.set(fieldName, value);
			} else if (field.getType().equals(Integer[].class)) {
				Integer[] value = getParamIntegerArray( param );
				b.set(fieldName, value);
			} else if (field.getType().equals(String[].class)) {
				String[] value = getParamStringArray( param );
				b.set(fieldName, value);
			} else {
				throw new Exception("Ainda não mapeado o parametro: " + param + " do tipo " + field.getType());
			}
		}

		return (T) b;
	}


	//
	// manipular HTML / SCRIPT
	//
	public void addStyleLink(String link) {
		this.getResultHtml().addStyleLink(link, null);
	}
	public void addScriptLink(String link) {
		this.getResultHtml().addScriptLink(link, null);
	}
	public void addScript(String script) {
		this.getResultHtml().addScript(script);
	}


	private void setRota(ComponentHtml comp) throws DeveloperException {
		setRota(getResultHtml(), getClass(), comp);
	}
	/**
	 * Este metodo irá colocar o destino do campo button (classe)
	 * Ainda definira a origem dos dados da tabela (classe)
	 *  
	 * @param comp
	 * @throws DeveloperException
	 */
	public static void setRota(ResultHtml html, Class<?> clazz, ComponentHtml comp) throws DeveloperException {
		Menu m = clazz.getDeclaredAnnotation(Menu.class);

		comp.setHtml(html);
		if (comp instanceof DataComponentHtml) {
			DataComponentHtml c = (DataComponentHtml)comp;
			c.setDataClass(clazz);

			if ((m != null) && (m.selecteds() > 0) && (c.getDataMethod() == null)) {
				throw new DeveloperException("Não pode passar null para o doData do componente "+ c.getClass().getSimpleName() +" na ação \""+ m.group() + (StringUtils.hasValue(m.group()) ? ":" : "") + m.label() +"\" quando é de um item da tabela. (" + clazz.getName()  + ")");
			}


			if (c instanceof FormHtml) {
				FormHtml f = (FormHtml)c;
				List<ButtonHtml> btnList = f.getAllButtonList();
				for (ButtonHtml btn : btnList) {
					btn.setClassAction(clazz);
					btn.setHtml(html);
				}
				List<AbstractFieldHtml> fieldList = f.getAllFieldList();
				for (AbstractFieldHtml field : fieldList) {
					field.setHtml(html);
				}

			} else if (c instanceof TableHtml) {
				TableHtml t = (TableHtml)c;
				List<ButtonHtml> btnList = t.getAllButtonList();
				for (ButtonHtml btn : btnList) {
					btn.setClassAction(clazz);
					btn.setHtml(html);
				}
			}
		} else if (comp instanceof ContainerComponentHtml) {
			List<ComponentHtml> cList = ((ContainerComponentHtml) comp).getComponentList();
			for (ComponentHtml c : cList) {
				setRota(html, clazz, c);
			}
		}
	}

	protected void doDownload(String rel) throws Exception {
		Integer id = getParamInteger("id");

		HashMap<String, Object> map = new HashMap<String, Object>();		
		map.put("uniId", getUserContext().getUnidade().getId());
		map.put("id", id);

		doDownload(rel, map, null, null);
	}
	protected void doDownload(String rel, HashMap<String, Object> map) throws Exception {
		doDownload(rel, map, null, null);
	}
	protected void doDownload(String rel, HashMap<String, Object> map, String fileName) throws Exception {
		doDownload(rel, map, null, fileName);
	}
	protected void doDownload(String rel, HashMap<String, Object> map, String type, String fileName) throws Exception {
		String pathRelative = this.getClass().getPackage().getName();
		pathRelative = pathRelative.replaceAll("\\.", "/") + "/";

		if (StringUtils.hasValue(type) == false) type = getParamString("type");
		String path = getUserContext().getServerPath() + FWConstants.TOMCAT_PATH_CLASSES + pathRelative + rel +".dsrxml";
		FileMemory f = new FileMemory(path);
		DotumSmartReport report = DotumSmartReportBuilder.prepare( f.getText() );

		DoServlet.exportReportDSR(getResponse(), getTransaction(), getUserContext(), report, map, type, fileName);
	}
	protected void doDownload(AbstractReport report, boolean required) throws Exception {
		doDownload(report, required, null);
	}
	protected void doDownload(AbstractReport report, boolean required, String fileName) throws Exception {
		report.setRequest(getRequest());
		FileMemory fm = DispacherReport.getBytesByIReport(getTransaction(), report);
		if (fileName != null) fm.setName(fileName);
		doDownload(fm, true);
	}
	protected void doDownload(Class<? extends AbstractReport> report, boolean required) throws Exception {
		doDownload(report, "id", required);
	}
	/**
	 * Gerar o relatorio baseado no iReport
	 * caso o tamanho do arquivo (PDF) seja 910 o mesmo irá mostrar uma mensagem generica;
	 * 
	 * @param report
	 * @param param
	 * @throws Exception
	 */
	protected void doDownload(Class<? extends AbstractReport> report, String param, boolean required) throws Exception {
		try {
			Integer id = getParamInteger("id");
			AbstractReport r = report.newInstance();
			r.setRequest(getRequest());
			r.addParamField("uniId", getUserContext().getUnidade().getId());
			r.addParamField(param, id);

			FileMemory fm = DispacherReport.getBytesByIReport(getTransaction(), r);
			doDownload(fm, 910, required);
		} catch (Exception e) {
			throw new WarningException(e.getMessage());
		}
	}
	protected void doDownload(FileMemory fm, boolean required) throws Exception {
		doDownload(fm, 0, required);
	}
	protected void doDownload(FileMemory fm, Integer sizeZerado, boolean required) throws Exception {
		try {
			downloadRequiredList.add(required);
			downloadSizeList.add(sizeZerado);
			downloadFileMemoryList.add(fm);
		} catch (Exception e) {
			throw new WarningException(e.getMessage());
		}
	}
	protected void doDownloadPost(AbstractReport report) throws Exception {
		FileMemory fm = DispacherReport.getBytesByIReport(getTransaction(), report);
		getResponse().setContentType("application/pdf");
		getResponse().setHeader("Content-disposition","attachment; filename=\""+fm.getName()+"\"");
		OutputStream ops = getResponse().getOutputStream();
		ops.write(fm.getBytes());
		ops.flush();
		ops.close();
	}
	protected void doDownloadLink(FileMemory fm) throws DeveloperException {
		DoServlet.exportFileMemory(getResponse(), getUserContext(), fm);		
	}

	public void addBody(String comp) throws DeveloperException {
		Html h = new Html(comp);
		setRota(h);
		this.bodyCompList.add( h );		
	}
	public void addBody(ComponentHtml comp) throws DeveloperException {
		setRota(comp);
		this.bodyCompList.add( comp );		
	}

	public List<ComponentHtml> getBodyCompList() {
		return this.bodyCompList;
	}

	//
	// METODOS DE LOG ARQUIVO
	//
	public void logError(String msg) {
		logError(msg, null);
	}
	public void logWarn(String msg) {
		logWarn(msg, null);
	}
	public void logInfo(String msg) {
		logInfo(msg, null);
	}
	public void logDebug(String msg) {
		logDebug(msg, null);		
	}

	public void logError(String msg, Throwable e) {
		Log LOG = LogFactory.getLog(getClass());
		LOG.error(getUserContext(), msg, e);		
	}
	public void logWarn(String msg, Throwable e) {
		Log LOG = LogFactory.getLog(getClass());
		LOG.warn(getUserContext(), msg, e);		
	}
	public void logInfo(String msg, Throwable e) {
		Log LOG = LogFactory.getLog(getClass());
		LOG.info(getUserContext(), msg, e);		
	}
	public void logDebug(String msg, Throwable e) {
		Log LOG = LogFactory.getLog(getClass());
		LOG.debug(getUserContext(), msg, e);		
	}


	public boolean isApi() {
		return api;
	}
	public void setApi(boolean api) {
		this.api = api;
	}
	
	public void addParamToken(String key, Object value) {
		tokenParam.put(key, value);
	}

	
}
