package br.com.dotum.core.component;

import java.util.List;

import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.core.html.view.ViewHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public abstract class AbstractController extends AbstractCommon {

	public abstract void doScreen() throws Exception;

	public void addComponente(String comp) {
		getResultHtml().addComponent( new Html(comp) );
	}

	public void redirect(UrlHtml url) throws Exception {
		getResponse().sendRedirect(url.getComponente());
	}

	public void addBody(ComponentHtml comp) throws DeveloperException {
		super.addBody(comp);
		
		// isso é necessario para criar o createScriptPopulateByJson dos componentes necessarios
		recursiveDataIdFormScript(comp);
	}

	private void recursiveDataIdFormScript(ComponentHtml comp) throws DeveloperException {

		if (comp instanceof DataComponentHtml) {
			DataComponentHtml c = (DataComponentHtml)comp;
			c.setDataClass(getClass());

			if (c instanceof FormHtml) {
				FormHtml f = (FormHtml)c;
				if (c.getDataMethod() == null) return;
                 if (f.isOnLoad()) 
                	 getResultHtml().addScript(f.createScriptPopulateByJson());
			} else if (c instanceof TableHtml) {
				// TableHtml t = (TableHtml)c;
				// !!!!! nao pode criar esse script aqui por que a tabela a primeira vez sabe se virar !!!!!
				// getResultHtml().addScript(t.createScriptPopulateByJson());
			} else if (c instanceof ViewHtml) {
				ViewHtml v = (ViewHtml)c;
				getResultHtml().addScript(v.createScriptPopulateByJson());
			}
		} else if (comp instanceof ContainerComponentHtml) {
			List<ComponentHtml> cList = ((ContainerComponentHtml) comp).getComponentList();
			for (ComponentHtml c : cList) {
				recursiveDataIdFormScript(c);
			}
		}

	}
}

