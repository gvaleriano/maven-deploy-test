package br.com.dotum.core.component;

import java.util.Date;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;

public abstract class AbstractJob implements Runnable {
	private static Log LOG = LogFactory.getLog(AbstractJob.class);

	private Integer executeX = 0;
	private TransactionDB trans;
	private UserContext userContext;

	private String messageSuccess;
	private String messageWarning;
	private String messageError;


	public abstract void doExecute() throws Exception;

	public TransactionDB getTransaction() throws Exception {
		if (trans == null) trans = TransactionDB.getInstance(null);
		return trans;
	}

	public void run() {
		String result = null;
		Date d1 = new Date();

		try {
			++executeX;

			try {
				doExecute();
				if (trans != null) trans.commit();
			} catch (Exception e) {
				if (trans != null) trans.rollback();
				setMessageError(e.getLocalizedMessage());
			} finally {
				if (trans != null) trans.close();
				trans = null;
			}

			Date d2 = new Date();
			result = "[JOB]["+ FormatUtils.formatDateWithHour(d2) +"]["+ executeX +"x][" + getClass().getName() + "]["+ DateUtils.diffTimeSimple(d1, d2) +"] " + getMessage();

			System.out.println( result );
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	public void setMessageSuccess(String messageSuccess) {
		this.messageSuccess = messageSuccess;
	}
	public void setMessageWarning(String messageWarning) {
		this.messageWarning = messageWarning;
	}
	public void setMessageError(String messageError) {
		this.messageError = messageError;
	}

	public String getMessage() {
		if (StringUtils.hasValue(messageError)) {
			return this.messageError;
		} else if (StringUtils.hasValue(messageWarning)) {
			return this.messageWarning;
		} else if (StringUtils.hasValue(messageSuccess)) {
			return this.messageSuccess;
		}
		return "executado com sucesso.";
	}

}


