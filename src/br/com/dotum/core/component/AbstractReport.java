package br.com.dotum.core.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.servlet.ServletHelper;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

public abstract class AbstractReport {
	private static Log LOG = LogFactory.getLog(AbstractReport.class);

	public static String TYPE_HTML = "HTML";
	public static String TYPE_PDF = "PDF";
	public static String TYPE_XLS = "XLS";
	public static String TYPE_DOCX = "DOCX";
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	private Map<String, Object> list;
	private UserContext userContext;
	private TransactionDB trans;
	private ResultHtml html;

	
	private String nameFile;
	private String subTitle;
	private String footer;
	private String type;
	private List<AbstractFieldHtml> fieldList = new ArrayList<AbstractFieldHtml>();
	private Map<String, Object> map = new HashMap<String, Object>();

	
	public abstract void doExecute() throws Exception;
	
	public void prepare(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, ResultHtml html, Map<String, Object> list) throws Exception {
		this.request = request;
		this.response = response;
		this.trans = trans;
		this.html = html;
		this.list = list;

		this.userContext = ServletHelper.getUsuarioSession(request);
		this.html.setUserContext(userContext);
	}
	
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}
	
	public String getNameFile() {
		return nameFile;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	
	public void setFooter(String footer) {
		this.footer = footer;
	}
	
	public void addField(AbstractFieldHtml field) {
		this.fieldList.add(field);
	}
	
	public String getGroup() {
		Menu m = (Menu)getClass().getAnnotation(Menu.class);
		return m.group();
	}
	
	public String getTitle() {
		Menu m = (Menu)getClass().getAnnotation(Menu.class);
		return m.label();
	}
	
	
	public List<AbstractFieldHtml> getField() {
		return fieldList;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		if (this.type == null) return TYPE_PDF;
		return type.toUpperCase();
	}
	
	public UserContext getUserContext() {
		return userContext;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public Map<String, Object> getList() {
		return list;
	}

	public void setList(HashMap<String, Object> list) {
		this.list = list;
	}

	public TransactionDB getTransaction() {
		return trans;
	}

	public void setTrans(TransactionDB trans) {
		this.trans = trans;
	}

	public ResultHtml getResultHtml() {
		return html;
	}

	public void setResultHtml(ResultHtml html) {
		this.html = html;
	}

	public String getPath() {
		if (getNameFile().contains("/") == true) {
			LOG.error("C é loko... tira o endereço do relatorio " + getNameFile() + ". Deixando apenas o nome do jasper");
			return "";
		} else {
			return getClass().getPackage().getName().replaceAll("\\.", "/") + "/";
		}
	}
	
	public void addParamField(String param, Object value) {
		map.put(param, value);
	}
	
	public void setParamField(int i, Object value) throws DeveloperException {
		List<AbstractFieldHtml> fieldList = getField();
		if (fieldList.size() == 0) 
			throw new DeveloperException("Para chamar o metodo \"setParamField\" é preciso invocar antes do execute.");
		
		map.put(fieldList.get(i).getName(), value);
	}
	
	public Map<String, Object> getMap() {
		return map;
	}
	
}

