package br.com.dotum.core.component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.ButtonHtml;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.GroupButtonHtml;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.other.HelpHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.core.html.other.TableHtml.SummaryEnum;
import br.com.dotum.core.html.other.WindowHtml;
import br.com.dotum.core.servlet.ServletHelper;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.StringUtils;

public abstract class AbstractView {

	public static final String VIEW_TABLE_ID = "masterTableScreenView";
	public static final String VIEW_TIME_PERFORMANCE = ".nav-search";

	private HttpServletRequest request;
	private HttpServletResponse response;
	private Map<String, Object> list;
	private UserContext userContext;
	private TransactionDB trans;
	private ResultHtml html;

	private TableHtml masterTable;
	private boolean dataClosed = false;
	private boolean basket = false;
	
	private HelpHtml help;
	private List<ButtonHtml> actionToolbarList = new ArrayList<ButtonHtml>();
	private List<ButtonHtml> actionTableList = new ArrayList<ButtonHtml>();
	private List<Bean> tabList = new ArrayList<Bean>();
	private List<Boolean> filterDestinationList = new ArrayList<Boolean>();
	private List<Condition> filterConditionList = new ArrayList<Condition>();
	private List<AbstractFieldHtml> filterFieldList = new ArrayList<AbstractFieldHtml>();


	public void prepare(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, ResultHtml html, Map<String, Object> list) throws Exception {
		this.request = request;
		this.response = response;
		this.trans = trans;
		this.html = html;
		this.list = list;

		this.userContext = ServletHelper.getUsuarioSession(request);
		this.html.setUserContext(userContext);

		this.masterTable = new TableHtml("doData");
		this.masterTable.setId(AbstractView.VIEW_TABLE_ID);
		this.masterTable.setCheckbok(true);
		
		this.help = new HelpHtml();

		this.masterTable.setDataClass(getClass());
		this.masterTable.setResultHtml(html);
		this.masterTable.setServerSide(true);
		this.help.setResultHtml(html);
	}

	public abstract void doExecute() throws Exception;
	public abstract void doFilter() throws Exception;
	public abstract Object[] doData() throws Exception;

	
	
	//
	// GET / SET ATRIBUTOS
	//
	public HttpServletRequest getRequest() {
		return request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public Map<String, Object> getList() {
		return list;
	}
	public UserContext getUserContext() {
		return userContext;
	}
	public TransactionDB getTransaction() {
		return this.trans;
	}
	public ResultHtml getResultHtml() {
		return html;
	}

	public void dataClosed(boolean dataClosed) {
		this.dataClosed = dataClosed;
	}
	public boolean isDataClosed() {
		return dataClosed;
	}
	public boolean isBasket() {
		Basket b = getClass().getAnnotation(Basket.class);
		return b != null;
	}
	public void basket(boolean basket) {
		this.addTab("Cesta", FWConstants.TAB_CESTA, true);
		this.basket = basket;
	}

	public void addTableColumn(String column, String attribute) {
		masterTable.addColumn(null, column, attribute, null, false, null, false);
	}
	public void addTableColumn(String column, String attribute, Integer decimal) {
		masterTable.addColumn(null, column, attribute, decimal, false, null, false);
	}
	public void addTableColumn(String column, String attribute, SummaryEnum summary) {
		masterTable.addColumn(null, column, attribute, null, false, summary, false);
	}
	public void addTableColumn(String column, String attribute, Integer decimal, SummaryEnum summary) {
		masterTable.addColumn(null, column, attribute, decimal, false, summary, false);
	}
	public void addTableColumn(String column, String attribute, Boolean hidden, SummaryEnum summary) {
		masterTable.addColumn(null, column, attribute, null, hidden, summary, false);
	}
	public void addTableColumn(String column, String attribute, Integer decimal, Boolean hidden, SummaryEnum summary) {
		masterTable.addColumn(null, column, attribute, decimal, hidden, summary, false);
	}
	public void addTableColumn(String column, String attribute, Boolean hidden) {
		masterTable.addColumn(null, column, attribute, null, hidden, null, false);
	}
	public void addTableColumn(String column, String attribute, Integer decimal, Boolean hidden) {
		masterTable.addColumn(null, column, attribute, decimal, hidden, null, false);
	}


	
	public void setObjectSession(String key, Object value) {
		ServletHelper.setSession(request, key, value);
	}
	public <T> T getObjectSession(String key) {
		return ServletHelper.getSession(request, key);
	}

	
	
	public TableHtml getTable() {
		return masterTable;
	}
	public void addActionToolbar(ButtonHtml button) {
		actionToolbarList.add(button);
	}
	public void addActionTable(ButtonHtml button) {
		button.setLabel(null);
		actionTableList.add(button);
	}

	public void doPreExecute() throws Exception {
		Menu viewAnn = (Menu) getClass().getAnnotation(Menu.class);
		String title = "";
		if(StringUtils.hasValue(html.getTitle()) == false){
			if(StringUtils.hasValue(viewAnn.group())) title += viewAnn.group()+": ";
			title += viewAnn.label();
			html.setTitle(title);
		}
		
		
		StringBuilder htmlStr = new StringBuilder();
		StringBuilder scriptStr = new StringBuilder();

		Map<String, GroupButtonHtml> map = new LinkedHashMap<String, GroupButtonHtml>();
		for (ButtonHtml btn : actionToolbarList) {
			if (StringUtils.hasValue(btn.getGroup()) == false) continue;
			
			GroupButtonHtml group = map.get(btn.getGroup());
			if (group == null) {
				group = new GroupButtonHtml();
				group.setLabel(btn.getGroup());
				map.put(btn.getGroup(), group);
			}
			
			group.addButton(btn);
		}
		
		List<String> groupAdded = new ArrayList<String>();
		for (ButtonHtml btn : actionToolbarList) {
			scriptStr.append(btn.getScript());

			if (StringUtils.hasValue(btn.getGroup())) {
				if (groupAdded.contains(btn.getGroup()) == true) continue;
				
				GroupButtonHtml group = map.get(btn.getGroup());
				htmlStr.append ( group.getComponente() );

				groupAdded.add(btn.getGroup());
			} else {
				
				htmlStr.append ( btn.getComponente() );
				
			}
			
		}
		htmlStr.append("<hr>");
		Html h = new Html(htmlStr.toString());
		h.setScript(scriptStr.toString());
		this.html.addComponent( h );
	}
	public void doPosExecute() throws Exception {
		for (ButtonHtml btn : actionTableList){
			masterTable.addButton(btn);
		}

		String filterProperty = getObjectSession(getNameSessionFilterAbaProperty());
		Object filterValue = getObjectSession(getNameSessionFilterAbaValue());

		boolean filtro = false;
		StringBuilder abaAndFilter = new StringBuilder();
		//
		// ABA FILTER
		//
		abaAndFilter.append(" <ul class=\"nav nav-tabs tab-color-blue\" id=\"screenViewTab\">");

//		String label = "Cesta";
//		label += " <span class=\"badge badge-danger "+ FWConstants.VIEW_TAB_QTD +"\">...</span>";
//		if (isBasket() == true) {
//			abaAndFilter.append("  <li>");
//			abaAndFilter.append("   <a class=\"basket\" data-showwhentab=\"basket\" data-toggle=\"tab\" aria-expanded=\"true\">");
//			abaAndFilter.append("    " + label);
//			abaAndFilter.append("   </a>");
//			abaAndFilter.append("  </li>");
//		}
		
		for (int i = 0; i < this.tabList.size(); i++) {
			String nameAba = "filterAba"+(i+1);

			Bean b = this.tabList.get(i);
			String labelStr = b.getAttribute("label");
			String label = b.getAttribute("label");
			String property = b.getAttribute("property");
			Object value = b.getAttribute("value");
			String valueStr = ""+value;

			String abaActive = "";
			label += " <span class=\"badge badge-danger "+ FWConstants.VIEW_TAB_QTD +"\">...</span>";
			if ((filterValue == null) && (basket == true) && (i == 1)) {
				abaActive = " class=\"active\"";
			} else if ((filterValue == null) && (basket == false) && (i == 0)) {
				abaActive = " class=\"active\"";
			} else if ((value != null) && (valueStr.equals(filterValue)) && (filterProperty.equals(property))) {
				abaActive = " class=\"active\"";
			}
			
			abaAndFilter.append("  <li"+abaActive+">");
			abaAndFilter.append("   <a class=\""+nameAba+"\" data-showwhentab=\""+labelStr+"\" data-toggle=\"tab\" aria-expanded=\"true\">");
			abaAndFilter.append("    " + label);
			abaAndFilter.append("   </a>");
			abaAndFilter.append("  </li>");

			// criar um script aqui para quando clicar na aba enviar um objeto para a sessao via ajax
			// esse objeto tem um nome filtro padrao para esse manager
			// assim que montar a tela tem que ver se tem objeto e colocar a aba ativa
			// assim que carregar os dados tem que ver o filtro e aplicar automaticamente sozinho sem ninguem


			StringBuilder script = new StringBuilder();
			script.append(ResultHtml.PL);
			if (ResultHtml.DEBUG == true) script.append("/*SCRIPT DE FILTRO DA ABA*/" + ResultHtml.PL);


			script.append("$('."+nameAba+"').click(function(){");
			Ajax ajax = new Ajax();
			ajax.addParam(FWConstants.PARAM_FW_CLASS, ClassUtils.getCodeTask(getClass()));
			ajax.addParam(FWConstants.PARAM_FW_METHOD, "doFilterAba");
			ajax.addParam(FWConstants.PARAM_FW_FILTER_ABA_PROPERTY, property);
			ajax.addParam(FWConstants.PARAM_FW_FILTER_ABA_VALUE, ""+(value!= null ? value : ""));
//			ajax.addSuccess(TableHtml.createScriptPopulateByJson(masterTable.getId(), "screenViewShowWhenTab("+ (i+1) +")"));
			ajax.addSuccess( masterTable.createScriptPopulateByJson() );
			
			
			
			script.append( ajax.getComponent() );

			script.append("});");
			script.append(ResultHtml.PL);

			// filtro da aba
			this.html.addScript(script.toString());
			filtro = true;
		}


		//
		// FILTER
		//
		if (this.filterFieldList.size() > 0) {
			String openWinFilterBtn = "openWinFilterBtn";
			String clearFilterBtn = "clearFilterBtn";

			WindowHtml win = new WindowHtml("Filtro avançado", CssIconEnum.FILTER);
			win.setHtml(getResultHtml());
			FormHtml form = new FormHtml("doDataFilterField");
			form.setHtml(getResultHtml());
			form.setFilter(true);
			form.setDataClass(this.getClass());


			int qtdFiltros = 0;
			// ver se tiver muitos campos de filtro.. colocar em duas colunas
			for (int i = 0; i < this.filterFieldList.size(); i++) {
				AbstractFieldHtml field = this.filterFieldList.get(i);
				Condition c = this.filterConditionList.get(i);
				Object obj = field.getValue();

				Object xx = (getObjectSession(getNameSessionFilterField(filterFieldList.get(i).getName())));
				if (xx != null) {
					field.setValue(xx);

					qtdFiltros++;
				} else if (obj != null) {
					setObjectSession(getNameSessionFilterField(field.getName()), obj);

					qtdFiltros++;
				}

				int column = (i%2)+1;

				form.addGroup(null, column, field);
			}
			form.setDataComponent(true, masterTable);
			form.addButton(getClass(), "doFilterField", "Filtrar");
			win.addContent(form);

			html.addComponent(win);

			StringBuilder script = new StringBuilder();
			script.append(ResultHtml.PL);
			if (ResultHtml.DEBUG == true) script.append("/*SCRIPT DE FILTRO AVANCADO*/" + ResultHtml.PL);
			script.append("$('."+ openWinFilterBtn +"').click(function(){");
			script.append( form.createScriptPopulateByJson());
			script.append( win.createScriptAbreWindow());
			script.append("});");
			script.append(ResultHtml.PL);

			// filtro avancado
			this.html.addScript(script.toString());
			filtro = true;

			String qtd = "";
			//			if (qtdFiltros > 0) qtd = " ("+ qtdFiltros +")";
			abaAndFilter.append("  <div class=\"btn-group pull-right\"><button title=\"Limpar filtro(s)\" class=\""+ clearFilterBtn + " pull-right btn btn-primary btn-xs\"><i class=\""+ CssIconEnum.ERASER +"\"></i></button>");
			abaAndFilter.append("  <button class=\""+ openWinFilterBtn + " pull-right btn btn-primary btn-xs\">Filtros "+ qtd +" </button></div>");


			Ajax ajax = new Ajax(getClass(), FWConstants.PARAM_FW_FILTER_CLEAN);
			ajax.setSuccess( masterTable.createScriptPopulateByJson());
			ajax.setMessage(true);

			script = new StringBuilder();
			script.append("$('."+ clearFilterBtn +"').click(function(){");
			script.append( ajax.getComponent() );
			script.append("});");
			script.append(ResultHtml.PL);
			this.html.addScript(script.toString());


			if ((isDataClosed() == true) && (true)) {
				this.html.addScript(win.createScriptAbreWindow());
			}
		}

		abaAndFilter.append(" </ul> ");

		this.html.addComponent( help );
		if (filtro) this.html.addComponent( new Html(abaAndFilter.toString()) );
		this.html.addComponent( masterTable );
	}

	public String getNameSessionFilterAbaProperty() {
		return getClass().getName()+"_"+FWConstants.PARAM_FW_FILTER_ABA_PROPERTY;
	}

	public <T> T getFilterByName(String name) {
		return getObjectSession(getNameSessionFilterField(name.toLowerCase()));
	}
	public String getNameSessionFilterField(String posFix) {
		return getNameSessionFilterField(getClass(), posFix);
	}
	public static String getNameSessionFilterField(Class classe, String posFix) {
		return classe.getName()+"_filter_"+posFix;
	}
	public String getNameSessionFilterAbaValue() {
		return getClass().getName()+"_"+FWConstants.PARAM_FW_FILTER_ABA_VALUE;
	}


	public void addComponente(ComponentHtml comp) {
		this.html.addComponent( comp );
	}
	public void addComponente(String comp) {
		this.html.addComponent( new Html(comp) );
	}


	public void addHelp(Help helpAnn) {
		if (helpAnn == null) return;

		String valor = helpAnn.value();
		if (valor.startsWith("http")) {


			String titulo = "Tutorial em vídeo";
			WindowHtml win = new WindowHtml(titulo, CssIconEnum.YOUTUBE);
			win.setResultHtml(getResultHtml());
			win.addContent(new Html("<div class=\"center\"><iframe width=\"853\" height=\"480\" src=\""+ valor +"\" frameborder=\"0\" allowfullscreen></iframe></div>"));
			win.setSize(8);

			StringBuilder sb = new StringBuilder();
			sb.append("<div class=\"btn btn-link \">");
			sb.append("<i class=\""+ CssIconEnum.YOUTUBE +" bigger-150 red\"></i>");
			sb.append("<a class=\"bolder\" href=\"javascript:"+ win.createScriptAbreWindow() +"\">"+ titulo +"</a>");
			sb.append("</div><br><br>");

			html.addComponent(win);
			help.addContent(sb.toString());
		} else {
			Menu m = this.getClass().getDeclaredAnnotation(Menu.class);
			help.addContent(null, null, CssColorEnum.GREEN, "Sobre", valor);
		}
	}
	public void addHelp(Menu actionAnn, Help helpAnn) {
		if (helpAnn == null) return;

		Integer selecteds = actionAnn.selecteds();
		String valor = helpAnn.value();
		CssIconEnum icon = actionAnn.icon();
		CssColorEnum color = actionAnn.color();
		String label = actionAnn.label();

		help.addContent(selecteds, icon, color, label, valor);
	}

	public void addTab(String label, String property, Object value) {
		Bean b = new Bean();
		b.setAttribute("label", label);
		b.setAttribute("property", property);
		b.setAttribute("value", value);
		this.tabList.add(b);
	}
	protected List<Bean> getAbas(){
		return this.tabList;
	}
	public Bean getAba1() {
		if ((basket == true) && (this.tabList.size() > 2)) {
			return this.tabList.get(1);
		} else if (this.tabList.size() > 1) {
			return this.tabList.get(0);
		}

		return null;
	}



	public void addFilterToSelect(Condition condition, AbstractFieldHtml field) {
		if ((getObjectSession(getNameSessionFilterField(field.getName())) == null) && (field.getValue() != null)) {
			setObjectSession(getNameSessionFilterField(field.getName()), field.getValue());
		}

		addFilterField(false, condition, field);
	}
	public void addFilterField(Condition condition, AbstractFieldHtml field) throws DeveloperException {
		if (field.getName().equalsIgnoreCase("id"))
			throw new DeveloperException("Não é possivel criar um filtro com o nome de ID, por favor duplique o ID e de outro nome para ele la no ORM.");

		for (AbstractFieldHtml fieldTemp : filterFieldList) { 
			if (fieldTemp.getName().equals(field.getName()) == false) continue;
			throw new DeveloperException("Não é possivel criar dois filtros com o mesmo nome, por favor duplique o "+ field.getName() +" e coloque \""+ field.getName() +"Inicial\" e \""+ field.getName() +"Final\" tando no field quanto no ORM.");
		}

		addFilterField(true, condition, field);
	}
	private void addFilterField(boolean fw, Condition condition, AbstractFieldHtml field) {
		this.filterDestinationList.add(fw);
		this.filterConditionList.add(condition);
		this.filterFieldList.add(field);
	}



	public List<Boolean> getFilterDestination() {
		return filterDestinationList;
	}
	public List<Condition> getFilterCondition() {
		return filterConditionList;
	}
	public List<AbstractFieldHtml> getFilterField() {
		return filterFieldList;
	}

}

