package br.com.dotum.core.component;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.component.xml.smartmanager.model.DotumSmartManager;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.core.db.AbstractDD;
import br.com.dotum.jedi.core.db.OracleDB;
import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.OrderDB.Orientation;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.db.bean.ComponenteBean;
import br.com.dotum.jedi.core.db.bean.ComponenteEstatisticaBean;
import br.com.dotum.jedi.core.db.bean.ComponenteTipoBean;
import br.com.dotum.jedi.core.db.bean.TarefaBean;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.gen.GeneratorModel;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.FileUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.StringUtils;

public class ComponentHelper {
	private static Log LOG = LogFactory.getLog(ComponentHelper.class);

	public static final int CPTT_VIEW = 1;
	public static final int CPTT_ACTION = 2;
	public static final int CPTT_CONTROLLER = 3;
	public static final int CPTT_JASPERREPORT = 4;
	public static final int CPTT_JOB = 5;
	public static final int CPTT_DOTUMREPORT = 6;
	private static final String SEPARATOR = " > ";
	private static Map<String, Class<?>> classeFullMap = new LinkedHashMap<String, Class<?>>();
	private static Map<String, Class<? extends AbstractView>> viewMap = new LinkedHashMap<String, Class<? extends AbstractView>>();
	private static Map<String, Class<? extends AbstractAction>> actionMap = new LinkedHashMap<String, Class<? extends AbstractAction>>();
	private static Map<String, Class<? extends AbstractJob>> jobMap = new LinkedHashMap<String, Class<? extends AbstractJob>>();
	private static Map<String, Class<? extends AbstractController>> controllerMap = new LinkedHashMap<String, Class<? extends AbstractController>>();
	private static Map<String, Class<? extends AbstractReport>> jasperReportMap = new LinkedHashMap<String, Class<? extends AbstractReport>>();
	private static Map<String, DotumSmartReport> smartReportMap = new LinkedHashMap<String, DotumSmartReport>();



	public static void mergeComponenteTipo(TransactionDB trans) throws SQLException, Exception {
		trans.merge( new ComponenteTipoBean(CPTT_VIEW, AbstractView.class.getSimpleName()) );
		trans.merge( new ComponenteTipoBean(CPTT_ACTION, AbstractAction.class.getSimpleName()) );
		trans.merge( new ComponenteTipoBean(CPTT_CONTROLLER, AbstractController.class.getSimpleName()) );
		trans.merge( new ComponenteTipoBean(CPTT_JASPERREPORT, AbstractReport.class.getSimpleName()) );
		trans.merge( new ComponenteTipoBean(CPTT_JOB, AbstractJob.class.getSimpleName()) );
		trans.merge( new ComponenteTipoBean(CPTT_DOTUMREPORT, DotumSmartReport.class.getSimpleName()) );
	}
	public static void mergeComponente(TransactionDB trans, Map<String, Class<?>> classMap, Map<String, DotumSmartReport> xmlMap) throws SQLException, Exception {
		mergeComponenteTipo(trans);
		List<ComponenteBean> list = new ArrayList<ComponenteBean>();
		list.addAll( prepareComponent(trans, classMap) );
		list.addAll( prepareComponentReportXml(trans, xmlMap) );
		//		deleteComponente(trans);


		List<TarefaBean> tarList = new ArrayList<TarefaBean>();
		for (ComponenteBean cptBean : list) {
			try {
				trans.merge(cptBean);
			} catch (Exception e) {
				ComponenteBean bean = null;
				try {
					bean = trans.selectById(ComponenteBean.class, cptBean.getId());
					throw new DeveloperException("Problema para inserir o componente \"" + cptBean.getDescricao() + "\" com o codigo \"" + cptBean.getCodigo() + "\" pois ja tem um com o mesmo ID no banco de dados. " + bean.getDescricao(), e);
				} catch (NotFoundException nof) {
					throw new DeveloperException("Problema para inserir o componente. Comp: " + cptBean, e);
				}
			}

			if ((Boolean)cptBean.get("task") && (Boolean)cptBean.get("public") == false) {
				TarefaBean tarBean = new TarefaBean();
				tarBean.setId(cptBean.getId());
				tarBean.setDescricao(cptBean.getDescricao());
				tarBean.setCodigo(cptBean.getCodigo());
				trans.merge(tarBean);

				tarList.add(tarBean);
			}
		}

		// apagar as task em desuso
		// apagar o vinculo das task em desuso
		// tem que ter no banco de dados apenas as que inseriu aqui a cima e as que estao no DD
		List<TarefaBean> tarDDList = trans.select(TarefaBean.class);

		List<TarefaBean> list2 = AbstractDD.getList(TarefaBean.class);
		if (list2 != null) {
			tarList.addAll(list2);
		}
		tarDDList.removeAll(tarList);


		for (TarefaBean tarBean : tarDDList) {
			WhereDB where = new WhereDB();
			where.add("Tarefa.Id", Condition.EQUALS, tarBean.getId());

			//			trans.delete(PessoaTarefaBean.class, where);
			//			trans.delete(tarBean);
		}
	}

	public static void mergeTask(TransactionDB trans) {
		
	}
	
	private static List<ComponenteBean> prepareComponentReportXml(TransactionDB trans, Map<String, DotumSmartReport> xmlMap) throws Exception {
		List<ComponenteBean> list = new ArrayList<ComponenteBean>();

		for (String codigo : xmlMap.keySet()) {
			DotumSmartReport report = xmlMap.get(codigo);

			String descricao = (StringUtils.hasValue(report.getMenu().getGroup()) ? report.getMenu().getGroup() + SEPARATOR : "Relatório" + SEPARATOR) + report.getTitle();
			Integer cpttId = CPTT_DOTUMREPORT;

			ComponenteBean b = new ComponenteBean();
			b.setId(getIdByCodigo(codigo));
			b.getComponenteTipo().setId(cpttId);
			b.setCodigo(codigo);
			b.setDescricao(descricao);
			b.setXml(report.getXml());
			b.setMenu(report.getMenu().getPosition());
			b.set("task", true);
			b.set("public", false);
			list.add(b);
		}

		return list;
	}
	private static List<ComponenteBean> prepareComponent(TransactionDB trans, Map<String, Class<?>> classMap) throws SQLException, Exception {
		List<ComponenteBean> list = new ArrayList<ComponenteBean>();
		for (String key : classMap.keySet()) {
			Class<?> clazz = classMap.get(key);

			Menu m1 = clazz.getAnnotation(Menu.class);
			Task t1 = clazz.getAnnotation(Task.class);
			if (m1 == null) continue;
			String part = "";

			String descricao = (StringUtils.hasValue(m1.group()) ? m1.group() + SEPARATOR : "") + m1.label();
			String codigo = ClassUtils.getCodeTask(clazz);
			Integer cpttId = null;
			if (clazz.getSuperclass().equals(AbstractView.class)) {
				cpttId = CPTT_VIEW;
			} else if (clazz.getSuperclass().equals(AbstractAction.class)) {
				cpttId = CPTT_ACTION;


				String codigoPai = codigo.substring(0 ,  codigo.lastIndexOf("."));
				Class<?> x = classMap.get(codigoPai);

				if (x != null) {
					Menu m2 = x.getAnnotation(Menu.class);
					if (m2 != null) {
						part = "" + (StringUtils.hasValue(m2.group()) ? m2.group() + SEPARATOR : "")  
								+ (StringUtils.hasValue(m2.label()) ? m2.label() + SEPARATOR : "");
					}

				}

			} else if (clazz.getSuperclass().equals(AbstractController.class)) {
				cpttId = CPTT_CONTROLLER;
			} else if (clazz.getSuperclass().equals(AbstractReport.class)) {
				cpttId = CPTT_JASPERREPORT;
			} else if (clazz.getSuperclass().equals(AbstractJob.class)) {
				cpttId = CPTT_JOB;
			}


			Boolean classeFw = (clazz.getPackage().getName().equals(FWConstants.PACK_CLASSE_DEFAULT));
			Boolean temTask = (t1 != null);
			Boolean criarTask = true;
			if (classeFw || temTask)
				criarTask = false;

			ComponenteBean b = new ComponenteBean();
			b.setId(getIdByClass(clazz));
			b.getComponenteTipo().setId(cpttId);
			b.setCodigo(codigo);
			b.setDescricao(part + descricao);
			b.setMenu(m1.inMenu());
			b.set("task", criarTask);
			b.set("public", m1.publico());
			list.add(b);
		}

		return list;
	}

	/**
	 * Este metodo recebe o caminho absoluto de um DotumSmartReport para pegar e montar o codigo
	 * 
	 * @param codigo
	 * @return
	 */
	public static String getCodeByPath(String path) {

		String nameFile = path.substring(path.lastIndexOf("/")+1);
		String part = path.split(FWConstants.TOMCAT_PATH_CLASSES)[1];
		String pack = StringUtils.getNumberOnly(part.substring(0, (part.length() - nameFile.length())));
		String codigo = StringUtils.getNumberOnly(nameFile);

		String fullCode = "1." + pack + "." + codigo;


		return fullCode; 
	}
	public static String getCodeByClass(Class<?> clazz) {
		String pai = StringUtils.getNumberOnly(clazz.getPackage().getName());
		String filho = StringUtils.getNumberOnly(clazz.getSimpleName());
		String codigo = pai + (StringUtils.hasValue(filho) ? "." + filho : "");

		return codigo;
	}
	public static Integer getIdByCodigo(String codigo) {
		return NumberUtils.parseInt(StringUtils.getNumberOnly(codigo));
	}
	public static Integer getIdByClass(Class<?> clazz) {
		String pai = StringUtils.getNumberOnly(clazz.getPackage().getName());
		String filho = StringUtils.getNumberOnly(clazz.getSimpleName());
		Integer id = NumberUtils.parseInt("1" + pai +""+ filho);

		return id;
	}

	public static String getDescriptionByClass(Class<?> clazz) {
		Menu m1 = clazz.getAnnotation(Menu.class);
		String descricao = m1.label();

		String view = "";
		if (clazz.getSuperclass().equals(AbstractAction.class)) {
			try {
				String classStr = clazz.getPackage().getName() + "." + FWConstants.CLASS_VIEW;
				Class<?> fwView = Class.forName(classStr);
				Menu m2 = fwView.getAnnotation(Menu.class);
				if (m2 != null) 
					view = m2.label() + ": ";

			} catch (ClassNotFoundException e) {
			}
		}

		return view + descricao;
	}

	/**
	 * Colocando as classes em cache para usar apenas uma vez em toda o sistema
	 * @param preFixPackage
	 * @param refresh
	 */
	public static void doCacheClass(TransactionDB trans, ServletContext sc, String preFixPackage, boolean refresh) {
		LOG.info("Carregando as classes do pacote " + preFixPackage + "...");

		try {
			if (refresh == true) {
				classeFullMap = new LinkedHashMap<String, Class<?>>();
				viewMap = new LinkedHashMap<String, Class<? extends AbstractView>>();
				actionMap = new LinkedHashMap<String, Class<? extends AbstractAction>>();
				jobMap = new LinkedHashMap<String, Class<? extends AbstractJob>>();
				controllerMap = new LinkedHashMap<String, Class<? extends AbstractController>>();
				jasperReportMap = new LinkedHashMap<String, Class<? extends AbstractReport>>();
				smartReportMap = new LinkedHashMap<String, DotumSmartReport>();
			}

			if (classeFullMap.size() == 0) {

				List<Class<?>> classList = ClassUtils.getClasses( preFixPackage );
				List<Class<?>> coreClassList = ClassUtils.getClasses( FWConstants.PACK_CLASSE_DEFAULT );
				classList.addAll( coreClassList );
				classList = ClassUtils.orderAlphaNumeric(classList);

				for (Class<?> clazz : classList) {
					Class<?> superClass = clazz.getSuperclass();
					if ((superClass == null) || (superClass.equals(Object.class))) continue;

					String key = ClassUtils.getCodeTask(clazz);
					Menu m1 = clazz.getAnnotation(Menu.class);

					if (	(m1 != null) 
							&&	((superClass.equals(AbstractView.class))
									|| (superClass.equals(AbstractAction.class))
									|| (superClass.equals(AbstractController.class))
									|| (superClass.equals(AbstractReport.class))
									//									|| (superClass.equals(AbstractJob.class))
									)) {

						Class<?> old = classeFullMap.get(key);
						if (old != null) 
							LOG.error("A classe \"" + old.getName() + "\" esta com o mesmo codigo da classe \"" + clazz.getName() + "\".");

						classeFullMap.put(key, clazz);
					}
					if ((m1 != null) && (superClass.equals(AbstractView.class))) 		viewMap.put(key, (Class<? extends AbstractView>)clazz);
					if ((m1 != null) && (superClass.equals(AbstractAction.class))) 		actionMap.put(key, (Class<? extends AbstractAction>)clazz);
					if ((m1 != null) && (superClass.equals(AbstractController.class)))	controllerMap.put(key, (Class<? extends AbstractController>)clazz);
					if ((m1 != null) && (superClass.equals(AbstractReport.class))) 		jasperReportMap.put(key, (Class<? extends AbstractReport>)clazz);
					if (superClass.equals(AbstractJob.class)) 							jobMap.put(key, (Class<? extends AbstractJob>)clazz);
				}

				sc.setAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_FULL_MAP, classeFullMap);
				sc.setAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_VIEW_MAP, viewMap);
				sc.setAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_ACTION_MAP, actionMap);
				sc.setAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_JOB_MAP, jobMap);
				sc.setAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_CONTROLLER_MAP, controllerMap);
				sc.setAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_JASPERREPORT_MAP, jasperReportMap);
			}

			String path = sc.getRealPath("/") + FWConstants.TOMCAT_PATH_CLASSES + preFixPackage + "/";
			List<File> xxx = FileUtils.listPath(null, new File(path), "dsrxml");
			for (File f : xxx) {
				FileMemory fm = new FileMemory(f);
				String fullCode = ComponentHelper.getCodeByPath(fm.getParent() + fm.getName() );

				String relatorioStr = fm.getText();
				DotumSmartReport report = DotumSmartReportBuilder.prepare( relatorioStr );
				report.setType("HTML");

				smartReportMap.put(fullCode, report);
			}
			sc.setAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_SMARTREPORT_MAP, smartReportMap);


		} catch (Exception e) {
			LOG.error("Erro ao carregar as classes e colocar em cache", e);
		}
	}
	public static void doCacheCustom(TransactionDB trans, ServletContext sc) throws Exception {
		// carregar os relatorios (XML)
		List<ComponenteBean> cptList = null;
		try {
			WhereDB where = new WhereDB();
			where.add("ComponenteTipo.Id", Condition.EQUALS, CPTT_DOTUMREPORT);
			where.add("XmlCustomizado", Condition.ISNOTNULL, null);
			
			cptList = trans.select(ComponenteBean.class, where);
		} catch (NotFoundException e) {
			cptList = new ArrayList<ComponenteBean>();
		}

		for (ComponenteBean temp : cptList) {
			if (StringUtils.hasValue(temp.getXmlCustomizado()) == false) continue;

			doCacheCustom(trans, sc, temp.getCodigo(), temp.getXmlCustomizado());
		}
		sc.setAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_SMARTREPORT_MAP, smartReportMap);
	}
	
	public static void doCacheCustom(TransactionDB trans, ServletContext sc, String codigo, String xml) throws Exception {
		DotumSmartReport report = DotumSmartReportBuilder.prepare( xml );
		report.setType("HTML");
		smartReportMap.put(codigo, report);
	}
	


	public static String createTableComponente(TransactionDB trans) throws SQLException, Exception {

		if (hasTableComponente(trans) == false) {
			StringBuilder sb = new StringBuilder();

			List<Class<?>> list = new ArrayList<Class<?>>();
			list.add(ComponenteTipoBean.class);
			list.add(ComponenteBean.class);
			list.add(ComponenteEstatisticaBean.class);


			Map<String, List<String>> map = GeneratorModel.genScriptCreateTable(trans, null, null, list);
			for (String key : map.keySet()) {
				List<String> com = map.get(key);
				for (String comando : com) {
					sb.append( OracleDB.execute(trans, key, comando) );
				}
			}


			String result = sb.toString();
			while (result.contains("  ")) result = result.replaceAll("  ", " ");
			return result;		
		} 
		return "Já existe as tabelas de componente";
	}
	private static boolean hasTableComponente(TransactionDB trans) {
		try {
			trans.selectById(ComponenteTipoBean.class, 1);
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	public static void insertReport(TransactionDB trans, ServletContext sc, String xml) throws DeveloperException, Exception {
		String codigo = "1.900.001";
		try {
			OrderDB order = new OrderDB();
			order.add("Id", Orientation.DESC);
			WhereDB where = new WhereDB();
			where.add("ComponenteTipo.Id", Condition.EQUALS, ComponentHelper.CPTT_DOTUMREPORT);
			where.add("Codigo", Condition.LIKEBEGIN, "1.900");

			List<ComponenteBean> cptList = trans.select(ComponenteBean.class, where, order);
			ComponenteBean cptBean = cptList.get(0);
			String[] parts = cptBean.getCodigo().split("\\.");
			codigo = "1.900." + StringUtils.fillLeft((Integer.parseInt(parts[2]) + 1), '0', 3);
		} catch (NotFoundException e) {

		}

		
		DotumSmartReport report = DotumSmartReportBuilder.prepare(xml);


		ComponenteBean bean = new ComponenteBean();
		bean.setId(ComponentHelper.getIdByCodigo(codigo));
		bean.setCodigo(codigo);
		bean.setDescricao(report.getTitle());
		bean.getComponenteTipo().setId(ComponentHelper.CPTT_DOTUMREPORT);
		bean.setMenu(Menu.LEFT);
		bean.setXmlCustomizado(xml);
		trans.insert(bean);
		
		TarefaBean tarBean = new TarefaBean();
		tarBean.setId(bean.getId());
		tarBean.setDescricao(bean.getDescricao());
		tarBean.setCodigo(bean.getCodigo());
		trans.insert(tarBean);


		//
		// MELHORAR ISSO AQUI
		//
		ComponentHelper.doCacheCustom(trans, sc);		
	}
	public static Map<String, DotumSmartReport> getCacheDotumSmartReport(HttpServletRequest request) {
		Map<String, DotumSmartReport> result = (Map<String, DotumSmartReport>)request.getServletContext().getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_SMARTREPORT_MAP);
		if (result == null) result = new LinkedHashMap<String, DotumSmartReport>();
		
		return result;
	}
	public static Map<String, DotumSmartManager> getCacheDotumSmartManager(HttpServletRequest request) {
		Map<String, DotumSmartManager> result = (Map<String, DotumSmartManager>)request.getServletContext().getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_SMARTMANAGER_MAP);
		if (result == null) result = new LinkedHashMap<String, DotumSmartManager>();
		
		return result;
	}
}

