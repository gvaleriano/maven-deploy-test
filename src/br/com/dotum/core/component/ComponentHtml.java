package br.com.dotum.core.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.servlet.ApplicationContext;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.NumberUtils;

public abstract class ComponentHtml extends Css {
	private static Log LOG = LogFactory.getLog(ComponentHtml.class);

	
	private ApplicationContext applicationContext;
	private ComponentHtml parent;
	private UserContext userContext;
	private String prefix = "";
	private static Integer _id = 0;
	private ResultHtml html;
	private String id;
	private String name;
	private String label;
	private Integer size;
	private CssIconEnum icon;
	private CssColorEnum color;
	private Map<String, Object> attrMap = new HashMap<String, Object>();
	boolean iconVisible = true; 

	public abstract String getComponente() throws Exception;
	public abstract String getScript() throws DeveloperException;
	public abstract void createScript() throws DeveloperException;

	public void setResultHtml(ResultHtml html) {
		this.html = html;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		if (id == null) {
			//			if ((id == null) && (html == null)) {
			id = this.getClass().getSimpleName().substring(0,1) + NumberUtils.random(6);

			if (ResultHtml.DEBUG == true)
				LOG.warn(id + ". Comp: " + this.getClass().getSimpleName() + " - " + getLabel());
		}
		//		if (id == null) id = "c" + html.nextId();
		return id;
	}
	public String getIdFull() {
		return getPrefix() + getId();
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name.toLowerCase();

		// isso aqui nao pode quanto tem o inserir e alterar no mesmo screenview
		//		if (this instanceof AbstractFieldHtml) {
		//			setId("c" + this.name.replaceAll("\\.", ""));
		//		}

	}
	
	public boolean isIcon() {
		return this.iconVisible;
	}
	/**
	 * Caso informado false, não irá aparecer o icone padrão do campo;
	 * @param value
	 */
	public void setIcon(boolean value) {
		this.iconVisible = value;
	}
	
	public CssIconEnum getIcon() {
		return icon;
	}
	public void setIcon(CssIconEnum icon) {
		this.icon = icon;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public CssColorEnum getColor() {
		return color;
	}
	public void setColor(CssColorEnum color) {
		this.color = color;
	}

	public ResultHtml getHtml() {
		return html;
	}

	public void setHtml(ResultHtml html) {
		if (html == null) return;

		this.html = html;
	}
	protected static void prepare(ResultHtml html, List<? extends ComponentHtml> comps) throws DeveloperException {
		for (ComponentHtml c : comps) {
			c.setHtml(html);
			c.createScript();
		}
	}
	protected static void prepare2(ResultHtml html, List<ComponentHtml> comps) throws DeveloperException {
		for (ComponentHtml c : comps) {
			c.setHtml(html);
			c.createScript();
		}
	}

	public void removeAttr(String attr) {
		attrMap.remove(attr);
	}
	public void addAttr(String attr, Object value) {
		if (value == null) return;

		String temp = value.toString();
		if (temp.length() == 0) return;

		attrMap.put(attr, value);
	}

	public Object getAttr(String attr) {
		return this.attrMap.get(attr);
	}

	public Map<String, Object> getAttr() {
		return this.attrMap;
	}
	public boolean hasAttr(String attr) {
		return this.attrMap.containsKey(attr);
	}
	public String getAttrStr() {
		StringBuilder sb = new StringBuilder();

		for (String key : attrMap.keySet()) {
			Object obj = attrMap.get(key);
			if (obj == null) continue;

			sb.append(" ");
			sb.append(key);
			sb.append("=");
			sb.append("\"");
			sb.append( obj );
			sb.append("\"");
		}

		return sb.toString();
	}

	public String space(int e) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < e; i++) {
			sb.append(" ");
		}
		return "";
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public void setSize(Integer size) {
		this.size = size;

	}
	public Integer getSizeInt() {
		return this.size;
	}
	public String getSize() {
		return (size == null ? "" : " col-sm-" + size);
	}

	public String createScriptHide() {
		return "$('#"+getId()+"').hide();";
	}
	public String createScriptShow() {
		return "$('#"+getId()+"').show();";
	}
	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;		
	}
	public UserContext getUserContext() {
		return this.userContext;
	}
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	public ComponentHtml getParent() {
		return parent;
	}
	public void setParent(ComponentHtml parent) {
		this.parent = parent;
	}
}

