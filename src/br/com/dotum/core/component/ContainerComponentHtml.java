package br.com.dotum.core.component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class ContainerComponentHtml extends ComponentHtml {

	private Map<Object, List<ComponentHtml>> compMap = new LinkedHashMap<Object, List<ComponentHtml>>();
	private Map<Object, Boolean> reloadMap = new LinkedHashMap<Object, Boolean>();

	public Map<Object, List<ComponentHtml>> getMap() {
		return compMap;
	}
	
	public Map<Object, Boolean> getReloadMap() {
		return reloadMap;
	}
	
	public void add(Object key, ComponentHtml comp) {
		add(key, comp, false);
	}
	
	public void add(Object key, ComponentHtml ... compArray) {
		for (ComponentHtml comp : compArray) {
			add(key, comp, false);
		}
	}
	
	/**
	 * Ao passar o parametro true, voce habilita o relad da aba, isso é muito util pra todas as coisa 
	 * @param key
	 * @param comp
	 * @param reload
	 */
	public void add(Object key, ComponentHtml comp, boolean reload) {
		comp.setHtml(getHtml());

		if (compMap.containsKey(key) == false) {
			List<ComponentHtml> cList = new ArrayList<ComponentHtml>();
			cList.add(comp);
			compMap.put(key, cList);
			reloadMap.put(key, reload);
		} else {
			List<ComponentHtml> cList = compMap.get(key);
			cList.add(comp);
		}
	}

	
	public List<ComponentHtml> getComponentList() {
		List<ComponentHtml> result = new ArrayList<ComponentHtml>();
		for (Object aba : this.compMap.keySet()) {
			result.addAll( this.compMap.get(aba) );
		}

		return result;
	}

	public ComponentHtml[] getComponentArray() {
		List<ComponentHtml> result = new ArrayList<ComponentHtml>();
		for (Object aba : this.compMap.keySet()) {
			result.addAll( this.compMap.get(aba) );
		}

		return result.toArray(new ComponentHtml[result.size()]);
	}
	public int size() {
		return compMap.keySet().size();
	}
	
}

