package br.com.dotum.core.component;


import java.util.ArrayList;
import java.util.List;

import br.com.dotum.jedi.util.StringUtils;

public class CssClassHtml {

    private List<String> cssClass = new ArrayList<String>();
    
    protected CssClassHtml() {
    }
    
    public void addCssClass(String cssClass) {
    	if (StringUtils.hasValue(cssClass) == false) return;
    	
    	String[] partArray = cssClass.split(" ");
    	for (String part : partArray) {
    		if (hasCssClass(part) == true) continue; 

    		this.cssClass.add(part);
    	}
    }
    
    public boolean removeCssClass(String cssClass) {
        return this.cssClass.remove(cssClass);
    }
    
    public boolean hasCssClass(String cssClass) {
        for (String str : this.cssClass) {
            if (str.equalsIgnoreCase(cssClass)) {
                return true;
            }
        }
        
        return false;        
    }
    
    public int cssClassSize() {
        return cssClass.size();
    }
    
    public String getCssClassComponent() {
        StringBuilder sb = new StringBuilder();

//        if (cssClassSize() > 0) sb.append( " class=\"" );
        for (String str : cssClass) {
            sb.append( str );
            sb.append( " " );
        }
        if (cssClassSize() > 0) sb.delete(sb.length()-1, sb.length());
//        if (cssClassSize() > 0) sb.append( "\" " );
        return sb.toString(); 
    }
}
