package br.com.dotum.core.component;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.core.html.other.WindowHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public abstract class DataComponentHtml extends ComponentHtml {

	private Class<?> dataClasse;
	private String dataMethod;
	private String editMethod;
	private Integer dataId;	
	
	public abstract String createScriptPopulateByJson() throws DeveloperException;
	
	public static String createScriptPopulateByJsonOpenWindowIfAllOk(TableHtml table, List<ComponentHtml> compList, WindowHtml win, String origem) throws DeveloperException {
		return createScriptPopulateByJsonOpenWindowIfAllOk(table, compList.toArray(new ComponentHtml[compList.size()]), win, origem);
	}
	public static String createScriptPopulateByJsonOpenWindowIfAllOk(TableHtml table, ComponentHtml[] compArray, WindowHtml win, String origem) throws DeveloperException {
		StringBuilder script = new StringBuilder();

		
		String scriptReloadTableMain = null;
		if ((table != null) && (win.isWindow() == false)) {
//		if (table != null) {
			// estou trocando o momento do evento que será invocado..
			// nao quando interage em um form e sim quando fecha a window
			// com isso o fw irá reduzir o numero de requisições desnecessarias
			scriptReloadTableMain = table.createScriptPopulateByJson();
		}

		if (ResultHtml.DEBUG == true) script.append(ResultHtml.PL + ResultHtml.PL + "/*SCRIPT DO BUTTON "+ origem +"*/" + ResultHtml.PL);

		Object[] parts = null;
		List<String> varList = new ArrayList<String>();
		for (ComponentHtml comp : compArray) {


			parts = DataComponentHtml.populateByJson(comp, scriptReloadTableMain);
			script.append( parts[0] );
			varList.addAll( (List<String>) parts[1] );

		}

		// Agora iremos montar uma estrutura para ver se vamos abrir a windows
		// primeira questao é que a janela precisa abrir se todos os DataComponent forem sucesso em sua requisição
		// neste ponto nenhum metodo doData pode mandar um WarningException ou super.setMessageWarning("");
		// caso isto ocorra deve ser apresentado a mensagem e não abrir a window
		// caso contrario abra a window
		if (win != null) {

			script.append("if(true");
			String temp = "";
			for (String string : varList) {
				script.append(" && (" + string + "."+ FWConstants.JSON_ATTR_TYPE +" == 'success')" + ResultHtml.PL);
			}
			script.append("){");
			script.append( win.createScriptAbreWindow() );
			script.append("}");
		}


		return script.toString();
	}
	
	public static Object[] populateByJson(ComponentHtml c1, String scriptFormAdicional) throws DeveloperException {
		StringBuilder scriptSb = new StringBuilder();
		List<String> varList = new ArrayList<String>(); 

		if (c1 instanceof DataComponentHtml) {
			if (c1 instanceof FormHtml){
				FormHtml form = (FormHtml)c1;
				form.setScriptSuccess( scriptFormAdicional );
			}

			scriptSb.append(((DataComponentHtml)c1).createScriptPopulateByJson() );
			
			// so vou verificar de ViewHtml e FormHtml por que tenho o controle via ajax
			// a TableHtml é um componente de terceiro e nao tenho essa validação.
			// se um dia alguem precisar para table... ferrou!!!
			// temos q mudar o componente por inteiro e desenvolver um ou achar a resposta do mesmo.
			// para apresentar a mensagem igual o view e o form
			if (c1 instanceof TableHtml == false)
				varList.add( c1.getId() );
			
			
		} else if (c1 instanceof ContainerComponentHtml) {
			
			ContainerComponentHtml c2 = (ContainerComponentHtml)c1;
			List<ComponentHtml> c2List = c2.getComponentList();
			for (ComponentHtml c3: c2List) {
				
				Object[] parts = populateByJson(c3, scriptFormAdicional);
				scriptSb.append( parts[0] );
				varList.addAll( (ArrayList<String>)parts[1] );
			}
		}
		
		return new Object[]{scriptSb.toString(), varList};
	}

	public Class<?> getDataClass() {
		return dataClasse;
	}

	public void setDataClass(Class<?> dataClass) {
		this.dataClasse = dataClass;
	}

	public String getDataMethod() {
		return dataMethod;
	}

	public void setDataMethod(String dataMethod) {
		this.dataMethod = dataMethod;
	}
	public String getEditMethod() {
		return editMethod;
	}
	public void setEditMethod(String editMethod) {
		this.editMethod = editMethod;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	public Integer getDataId() {
		return dataId;
	}

}

