package br.com.dotum.core.component;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.ORM;
import br.com.dotum.jedi.core.db.OracleDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.gen.TableC;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
public class GeneratorScreen {

	public static final String PL = "\n";
	public static final String TAB = "\t";

	public static void run(String path, TransactionDB trans, Class<? extends Bean> xxx) throws Exception {
		String packageBase = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
		String packagePath = packageBase + "._screen." + xxx.getSimpleName().toLowerCase().substring(0, xxx.getSimpleName().length()-4);

		String screenView = createScreen(trans, xxx, packagePath);
		String detalhar = createDetalhar(trans, xxx, packagePath);
		String inserir = createInsert(trans, xxx, packagePath);
		String alterar = createAlterar(trans, xxx, packagePath);
		String inativar = createInativar(trans, xxx, packagePath);
		String cancelar = createCancelar(trans, xxx, packagePath);

		boolean fieldAtivo = false;
		boolean fieldSituacao = false;
		for (Field f : xxx.getDeclaredFields()) {
			if (f.getName().equals("ativo") == true) fieldAtivo = true;
			if (f.getName().equals("situacao") == true) fieldSituacao = true;
		}

		TableDB table = xxx.getDeclaredAnnotation(TableDB.class);
		String sigla = StringUtils.toUpperCaseFirstLetter(table.acronym()); 


		//
		// criar o arquivo agora com as 3 classes
		// screen / inserir / alterar
		//
		criarArquivo(screenView, path, packagePath, "ScreenView.java");
//		criarArquivo(detalhar, path, packagePath, sigla + "001Detalhar.java");
		criarArquivo(inserir, path, packagePath,  sigla + "010Inserir.java");
		criarArquivo(alterar, path, packagePath, sigla + "015Alterar.java");
		if (fieldAtivo == true) criarArquivo(inativar, path, packagePath, sigla + "020Inativar.java");
		if (fieldSituacao == true) criarArquivo(cancelar, path, packagePath, sigla + "022Cancelar.java");
		System.out.println("Ok");
	}

	private static String createScreen(TransactionDB trans, Class<? extends Bean> xxx, String packagePath) throws NotFoundException, Exception {
		String sql = OracleDB.getSql(null, trans.getConnection(), xxx, null, null, null, null, false, false);
		String[] lines = sql.split("\n");

		TableDB t = xxx.getAnnotation(TableDB.class);
		if (t == null) return null;

		StringBuilder screenViewSb = new StringBuilder();

		// DO EXECUTE
		screenViewSb.append(createPackageImportDefault(packagePath, xxx));
		screenViewSb.append("@Menu(group=\"Cadastro\", label=\""+ t.label() +"\", inMenu=Menu.LEFT)" + PL);
		screenViewSb.append("@Task({Task.ADMIN})" + PL);
		screenViewSb.append("public class ScreenView extends AbstractView {" + PL);
		screenViewSb.append(PL);
		screenViewSb.append(TAB + "public void doExecute() throws Exception {" + PL);

		for (Field f : xxx.getDeclaredFields()) {
			ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
			FieldDB filAnn = f.getAnnotation(FieldDB.class);
			if (colAnn == null) continue;
			if (filAnn == null) continue;


			if (f.getType().getSuperclass().equals(Bean.class)) {
				Class clazze = Class.forName(TransactionDB.PACKAGE_BEAN + "." + f.getType().getSimpleName());
				Bean b = (Bean)clazze.newInstance();
				if (b.hasAttribute("descricao")) {
					screenViewSb.append(TAB+TAB+"super.addTableColumn(\""+ filAnn.label() +"\", \""+ f.getName() +"Descricao\");" + PL);
				}
			} else {
				screenViewSb.append(TAB+TAB+"super.addTableColumn(\""+ filAnn.label() +"\", \""+ f.getName() +"\");" + PL);
			}
		}
		screenViewSb.append(TAB+"}" + PL);

		screenViewSb.append(PL);
		screenViewSb.append(TAB+"public void doFilter() throws Exception {" + PL);

		boolean abaAtivo = false;
		boolean filtroDescricao = false;
		for (Field f : xxx.getDeclaredFields()) {
			if (f.getName().equals("ativo") == true) abaAtivo = true;
			if (f.getName().equals("descricao") == true) filtroDescricao = true;
		}

		if (abaAtivo == true) {
			screenViewSb.append(TAB+TAB+"super.addTab(\"Ativo\", \"ativo\", FWConstants.ATIVO);" + PL);
			screenViewSb.append(TAB+TAB+"super.addTab(\"Inativos\", \"ativo\", FWConstants.INATIVO);" + PL);
		}

		if (filtroDescricao == true) {
			screenViewSb.append(TAB+TAB+"TextFieldHtml f1 = new TextFieldHtml(\"descricao\", \"Descrição\", null, false);" + PL);
			screenViewSb.append(TAB+TAB+"super.addFilterField(Condition.LIKE, f1);" + PL);
		}


		screenViewSb.append(TAB+"}" + PL);
		screenViewSb.append(PL);
		screenViewSb.append(PL);


		// DO DATA
		screenViewSb.append(TAB+"public Object[] doData() throws Exception {" + PL);
		screenViewSb.append(TAB+TAB+"StringBuilder sb = new StringBuilder();" + PL);
		for (int i = 1; i < lines.length; i++) {
			String line = lines[i];

			if (i == 1) line = "     select " + line.substring(12);
			screenViewSb.append(TAB+TAB+"sb.append(\"     ");
			screenViewSb.append(StringUtils.fillRight(line, ' ', 120) );
			screenViewSb.append("\");" + PL);
		}
		screenViewSb.append(TAB+TAB+"sb.append(\"     ");
		screenViewSb.append(TAB+StringUtils.fillRight("where 1 = 1", ' ', 120));
		screenViewSb.append("\");" + PL);
		screenViewSb.append(PL);
		screenViewSb.append(PL);
		screenViewSb.append(TAB+TAB+"ORM orm = new ORM();" + PL);


		int i = -1;

		boolean unidadeId = false;
		for (Field f : xxx.getDeclaredFields()) {
			ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
			FieldDB filAnn = f.getAnnotation(FieldDB.class);
			if (colAnn == null) continue;
			if (filAnn == null) continue;

			screenViewSb.append(TAB+TAB+"orm.add(");

			if (f.getType().getSuperclass().equals(Bean.class)) {
				i++;

				if (f.getName().equalsIgnoreCase("unidade")) unidadeId = true;
				// caso seja o bean colocar o Id e a Descricao
				// id
				screenViewSb.append(StringUtils.fillRight("Integer.class,", ' ', 20));
				screenViewSb.append(StringUtils.fillRight("\""+ f.getName() +"Id\",", ' ', 40));
				screenViewSb.append("\"t_"+ colAnn.name() +"\");" + PL);

				// descricao
				Class clazze = Class.forName(TransactionDB.PACKAGE_BEAN +"." + f.getType().getSimpleName());
				Bean b = (Bean)clazze.newInstance();
				if (b.hasAttribute("descricao")) {

					screenViewSb.append(TAB+TAB+"orm.add(");
					screenViewSb.append(StringUtils.fillRight("String.class,", ' ', 20));
					screenViewSb.append(StringUtils.fillRight("\""+ f.getName() +"Descricao\",", ' ', 40));
					screenViewSb.append("\"t"+i+"_"+colAnn.name().substring(0, colAnn.name().length()-2) +"descricao\");" + PL);
				}




			} else {
				screenViewSb.append(StringUtils.fillRight(f.getType().getSimpleName() +".class,", ' ', 20));
				screenViewSb.append(StringUtils.fillRight("\""+ f.getName() +"\",", ' ', 40));
				screenViewSb.append("\"t_"+ colAnn.name() +"\");" + PL);
			}
		}

		screenViewSb.append(PL);
		if (unidadeId == true) {
			screenViewSb.append(TAB+TAB+"WhereDB where = new WhereDB();" + PL);
			screenViewSb.append(TAB+TAB+"where.add(\"unidadeId\", Condition.EQUALS, getUserContext().getUnidade().getId());" + PL);
			screenViewSb.append(PL);
			screenViewSb.append(TAB+TAB+"return new Object[] {orm, sb.toString(), where};" + PL);
		} else {
			screenViewSb.append(TAB+TAB+"return new Object[] {orm, sb.toString()};" + PL);
		}


		screenViewSb.append(TAB+"}" + PL);
		screenViewSb.append(PL);
		screenViewSb.append(PL);

		// FORM CRUD PADRAO
		screenViewSb.append(TAB+"public static FormHtml createForm(String doData, String doProcess) throws Exception {" + PL);
		screenViewSb.append(TAB+TAB+"FormHtml form = new FormHtml(doData);" + PL);

		i = 0;
		StringBuilder fields = new StringBuilder();
		for (Field f : xxx.getDeclaredFields()) {

			ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
			FieldDB filAnn = f.getAnnotation(FieldDB.class);
			if (colAnn == null) continue;
			if (filAnn == null) continue;
			if (f.getName().equalsIgnoreCase("unidade") == true) continue;
			if (f.getName().equalsIgnoreCase("ativo") == true) continue;
			if (f.getName().equalsIgnoreCase("cadastro") == true) continue;
			if (f.getName().equalsIgnoreCase("atualizado") == true) continue;
			if (f.getName().equalsIgnoreCase("situacao") == true) continue;
			i++;
			if (colAnn.pk() == true) continue;


			String fieldName = "f" + StringUtils.fillLeft(i, '0', 2);

			if (f.getName().equalsIgnoreCase("ativo")) {
				screenViewSb.append(TAB+TAB+"ComboNumberFieldHtml "+ fieldName +" = new ComboNumberFieldHtml(\""+ f.getName() +"\", \""+ filAnn.label() +"\", null, true);" + PL);
				screenViewSb.append(TAB+TAB+""+ fieldName +".addOption(FWConstants.SIM, FWConstants.SIM_STR);" + PL);
				screenViewSb.append(TAB+TAB+""+ fieldName +".addOption(FWConstants.NAO, FWConstants.NAO_STR);" + PL);
			} else if (f.getType().equals(String.class)) {
				screenViewSb.append(TAB+TAB+"TextFieldHtml "+ fieldName +" = new TextFieldHtml(\""+ f.getName() +"\", \""+ filAnn.label() +"\", null, "+ filAnn.required() +");" + PL);
			} else if (f.getType().equals(Double.class) || f.getType().equals(BigDecimal.class)) {
				screenViewSb.append(TAB+TAB+"DecimalFieldHtml "+ fieldName +" = new DecimalFieldHtml(\""+ f.getName() +"\", \""+ filAnn.label() +"\", null, "+ filAnn.required() +");" + PL);
			} else if (f.getType().equals(Date.class)) {
				screenViewSb.append(TAB+TAB+"DateFieldHtml "+ fieldName +" = new DateFieldHtml(\""+ f.getName() +"\", \""+ filAnn.label() +"\", null, "+ filAnn.required() +");" + PL);
			} else if (f.getType().equals(Integer.class)) {
				screenViewSb.append(TAB+TAB+"NumberFieldHtml "+ fieldName +" = new NumberFieldHtml(\""+ f.getName() +"\", \""+ filAnn.label() +"\", null, "+ filAnn.required() +");" + PL);
			} else if (f.getType().getSuperclass().equals(Bean.class)) {
				screenViewSb.append(TAB+TAB+"ComboNumberFieldHtml "+ fieldName +" = new ComboNumberFieldHtml(\""+ f.getName() +"Id\", \""+ filAnn.label() +"\", null, "+ filAnn.required() +");" + PL);
				screenViewSb.append(TAB+TAB+""+ fieldName +".setClassBean("+ f.getType().getSimpleName() +".class);" + PL);
			} else {
				screenViewSb.append(TAB+TAB+"FieldHtml "+ fieldName +" = new FieldHtml(\""+ f.getName() +"\", \""+ filAnn.label() +"\", null, "+ filAnn.required() +");" + PL);
			}

			screenViewSb.append(TAB+TAB+"//" + PL);

			fields.append(fieldName +", ");
		}			
		screenViewSb.append(TAB+TAB+"form.addGroup(null, 1, "+ fields.toString().substring(0, fields.length() -2) +");"+PL);
		screenViewSb.append(TAB+TAB+"form.addButton(doProcess, \"Salvar\");"+PL);
		screenViewSb.append(TAB+TAB+"return form;"+PL);
		screenViewSb.append(TAB+"}"+PL);
		screenViewSb.append("}"+PL);
		screenViewSb.append(PL);

		return screenViewSb.toString();
	}
	private static String createInsert(TransactionDB trans, Class<? extends Bean> xxx, String packagePath) {
		StringBuilder sb = new StringBuilder();

		TableDB table = xxx.getDeclaredAnnotation(TableDB.class);
		String sigla = StringUtils.toUpperCaseFirstLetter(table.acronym());


		sb.append(createPackageImportDefault(packagePath, xxx));
		sb.append("@Menu(label=\"Inserir\", selecteds=Menu.ZERO, icon=CssIconEnum.INSERIR)"+PL);
		sb.append("public class "+ sigla +"010Inserir extends AbstractAction {"+PL);
		sb.append(""+PL);
		sb.append(TAB+"public void doScreen() throws Exception {"+PL);
		sb.append(TAB+TAB+"FormHtml form = ScreenView.createForm(null, \"doProcess\");"+PL);
		sb.append(TAB+TAB+"super.addBody(form);"+PL);
		sb.append(TAB+"}"+PL);

		//
		// METODO doProcess
		//
		sb.append(""+PL);
		sb.append(TAB+"public void doProcess(Integer id) throws Exception {"+PL);
		sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();"+PL);
		sb.append(TAB+TAB+"UserContext userContext = super.getUserContext();"+PL);
		sb.append(TAB+TAB+"Integer uniId = userContext.getUnidade().getId();"+PL);
		sb.append(TAB+TAB+"Date dataAtual = trans.getDataAtual();"+PL);
		sb.append(TAB+TAB+"String horaAtual = trans.getHoraAtual();"+PL);
		sb.append(PL);

		int i = 0;
		for (Field f : xxx.getDeclaredFields()) {

			ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
			FieldDB filAnn = f.getAnnotation(FieldDB.class);
			if (colAnn == null) continue;
			if (filAnn == null) continue;
			i++;
			if (colAnn.pk() == true) continue;

			if (f.getType().getSuperclass().equals(Bean.class)) {
				sb.append(TAB+TAB+ "Integer "+ f.getName() +"Id = super.getParamInteger(\""+ f.getName() +"Id\"); // f"+(i) +PL);
			} else {
				sb.append(TAB+TAB+ f.getType().getSimpleName() +" "+ f.getName() +" = super.getParam"+ f.getType().getSimpleName() +"(\""+ f.getName() +"\"); // f"+(i) +PL);
			}

		}
		sb.append(PL);
		sb.append(PL);

		sb.append(TAB+TAB+ xxx.getSimpleName() + " bean = new "+ xxx.getSimpleName() +"();"+PL);
		i = 0;
		for (Field f : xxx.getDeclaredFields()) {

			ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
			FieldDB filAnn = f.getAnnotation(FieldDB.class);
			if (colAnn == null) continue;
			if (filAnn == null) continue;
			i++;
			if (colAnn.pk() == true) continue;

			String name = f.getName();
			String temp = name.substring(0, 1).toUpperCase();
			temp += name.substring(1);

			if (f.getType().getSuperclass().equals(Bean.class)) {
				String yyy = f.getName() +"Id";
				if (f.getName().equalsIgnoreCase("unidade")) {
					yyy = "uniId";
				}
				String beanName = f.getType().getSimpleName();
				sb.append(TAB+TAB+"bean.get"+ beanName.substring(0, beanName.length()-4) +"().setId("+ yyy +");"+PL);
			} else {
				sb.append(TAB+TAB+"bean.set"+ temp +"("+ f.getName() +");"+PL);
			}
		}
		sb.append(TAB+TAB+"trans.insert(bean); "+PL);
		sb.append(PL);

		sb.append(TAB+TAB+"super.setMessageSuccess(\"Registro inserido com sucesso!\");"+PL);
		sb.append(TAB+"}"+PL);
		sb.append("}" + PL);

		return sb.toString();
	}
	private static String createAlterar(TransactionDB trans, Class<? extends Bean> xxx, String packagePath) {
		StringBuilder sb = new StringBuilder();

		TableDB table = xxx.getDeclaredAnnotation(TableDB.class);
		String sigla = StringUtils.toUpperCaseFirstLetter(table.acronym());


		sb.append(createPackageImportDefault(packagePath, xxx));
		sb.append("@Menu(label=\"Alterar\", selecteds=Menu.ONE, icon=CssIconEnum.ALTERAR, color=CssColorEnum.BTN_ICON_BLUE)"+PL);
		sb.append("public class "+ sigla +"015Alterar extends AbstractAction {"+PL);
		sb.append(""+PL);
		sb.append(TAB+"public void doScreen() throws Exception {"+PL);
		sb.append(TAB+TAB+"FormHtml form = ScreenView.createForm(\"doData\", \"doProcess\");"+PL);
		sb.append(TAB+TAB+"super.addBody(form);"+PL);
		sb.append(TAB+"}"+PL);

		//
		// METODO doProcess
		//
		sb.append(TAB+"public Object doData(Integer id) throws Exception {"+PL);
		sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();"+PL);
		sb.append(TAB+TAB+""+ xxx.getSimpleName() +" bean = trans.selectById("+ xxx.getSimpleName() +".class, id);"+PL);
		sb.append(TAB+TAB+"return bean;"+PL);
		sb.append(TAB+"}"+PL);


		sb.append(""+PL);
		sb.append(TAB+"public void doProcess(Integer id) throws Exception {"+PL);
		sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();"+PL);
		sb.append(TAB+TAB+"UserContext userContext = super.getUserContext();"+PL);
		sb.append(TAB+TAB+"Integer uniId = userContext.getUnidade().getId();"+PL);
		sb.append(TAB+TAB+"Date dataAtual = trans.getDataAtual();"+PL);
		sb.append(TAB+TAB+"String horaAtual = trans.getHoraAtual();"+PL);
		sb.append(PL);


		int i = 0;
		for (Field f : xxx.getDeclaredFields()) {

			ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
			FieldDB filAnn = f.getAnnotation(FieldDB.class);
			if (colAnn == null) continue;
			if (filAnn == null) continue;
			if (f.getName().equalsIgnoreCase("cadastro") == true) continue;
			if (f.getName().equalsIgnoreCase("id") == true) continue;
			i++;

			if (f.getType().getSuperclass().equals(Bean.class)) {
				sb.append(TAB+TAB+ "Integer "+ f.getName() +"Id = super.getParamInteger(\""+ f.getName() +"Id\"); // f"+(i) +PL);
			} else {
				sb.append(TAB+TAB+ f.getType().getSimpleName() +" "+ f.getName() +" = super.getParam"+ f.getType().getSimpleName() +"(\""+ f.getName() +"\"); // f"+(i) +PL);
			}

		}
		sb.append(PL);
		sb.append(PL);

		sb.append(TAB+TAB+ xxx.getSimpleName() + " bean = trans.selectById("+ xxx.getSimpleName() +".class, id);"+PL);
		i = 0;
		for (Field f : xxx.getDeclaredFields()) {

			ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
			FieldDB filAnn = f.getAnnotation(FieldDB.class);
			if (colAnn == null) continue;
			if (filAnn == null) continue;
			if (f.getName().equalsIgnoreCase("cadastro") == true) continue;
			i++;
			if (colAnn.pk() == true) continue;


			String name = f.getName();
			String temp = name.substring(0, 1).toUpperCase();
			temp += name.substring(1);
			
			
			if (f.getType().getSuperclass().equals(Bean.class)) {
				String yyy = f.getName() +"Id";
				if (f.getName().equalsIgnoreCase("unidade")) {
					yyy = "uniId";
				}
				String beanName = f.getType().getSimpleName();
				sb.append(TAB+TAB+"bean.get"+ beanName.substring(0, beanName.length()-4) +"().setId("+ yyy +");"+PL);
			} else {
				sb.append(TAB+TAB+"bean.set"+ temp +"("+ f.getName() +");"+PL);
			}
		}
		sb.append(TAB+TAB+"trans.update(bean); "+PL);
		sb.append(PL);

		sb.append(TAB+TAB+"super.setMessageSuccess(\"Registro alterado com sucesso!\");"+PL);
		sb.append(TAB+"}"+PL);
		sb.append("}" + PL);

		return sb.toString();
	}
	private static String createInativar(TransactionDB trans, Class<? extends Bean> xxx, String packagePath) {
		StringBuilder sb = new StringBuilder();


		TableDB table = xxx.getDeclaredAnnotation(TableDB.class);
		String sigla = StringUtils.toUpperCaseFirstLetter(table.acronym());


		sb.append(createPackageImportDefault(packagePath, xxx));
		sb.append("@Menu(label=\"Ativar/Inativar\", selecteds=Menu.ONE, icon=CssIconEnum.APAGAR, color=CssColorEnum.BTN_ICON_BLUE)"+PL);
		sb.append("public class "+ sigla +"020Inativar extends AbstractAction {"+PL);
		sb.append(""+PL);
		sb.append(TAB+"public void doScreen() throws Exception {"+PL);
		sb.append(TAB+TAB+"MessageHtml message = new MessageHtml(Type.INFO);"+PL);
		sb.append(TAB+TAB+"message.addContent(\"Deseja realmente ativar/inativar este registro?\");"+PL);

		sb.append(TAB+TAB+"FormHtml form = new FormHtml();"+PL);
		sb.append(TAB+TAB+"form.addButton(\"doProcess\", \"Inativar\");"+PL);

		sb.append(TAB+TAB+"super.addBody(message);"+PL);
		sb.append(TAB+TAB+"super.addBody(form);"+PL);
		sb.append(TAB+"}"+PL);

		//
		// METODO doProcess
		//
		sb.append(TAB+"public Object doData(Integer id) throws Exception {"+PL);
		sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();"+PL);
		sb.append(TAB+TAB+""+ xxx.getSimpleName() +" bean = trans.selectById("+ xxx.getSimpleName() +".class, id);"+PL);
		sb.append(TAB+TAB+"return bean;"+PL);
		sb.append(TAB+"}"+PL);


		sb.append(""+PL);
		sb.append(TAB+"public void doProcess(Integer id) throws Exception {"+PL);
		sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();"+PL);
		sb.append(TAB+TAB+"UserContext userContext = super.getUserContext();"+PL);
		sb.append(TAB+TAB+""+PL);
		sb.append(TAB+TAB+"String mensagem = \"ativado\";"+PL);
		sb.append(TAB+TAB+"Integer value = FWConstants.ATIVO;"+PL);
		sb.append(TAB+TAB+ xxx.getSimpleName() + " bean = trans.selectById("+ xxx.getSimpleName() +".class, id);"+PL);
		sb.append(TAB+TAB+"if (bean.getAtivo().equals(FWConstants.ATIVO)) {"+PL);
		sb.append(TAB+TAB+TAB+"mensagem = \"inativado\";"+PL);
		sb.append(TAB+TAB+TAB+"value = FWConstants.INATIVO;"+PL);
		sb.append(TAB+TAB+"}"+PL);
		sb.append(TAB+TAB+""+PL);
		sb.append(TAB+TAB+"bean.setAtivo( value );"+PL);
		sb.append(TAB+TAB+"trans.update(bean); "+PL);
		sb.append(TAB+TAB+""+PL);
		sb.append(TAB+TAB+"super.setMessageSuccess(\"Registro \"+ mensagem +\" com sucesso!\");"+PL);
		sb.append(TAB+TAB+""+PL);
		sb.append(TAB+"}"+PL);
		sb.append("}" + PL);

		return sb.toString();
	}
	private static String createCancelar(TransactionDB trans, Class<? extends Bean> xxx, String packagePath) {
		StringBuilder sb = new StringBuilder();
		
		
		TableDB table = xxx.getDeclaredAnnotation(TableDB.class);
		String sigla = StringUtils.toUpperCaseFirstLetter(table.acronym());
		
		
		sb.append(createPackageImportDefault(packagePath, xxx));
		sb.append("@Menu(label=\"Cancelar\", selecteds=Menu.ONE, icon=CssIconEnum.TRASH, color=CssColorEnum.BTN_ICON_RED)"+PL);
		sb.append("public class "+ sigla +"022Cancelar extends AbstractAction {"+PL);
		sb.append(""+PL);
		sb.append(TAB+"public void doScreen() throws Exception {"+PL);
		sb.append(TAB+TAB+"MessageHtml message = new MessageHtml(Type.INFO);"+PL);
		sb.append(TAB+TAB+"message.addContent(\"Deseja realmente cancelar este registro?\");"+PL);
		
		sb.append(TAB+TAB+"FormHtml form = new FormHtml();"+PL);
		sb.append(TAB+TAB+"form.addButton(\"doProcess\", \"Sim\");"+PL);
		
		sb.append(TAB+TAB+"super.addBody(message);"+PL);
		sb.append(TAB+TAB+"super.addBody(form);"+PL);
		sb.append(TAB+"}"+PL);
		
		//
		// METODO doProcess
		//
		sb.append(TAB+"public Object doData(Integer id) throws Exception {"+PL);
		sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();"+PL);
		sb.append(TAB+TAB+""+ xxx.getSimpleName() +" bean = trans.selectById("+ xxx.getSimpleName() +".class, id);"+PL);
		sb.append(TAB+TAB+"return bean;"+PL);
		sb.append(TAB+"}"+PL);
		
		
		sb.append(""+PL);
		sb.append(TAB+"public void doProcess(Integer id) throws Exception {"+PL);
		sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();"+PL);
		sb.append(TAB+TAB+"UserContext userContext = super.getUserContext();"+PL);
		sb.append(TAB+TAB+""+PL);
		sb.append(TAB+TAB+""+ xxx.getSimpleName() +" bean = new "+ xxx.getSimpleName() +"(id);"+PL);
		sb.append(TAB+TAB+"bean.getSituacao().setId( SitDD.CANCELADO );"+PL);
		sb.append(TAB+TAB+"trans.updateIfNotNull(bean); "+PL);
		sb.append(TAB+TAB+""+PL);
		sb.append(TAB+TAB+"super.setMessageSuccess(\"Registro cancelado com sucesso!\");"+PL);
		sb.append(TAB+TAB+""+PL);
		sb.append(TAB+"}"+PL);
		sb.append("}" + PL);
		
		return sb.toString();
	}
	private static String createDetalhar(TransactionDB trans, Class<? extends Bean> xxx, String packagePath) throws SQLException, Exception {
		StringBuilder sb = new StringBuilder();



		TableDB table = xxx.getDeclaredAnnotation(TableDB.class);
		String sigla = StringUtils.toUpperCaseFirstLetter(table.acronym());
		List<Bean> list = selectRelacionamento(trans, table.acronym());
		if (list == null) return null;


		sb.append(createPackageImportDefault(packagePath, xxx));

		sb.append("@Menu(label=\"Detalhar\", selecteds=Menu.UM, color=CssColorEnum.BTN_ICON_BLUE)"+PL);
		sb.append("public class "+ sigla +"001Detalhar extends AbstractAction {"+PL);
		sb.append(""+PL);
		sb.append(TAB+"public void doScreen() throws Exception {"+PL);
		sb.append(TAB+TAB+"TabHtml tab = new TabHtml();" + PL);
		sb.append(TAB+TAB+ PL);


		//
		// VIEW PRINCIPAL
		//
		sb.append(TAB+TAB+ "ViewHtml view = new ViewHtml(\"doDataMasterView\");" + PL);
		int i = 0;
		String fieldStr = "";
		String temp = "";
		for (Field f : xxx.getDeclaredFields()) {
			ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
			FieldDB filAnn = f.getAnnotation(FieldDB.class);
			if (colAnn == null) continue;
			if (filAnn == null) continue;


			i++;
			fieldStr += temp;
			fieldStr += "f"+i;
			temp = ", ";

			String part = "";
			if (f.getType().getSuperclass().equals(Bean.class)) {
				Class clazze = Class.forName(TransactionDB.PACKAGE_BEAN +"."+ f.getType().getSimpleName());
				Bean b = (Bean)clazze.newInstance();
				if (b.hasAttribute("descricao")) {
					part = "Descricao";
				} else {
					part = "\"XXX";
				}
			}
			sb.append(TAB+TAB+"FieldViewHtml f"+ i +" = new FieldViewHtml(\""+ filAnn.label() +"\", \""+ f.getName() + part + "\");" + PL);

		}
		sb.append(PL);
		sb.append(TAB+TAB+"view.addGroup(null, 2, "+ fieldStr +");" + PL);
		sb.append(TAB+TAB+"tab.add(\"Geral\", view);" + PL);
		sb.append(PL);


		//
		// RELACIONAMENTOS COM TABELA
		//
		i = 0;
		for (Bean bean : list) {
			i++;

			String tableName = bean.get("table");
			TableC tC = TableC.getTableByName(tableName);
			Class classe = ClassUtils.getClass(TransactionDB.PACKAGE_BEAN +"."+ tC.getClassName()+ "Bean");

			sb.append(TAB+TAB+"TableHtml table"+ i +" = new TableHtml(\"doDataTable"+ tC.getClassName() +"\");" + PL);

			for (Field f : classe.getDeclaredFields()) {
				ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
				FieldDB filAnn = f.getAnnotation(FieldDB.class);
				if (colAnn == null) continue;
				if (filAnn == null) continue;


				sb.append(TAB+TAB+"table"+ i +".addColumn(\""+ filAnn.label() +"\", \""+ f.getName() + "\");" + PL);
			}

			sb.append(TAB+TAB+"tab.add(\""+ tC.getLabel() +"\", table"+ i +");" + PL);
			sb.append(PL);

		}



		sb.append(TAB+"}" + PL);


		//
		// METODO DODATA DO VIEW PRINCIPAL
		//
		sb.append(PL);
		sb.append(TAB+"public Object doDataMasterView(Integer id) throws Exception {" + PL);
		sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();" + PL);
		sb.append(TAB+TAB+ xxx.getSimpleName() + " bean = trans.selectById("+ xxx.getSimpleName() +".class, id);" + PL);
		sb.append(TAB+TAB+"return bean;" + PL);
		sb.append(TAB+"}" + PL);

		sb.append(PL);

		//
		// doData dos RELACIONAMETOS
		//
		i = 0;
		for (Bean bean : list) {
			i++;

			String tableName = bean.get("table");
			TableC tC = TableC.getTableByName(tableName);
			Class classe = ClassUtils.getClass(TransactionDB.PACKAGE_BEAN +"." + tC.getClassName()+ "Bean");

			sb.append(PL);
			sb.append(TAB+"public Object doDataTable"+ tC.getClassName() +"(Integer id) throws Exception {" + PL);
			sb.append(TAB+TAB+"TransactionDB trans = super.getTransaction();" + PL);

			sb.append(TAB+TAB+"WhereDB where = new WhereDB();" + PL);
			sb.append(TAB+TAB+"where.add(\""+ xxx.getSimpleName() +".Id\", Condition.EQUALS, id);" + PL);
			sb.append(TAB+TAB+"List<" + classe.getSimpleName() + "> list = trans.select("+ classe.getSimpleName() +".class, where);" + PL);
			sb.append(TAB+TAB+"return list;" + PL);
			sb.append(TAB+"}" + PL);
		}

		sb.append("}" + PL);

		return sb.toString();
	}

	private static Object createPackageImportDefault(String packagePath,  Class<? extends Bean> xxx) {
		StringBuilder sb = new StringBuilder();

		sb.append("package " + packagePath +";"+PL);
		sb.append(""+PL);
		sb.append("import java.math.BigDecimal;"+PL);
		sb.append("import java.util.Date;"+PL);
		sb.append("import java.util.List;"+PL);
		sb.append(""+PL);
		sb.append("import br.com.dotum.core.component.AbstractAction;"+PL);
		sb.append("import br.com.dotum.core.component.Menu;"+PL);
		sb.append("import br.com.dotum.core.component.AbstractView;" + PL);
		sb.append("import br.com.dotum.core.component.Task;" + PL);
		sb.append("import br.com.dotum.core.constants.FWConstants;"+PL);
		sb.append("import br.com.dotum.core.enuns.CssColorEnum;"+PL);
		sb.append("import br.com.dotum.core.enuns.CssIconEnum;"+PL);
		sb.append("import br.com.dotum.core.html.form.field.*;" + PL);
		sb.append("import br.com.dotum.core.html.form.FormHtml;"+PL);
		sb.append("import br.com.dotum.core.html.other.MessageHtml;"+PL);
		sb.append("import br.com.dotum.core.html.other.MessageHtml.Type;"+PL);
		sb.append("import br.com.dotum.core.html.other.TabHtml;"+PL);
		sb.append("import br.com.dotum.core.html.other.TableHtml;"+PL);
		sb.append("import br.com.dotum.core.html.other.TableHtml.SummaryEnum;"+PL);


		sb.append("import br.com.dotum.core.html.view.FieldViewHtml;"+PL);
		sb.append("import br.com.dotum.core.html.view.ViewHtml;"+PL);
		sb.append("import br.com.dotum.jedi.core.db.TransactionDB;"+PL);
		sb.append("import br.com.dotum.jedi.core.db.ORM;" + PL);
		sb.append("import br.com.dotum.jedi.core.db.WhereDB;"+PL);
		sb.append("import br.com.dotum.jedi.core.db.WhereDB.Condition;"+PL);
		sb.append("import br.com.dotum.jedi.core.security.UserContext;"+PL);


		if (xxx != null) {
			sb.append("import "+ TransactionDB.PACKAGE_DD + ".*;"+PL);
			sb.append("import "+ TransactionDB.PACKAGE_BEAN + "." + xxx.getSimpleName() +";"+PL);

			for (Field f : xxx.getDeclaredFields()) {
				ColumnDB colAnn = f.getAnnotation(ColumnDB.class);
				FieldDB filAnn = f.getAnnotation(FieldDB.class);
				if (colAnn == null) continue;
				if (filAnn == null) continue;
				if (f.getType().getSuperclass().equals(Bean.class) == false) continue;

				sb.append("import "+ TransactionDB.PACKAGE_BEAN + "." + f.getType().getSimpleName() +";" + PL);
			}
		}
		sb.append( PL );

		return sb.toString();
	}
	private static void criarArquivo(String conteudo, String path, String pack, String fileName) throws Exception {
		path = path + (path.endsWith("/") ? "" : "/") + "src/main/java/" + pack.replace(".", "/") + "/";

		FileMemory fm = new FileMemory(conteudo, fileName);
		fm.setPath(path);
		fm.createFile();
	}
	private static List<Bean> selectRelacionamento(TransactionDB trans, String pk) throws SQLException, Exception {

		List<Bean> list = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("          SELECT TABLE_NAME, CONSTRAINT_NAME                      ");
			sb.append("            FROM USER_CONSTRAINTS                                 ");
			sb.append("           WHERE R_CONSTRAINT_NAME = 'PK_"+ pk.toUpperCase() +"'     ");

			ORM orm = new ORM();
			orm.add(String.class, "table", "TABLE_NAME");

			list = trans.select(Bean.class, orm, sb.toString());
		} catch (NotFoundException e) {
		}

		return list;
	}

}

