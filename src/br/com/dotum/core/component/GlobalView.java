package br.com.dotum.core.component;

import java.lang.annotation.IncompleteAnnotationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.enuns.CssAnimationEnum;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.core.servlet.AbstractDispacher;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.SecurityTaskBean;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.MathUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

public class GlobalView {
	private static Log LOG = LogFactory.getLog(GlobalView.class);

	public static List<Class<?>> classes = null;
	public static String TIPO;

	private HttpServletRequest request;

	public GlobalView(HttpServletRequest request) {
		this.request = request;
	}

	static {
		TIPO = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
		if (StringUtils.hasValue(TIPO) == false) TIPO = "erp";
	}

	public Html criaHtmlTop(ResultHtml html, UserContext userContext, String code, String title, Map<Double, List<ComponentHtml>> menuTop) throws Exception {
		//		if (userContext == null) userContext = new UserContext();


		Map<Double, List<Bean>> taskList = getTask(userContext);



		Html compHtml = null;
		//		Html compHtml = userContext.getUser().get(FWConstants.HTML_TOP);
		//		if (compHtml == null) {
		StringBuilder sb = new StringBuilder();
		//
		// criar o menu aqui
		//
		sb.append("<div id=\"navbar\" class=\"navbar navbar-default ace-save-state navbar-fixed-top\">" + ResultHtml.PL);
		sb.append( "<div id=\"navbar-container\" class=\"navbar-container ace-save-state\">" + ResultHtml.PL);
		sb.append(  "<button type=\"button\" class=\"navbar-toggle menu-toggler pull-left\" id=\"menu-toggler\" data-target=\"#sidebar\">" + ResultHtml.PL);
		sb.append(   "<span class=\"sr-only\">Toggle sidebar</span>" + ResultHtml.PL);
		sb.append(   "<span class=\"icon-bar\"></span>" + ResultHtml.PL);
		sb.append(   "<span class=\"icon-bar\"></span>" + ResultHtml.PL);
		sb.append(   "<span class=\"icon-bar\"></span>" + ResultHtml.PL);
		sb.append(  "</button>" + ResultHtml.PL);

		sb.append( "<a href=\""+PropertiesUtils.getString("system.link.home")+"\" class=\"navbar-brand\">" + ResultHtml.PL);
		String icon = PropertiesUtils.getString(PropertiesConstants.SYSTEM_ICON);
		if (StringUtils.hasValue(icon)) 
			sb.append(  "<small><i class=\""+ icon +"\"></i></small>"+ ResultHtml.PL);
		String titulo = PropertiesUtils.getString(PropertiesConstants.SYSTEM_TITLE);
		if (StringUtils.hasValue(titulo)) 
			sb.append(  titulo + ResultHtml.PL);
		sb.append( "</a>" + ResultHtml.PL);




		sb.append( createHtmlMenu1(taskList.get(Menu.TOP)).getComponente() );

		sb.append("<div class=\"navbar-buttons navbar-header pull-right\" role=\"navigation\">");
		sb.append(  "<ul class=\"nav ace-nav\">");



		// 
		// MENU DE OPÇÔES NORMAIS 
		// 
		for (int i = 3; i >= 0; i--) {
			Double item = (5.0 + (i/10.0));
			sb.append( createHtmlMenu5(item, taskList.get(item)) );
		}
		for (int i = 9; i >= 0; i--) {
			Double item = (4.0 + (i/10.0));
			sb.append( createHtmlMenu4(html, item, taskList.get(item), menuTop) );
		}

		for (int i = 3; i >= 0; i--) {
			Double item = (2.0 + (i/10.0));
			sb.append( createHtmlMenu2(item, taskList.get(item), userContext) ); 
		}


		sb.append(   "</ul>"); // /.ace-nav
		sb.append(  "</div>");

		String version = PropertiesUtils.getString(PropertiesConstants.SYSTEM_VERSION);
		//		sb.append("<div class=\"navbar-collapse pull-right white\"><div class=\"space-10\"></div>"+ version +"</div>" + ResultHtml.PL);
		sb.append( "</div>");
		sb.append("</div>");

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		sb.append("<div class=\"main-container ace-save-state\" id=\"main-container\">" + ResultHtml.PL);

		sb.append( createHtmlMenu3(taskList.get(Menu.LEFT)).getComponente());

		sb.append( "<div class=\"main-content\">" + ResultHtml.PL);
		sb.append(  "<div class=\"main-content-inner\">" + ResultHtml.PL);

		sb.append(   "<div id=\"breadcrumbs\" class=\"breadcrumbs ace-save-state breadcrumbs-fixed\" title=\""+ code +"\">" + ResultHtml.PL);
		sb.append(    "<ul class=\"breadcrumb\">" + ResultHtml.PL);
		sb.append(     "<li class=\"lighter bolder menu-text\">" + ResultHtml.PL);
		sb.append(title + ResultHtml.PL);
		sb.append(     "</li>" + ResultHtml.PL);
		sb.append(    "</ul>" + ResultHtml.PL); // <!-- .breadcrumb -->
		sb.append(    "<div class=\"nav-search\">" + ResultHtml.PL);
		sb.append(    "</div>" + ResultHtml.PL);
		sb.append(   "</div>" + ResultHtml.PL);


		sb.append(   "<div class=\"page-content\">" + ResultHtml.PL);
		sb.append(    "<div class=\"row\">" + ResultHtml.PL);
		sb.append(     "<div class=\"col-xs-12\">" + ResultHtml.PL);
		if (ResultHtml.DEBUG == true) sb.append("<!-- AQUI COMECA O CONTEUDO DA BAGACA -->" + ResultHtml.PL);

		compHtml = new Html(sb.toString());
		//			userContext.getUser().set(FWConstants.HTML_TOP, compHtml);
		//		} 
		return compHtml;
	}

	public Html criaHtmlBottom(UserContext usBean) throws Exception {
		StringBuilder sb = new StringBuilder();
		if (ResultHtml.DEBUG == true) sb.append("<!-- AQUI TERMINA O CONTEUDO DA BAGACA -->" + ResultHtml.PL);
		sb.append(     "</div>" + ResultHtml.PL);
		sb.append(    "</div>" + ResultHtml.PL);
		sb.append(   "</div>" + ResultHtml.PL);
		sb.append(  "</div>" + ResultHtml.PL);
		sb.append( "</div>" + ResultHtml.PL);
		sb.append("</div>" + ResultHtml.PL);

		return new Html(sb.toString());
	}

	// MENUS
	private Html createHtmlMenu1(List<Bean> taskList) throws Exception {
		StringBuilder sb = new StringBuilder();

		// varre tudo para ver o que tem  :)
		if ((taskList != null) && (taskList.size() > 0)) {
			sb.append( createHtmlAbreMenu1() );

			//
			// inicio do menu
			//
			sb.append("<!-- INICIO DOS MODULOS -->" + ResultHtml.PL);
			sb.append("    <li class=\"hover\">" + ResultHtml.PL);
			sb.append("     <a class=\"dropdown-toggle\">" + ResultHtml.PL);
			sb.append("      <div class=\"\">" + ResultHtml.PL);
			sb.append("       <i class=\"menu-icon fa fa-th\"></i>" + ResultHtml.PL);
			sb.append("       <span>Modulos</span>" + ResultHtml.PL);
			sb.append("       <i class=\"arrow fa fa-angle-down\"></i>" + ResultHtml.PL);
			sb.append("      </div>" + ResultHtml.PL);
			sb.append("     </a>" + ResultHtml.PL);
			sb.append("     <ul class=\"submenu submenubottom can-scroll\">" + ResultHtml.PL);
			sb.append("<!-- INICIO DOS MODULOS -->" + ResultHtml.PL);


			//
			//
			//
			String grupoOld = null;
			String subGrupoOld = null;
			int i = -1;
			for (Bean b : taskList) {
				String grupo = b.getAttribute("grupo");
				String subGrupo = b.getAttribute("subGrupo");
				String descricao = b.getAttribute("descricao");
				String url = b.getAttribute("url");
				String window = b.getAttribute("window");
				CssIconEnum iconMenu = b.getAttribute("icon");
				Double menu = b.getAttribute("menu");
				if (menu.equals(1.0) == false) continue;
				if(StringUtils.hasValue(grupo) == false) continue;
				i++;
				if (grupo.equals(grupoOld) == false) {

					if (i > 0) {
						if ((subGrupo == null) && (subGrupoOld != null)) {
							sb.append("                     </li>" + ResultHtml.PL);
							sb.append("                    </ul>" + ResultHtml.PL);
							sb.append("                  </li>" + ResultHtml.PL);
						}

						sb.append("                </ul>" + ResultHtml.PL);
						sb.append("              </li>" + ResultHtml.PL);
					}
					sb.append("                  <li class=\"hover\">" + ResultHtml.PL);
					sb.append("                    <a href=\"#\">" + ResultHtml.PL);
					sb.append("                      <i class=\"menu-icon fa fa-caret-right\"></i>" + ResultHtml.PL);
					sb.append(                         grupo  + ResultHtml.PL);
					sb.append("                        <b class=\"arrow fa fa-angle-right\"></b>" + ResultHtml.PL);
					sb.append("                    </a>" + ResultHtml.PL);
					sb.append("                    <ul class=\"submenu can-scroll\">" + ResultHtml.PL);
				}

				sb.append("                          <li class=\"hover\">" + ResultHtml.PL);


				if (((subGrupo != null) && subGrupo.equals(subGrupoOld) == false)) {

					sb.append("                      <li class=\"hover\">" + ResultHtml.PL);
					sb.append("                        <a href=\"#\">" + ResultHtml.PL);
					sb.append("                          <i class=\"menu-icon fa fa-print\"></i>" + ResultHtml.PL);
					sb.append(                           subGrupo  + ResultHtml.PL);
					sb.append("                          <b class=\"arrow fa fa-angle-right\"></b>" + ResultHtml.PL);
					sb.append("                        </a>" + ResultHtml.PL);
					sb.append("                      <ul class=\"submenu can-scroll\">" + ResultHtml.PL);
					sb.append("                       <li class=\"hover\">" + ResultHtml.PL);
				}


				if (url != null) {
					sb.append("                            <a href=\""+ url +"\">" + ResultHtml.PL);
				} else {
					sb.append("                            <a href=\"#\" class=\""+ window +"\">" + ResultHtml.PL);
				}
				if (iconMenu != null) 
					sb.append("                              <i class=\""+ iconMenu +"\"></i>" + ResultHtml.PL);
				sb.append("                              &nbsp;&nbsp;" + descricao + ResultHtml.PL);


				sb.append("                            </a>" + ResultHtml.PL);
				sb.append("                          </li>" + ResultHtml.PL);
				grupoOld = grupo;
				subGrupoOld = subGrupo;
			}




			sb.append("                                                       </ul>" + ResultHtml.PL);
			sb.append("                                                     </li>" + ResultHtml.PL);
			sb.append("                                                   </ul>" + ResultHtml.PL);
			sb.append("                                                 </li>" + ResultHtml.PL);



			//
			// separador para itens avulsos
			//
			sb.append("                                               <li class=\"divider\"></li>");



			//
			// isso aqui é para colocar o dashboard sem ter um submenu!!!
			//
			for (Bean b : taskList) {
				String grupo = b.getAttribute("grupo");
				String descricao = b.getAttribute("descricao");
				String url = b.getAttribute("url");
				String window = b.getAttribute("window");
				CssIconEnum iconMenu = b.getAttribute("icon");
				Double menu = b.getAttribute("menu");
				if (menu.equals(Menu.TOP) == false) continue;
				if(StringUtils.hasValue(grupo)) continue;
				sb.append("          <li class=\"hover\">" + ResultHtml.PL);

				if (url != null) {
					sb.append("            <a href=\""+ url +"\">" + ResultHtml.PL);
				} else {
					sb.append("            <a href=\"#\" class=\""+ window +"\">" + ResultHtml.PL);
				}
				if (iconMenu != null) sb.append("              <i class=\""+ iconMenu +"\"></i>" + ResultHtml.PL);

				sb.append("&nbsp;&nbsp;" + descricao + ResultHtml.PL);
				sb.append("            </a>" + ResultHtml.PL);
				sb.append("          </li>" + ResultHtml.PL);

			}

			sb.append(createHtmlFechaMenu1());
		}
		return new Html(sb.toString());
	}

	private Object createHtmlAbreMenu1() {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"top\">" + ResultHtml.PL);
		sb.append(" <nav class=\"navbar-menu pull-left collapse navbar-collapse\">" + ResultHtml.PL);
		sb.append("  <div class=\"sidebar h-sidebar navbar-collapse collapse\">" + ResultHtml.PL);
		sb.append("   <ul class=\"nav nav-list\">" + ResultHtml.PL);
		sb.append("   <!-- INICIO GERAL -->" + ResultHtml.PL);
		return sb.toString();
	}
	private Object createHtmlFechaMenu1() {
		StringBuilder sb = new StringBuilder();
		sb.append("   <!-- FIM GERAL -->" + ResultHtml.PL);
		sb.append("   </ul>" + ResultHtml.PL);
		sb.append("  </div>" + ResultHtml.PL);
		sb.append(" </nav>" + ResultHtml.PL);
		sb.append("</div>" + ResultHtml.PL);
		return sb.toString();
	}

	private String createHtmlMenu2(Double menuInt, List<Bean> taskList, UserContext userContext) throws Exception {
		StringBuilder sb = new StringBuilder();

		// varre tudo para ver o que tem  :)
		if ((taskList != null) && (taskList.size() > 0)) {
			Integer resto = (MathUtils.multiply(MathUtils.subtract(menuInt, menuInt.intValue()), 10)).intValue();
			CssIconEnum cssIcon = null;
			String color = null;
			if (TIPO.equals("sicoob")) {
				color = "green";
				if (resto == 0) {color = "green"; cssIcon = CssIconEnum.CARET_DOWN;}; 
				if (resto == 1) {color = "green"; cssIcon = CssIconEnum.TASKS;}; 
				if (resto == 2) {color = "green"; cssIcon = CssIconEnum.ENVELOPE;}; 
				if (resto == 3) {color = "green"; cssIcon = CssIconEnum.EXCLAMATION_TRIANGLE;}; 
			} else {
				color = "light-blue";
				if (resto == 0) {color = "light-blue"; cssIcon = CssIconEnum.CARET_DOWN;}; 
				if (resto == 1) {color = "light-blue"; cssIcon = CssIconEnum.TASKS;}; 
				if (resto == 2) {color = "light-blue"; cssIcon = CssIconEnum.ENVELOPE;}; 
				if (resto == 3) {color = "light-blue"; cssIcon = CssIconEnum.EXCLAMATION_TRIANGLE;}; 
			}


			sb.append(    "<li class=\""+ color +" dropdown-modal\">");
			sb.append(     "<a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">");

			if (resto == 0) {
				sb.append(     "<span class=\"user-info\">" + ResultHtml.PL);
				sb.append(      "<small>"+ StringUtils.coalesce(userContext.getUnidade().getNome(), "Bem vindo(a)") +",</small>");
				sb.append(        userContext.getUser().getNome());
				sb.append(      "</span>");	
			}

			sb.append(     "<i class=\""+ cssIcon.getDescricao() +"\"></i>");
			sb.append(    "</a>");
			sb.append(    "<ul class=\"user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close\">");

			String grupoOld = null;
			for (Bean b : taskList) {
				String grupo = b.getAttribute("grupo");
				String descricao = b.getAttribute("descricao");
				String url = b.getAttribute("url");
				String window = b.getAttribute("window");
				CssIconEnum icon = b.getAttribute("icon");
				Double menu = b.getAttribute("menu");
				if (menu.equals(menuInt) == false) continue;
				if (grupo.equals(grupoOld) == false) {
					if (grupoOld != null) {
						sb.append(   "<li class=\"divider\"></li>");
					}
				}

				sb.append(   "<li>");

				if (url != null) {
					sb.append("<a href=\""+ url +"\">");
					if (icon != null) {
						sb.append("<i class=\""+ icon +"\"></i>");
					}
					sb.append(descricao);
					sb.append("</a>");
				} else {
					sb.append("<a href=\"#\" class=\""+ window +"\">");
					if (icon != null) sb.append("<i class=\""+ icon +"\"></i>");
					sb.append( descricao);
					sb.append("</a>");
				}

				sb.append(   "</li>");
				grupoOld = grupo;
			}

			sb.append(    "</ul>");
			sb.append(    "</li>");
		}
		return sb.toString();
	}
	private String createHtmlMenu4(ResultHtml html, Double menuInt, List<Bean> taskList, Map<Double, List<ComponentHtml>> menuTop) throws Exception {
		StringBuilder sb = new StringBuilder();

		// varre tudo para ver o que tem  :)
		if ((taskList != null) && (taskList.size() > 0)) {
			Bean bean = taskList.get(0);

			List<ComponentHtml> compList = null;
			for (Double x : menuTop.keySet()) {
				if (x.equals(menuInt) == false) continue;

				compList = menuTop.get(x);
				break;
			}

			if (compList == null) {
				// TODO ver esse log se esta certo... 
				//				LOG.error("Tem um componente que esta tentando ser inserido no menu " + menuInt + " e o mesmo não possui a opção de window off.");
				return "";
			}


			Class<?> classe = bean.getAttribute("classe");
			Menu m = classe.getAnnotation(Menu.class);
			CssIconEnum icon = bean.getAttribute("icon");

			if (TIPO.equals("sicoob")) {
				sb.append(    "<li class=\"green dropdown-modal\">");
			} else {
				sb.append(     "<li class=\"light-blue dropdown-modal\">");
			}
			sb.append(     "<a data-toggle=\"dropdown\" class=\"dropdown-toggle BTN_"+ classe.getSimpleName()+"\" href=\"#\">");

			if (icon.equals(CssIconEnum.STAR) == false) {
				sb.append(     "<i class=\""+ icon.getDescricao() +"\"></i>");
			} else {
				sb.append(         m.label());
			}

			if (m.notification()) {
				sb.append(       "<span class=\"FW_QTD_"+ classe.getSimpleName()+" badge badge-important\">0</span>");
			}

			sb.append(    "</a>");
			sb.append(    "<ul class=\"dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close\">");
			sb.append(     "<li class=\"dropdown-header\">");
			sb.append(      "<i class=\""+ icon.getDescricao() +"\"></i>");
			sb.append(           m.label());


			sb.append(     "</li>");

			sb.append(     "<li class=\"dropdown-content\">");
			sb.append(      "<ul class=\"FW_"+ classe.getSimpleName() +" dropdown-menu dropdown-navbar\">");

			sb.append(      "<li>");
			for (ComponentHtml c : compList) {
				sb.append(c.getComponente());
			}
			sb.append(      "</li>");

			sb.append(      "</ul>");
			sb.append(     "</li>");
			sb.append(    "</ul>");
			sb.append(   "</li>");
		}
		return sb.toString();
	}
	private String createHtmlMenu5(Double menuInt, List<Bean> taskList) throws Exception {
		StringBuilder sb = new StringBuilder();

		// varre tudo para ver o que tem  :)
		if ((taskList != null) && (taskList.size() > 0)) {
			for (Bean b : taskList) {
				String grupo = b.getAttribute("grupo");
				String descricao = b.getAttribute("descricao");
				String url = b.getAttribute("url");
				String window = b.getAttribute("window");
				CssIconEnum iconMenu = b.getAttribute("icon");
				CssColorEnum iconColor = b.getAttribute("color");
				CssAnimationEnum iconAnimation = b.getAttribute("animation");
				Double menu = b.getAttribute("menu");
				if (menu.equals(menuInt) == false) continue;
				//				if (grupo.equals(grupoOld) == false) {
				//					if (grupoOld != null) {
				//						sb.append(   "<li class=\"divider\"></li>");
				//					}
				//				}

				if (TIPO.equals("sicoob")) {
					sb.append("<li class=\"green\">");
				} else {
					sb.append("<li class=\"light-blue\" title=\""+descricao+"\">");

				}

				if (url != null) {
					sb.append("<a href=\""+ url +"\">");
					if (iconMenu != null) {
						sb.append("<i class=\""+ iconMenu +" "+ iconColor + iconAnimation +"\"></i>");
					}
				} else {
					sb.append("<a href=\"#\" class=\""+ window +"\">");
					if (iconMenu != null) sb.append("<i class=\""+ iconMenu +" "+ iconColor+ iconAnimation +"\"></i>");
				}

				sb.append("&nbsp;");
				sb.append( grupo);
				sb.append("</a>");
				sb.append( "</li>");
			}
		}

		return sb.toString();
	}
	private Html createHtmlMenu3(List<Bean> taskList) throws Exception {
		StringBuilder sb = new StringBuilder();

		// varre tudo para ver o que tem  :)
		Double menuInt = Menu.LEFT;
		if ((taskList != null) && (taskList.size() > 0)) {
			String scrollMenu = "";
			Boolean menufixo = PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_MENUFIXO);
			if (menufixo == true) {
				scrollMenu = "sidebar-fixed sidebar-scroll";
			}

			//
			// INICIO
			//
			sb.append("<div id=\"sidebar\" class=\"sidebar responsive ace-save-state "+scrollMenu+"\" data-sidebar=\"true\" data-sidebar-scroll=\"true\">" + ResultHtml.PL);

			sb.append("<div class=\"sidebar-shortcuts\" id=\"sidebar-shortcuts\">" + ResultHtml.PL);
			sb.append( "<div class=\"sidebar-shortcuts-large\" id=\"sidebar-shortcuts-large\">" + ResultHtml.PL);
			sb.append(  "<input type=\"text\" placeholder=\"Pesquisar...\" class=\"input-sm nav-search-input\" id=\"search-menu\" autocomplete=\"off\">" + ResultHtml.PL);
			sb.append(  "</div>" + ResultHtml.PL);

			if(menufixo.equals(true) == false) {
				sb.append( "<div class=\"sidebar-toggle sidebar-collapse\" style=\"z-index: 1;\">" + ResultHtml.PL);
				sb.append(  "<i id=\"sidebar-toggle-icon\" class=\"ace-save-state ace-icon fa fa-angle-double-right\" data-icon1=\"ace-icon fa fa-angle-double-left\" data-icon2=\"ace-icon fa fa-angle-double-right\"></i>" + ResultHtml.PL);
				sb.append( "</div>" + ResultHtml.PL);
			}			
			sb.append( "<div class=\"sidebar-shortcuts-mini bigger-110\" id=\"sidebar-shortcuts-mini\">" + ResultHtml.PL);
			sb.append(   "<span class=\"btn btn-link bigger-160\">" + ResultHtml.PL);
			sb.append(     "<i class=\"ace-icon fa fa-search bigger-100 grey\"></i>" + ResultHtml.PL);
			sb.append(   "</span>" + ResultHtml.PL);
			sb.append( "</div>" + ResultHtml.PL);
			sb.append("</div>" + ResultHtml.PL);

			// menu quando diminui a tela
			sb.append( "<ul class=\"nav nav-list\">" + ResultHtml.PL);

			//
			// ITENS
			//
			
			for (Bean b : taskList) {
				boolean tarefaAtivo = b.getAttribute("tarefaAtivo");
				String grupo = b.getAttribute("Grupo");
				String descricao = b.getAttribute("Descricao");
				String url = b.getAttribute("url");
				CssIconEnum icon = b.getAttribute("icon");
				String icone = "";
				if(icon != null){
					icone = icon.getDescricao().replace("ace-icon", "menu-icon");
				}
				Double menu = b.getAttribute("Menu");
				if (menu.equals(menuInt) == false) continue;
				if (StringUtils.hasValue(grupo)) continue;
				String tarefaAtivoCss = "";
				if (tarefaAtivo == true) tarefaAtivoCss = "active";

				if (ResultHtml.DEBUG == true) sb.append("<!-- part 1 -->" + ResultHtml.PL);

				if (StringUtils.hasValue(tarefaAtivoCss)) {
					sb.append("<li class=\""+ tarefaAtivoCss +"\">" + ResultHtml.PL);
				} else {
					sb.append("<li>" + ResultHtml.PL);
				}
				sb.append( "<a href=\""+ url +"\">" + ResultHtml.PL);
				sb.append(  "<i class=\""+icone+"\"></i>" + ResultHtml.PL);
				sb.append(   "<span class=\"menu-text\">"+descricao +"</span>"+ ResultHtml.PL);
				sb.append( "</a>" + ResultHtml.PL);
				sb.append( "<b class=\"arrow\"></b>" + ResultHtml.PL);
				sb.append("</li>" + ResultHtml.PL);
			}

			
			
			String grupoOld = null;
			for (Bean b : taskList) {
				Boolean grupoAtivo = b.getAttribute("grupoAtivo");
				Boolean tarefaAtivo = b.getAttribute("tarefaAtivo");
				String grupo = b.getAttribute("grupo");
				String descricao = b.getAttribute("descricao");
				String url = b.getAttribute("url");
				Double menu = b.getAttribute("menu");
				if (menu.equals(menuInt) == false) continue;
				if (StringUtils.hasValue(grupo) == false) continue;
				String grupoAtivoCss = "open";
				if (grupoAtivo == true) grupoAtivoCss = "active open";
				String tarefaAtivoCss = "";
				if (tarefaAtivo == true) tarefaAtivoCss = "active";
				CssIconEnum icon = b.getAttribute("icon");
				String icone = "menu-icon fa fa-caret-right";
				if(icon != null){
					icone = icon.getDescricao().replace("ace-icon", "menu-icon");
				}
				if (grupo.equals(grupoOld) == false) {

					if (grupoOld != null) {
						sb.append("</ul>" + ResultHtml.PL);
						sb.append("</li>" + ResultHtml.PL);
					}

					if (ResultHtml.DEBUG == true) sb.append("<!-- grupo -->" + ResultHtml.PL);
					if (StringUtils.hasValue(grupoAtivoCss)) {
						sb.append("<li class=\""+ grupoAtivoCss +"\">" + ResultHtml.PL);
					} else {
						sb.append("<li>" + ResultHtml.PL);
					}

					sb.append( "<a href=\"#\" class=\"dropdown-toggle\">" + ResultHtml.PL);
					sb.append( "<span class=\"badge badge-success\">"+ grupo.substring(0,3).toUpperCase() +"</span>" + ResultHtml.PL);
					sb.append(   "<span class=\"menu-text\"> "+ grupo +" </span>" + ResultHtml.PL);
					sb.append(   "<b class=\"arrow fa fa-angle-down\"></b>" + ResultHtml.PL);
					sb.append( "</a>" + ResultHtml.PL);
					sb.append( "<b class=\"arrow\"></b>" + ResultHtml.PL);
					sb.append( "<ul class=\"submenu\">" + ResultHtml.PL);
				}

				if (ResultHtml.DEBUG == true) sb.append("<!-- item -->" + ResultHtml.PL);
				if (StringUtils.hasValue(tarefaAtivoCss)) {
					sb.append("<li class=\""+ tarefaAtivoCss +"\">" + ResultHtml.PL);
				} else {
					sb.append("<li>" + ResultHtml.PL);
				}

				sb.append(   "<a href=\""+ url +"\">" + ResultHtml.PL);
				sb.append(    "<i class=\""+icone+"\"></i>" + ResultHtml.PL);
				sb.append(    descricao + ResultHtml.PL);
				sb.append(   "</a>" + ResultHtml.PL);
				sb.append(   "<b class=\"arrow\"></b>" + ResultHtml.PL);
				sb.append(  "</li>" + ResultHtml.PL);

				grupoOld = grupo;
			}
			sb.append( "</ul>" + ResultHtml.PL);

			//
			// TERMINO
			//

			sb.append( "</ul>" + ResultHtml.PL); // <!-- /.nav-list -->
			sb.append( "<div class=\"sidebar-toggle sidebar-collapse\" style=\"z-index: 1;\">" + ResultHtml.PL);
			sb.append(  "<i id=\"sidebar-toggle-icon\" class=\"ace-save-state ace-icon fa fa-angle-double-right\" data-icon1=\"ace-icon fa fa-angle-double-left\" data-icon2=\"ace-icon fa fa-angle-double-right\"></i>" + ResultHtml.PL);
			sb.append( "</div>" + ResultHtml.PL);

			sb.append("</div>" + ResultHtml.PL);
		}
		return new Html(sb.toString());
	}


	public static String criarHtmlRedirecionarLogin() {
		String link = PropertiesUtils.getString(PropertiesConstants.SYSTEM_LINK_LOGIN);

		StringBuilder sb = new StringBuilder();
		sb.append("<html>" + ResultHtml.PL);
		sb.append("<head><META http-equiv=\"refresh\" content=\"1;URL="+link+"\"></head>" + ResultHtml.PL);
		sb.append("<body bgcolor=\"#ffffff\"><center>Redirecionando...</center></body>" + ResultHtml.PL);
		sb.append("</html>" + ResultHtml.PL);

		return sb.toString();		
	}

	private Map<Double, List<Bean>> getTask(UserContext userContext) throws Exception {
		Map<Double, List<Bean>> result = new HashMap<Double, List<Bean>>();



		if (classes == null) {
			classes = new ArrayList<Class<?>>();

			Map<String, Class<?>> classeFullMap = (Map<String, Class<?>>)request.getServletContext().getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_FULL_MAP);
			for (String key : classeFullMap.keySet()) {
				Class<?> clazz = classeFullMap.get(key);
				Menu m = (Menu)clazz.getAnnotation(Menu.class);
				if (m == null) continue;

				classes.add(clazz);
			}
		}

		for (Class<?> clazz : classes) {
			try {
				Menu m = (Menu)clazz.getDeclaredAnnotation(Menu.class);
				if (result.get(m.inMenu()) == null) {
					result.put(m.inMenu(), new ArrayList<Bean>());
				}
			}catch(NullPointerException e) {
				System.out.println("Classe " + clazz.getName() + " está sem Menu ou com a Annotation errada.");
			}
		}
		if (result.get(Menu.LEFT) == null)
			result.put(Menu.LEFT, new ArrayList<Bean>());


		List<String> groups = new ArrayList<String>();
		
		boolean addMaisAcessados = false;
		int qtdTaskAllow = 0;
		for (Class<?> clazz : classes) {
			Menu m = (Menu)clazz.getAnnotation(Menu.class);
			boolean temAcesso = AbstractDispacher.temAcesso(clazz, userContext);
			if (m.inMenu() != Menu.LEFT) continue;
			if (temAcesso == false) continue;
			
			qtdTaskAllow++;
		}
		if (qtdTaskAllow > 25) {
			addMaisAcessados = true;
		}
		
		if (addMaisAcessados == true) {
			groups.add("Mais Acessados");
		}
		
		for (Class<?> clazz : classes) {
			Menu m = (Menu)clazz.getAnnotation(Menu.class);
			if (groups.contains(m.group()) == true) continue;

			boolean temAcesso = AbstractDispacher.temAcesso(clazz, userContext);
			if (temAcesso == false) continue;
			
			groups.add( m.group() );
		}


		// colocar os XML
		Map<String, DotumSmartReport> classeFullMap = ComponentHelper.getCacheDotumSmartReport(request);
		for (String key : classeFullMap.keySet()) {
			DotumSmartReport report = classeFullMap.get(key);
			String group = report.getMenu().getGroup();
			if (group == null) continue;
			if (userContext == null) continue;
			if (groups.contains(group) == true) continue;

			boolean temAcesso = userContext.getUser().hasAccessTaskByCode(key);
			if (temAcesso == false) continue;


			groups.add( group );
		}


		Class<?> taskClass = (userContext != null ? userContext.getUser().getCurrentTask() : null);
		List<Class<?>> addedList = new ArrayList<Class<?>>();
		List<Class<?>> maisAcessadosList = new ArrayList<Class<?>>();
		for (String group : groups) {
			for (Class<?> clazz : classes) {
				Menu m = (Menu)clazz.getAnnotation(Menu.class);
				String descricao = m.label();
				
				boolean maisAcessado = false;
				if(group.equals("Mais Acessados") && (addMaisAcessados == true)) {
					List<SecurityTaskBean> taskList = userContext != null ? userContext.getUser().getTasks() : new ArrayList<SecurityTaskBean>();
					for(SecurityTaskBean taskBean : taskList) {
						
						if((ClassUtils.getCodeTask(clazz).equals(taskBean.getCodigo()) == false) || 
								(taskBean.get("group") != null && taskBean.get("group").equals("Mais Acessados") == false)) continue;
						
						maisAcessado = true;
						descricao = m.group() + " > " + m.label();
						maisAcessadosList.add(clazz);
					}
				}
				
				if (group.equals(m.group()) == false && maisAcessado == false) continue;

				boolean temAcesso = AbstractDispacher.temAcesso(clazz, userContext);
				if (temAcesso == false) continue;
				
				if ((addedList.contains(clazz) == true) && (maisAcessadosList.contains(clazz) == false)) continue;
				addedList.add(clazz);
				boolean active = taskClass != null && taskClass.equals(clazz);

				String url = null;
				String window = null;


				String classStr = ClassUtils.getCodeTask(clazz);
				if (clazz.getSuperclass().equals(AbstractView.class)) {
					UrlHtml linkUrl = new UrlHtml();
					linkUrl.addParam(FWConstants.PARAM_FW_CLASS, classStr);
					url = linkUrl.getComponente();
					window = null;
				} else if (clazz.getSuperclass().equals(AbstractReport.class)) {
					UrlHtml linkUrl = new UrlHtml();
					linkUrl.addParam(FWConstants.PARAM_FW_CLASS, classStr);
					url = linkUrl.getComponente();
					window = null;
				} else if (clazz.getSuperclass().equals(AbstractController.class)) {
					UrlHtml linkUrl = new UrlHtml();
					linkUrl.addParam(FWConstants.PARAM_FW_CLASS, classStr);
					url = linkUrl.getComponente();
					window = null;
				} else if (clazz.getSuperclass().equals(AbstractAction.class)) {
					url = null;
					window = clazz.getSimpleName();
				} else if (StringUtils.hasValue(m.link())) {
					url = m.link();
					window = null;
				}

				Double inMenu = m.inMenu();

				try {
					Bean tar = new Bean();
					tar.setAttribute("grupoAtivo", false);
					tar.setAttribute("tarefaAtivo", active);
					tar.setAttribute("icon", m.icon());
					tar.setAttribute("color", m.color());
					tar.setAttribute("animation", m.animation());
					tar.setAttribute("grupo", group);
					tar.setAttribute("descricao", descricao);
					tar.setAttribute("menu", inMenu);
					tar.setAttribute("url", url);
					tar.setAttribute("window", window);
					tar.setAttribute("classe", clazz);

					if (userContext.getUser().isMenuAtivo(inMenu))
						result.get(m.inMenu()).add(tar);
				} catch (IncompleteAnnotationException e) {
					throw new DeveloperException(e.getMessage() + " of class " + clazz.getName(), e);
				}
			} // aqui termina o dos class


			for (String key : classeFullMap.keySet()) {
				DotumSmartReport report = classeFullMap.get(key);
				String g = report.getMenu().getGroup();
				if (g == null) continue;
				if (group.equals(g) == false) continue;
				if (userContext == null) continue;

				boolean temAcesso = userContext.getUser().hasAccessTaskByCode(key);
				if (temAcesso == false) continue;

				boolean active = false;



				UrlHtml linkUrl = new UrlHtml();
				//				linkUrl.addParam(FWConstants.PARAM_FW_CLASS, Controller010Relatorio.class);
				linkUrl.addParam(FWConstants.PARAM_FW_CLASS, key);
				String url = linkUrl.getComponente();
				String window = null;
				Double inMenu = report.getMenu().getPosition();				

				Bean tar = new Bean();
				tar.setAttribute("grupoAtivo", false);
				tar.setAttribute("tarefaAtivo", active);
				tar.setAttribute("icon", CssIconEnum.PRINT);
				tar.setAttribute("color", CssColorEnum.BLUE);
				tar.setAttribute("animation", null);
				tar.setAttribute("grupo", group);
				tar.setAttribute("subGrupo", report.getMenu().getSubGroup());
				tar.setAttribute("descricao", report.getTitle());
				tar.setAttribute("menu", report.getMenu().getPosition());
				tar.setAttribute("url", url);
				tar.setAttribute("window", window);
				tar.setAttribute("classe", null);

				System.out.println("inMenu =>"+inMenu);
				System.out.println("tar =>"+tar.get("descricao"));
				System.out.println("getPosition =>"+report.getMenu().getPosition());
				System.out.println("result.get(inMenu) =>"+result.get(inMenu));

				for (Double db : result.keySet()) {
					List<Bean> beans = result.get(db);
					for (Bean b: beans) {
						System.out.print ( "descricao: "+b.get("descricao"));
						System.out.println ( "/menu: "+b.get("menu"));
					}
				}

				if (userContext.getUser().isMenuAtivo(inMenu))
					result.get(inMenu).add(tar);
			}
		}

		
		//		for (Double d : result.keySet()) {
		//			for (Bean b : result.get(d)) {
		//			
		//			boolean active = b.get("tarefaAtivo");
		//			String grupo = b.get("grupo");
		//			if (active == true) {
		//				for (Bean b2 :result) {
		//					String grupo2 = b2.get("grupo");
		//					if (grupo.equals(grupo2)) {
		//						b2.set("grupoAtivo", true);
		//					}
		//				}
		//				break;
		//			}
		//		}

		return result;
	}
}

