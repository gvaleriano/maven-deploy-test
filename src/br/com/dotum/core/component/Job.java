package br.com.dotum.core.component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Job {
	

	public String label();
	
	/**# Example of job definition:<br />
	 * # .---------------- minute (0 - 59)<br />
	 * # |  .------------- hour (0 - 23)<br />
	 * # |  |  .---------- day of month (1 - 31)<br />
	 * # |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...<br />
	 * # |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat<br />
	 * # |  |  |  |  |<br />
	 * # *  *  *  *  * user-name  command to be executed<br />
	 */
	public String frequency();
	
}

