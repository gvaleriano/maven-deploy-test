package br.com.dotum.core.component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.dotum.core.enuns.CssAnimationEnum;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Menu {
	

	public static final double TOP              = 1.0;
	
	@Deprecated
	/**
	 * Usar a constante Menu.POSITION_2_0_USER
	 */
	public static final double USER          = 2;
	
	
	public static final double POSITION_2_0_USER = 2.0;
	public static final double POSITION_2_1_TASKS = 2.1;
	public static final double POSITION_2_2_ENVELOPE = 2.2;
	public static final double POSITION_2_3_EXCLAMATION_TRIANGLE  = 2.3;
	public static final double LEFT          = 3.0;
	public static final double COMP_TOP_4_0  = 4.0;
	public static final double COMP_TOP_4_1  = 4.1;
	public static final double COMP_TOP_4_2  = 4.2;
	public static final double COMP_TOP_4_3  = 4.3;
	public static final double COMP_TOP_4_4  = 4.4;
	public static final double COMP_TOP_4_5  = 4.5;
	public static final double COMP_TOP_4_6  = 4.6;
	public static final double COMP_TOP_4_7  = 4.7;
	public static final double COMP_TOP_4_8  = 4.8;
	public static final double COMP_ALERT    = 4.9;
	
	public static final double COMP_TOP_5_0  = 5.0;
	public static final double COMP_TOP_5_1  = 5.1;
	public static final double COMP_TOP_5_2  = 5.2;
	public static final double COMP_TOP_5_3  = 5.3;

	@Deprecated
	/**
	 * Usar a constante Menu.ZERO
	 */
	public static final int NENHUM       = 0;
	@Deprecated
	/**
	 * Usar a constante Menu.ONE
	 */
	public static final int UM           = 1;

	
	public static final int ZERO         = 0;
	public static final int ONE          = 1;
	public static final int MANY         = 2;

	
	public String group() default "";
	public String label();
	public String link() default "";
	public boolean publico() default false;
	public boolean window() default true;
	public int windowSize() default 10;
	
	/**
	 * Usar esta opção apenas quando desejar colocar a View/Action em um dos menus;<br />
	 * <br />
	 * 1 no topo superior direito<br />
	 * 2 no topo superior esquerdo (menu do usuario)<br />
	 * 3 na lateral esquerda<br />
	 * 4.1, 4.2, 4.3, 4.4: no topo superior direito (Ao lado do menu do usuario)<br />
	 * <br />
	 * <br />
	 * PS: Caso esteja utilizando este item em uma ação a mesma não irá aparecer nas ações da view!
	 * @return
	 */
	public double inMenu() default -1.0;
	
	public CssIconEnum icon() default CssIconEnum.STAR;
	public CssColorEnum color() default CssColorEnum.BLUE;
	public CssAnimationEnum animation() default CssAnimationEnum.NONE;
	public int selecteds() default ZERO;
	boolean notification() default false;
	
	/**
	 * Este recurso irá permitir montar uma tela dinamica
	 * os componentes não serão carregados ao abrir o ScreenView e congelados no HTML.
	 * Será montado ao ser chamada a ação, desta forma é possivel usar o ID do registro Master
	 * e personalizar a tela (formulario, campos diferentes para cada registro) conforme seu valor.
	 * carregar.<br>
	 * <br>
	 * Use isto com moderação por que usa muito pacote de dados de usuario (no celular);<br>
	 * 
	 * @return
	 */
	boolean dynamic() default false;
	
}

