package br.com.dotum.core.component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Task {
	
	public String[] value() default "";

	public static final String DEV_CODE = "FWD"; // apenas no momento de desenvolvimento
	public static final String ADMIN_CODE = "FWA"; // apenas administrador
	public static final String USER_CODE = "FWU"; // todos os usuarios terão acesso 
	
}

