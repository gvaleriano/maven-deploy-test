package br.com.dotum.core.component.basket;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Task;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.MessageHtml.Type;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.UserContext;

@Menu(label="Adicionar", group="Cesta", icon=CssIconEnum.PLUS, selecteds=Menu.MANY)
@Task(Task.USER_CODE)
public class Action001AdicionarNaCesta extends AbstractAction {

	public void doScreen() throws Exception {
		MessageHtml msg = new MessageHtml(Type.WARNING);
		msg.addContent("Adicionar na cesta?");

		FormHtml form = new FormHtml("doData");
		form.addButton("doProcess", "Sim");
		
		super.addBody(msg);
		super.addBody(form);
	}

	public void doProcess(Integer ... id) throws Exception {
		UserContext userContext = super.getUserContext();
		TransactionDB trans = super.getTransaction();

		List<Integer> cestaList = super.getObjectSessionScreen("cesta");
		if (cestaList == null) cestaList = new ArrayList<Integer>();
		
		int count = 0;
		for (int i = 0; i < id.length; i++) {
			if (cestaList.contains(id[i])) continue;
			
			cestaList.add(id[i]);
			
			count++;
		}
		
		super.setObjectSessionScreen("cesta", cestaList);
		
		super.setMessageSuccess("Adicionado "+ count +" registros na cesta com sucesso!");
		
	}
}
