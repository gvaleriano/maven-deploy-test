package br.com.dotum.core.component.basket;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Task;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.MessageHtml.Type;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.UserContext;

@Menu(label="Limpar a cesta", group="Cesta", icon=CssIconEnum.REFRESH, selecteds=Menu.ZERO)
@Task(Task.USER_CODE)
public class Action003LimparCesta extends AbstractAction {

	public void doScreen() throws Exception {
		MessageHtml msg = new MessageHtml(Type.WARNING);
		msg.addContent("Adicionar na cesta?");

		FormHtml form = new FormHtml("doData");
		form.addButton("doProcess", "Sim");
		
		super.addBody(msg);
		super.addBody(form);
	}

	public void doProcess(Integer ... id) throws Exception {
		UserContext userContext = super.getUserContext();
		TransactionDB trans = super.getTransaction();

		List<Integer> cestaList = super.getObjectSessionScreen("cesta");
		if (cestaList == null) cestaList = new ArrayList<Integer>();
		super.setObjectSessionScreen("cesta", cestaList);
		
		super.setMessageSuccess("Cesta limpa com sucesso!");
		
	}
}
