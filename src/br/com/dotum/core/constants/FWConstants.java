package br.com.dotum.core.constants;

public class FWConstants {


	public static final String VIEW_TAB_QTD = "VIEW_TAB_QTD";
	public static final String VIEW_TIME_PERFORMANCE = ".nav-search";
	
	public static final String PACK_CLASSE_DEFAULT = "br.com.dotum.core";
	public static final String PATH_RELEASE = "release";


	public static final String CLASS_VIEW = "ScreenView";
	public static final String TAB_CESTA = "_TAB_CESTA_";

	public static final String URL_DO = "do";
	public static final String URL_JSON = "json";
	public static final String URL_TOKEN = "fwtc";

	public static final String PARAM_FW_ID = "id";
	public static final String PARAM_FW_MASTER_ID = "_fkmid";
	public static final String PARAM_FW_SLAVE_ID = "id";

	public static final String PARAM_FW_CLASS_OLD = "fk_name";
	public static final String PARAM_FW_CLASS = "_fkc";
	public static final String PARAM_FW_REPORT_CODE = "_fkr";
	
	public static final String PARAM_FW_TOKEN_AUTHORIZATION = "Authorization";
	public static final String PARAM_FW_TOKEN_TYPE = "Bearer ";
	
	
	public static final String PARAM_FW_METHOD = "_fkm";
	public static final String PARAM_FW_METHOD_OLD = "fk_action";

	public static final String PARAM_HAS_SAME_VALUE_DB = "hasSameValueDB";
	public static final String PARAM_DISPLAY = "display";
	public static final String PARAM_TERM = "term";
	public static final String PARAM_BEAN_CLASS = "beanClass";
	public static final String PARAM_JSON_CLASS = "jsonClass";

	
	
	public static final String PARAM_FW_FILTER_FIELD_PREFIX = "fk_filterfieldproperty";
	public static final String PARAM_FW_FILTER_ABA_PROPERTY = "fk_filterabaproperty";
	public static final String PARAM_FW_FILTER_ABA_VALUE = "fk_filterabavalue";
	public static final String PARAM_FW_FILTER_CLEAN = "doFilterClean";


	public static final String ATTRIBUTE_SESSION_CACHE_FULL_MAP = "_fwSessionCacheFullMap";
	public static final String ATTRIBUTE_SESSION_CACHE_VIEW_MAP = "_fwSessionCacheViewMap";
	public static final String ATTRIBUTE_SESSION_CACHE_ACTION_MAP = "_fwSessionCacheActionMap";
	public static final String ATTRIBUTE_SESSION_CACHE_JOB_MAP = "_fwSessionCacheJobMap";
	public static final String ATTRIBUTE_SESSION_CACHE_CONTROLLER_MAP = "_fwSessionCacheControllerMap";
	public static final String ATTRIBUTE_SESSION_CACHE_JASPERREPORT_MAP = "_fwSessionCacheJasperReportMap";
	public static final String ATTRIBUTE_SESSION_CACHE_SMARTREPORT_MAP = "_fwSessionCacheSmartReportMap";
	public static final String ATTRIBUTE_SESSION_CACHE_SMARTMANAGER_MAP = "_fwSessionCacheSmartManagerMap";

	public static final String PARAM_FW_USER_LOGGED = "fw_user_logged";
	public static final String PARAM_LOGIN = "login";
	public static final String PARAM_USUARIO_LOGADO = "_fwSessionUsuarioLogadoQueVaiMorrerEVirarUmToken";
	public static final String PARAM_USUARIO = "usuario";
	public static final String PARAM_SENHA = "senha";
	public static final String PARAM_WINDOW = "openWindow";


	// Diversos
	public static final String SIM_STR = "sim";
	public static final String NAO_STR = "nao";
	public static final int NAO = 0;
	public static final int SIM = 1;

	public static final int INATIVO = 0;
	public static final int ATIVO = 1;
	public static final String INATIVO_STR = "Inativo";
	public static final String ATIVO_STR = "Ativo";

	public static final String DEBITO = "D";
	public static final String CREDITO = "C";

	public static final int DEBITO_INT = -1;
	public static final int CREDITO_INT = 1;

	public static final String ENTRADA_STR = "entrada";
	public static final String SAIDA_STR = "saida";
	public static final String ENTRADA = "E";
	public static final String SAIDA = "S";

	//DATE
	public static final String AUTO = "top left";
	public static final String TOP_AUTO = "top auto";
	public static final String BOTTOM_AUTO = "bottom auto";
	public static final String AUTO_LEFT = "auto left";
	public static final String BOTTOM_LEFT = "bottom left";
	public static final String AUTO_RIGHT = "auto right";
	public static final String TOP_RIGHT = "top right";
	public static final String BOTTOM_RIGHT = "bottom right";

	//JSON PARAM
	public static final String JSON_ATTR_TYPE = "type";
	public static final String JSON_ATTR_STATUS = "status";
	public static final String JSON_ATTR_TYPE_SUCCESS = "success";
	public static final String JSON_ATTR_TYPE_WARNING = "warning";
	public static final String JSON_ATTR_TYPE_ERROR = "error";

	public static final String JSON_ATTR_DATA = "data";
	public static final String JSON_ATTR_MESSAGE = "message";
	public static final String JSON_ATTR_REPORT = "report";
	public static final String JSON_ATTR_HTML = "html";
	public static final String JSON_ATTR_SIZE = "size";
	public static final String JSON_ATTR_FILENAME = "fileName";
	public static final String JSON_ATTR_CONTENTTYPE = "contentType";

	
	public static final String TOMCAT_PATH_CLASSES = "/WEB-INF/classes/";
}

