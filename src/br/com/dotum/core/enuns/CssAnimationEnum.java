package br.com.dotum.core.enuns;

public enum CssAnimationEnum {

	NONE(""),
    BELL(" icon-animated-bell"),
    VERTICAL(" icon-animated-vertical"),
    HAND(" icon-animated-hand-pointer"),
    WRENCH(" icon-animated-wrench"),
    ;
	
    
    private String descricao;
            
    CssAnimationEnum(String descricao) {
        this.descricao = descricao;
    }
    
    public String getDescricao() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
	@Override
	public String toString() {
		return getDescricao();
	}
}
