package br.com.dotum.core.enuns;

public enum CssColorEnum {

	
	@Deprecated
    BTN_BOX_GREEN("btn-success"),
    @Deprecated
	BTN_BOX_BLUE("btn-primary"),
    @Deprecated
    BTN_BOX_BLUE_LIGHT("btn-info"),
    @Deprecated
    BTN_BOX_ORANGE("btn-warning"),
    @Deprecated
    BTN_BOX_RED("btn-danger"),
    @Deprecated
    BTN_BOX_PURPLE("btn-purple"),
    @Deprecated
    BTN_BOX_PINK("btn-pink"),
    @Deprecated
    BTN_BOX_GREY("btn-grey"),
    @Deprecated
    BTN_BOX_BLACK("btn-inverse"),
    @Deprecated
    BTN_BOX_GREY_LIGHT("btn-light"),
    @Deprecated
    BTN_ICON_GREEN("btn-link green"),
    @Deprecated
    BTN_ICON_BLUE("btn-link blue"),
    @Deprecated
    BTN_ICON_BLUE_LIGHT("btn-link blue light"),
    @Deprecated
    BTN_ICON_RED("btn-link red"),
    @Deprecated
    BTN_ICON_YELLOW("btn-link orange2"),
    @Deprecated
    BTN_ICON_ORANGE("btn-link orange"),
    @Deprecated
    BTN_ICON_BROWN("btn-link black"),
    @Deprecated
    BTN_ICON_DARK("btn-link dark"),

    GREEN("btn-success green"),
    BLUE("btn-primary blue"),
    ORANGE("btn-warning orange"),
    YELLOW("btn-yellow orange2"),
    PURPLE("btn-purple purple"),
    RED("btn-danger red"),
    WHITE("btn-white"),
    BLACK("btn-inverse dark"),
    PINK("btn-pink pink"),
    ;
	
    private String descricao;
            
    CssColorEnum(String descricao) {
        this.descricao = descricao;
    }
    
    public String getDescricao() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
	@Override
	public String toString() {
		return getDescricao();
	}
}
