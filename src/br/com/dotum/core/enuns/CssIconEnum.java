package br.com.dotum.core.enuns;

//https://fontawesome.com/v4.7.0/icons/
public enum CssIconEnum {

	ALTERAR("ace-icon fa fa-pencil"),
	APAGAR("ace-icon fa fa-remove"),
	ERASER("ace-icon fa fa-eraser"),
	CONCLUIR("ace-icon fa fa-check"),
	DETALHAR("ace-icon fa fa-list"),
	INSERIR("ace-icon fa fa-pencil-square-o"),
	RELATORIO("ace-icon fa fa-print"),
	SALVAR("ace-icon fa fa-save"),
	LOCK("ace-icon fa fa-lock"),
	UNLOCK("ace-icon fa fa-unlock"),
	PERSON("ace-icon fa fa-user"),
	INBOX("ace-icon fa fa-inbox"),
	MALE("ace-icon fa fa-male"),
	BUILDING("ace-icon fa fa-building-o"),
	CLOCK("ace-icon fa fa-clock-o"),
	PHONE("ace-icon fa fa-phone"),
	EYE("ace-icon fa fa-eye"),
	ENVELOPE("ace-icon fa fa-envelope"),
	ENVELOPE_O("ace-icon fa fa-envelope-o"),
	HANDSHAKE("ace-icon fa fa-handshake-o"),
	MOBILE("ace-icon fa fa-mobile"),
	EXCLAMATION_TRIANGLE("ace-icon fa fa-exclamation-triangle"),
	PAPERCLIP("ace-icon fa fa-paperclip"),
	MAPMARKER("ace-icon fa fa-map-marker"),
	MAP_O("ace-icon fa fa-map-o"),
	ARROWS("ace-icon fa fa-arrows"),
	ARROWS_ALT("ace-icon fa fa-arrows-alt"),
	SEARCH("ace-icon fa fa-search"),

	ASTERISK("ace-icon fa fa-asterisk"),
	BELL("ace-icon fa fa-bell"),
	BOOK("ace-icon fa fa-book"),
	BAN_CIRCLE("ace-icon fa fa-ban"),
	CIRCLE("ace-icon fa fa-circle"),
	CALENDAR("ace-icon fa fa-calendar"),
	CALENDAR_CHECK("ace-icon fa fa-calendar-check-o"),
	CALCULATOR("ace-icon fa fa-calculator"),
	COGS("ace-icon fa fa-cogs"),
	FACEBOOK("ace-icon fa fa-facebook-square"),
	COG("ace-icon fa fa-cog"),
	CREDIT_CARD("ace-icon fa fa-credit-card"),
	COFFEE("ace-icon fa fa-coffee"),
	DOLLAR("ace-icon fa fa-dollar"), 
	CLOUD("ace-icon fa fa-cloud"), 
	DOWNLOAD("ace-icon fa fa-download"),
	CLOUD_DOWNLOAD("ace-icon fa fa-cloud-download"),
	UPLOAD("ace-icon fa fa-cloud-upload"),
	HEART("ace-icon fa fa-heart"),
	HOME("ace-icon fa fa-home"),
	DASHBOARD("ace-icon fa fa-tachometer"),
	REFRESH("ace-icon fa fa-refresh"),
	STAR("ace-icon fa fa-star"),
	SPINNER("ace-icon fa fa-spinner"),
	COMMENTS("ace-icon fa fa-comments"),
	DESKTOP("ace-icon fa fa-desktop"),
	TRASH("ace-icon fa fa-trash"),
	TAG("ace-icon fa fa-tag"),
	BOLT("ace-icon fa fa-bolt"),
	TASKS("ace-icon fa fa-tasks"),
	POWER_OFF("ace-icon fa fa-power-off"),
	USER("ace-icon fa fa-user"),
	USERS("ace-icon fa fa-users"),
	USER_PLUS("ace-icon fa fa-user-plus"),
	CUBES("ace-icon fa fa-cubes"),
	MAPSINGS("ace-icon fa fa-map-signs"),
	CARET_DOWN("ace-icon fa fa-caret-down"),
	CARET_SQUARE_DOWN("ace-icon fa fa-caret-square-o-down"),
	CARET_RIGHT("ace-icon fa fa-caret-right"),
	COMMENT("ace-icon fa fa-comment"),
	PAUSE("ace-icon fa fa-pause-circle-o"),
	PLAY("ace-icon fa fa-play-circle-o"),
	SHARE("ace-icon fa fa-share"),
	SHARE_LEFT("ace-icon fa fa-share fa-flip-horizontal"),
	BALL("ace-icon fa fa-futbol-o"),
	PLUS("ace-icon fa fa-plus"),
	PRINT("ace-icon fa fa-print"),
	CANCELAR("ace-icon fa fa-spinner"),
	FILE("ace-icon fa fa-file-text-o"),
	EXCHANGE("ace-icon fa fa-exchange"),
	KEY("ace-icon fa fa-key"),
	YOUTUBE("ace-icon fa fa-youtube"),
	TWITTER("ace-icon fa fa-twitter-square"),
	ARROW_RIGHT("ace-icon fa fa-arrow-right"),
	ARROW_LEFT("ace-icon fa fa-arrow-left"),
	ARROW_DOWN("ace-icon fa fa-arrow-down"),
	ARROW_UP("ace-icon fa fa-arrow-up"),
	BRIEFCASE("ace-icon fa fa-briefcase"),
	TICKET("ace-icon fa fa-ticket"),
	CHECK("ace-icon fa fa-check"),
	CHECK_EMPTY("ace-icon fa fa-check-sign"),
	SHOPPING_CART_RIGHT("ace-icon fa fa-shopping-cart"),
	SHOPPING_CART_LEFT("ace-icon fa fa-shopping-cart fa-flip-horizontal"),
	CART_PLUS("ace-icon fa fa-cart-plus"),
	FILTER("ace-icon fa fa-filter"),
	CAR("ace-icon fa fa-car"), 
	DROPBOX("ace-icon fa fa-dropbox"), 
	DOLLAR_SQUARE("ace-icon fa fa-university"), 
	FILE_CODE("ace-icon fa fa-file-code-o"), 
	CHECK_SQUARE("ace-icon fa fa-check-square"), 
	CHECK_SQUARE_O("ace-icon fa fa-check-square-o"), 
	GIFT("ace-icon fa fa-gift"), 
	TRUCK("ace-icon fa fa-truck"), 
	ROAD("ace-icon fa fa-road"), 

	LIST("ace-icon fa fa-list"),
	LIST_OL("ace-icon fa fa-list-ol"),
	LINE_CHART("ace-icon fa fa-line-chart"),
	PIE_CHART("ace-icon fa fa-pie-chart"),
	LINE_CHART_LEFT("ace-icon fa fa-line-chart fa-flip-horizontal"),
	BAR_CHART("ace-icon fa fa-bar-chart"),
	CIRCLE_CHECK("ace-icon fa fa-check-circle-o"),
	BOOKMARK("ace-icon fa fa-bookmark-o"),
	INTERAGIR("ace-icon fa fa-comment-o"),
	PLUSTHICK("icon-plusthick"),
	CIRCLE_ARROW_RIGHT("icon-circle-arrow-right"),
	MINUS("ace-icon fa fa-minus"),
	PENCIL("glyphicon-pencil"), 
	
	;

	private String descricao;

	CssIconEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static final String GREEN_MALE = MALE +" green";
	public static final String RED_CIRCLE = CIRCLE +" red";
	public static final String BLUE_CIRCLE = CIRCLE +" blue";
	public static final String YELLOW_CIRCLE = CIRCLE +" yellow";
}
