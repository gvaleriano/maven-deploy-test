package br.com.dotum.core.html.form;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.form.field.AutoCompleteFieldHtml;
import br.com.dotum.core.html.form.field.CheckboxFieldHtml;
import br.com.dotum.core.html.form.field.ConditionJs;
import br.com.dotum.core.html.form.field.HiddenFieldHtml;
import br.com.dotum.core.html.form.field.RadioNumberFieldHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.core.html.other.WindowHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.StringUtils;

public class ButtonHtml extends ComponentHtml {
	private static Log LOG = LogFactory.getLog(ButtonHtml.class);

	private UrlHtml url;
	private String script;
	private Class<?> classe;
	private String method;
	private boolean positionBottom = false;
	private String windows;

	private String group;

	public ButtonHtml(CssIconEnum icon, CssColorEnum cor, String method, String label) {
		if (cor == null) cor = CssColorEnum.BLUE;

		setIcon(icon);
		setColor(cor);
		this.method = method;
		setLabel(label);
	}
	public ButtonHtml(String label) {
		this(null, null, null, label);
	}
	public ButtonHtml(String method, String label) {
		this(null, null, method, label);
	}
	public ButtonHtml(CssIconEnum icon, String link) {
		this(icon, null, null, null);
		this.url = new UrlHtml();
		this.url.setLink(link);
	}

	public void setUrl(UrlHtml url) {
		this.url = url;
	}
	
	public void setGroup(String group) {
		this.group = group; 
	}
	public String getGroup() {
		return this.group; 
	}

	public void setWindown(WindowHtml win) {
		this.windows = win.getId();
	}	
	public String getComponente() throws DeveloperException {
		StringBuilder sb = new StringBuilder();


		// esse class é para o caso do grupo de botao SPLIT (aquele que tem uma seta e abre varias opçoes)

		if (url != null) {
			super.addCssClass("btn btn-sm " + getColor().getDescricao() + " " + getId() );
			super.addAttr("id", getId());
			super.addAttr("class", getCssClassComponent());
			super.addAttr("style", "margin-right:2px;");
			super.addAttr("href", url.getComponente());
			super.addAttr("data-toggle", "modal");


			sb.append("<div class=\"btn-group\">");
			//			sb.append( url.getComponente() );
			sb.append("<a ");
			sb.append( super.getAttrStr() );
			sb.append(">");
			if (getIcon() != null) {
				sb.append("<i class=\""+ getIcon().getDescricao() +"\"></i>");
			}
			if (StringUtils.hasValue(getLabel())) {
				sb.append( getLabel() );
			}
			sb.append("</a>");
			sb.append("</div>");

		} else {
			
				String cssClass = " btn-small ";
				if ((getIcon() != null) && (StringUtils.hasValue(getLabel()) == false)) {
					cssClass = "";
				}
				
				String[] part = getColor().getDescricao().split(" ").length > 1 ? getColor().getDescricao().split(" ") : new String[] {getColor().getDescricao(), null};

				if (getCssClassComponent().contains("inline")) {
					cssClass += "btn-sm";
				}
				super.addCssClass("btn " + part[0] + cssClass + " " + getId() );
				super.addAttr("id", getId());
				super.addAttr("class", getCssClassComponent());
				super.addAttr("type", "submit");
				super.addAttr("name", method);

				if (StringUtils.hasValue(windows)) {
					super.addAttr("data-toggle","modal");
					super.addAttr("href", windows);
				}

				String cssGroup = "btn-group";
				if (getCssClassComponent().contains("inline")) {
					sb.append("<div class=\"space-14\">&nbsp;</div>");
					cssGroup = "form-group";
				}
				
				sb.append("<div class=\""+cssGroup+"\">");
				
				// TODO Priscila.. quando vc descobrir para que esse cara ,,, me avise...
				// estou colocando o if method == null para evitar um monte de html indo para a tela
				// Ass. Keynes
				if (method != null)
					sb.append("<input type=\"hidden\" name=\""+ method +"\" value=\"\" />");

				sb.append("<button");
				sb.append( super.getAttrStr() );
				sb.append(">");

				if (getIcon() != null) {
					String color = part[1];
					if(getCssClassComponent().toString().contains("link") == false) {
						color = "";
					}
					if(getCssClassComponent().toString().contains("link")) {
						String descricao = getIcon().getDescricao();
						descricao = descricao.replaceAll(" white", "");
						getIcon().setDescricao(descricao);
					}
						
					sb.append("<i class=\""+ getIcon().getDescricao() + " " + color +" bigger-130 \"></i>");
				}
				if (StringUtils.hasValue(getLabel())) {
					sb.append( getLabel() );
				}
				sb.append("</button>");
				sb.append("</div>");
//			}


			if (this.classe != null) {
				HiddenFieldHtml f1 = null;
				f1 = new HiddenFieldHtml(FWConstants.PARAM_FW_CLASS, ClassUtils.getCodeTask(this.classe));
				f1.setHtml(getHtml());
				sb.append(f1.getComponente());
			}
			if (StringUtils.hasValue(this.method)) {
				HiddenFieldHtml f2 = new HiddenFieldHtml(FWConstants.PARAM_FW_METHOD, this.method);
				f2.setHtml(getHtml());
				sb.append(f2.getComponente());
			}
		}

		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}



	public static final String CLASS_ACTION = "<ACTION_CLASSE>";

	@Override
	public String getScript() {
		if (StringUtils.hasValue(script) == true) {
			return script;
		} else {
		}

		return "";
	}

	public UrlHtml getUrl() {
		return url;
	}

	public void setScript(String script) {
		this.script = script;
	}
	public void setClassAction(Class<?> classe) throws DeveloperException {
		this.classe = classe;

		if (method != null) {
			int p = ArrayUtils.search(new String[] {"doFilterField"}, method, false);
			if (p > -1) return;

			try {
				classe.getDeclaredMethod(method);
			} catch (NoSuchMethodException e) {
				try {
					classe.getDeclaredMethod(method, Integer.class);
				} catch (NoSuchMethodException e2) {
					try {
						classe.getDeclaredMethod(method, Integer[].class);
					} catch (NoSuchMethodException e3) {
						LOG.error("Não existe o metodo \"" + method + "(Integer id)\" na classe \"" + classe.getName() + "\".");
					}
				}
			}
		}
	}

	public boolean isPositionBottom() {
		return positionBottom;
	}

	public void setPositionBottom(boolean positionBottom) {
		this.positionBottom = positionBottom;
	}

	public void onClick(String script) {
		on("click", script);
	}
	public void onClick(Ajax ajax) {
		on("click", ajax.getComponent());
	}
	public void onToggle(String script1, String script2) {
		StringBuilder sb = new StringBuilder();

		sb.append( script1 );
		sb.append("},function(e){");
		sb.append( script2 );

		on("toggle", sb.toString());
	}
	public void on(String event, String script) {
		String seletor = "#";
		seletor = ".";

		StringBuilder sb = new StringBuilder();
		sb.append("$('body').on('"+ event +"', '"+ seletor +""+ getId() +"', function(e){");
		sb.append(script);
		sb.append("});");

		this.script = sb.toString();
	}

	public static String scriptAddFields(String classe) throws Exception {
		return "$(this).parents('div.widget-main:first').append( $(this).parents('form:first').prev().find('div."+ classe +"').html()  );" + ResultHtml.PL;
	}
	public static String scriptRemoveFields() {
		return "$(this).parents('div.row:first').remove();" + ResultHtml.PL;
	}
	public static String scriptOcultaGroup(AbstractFieldHtml field) {
		StringBuilder sb = new StringBuilder();
		sb.append("var _f=$('#"+field.getId()+"');");
		sb.append("var _fg=_f.parents('div.widget-main:first');");
		sb.append("var _al=_fg.find('select, input, textarea');");

		sb.append("var e = $(this).hasClass('visivel');");
		sb.append("if (e == false) {");
		sb.append("$(this).addClass('visivel').text('-');");
		sb.append("_fg.show('slow');");

		sb.append("$.each(_al, function(key, field){");
		sb.append("  $(field).removeAttr('disabled');");
		sb.append("});");

		sb.append("}else{");

		sb.append("$(this).removeClass('visivel').text('+');");
		sb.append("_fg.hide('slow');");
		sb.append("$.each(_al, function(key, field){");
		sb.append("  $(value).attr('disabled','disabled');");
		sb.append("});");
		sb.append("}");
		return sb.toString();
	}	
	public static String scriptOcultaField(ButtonHtml btn, AbstractFieldHtml ... fieldArray) {
		StringBuilder sb = new StringBuilder();

		sb.append("var _fA=[");
		String temp = "";
		for (AbstractFieldHtml field : fieldArray) {
			sb.append(temp);

			if (field instanceof AutoCompleteFieldHtml) {
				sb.append("$('#"+field.getPrefix() + field.getId()+"')");
				sb.append(", ");
				sb.append("$('#"+field.getId()+"')");
			} else{
				sb.append("$('#"+field.getId()+"')");
			}

			temp = ", "; 
		}
		sb.append("];");

		sb.append("var tipo = _fA[0].attr('disabled');");
		sb.append("$.each(_fA, function(key, field){");
		sb.append(" var _fg=$(field).parents('div.form-group:first');");
		sb.append(" if(tipo == 'disabled'){");
		sb.append("  $(field).removeAttr('disabled');");
		sb.append("  $('#"+ btn.getId() +"').text('-');");
		sb.append("  _fg.show();");
		sb.append(" }else{");
		sb.append("  $(field).attr('disabled','disabled');");
		sb.append("  $('#"+ btn.getId() +"').text('+');");
		sb.append("  _fg.hide();");
		sb.append(" }");
		sb.append("});");
		sb.append("if (tipo == 'disabled'){");
		sb.append("$.each(_fA, function(key, field){");
		sb.append("  $(field).change();");
		sb.append("});");
		sb.append("};");

		return sb.toString();
	}


	public void scriptShowWhen(AbstractFieldHtml field, ConditionJs conditionJs, Object ... values) {
		scriptShowWhen(field, true, conditionJs, false, values);
	}
	public void scriptShowWhen(AbstractFieldHtml field, boolean start, ConditionJs conditionJs, boolean desabled, Object ... values) {
		StringBuilder sb = new StringBuilder();

		sb.append(getScript());

		if (field instanceof RadioNumberFieldHtml) {

			sb.append("$('#"+getId()+"').ready( function() {");
			sb.append(" var _f=$('#"+getPrefix()+getId()+"');");
			sb.append(" $('#"+field.getId()+"').each(function() {");
			sb.append("   if ( ");

			String temp = "";
			for (Object obj : values) {
				sb.append(temp);

				if (obj instanceof String) {
					sb.append("($(this).val()"+ conditionJs.toString() +" '"+ obj +"')");
				} else {
					sb.append("($(this).val()"+ conditionJs.toString() + obj +")");
				}
				temp = "||";
			}
			sb.append(") {");
			// exibir o botaoo
			if (desabled == false) {
				sb.append("_f.show();");
			}
			// ocultar o botao
			sb.append("} else {");
			if (desabled == false) {
				sb.append("_f.hide();");
			}
			sb.append("  }");
			sb.append("});");
			sb.append("});");

			sb.append("$('."+ field.getId() +"').change(function() {");
			String byField = getPrefix()+getId();
			sb.append(" var _f=$('#"+byField+"');");
			sb.append(" $(this).each(function() {");

			sb.append("   if (($(this).prop('checked') === true) && ");
			temp = "";
			for (Object obj : values) {
				sb.append(temp);
				if (obj instanceof String) {
					sb.append("($(this).val() "+ conditionJs.toString() +" '"+ obj +"')");
				} else {
					sb.append("($(this).val() "+ conditionJs.toString() + obj +")");
				}
				temp = "||";
			}
			sb.append(") {");
			//			sb.append(" alert('II ->>>'+$('#"+byField+"').attr('id')); ");
			sb.append("_f.show();");
			sb.append("}");
			sb.append("});");
			sb.append("});");

		} else {
			sb.append("$('#"+ field.getId() +"').change(function() {");

			sb.append("var _f=$('#"+getPrefix()+getId()+"');");

			sb.append("if(");
			if (field instanceof CheckboxFieldHtml) {
				if (conditionJs.equals(ConditionJs.CHECKED)) {
					sb.append("($(this)." +ConditionJs.CHECKED.toString()+ "(':checked'))");
				} else {
					sb.append("(!$(this)." +ConditionJs.CHECKED.toString()+ "(':checked'))");
				}

			} else {
				String temp = "";
				for (Object obj : values) {
					sb.append(temp);

					if (obj instanceof String) {
						sb.append("($(this).val()"+ conditionJs.toString() +" '"+ obj +"')");
					} else {
						sb.append("($(this).val()"+ conditionJs.toString() + obj +")");
					}
					temp = "||";
				}
			}
			sb.append(")");

			// exibir o botao
			sb.append("{");
			if (desabled == false) {
				sb.append("_f.show();");
			}

			// ocultar o botao
			sb.append("} else {");
			if(desabled == false){
				sb.append("_f.hide();");
			}
			sb.append("}");
			if (start == false) {
				sb.append("});");
			} else {
				sb.append("}).change();");
			}
		}
		setScript(sb.toString());
	}
	public String createScriptShowWaiting() {
		return "$('#"+getId()+"').text('Aguarde...').prop('disabled',true);";
	}
	public String createScriptHideWaiting() {
		return "$('#"+ getId() +"').text('"+ getLabel() +"').prop('disabled',false);";
	}
}

