package br.com.dotum.core.html.form;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public class DividerHtml extends ComponentHtml {
	
	private Integer size; 

	public DividerHtml(Integer value) {
		this.size = value;
	}
	
	@Override
	public String getComponente() throws Exception {
		return "<div class=\"space-"+ this.size +"\"></div>";
	}

	@Override
	public void createScript() throws DeveloperException {
		
	}

	@Override
	public String getScript() throws DeveloperException {
		return null;
	}

	
	
}

