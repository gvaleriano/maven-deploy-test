package br.com.dotum.core.html.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.ajax.ScriptHelper;
import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.DataComponentHtml;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.form.field.FileFieldHtml;
import br.com.dotum.core.html.form.field.HiddenFieldHtml;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.core.html.other.WindowHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.StringUtils;

public class FormHtml extends DataComponentHtml {

	// estes atributos serve para criar a tela de qualquer modo
	// a ideia é fazer o form inline
	private Map<Integer, List<Integer>> compInLineMap = new LinkedHashMap<Integer, List<Integer>>();
	private List<ComponentHtml> compInLineList = new ArrayList<ComponentHtml>();

	// aqui é o form padrao com grupos de campos
	private List<GroupFieldHtml> groupFieldList = new ArrayList<GroupFieldHtml>();

	// outros atributos
	private List<ButtonHtml> buttonList = new ArrayList<ButtonHtml>();
	private List<ButtonHtml> buttonExtaList = new ArrayList<ButtonHtml>();
	private List<String> scriptExtaList = new ArrayList<String>();
	private WindowHtml win;
	private List<DataComponentHtml> compList;
	private boolean closeWin = true;
	private boolean filter = false;
	private boolean scriptAjax = true;
	private boolean refresh = false;

	private String scriptSuccess;
	private String scriptComplete;
	private String scriptBeforeSubmit;
	private boolean reload = false;
	private boolean onLoad = true;
	private Map<String, String> cloneGloupMap = new HashMap<String, String>();
	private String valueSlave;

	/**
	 * Utilizar o construtor com method
	 */
	public FormHtml() {
		super.setDataMethod("doData");

	}

	public FormHtml(String dataMethod) {
		super.setDataMethod(dataMethod);
	}

	public FormHtml(String dataMethod, Integer dataId) {
		super.setDataMethod(dataMethod);
		super.setDataId(dataId);
	}

	public void addGroup(String groupStr, AbstractFieldHtml... fields) {
		addGroup(groupStr, 1, fields);
	}

	public void addGroup(String groupStr, Integer column, ComponentHtml... compArray) {
		addComp(groupStr, column, null, false, compArray);
	}
	private void addComp(String groupStr, Integer column, Integer line, boolean inLine, ComponentHtml... compArray) {
		boolean added = false;
		GroupFieldHtml group = null;
		for (GroupFieldHtml temp : this.groupFieldList) {
			if ((groupStr != null) && (groupStr.equals(temp.getLabel()) == false))
				continue;
			
			group = temp;
			added = true;
			break;
		}

		if (added == false) {
			group = new GroupFieldHtml(groupStr);
			group.setParent(this);
		}

		
		for (ComponentHtml c : compArray) {
			c.setParent(this);
		}

		if(inLine == true) {
			group.addInLine(line, column, compArray);
		} else {
			group.addComp(column, compArray);
		}
		
		if (added == false)
			this.groupFieldList.add(group);

	}


	/**
	 * Este metodo pode ser substituido pelo que não possui o parametro de class nos
	 * casos que for passado o componente<br>
	 * diretamente para o super.addBody(form) do controller e action.<br>
	 * Desta forma é permitido:<br>
	 * super.addBody(form)<br>
	 * ou ainda <br>
	 * tab.add(form);<br>
	 * super.addBody(tab);<br>
	 * <br>
	 * Caso utilize o metodo getComponente antes de chamar o super.addBody(new
	 * Html(form.getComponent()));<br>
	 * é obrigatorio passar o class.<br>
	 * 
	 * @param view
	 * @param method
	 * @param label
	 * @throws DeveloperException
	 */
	public void addButton(Class view, String method, String label) throws DeveloperException {
		addButton(view, null, null, method, label);
	}

	public void addButton(String method, String label) throws DeveloperException {
		addButton(null, null, null, method, label);
	}

	public void addButton(CssIconEnum icon, CssColorEnum cor, String method, String label) throws DeveloperException {
		addButton(null, icon, cor, method, label);
	}

	private void addButton(Class<?> view, CssIconEnum icon, CssColorEnum cor, String method, String label) throws DeveloperException {
		ButtonHtml button = new ButtonHtml(icon, cor, method, label);

		if (view != null)
			button.setClassAction(view);

		button.setPositionBottom(true);

		buttonList.add(button);
	}

	public String getComponente() throws Exception {
		StringBuilder sb = new StringBuilder();

		// aqui acho que tenho o getHtml()...
		for (TableHtml table : getAllTableList()) {
			table.setDataClass(getDataClass());
		}

		addCssClass("form-group");

		sb.append("<div class=\"hide\">");
		for (String key : this.cloneGloupMap.keySet()) {
			String group = this.cloneGloupMap.get(key);
			
			sb.append("<div class=\""+ key +"\">");
			sb.append(group);
			sb.append("</div>");
		}
		sb.append("</div>");
		if (hasFieldFile()) {
			sb.append("<form id=\""+ this.getId() +"\" class=\""+ super.getCssClassComponent() +"\" role=\"form\" method=\"POST\" action=\""+ FWConstants.URL_DO +"\" autocomplete=\"off\" enctype=\"multipart/form-data\">");
		} else {
			sb.append("<form id=\""+ this.getId() +"\" class=\""+ super.getCssClassComponent() +"\" role=\"form\" method=\"POST\" action=\""+ FWConstants.URL_DO +"\" autocomplete=\"off\">");
		}
		
		

		// isso aqui ja vai por padrao para os form de alterar...
		// para outros casos nao muda nada...
		// entao sempre vai o ID
		HiddenFieldHtml f1 = new HiddenFieldHtml(FWConstants.PARAM_FW_MASTER_ID, null);
		f1.setResultHtml(getHtml());
		HiddenFieldHtml f2 = new HiddenFieldHtml(FWConstants.PARAM_FW_SLAVE_ID, this.valueSlave);
		f2.setResultHtml(getHtml());
		sb.append(f1.getComponente());
		sb.append(f2.getComponente());

		sb.append("<div id=\""+getId()+"Accordian\" class=\"accordion-style1 panel-group accordion-style2\">" + ResultHtml.PL);


		// aqui sao os grupos padrao
		for (int i = 0; i < groupFieldList.size(); i++) {
			GroupFieldHtml group = groupFieldList.get(i);
			group.setHtml(getHtml());
			group.setPosition(i);
			sb.append(group.getComponente());
		}

		boolean inBottom = false;
		if (buttonList.size() > 0) {
			for (ButtonHtml button : buttonList) {
				if (button.isPositionBottom() == false)
					continue;

				inBottom = true;
				break;
			}

		}
		if (inBottom == true) {
			sb.append("<div class=\"space-4\"></div>" + ResultHtml.PL);
			sb.append("<div class=\"padding-8 clearfix\">" + ResultHtml.PL);
			sb.append("<div class=\"pull-right\">" + ResultHtml.PL);
			for (ButtonHtml button : buttonList) {
				button.setHtml(getHtml());

				boolean bootom = button.isPositionBottom();
				if (bootom == false)
					continue;

				sb.append(button.getComponente() + ResultHtml.PL);
			}
			sb.append("</div>" + ResultHtml.PL);
			sb.append("</div>" + ResultHtml.PL);
		}
		sb.append("</div>" + ResultHtml.PL);
		sb.append("</form>" + ResultHtml.PL);

		// Validator

		return sb.toString();
	}

	private boolean hasFieldFile() {
		for (GroupFieldHtml group : groupFieldList) {
			for (AbstractFieldHtml field : group.getFieldList()) {
				if ((field instanceof FileFieldHtml) == false)
					continue;
				return true;
			}
		}
		return false;
	}

	@Override
	public void createScript() throws DeveloperException {
		for (TableHtml table : getAllTableList()) {
			table.setDataClass(getDataClass());
		}

		ComponentHtml.prepare(getHtml(), groupFieldList);
		// nao precisa mais isso aqui por que o addInLine joga o comp no groupFieldList de cima!! 
//		ComponentHtml.prepare2(getHtml(), compInLineList);

		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() throws DeveloperException {
		StringBuilder sb = new StringBuilder();

		if (scriptAjax == true) {

			sb.append(ResultHtml.PL);

			if (ResultHtml.DEBUG == true) {

				sb.append("/*SCRIPT DO FORM "+ (getDataClass() != null ? getDataClass().getName() : "SEM CLASSE") + "." + getDataMethod() +"*/" + ResultHtml.PL);
			}



			if (hasValidate()) {
				sb.append("$('#" + getId() + "').validate({                                                                 " + ResultHtml.PL);
				sb.append("debug: true,                                                                                     " + ResultHtml.PL);
				sb.append("errorElement: 'div',                                                                             " + ResultHtml.PL);
				sb.append("errorClass: 'help-block',                                                                        " + ResultHtml.PL);
				sb.append("focusInvalid: false,                                                                             " + ResultHtml.PL);
				sb.append("ignore: '',                                                                                      " + ResultHtml.PL);
				sb.append("submitHandler: function("+getId()+") {                                                                  " + ResultHtml.PL);
			} else {
				sb.append("$(\"#" + getId() + "\").submit(function(e) {");
			}

			StringBuilder beforeSend = new StringBuilder();
			for (ButtonHtml btn : buttonList) {
				if (StringUtils.hasValue(btn.getLabel())) {
					beforeSend.append(btn.createScriptShowWaiting());
				}
			}
			if (StringUtils.hasValue(this.scriptBeforeSubmit)) {
				beforeSend.append(this.scriptBeforeSubmit);
			}
			String mensagem = MessageHtml.createScript(null, "'+data."+ FWConstants.JSON_ATTR_TYPE +"+'", "'+data."+ FWConstants.JSON_ATTR_MESSAGE +"+'");

			StringBuilder success = new StringBuilder();
			success.append("var _download = data."+ FWConstants.JSON_ATTR_REPORT +" != null;");
			success.append("var _dynamic = data."+ FWConstants.JSON_ATTR_HTML +" != null;");
			success.append("if((_download == true) && (data."+ FWConstants.JSON_ATTR_TYPE +"=='"+ FWConstants.JSON_ATTR_TYPE_SUCCESS +"')){");
			success.append(ScriptHelper.createScriptDownladAjax());
			success.append("} else if(_dynamic == true) {");
			success.append("$('#"+ getDataClass().getSimpleName() +"').find('div.widget-main').html(data.html);");
			success.append("}else{");
			success.append(mensagem);
			success.append(createScriptClean());

			// fechar a windows e monstrar a mensgem de sucesso OK
			if (refresh == true) {

				success.append("window.location.reload();");

			} else if ((this.compList != null) && (this.compList.size() > 0)) {

				for (DataComponentHtml comp2 : this.compList) {
					success.append(comp2.createScriptPopulateByJson());
				}
			}

			if ((win != null) && (closeWin == true)) {
				success.append(win.createScriptCloseWindow());
			}

			// este if é utilizado pelo framework
			// para recarregar a tabela principal
			// vem da classe AbstractDispacher:549
			// >>> scriptReloadTableMain = view.getTable().createScriptPopulateByJson();
			// ainda pode setar script aqui atraves do metodo setScriptSucess(String);
			if (this.scriptSuccess != null) {
				success.append(this.scriptSuccess);
			}

			success.append("}");

			StringBuilder complete = new StringBuilder();
			for (ButtonHtml btn : buttonList) {
				if (StringUtils.hasValue(btn.getLabel())) {
					complete.append(btn.createScriptHideWaiting());
				}
			}
			if (StringUtils.hasValue(this.scriptComplete)) {
				complete.append(this.scriptComplete);
			}

			if (reload == true)
				complete.append(createScriptPopulateByJson());

			Ajax ajax = new Ajax();

			if (hasValidate()) {
				ajax.setJsObjForm(getId());
			} else {
				ajax.setForm(true);
			}


			//
			// script especial para pegar os itens selecionados na table e mandar junto com o form
			//
			for (TableHtml table : getAllTableList()) {
				ajax.addParamComponent(table);
			}


			ajax.setBeforeSend(beforeSend.toString());
			ajax.setSuccess(success.toString());
			ajax.setComplete(complete.toString());
			ajax.addProperty("processData", false);
			ajax.addProperty("contentType", false);

			sb.append(ajax.getComponent());

			List<AbstractFieldHtml> fList = getAllFieldList();
			if (fList.size() > 0) {
				sb.append("$('#"+ getId() +" :input:enabled:visible:first').focus();");
			}


			if (hasValidate()) {
				sb.append("return false;");
				sb.append("},                                                                                               " + ResultHtml.PL);
				sb.append("rules:{                                                                                                                                                                                              " + ResultHtml.PL);
				String temp = "";
				for (AbstractFieldHtml fild : getAllFieldList()) {
					if (fild.getValidate() != null) {
						Map<String, String> map = fild.getValidate();
						sb.append(temp);
						for (String key : map.keySet()) {
							sb.append(key                                                                                                                                 + ResultHtml.PL);
						}
						temp = ",";
					}
				}
				sb.append("},                                                                                                                                " + ResultHtml.PL);
				sb.append("messages: {                                                                                                                                " + ResultHtml.PL);
				temp = "";
				for (AbstractFieldHtml field : getAllFieldList()) {
					if (field.getValidate() != null) {
						Map<String, String> map = field.getValidate();
						sb.append(temp);
						for (String key : map.keySet()) {
							sb.append(map.get(key)                                                                                                                                 + ResultHtml.PL);
						}
						temp = ",";
					}
				}
				sb.append("},                                                                                                                                " + ResultHtml.PL);
				sb.append("highlight: function (e) {                                                                                                         " + ResultHtml.PL);
				sb.append("$(e).closest('.form-group').removeClass('has-info').addClass('has-error');                                                        ");
				sb.append("},                                                                                                                                " + ResultHtml.PL);
				sb.append("success: function (e) {                                                                                                           " + ResultHtml.PL);
				sb.append("$(e).closest('.form-group').removeClass('has-error').addClass('has-success');                                                     " + ResultHtml.PL);
				sb.append("$(e).remove();                                                                                                                    ");
				sb.append("$('#" + getId() + "').find('button[name=doProcess]').removeClass('disabled');                                                     ");
				sb.append("},                                                                                                                                ");
				sb.append("errorPlacement: function (error, element) {                                                                                       ");
				sb.append("$('#" + getId() + "').find(\"button[name=doProcess]\").addClass('disabled');                                                      ");
				sb.append("	if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {                                                      ");
				sb.append("		var controls = element.closest('div[class*=\"col-\"]');                                                                      ");
				sb.append("		if(controls.find(':checkbox,:radio').length > 1) controls.append(error);                                                                ");
				sb.append("		else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));                                                                ");
				sb.append("	}                                                                                                                                ");
				sb.append("	else if(element.is('.select2')) {                                                                                                                                ");
				sb.append("		error.insertAfter(element.siblings('[class*=\"select2-container\"]:eq(0)'));                                                                ");
				sb.append("	}                                                                                                                                ");
				sb.append("	else if(element.is('.chosen-select')) {                                                                ");
				sb.append("		error.insertAfter(element.siblings('[class*=\"chosen-container\"]:eq(0)'));                                                                ");
				sb.append("	}                                                                                                                                ");
				sb.append("	else error.insertAfter(element.parent());                                                                ");
				sb.append("}                                                                                                                                ");
				sb.append("});                                                                                                                                " + ResultHtml.PL);
			} else {
				sb.append("e.preventDefault();");
				sb.append("});                                                                                                                                " + ResultHtml.PL);
			}

			sb.append(ResultHtml.PL);
		} else {
			for (ButtonHtml btn : buttonList) {
				sb.append(btn.getScript());
			}
		}
		/**
		 * Bloco da misericordia, está aqui pois tendo necessidade de enviar um js
		 * para pagina só era possivel atravez do super.addScript,
		 * Dificultando a vida do programador, agora script referente ao formulario
		 * manda por essa duas opções aqui mano
		 * 
		 * by Isac C. Farias
		 * @date 08/08/2018
		 */
		for (ButtonHtml btn : buttonExtaList) {
			sb.append( btn.getScript() );
		}

		for (String script : scriptExtaList) {
			sb.append(script);
		}

		for (TableHtml table : getAllTableList()) {
			table.setDataClass(getDataClass());
			sb.append(table.getScript());
		}


		/*
		 * Tentamos colocar no clean, porem na primeira vez que submete ele não limpava o fileInput
		 * quando vem para ca  ja executa de primeira
		 * 
		 */
		for (AbstractFieldHtml field : getAllFieldList()) {
			if ((field instanceof FileFieldHtml)) {
				sb.append("$('#"+getId()+"').on('submit', function(e) {");
				sb.append("$('#"+field.getId()+"').ace_file_input('reset_input_ui');");
				sb.append("});");
			}
		}

		return sb.toString();
	}


	public boolean hasValidate() {
		Boolean hasValidate = false;
		for (AbstractFieldHtml field : getAllFieldList()) {
			if (field.getValidate() == null)
				continue;
			hasValidate = true;
		}
		return hasValidate;
	}


	public Object getValueByNameField(String name) {
		for (AbstractFieldHtml x : getAllFieldList()) {
			if (x.getName().equals(name) == false)
				continue;

			return x.getValue();
		}
		return null;
	}

	public List<AbstractFieldHtml> getAllFieldList() {
		List<AbstractFieldHtml> list = new ArrayList<AbstractFieldHtml>();
		for (GroupFieldHtml group : groupFieldList) {
			list.addAll(group.getFieldList());
		}
		for (ComponentHtml comp : compInLineList) {
			if (comp instanceof AbstractFieldHtml)
				list.add((AbstractFieldHtml)comp);
		}

		return list;
	}

	public List<TableHtml> getAllTableList() {
		List<TableHtml> list = new ArrayList<TableHtml>();
		for (GroupFieldHtml group : groupFieldList) {
			list.addAll(group.getTableList());
		}
		for (ComponentHtml comp : compInLineList) {
			if (comp instanceof TableHtml)
				list.add((TableHtml)comp);
		}

		return list;
	}


	public List<ButtonHtml> getAllButtonList() {
		for (GroupFieldHtml group : groupFieldList) {
			buttonList.addAll(group.getButtons());
		}
		for (ComponentHtml comp : compInLineList) {
			if (comp instanceof ButtonHtml)
				buttonList.add((ButtonHtml)comp);
		}
		return buttonList;
	}

	public void setParent(WindowHtml win) {
		this.win = win;
	}

	public void setDataComponent(DataComponentHtml ... compList) {
		this.compList = ((List<DataComponentHtml>)Arrays.asList(compList));

		this.closeWin = false;
	}

	public void setDataComponent(boolean closeWin, DataComponentHtml ... compList) {
		setDataComponent(compList);
		this.closeWin = closeWin;
	}

	public String createScriptClean() {
		StringBuilder sb = new StringBuilder();
		// isso aqui da problema nos screenView quando o mesmo form usa para inserir e alterar
		// acredito que era usado no Controller Projeto... 
		// sera preciso revisar e padronizar isso de uma vez por todas!!!
		if (super.getDataClass().getSuperclass().equals(AbstractController.class) == false) {
			// limpa tbm o slave
			sb.append("$('#"+ getId() +" input[type=hidden]').not('input[name=\""+ FWConstants.PARAM_FW_MASTER_ID +"\"], input[name=\""+ FWConstants.PARAM_FW_CLASS +"\"], input[name=\""+ FWConstants.PARAM_FW_METHOD +"\"]').val('');");
		} else {
			sb.append("$('#"+ getId() +" input[type=hidden]').not('input[name=\""+ FWConstants.PARAM_FW_MASTER_ID +"\"], input[name=\""+ FWConstants.PARAM_FW_SLAVE_ID+"\"], input[name=\""+ FWConstants.PARAM_FW_CLASS +"\"], input[name=\""+ FWConstants.PARAM_FW_METHOD +"\"]').val('');");
		}
		
		
		sb.append("$('#"+ getId() +"')[0].reset();");
		sb.append("$('body').find('#"+ getId() +" div.clone').remove();");

		return sb.toString();
	}

	public String createScriptPopulateByJson() throws DeveloperException {
		if ((super.getDataClass() != null) && (StringUtils.hasValue(super.getDataMethod()))) {

			StringBuilder success = new StringBuilder();
			success.append("populateForm('#"+ getId() +"', data.data);");


			// XXX esta linha esta sendo comentada por causa da primeira vez que abre uma action
			// na tela de ScreenView...
			// a ideia é simples
			// nao precisa fazer um reload na tabela principal quando apenas abre a tela
			//			if (this.scriptSuccess != null) success.append( this.scriptSuccess );

			Ajax ajax = new Ajax(super.getDataClass(), super.getDataMethod());
			ajax.addProperty("type", "GET");
			ajax.setNameVar(getId());
			ajax.setSuccessOrWarningMessage(success.toString());

			if (filter == false) {
				ajax.addParamJs(FWConstants.PARAM_FW_MASTER_ID, "("+getDataId()+" || _id)");
				ajax.addParamJs(FWConstants.PARAM_FW_SLAVE_ID, "_idSlave");
			} else {
				ajax.addParamJs(FWConstants.PARAM_FW_MASTER_ID, "_id");
			}
			return ajax.getComponent();
		}

		return "";
	}

	@Deprecated
	/**
	 * Ao inves de utilizar este metodo... invokar o metodo setScriptSuccess
	 * 
	 * @param script
	 */
	public void setScript(String scriptSuccess) {
		this.scriptSuccess = scriptSuccess;
	}

	public void setScriptSuccess(String scriptSuccess) {
		this.scriptSuccess = scriptSuccess;
	}

	public void setScriptBeforeSubmit(String scriptBeforeSubmit) {
		this.scriptBeforeSubmit = scriptBeforeSubmit;
	}

	public void setScriptComplete(String scriptComplete) {
		this.scriptComplete = scriptComplete;
	}

	public void addExtraScript(String script) {
		this.scriptExtaList.add(script);
	}

	public void addButton(ButtonHtml button) throws DeveloperException {
		this.buttonList.add(button);
	}


	public void setAjax(boolean ajax) {
		this.scriptAjax = ajax;
	}

	public void setColumnSize(String groupStr, Integer... values) {
		for (GroupFieldHtml temp : groupFieldList) {
			if ((groupStr != null) && (groupStr.equals(temp.getLabel()) == false))
				continue;

			temp.setColumnSize(values);
		}
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public void setFilter(boolean filter) {
		this.filter = filter;
	}

	public void addInLine(int line, int columnSize, ComponentHtml comp) throws DeveloperException {
		addInLine(null, line, columnSize, comp);
	}
	public void addInLine(String groupStr, int line, int columnSize, ComponentHtml comp) throws DeveloperException {
		List<Integer> list = compInLineMap.get(line);
		if (list == null) list = new ArrayList<Integer>();
		list.add(columnSize);
		compInLineMap.put(line, list);
		compInLineList.add(comp);
		
		if (comp instanceof GroupFieldHtml) {
			GroupFieldHtml g = (GroupFieldHtml)comp;
			List<ComponentHtml> cList = g.getComponentList();
			List<Integer> sList = g.getColumnSize();
			Integer size = 6;
			for (int i = 0; i < cList.size(); i++) {
				if(sList.size() > 0) size =  sList.get(i);
				addComp(groupStr, size, line, true, cList.get(i));
			}
			
//			String xxx = g.getFieldList().get(0).getName().split("\\.")[0];
//			addComp(groupStr, 12, line+1, true, new Html("<div id=\""+ xxx +"\"></div>"));
			
			if ((groupStr != null) && (groupStr.equals(g.getLabel()) == false)) {
				throw new DeveloperException("O label do grupo deve ser passado tbm ao adicionar no formulario");
			}
			
			GroupFieldHtml x = getGroup(groupStr);
			x.setId(g.getId());
		} else {
			addComp(groupStr, columnSize, line, true, comp);
		}
	}

	public boolean isOnLoad() {
		return onLoad;
	}

	public void setOnLoad(boolean onLoad) {
		this.onLoad = onLoad;
	}

	/**
	 * Quanto matar esse metodo avisar o isac - credip
	 */
	public void reloadDoDataOnSubmit() {
		this.reload = true;
	}

	/**
	 * Nao ha mais necessidade deste metodo
	 * Pode apagar sem medo!!
	 * @param b
	 */
	@Deprecated
	public void setDownload(boolean b) {
	}

	public GroupFieldHtml getGroup(String groupStr) {
		for (GroupFieldHtml group : groupFieldList) {
			if (group.getLabel().equals(groupStr) == false) continue;

			return group;
		}

		return null;
	}

	protected void addCloneGroup(String key, GroupFieldHtml groupFieldHtml) throws Exception {
		this.cloneGloupMap.put(key, groupFieldHtml.createHtmlFieldColumn());
	}

	public void setValueSlave(String valueSlave) {
		this.valueSlave = valueSlave;
	}
}
