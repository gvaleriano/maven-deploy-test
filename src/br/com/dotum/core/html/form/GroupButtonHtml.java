package br.com.dotum.core.html.form;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.core.component.ComponentHtml;

public class GroupButtonHtml extends ComponentHtml {

	private List<ButtonHtml> list = new ArrayList<ButtonHtml>();

	public void addButton(ButtonHtml ...btn) {
		for(ButtonHtml temp : btn) {
			list.add(temp);
		}

	}

	@Override
	public String getComponente() throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"hidden-sm hidden-xs btn-group\">");
		sb.append("<span class=\"btn btn-small btn-primary dropdown-toggle\" data-toggle=\"dropdown\">");
		sb.append( getLabel() );
		sb.append("<i class=\"ace-icon fa fa-caret-down icon-on-right\"></i>");
		sb.append("</span>");
		sb.append("<ul class=\"dropdown-menu no-padding\">");

		for (ButtonHtml btn : list) {
			btn.addCssClass("btn-link");
			sb.append("<li class=\"list-group-item\">");
			sb.append( btn.getComponente() );
			sb.append("</li>");
		}

		sb.append("</ul>");
		sb.append("</div>");

		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		return "";
	}

}

