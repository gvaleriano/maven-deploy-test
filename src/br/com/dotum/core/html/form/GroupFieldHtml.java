package br.com.dotum.core.html.form;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.form.field.ConditionJs;
import br.com.dotum.core.html.form.field.HiddenFieldHtml;
import br.com.dotum.core.html.form.field.ShowWhen;
import br.com.dotum.core.html.form.field.ShowWhen.Operation;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.StringUtils;

public class GroupFieldHtml extends ComponentHtml {

	private Map<Integer, List<Integer>> compInLineMap = new LinkedHashMap<Integer, List<Integer>>();
	private List<ComponentHtml> compInLineList = new ArrayList<ComponentHtml>();

	private Map<Integer, List<ComponentHtml>> map = new LinkedHashMap<Integer, List<ComponentHtml>>();
	private List<Integer> columnSize = new ArrayList<Integer>();
	private int position;
	private ShowWhen scriptShowWhen = new ShowWhen();
	
	private boolean clonal = false;
	private String clonalScript = null;
	
	public GroupFieldHtml(String label, Integer ... sizeColumns) {
		setLabel(label);
		setColumnSize(sizeColumns);
	}

	public GroupFieldHtml(String label) {
		setLabel(label);
	}

	public void addComp(Integer column, ComponentHtml ... compArray) {
		List<ComponentHtml> compList = this.map.get(column);
		if (compList == null) {
			compList = new ArrayList<ComponentHtml>();
			this.map.put(column, compList);
		}

		if (compArray != null) {
			for (ComponentHtml c : compArray) {
				compList.add(c);
			}
		}
	}
	public void addInLine(Integer line, Integer column, ComponentHtml ... compArray) {
		List<Integer> list = compInLineMap.get(line);
		if (list == null) list = new ArrayList<Integer>();
		columnSize.add(column);
		list.add(column);
		compInLineMap.put(line, list);

		if (compArray != null) {
			for (ComponentHtml c : compArray) {
				if(c instanceof ButtonHtml) {
					c.addCssClass("inline");
				}
				compInLineList.add(c);
			}
		}
	}

	public List<AbstractFieldHtml> getFieldList() {
		List<AbstractFieldHtml> list = new ArrayList<AbstractFieldHtml>();
		for (Integer column : this.map.keySet()) {

			for (ComponentHtml c : this.map.get(column)) {
				if (c instanceof AbstractFieldHtml == false) continue;
				list.add( (AbstractFieldHtml)c );
			}

		}

		if(compInLineList.size() > 0) {
			for (ComponentHtml c : compInLineList) {
				if (c instanceof AbstractFieldHtml == false) continue;
				list.add( (AbstractFieldHtml)c );
			}
		}
		return list;
	}

	public List<ComponentHtml> getComponentList() {
		List<ComponentHtml> list = new ArrayList<ComponentHtml>();
		for (Integer column : this.map.keySet()) {
			for (ComponentHtml c : this.map.get(column)) {
				list.add( c );
			}
		}

		if(compInLineList.size() > 0) {
			for (ComponentHtml c : compInLineList) {
				list.add( c );
			}
		}
		return list;
	}

	public List<TableHtml> getTableList() {
		List<TableHtml> list = new ArrayList<TableHtml>();
		for (Integer column : this.map.keySet()) {

			for (ComponentHtml c : this.map.get(column)) {
				if (c instanceof TableHtml == false) continue;
				list.add( (TableHtml)c );
			}

		}

		return list;
	}

	public List<ButtonHtml> getButtons() {
		List<ButtonHtml> list = new ArrayList<ButtonHtml>();
		for (Integer column : this.map.keySet()) {

			for (ComponentHtml c : this.map.get(column)) {
				if (c instanceof ButtonHtml == false) continue;
				list.add( (ButtonHtml)c );
			}

		}

		return list;
	}

	@Override
	public String getComponente() throws Exception{
		StringBuilder sb = new StringBuilder();

		boolean exibirLabel = false;
		List<AbstractFieldHtml> list = getFieldList();
		for (AbstractFieldHtml x : list) {
			if (x instanceof HiddenFieldHtml == true) continue; 
			exibirLabel = true;
			break;
		}
		if (StringUtils.hasValue(getLabel()) == false) exibirLabel = false;

		sb.append("<div class=\"space-4\"></div>" + ResultHtml.PL);
		sb.append("<div class=\"col-sm-12 no-padding\">" + ResultHtml.PL);

		sb.append("	<div class=\"widget-box no-border\" id=\""+ getId() +"\">");
		if (exibirLabel == true) {
			sb.append("<div class=\"widget-header widget-header-flat\">");
			sb.append("<h4 class=\"widget-title\">");
			sb.append(getLabel());
			sb.append("</h4>");
			sb.append("</div>");
		}

		sb.append("<div class=\"widget-body no-padding-left no-padding-right\">");
		sb.append("<div class=\"widget-main no-padding-left no-padding-right\">");

		sb.append( createHtmlFieldColumn() );

		sb.append("</div>");

		sb.append("</div>");
		sb.append("</div>");
		sb.append("</div>");

		return sb.toString();
	}

	public String createHtmlFieldColumn() throws Exception {
		StringBuilder sb = new StringBuilder();

		int qtdColumn = this.map.size();
		int i = 0;
		if (clonal == true) sb.append("<div class=\"clone\">");
		for (Integer column : this.map.keySet()) {
			Integer size = 6;
			if (columnSize.size() > i) {
				size = columnSize.get(i);
			}

			if (qtdColumn > 1) {
				sb.append("<div class=\"col-xs-12 col-sm-"+ size +"\">");
			}
			for (ComponentHtml c : this.map.get(column) ) {
				sb.append(  c.getComponente() );
			}
			if (qtdColumn > 1) {
				sb.append("</div>");
			}
			i++;
		}
		if(compInLineMap.size() > 0) {
			int il = -1;
			sb.append("<div class=\"row\">");
			sb.append("<div class=\"col-sm-12\">");
			for (Integer line : compInLineMap.keySet()) {
				List<Integer> columnSizeList = compInLineMap.get(line);
				for (Integer columnSize : columnSizeList) {
					il++;
					ComponentHtml comp = compInLineList.get(il);
					comp.setHtml(getHtml());

					if (comp instanceof AbstractFieldHtml) {
						AbstractFieldHtml x = (AbstractFieldHtml)comp;
						x.inLine(true);
						x.setSize(12);
					}
					comp.setSize(columnSize);
					sb.append(comp.getComponente());
				}
			}
			sb.append("</div>");
			sb.append("</div>");
		}
		if (this.clonal == true) sb.append("</div>");

		return sb.toString();
	}

	@Override
	public void createScript() throws DeveloperException {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() throws DeveloperException {
		ComponentHtml.prepare(getHtml(), getFieldList());

		StringBuilder sb = new StringBuilder();
		// preocupar-se com o showWhen Group
		sb.append( scriptShowWhen.createScript(this) );

		if (this.clonal == true) {
			sb.append( this.clonalScript );
		}


		for (ButtonHtml btn : getButtons()) {
			sb.append( btn.getScript() );
		}
		return sb.toString();
	}

	public void setPosition(int position) {
		this.position = position;

	}

	public void setColumnSize(Integer ... values) {
		for (Integer value : values) {
			columnSize.add(value); 
		}
	}

	public void scriptShowWhen(AbstractFieldHtml field, ConditionJs conditionJs, Object ... values) {
		scriptShowWhen.add(field, conditionJs, values);
	}

	public void scriptShowWhen(Operation operation, AbstractFieldHtml field, ConditionJs conditionJs, Object ... values) {
		scriptShowWhen.add(operation, field, conditionJs, values);
	}

	protected List<Integer> getColumnSize() {
		return columnSize;
	}

	protected void setCompInLine(List<ComponentHtml> compInLineList, Map<Integer, List<Integer>> compInLineMap) {
		this.compInLineList = compInLineList;
		this.compInLineMap = compInLineMap;
	}

	public void setClonal(FormHtml form) throws Exception {
		this.clonal = true;
		
		String partOld = null;
		List<String> labelList = new ArrayList<String>();
		List<AbstractFieldHtml> fieldList = getFieldList();
		for (AbstractFieldHtml f : fieldList) {
			String name = f.getName();
			labelList.add(f.getLabel());
			f.setLabel(null);
			
			if (name.contains(".") == false) 
				throw new DeveloperException("Não foi configurado o grupo de forma correta. É preciso que todos tenham o mesmo prefixo por causa do doData");
			
			String part = name.split("\\.")[0];
			if (partOld != null && part.equals(partOld) == false)
				throw new DeveloperException("Não foi definido o mesmo prefixo dos campos do grupo especial");
			
			partOld = part;
		}
		setId(partOld);
		
		
		this.addCssClass("clone");
		
		ButtonHtml minusBtn = new ButtonHtml(CssIconEnum.MINUS, "#");
		minusBtn.setColor(CssColorEnum.RED);
		minusBtn.addAttr("tabIndex", -1);
		minusBtn.onClick(ButtonHtml.scriptRemoveFields());
		addInLine(1, 1, minusBtn);

		form.addCloneGroup(getId(), this);
		
		
		for (int i = 0; i < fieldList.size(); i++) {
			String label = labelList.get(i);
			AbstractFieldHtml f = fieldList.get(i);
			
			f.setLabel(label);
		}

		
		
		ButtonHtml plusBtn = new ButtonHtml(CssIconEnum.PLUS, "#");
		plusBtn.onClick(ButtonHtml.scriptAddFields(getId()));
		addInLine(1, 1, plusBtn);
		removeComponentPlusClonal(minusBtn);

		this.clonalScript = minusBtn.getScript();
		this.clonalScript += plusBtn.getScript();
		
	}

	private void removeComponentPlusClonal(ComponentHtml comp) {
		compInLineList.remove(comp);
		List<Integer> sizeLine1 = compInLineMap.get(1);
		sizeLine1.remove(sizeLine1.size()-1);
	}
}

