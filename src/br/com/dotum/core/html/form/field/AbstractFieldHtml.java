package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.field.ShowWhen.Operation;
import br.com.dotum.jedi.util.StringUtils;


public abstract class AbstractFieldHtml extends ComponentHtml {

	private String visible;
	private String cssClass;
	private String cssStyle;
	private Class<?> typeValue;
	private String mask;
	private CssIconEnum iconAfter;
	private CssIconEnum iconBefore;
	private int type;
	private StringBuilder script = new StringBuilder();
	private List<ValidatorField> validators = new ArrayList<ValidatorField>();
	private String tooltip;
	private boolean inLine = false;



	public abstract String getValueToHtml() throws Exception;
	public abstract Map<String, String> getValidate();

	public abstract Object getValue();
	public abstract void setValue(Object value);

	public void setRequired(boolean value) {
		if (value == true) {
			addAttr("required", "required");
		}
	}
	public boolean isRequired() {
		return super.hasAttr("required");
	}

	public void setReadOnly(boolean value) {
		if (value == true) {
			addAttr("readonly", value);
		}
	}

	public void setDisabled(boolean value) {
		if (value == true) {
			addAttr("disabled", "disabled");
		}
	}

	public void setVisible(boolean value) {
		if (value == false) {
			visible = " style=\"display:none;\"";
		}
	}

	public void setMinValue(Integer value) {
		addAttr("min", ""+value);		
	}

	public void setMaxValue(Integer value) {
		addAttr("max", ""+value);		
	}

	public void setPlaceholder(String value) {
		addAttr("placeholder", ""+value);
	}

	public void setMaxLength(int value) {
		addAttr("maxlength", ""+value);
	}

	public void setMinLength(int value) {
		addAttr("minlength", ""+value);
	}

	public void inLine(boolean inLine) {
		this.inLine = inLine;
	}
	public Boolean getInLine() {
		return this.inLine; 
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}


	public Class<?> getTypeValue() {
		return typeValue;
	}
	public void setTypeValue(Class<?> typeValue) {
		this.typeValue = typeValue;
	}

	public void addValidator(ValidatorField validator){
		validators.add(validator);
	}

	public List<ValidatorField> getValidators(){
		return this.validators;
	}

	public String getLabelBefore() {
		StringBuilder sb = new StringBuilder();

		String part = "";
		if (StringUtils.hasValue(visible) == true) {
			part = " "+ visible;
		}

		if(StringUtils.hasValue(super.getSize()) == false) {
			setSize(12);
		}

		if (StringUtils.hasValue(getLabel()) == true) {
			sb.append("<div class=\"form-group no-padding"+ super.getSize() + "\""+ part +">");
		} else {
			sb.append("<div class=\"form-group "+ super.getSize() + "\""+ part +">");
			sb.append("<div class=\"space-14\">&nbsp;</div>");
		}

		if (StringUtils.hasValue(getLabel()) == true) {
			String classRequired = "";
			String asterisco = "";
			if (isRequired()) {
				classRequired = " orange bolder";
				asterisco = " *";
			}
			if(getInLine() == false) {
				sb.append( "<label class=\"control-label text-right col-xs-12 col-sm-3 no-padding inline"+ classRequired +"\" for=\""+ getId() +"\">"+ getLabel() + asterisco   );
				if (getTooltip() != null) {
					sb.append("<span class=\"help-button popover-warning\" data-rel=\"popover\" data-trigger=\"hover\" data-placement=\"left\" data-content=\""+getTooltip()+"\" title=\"Ajuda\">?</span>");
				}
				sb.append("</label>");
			}

			int sizeInLine = 9;
			if (inLine == true) sizeInLine = 12;


			sb.append(  "<div class=\"col-xs-12 col-sm-"+ sizeInLine +"\">");


			if(getInLine() == true) {
				sb.append(  "<label class=\"control-label col-xs-12 no-padding inline"+ classRequired +"\" for=\""+ getId() +"\">"+ getLabel() + asterisco + "</label>");
				if (getTooltip() != null) {
					sb.append("<span class=\"help-button popover-warning\" data-rel=\"popover\" data-trigger=\"hover\" data-placement=\"left\" data-content=\""+getTooltip()+"\" title=\"Ajuda\">?</span>");
				}

			} 
		}
		return sb.toString();
	}

	public String getLabelAfter() {
		StringBuilder sb = new StringBuilder();

		if (getLabel() != null) {
			sb.append(  "</div>");
			sb.append(  "<div class=\"col-sm-1 red\" id=\""+ getId() +"Resultado\" class=\"help-inline red\"></div>");
		}
		sb.append("</div>");

		return sb.toString();
	}

	public String getCssClass() {
		return this.cssClass;
	}
	public String getCssStyle() {
		return this.cssStyle;
	}
	public void setCssStyle(String css) {
		this.cssStyle = css;
	}
	public void setCssClass(String css) {
		this.cssClass = css;
	}

	public String getMask() {
		return mask;
	}

	public void setMask(String mask) {
		if (mask.equalsIgnoreCase(Mask.TELEFONE)) {
			addCssClass("telefonefield");
		}
		if (mask.equalsIgnoreCase(Mask.EMAIL)) {
			addCssClass("emailfield");
		} else {
			this.mask = "$('#"+ getId() +"').mask('"+ mask +"');";
		}
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public String getTooltip() {
		return this.tooltip;
	}

	public void setIconBefore(CssIconEnum cssIconEnum) {
		this.iconBefore = cssIconEnum;
	}

	public String getIconBefore(Integer size, CssIconEnum icon) {
		return getIconBefore(size, icon, "space-3 ");
	}
	public String getIconBefore(Integer size, CssIconEnum iconBefore, String temp) {
		this.iconBefore = iconBefore;
		StringBuilder sb = new StringBuilder();

		if (getInLine() == true) size = 12;

		if (temp == null) temp = "";

		sb.append(  "<div class=\"input-group "+ temp +" col-sm-"+ size +" col-xs-"+ size +"\">");
		if (iconBefore != null) {
			sb.append("");
			sb.append( "<span class=\"input-group-addon\">");
			sb.append(     "<i class=\""+ iconBefore.getDescricao() +"\"></i>");
			sb.append( "</span>");
			sb.append( "</span>");
		}
		if(iconAfter != null) {
			sb.append( "<span class=\"block input-icon input-icon-right\">");
		}
		return sb.toString();
	}

	public void setIconAfter(CssIconEnum iconAfter) {
		this.iconAfter = iconAfter;
	}

	public String getIconAfter() {
		StringBuilder sb = new StringBuilder();
		if (iconAfter != null) {
			sb.append(     "<i class=\""+ iconAfter.getDescricao() +"\"></i>");
			sb.append(   "</span>");
		}
		sb.append(  "</div>");
		return sb.toString();
	}

	public String getScript() {
		String str = script.toString();

		str += scriptShowWhen.createScript(this);
		return StringUtils.hasValueWithTrim(str) == true ? str : "";
	}

	private ShowWhen scriptShowWhen = new ShowWhen();
	public void scriptShowWhen(AbstractFieldHtml field, ConditionJs conditionJs, Object ... values) {
		scriptShowWhen.add(field, conditionJs, values);
	}

	public void scriptShowWhen(Operation operation, AbstractFieldHtml field, ConditionJs conditionJs, Object ... values) {
		scriptShowWhen.add(operation, field, conditionJs, values);
	}


	public void scriptSameValue(AbstractFieldHtml fiel) {
		StringBuilder sb = new StringBuilder();
		sb.append("$('#"+ fiel.getId() +"').change(function(){");
		sb.append("$('#"+ getId() +"').val($(this).val()).change();");
		sb.append("});");

		this.script.append( sb.toString() );
	}

	public void scriptHasSameValueDB(Class classe) {
		super.addAttr("data-bean", classe.getName());
	}


}
