package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.view.HtmlHelper;
import br.com.dotum.core.servlet.ServletHelper;
import br.com.dotum.jedi.core.db.AbstractJson;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.StringUtils;

public class AutoCompleteFieldHtml extends AbstractFieldHtml {
	private static Log LOG = LogFactory.getLog(AutoCompleteFieldHtml.class);

	private AbstractFieldHtml fieldDependente;
	private Bean bean;
	private Integer value;
	private Class<? extends Bean> beanClass;
	private Class<? extends AbstractJson> jsonClass;
	private String filterBy = "descricao;"+Condition.LIKE;
	private List<String> filters = new ArrayList<String>();
	private List<AbstractFieldHtml> fillField = new ArrayList<AbstractFieldHtml>();
	private List<String> fillValue = new ArrayList<String>();

	private String propertyKey = "id"; // Como será montado o valor
	private String[] property; // valores que irão retornar (esquema de navegação dos Beans)
	private String propertyFormat; // Como será montado a lista

	private CssIconEnum iconBefore;

	String PRE_STR = "#pre";
	String POS_STR = "#pos";
	String PRE_STR1 = "'+item['";
	String POS_STR2 = "'] +'";


	public AutoCompleteFieldHtml(String name, String label, Object value, boolean required) throws Exception {
		super.setTypeValue(Integer.class);

		if (name.endsWith("Id"))
			name = name.substring(0, name.length() - 2);
		if (name.endsWith("id"))
			name = name.substring(0, name.length() - 2);

		setName(name);
		setLabel(label);
		setRequired(required);
		setValue(value);

		super.setPrefix("ac");
	}

	public void setProperty(String key, String... displayArray) throws DeveloperException {
		this.propertyKey = key;

		if ((beanClass == null) && (jsonClass == null))
			throw new DeveloperException("Antes de chamar o metodo \"setProperty\" é preciso chamar o metodo \"setClassBean\" ou \"setClassJson\"");

		String[] array = new String[displayArray.length];

		for (int i = 0; i < displayArray.length; i++) {
			String displayItem = displayArray[i];
			if (beanClass != null) {
				// criar um esquema para validar auqi
			}

			array[i] = displayItem.toLowerCase();
		}
		this.property = array;
	}

	public void setClassJson(Class<? extends AbstractJson> jsonClass) {
		this.jsonClass = jsonClass;

		String displayItem = "Descricao";
		if (jsonClass != null) {
			String className = jsonClass.getSimpleName();
			className = className.substring(0, className.length() - 4);

			displayItem = displayItem.toLowerCase();
		}
		this.property = new String[] { displayItem };
	}

	public void setClassBean(Class<? extends Bean> beanClass) {
		this.beanClass = beanClass;

		String displayItem = "Descricao";
		if (beanClass != null) {
			String className = beanClass.getSimpleName();
			className = className.substring(0, className.length() - 4);

			displayItem = displayItem.toLowerCase();
		}
		this.property = new String[] { displayItem };
	}


	public void setFilterBy(String ... filterBy) {
		setFilterBy(null, filterBy);
	}


	public void setFilterBy(Condition condition, String... filterBy) {
		if (condition == null) condition = condition.LIKE;

		StringBuilder sb = new StringBuilder();
		String temp = "";
		for (String str : filterBy) {
			sb.append(temp);
			sb.append(str);
			sb.append(";");
			sb.append(condition);
			temp = "#";
		}
		this.filterBy = sb.toString();
	}

	@Override
	public String getComponente() throws Exception {
		prepareValues();

		super.addCssClass("form-control ui-autocomplete-input");
		// super.setPlaceholder("...");
		super.addAttr("id", getPrefix() + getId());
		super.addAttr("type", "text");
		super.addAttr("autocomplete", "off");
		super.addAttr("class", getCssClassComponent());

		// isso aqui nao é o getName() por causa do padrao do Bean / Json do Transaction
		// no caso usa no alterar do form padrao!!!
		super.addAttr("name", getNamePog());
		super.addAttr("value", getValueToHtml());
		// especifico
		if (hasAttr("placeholder") == false) {
			super.setPlaceholder("digite pelo menos " + getMinlength() + " letra(s)...");
		}

		StringBuilder sb = new StringBuilder();
		sb.append(super.getLabelBefore());
		//		sb.append(super.getIconBefore(12, getIconBefore()));
		if(isIcon() == false) {
			sb.append( super.getIconBefore(12, null ) );
		} else {
			sb.append(super.getIconBefore(12, CssIconEnum.SEARCH));
		}
		sb.append("<input");
		sb.append(super.getAttrStr());
		sb.append(">");
		sb.append("<input id=\"" + super.getId() + "\" type=\"hidden\" name=\"" + getName() + "id\" value=\""+ getValue() + "\"/>");
		sb.append(super.getIconAfter());
		sb.append(super.getLabelAfter());
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.getScript());

		if (fieldDependente != null) {
			sb.append("$('#" + fieldDependente.getId() + "').change(function(){");
			sb.append("  $('#" + super.getId() + "').val('');");
			sb.append("  $('#" + super.getPrefix() + super.getId() + "').val('');");
			sb.append("});");
		}

		sb.append("$('#" + super.getPrefix() + super.getId() + "' ).autocomplete({");
		sb.append("minLength: " + getMinlength() + ",");

		String displayStr = "";
		String temp = "";
		for (String displayItem : this.property) {
			displayStr += temp;
			displayStr += displayItem;
			temp = ",";
		}
		String filterStr = "";
		temp = "";
		for (String filter : filters) {
			filterStr += temp;
			filterStr += filter;
			temp = "#";
		}

		//
		// INI SOURCE
		//

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", this.getName());
		if ((StringUtils.hasValue(propertyKey)) && (propertyKey.equalsIgnoreCase("id") == false))
			map.put("key", propertyKey);
		if ((StringUtils.hasValue(filterBy)) && (filterBy.equalsIgnoreCase("descricao") == false))
			map.put("filterBy", filterBy);
		if ((StringUtils.hasValue(displayStr)) && (displayStr.equalsIgnoreCase("descricao") == false))
			map.put("display", displayStr);
		if (StringUtils.hasValue(filterStr))
			map.put("filter", filterStr);
		if (beanClass != null)
			map.put("beanClass", beanClass.getName());
		if (jsonClass != null)
			map.put("jsonClass", jsonClass.getName());
		sb.append("data: '{" + Ajax.mapToJson(map) + "}', ");

		// sb.append("xx: '',");
		sb.append("formatResult: '{\"id\": " + PRE_STR + "id" + POS_STR + ",");
		if (fillField.size() > 0)
			sb.append(getFillValueStr() + ",");
		// sb.append("\"value\": \"" + getPropertyValueAndLabelStr() + "\",");
		sb.append("\"label\": \"" + getPropertyValueAndLabelStr() + "\"}'");

		// sb.append("propertyValueAndLabel: '" + getPropertyValueAndLabelStr() + "',");
		// sb.append("fillValue: '" + getFillValueStr() + "'");

		//
		// FIM SOURCE
		//
		if (fillValue.size() > 0) {
			sb.append(",");
			sb.append("select: function( event, ui ) {");
			// o next do jQuery pega o proximo elemento, neste caso o input, e o val seta o
			// valor do elemento.
			sb.append("$(this).next().val(ui.item.id).change();");

			for (int i = 0; i < fillValue.size(); i++) {
				sb.append("var f = $('#" + fillField.get(i).getPrefix() + fillField.get(i).getId() + "');");
				sb.append("var p = $('#" + fillField.get(i).getId() + "');");
				sb.append("var _vt=ui.item." + getFillFormatStr(i) + ";");
				sb.append("if ((_vt) && (_vt != 'null')) {");
				// Vamos lá, ao que parece aqui tem um lance de formação não concuido
				sb.append("if ($(f).attr('type') == 'hidden') {");
				sb.append("$(p).val(_vt).change();");
				sb.append("} else if (typeof parseFloat(_vt) === 'number' && isInteger(parseFloat(_vt))) {");
				sb.append("  _vt = formatDecimalDec(parseFloat(_vt), 0);");
				/*
				 * Foi idenditicado que o parseFolt aqui estava retornando type === number mesmo
				 * quando era testo literal contendo numeros e outras palavra /* sb.
				 * ); Foi alterado para o modelo baixo que ficou funcionado
				 * 
				 * Continuacao da novela capitlo 2 - 09/11/2018
				 * linha comentada pq estava ignorando e jogando milhar; Ex. 7.9;
				 * para funcionar decimal certo voltamos ao que estava antes;
				 * se der erro em outro cara qualquer, verificar como fazer
				 * 
				 */
				sb.append("} else if ((typeof parseFloat(_vt) === 'number') && (isDecimal(parseFloat(_vt)))) {");
//				sb.append("} else if ((typeof _vt === 'number') && (isDecimal(parseFloat(_vt)))) {");
				sb.append("  _vt = formatDecimalDec(parseFloat(_vt), 5);");
				sb.append("} ");
				sb.append("if ($(f).is(':input')) {");
				sb.append("$(f).val(_vt).change();");
				sb.append("} else {");
				sb.append("$(f).text(_vt);");
				sb.append("}");
				sb.append("}");
			}
			sb.append("}");
		}
		sb.append("});");

		if (getMinlength() == 0) {
			sb.append("$('#" + super.getPrefix() + super.getId() + "').focus(function(){");
			sb.append("$('#" + super.getPrefix() + super.getId() + "').autocomplete( \"search\", \"\" );");
			sb.append("});");
		}
		// sb.append("$( '#"+ super.getPrefix()+super.getId() +"'
		// ).autocomplete(\"option\", \"position\", { my : \"right-10 top+10\", at:
		// \"right top\", collision: \"fit none\" });");

		return sb.toString();
	}

	private int getMinlength() {
		Integer xxx = NumberUtils.parseInt((String) super.getAttr("minlength"));
		return xxx == null ? 2 : xxx;
	}

	@Override
	public void setValue(Object value) {
		try {
			if (value instanceof String) {
				this.value = FormatUtils.parseInt((String) value);
			} else {
				this.value = (Integer) value;
			}
		} catch (Exception e) {
			LOG.error("Erro ao converter o valor " + value + " para o campo " + getName(), e);
		}
	}

	public String getValue() {
		if (value == null)
			return null;
		return "" + value;
	}

	public String getValueToHtml() {
		if (bean == null)
			return "";

		String temp = "";
		StringBuilder sb = new StringBuilder();
		for (String str : property) {
			Object obj = bean.getAttribute(str);

			sb.append(temp);
			if (obj instanceof Integer) {
				sb.append((Integer) obj);
			} else if (obj instanceof String) {
				sb.append((String) obj);
			}
			temp = " - ";
		}

		return sb.toString();
	}

	public void prepareValues() throws Exception {
		UserContext userContext = super.getHtml().getUserContext();
		if ((value != null) && (this.beanClass != null)) {
			TransactionDB trans = null;
			try {
				trans = TransactionDB.getInstance(userContext);
				this.bean = trans.selectById(this.beanClass, value);

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw e;
			} finally {
				trans.close();
			}
		} else if ((value != null) && (this.jsonClass != null)) {
			TransactionDB trans = null;
			try {
				trans = TransactionDB.getInstance(userContext);

				AbstractJson json = jsonClass.newInstance();
				json.setTransaction(trans);
				json.setUserContext(userContext);
				json.setProperty(propertyKey, property);
				
				WhereDB where = new WhereDB();
				where.add(propertyKey, Condition.EQUALS, value);
				json.setWhere(where);

				List<Bean> model = null;
				try {
					model = json.run(filterBy);
					this.bean = model.get(0);
				} catch (NotFoundException e) {
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw e;
			} finally {
				trans.close();
			}
		}
	}

	public void addFilterByMaster(String attribute) {
		filters.add(attribute + ";" + Condition.EQUALS + ";\\' + $(\"#" + getId()
		+ "\").parents(\"form:first\").children(\\'input[name=\"" + FWConstants.PARAM_FW_MASTER_ID
		+ "\"]\\').val() + \\'");
	}

	public void addFilter(String attribute, Condition condition, AbstractFieldHtml field) {
		filters.add(attribute + ";" + condition + ";\\' + $(\"#" + field.getId() + "\").val() + \\'");
		fieldDependente = field;
	}

	public void addFilter(String attribute, Condition condition, Object value) {
		if (value instanceof Integer[]) {
			String temp = ArrayUtils.stringToIn((Integer[]) value);
			filters.add(attribute + ";" + condition + ";" + temp.substring(1, temp.length() - 1));
		} else {
			filters.add(attribute + ";" + condition + ";" + value);
		}
	}

	public void addFillField(AbstractFieldHtml field, String value) {
		if (field == null)
			return;

		this.fillField.add(field);
		this.fillValue.add(value);
	}

	private String getFillValueStr() {
		if (fillValue.size() == 0)
			return HtmlHelper.VAZIO;

		StringBuilder sb = new StringBuilder();
		String temp = "";
		for (int i = 0; i < fillField.size(); i++) {
			AbstractFieldHtml field = fillField.get(i);

			sb.append(temp);
			sb.append("\"" + field.getId() + "\":\"");
			temp = fillValue.get(i);
			if (temp == null)
				temp = HtmlHelper.VAZIO;
			for (int j = 0; j < property.length; j++) {
				String label = property[j];
				temp = temp.replaceAll("\\{" + j + "\\}", "" + PRE_STR + "" + label + "" + POS_STR + "");
			}
			sb.append(temp);
			sb.append("\"");
			temp = ",";
		}

		return sb.toString();
	}

	private String getPropertyValueAndLabelStr() {
		String temp = getPropertyFormat();
		for (int i = 0; i < property.length; i++) {
			String label = property[i];
			temp = temp.replaceAll("\\{" + i + "\\}", "" + PRE_STR + "" + label + "" + POS_STR + "");
		}
		return temp;
	}

	private String getPropertyFormat() {
		String xxx = propertyFormat;

		String temp = "";
		if (xxx == null) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < property.length; i++) {
				sb.append(temp);
				sb.append("{" + i + "}");
				temp = " - ";
			}
			xxx = sb.toString();
		}

		return xxx;
	}

	public String getNamePog() {
		return getName() + "." + property[0];
	}

	public void setPropertyFormat(String propertyFormat) {
		this.propertyFormat = propertyFormat;
	}

	public void  setIconBefore(CssIconEnum iconBefore) {
		this.iconBefore = iconBefore;
	}

	public CssIconEnum getIconBefore() {
		return this.iconBefore;
	}

	private String getFillFormatStr(int index) {
		if (fillField.size() == 0)
			return HtmlHelper.VAZIO;

		String temp = fillField.get(index).getId();
		for (int i = 0; i < fillField.size(); i++) {
			String label = temp;
			temp = temp.replaceAll("\\{" + i + "\\}", "'+item." + label + "+'");
		}
		return temp;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}

}

