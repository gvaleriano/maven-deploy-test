package br.com.dotum.core.html.form.field;

import java.text.ParseException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.view.HtmlHelper;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.table.TableFieldEditType;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;


public class CalcField extends AbstractFieldHtml {
	private static Log LOG = LogFactory.getLog(CalcField.class);

	public static final Integer SUM = 1;
	public static final Integer MENUS = 1;
	public static final String SUM_STR = " + ";
	public static final String MENUS_STR = " - ";


	private Double value;
	private String formula1;
	private Integer precision1 = 2;
	private String formula2;
	private Integer precision2 = 2;
	private AbstractFieldHtml[] fields1;
	private AbstractFieldHtml[] fields2;

	public CalcField(String label) {
		this.setType(TableFieldEditType.CALCFIELD);
		setLabel(label);
	}

	public void setFormula1(String formula, Integer precision) {
		this.formula1 = formula;
		this.precision1 = precision;
	}
	public void setFormula2(String formula, Integer precision) {
		this.formula2 = formula;
		this.precision2 = precision;
	}

	public void setFormula1(String formula) {
		this.formula1 = formula;
	}
	public void setFormula2(String formula) {
		this.formula2 = formula;
	}

	public void setFields1(AbstractFieldHtml ... fields) {
		this.fields1 = fields;
	}

	public void setFields2(AbstractFieldHtml ... fields) {
		this.fields2 = fields;
	}

	public void addLink() {
	}

	@Override
	public void createScript() throws DeveloperException {
		beforeCreate();

		if (StringUtils.hasValue(formula1)) {
			String script = criaScriptPorFormula("calcFor1", formula1, precision1, fields1); 
			getHtml().addScript(script);
		}

		if (StringUtils.hasValue(formula2)) {
			String script = criaScriptPorFormula("calcFor2", formula2, precision2, fields2); 
			getHtml().addScript(script);
		}
	}

	private String criaScriptPorFormula(String preFix, String formula, Integer precision, AbstractFieldHtml[] fields) throws DeveloperException {
		StringBuilder sb = new StringBuilder();
		sb.append("function "+ preFix + getId() +"(){");
		if (fields != null) {
			for (int i = 0; i < fields.length; i++) {
				AbstractFieldHtml field = fields[i];
				String id = field.getId();

				String valueJS = " parseDecimal(\\$('\\#"+ id +"').val()) ";
//				if (field.getType() == TableFieldEditType.INTEGERFIELD) {
//					valueJS = " parseInteger(\\$('\\#"+ id +"').val()) ";
//				} else if (field.getType() == TableFieldEditType.DECIMALFIELD) {
//					
//				} else if (field.getType() == TableFieldEditType.CALCFIELD) {
//					valueJS = " parseDecimal(\\$('\\#"+ preFix + id +"').html()) ";
//				} else {
//					throw new DeveloperException("Não é possivel fazer calculo com tipo de campo " + TableFieldEditType.getFieldById(field.getType()).getType() + " OU AINDA NAO FOI IMPLEMENTADO...");
//				}

				formula = formula.replaceAll("\\{"+ i +"\\}", valueJS);
			}
		}
		String maskIni = "";
		String maskFim = "";
		if (precision != null) {
			maskIni = "formatDecimalDec(";
			maskFim = ", "+ precision +")";
		}
		sb.append(" $('#" + preFix + getId() +"').html("+ maskIni + formula + maskFim +");");
		sb.append("};" + ResultHtml.PL);

		if (fields != null) {
			for (AbstractFieldHtml f : fields) {
				sb.append("$('#"+ StringUtils.coalesce(f.getId(), f.getName()) +"').blur(function () {");
				sb.append(preFix + getId() +"();");
				sb.append("});" + ResultHtml.PL);
			}
		}
		sb.append(preFix + getId() +"();" + ResultHtml.PL);

		return sb.toString();
	}

	public void beforeCreate() throws DeveloperException {
		if (StringUtils.hasValue(formula1)) validarFormula(formula1, fields1);
		if (StringUtils.hasValue(formula2)) validarFormula(formula2, fields2);
	}


	private void validarFormula(String formula, AbstractFieldHtml[] fields) throws DeveloperException {
		if (formula == null) 
			throw new DeveloperException ("Não foi definido a formula para o CalcFieldFormHtml " + getId());

		Pattern pattern = Pattern.compile("\\{[0-9]\\}"); 
		Matcher matcher = pattern.matcher(formula); 
		// Mostra as similaridades
		int fieldsSize = 0;
		while (matcher.find()) {
			String x = matcher.group();

			String temp = x.substring(1, x.length()-1);
			Integer fieldNumber = 0;
			try {
				fieldNumber = FormatUtils.parseInt(temp);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if (fieldNumber > fieldsSize) {
				fieldsSize = fieldNumber;
			}

		}
		if ((fields != null) && ((fieldsSize+1) > fields.length)) 
			throw new DeveloperException ("A quantidade de fields da formula (" + fieldsSize+1 + ") não confere com a quantidade de fields (" + fields.length + ") informados.");

		if (StringUtils.hasValue(getLabel()) == false) {
			String formulaTemp = formula;
			for (int i = 0; i < fields.length; i++) {
				AbstractFieldHtml field = fields[i];
				formulaTemp = formulaTemp.replaceAll("\\{"+ i +"\\}", field.getLabel());
			}
			setLabel(formulaTemp);
		}
	}

	@Override
	public String getComponente() throws DeveloperException {
		beforeCreate();

		Integer size = 8;
		String icon = null;
		addCssClass("form-control");
		super.addAttr("id", getId());
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( getInput() );
		sb.append( super.getLabelAfter() );

		return sb.toString();
	}


	public String getInput() {
		StringBuilder sb = new StringBuilder();
		sb.append("<span ");
		sb.append(" class=\"col-sm-12 control-label\"");
		sb.append( ">" );
		
		sb.append("<span ");
		sb.append(" id=\"calcFor1"+ getId() +"\"");
		sb.append(" style=\"text-align: right;\"");
		sb.append(" class=\"col-sm-6\"");
		sb.append( ">" );
		sb.append( "0,0" );
		sb.append( "</span>" );

		sb.append("<span ");
		sb.append(" id=\"calcFor2"+ getId() +"\"");
		sb.append(" style=\"text-align: right;\"");
		sb.append(" class=\"col-sm-6\"");
		sb.append( ">" );
		sb.append( "" );
		sb.append( "</span>" );
		
		sb.append( "</span>" );

		return sb.toString();
	}

	@Override
	public Object getValue() {
		return "0.0";
	}


	@Override
	public String getValueToHtml() {
		if (value == null) {
			return HtmlHelper.VAZIO;
		} else {
			return FormatUtils.formatDouble(value);
		}
	}


	@Override
	public void setValue(Object value) {
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}

}


