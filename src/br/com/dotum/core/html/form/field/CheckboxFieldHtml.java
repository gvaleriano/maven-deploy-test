package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.StringUtils;

public class CheckboxFieldHtml extends AbstractFieldHtml {
	private static Log LOG = LogFactory.getLog(CheckboxFieldHtml.class);

	private Class<? extends Bean> bean;
	private String key;
	private String[] display;

	private Object value;
	private List<Object> values = new ArrayList<Object>();
	private List<String> labels = new ArrayList<String>();

	public CheckboxFieldHtml(String name, String label, String key, String[] display, Class<? extends Bean> bean, Integer value, boolean required) throws Exception {
		setTypeValue(Integer[].class);
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);

		this.key = key;
		this.display = display;
		this.bean = bean;

		prepareValues();
	}
	public CheckboxFieldHtml(String name, String label, Object value, boolean required) {
		setTypeValue(Integer[].class);
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);
	}
	
	@Override
	public String getComponente() {
		StringBuilder sb = new StringBuilder();

		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null ) );

		
		
//		<label>
//			<input name="form-field-checkbox" type="checkbox" class="ace">
//			<span class="lbl"> choice 1</span>
//		</label>
//	</div>
		

		for (int i = 0; i < values.size(); i++) {
			
			sb.append("<div class=\"checkbox\">");
			sb.append("<label>");
			
			sb.append("<input ");
			if (values.size() == 1)
				sb.append(" id=\""+ getId() +"\"");
			
			sb.append(" type=\"checkbox\"");
			sb.append(" class=\"ace ace-checkbox-2\"");
			sb.append(" name=\""+ getName() +"\"");

			String vStr = "";
			Object v = values.get(i);
			String l = labels.get(i);

			String part = null;
			if (v == null) {
				part = "";
				vStr = " value=\"\" ";
			} else if (v instanceof Integer) {
				part = (v.equals((Integer)value) ? " checked=\"checked\"" : "");
				vStr = " value=\""+v+"\" ";
			} else if (v instanceof Boolean) {
				part = (v.equals((Boolean)value) ? " checked=\"checked\"" : "");
				vStr = " value=\""+v+"\" ";
			} else if (v instanceof String) {
				part = (v.equals((String)value) ? " checked=\"checked\"" : "");
				vStr = " value=\""+v+"\" ";
			}
			sb.append( vStr + part +" />" );
			sb.append( "<span class=\"lbl\">"+StringUtils.coalesce(l, "")+"</span>" + ResultHtml.PL);
			
			sb.append("</label>");
			sb.append("</div>");

		}
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	private void prepareValues() throws Exception {
		List<? extends Bean> beanList = null;
		TransactionDB trans = null;
		try{
			trans = TransactionDB.getInstance(null);
			try {
				beanList = trans.select(bean);
			} catch (Exception e) {
				beanList = new ArrayList<Bean>();
			}
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			throw e;
		}finally{
			trans.close();
		}

		labels = new ArrayList<String>();

		labels.add("");
		values.add(null);
		if (beanList.size() > 0) {
			for (Bean bean : beanList) {
				Integer value = (Integer)bean.getAttribute(key);


				String temp = "";
				StringBuilder sb = new StringBuilder();
				for (String str : display) {
					Object obj = bean.getAttribute(str);

					sb.append(temp);
					if (obj instanceof Integer) {
						sb.append( (Integer)obj);
					} else if (obj instanceof String) {
						sb.append( (String)obj);
					}
					temp = " - ";
				}

				labels.add(sb.toString());
				values.add(value);
			}
		}
	}

	public void setValue(String value) {
		this.value = value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}
	
	@Override
	public String getScript() {
		return "";
	}

	@Override
	public void setValue(Object value) {
		this.value = value;	
	}
	@Override
	public Object getValue() {
		return null;
	}
	@Override
	public String getValueToHtml() {
		return null;
	}
	
	public void addOption(Integer value, String label) {
		this.values.add( value );
		this.labels.add( label );
	}
	public void setValues(Integer ... values) {
		for  (Integer value : values) {
			this.values.add( value );
		}
	}
	public void setLabels(String ... labels) {
		for  (String label : labels) {
			this.labels.add( label );
		}
	}
	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}
}

