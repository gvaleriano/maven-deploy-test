package br.com.dotum.core.html.form.field;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.dotum.jedi.core.db.AbstractJson;
import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.FormatUtils;

public class ComboNumberFieldHtml extends AbstractFieldHtml {
	private static Log LOG = LogFactory.getLog(ComboNumberFieldHtml.class);

	private Class<? extends Bean> beanClass;
	private Class<? extends AbstractJson> jsonClass;
	private WhereDB where = new WhereDB();
	private String propertyKey = "Id";
	private String[] property = new String[] {"Descricao"};

	private Integer value;
	private List<Integer> values = new ArrayList<Integer>();
	private List<String> labels = new ArrayList<String>();

	public ComboNumberFieldHtml(String name, String label, List<Integer> values, List<String> labels, Integer value, boolean required) throws Exception {
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);

		if (values != null) this.values = values;
		if (labels != null) this.labels = labels;
	}

	public ComboNumberFieldHtml(String name, String label, Object value, boolean required) {
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);
	}

	@Override
	public String getComponente() throws Exception {
		prepareValues();

		super.addCssClass("form-control");
		super.setPlaceholder("...");
		super.addAttr("id", getId());
		super.addAttr("type", "text");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		
		
		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null ) );
		sb.append("<select");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append("<option value=\"\">&nbsp;</option>");
		if(values.size() > 0){
			for (int i = 0; i < values.size(); i++) {
				String vStr = "";
				Object v = values.get(i);
				String l = labels.get(i);
				
				String part = null;
				if (v == null) {
					part = "";
					vStr = "";
				} else if (v instanceof Integer) {
					part = (v.equals((Integer)value) ? " selected=\"selected\"" : "");
					vStr = ""+v;
				}
				sb.append("<option value=\""+ vStr +"\""+ part +">" + l + "</option>");
			} 
		} else {
			sb.append( "<option value=\"\">"+ "Cadastre " + super.getLabel()  +"</option>");
		}
		sb.append("</select>");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	private void prepareValues() throws Exception {
		if ((beanClass == null) && (jsonClass == null)) return; 

		TransactionDB trans = null;
		List<? extends Bean> beanList = null;
		try{
			trans = TransactionDB.getInstance(null);
			if (beanClass != null) {
				OrderDB orders = new OrderDB();
				for (String attribute : property) {
					orders.add(attribute);
				}
				beanList = trans.select(beanClass, where, orders);
			} else if (jsonClass != null) {
				AbstractJson json = (AbstractJson)jsonClass.newInstance();
				json.setTransaction(trans);
				json.setWhere(where);
				beanList = json.run();
			}
		}catch(NotFoundException e){
			beanList = new ArrayList<Bean>();
		}catch(Exception e){
			beanList = new ArrayList<Bean>();
			
			LOG.error("Problema ao montar o campo" + this.getName() + ". Motivo: " + e.getMessage(), e);
		}finally{
			trans.close();
		}


		if (beanList.size() > 0) {
			for (int i = 0; i < beanList.size(); i++) {
				Bean bean = beanList.get(i);
				Integer value = (Integer)bean.getAttribute(propertyKey);


				String temp = "";
				StringBuilder sb = new StringBuilder();
				for (String str : property) {
					Object obj = bean.getAttribute(str);

					sb.append(temp);
					if (obj instanceof Integer) {
						sb.append( (Integer)obj);
					} else if (obj instanceof String) {
						sb.append( (String)obj);
					} else if (obj instanceof BigDecimal) {
						sb.append( (BigDecimal)obj);
					}
					temp = " - ";
				}

				labels.add( sb.toString() );
				values.add( value );
			}
		}
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	@Override
	public Class<?> getTypeValue() {
		return Integer.class;
	}
	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );
//		sb.append( "$('#"+getId()+"').change(function(){console.log($(this).val());});");
		return sb.toString();
	}

	@Override
	public void setValue(Object value) {
		try {
			if (value instanceof String) {
				this.value = FormatUtils.parseInt((String) value);	
			} else if (value instanceof Double) {
				this.value = ((Double)value).intValue();	
			} else {
				this.value = (Integer)value;	
			}
		} catch (Exception e) {
			LOG.error("Erro ao converter o valor " + value + " para o campo " + getName(), e);
		}
	}

	@Override
	public Object getValue() {
		return value;
	}
	@Override
	public String getValueToHtml() {
		return ""+value;
	}
	public void addOption(Integer value, String label) {
		this.values.add( value );
		this.labels.add( label );
	}
	public void setValues(Integer ... values) {
		for  (Integer value : values) {
			this.values.add( value );
		}
	}
	public void setLabels(String ... labels) {
		for  (String label : labels) {
			this.labels.add( label );
		}
	}
	public void setProperty(String key, String ... display) {
		this.propertyKey = key;
		this.property = display;
	}
	public void setClassBean(Class<? extends Bean> beanClass) {
		this.beanClass = beanClass;
	}
	public void addFilter(String field, Condition condition, Object value) throws DeveloperException {
		if ((beanClass == null) && (jsonClass == null))  
			throw new DeveloperException("Antes de invocar o metodo \"addFilter\" do ComboNumberFieldHtml (Name: " + getName() + ") chame o metodo \"setClassBean\" ou \"setClassJson\".");

		where.add(field, condition, value);
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}


}

