package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public class ComboTextFieldHtml extends AbstractFieldHtml {
	
	private Object value;
	private List<String> values = new ArrayList<String>();
	private List<String> labels = new ArrayList<String>();

	public ComboTextFieldHtml(String name, String label, String value, boolean required) {
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);
	}
	
	public void setValues(String ... values) {
		for  (String value : values) {
			this.values.add( value );
		}
	}

	public void setLabels(String ... labels) {
		for  (String value : labels) {
			this.labels.add( value );
		}
	}
	public void addOption(String value, String label) {
		this.values.add( value );
		this.labels.add( label );
	}
	
	public void setValues(List<String> values) {
		this.values = values;
	}
	
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	
	@Override
	public String getComponente() throws DeveloperException {
		if (values == null) throw new DeveloperException("Não foi definido os valores para o combo \"" + getName() + "\" com o label \"" +getLabel() + "\".");
		if (labels == null) throw new DeveloperException("Não foi definido os labels para o combo \"" + getName() + "\" com o label \"" +getLabel() + "\".");

		
		super.addCssClass("form-control");
//		super.setPlaceholder("...");
		super.addAttr("id", getId());
//		super.addAttr("type", "text");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		
		
		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null ) );
		sb.append("<select");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append("<option value=\"\">&nbsp;</option>" + ResultHtml.PL);
		for (int i = 0; i < values.size(); i++) {
			String vStr = "";
			
			Object v = values.get(i);
			String l = labels.get(i);

			String part = null;
			if (v == null) {
				part = "";
				vStr = "";
			} else if (v instanceof String) {
				part = (v.equals((String)value) ? " selected=\"selected\"" : "");
				vStr = ""+v;
			}
			sb.append("<option value=\""+ vStr +"\""+ part +">" + l + "</option>" + ResultHtml.PL);
		}

		sb.append("</select>" + ResultHtml.PL);
		
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public Class<?> getTypeValue() {
		return String.class;
	}
	
	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}
	
	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );
		return sb.toString();
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}
	
	@Override
	public Object getValue() {
		return null;
	}
	@Override
	public String getValueToHtml() {
		return null;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}

}

