package br.com.dotum.core.html.form.field;

public enum ConditionJs {
	EQUALS("=="),
	GREATOREQUALS(">="),
	LESSOREQUALS("<="),
	NOTEQUALS("!="),
	CHECKED("is"),
	UNCHECKED("not"),
;
	private String value;
	ConditionJs(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}

