package br.com.dotum.core.html.form.field;

import java.util.HashMap;
import java.util.Map;

import br.com.dotum.jedi.util.StringUtils;

public class CpfCnpjFieldHtml extends AbstractFieldHtml {

	private String value;

	public CpfCnpjFieldHtml(String name, String label, String value, boolean required) {
		super.setTypeValue(String.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);

		this.value = value;
	}

	@Override
	public String getComponente() {
		super.addCssClass("form-control block hibridCpf");
		super.setPlaceholder("...");
		super.addAttr("id", getId());
		super.addAttr("type", "text");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());



		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null ) );
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );
//		sb.append("$('#"+getId()+"').mask('999.999.999-99');");
//		sb.append("$('#"+getId()+"').mask('99.999.999/9999-99');");

		return sb.toString();
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}

	public String getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (StringUtils.hasValue(value) == false) return "";
		return ""+value;
	}

	@Override
	public Map<String, String> getValidate() {
		Map<String, String> map = new HashMap<String, String>(); 
		StringBuilder role = new StringBuilder();
		StringBuilder msg = new StringBuilder();
		
		String part = "";
		if (StringUtils.hasValue((String)super.getAttr("data-bean"))) 
			part = ", sameValueDB: true";
		
		role.append("'"+getName()+"': {cpfCnpj: true"+ part +"}");
		msg.append("'"+getName()+"':{}");

		map.put(role.toString(),msg.toString());
		return map;
	}

}

