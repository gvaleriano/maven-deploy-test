package br.com.dotum.core.html.form.field;

import java.util.Date;
import java.util.Map;

import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.servlet.DoServlet;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.FormatUtils;

public class DateFieldHtml extends AbstractFieldHtml {
	private static Log LOG = LogFactory.getLog(DateFieldHtml.class);

	private Date value;


	public DateFieldHtml(String name, String label, Date value, boolean required) {
		super.setTypeValue(Date.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);

		this.value = value;
	}

	@Override
	public String getComponente() throws Exception {
		super.addCssClass("datepicker col-sm-12");
		//		super.setPlaceholder("...");
		super.addAttr("id", getId());
		super.addAttr("type", "text");
		super.addAttr("autocomplete", "off");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());



		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		if(isIcon() == false) {
			sb.append( super.getIconBefore(6, null ) );
		} else {
			sb.append( super.getIconBefore(6, CssIconEnum.CALENDAR) );
		}
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();

		sb.append( super.getScript() );
		//		sb.append("$('#"+ this.getId() +"').mask('99/99/9999');");
		//		sb.append("$('#"+ this.getId() +"').datepicker({");
		//		sb.append("format:\"dd/mm/yyyy\",");
		//		sb.append("language:\"pt-BR\",");
		//		sb.append("autoclose:true,");
		//		sb.append("todayHighlight:true,");
		//		sb.append("toggleActive:false");
		//		sb.append("});");
		return sb.toString();
	}

	@Override
	public void setValue(Object value) {
		try {

			if (value instanceof Date) {
				this.value = (Date) value;	
			} else if (value instanceof String) {
				this.value = FormatUtils.parseDate((String) value);	
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	public Date getValue() {
		return value;
	}

	public String getValueToHtml() throws Exception {
		if (value == null) return "";
		return FormatUtils.formatDate(value);
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}
}

