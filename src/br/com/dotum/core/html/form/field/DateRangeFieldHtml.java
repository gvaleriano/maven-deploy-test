package br.com.dotum.core.html.form.field;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.FormatUtils;

@Deprecated
public class DateRangeFieldHtml extends AbstractFieldHtml {
	private static Log LOG = LogFactory.getLog(DateRangeFieldHtml.class);

	private Date[] value;

	public DateRangeFieldHtml(String name, String label, Date[] value, boolean required) {
		super.setTypeValue(Date.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);

		this.value = value;
	}

	@Override
	public String getComponente() throws Exception {
		super.addCssClass("form-control");
//		super.setPlaceholder("...");
//		super.addAttr("id", getId());
		super.addAttr("type", "text");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		
		
		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null, " input-daterange" ) );
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append("<span class=\"input-group-addon\">");
		sb.append("  <i class=\""+ CssIconEnum.EXCHANGE +"\"></i>");
		sb.append("</span>");
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );
		return sb.toString();
	}

	@Override
	public void setValue(Object value) {

		try {

			if (value instanceof Object[]) {
				this.value = ArrayUtils.parseToDate((Object[])value);
			} else if (value instanceof Date[]) {
				this.value = (Date[]) value;	
			} else {
				LOG.error("Situação nao prevista " + DateRangeFieldHtml.class.getName());
			}
		} catch (ParseException e) {
			LOG.error("Erro de parse", e);
		}

	}

	public Date[] getValue() {
		return value;
	}

	public String getValueToHtml() throws Exception {
		if (value == null) return "";
		return FormatUtils.formatDate(value[0]);
	}

	public String getValueToHtml2() throws Exception {
		if (value == null) return "";
		return FormatUtils.formatDate(value[1]);
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}
}

