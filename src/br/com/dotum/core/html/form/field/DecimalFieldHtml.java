package br.com.dotum.core.html.form.field;

import java.math.BigDecimal;
import java.util.Map;

import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.jedi.util.FormatUtils;

public class DecimalFieldHtml extends AbstractFieldHtml {

	private BigDecimal value;
	private Integer decimal = 2;

	public static final Integer MASK_MONEY = 1;
	public static final Integer MASK_AUTONUMERIC = 2;

	private Integer mask = MASK_AUTONUMERIC;


	public DecimalFieldHtml(String name, String label, BigDecimal value, boolean required) {
		super.setTypeValue(BigDecimal.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);

		super.addCssClass("decimal");
		this.value = value;
	}

	@Override
	public String getComponente() {
		super.addCssClass("form-control");
		super.addAttr("id", getId());
		super.addAttr("type", "text");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());



		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		if(isIcon() == false) {
			sb.append( super.getIconBefore(6, null ) );
		} else {
			sb.append( super.getIconBefore(6, CssIconEnum.CALCULATOR ) );
		}
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );

		if (mask.equals(MASK_MONEY)) {
			sb.append("$('#"+ this.getId() +"').maskMoney({thousands:'.', decimal:',', allowZero:true, precision: "+ decimal +"});");
		} else {
			sb.append("$('#"+ this.getId() +"').autoNumeric({aSep: '.', aDec: ',', mDec: "+ decimal +"});");
		}

		return sb.toString();
	}
	@Override
	public void setValue(Object value) {
		this.value = (BigDecimal) value;	
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (value == null) return "";

		String valueStr = FormatUtils.formatBigDecimal(value);
		return valueStr;
	}

	public void setDecimal(Integer decimal) {
		this.decimal = decimal;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}


	public void setMask(Integer mask) {
		this.mask = mask;
	}
}

