package br.com.dotum.core.html.form.field;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.StringUtils;


/**
 * Fabrica de campos html para formulario do core<br />
 * <br />
 * <b>TextField</b>: Campo de texto<br />
 * <b>TextAreaField</b>: Campo de texto grande<br />
 * <b>NumberField</b>: Campo numero inteiro<br />
 * <b>DecimalField</b>: Campo de numero fracionado<br />
 * <b>ComboNumberField</b>: Campo com uma serie de opcoes pre definida<br />
 * Tal campo pode pegar uma tabela do banco de dados ou uma lista
 * <br />
 * Ex:<br />
 * TYP:ComboNumberField#LV:Area<br />
 * TYP:ComboTextField#VLS:1;2;3#LBS:Decrescente;Última interação no fim da lista;Última interação no inicio da lista<br />
 * 
 * 
 * @author keynes
 *
 */

public class FieldFactory {
	private static Log LOG = LogFactory.getLog(FieldFactory.class);


	public static final String SEPARATOR_ELEMENT = "#";
	public static final String SEPARATOR_KEYVALUE = ":";
	public static final String SEPARATOR_VALUES = ";";
	public static final String TYPE = "TYP";
	public static final String LIST_VIEW = "LV";
	public static final String LIST_VALUES = "VLS";
	public static final String LIST_DISPLAY = "LBS";
	public static final String FILTER_FIELD = "FF";
	public static final String FILTER_DB = "FDB";
	public static final String FILTER_BY = "FB";


	public static AbstractFieldHtml createField(UserContext userContext, String name, String label, Object value, boolean required, String chave) throws ClassNotFoundException, DeveloperException, Exception {
		AbstractFieldHtml field = null;

		String typeStr = getValue(name, label, TYPE, chave);
		Class typeClass = getFieldType(name, label, chave);

		if (typeClass.equals(TextFieldHtml.class)) {
			field = new TextFieldHtml(name, label, (String)value, required);
		} else if (typeClass.equals(TextAreaFieldHtml.class)) {
				field = new TextAreaFieldHtml(name, label, (String)value, required);
		} else if (typeClass.equals(DateFieldHtml.class)) {
			field = new DateFieldHtml(name, label, null, required);
			if (value instanceof Date) {
				field.setValue((Date)value);
			} else if (value instanceof String) {
				field.setValue(FormatUtils.parseDate((String)value));
			}
		} else if (typeClass.equals(CheckboxFieldHtml.class)) {
			field = new CheckboxFieldHtml(name, label, value, required);
		} else if (typeClass.equals(NumberFieldHtml.class)) {

			Integer temp = null;
			if (value instanceof Integer) {
				temp = (Integer)value;
			} else if (value instanceof Double) {
				temp = ((Double)value).intValue();
			}
			field = new NumberFieldHtml(name, label, temp, required);

		} else if (typeClass.equals(DecimalFieldHtml.class)) {
			BigDecimal temp = null;
			if (value instanceof Integer) {
				temp = new BigDecimal(""+value);
			} else if (value instanceof Double) {
				temp = new BigDecimal(""+value);
			}
			field = new DecimalFieldHtml(name, label, temp, required);

		} else if (typeClass.equals(ComboTextFieldHtml.class)) {

			String valuesStr = getValue(name, label, LIST_VALUES, chave);
			String labelsStr = getValue(name, label, LIST_DISPLAY, chave);

			String[] values = valuesStr.split(SEPARATOR_VALUES);
			String[] labels = labelsStr.split(SEPARATOR_VALUES);

			ComboTextFieldHtml f = new ComboTextFieldHtml(name, label, (String)value, required);
			f.setValues(values);
			f.setLabels(labels);
			field = f;
		} else if (typeClass.equals(ComboNumberFieldHtml.class)) {

			try {
				String valuesStr = getValue(name, label, LIST_VALUES, chave);
				String labelsStr = getValue(name, label, LIST_DISPLAY, chave);

				List<Integer> values = new ArrayList<Integer>();
				String[] parts = valuesStr.split(SEPARATOR_VALUES);
				for (String part : parts) {
					values.add(Integer.parseInt(part));
				}

				List<String> labels = new ArrayList<String>();
				parts = labelsStr.split(SEPARATOR_VALUES);
				for (String part : parts) {
					labels.add(part);
				}

				ComboNumberFieldHtml f = new ComboNumberFieldHtml(name, label, value, required);
				f.setValues(values.toArray(new Integer[values.size()]));
				f.setLabels(labels.toArray(new String[labels.size()]));
				field = f;
			} catch (DeveloperException e) {
				String listViewStr = getValue(name, label, LIST_VIEW, chave);
				Class<? extends Bean> listView = (Class<? extends Bean>)Class.forName(TransactionDB.PACKAGE_BEAN +"."+listViewStr+"Bean");

				ComboNumberFieldHtml f = new ComboNumberFieldHtml(name, label, value, required);
				f.setClassBean(listView);

				Object[] parts = prepareFilter(userContext, name, label, chave, FILTER_DB);
				f.addFilter((String)parts[0], (Condition)parts[1], (Object)parts[2]);

				field = f;
			}
		} else if (typeClass.equals(AutoCompleteFieldHtml.class)) {
			try {
				String listViewStr = getValue(name, label, LIST_VIEW, chave);
				Class<? extends Bean> listView = (Class<? extends Bean>)Class.forName(TransactionDB.PACKAGE_BEAN + "."+listViewStr+"Bean");
				Object[] parts = prepareFilter(userContext, name, label, chave, FILTER_DB);

				String valuesStr = getValue(name, label, LIST_VALUES, chave);
				String[] values = valuesStr.split(SEPARATOR_VALUES);

//				String labelsStr = getValue(name, label, LIST_DISPLAY, chave);
//				String[] labels = labelsStr.split(SEPARATOR_VALUES);
				
				String filter = getValue(name, label, FILTER_BY, chave);
				String[] filterBy = filter.split(SEPARATOR_VALUES);
				
				
				AutoCompleteFieldHtml f = new AutoCompleteFieldHtml(name, label, value, required);
				f.setClassBean(listView);
				f.addFilter((String)parts[0], (Condition)parts[1], (Object)parts[2]);
				if(filterBy != null)f.setFilterBy(filterBy);
				if(values != null)f.setProperty("id", values);
				

				field = f;
			} catch (Exception e) {
				TextFieldHtml f = new TextFieldHtml(name, label, "Campo criado com problema", required);
				f.setDisabled(true);
				field = f;
				LOG.error(e.getMessage());
			}
		} else {
			throw new DeveloperException("Erro ao criar o campo "+ name +" com o label "+label+" motivo tipo desconhecido: " + typeClass + ". Chave: " + chave);
		}

		return field;
	}

	private static Object[] prepareFilter(UserContext userContext, String name, String label, String chave, String filter) throws DeveloperException {
		String filterDb = getValue(name, label, filter, chave);
		if (StringUtils.hasValue(filterDb)) {
			Object filterValue = null;
			
			String[] parts = filterDb.split(SEPARATOR_VALUES);
			String attributeStr = parts[0];
			String conditionStr = parts[1];
			String filterValueStr = parts[2];
			if (filterValueStr.equalsIgnoreCase("$V{unidadesId}")) {
				filterValue = userContext.getUnidadeArray();
			} else if (filterValueStr.equalsIgnoreCase("$V{unidadeId}")) {
				filterValue = userContext.getUnidade().getId();
			} else if (filterValueStr.equalsIgnoreCase("$V{usuarioId}")) {
				filterValue = userContext.getUser().getId();
			} else {
				filterValue = parts[2];
			}
			
			return new Object[] {attributeStr, Condition.getConditionByStr(conditionStr), filterValue};
		}
		return null;
	}

	public static Object parseValueStr(AbstractFieldHtml field, String value) throws ParseException, DeveloperException {
		return parseValue(field, value);
	}
	public static Object parseValue(AbstractFieldHtml field, Object value) throws ParseException, DeveloperException {
		if (value == null) return null;

		Object obj = null;
		if (field instanceof ComboNumberFieldHtml) {
			obj = FormatUtils.parseInt(""+value);
		} else if ((field instanceof MultSelectNumberFieldHtml) && (value instanceof String)) {
			obj = FormatUtils.parseInt(""+value);
		} else if (field instanceof MultSelectNumberFieldHtml) {
			obj = ArrayUtils.parseToInt((String[])value);
		} else if (field instanceof ComboTextFieldHtml) {
			obj = ""+value;
		} else if (field instanceof DateFieldHtml) {
			obj = FormatUtils.parseDate(""+value);
		} else if (field instanceof DateRangeFieldHtml) {
			Date d1 = FormatUtils.parseDate(((String[])value)[0]);
			Date d2 = FormatUtils.parseDate(((String[])value)[1]);
			obj = new Date[] {d1, d2};
		} else if (field instanceof TextFieldHtml) {
			obj = ""+value;
		} else if (field instanceof CheckboxFieldHtml) {
			obj = ""+value;
		} else if (field instanceof TextAreaFieldHtml) {
			obj = ""+value;
		} else if (field instanceof NumberFieldHtml) {
			obj = FormatUtils.parseInt(""+value);
		} else if (field instanceof DecimalFieldHtml) {
			obj = NumberUtils.parseBigDecimal(""+value);
		} else if (field instanceof AutoCompleteFieldHtml) {
			obj = FormatUtils.parseInt(""+value);
		} else {
			throw new DeveloperException("Impossivel converter o tipo de dado para o campo \""+ field.getName()  + "\" do tipo \"" + field.getClass().getSimpleName() +"\".");
		}

		return obj;
	}

	private static Class<? extends AbstractFieldHtml> getFieldType(String name, String label, String chave) throws ClassNotFoundException, DeveloperException {
		String typeStr = getValue(name, label, TYPE, chave);
		Class typeClass = Class.forName(FieldFactory.class.getPackage().getName() +"."+ typeStr + "Html");
		return typeClass;
	}

	private static String getValue(String name, String label, String key, String chave) throws DeveloperException {
		if (chave == null)
			throw new DeveloperException("Chave nula. Propriedade: \"" + key + "\". Name: \"" + name + "\". Label: \"" + label + "\".");

		String[] parts = chave.split(SEPARATOR_ELEMENT);
		for (String part : parts) {
			if (part.toUpperCase().startsWith(key.toUpperCase())) {
				return part.substring(key.length()+1);
			}
		}

		throw new DeveloperException("Chave ("+ chave +") sem a propriedade: \"" + key + "\". Name: \"" + name + "\". Label: \"" + label + "\".");
	}

	private static String getPartStringChave(String chave, String namePart) throws DeveloperException{
		if (chave == null) return null;
		String[] parts = chave.split("#");
		for (String part : parts) {
			if (part.startsWith(namePart + ":") == false) continue;
			return part.substring(namePart.length() + 1);
		}
		return null;
	}
}

