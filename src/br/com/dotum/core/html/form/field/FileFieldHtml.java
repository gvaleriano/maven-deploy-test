package br.com.dotum.core.html.form.field;

import java.util.Map;

import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.StringUtils;

/**
 *  https://rpo.wrotapodlasia.pl/docs/sections/custom/file-input.html   
 * @author dotum01
 *
 */
public class FileFieldHtml extends AbstractFieldHtml {

	private String value;
	private boolean multiple;
	private String[] allowExt;
	private int maxSizeKb = -1;
	
	public FileFieldHtml(String name, String label, String value, boolean required) {
		super.setTypeValue(String.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);
		
		this.value = value;
	}

	@Override
	public String getComponente() throws Exception {
		super.addCssClass("form-control");
		super.setPlaceholder("...");
		super.addAttr("id", ""+getId());
		super.addAttr("type", "file");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());
		// especifico
		if (this.multiple == true) 
			super.addAttr("multiple", "multiple");

		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null ) );
		sb.append("<input ");
		if ((allowExt != null) && (allowExt.length > 0)) {
			sb.append("accept =\"");
			String temp = "";
			for(String allow : this.allowExt) {
				sb.append(temp+allow);
				temp = ",";
			}
			sb.append("\"");
		}
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}
	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}
	
	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();

		sb.append("$('#"+getId()+"').ace_file_input({");
		sb.append("no_file:'Sem arquivo ...',");
		
		if ((allowExt != null) && (allowExt.length > 0)) {
			sb.append("allowExt:  [");

			String temp = "";
			for (String ext : allowExt) {
				sb.append( temp );
				sb.append("'"+ ext +"'");
				temp = ",";
			}
			sb.append("],");
		}
		if (maxSizeKb > -1) {
			sb.append("maxSize: "+ maxSizeKb +",");
		}
		
		sb.append("btn_choose:'Procurar',");
		sb.append("btn_change:'Trocar',");
		sb.append("droppable:false,");
		sb.append("thumbnail:false");
		sb.append("});");
		sb.append( super.getScript() );
		return sb.toString();
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}
	
	public String getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (StringUtils.hasValue(value) == false) return "";
		return ""+value;
	}

	public boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	@Override
	public Map<String, String> getValidate() {
		return null;
	}

	public String[] getAllowExt() {
		return allowExt;
	}

	public void setAllowExt(String ... allowExt) {
		this.allowExt = allowExt;
	}

	public int getMaxSizeKb() {
		return maxSizeKb;
	}

	public void setMaxSizeKb(int maxSizeKb) {
		this.maxSizeKb = maxSizeKb * 1024;
	}

}

