package br.com.dotum.core.html.form.field;

import java.util.Map;

import br.com.dotum.jedi.util.StringUtils;

public class HiddenFieldHtml extends AbstractFieldHtml {

	private String value;
	
	public HiddenFieldHtml(String name, String value) {
		super.setTypeValue(String.class);
		
		super.setName(name);
		this.value = value;
	}
	
	@Override
	public String getComponente() {
//		super.addCssClass("form-control");
//		super.setPlaceholder("...");
		super.addAttr("id", getId());
		super.addAttr("type", "hidden");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		
		
		StringBuilder sb = new StringBuilder();
//		sb.append( super.getLabelBefore() );
//		sb.append( super.getIconBefore(12, null ) );
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append("/>");
//		sb.append( super.getIconAfter() );
//		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}
	
	@Override
	public String getScript() {
		//não mexer nisso aki pq é usado no sameValue nos formulario
		//correndo RISCO DE MORTE remover isso aki ;/ PRI 25/10/2018
		return super.getScript(); 
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}
	
	public String getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (StringUtils.hasValue(value) == false) return "";
		return ""+value;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}
}

