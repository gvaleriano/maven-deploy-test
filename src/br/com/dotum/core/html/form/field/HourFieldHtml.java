package br.com.dotum.core.html.form.field;

import java.util.Map;

import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.jedi.util.StringUtils;

public class HourFieldHtml extends AbstractFieldHtml {

	private String value;
	private CssIconEnum icon;
	private CssIconEnum iconBefore;

	public HourFieldHtml(String name, String label, String value, boolean required) {
		super.setTypeValue(String.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);
		super.setMask(Mask.HORA);

		this.value = value;
	}

	@Override
	public String getComponente() {
		//		if(getType() == TableFieldEditType.TELEFONEFIELD)
		//			addCssClass("telefonefield");

		super.addCssClass("form-control block");

		super.addAttr("id", getId());
		super.addAttr("type", "text");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		if(isIcon() == false) {
			sb.append( super.getIconBefore(6, null ) );
		} else {
			sb.append( super.getIconBefore(6, CssIconEnum.CLOCK) );
		}
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );

		if (StringUtils.hasValue(getMask())) {
			sb.append(getMask());
		}

		//		if(getType() == TableFieldEditType.TELEFONEFIELD){
		//			sb.append("$('input[id="+this.getId()+"]').focusout(function(){");
		//			sb.append("var phone, element;");
		//			sb.append("element = $(this);");
		//			sb.append("element.unmask();");
		//			sb.append("phone = element.val().replace(/\\D/g, '');");
		//			sb.append("if(phone.length > 10) {");
		//			sb.append("element.mask('(99) 99999-999?9');");
		//			sb.append("} else {");
		//			sb.append("element.mask('(99) 9999-9999?9');");
		//			sb.append("}");
		//			sb.append("}).trigger('focusout');");
		//		}

		return sb.toString();
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}
	public String getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (StringUtils.hasValue(value) == false) return "";
		return ""+value;
	}

	public void  setIcon(CssIconEnum icon) {
		this.icon = icon;
	}

	public CssIconEnum getIcon() {
		return this.icon;
	}
	public void  setIconBefore(CssIconEnum iconBefore) {
		this.iconBefore = iconBefore;
	}

	public CssIconEnum getIconBefore() {
		return this.iconBefore;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}
}

