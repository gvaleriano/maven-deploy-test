package br.com.dotum.core.html.form.field;

import java.util.Map;

import br.com.dotum.jedi.util.StringUtils;

public class HtmlEditorFieldHtml extends AbstractFieldHtml {

	public static final String JAVA = "java";
	public static final String SQL = "sql";
	public static final String XML = "xml";
	public static final String HTML = "html";
	private String typeEditor = XML;
	private String value;
	private AbstractFieldHtml field;

	public HtmlEditorFieldHtml(String name, String label, String value, boolean required) {
		super.setTypeValue(String.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);

		this.value = value;
	}

	@Override
	public String getComponente() {
		StringBuilder sb = new StringBuilder();
//		sb.append( super.getLabelBefore() );
//		sb.append( super.getIconBefore(12, null ) );

		sb.append("<div");
		sb.append(" id=\"div"+ getId()+ "\"");
		sb.append(" style=\"height: 410px;\"");
		sb.append(">");
		sb.append("</div>");
		sb.append("<textarea style=\"display:none;\"");
		sb.append(" id=\""+ getId() + "\""); 
		sb.append(" name=\"" + getName() + "\"");
		sb.append(">");
		sb.append( getValueToHtml() );
		sb.append(" </textarea>");

		return sb.toString();
	}

	@Override
	public void createScript() {
		// TODO erro aqui de link

		getHtml().addScriptLink("./fw/ace/ace.js", null);
		getHtml().addScriptLink("./fw/ace/ace-ext-language_tools.js", null);
		getHtml().addScriptLink("./fw/ace/ace-snippets-xml.js", null);
		getHtml().addScriptLink("./fw/ace/ace-theme-eclipse.js", null);
		getHtml().addScriptLink("./fw/ace/ace-mode-" +getTypeEditor() + ".js", null);
		getHtml().addScriptLink("./components/markdown/lib/markdown.min.js", null);
		
		
		String divId = "div" + getId() ;
		String textareaId = getId();

		StringBuilder sb = new StringBuilder();
		sb.append("var langTools = ace.require(\"ace/ext/language_tools\");");
		sb.append("var textarea"+ getId() +" = $('#"+ textareaId +"');");
		sb.append("var editor"+ getId() +" = ace.edit(\""+ divId +"\");");
		sb.append("editor"+ getId() +".setTheme(\"ace/theme/eclipse\");");



		if (field != null) {
			sb.append("editor"+ getId() +".getSession().selection.on('changeSelection', function(e) {");
			sb.append("  $('#"+field.getId()+"').val( editor"+ getId() +".getSession().getTextRange(editor"+ getId() +".getSelectionRange()) );");
			sb.append("});");	
		}

		sb.append("editor"+ getId() +".getSession().setMode(\"ace/mode/"+ getTypeEditor() +"\");");
		
		
		sb.append("textarea"+ getId() +".on('change', function () {editor"+ getId() +".getSession().setValue(textarea"+ getId() +".val());});");
		
		sb.append("editor"+ getId() +".getSession().setValue(textarea"+ getId() +".val());");
		sb.append("editor"+ getId() +".getSession().on('change', function () {textarea"+ getId() +".val(editor"+ getId() +".getSession().getValue());});");
//		sb.append("editor"+ getId() +".setOptions({enableBasicAutocompletion: true,enableSnippets: true});");

//		sb.append("var rhymeCompleter = {");
//		sb.append("getCompletions: function(editor, session, pos, prefix, callback) {");
//		sb.append("if (prefix.length === 0) { callback(null, []); return }");
//		sb.append("$.getJSON(");
//		sb.append("\"http://rhymebrain.com/talk?function=getRhymes&word=\" + prefix,");
//		sb.append("function(wordList) {");
//		sb.append("callback(null, wordList.map(function(ea) {");
//		sb.append("return {name: ea.word, value: ea.word, score: ea.score, meta: \"dotum\"}");
//		sb.append("}));");
//		sb.append("})");
//		sb.append("}");
//		sb.append("};");
//		sb.append("langTools.addCompleter(rhymeCompleter);");
		getHtml().addScript(sb.toString());
		
		getHtml().addScript(getScript());
	}
	
	public String getTypeEditor() {
		return typeEditor;
	}

	public void setTypeEditor(String typeEditor) {
		this.typeEditor = typeEditor;
	}

	
	@Override
	public String getScript() {
		return "";
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}

	public String getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (StringUtils.hasValue(value) == false) return "";
		return ""+value;
	}
	
	public void setFieldSelectedContent(AbstractFieldHtml field) {
		this.field = field;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}


}

