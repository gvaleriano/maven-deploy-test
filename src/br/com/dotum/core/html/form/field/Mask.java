package br.com.dotum.core.html.form.field;

public class Mask {

	public static final String NUMERO_ESPACO = "$.mask.definitions['f'] = \"[0-9 ]\";";
	public static final String TELEFONE = "(99) 99999-99?9";
	public static final String CEP = "99.999-999";
	public static final String CPF = "999.999.999-99";
	public static final String DATA = "99/99/9999";
	public static final String MES = "99/9999";
	public static final String HORA = "99:99";
	public static final String EMAIL = "email";
	public static final String DECIMALL = "9.999,99";
	
}

