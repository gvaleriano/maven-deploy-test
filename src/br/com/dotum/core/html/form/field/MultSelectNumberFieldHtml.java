package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.jedi.core.db.AbstractJson;
import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ArrayUtils;

public class MultSelectNumberFieldHtml extends AbstractFieldHtml {
	private static Log LOG = LogFactory.getLog(MultSelectNumberFieldHtml.class);

	private Class<? extends Bean> beanClass;
	private Class<? extends AbstractJson> jsonClass;
	private WhereDB where = new WhereDB();
	private String propertyKey = "Id";
	private String[] property = new String[] {"Descricao"};

	private Integer[] value;
	private List<Integer> values = new ArrayList<Integer>();
	private List<String> labels = new ArrayList<String>();

	public MultSelectNumberFieldHtml(String name, String label, List<Integer> values, List<String> labels, Integer[] value, boolean required) throws Exception {
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);

		this.values = values;
		this.labels = labels;
	}

	public MultSelectNumberFieldHtml(String name, String label, Integer value[], boolean required) {
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);
	}

	@Override
	public String getComponente() throws Exception {
		prepareValues();

		super.addCssClass("form-control multiselect");
		super.addAttr("id", getId());
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("multiple", "multiple");


		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null , null) );
		sb.append("<select ");
		sb.append( super.getAttrStr() );
//		sb.append(">" + ResultHtml.PL);
		sb.append(">");

		if(values.size() > 0){
			for (int i = 0; i < values.size(); i++) {
				String vStr = "";
				Object v = values.get(i);
				String l = labels.get(i);

				String part = "";
				if (v == null) {
					vStr = "";
				} else if (v instanceof Integer) {

					if (value != null && value.length > 0) {
						for (int j = 0; j < value.length; j++) {
							if (v.equals((Integer)value[j]) == true) {
								part = " selected=\"selected\"";
								break;
							}
						}
					}
					vStr = ""+v;
				}
//				sb.append("<option value=\""+ vStr +"\""+ part +">" + l + "</option>" + ResultHtml.PL);
				sb.append("<option value=\""+ vStr +"\""+ part +">" + l + "</option>");
			} 
		} else {
			sb.append( "<option value=\"\">"+ "Cadastre " + super.getLabel()  +"</option>");
//			sb.append( "<option value=\"\">"+ "Cadastre " + super.getLabel()  +"</option>" + ResultHtml.PL);
		}

//		sb.append("</select>" + ResultHtml.PL);
		sb.append("</select>");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	private void prepareValues() throws Exception {
		if ((beanClass == null) && (jsonClass == null)) return; 


		TransactionDB trans = null;
		List<? extends Bean> beanList = null;
		try{
			trans = TransactionDB.getInstance(null);
			if (beanClass != null) {
				OrderDB orders = new OrderDB();
				for (String attribute : property) {
					orders.add(attribute);
				}
				beanList = trans.select(beanClass, where, orders);
			} else if (jsonClass != null) {
				AbstractJson json = (AbstractJson)jsonClass.newInstance();
				json.setTransaction(trans);
				json.setWhere(where);
				beanList = json.run();
			}
		}catch(NotFoundException e){
			beanList = new ArrayList<Bean>();
		}catch(Exception e){
			beanList = new ArrayList<Bean>();
			LOG.error(e.getMessage(), e);
		}finally{
			trans.close();
		}


		if (beanList.size() > 0) {
			for (int i = 0; i < beanList.size(); i++) {
				Bean bean = beanList.get(i);
				Integer value = (Integer)bean.getAttribute(propertyKey);


				String temp = "";
				StringBuilder sb = new StringBuilder();
				for (String str : property) {
					Object obj = bean.getAttribute(str);

					sb.append(temp);
					if (obj instanceof Integer) {
						sb.append( (Integer)obj);
					} else if (obj instanceof String) {
						sb.append( (String)obj);
					}
					temp = " - ";
				}

				labels.add( sb.toString() );
				values.add( value );
			}
		}
	}

	public void setValue(Integer[] value) {
		this.value = value;
	}

	@Override
	public Class<?> getTypeValue() {
		return Integer.class;
	}
	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		//		sb.append("$('.multiselect').multiselect({");
		//		sb.append("enableFiltering: true,");
		//		sb.append("enableHTML: true,");
		//		sb.append("buttonClass: 'btn btn-white btn-primary',");
		//		sb.append("templates: {");
		//		sb.append("button: '<button type=\"button\" class=\"multiselect dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"multiselect-selected-text\"></span> &nbsp;<b class=\"fa fa-caret-down\"></b></button>',");
		//		sb.append("ul: '<ul class=\"multiselect-container dropdown-menu\"></ul>',");
		//		sb.append("filter: '<li class=\"multiselect-item filter\"><div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-search\"></i></span><input class=\"form-control multiselect-search\" type=\"text\"></div></li>',");
		//		sb.append("filterClearBtn: '<span class=\"input-group-btn\"><button class=\"btn btn-default btn-white btn-grey multiselect-clear-filter\" type=\"button\"><i class=\"fa fa-times-circle red2\"></i></button></span>',");
		//		sb.append("li: '<li><a tabindex=\"0\"><label></label></a></li>',");
		//		sb.append("divider: '<li class=\"multiselect-item divider\"></li>',");
		//		sb.append("liGroup: '<li class=\"multiselect-item multiselect-group\"><label></label></li>'");
		//		sb.append("}");
		//		sb.append("});");


		sb.append( super.getScript() );
		return sb.toString();
	}

	@Override
	public void setValue(Object value) {
		try {

			if (value instanceof String[]) {
				this.value = ArrayUtils.parseToInt((String[]) value);	
			} else if (value instanceof Integer[]) {
				this.value = (Integer[]) value;	
			}
		} catch (Exception e) {
			LOG.error("Erro ao setar o valor \""+ value +"\" no campo " + getLabel() + ".", e);
		}
	}

	@Override
	public Object getValue() {
		return value;
	}
	@Override
	public String getValueToHtml() {
		return null;
	}
	public void addOption(Integer value, String label) {
		this.values.add( value );
		this.labels.add( label );
	}
	public void setValues(Integer ... values) {
		for  (Integer value : values) {
			this.values.add( value );
		}
	}
	public void setLabels(String ... labels) {
		for  (String label : labels) {
			this.labels.add( label );
		}
	}
	public void setProperty(String key, String ... display) {
		this.propertyKey = key;
		this.property = display;
	}
	public void setClassBean(Class<? extends Bean> beanClass) {
		this.beanClass = beanClass;
	}
	public void setClassJson(Class<? extends AbstractJson> jsonClass) {
		this.jsonClass = jsonClass;
	}
	public void addFilter(String field, Condition condition, Object value) throws DeveloperException {
		if ((beanClass == null) && (jsonClass == null))  
			throw new DeveloperException("Antes de invocar o metodo \"addFilter\" do MultSelectNumberFieldHtml (Name: " + getName() + ") chame o metodo \"setClassBean\" ou \"setClassJson\".");

		where.add(field, condition, value);
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}


}

