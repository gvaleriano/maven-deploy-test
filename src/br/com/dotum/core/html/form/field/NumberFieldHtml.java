package br.com.dotum.core.html.form.field;

import java.util.Map;

import br.com.dotum.core.enuns.CssIconEnum;

public class NumberFieldHtml extends AbstractFieldHtml {

	private Integer value;
	
	public NumberFieldHtml(String name, String label, Integer value, boolean required) {
		super.setTypeValue(Integer.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);

		super.addCssClass("number col-sm-12");
		this.value = value;
	}

	@Override
	public String getComponente() {
		
		super.addCssClass("spinbox-input");
		super.addAttr("id", getId());
		super.addAttr("type", "text");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		if(isIcon() == false) {
			sb.append( super.getIconBefore(6, null) );
		} else {
			sb.append( super.getIconBefore(6, CssIconEnum.CALCULATOR ) );
		}
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}
	
	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}
	
	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );
		sb.append( "$('#"+ this.getId() +"').autoNumeric({aSep: '.', aDec: ',', mDec: 0});" );
		sb.append( "$('.money').mask('000.000.000.000.000,00', {reverse: true});" );
		return sb.toString();
	}


	@Override
	public void setValue(Object value) {
		this.value = (Integer) value;	
	}
	
	public Integer getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (value == null) return "";
		return ""+value;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}
	
}

