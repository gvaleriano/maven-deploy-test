package br.com.dotum.core.html.form.field;

import java.util.Map;

import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.jedi.util.StringUtils;

public class PasswordFieldHtml extends AbstractFieldHtml {

	private boolean strength = false;
	private String value;

	public PasswordFieldHtml(String name, String label, String value, boolean required) {
		super.setTypeValue(String.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);

		this.value = value;
	}

	@Override
	public String getComponente() {
		super.addCssClass("form-control");
		super.addAttr("id", getId());
		super.addAttr("type", "password");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		if(isIcon() == false) {
			sb.append( super.getIconBefore(8, null ) );
		} else {
			sb.append( super.getIconBefore(8, CssIconEnum.ASTERISK ) );
		}
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );

		//		sb.append("<div class=\"col-sm-6 col-sm-offset-2\" style=\"padding-top: 30px;\">"); 
		//		sb.append("<div class=\"pwstrength_viewport_progress\"></div>");
		//		sb.append("</div>");



		return sb.toString();
	}

	@Override
	public void createScript() {

		if (strength == true) {
			getHtml().addScriptLink("./fw/js/i18next.js", null);
			getHtml().addScriptLink("./fw/js/pwstrength.js", null);
		}


		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder result = new StringBuilder();
		if (strength == true) {

			result.append("   i18next.init({ ");
			result.append("       lng: 'pt', ");
			result.append("       resources: { ");
			result.append("         pt: { ");
			result.append("           translation: { ");
			//			result.append("             wordLength: 'Tu contraseña es demasiado corta', ");
			//			result.append("             wordNotEmail: 'Não use seu email como senha', ");
			//			result.append("             wordSimilarToUsername: 'Tu contraseña no puede contener tu nombre de usuario', ");
			//			result.append("             wordTwoCharacterClasses: 'Mezcla diferentes clases de caracteres', ");
			//			result.append("             wordRepetitions: 'Demasiadas repeticiones', ");
			//			result.append("             wordSequences: 'Tu contraseña contiene secuencias', ");
			//			result.append("             errorList: 'Errores:', ");
			result.append("             veryWeak: 'Muito fraca', ");
			result.append("             weak: 'Fraca', ");
			result.append("             normal: 'Normal', ");
			result.append("             medium: 'Media', ");
			result.append("             strong: 'Forte', ");
			result.append("             veryStrong: 'Muito formte', ");
			result.append("             start: 'Informe sua senha', ");
			//			result.append("             label: 'Contraseña', ");
			//			result.append("             pageTitle: 'Bootstrap 3 Password Strength Meter - Ejemplo de Traducciones', ");
			result.append("             goBack: 'Atrás' ");
			result.append("           } ");
			result.append("         } ");
			result.append("       } ");
			result.append("     }, function () { ");

			result.append("         var options = {};");
			result.append("         options.common = {");
			result.append("                 onLoad: function () {");
			result.append("                     $('#messages').html(i18next.t('start'));");
			result.append("                 }");
			result.append("             };");


			result.append("         $(':password').pwstrength(options);");
			result.append("     }); ");
		}

		return result.toString();
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}

	public String getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (StringUtils.hasValue(value) == false) return "";
		return ""+value;
	}

	public boolean isStrength() {
		return strength;
	}

	public void setStrength(boolean strength) {
		this.strength = strength;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}
}

