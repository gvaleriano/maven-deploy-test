package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.html.other.ResultHtml;

public class RadioNumberFieldHtml extends AbstractFieldHtml {

	private Integer value;
	private List<Integer> values = new ArrayList<Integer>();
	private List<String> labels = new ArrayList<String>();;

	public RadioNumberFieldHtml(String name, String label, Integer value, boolean required) {
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);
	}

	public void addOption(Integer integer, String label) {
		this.values.add( integer );
		this.labels.add( label );
	}

	@Override
	public String getComponente() {
		StringBuilder sb = new StringBuilder();

		super.addCssClass("form-control");
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(8, null));

		sb.append("	<div class=\"radio\">" + ResultHtml.PL);
		for (int i = 0; i < values.size(); i++) {
			String vStr = "";
			Integer v = values.get(i);
			String l = labels.get(i);

			String part = null;
			if (v == null) {
				part = "";
				vStr = "";
			} else {
				//
				part = (v.equals(getValue()) ? "checked=\"checked\" selected=\"selected\"  ": "");
				vStr = ""+v;
			}
			sb.append("		<label class=\"col-sm-12 no-padding\">" + ResultHtml.PL);
			sb.append("		  <input class=\"ace "+getId()+" col-sm-2\" id=\""+getId()+"\" type=\"radio\" name=\""+ getName() +"\" value=\""+ vStr +"\""+ part + super.getAttrStr() +" />" + ResultHtml.PL);
			sb.append("		  <div class=\"lbl col-sm-10\"> " + l + "</div>" + ResultHtml.PL);
			sb.append("		</label>" + ResultHtml.PL);
		}
		sb.append("</div>" + ResultHtml.PL);
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public Class<?> getTypeValue() {
		return String.class;
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder script = new StringBuilder();
		script.append("$('body').on('click', '#"+getId()+"', function(e) {");
		script.append("var valor = $(this).val();");//aqui cata o valor de quem esta sendo clicado e armazena para validação posterios
		script.append(" $('."+getId()+"').each(function() {");//percorre todos que tem a class
		script.append("   if (valor === $(this).val()) {");//verifica se confere com o valor informado, se true, bora marcar os fia da puta como seleciondo e checado
		script.append("     $(this).attr('selected','selected');");
		script.append("     $(this).attr('checked','checked');");
		script.append("     $(this).prop('checked', true);");
		script.append("    } else {");//não é quem eu rpocuro desmarca essa coisa
		script.append("     $(this).removeAttr('selected','selected');");
		script.append("     $(this).removeAttr('checked','checked');");
		script.append("     $(this).prop('checked', false);");
		script.append("   }");
		script.append("  });");
		script.append(" });");

		/**
		 * Ai meu irmão, se tu quiser pegar o valor do radio selecionado, percorre os porra louca e vá ser feliz como o exemplo abaixo
		 * 
		 * by Isac C. Farias

		 $('#btn').click(function(){
		        $('.R8251').each(function() {
		         if($(this).is(':checked')){
		          alert($(this).val());
		        }
		      });
		    });
		    
		    
		 **/
		return script.toString();
	}

	@Override
	public void setValue(Object value) {
		if(value instanceof String) {
			this.value = Integer.parseInt((String) value);	
		} else if (value instanceof Integer) {
			this.value = (Integer) value;
		}
	}

	@Override
	public Object getValue() {
		return this.value;
	}
	@Override
	public String getValueToHtml() {
		return this.value+"";
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}

}

