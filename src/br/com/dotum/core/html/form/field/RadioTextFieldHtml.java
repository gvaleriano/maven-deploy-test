package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.other.ResultHtml;

public class RadioTextFieldHtml extends AbstractFieldHtml {

	private Object value;
	private List<Object> values = new ArrayList<Object>();
	private List<String> labels;

	public RadioTextFieldHtml(String name, String label, List<String> values, List<String> labels, String value, boolean required) {
		setName(name);
		setLabel(label);
		setValue(value);
		setRequired(required);

		this.values.addAll(values);
		this.labels = labels;
	}

	@Override
	public String getComponente() {
		StringBuilder sb = new StringBuilder();

		sb.append( super.getLabelBefore() );
		if(isIcon() == false) {
			sb.append( super.getIconBefore(8, null ) );
		} else {
			sb.append( super.getIconBefore(8, CssIconEnum.ASTERISK ) );
		}

		sb.append("	<div class=\"radio\">" + ResultHtml.PL);
		for (int i = 0; i < values.size(); i++) {
			String vStr = "";
			Object v = values.get(i);
			String l = labels.get(i);

			String part = null;
			if (v == null) {
				part = "";
				vStr = "";
			} else {
				part = (v.equals((String)value) ? " selected=\"selected\"" : "");
				vStr = ""+v;
			}
			sb.append("		<label>" + ResultHtml.PL);
			sb.append("		  <input class=\"ace\" id=\""+getId()+"\" type=\"radio\" name=\""+ getName() +"\" value=\""+ vStr +"\""+ part +" />" + ResultHtml.PL);
			sb.append("		  <span class=\"lbl col-sm-2\"> " + l + "</span>" + ResultHtml.PL);
			sb.append("		</label>" + ResultHtml.PL);
		}
		sb.append("					</div>" + ResultHtml.PL);
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public Class<?> getTypeValue() {
		return String.class;
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		return "";
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}

	@Override
	public Object getValue() {
		return null;
	}
	@Override
	public String getValueToHtml() {
		return null;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}
}

