package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.core.html.form.GroupFieldHtml;
import br.com.dotum.jedi.util.SQLUtils;

public class ShowWhen {

	private List<ShowWhen> list;
	private Integer group;
	private Operation operation;
	private AbstractFieldHtml field;
	private ConditionJs condition;
	private Object[] value;


	public ShowWhen() {
		list = new ArrayList<ShowWhen>();
	}

	private ShowWhen(Integer group, Operation operation, AbstractFieldHtml field, ConditionJs condition, Object ... value) {
		this.group = group;
		this.operation = (operation == null ? Operation.AND : operation);
		this.field = field;
		this.condition = condition;
		this.value = value;
	}

//	public void add()
	
	public void add(Integer group, Operation operation, AbstractFieldHtml field, ConditionJs condition, Object ... value) {
		list.add(new ShowWhen(group, operation, field, condition, value));
	}

	public void add(Operation operation, AbstractFieldHtml field, ConditionJs condition, Object ... value) {
		list.add(new ShowWhen(null, operation, field, condition, value));
	}

	public void add(Integer group, AbstractFieldHtml field, ConditionJs condition, Object ... value) {
		list.add(new ShowWhen(group, null, field, condition, value));
	}

	public void add(AbstractFieldHtml field, ConditionJs condition, Object ... value) {
		list.add(new ShowWhen(null, null, field, condition, value));
	}

	public List<ShowWhen> getList() {
		return list;
	}

	public Operation getOperation() {
		return operation;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	public AbstractFieldHtml getField() {
		return field;
	}
	public void setField(AbstractFieldHtml field) {
		this.field = field;
	}
	public ConditionJs getCondition() {
		return condition;
	}
	public void setCondition(ConditionJs condition) {
		this.condition = condition;
	}
	public Object[] getValue() {
		return value;
	}
	public void setValue(Object ... value) {
		this.value = value;
	}
	public int size() {
		return list.size();
	}

	public String createScript(AbstractFieldHtml f) {
		StringBuilder sb = new StringBuilder();
		List<ShowWhen> showWhenList = getList();


		if (showWhenList.size() == 0) return "";

//		script.append("	$('#"+form.getId()+"').on('change', 'input[name="+change.getName()+"]', function() {	");
		sb.append("$('#"+f.getParent().getId()+"').on('ready change', '");

		String temp = "";
		for (ShowWhen sw : showWhenList) {
			AbstractFieldHtml field = sw.getField();
			String fieldName = field.getName().replaceAll("\\.", "\\\\.");

			sb.append(temp);

			if ((field instanceof RadioNumberFieldHtml) || (field instanceof RadioTextFieldHtml)) {
				sb.append("input[name="+ fieldName +"]");
			} else {
				sb.append("#" + field.getIdFull());
			}

			temp = ", ";
		}
		sb.append("', function(){");
		sb.append("var _f=$('#"+f.getIdFull()+"');");
		sb.append("var _fg=_f.parents('div.form-group:first');");

		// se tiver campo com radio.. preciso ver antes.. qual esta selecionado e mandar em uma variavel!!!
		// para dai entrar no IF
		for (ShowWhen sw : showWhenList) {
			AbstractFieldHtml field = sw.field;
			String fieldName = field.getName().replaceAll("\\.", "\\\\.");

			if ((field instanceof RadioNumberFieldHtml) || (field instanceof RadioTextFieldHtml)) {
				sb.append("var _" + fieldName + " = null;");
				
				sb.append("$('#"+f.getParent().getId()+" input[name="+ fieldName +"]').each(function(){");
				sb.append("  if($(this).prop('checked') === true) {" );

				sb.append("  _" + field.getName() + " = $(this).val();" );
				sb.append("  }");
				sb.append("});");
			}
		}

		sb.append("if(");

		int i = 0;
		for (ShowWhen sw : showWhenList) {

			AbstractFieldHtml field = sw.field;

			ConditionJs conditionJs = sw.condition;
			Object[] values = sw.value;
			Operation operation = sw.operation;

			if (i > 0) 
				sb.append(operation.toString());

			i++;
			if (field instanceof CheckboxFieldHtml) {
				if (conditionJs.equals(ConditionJs.CHECKED)) {
					sb.append("($('#"+ field.getIdFull() +"')." +ConditionJs.CHECKED.toString()+ "(':checked'))");
				} else {
					sb.append("(!$('#"+ field.getIdFull() +"')." +ConditionJs.CHECKED.toString()+ "(':checked'))");
				}
			} else if ((field instanceof RadioNumberFieldHtml) || (field instanceof RadioTextFieldHtml)) {
				temp = "";

				for (Object obj : values) {
					sb.append(temp);

					if (obj instanceof String) {
						sb.append("(_" + field.getName() + conditionJs.toString() +" '"+ obj +"')");
					} else {
						sb.append("(_" + field.getName() + conditionJs.toString() + obj +")");
					}
					temp = "||";
				}

			} else if(field instanceof AutoCompleteFieldHtml) {
				temp = "";

				for (Object obj : values) {
					sb.append(temp);

					if (obj instanceof String) {
						sb.append("($('#"+ field.getId() +"').val()"+ conditionJs.toString() +" '"+ obj +"')");
					} else {
						sb.append("($('#"+ field.getId() +"').val()"+ conditionJs.toString() + obj +")");
					}
					temp = "||";
				}
				
			} else {
				temp = "";

				for (Object obj : values) {
					sb.append(temp);

					if (obj instanceof String) {
						sb.append("($('#"+ field.getIdFull() +"').val()"+ conditionJs.toString() +" '"+ obj +"')");
					} else {
						sb.append("($('#"+ field.getIdFull() +"').val()"+ conditionJs.toString() + obj +")");
					}
					temp = "||";
				}
			}
		}


		sb.append("){");
		if (f.isRequired()) {
			sb.append("_f.removeAttr('disabled');");
			sb.append("_f.attr('required','required');");
		}
		sb.append("_fg.show();");

		// ocultar o campo e tirar o requerido
		sb.append("} else {");
		if (f.isRequired()) {
			sb.append("_f.attr('disabled','disabled').val('');");
			sb.append("_f.removeAttr('required','required');");
		}
		sb.append("_fg.hide();");
		sb.append("}");
		sb.append("}).change();");

		return sb.toString();
	}

	public String createScript(GroupFieldHtml g) {
		StringBuilder sb = new StringBuilder();
		List<ShowWhen> showWhenList = getList();
		
		if (showWhenList.size() == 0) return "";
		
		sb.append("$('#"+g.getParent().getId()+"').on('ready change', '");
		String temp = "";
		for (ShowWhen sw : showWhenList) {
			AbstractFieldHtml field = sw.getField();
			String fieldName = field.getName().replaceAll("\\.", "\\\\.");
			
			sb.append(temp);
			
			if ((field instanceof RadioNumberFieldHtml) || (field instanceof RadioTextFieldHtml)) {
				sb.append("input[name="+ fieldName +"]");
			} else {
				sb.append("#" + field.getIdFull());
			}
			
			temp = ", ";
		}
		sb.append("', function(){");
		
//		sb.append("console.log($('#"+g.getParent().getId()+"'));");
		sb.append("var _g = $('#" + g.getParent().getId() +" #"+g.getId()+"');");
		// se tiver campo com radio.. preciso ver antes.. qual esta selecionado e mandar em uma variavel!!!
		// para dai entrar no IF
		for (ShowWhen sw : showWhenList) {
			AbstractFieldHtml field = sw.field;
			String fieldName = field.getName().replaceAll("\\.", "\\\\.");
			
			if ((field instanceof RadioNumberFieldHtml) || (field instanceof RadioTextFieldHtml)) {
				sb.append("var _" + field.getName() + " = null;");
				
				sb.append("$('#"+g.getParent().getId()+" input[name="+ fieldName +"]').each(function(){");
				sb.append("  if($(this).prop('checked') === true) {" );
				
				sb.append("  _" + field.getName() + " = $(this).val();" );
				sb.append("  }");
				sb.append("});");
			}
		}
		
		
		sb.append("if(");
		
		int i = 0;
		for (ShowWhen sw : showWhenList) {
			
			AbstractFieldHtml field = sw.field;
//			String fieldName = field.getName().replaceAll("\\.", "\\\\.");
			
			ConditionJs conditionJs = sw.condition;
			Object[] values = sw.value;
			Operation operation = sw.operation;
			
			if (i > 0) 
				sb.append(operation.toString());
			
			i++;
			if (field instanceof CheckboxFieldHtml) {
				if (conditionJs.equals(ConditionJs.CHECKED)) {
					sb.append("($('#"+ field.getIdFull() +"')." +ConditionJs.CHECKED.toString()+ "(':checked'))");
				} else {
					sb.append("(!$('#"+ field.getIdFull() +"')." +ConditionJs.CHECKED.toString()+ "(':checked'))");
				}
			} else if ((field instanceof RadioNumberFieldHtml) || (field instanceof RadioTextFieldHtml)) {
				temp = "";
				
				for (Object obj : values) {
					sb.append(temp);
					
					if (obj instanceof String) {
						sb.append("(_" + field.getName() + conditionJs.toString() +" '"+ obj +"')");
					} else {
						sb.append("(_" + field.getName() + conditionJs.toString() + obj +")");
					}
					temp = "||";
				}
				
			} else if(field instanceof AutoCompleteFieldHtml) {
				temp = "";
				
				for (Object obj : values) {
					sb.append(temp);
					
					if (obj instanceof String) {
						sb.append("($('#"+ field.getId() +"').val()"+ conditionJs.toString() +" '"+ obj +"')");
					} else {
						sb.append("($('#"+ field.getId() +"').val()"+ conditionJs.toString() + obj +")");
					}
					temp = "||";
				}
				
			} else {
				temp = "";
				
				for (Object obj : values) {
					sb.append(temp);
					
					if (obj instanceof String) {
						sb.append("($('#"+ field.getIdFull() +"').val()"+ conditionJs.toString() +" '"+ obj +"')");
					} else {
						sb.append("($('#"+ field.getIdFull() +"').val()"+ conditionJs.toString() + obj +")");
					}
					temp = "||";
				}
			}
		}
		
		
		sb.append("){");
		
		for(AbstractFieldHtml f : g.getFieldList()) {
			
			if (f.isRequired()) {
				sb.append("var _f"+f.getIdFull()+"=$('#"+g.getParent().getId()+" #"+f.getIdFull()+"');");
				sb.append("_f"+f.getIdFull()+".removeAttr('disabled');");
				sb.append("_f"+f.getIdFull()+".attr('required','required');");
			}
		}
		sb.append("_g.css('display', 'block');");
		// ocultar o campo e tirar o requerido
		sb.append("} else {");
//		sb.append("console.log(_g);");
		for(AbstractFieldHtml f : g.getFieldList()) {
			if (f.isRequired()) {
				sb.append("var _f"+f.getIdFull()+"=$('#"+g.getParent().getId()+" #"+f.getIdFull()+"');");
				sb.append("_f"+f.getIdFull()+".attr('disabled','disabled').val('');");
				sb.append("_f"+f.getIdFull()+".removeAttr('required');");
//				sb.append("console.log(_f"+f.getIdFull()+");");
			}
		}
//		sb.append("console.log(_g);");
		sb.append("_g.css('display', 'none');");
		sb.append("}");
		sb.append("}).change();");
		
		return sb.toString();
	}
	

	public String toString() {
		StringBuilder sb = new StringBuilder();

		try {
			for (ShowWhen w : list) {
				String result = "";
				if (w.getValue() != null) {
					if (w.getValue() instanceof Integer[]) {
						result = SQLUtils.intToIn((Integer[])w.getValue());
					} else if (w.getValue() instanceof String[]) {
						result = SQLUtils.strToIn((String[])w.getValue());
					} else {
						result = ""+w.getValue();
					}
				}

				sb.append(w.getGroup() +" "+ w.getField() +" "+ w.getCondition() +" "+ result + "\n");
			}
			return sb.toString();
		} catch (Exception e) {
			return "Erro desconhecido ao efetuar o toString do ShowWhen";
		}

	}

	public enum Operation {
		AND("&&"),
		OR("||");

		private String value;
		Operation(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public Integer getGroup() {
		return group;
	}

}
