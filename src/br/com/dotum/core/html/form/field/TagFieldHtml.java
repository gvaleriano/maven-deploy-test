package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.jedi.core.db.AbstractJson;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.StringUtils;

public class TagFieldHtml extends AbstractFieldHtml {

	private Class<? extends Bean> beanClass;
	private Class<? extends AbstractJson> jsonClass;
	private String filterBy = "tag;"+Condition.LIKE;
	private String propertyKey = "id";                     	// Como será montado o valor   
	private String property = "tag";						// valores que irão retornar (esquema de navegação dos Beans)
	
	private List<String> filters = new ArrayList<String>();

	
	
	private String value;

	public TagFieldHtml(String name, String label, String value, boolean required) {
		super.setTypeValue(String.class);

		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);
		
		this.value = value;
		this.property = name;
	}

	@Override
	public String getComponente() throws DeveloperException {

		
		if (beanClass == null && jsonClass == null)
			throw new DeveloperException("Você esqueceu de colocar o BEAN ou JSON no campo TAG "+ getName());

		super.addCssClass("form-control");
		super.addCssClass("tag");
		super.setPlaceholder("...");
		super.addAttr("id", getId());
		super.addAttr("type", "text");
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());
		super.addAttr("value", getValueToHtml());

		
		
		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null ) );
		sb.append("<input");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScriptLink("./components/_mod/bootstrap-tag/bootstrap-tag.js", null);
//		getHtml().addScriptLink("./components/_mod/bootstrap-tagsinput/typeahead.js", null);
//		getHtml().addScriptLink("./components/_mod/bootstrap-tagsinput/bootstrap-tagsinput"+ResultHtml.MIN+".js", null);
//		getHtml().addStyleLink("./components/_mod/bootstrap-tagsinput/bootstrap-tagsinput.css", null);
		
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );

		StringBuilder sb2 = new StringBuilder();
		String temp = "";
		for (String filter : filters) {
			sb2.append(temp);
			sb2.append(filter);
			temp = "#";
		}

		sb.append("$('#"+getId()+"').tag({" + ResultHtml.PL);
		sb.append("  placeholder:$(this).attr('placeholder')," + ResultHtml.PL);
		sb.append("  source: function(query, process) {" + ResultHtml.PL);
		sb.append(space(2)+"$.ajax({");
		sb.append(space(3)+"url: '"+FWConstants.URL_JSON+"',");
		sb.append(space(3)+"dataType: 'json',");
		if (beanClass != null) {
			sb.append(space(3)+"data:{term:query,filterBy:'"+filterBy+"',key:'"+ propertyKey +"',display:'"+ this.property +"',beanClass:'"+beanClass.getName()+"',filter:'"+ sb2.toString() +"',tag:true},");
		} else if (jsonClass != null) {
			sb.append(space(3)+"data:{term:query,filterBy:'"+filterBy+"',key:'"+ propertyKey +"',display:'"+ this.property +"',jsonClass:'"+jsonClass.getName()+"',filter:'"+ sb2.toString() +"',tag:true},");
		}
		sb.append(space(3)+"success: function(data) {");
		sb.append("   var arr = $.map(data.result, function(el) { return el[\"tag\"]; });");
		sb.append("   process(arr);");
		sb.append("  }" + ResultHtml.PL);
		sb.append("  });" + ResultHtml.PL);
		sb.append(" }" + ResultHtml.PL);
		sb.append("});" + ResultHtml.PL);

		return sb.toString();
	}

	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}

	public String getValue() {
		return value;
	}

	public String getValueToHtml() {
		if (StringUtils.hasValue(value) == false) return "";
		return ""+value;
	}
	
	public void setClassJson(Class<? extends AbstractJson> jsonClass) {
		this.jsonClass = jsonClass;
	}

	public void setClassBean(Class<? extends Bean> beanClass) {
		this.beanClass = beanClass;
	}

	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}

}

