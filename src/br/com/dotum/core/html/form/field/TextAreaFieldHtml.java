package br.com.dotum.core.html.form.field;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.jedi.core.db.AbstractJson;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.util.StringUtils;

public class TextAreaFieldHtml extends AbstractFieldHtml {

	
	private Class<? extends AbstractJson> jsonClass;
	private String filterBy = "name";

	private String propertyKey = "id";                     // Como será montado o valor   
	private String[] property = new String[] {"name"};    // valores que irão retornar (esquema de navegação dos Beans)

	private List<String> filters = new ArrayList<String>();

	private String value;
	private boolean enterSubmit;

	public TextAreaFieldHtml(String name, String label, String value, boolean required) {
		super.setTypeValue(String.class);
		super.setName(name);
		super.setLabel(label);
		super.setRequired(required);

		this.value = value;
	}


	@Override
	public String getComponente() {
		super.addCssClass("form-control mention");
		super.addAttr("id", getId());
		super.addAttr("class", getCssClassComponent());
		super.addAttr("name", getName());

		
		StringBuilder sb = new StringBuilder();
		sb.append( super.getLabelBefore() );
		sb.append( super.getIconBefore(12, null ) );
		sb.append("<textarea");
		sb.append( super.getAttrStr() );
		sb.append(">");
		sb.append( getValueToHtml() );
		sb.append("</textarea>");
		sb.append("<input id=\""+ this.getId() +"Json\" type=\"hidden\" name=\""+ getName() +"Json\" value=\"\"/>");
		sb.append( super.getIconAfter() );
		sb.append( super.getLabelAfter() );
		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();
		sb.append( super.getScript() );


		if (enterSubmit == true) {
			sb.append("$('#"+ getId() +"').keyup(function (event) {");
			sb.append(" if (event.keyCode == 13) {");
			sb.append("  var content = this.value;");
			sb.append("  var caret = getCaret(this);");       
			sb.append("  if(event.shiftKey){");
			sb.append("   this.value = content.substring(0, caret - 1) + \"\\n\" + content.substring(caret, content.length);");
			sb.append("   event.stopPropagation();");
			sb.append("  } else {");
			sb.append("   this.value = content.substring(0, caret - 1) + content.substring(caret, content.length);");
			sb.append("   $(this).parents('form:first').submit();");
			sb.append("  }");
			sb.append(" }");
			sb.append("});");
		}



		if (jsonClass != null) {
			StringBuilder sb2 = new StringBuilder();
			String temp = "";
			for (String filter : filters) {
				sb2.append(temp);
				sb2.append(filter);
				temp = "#";
			}

			String display = "";
			String virgula = "";
			for (String displayItem : this.property) {
				display += virgula;
				display += displayItem;
				virgula = ",";
			}



			Map<String, Object> map = new HashMap<String, Object>();
			map.put("term", "\"+query+\"");
			map.put("key", propertyKey);
			map.put("filterBy", filterBy);
			map.put("filter", sb2.toString());
			map.put("display", display);
			map.put("jsonClass", jsonClass.getName());

			String data = Ajax.mapToJson(map);		

			//		sb.append(space(3)+"data:{term:request.term,filterBy:'"+filterBy+"',key:'"+ propertyKey +"',display:'"+ display +"',beanClass:'"+beanClass.getName()+"',filter:'"+ sb2.toString() +"'},");
			//		sb.append(space(3)+"data:{term:request.term,filterBy:'"+filterBy+"',key:'"+ propertyKey +"',display:'"+ display +"',jsonClass:'"+jsonClass.getName()+"',filter:'"+ sb2.toString() +"'},");

			sb.append("$('#"+ getId() +"').mentionsInput({");
			sb.append(" onDataRequest:function (mode, query, callback) {");
			sb.append("  $.getJSON('"+ FWConstants.URL_JSON +"', {"+ data +"}, function(responseData) {");
			sb.append("   callback.call(this, responseData.result);");
			sb.append("  });");
			sb.append(" },");
			sb.append(" elastic: false");
			sb.append("});");

			sb.append("$('#"+ getId() +"').focusout(function() {");
			sb.append("$(this).mentionsInput('getMentions', function(data) {");
			sb.append("$('#"+ this.getId() +"Json').val(JSON.stringify(data));");
			sb.append("});");
			sb.append("});");

		}

		return sb.toString();
	}


	@Override
	public void setValue(Object value) {
		this.value = (String) value;	
	}

	public String getValue() {
		return value;
	}
	public String getValueToHtml() {
		if (StringUtils.hasValue(value) == false) return "";
		return value;
	}


	public void setClassJson(Class<? extends AbstractJson> jsonClass, String ... filterBy) {
		this.jsonClass = jsonClass;

		if (filterBy.length > 0) {
			StringBuilder sb = new StringBuilder();
			String temp = "";
			for (String str : filterBy) {
				sb.append(temp);
				sb.append(str);
				temp = "#";
			}
			this.filterBy = sb.toString();
		}
	}

	public void addFilter(String attribute, Condition condition, Object value) {
		filters.add(attribute + ";" + condition +";" + value);
	}

	public void setEnterSubmit(boolean enterSubmit) {
		this.enterSubmit = enterSubmit;
	}


	@Override
	public Map<String, String> getValidate() {
		// TODO Auto-generated method stub
		return null;
	}

}

