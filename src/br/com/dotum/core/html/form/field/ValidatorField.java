// XXX Html5
package br.com.dotum.core.html.form.field;

import br.com.dotum.jedi.core.table.TableFieldEditType;


public class ValidatorField {

	protected ValidatorFieldTypeEnum validatorFieldType;
	protected AbstractFieldHtml field;
	protected String value;


	public ValidatorField(ValidatorFieldTypeEnum validatorFieldType) {
		this.validatorFieldType = validatorFieldType;
	}

	public ValidatorField(ValidatorFieldTypeEnum validatorFieldType, String value) {
		this.validatorFieldType = validatorFieldType;
		this.value = value;
	}

	public ValidatorField(ValidatorFieldTypeEnum validatorFieldType, AbstractFieldHtml field) {
		this.validatorFieldType = validatorFieldType;
		this.field = field;
	}

	public ValidatorFieldTypeEnum getType() {
		return validatorFieldType;
	}

	public void setType(ValidatorFieldTypeEnum type) {
		this.validatorFieldType = type;
	}

	public AbstractFieldHtml getField() {
		return field;
	}

	public void setField(AbstractFieldHtml field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static String getComponent(AbstractFieldHtml field){

		StringBuilder sb =  new StringBuilder();
		boolean delete = false;

		sb.append("$('#idform').validate({");
		sb.append("                              ");
		if ((field.getValidators().size() > 0) || (field.isRequired() == true)) {
			
			sb.append("'"+field.getPrefix() + field.getName()+"'" + ": {");
			if (field.getType() == TableFieldEditType.CPFFIELD) {
				sb.append("cpf: true, ");
			} else if (field.getType() == TableFieldEditType.CNPJFIELD) {
				sb.append("cnpj: true, ");
			} else if (field.getType() == TableFieldEditType.DATEFIELD) {
				sb.append("dateNL: true, ");
			} else if (field.getType() == TableFieldEditType.COMBOMULTSELECTFIELD ||
					field.getType() == TableFieldEditType.COMBOMULTSELECTTEXTFIELD ||
					field.getType() == TableFieldEditType.COMBOMULTSELECTNUMBERFIELD) {
				sb.append("validMultSelect: true, ");
			}
			//			if ((field.isRequired() == true) && (field.getValidators().size() > 0)) {
			//				sb.append(", ");
			//			}
			String temp2 = "";
			for (ValidatorField validator : field.getValidators()){

				sb.append(temp2);
				switch (validator.getType()) {
				case PASSWORD:    sb.append("passwordStrength: true"); break;
				case EMAIL:       sb.append("email: true"); break;
				case EQUALTO:     sb.append("equalTo: \"#" + validator.getField().getId() +"\""); break;
				}
				temp2 = ", "; 
			}
			sb.append("}, ");
			delete = true;
		}
		sb.append("});");
		if (delete == true) { 
			sb.delete(sb.length()-2, sb.length());
			sb.append( "," );
		}
		return sb.toString();
	}
}
