// XXX Html5
package br.com.dotum.core.html.form.field;

public enum ValidatorFieldTypeEnum {
    
    EQUALTO("equalto"),
    PASSWORD("password"),
	EMAIL("email");
    
    private String descricao;
            
    ValidatorFieldTypeEnum(String descricao) {
        this.descricao = descricao;
    }
    
    public String getDescricao() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
