package br.com.dotum.core.html.other;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;

public class ButtonTable {
	
	private Class action;
	private String method;
	
	private CssIconEnum icon = CssIconEnum.STAR;
	private CssColorEnum color = CssColorEnum.GREEN;
	private String label = "ok";
	private String title = "ok";
	private String script;
	private String scriptEvent;
	private boolean download = false;
	private ComponentHtml[] compWindowArray;
	private ComponentHtml[] compDataArray;
	private Integer selected = Menu.ONE;
	protected Integer TYPEPARSEBUTTONDEFAULT = 1;
	public static Integer TYPEPARSEBUTTON = 2;
	private Integer parseButtonType = TYPEPARSEBUTTONDEFAULT;

	 	
	public ButtonTable(Class action, String doProccess) {
		this.action = action;
		this.method = doProccess;
	}
	public ButtonTable icon(CssIconEnum cssIcon) {
		this.icon = cssIcon;
		return this;
	}
	public ButtonTable color(CssColorEnum cssColor) {
		this.color = cssColor;
		return this;
	}
	public ButtonTable label(String label) {
		this.label = label;
		return this;
	}
	public ButtonTable script(String script) {
		this.script = script;
		return this;
	}
	public ButtonTable title(String title) {
		this.title = title;
		return this;
	}
	public ButtonTable selecteds(Integer selected) {
		this.selected = selected;
		return this;
	}
	public ButtonTable createWindow(ComponentHtml ... compWindowArray) {
		this.compWindowArray = compWindowArray;
		this.compDataArray = compWindowArray;
		return this;
	}
	public ButtonTable dataComponent(ComponentHtml ... compDataArray) {
		this.compDataArray = compDataArray;
		return this;
	}
	public ButtonTable download(Boolean download) {
		this.download = download;
		return this;
	}
	public Class getAction() {
		return action;
	}
	public String getMethod() {
		return method;
	}
	public CssIconEnum getIcon() {
		return icon;
	}
	public CssColorEnum getColor() {
		return color;
	}
	public String getLabel() {
		return label;
	}
	public String getScript() {
		return script;
	}
	public String getScriptEvent() {
		return scriptEvent;
	}
	public String getTitle() {
		return title;
	}
	public boolean getDownload() {
		return download;
	}
	public ComponentHtml[] getCompWindowArray() {
		return compWindowArray;
	}
	public ComponentHtml[] getCompDataArray() {
		return compDataArray;
	}
	public Integer getSelecteds() {
		return selected;
	}
	
	public ButtonTable onClick(String script) {
		on("click", script);
		return this;
	}
	public ButtonTable onClick(Ajax ajax) {
		on("click", ajax.getComponent());
		return this;
	}
	
	public void on(String event, String script) {
		StringBuilder sb = new StringBuilder();
		sb.append(script);
		this.scriptEvent = sb.toString();
	}
	
	public Integer parseButtonType() {
		return parseButtonType;
	}
	public ButtonTable parseButtonType(Integer parseButtonType) {
		this.parseButtonType = parseButtonType;
		return this;
	}
	
	
}
