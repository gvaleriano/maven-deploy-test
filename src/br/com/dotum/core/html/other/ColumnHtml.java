package br.com.dotum.core.html.other;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public class ColumnHtml extends ContainerComponentHtml {
	private Integer[] size;

	public ColumnHtml() {

	}
	public ColumnHtml(Integer ... size) {
		this.size = size;
	}

	public void addContent(Integer column, ComponentHtml comp) {
		super.add(column, comp);
	}

	public String getComponente() throws Exception {
		StringBuilder sb = new StringBuilder();

		if (size == null) {
			List<Integer> xxx = new ArrayList<Integer>();
			for (Object x : getMap().keySet()) {
				if (xxx.contains(x)) continue;
				xxx.add((Integer)x);
			}
			Double columns = 12.0 / (xxx.size() * 1.0);
			Integer percentual = columns.intValue();
			size = new Integer[xxx.size()];
			for (int i = 0; i < xxx.size(); i++) {
				size[i] = percentual;
			}
		}

		Integer coluna = 1;
		String fechaColuna = "</div>";
		sb.append("<div class=\"space-4\"></div>" + ResultHtml.PL);
		sb.append("<div class=\"row\">");
		sb.append(abreColuna(size[0]));

		int i = -1;
		for (Object col : getMap().keySet()) {
			i++;
			List<ComponentHtml> compList = getMap().get(col);
			for (ComponentHtml com : compList) {
				if (coluna.equals(col) == false) {
					coluna++;
					sb.append(fechaColuna);

					
					if ((super.size() == 3) && (i == 1)) {
						sb.append(abreColuna(size[i], true));
					} else{
						sb.append(abreColuna(size[i], false));
					}
				}
				sb.append( com.getComponente() );
			}
		}
		sb.append(fechaColuna);
		sb.append("</div>");

		return sb.toString();
	}

	private String abreColuna(Integer size) {
		return abreColuna(size, false);
	}
	private String abreColuna(Integer size, boolean nopadding) {
		String part = "";
		//sem isso as telas ficou melhor formatada
//		if (nopadding == true) part = " no-padding-right";
		return "<div class=\"col-sm-"+ size +""+ part +"\">";
	}

	@Override
	public void createScript() throws DeveloperException {
		ComponentHtml.prepare2(getHtml(), getComponentList());
	}

	@Override
	public String getScript() {
		return "";
	}
}

