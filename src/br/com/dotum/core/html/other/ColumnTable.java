package br.com.dotum.core.html.other;

import br.com.dotum.core.html.other.TableHtml.SummaryEnum;

public class ColumnTable {

	private Class<?> type;
	private String label;
	private String attribute;
	private Integer decimal;
	private Boolean hidden;
	private SummaryEnum summary;
	private boolean editable = false;
	private boolean orderable = true;
	private String render;

	public Class<?> getType() {
		return type;
	}
	public String getLabel() {
		return label;
	}
	public ColumnTable label(String label) {
		this.label = label;
		return this;
	}
	public String getAttribute() {
		return attribute;
	}
	public ColumnTable attribute(String attribute) {
		this.attribute = attribute;
		return this;
	}
	public Integer getDecimal() {
		return decimal;
	}
	public ColumnTable decimal(Integer decimal) {
		this.decimal = decimal;
		return this;
	}
	public Boolean getHidden() {
		return hidden;
	}
	public ColumnTable hidden(Boolean hidden) {
		this.hidden = hidden;
		return this;
	}
	public SummaryEnum getSummary() {
		return summary;
	}
	public ColumnTable summary(SummaryEnum summary) {
		this.summary = summary;
		return this;
	}
	public String getRender() {
		return render;
	}
	public ColumnTable render(String render) {
		this.render = render;
		return this;
	}
	public boolean getEditable() {
		return editable;
	}
	public ColumnTable editable(Class<?> classe) {
		if (classe != null) {
			this.type = classe;
			this.editable = true;
		}
		return this;
	}
	public boolean getOrderable() {
		return orderable;
	}
	public ColumnTable orderable(boolean orderable) {
		this.orderable = orderable;
		return this;
	}


}

