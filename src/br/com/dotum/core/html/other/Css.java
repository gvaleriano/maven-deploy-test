package br.com.dotum.core.html.other;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.servlet.ApplicationContext;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.util.FormatUtils;

public class Css extends ComponentHtml implements Serializable {
	private static final long serialVersionUID = 1L;

	private String html;
	private String script;
	private Map<String, String> param = new HashMap<String, String>();

	public Css(String html) {
		this.html = html;
	}

	@Override
	public String getComponente() throws Exception {
		ApplicationContext appContext = super.getApplicationContext();

		if (appContext != null) {
			if (html.toLowerCase().endsWith(".css")) {
				String path = appContext.getFullPathOfClassRun();	
				FileMemory fm = new FileMemory(new File(path + html));
				this.html = fm.getText();

				for (String key : param.keySet()) {
					this.html = this.html.replaceAll("\\$P\\{" + key + "}", Matcher.quoteReplacement(param.get(key)));
				}
				
				this.html = "<style>" + this.html + "</style>";
			}
		}


		return html;
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		return this.script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public void addParam(String key, Object value) {
		String result = null;

		if (value instanceof Integer) {
			result = (Integer)value + "";
		} else if (value instanceof Double) {
			result = FormatUtils.formatDouble((Double)value);
		} else if (value instanceof Date) {
			result = FormatUtils.formatDate((Date)value);
		} else if (value instanceof String) {
			result = (String)value;
		}

		param.put(key, result);
	}
}
