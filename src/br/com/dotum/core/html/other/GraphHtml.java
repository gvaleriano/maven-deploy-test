package br.com.dotum.core.html.other;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.dotum.core.component.DataComponentHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.CrossTab;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.StringUtils;

public class GraphHtml extends DataComponentHtml {

	public static final String TYPE_PIE = "pie";
	public static final String TYPE_BAR = "bar";
	public static final String TYPE_COLUMN = "column";
	public static final String TYPE_LINE = "line";
	public static final String TYPE_SPLINE = "spline";

	private String title;
	private String subTitle;
	private String type;
	private String[] categoriesX;
	private String[] categoriesY;
	private HashMap<String, Object> yAxis = new HashMap<String, Object>();
	private HashMap<String, Object> xAxis = new HashMap<String, Object>();
	private HashMap<String, Object> legend = new HashMap<String, Object>();
	private boolean sharedTooltip = true; 

	private List<String> colorSerie = new ArrayList<String>();
	private List<String> nameSerie = new ArrayList<String>();
	private List<Object[]> valuesSerie = new ArrayList<Object[]>();


	public GraphHtml(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubTitle() {
		return subTitle;
	}
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	public String[] getCategoriesX() {
		return categoriesX;
	}
	public void setCategoriesX(String ... categoriesX) {
		this.categoriesX = categoriesX;
	}
	//	public String[] getCategoriesY() {
	//		return categoriesY;
	//	}
	//	public void setCategoriesY(String ... categoriesY) {
	//		this.categoriesY = categoriesY;
	//	}

	public void setColors(String ... colorSerie) {
		for (String color : colorSerie) {
			this.colorSerie.add( color );
		}
	}	

	public void addSerie(String nameSerie, BigDecimal[] valuesSerie) {
		this.nameSerie.add(nameSerie);
		this.valuesSerie.add(valuesSerie);
	}
	public void defineLegend(String layout, Integer x, Integer y, Integer borderWidth) {
		legend.put("layout", layout);
		legend.put("x", x);
		legend.put("y", y);
		legend.put("borderWidth", borderWidth);
	}
	public void defineXAxis(String title) {
		xAxis.put("title", title);
	}
	public void defineYAxis(String title) {
		yAxis.put("title", title);
	}
	public boolean isSharedTooltip() {
		return sharedTooltip;
	}
	public void setSharedTooltip(boolean sharedTooltip) {
		this.sharedTooltip = sharedTooltip;
	}

	public void createScript() {
		getHtml().addScriptLink("./fw/js/highcharts6.0.1.js", null);

		StringBuilder sb = new StringBuilder();
		sb.append("$('#"+ getId() +"').highcharts({"+ ResultHtml.PL);
		if(type.equals(TYPE_PIE)){
			sb.append(" chart:{");
			sb.append("  plotBackgroundColor: null,");
			sb.append("  plotBorderWidth: null,");
			sb.append("  plotShadow: false,");
			sb.append("  type: 'pie'");
			sb.append(" },");
		} else if(	type.equals(TYPE_COLUMN)){
			sb.append(" chart:{");
			sb.append("  type: 'column'");
			sb.append(" },");
		}
		if (StringUtils.hasValue(getTitle() )) {
			sb.append(" title:{text:'"+ getTitle() +"'},");
		} else {
			sb.append(" title:{text:null},");
		}
		if (StringUtils.hasValue(getSubTitle() )) {
			sb.append(" subtitle:{text:'"+ getSubTitle() +"'},");
		}
		sb.append( createLegend() );
		sb.append( createToolTip() );
		sb.append( createXAxis() );
		sb.append( createYAxis() );
		sb.append( createPlotOption() );
		sb.append( createSeries() );
		sb.append(" credits:{enabled: false}");
		sb.append("});");

		getHtml().addScript( sb.toString() );
	}

	private String createPlotOption() {
		StringBuilder sb = new StringBuilder();
		sb.append("plotOptions:{");
		if (this.type.equals( TYPE_PIE )) {
			sb.append("pie:{");
			sb.append("		allowPointSelect: true,");
			sb.append("		cursor:'pointer',");
			sb.append("		dataLabels:{enabled:false},");
			sb.append("		showInLegend: true");
			sb.append("}");
		} else if (this.type.equals( TYPE_BAR )) {
			//			sb.append("  }");
		} else if (this.type.equals( TYPE_COLUMN )) {
			sb.append("  column: {");
			sb.append("    pointPadding: 0.2,");
			sb.append("     borderWidth: 0,");
			sb.append("}");
		}
		sb.append("},");
		return sb.toString();
	}
	private String createLegend() {
		StringBuilder sb = new StringBuilder();
		sb.append("legend:{");

		Integer borderWidth = (Integer)legend.get("borderWidth");
		if (borderWidth == null) borderWidth = 1;
		sb.append("borderWidth:"+ borderWidth +",");
		sb.append("backgroundColor:'#FFFFFF',");
		sb.append("shadow:true");
		sb.append("},");
		return sb.toString();
	}
	private String createSeries() {
		StringBuilder sb = new StringBuilder();
		sb.append("series: [");
		String temp = "";
		if (this.type.equals(TYPE_PIE)) {
			sb.append("{name: '%', ");
			sb.append("colorByPoint: true, ");
			sb.append("data: [");
			temp = "";
			for (int i = 0; i < nameSerie.size(); i++) {
				String name = nameSerie.get(i);
				Object value = valuesSerie.get(i)[0];

				sb.append(temp);
				sb.append("['" + name.replaceAll("\'", "\\\\'") + "', " + value + "]");
				temp = ", ";
			}
			sb.append("]}");
		} else if ((this.type.equals(TYPE_COLUMN) || (this.type.equals(TYPE_LINE)))) {
			for (int i = 0; i < nameSerie.size(); i++) {
				String name = nameSerie.get(i);
				sb.append(temp);
				sb.append("{name: '"+ name.replaceAll("\'", "\\\\'") +"', ");

				if ((colorSerie != null) && (colorSerie.size() > i)) {
					sb.append("color: '"+ colorSerie.get(i) +"', ");
				}

				sb.append("data: [");
				String temp2 = "";
				for (Object value : valuesSerie.get(i)) {
					sb.append(temp2);
					sb.append(value);
					temp2 = ", ";
				}
				sb.append("]}");
				temp = "        ,";
			}
		}
		sb.append("],");

		return sb.toString();
	}
	private String createXAxis() {
		StringBuilder sb = new StringBuilder();
		String temp = "";

		boolean hasTitle = false;
		sb.append("xAxis: {");
		if (StringUtils.hasValue((String)xAxis.get("title"))) {
			sb.append("  title: {text: '"+ xAxis.get("title") +"'}");
			hasTitle = true;
		}

		if (categoriesX != null) {
			if (hasTitle) sb.append(",");
			sb.append("  categories: [");
			for (String cat : categoriesX) {
				sb.append(temp);
				sb.append("'"+ cat.replaceAll("\'", "\\\\'") +"'");
				temp = ", ";
			}
			sb.append("]");
		}
		sb.append("},");

		return sb.toString();
	}		

	private String createYAxis() {
		StringBuilder sb = new StringBuilder();
		String temp = "";

		boolean hasTitle = false;
		sb.append("yAxis: {");
		if (StringUtils.hasValue((String)yAxis.get("title"))) {
			sb.append("title: {text: '"+ yAxis.get("title") +"'}");
			hasTitle = true;
		}

		if (categoriesY != null) {
			if (hasTitle) sb.append(",");
			sb.append("categories: [");
			for (String cat : categoriesY) {
				sb.append(temp);
				sb.append("'"+ cat.replaceAll("\'", "\\\\'") +"'");
				temp = ", ";
			}
			sb.append("]");
		}
		sb.append("},");
		return sb.toString();
	}
	private String createToolTip() {
		StringBuilder sb = new StringBuilder();
		sb.append("tooltip: {");

		if (this.type.equals( TYPE_PIE )) {
			sb.append("pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'");
		} else if (sharedTooltip == true) {
			sb.append("crosshairs: true,");
			sb.append("shared: true");
		} else {
			sb.append("formatter: function() {");
			sb.append("return '<b>' + this.series.name +'</b><br/>' + this.x +': '+ this.y;");
			sb.append("}");
		}
		sb.append("},");
		return sb.toString();
	}
	@Override
	public String getComponente() throws DeveloperException {
		createScript();

		StringBuilder sb = new StringBuilder();
		//		sb.append("<div class=\"chart-container\">");
		sb.append("<div id=\""+ getId() +"\" class=\"chart\"></div>");
		//		sb.append("</div>");

		return sb.toString();
	}

	@Override
	public String getScript() {
		return null;
	}

	public void setProperties(List<? extends Bean> beanList, String column, String line, String value) throws Exception {
		HashMap<String, Object[]> valuesSerie = CrossTab.createCrossTabValues(beanList, column, line, value);
		String[] columnArray = CrossTab.createCrossTabColumns(beanList, column);

		setCategoriesX(columnArray);
		for (String serie : valuesSerie.keySet()) {
			if (serie.equals(CrossTab.COLUMN_DESCRIPTION)) continue;
			
			BigDecimal[] values = ArrayUtils.parseToBigDecimal(valuesSerie.get(serie));
			addSerie(serie, values);
		}
		defineYAxis("Valores");

	}

	@Override
	public String createScriptPopulateByJson() throws DeveloperException {
		return null;
	}

}
