package br.com.dotum.core.html.other;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;

public class HelpHtml extends ComponentHtml {

	private StringBuilder mensagemSb = new StringBuilder();;

	public void addContent(String link) {
		mensagemSb.append( link );
	}
	public void addContent(Integer selected, CssIconEnum icon, CssColorEnum color, String label, String mensagem) {
		String[] parts = null;
		if (color != null) {
			parts = color.getDescricao().split(" ");
		} else {
			parts = new String[]{"btn-primary", "blue"};
		}
		String icone = null;
		if (icon != null)
			icone  = icon.getDescricao();

		String classe = "";
		if (selected != null && selected > 0) {
			mensagemSb.append("<span class=\"btn btn-link\">");
			mensagemSb.append("<i class=\""+ icone +" "+ parts[0] +" bigger-130 icon-only\"></i>");
			mensagemSb.append("</span>");
		} else {
			classe = "btn btn-xs " + parts[0];
		}

		if (label != null) {
			mensagemSb.append("<span class=\""+ classe +" bolder\">");
			mensagemSb.append(label);
			mensagemSb.append("</span>");
			mensagemSb.append(": ");
		}
		mensagemSb.append(mensagem);
		mensagemSb.append("<br /><br />");
	}

	@Override
	public String getComponente() throws Exception {
		StringBuilder sb = new StringBuilder();


		sb.append("<div id=\""+ getId() +"\" class=\"modal aside\" data-body-scroll=\"false\" data-offset=\"true\" data-placement=\"right\" data-fixed=\"true\" data-backdrop=\"false\" tabindex=\"-1\">");
		sb.append( "<div class=\"modal-dialog\">");
		sb.append(  "<div class=\"modal-content\">");
		sb.append(   "<div class=\"modal-header no-padding\">");
		sb.append(    "<div class=\"table-header\">");
		sb.append(     "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">");
		sb.append(      "<span class=\"white\">&times;</span>");
		sb.append(     "</button>");
		sb.append(     "Ajuda da tela");
		sb.append(    "</div>");
		sb.append(   "</div>");

		sb.append(   "<div class=\"modal-body\">");

		sb.append( mensagemSb );

		sb.append(   "</div>");
		sb.append(  "</div><!-- /.modal-content -->");

		sb.append(  "<button class=\"aside-trigger btn btn-danger ace-settings-btn\" data-target=\"#"+ getId() +"\" data-toggle=\"modal\" type=\"button\">");
		sb.append(   "<i data-icon1=\"fa-plus\" data-icon2=\"fa-minus\" class=\"ace-icon fa fa-question bigger-110 icon-only\"></i>");
		sb.append(  "</button>");
		sb.append( "</div>"); // <!-- /.modal-dialog -->
		sb.append("</div>");

		if (mensagemSb.length() > 0) {
			return sb.toString();
		} else {
			return "";
		}
	}

	@Override
	public void createScript() {
		getHtml().addScript( getScript() );
	}

	@Override
	public String getScript() {
		return "$('.modal.aside').ace_aside();";
	}
}

