package br.com.dotum.core.html.other;

import java.util.List;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.core.html.form.ButtonHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public class MenuBarHtml extends ContainerComponentHtml {

	private	String title;
	
	public void add(ButtonHtml btn) throws DeveloperException {
		add(btn.getLabel(), btn);
	}	

	public String getComponente() throws Exception {
		createScript();
		title = (String) super.getAttr("title");

		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"col-xs-12 col-sm-12 widget-container-col ui-sortable  no-padding\" id=\"widget-container-col-2\">");
		sb.append(" <div id=\""+ getId() +"\" class=\"widget-box ui-sortable-handle\" id=\"widget-box-1\" style=\"opacity: 1;\">");
		sb.append("  <div class=\"widget-header\">");
		sb.append("    <h5 class=\"widget-title\">"+title+"</h5>");
		sb.append("     <div class=\"widget-toolbar\">");
		sb.append("       <ul class=\"nav navbar-nav\">");
		for (Object aba : getMap().keySet()) {
			List<ComponentHtml> cList = getMap().get(aba);
			for (ComponentHtml comp : cList) {
				sb.append( "<li>");
				sb.append( comp.getComponente() );
				sb.append( "</li>");
			}

		}
		sb.append("    </ul>" + ResultHtml.PL);
		sb.append("   </div>" + ResultHtml.PL);
		sb.append("  </div>" + ResultHtml.PL);
		sb.append(" </div>" + ResultHtml.PL);
		sb.append("</div>" + ResultHtml.PL);
		sb.append("<div class=\"clearfix\"></div>" + ResultHtml.PL);
		return sb.toString();
	}

	public void createScript() throws DeveloperException {
		ComponentHtml.prepare2(getHtml(), getComponentList());
	}

	public String getScript() throws DeveloperException {
		return "";
	}
}


