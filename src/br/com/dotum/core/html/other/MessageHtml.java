package br.com.dotum.core.html.other;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.jedi.util.StringUtils;

public class MessageHtml extends ComponentHtml {

	private Type type;
	private StringBuilder content = new StringBuilder();
	private boolean flutuante = false;

	public MessageHtml(Type type, String content) {
		this.type = type;
		if (StringUtils.hasValue(content) == false) return;
		
		this.content.append(prepareMessageHtml( content ));
	}
	public MessageHtml(Type type) {
		this(type, null);

	}

	@Override
	public String getComponente() {
		StringBuilder sb = new StringBuilder();

		if (flutuante == false)  {
			String typeStr = "";
			if(type != null) {
				if (type.equals( Type.SUCCESS )) {
					typeStr = "alert-success";
				}
				if (type.equals( Type.WARNING )) {
					typeStr = "alert-warning";
				}
				if (type.equals( Type.ERROR)) {
					typeStr = "alert-danger";
				}
				if (type.equals( Type.INFO)) {
					typeStr = "alert-info";
				}
			}


			sb.append("<div class=\"clearfix\"></div>");
			sb.append("<div class=\"space-4\"></div>");

			sb.append("<div id=\""+ this.getId() +"\" class=\"alert "+ typeStr +"\">");
			if(super.getLabel() != null) {
				sb.append("<h4>");
				sb.append("<strong>"+super.getLabel()+"</strong>");
				sb.append("</h4>");
			}
			if (StringUtils.hasValue(content.toString()))  {
				sb.append( content );
			}
			sb.append("</div>" + ResultHtml.PL);
		}

		return sb.toString();
	}

	@Override
	public void createScript() {
		if (isFlutuante()) {
			getHtml().addScript(getScript());
		}
	}

	@Override
	public String getScript() {
		String titleStr = "Sucesso";
		String typeStr = "success";
		if (type.equals( Type.WARNING )) {
			titleStr = "Atenção";
			typeStr = "warning";
		}
		if (type.equals( Type.ERROR)) {
			titleStr = "Erro";
			typeStr = "error";
		}
		if (type.equals( Type.INFO)) {
			titleStr = "";
			typeStr = "info";
		}

		String message = createScript(titleStr, typeStr, this.content.toString());


		return message;
	}

	public static String createScript(String title, String type, String message) {
		StringBuilder sb = new StringBuilder();

		if (StringUtils.hasValue(title)) {
			sb.append("message('"+ type +"','"+ message +"','"+ title +"');");
		} else {
			sb.append("message('"+ type +"','"+ message +"');");
		}

		return sb.toString();
	}

	public enum Type {
		SUCCESS("success"),
		WARNING("warning"),
		ERROR("error"),
		INFO("info");

		private String value;
		Type(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public boolean isFlutuante() {
		return flutuante;
	}
	public void setFlutuante(boolean flutuante) {
		this.flutuante = flutuante;
	}
	public void addContent(String content) {
		if (StringUtils.hasValue(content) == false) return; 
		this.content.append(content + "<br />");
	}
	public void addBreakLine() {
		addContent("&nbsp;");
	}

	public static String prepareMessageHtml(String message) {
		if (message == null) return "";
		
		message = StringUtils.parseStrToHtml(message);
		message = message.replaceAll("\"", "\\\\\"");
		
		return message;
	}

	public void setType(Type type) {
		this.type = type;
	}


}
