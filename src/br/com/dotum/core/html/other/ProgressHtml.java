package br.com.dotum.core.html.other;

import br.com.dotum.core.component.ComponentHtml;

public class ProgressHtml extends ComponentHtml {

	private Integer value;

	public ProgressHtml(Integer value) {
		this.value = value;
	}

	@Override
	public String getComponente() {
		StringBuilder sb = new StringBuilder();

		sb.append("<div class=\"easy-pie-chart percentage margin-10\" data-percent=\""+ value +"\" data-color=\"#87CEEB\" style=\"float:left;margin-right:10px;\">");
		sb.append("<span class=\"percent\">"+ value +"</span>%");
		sb.append("</div>");

		return sb.toString();
	}

	@Override
	public void createScript() {
		getHtml().addScript(getScript());
	}

	@Override
	public String getScript() {
		StringBuilder sb = new StringBuilder();

		sb.append("$('.easy-pie-chart.percentage').each(function(){");
		sb.append("$(this).easyPieChart({");
		sb.append("barColor: $(this).data('color'),");
		sb.append("trackColor: '#EEEEEE',");
		sb.append("scaleColor: false,");
		sb.append("lineCap: 'butt',");
		sb.append("lineWidth: 8,");
		sb.append("animate: ace.vars['old_ie'] ? false : 1000,");
		sb.append("size:75");
		sb.append("}).css('color', $(this).data('color'));");
		sb.append("});");


		return null;
	}

}

