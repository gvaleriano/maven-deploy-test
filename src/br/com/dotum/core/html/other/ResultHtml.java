package br.com.dotum.core.html.other;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.core.component.GlobalView;
import br.com.dotum.core.servlet.ApplicationContext;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.util.FileUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

public class ResultHtml {

	public static final String MIN = ".min";
	public static final boolean SCRIPT_IN_FILE = false;
	public static final boolean DEBUG = PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_DEBUGCODE);
	public static final String PL = (DEBUG == true ? "\n" : ""); 
	private Integer _id = 0;
	private static final Integer VERSAO_JS = 48;
	public static final String PFJQ = "$"; 

	public Integer nextId() {
		return _id++;
	}


	private ApplicationContext applicationContext;
	private UserContext userContext;
	private String title;
	private String code;
	private String ga;
	private boolean robots = false;
	private boolean interno = true;
	private boolean ajax = false;
	private StringBuilder script = new StringBuilder();
	private StringBuilder scriptTemp = new StringBuilder();
	private List<ComponentHtml> compList = new ArrayList<ComponentHtml>();
	private Map<Double, List<ComponentHtml>> listTop = new LinkedHashMap<Double, List<ComponentHtml>>();
	public boolean facebook = false;

	private List<String> scriptLink = new ArrayList<String>();
	private List<String> browserScriptLink = new ArrayList<String>();

	private HashMap<String, String> attr = new HashMap<String, String>();
	private List<String> styleLink = new ArrayList<String>();
	private List<String> browserStyleLink = new ArrayList<String>();
	private String classBody = "no-skin";
	private String style = null;
	private Integer versionJQuery = 2;
	private String url;
	private String descricao;
	private String urlImagem;

	public void addComponent(ComponentHtml ... list) {
		setApplicationContext(list);
		for (ComponentHtml c : list) {
			c.setHtml(this);
			this.compList.add(c);
			
		}
	}


	private void setApplicationContext(ComponentHtml ... list) {
		
		for (ComponentHtml c : list) {
			
			if (c instanceof ContainerComponentHtml) {
				ContainerComponentHtml cc = (ContainerComponentHtml)c;
				setApplicationContext(cc.getComponentArray());
			} else {
				c.setApplicationContext(applicationContext);
			}
		}
	}


	private List<String> validCompDuplicate(List<String> idList, List<ComponentHtml> list) throws DeveloperException {
		if (idList == null) idList = new ArrayList<String>();

		for (ComponentHtml c : list) {
			if (c instanceof ContainerComponentHtml) {
				idList.addAll( validCompDuplicate(null, ((ContainerComponentHtml)c).getComponentList()) );
			} else {
				String id = c.getId();
				if (idList.contains(id)) {
					throw new DeveloperException("Você adicionou o componente " + c.getClass() + " mais de uma vez na tela verifique. [Id: "+ id +", Name: "+ c.getName() +"]");
				}
				idList.add(id);
			}
		}
		
		return idList;
	}


	public void addComponentTop(Double label, List<ComponentHtml> list) {
		this.listTop.put(label, list);
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return this.title;
	}

	public ResultHtml(ApplicationContext applicationContext) {
		//		createScript();

		addScriptDefault();

		this.applicationContext = applicationContext;
		this.script.append("var _id=null;");
		this.script.append("var _idSlave=null;");
	}

	public void addScriptDefault() {
		addStyleLink("./components/bootstrap/dist/css/bootstrap"+ MIN +".css", null);
		addStyleLink("./components/font-awesome/css/font-awesome"+ MIN +".css", null);
		addStyleLink("./assets/css/ace-fonts"+ MIN +".css", null);
		addStyleLink("./components/_mod/jquery-ui/jquery-ui"+ MIN +".css", null);
		addStyleLink("./components/jquery.gritter/css/jquery.gritter"+ MIN +".css", null);
		addStyleLink("./components/bootstrap-multiselect/dist/css/bootstrap-multiselect"+ MIN +".css", null);
		addStyleLink("./components/bootstrap-datepicker/dist/css/bootstrap-datepicker3"+ MIN +".css", null);

		addStyleLink("./assets/css/ace"+ MIN +".css", null);
		//		addStyleLink("./assets/css/ace-skins"+ min +".css", null);

		// acho que nao usamos esse aqui
		//		addStyleLink("./assets/css/ace-rtl"+ min +".css", null);
		addStyleLink("./assets/css/ace-part2"+ MIN +".css", "lte IE 9");
		addStyleLink("./assets/css/ace-ie"+ MIN +".css", "lte IE 9");


		// TODO trocar esse plugin pelo bootstrap-tour (http://bootstraptour.com)
		if (this.robots == false) addStyleLink("./fw/css/bootstro"+ MIN +".css", null);
		addStyleLink("./fw/css/jquery.mentionsInput.css", null);
		addStyleLink("./fw/css/fw-global.css", null);


		//
		// TOP
		//
		addScriptLink("./assets/js/ace-extra"+ MIN +".js", null);
		addScriptLink("./components/html5shiv/dist/html5shiv"+ MIN +".js", "lte IE 8");
		addScriptLink("./components/respond/dest/respond"+ MIN +".js", "lte IE 8");
		// tem mais 2 js para isto funcionar.. comente e justifique sua resposta
		//		addStyleLink("./components/_mod/x-editable/bootstrap-editable.css", null);
		addStyleLink("./custom/css/global.css", null);



		//
		// BOTTOM
		//
		if (versionJQuery.equals(2)) {
			addScriptLink("./components/jquery/dist/jquery"+ MIN +".js", null);
		} else {
			addScriptLink("./components/jquery.1x/dist/jquery"+ MIN +".js", null);
		}

//		addScriptLink("./custom/js/jquery.maskMoney"+ MIN +".js", null);
		addScriptLink("./components/bootstrap/dist/js/bootstrap"+ MIN +".js", null);
		addScriptLink("./components/jquery-validation/dist/jquery.validate.js", null);
		addScriptLink("./components/jquery-validation/dist/additional-methods.js", null);

		addScriptLink("./components/jquery.gritter/js/jquery.gritter"+ MIN +".js", null);//msg
		//		addScriptLink("./assets/js/src/elements.scroller.js", null);
		//		addScriptLink("./assets/js/src/elements.aside.js", null);
		addScriptLink("./assets/js/ace-elements"+ MIN +".js", null);
		addScriptLink("./assets/js/ace"+ MIN +".js", null);
		addScriptLink("./components/jquery-ui/jquery-ui"+ MIN +".js", null);

		//		addScriptLink("./components/chosen/chosen.jquery"+ min +".js", null);
		addScriptLink("./components/autosize/dist/autosize"+MIN+".js", null);
		if (this.robots == false) addScriptLink("./components/jquery-inputlimiter/jquery.inputlimiter"+MIN+".js", null);

		addScriptLink("./components/jquery.maskmoney/jquery.maskMoney"+ MIN +".js", null);
		addScriptLink("./components/jquery.maskedinput/dist/jquery.maskedinput"+ MIN +".js", null);
		//		if (this.robots == false) addScriptLink("./assets/js/src/ace.widget-box.js", null);
		if (this.robots == false) addScriptLink("./assets/js/src/elements.fileinput.js", null);
		addScriptLink("./components/bootstrap-datepicker/dist/js/bootstrap-datepicker"+ MIN +".js", null);
		addScriptLink("./components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR"+ MIN +".js", null);
		addScriptLink("./components/_mod/bootstrap-multiselect/bootstrap-multiselect"+ MIN +".js", null);
		//		addScriptLink("./components/_mod/x-editable/bootstrap-editable.js", null);
		//		addScriptLink("./components/_mod/x-editable/ace-editable.js", null);

		if (this.robots == false) addScriptLink("./fw/js/jquery.autonumeric.js", null);
		if (this.robots == false) addScriptLink("./fw/js/jquery.maskMoney"+ MIN +".js", null);
		if (this.robots == false) addScriptLink("./fw/js/bootstro"+ MIN +".js", null);
		addScriptLink("./fw/js/underscore-min.js", null);


		// este plugin usa quando tem no textarea o @ para fazer o mesmo efeito do autocomplete
		addScriptLink("./fw/js/jquery.mentionsInput.js", null);
		if (this.robots == false) addScriptLink("./fw/js/FileSaver"+MIN+".js", null);
		addScriptLink("./fw/js/fw-function.js", null);

		if (this.robots == false) addScriptLink("./fw/js/bootstrap-session-timeout.min.js", null);
	}

	public void addScriptLink(String path, String browser) {
		if (scriptLink.contains(path) == true) return;
		scriptLink.add(path);
		browserScriptLink.add(browser);
	}

	public void addStyleLink(String path, String browser) {
		if (styleLink.contains(path) == true) return;
		styleLink.add(path);
		browserStyleLink.add(browser);
	}

	public void addStyle(String style){
		this.style = style;
	}

	public void addScript(String script) {
		if (StringUtils.hasValue(script) == false) return;
		this.scriptTemp.append(script + ResultHtml.PL);
	}


	/**
	 * 
	 * @param compList
	 * @param table = Serve para nao criar o script da TableHtml
	 * @throws DeveloperException
	 */
	public void criaScriptRecursive(List<ComponentHtml> compList, boolean table) throws DeveloperException {
		for (ComponentHtml c : compList) {
			if (c.getClass().getSuperclass().equals(ContainerComponentHtml.class)) {
				criaScriptRecursive(((ContainerComponentHtml) c).getComponentList(), table);

				if (table == false) continue;
				addScript(c.getScript());
			} else {
				if (c instanceof TableHtml == table) continue;

				c.setHtml(this);
				c.createScript();
			}
		}
	}

	public String getComponente() throws Exception {
		validCompDuplicate(null, compList);
		
		criaScriptRecursive(compList, false);
		criaScriptRecursive(compList, true);

		for (Double x : listTop.keySet()) {
			for (ComponentHtml c : listTop.get(x)) {
				c.createScript();
			}
		}


		//
		// HTML AQUI!!!
		//
		StringBuilder sb = new StringBuilder();
		if (ajax == true) {

			for (ComponentHtml c : compList) {
				sb.append( c.getComponente() );
			}


		} else {
			sb.append("<!DOCTYPE html>" + ResultHtml.PL);
			sb.append("<html lang=\"pt-br\">" + ResultHtml.PL);
			sb.append("<head>" + ResultHtml.PL);
			sb.append("<title>"+ PropertiesUtils.getString(PropertiesConstants.SYSTEM_TITLE) +"</title>" + ResultHtml.PL);

			String favIcon = PropertiesUtils.getString(PropertiesConstants.SYSTEM_FAVICON);
			if (StringUtils.hasValue(favIcon) == false) {
				favIcon = "./fw/images/favicon.ico";
			}
			sb.append("<link rel=\"shortcut icon\" href=\""+ favIcon +"\" type=\"image/x-icon\" />" + ResultHtml.PL);
			sb.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">");
			sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" + ResultHtml.PL);
			sb.append("<meta http-equiv=\"Content-Language\" content=\"pt-BR\" />" + ResultHtml.PL);

			if (this.facebook == true) {
				sb.append("<meta property=\"og:url\" content=\"https://"+this.url+"\" />" + ResultHtml.PL);
				sb.append("<meta property=\"og:type\" content=\"website\" />" + ResultHtml.PL);

				String nome = PropertiesUtils.getString(PropertiesConstants.SYSTEM_TITLE);
				sb.append("<meta property=\"og:title\" content="+nome+" />" + ResultHtml.PL);
				sb.append("<meta property=\"og:description\" content=\""+this.descricao+"\" />" + ResultHtml.PL);
				sb.append("<meta property=\"og:image\" content=\"https://"+this.urlImagem+"\" />" + ResultHtml.PL);

				sb.append("<script>");
				sb.append("(function(d, s, id) {");
				sb.append("var js, fjs = d.getElementsByTagName(s)[0];");
				sb.append("if (d.getElementById(id)) return;");
				sb.append("js = d.createElement(s); js.id = id;");
				sb.append("js.src = \"//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10\";");
				sb.append("fjs.parentNode.insertBefore(js, fjs);");
				sb.append("}(document, 'script', 'facebook-jssdk'));");
				sb.append("</script>");
			}

			if (robots == false) {
				sb.append("<meta name=\"robots\" content=\"noindex,nofollow\" />" + ResultHtml.PL);
			}
			if(ga != null) {
				sb.append("<script async src=\"https://www.googletagmanager.com/gtag/js?id="+ ga +"\"></script>");
				sb.append("<script>");
				sb.append("window.dataLayer = window.dataLayer || [];");
				sb.append("function gtag(){dataLayer.push(arguments);}");
				sb.append("gtag('js', new Date());");
				sb.append("gtag('config', '"+ ga +"');");
				sb.append("</script>");
			}

			this.script.append(this.scriptTemp);
			if (SCRIPT_IN_FILE == true) {

				StringBuilder sc2 = new StringBuilder();
				sc2.append(PFJQ + "(document).ready(function() {");
				sc2.append(this.script.toString());
				sc2.append("});");

				String content = sc2.toString();


				String nameFile = "script" + content.hashCode() + ".js";
				String path = ResultHtml.class.getResource("/").getPath();
				path = path.substring(1, path.indexOf("/WEB-INF/")) + "/";

				//				String path = userContext.getServerPath();
				String pathRelativo = "./fwgen/"+ nameFile;
				String fullName = path + pathRelativo;

				if (FileUtils.existFile(fullName) == false) {
					FileMemory fm = new FileMemory(content, fullName);
					fm.createFile();
				}



				addScriptLink(pathRelativo, null);

			}


			//
			// LINKS
			//
			String VERSION = "?v="+ VERSAO_JS +"-" + PropertiesUtils.getString(PropertiesConstants.SYSTEM_VERSION);
			for (int i = 0; i < styleLink.size(); i++) {
				String str = styleLink.get(i);
				String bro = browserStyleLink.get(i);

				if (StringUtils.hasValue(bro)) sb.append("<!--[if "+ bro +"]>" + ResultHtml.PL);
				sb.append("<link type=\"text/css\" rel=\"stylesheet\" href=\""+ str + VERSION + "\" />" + ResultHtml.PL); 
				if (StringUtils.hasValue(bro)) sb.append("<![endif]-->" + ResultHtml.PL);
			}
			for (int i = 0; i < scriptLink.size(); i++) {
				String str = scriptLink.get(i);
				String bro = browserScriptLink.get(i);

				if (StringUtils.hasValue(bro)) sb.append("<!--[if "+ bro +"]>" + ResultHtml.PL);
				if (str.startsWith("http")) VERSION = "";
				sb.append("<script type=\"text/javascript\" charset=\"utf-8\" src=\""+ str + VERSION +"\"></script>" + ResultHtml.PL);
				if (StringUtils.hasValue(bro)) sb.append("<![endif]-->" + ResultHtml.PL);
			}


			if(StringUtils.hasValue(this.style)) {
				sb.append("<style>" + ResultHtml.PL);
				sb.append(style + ResultHtml.PL);
				sb.append("</style>" + ResultHtml.PL);
			}

			//
			// SCRIPTS
			//
			GlobalView view = new GlobalView(applicationContext.getRequest());			

			// ESSE CARA AI TA MUITO LENTO!!!
			// PRECISA DE AGITUS
			Html top = view.criaHtmlTop(this, userContext, code, title, listTop);

			// TODO criar o arquivo em disco com o nome da pagina que esta sendo carregada
			// colocar aqui o link do js

			// se o arquivo ja existir:
			// veirifica um hash do arquivo para ver se mudou algo...
			// caso tenha mudado recria o arquivo
			// caso nao tenha mudado ... deixa queto
			if (SCRIPT_IN_FILE == false) {
				sb.append("<script>" + ResultHtml.PL);
				sb.append(PFJQ + "(document).ready(function() {");
				sb.append( this.script );
				sb.append("});");
				sb.append("</script>" + ResultHtml.PL);

			}
			sb.append("</head>" + ResultHtml.PL);


			StringBuilder tt = new StringBuilder();
			for (String key :attr.keySet()) {
				tt.append(" key=\""+ attr.get(key) +"\"");
			}


			sb.append("<body class=\""+ classBody +"\""+ tt +">" + ResultHtml.PL);
			if (interno == true) sb.append( top.getComponente() );

			for (ComponentHtml c : compList) {
				if (DEBUG == true) sb.append( "<!-- INICIO COMPONENTE: "+ c.getId() +" -->" + PL); 
				sb.append( c.getComponente() );
				if (DEBUG == true) sb.append( PL );
				if (DEBUG == true) sb.append( "<!-- FIM COMPONENTE: "+ c.getId() +" -->" + PL);
				if (DEBUG == true) sb.append( PL );
			}

			if (interno == true) sb.append( view.criaHtmlBottom(userContext).getComponente() );

			sb.append("</body>" + ResultHtml.PL);
			//			sb.append("<script>" + ResultHtml.PL);
			//			sb.append("$('button.btn').click(function () {" + ResultHtml.PL);
			//			sb.append("var btn = $(this);" + ResultHtml.PL);
			//			sb.append("btn.button('loading');" + ResultHtml.PL);
			//			sb.append("setTimeout(function () {" + ResultHtml.PL);
			//			sb.append("btn.button('reset')" + ResultHtml.PL);
			//			sb.append("}, 2000)" + ResultHtml.PL);
			//			sb.append("});" + ResultHtml.PL);
			//			sb.append("</script>" + ResultHtml.PL);

			sb.append("</html>" + ResultHtml.PL);

		}


		return sb.toString();
	}

	public void setAjax() {
		this.ajax = true;
	}

	public void setMensagem(Html h) {

	}

	public boolean isAjax() {
		return ajax;
	}

	public void addClassBody(String classBody) {
		this.classBody = classBody;
	}

	public boolean isInterno() {
		return interno;
	}

	public void setInterno(boolean interno) {
		this.interno = interno;
	}

	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}
	public UserContext getUserContext() {
		return userContext;
	}


	public boolean isRobots() {
		return robots;
	}

	public void setRobots(boolean robots, String ga) {
		this.robots = robots;
		this.ga = ga;
	}
	public void setFacebook(boolean facebook, String url, String descricao, String urlImagem) {
		this.facebook = facebook;
		this.url = url;
		this.descricao = descricao;
		this.urlImagem = urlImagem;
	}

	public void setVerionJQuery(Integer version) {
		this.versionJQuery = version;

	}

	public void createScript() {

		StringBuilder sb = new StringBuilder();

		//<!-- Start of Smartsupp Live Chat script -->
		sb.append("var _smartsupp = _smartsupp || {};" + PL);
		sb.append("_smartsupp.key = 'd6a7db3cfc418c8618d8f5df46a1eac1f6630c1f';" + PL);
		sb.append("window.smartsupp||(function(d) {" + PL);
		sb.append("var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];" + PL);
		sb.append("s=d.getElementsByTagName('script')[0];c=d.createElement('script');" + PL);
		sb.append("c.type='text/javascript';c.charset='utf-8';c.async=true;" + PL);
		sb.append("c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);" + PL);
		sb.append("})(document);" + PL);

		addScript(sb.toString());

	}

	public void addAttr(String attribute, String value) {
		attr.put(attribute, value);
	}
}
