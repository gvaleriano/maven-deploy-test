package br.com.dotum.core.html.other;

import java.util.List;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.core.component.DataComponentHtml;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;


public class TabHtml extends ContainerComponentHtml {

	public String getComponente() throws Exception {

		String cssClass = " class=\"active\"";
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"space-4\"></div>" + ResultHtml.PL);
		sb.append("<div class=\"clearfix\"></div>" + ResultHtml.PL);
		sb.append("<div id=\""+ getId() +"\">" + ResultHtml.PL);
		sb.append("<ul id=\""+getId()+"\" class=\"nav nav-tabs tab-color-blue\">" + ResultHtml.PL);

		int i = -1;
		for (Object aba : getMap().keySet()) {
			i++;
			List<ComponentHtml> cList = getMap().get(aba);

			if (i > 0) cssClass = "";

			sb.append(  "<li"+cssClass+">" + ResultHtml.PL);
			sb.append(   "<a data-toggle=\"tab\" class=\"aba" + getId() + i +"\" href=\"#aba" + getId() + i +"\">"+ aba +"</a>" + ResultHtml.PL);
			sb.append(  "</li>" + ResultHtml.PL);
		}
		sb.append( "</ul>" + ResultHtml.PL);
		sb.append( "<div class=\"tab-content\">" + ResultHtml.PL);


		cssClass = " active";
		i = -1;
		for (Object aba : getMap().keySet()) {
			i++;
			List<ComponentHtml> cList = getMap().get(aba);

			if (i > 0) cssClass = "";
			sb.append(  "<div id=\"aba" + getId() + i +"\" class=\"tab-pane"+ cssClass +"\">" + ResultHtml.PL);
			for (ComponentHtml comp : cList) {
				sb.append( comp.getComponente() );
			}
			sb.append(   "<div class=\"clearfix\"></div>" + ResultHtml.PL);
			sb.append(  "</div>" + ResultHtml.PL);
		}

		sb.append( "</div>" + ResultHtml.PL);
		sb.append("</div>" + ResultHtml.PL);

		return sb.toString();
	}

	@Override
	public void createScript() throws DeveloperException {
		ComponentHtml.prepare2(getHtml(), getComponentList());
		
		// TODO por que isso esta comentado???
//		String script = getScript();
//		getHtml().addScript(script);
	}

	@Override
	public String getScript() throws DeveloperException {
		StringBuilder sb = new StringBuilder();

		int i = -1;
		for (Object aba : getMap().keySet()) {
			i++;

			sb.append("$('body').on('click', '.aba" + getId() + i +"', function(e){");
			Boolean bList = getReloadMap().get(aba);
			if (bList.equals(true)) {
				List<ComponentHtml> cList = getMap().get(aba);
				
				sb.append(createScriptResetaForms(cList));
				sb.append( DataComponentHtml.createScriptPopulateByJsonOpenWindowIfAllOk(null, cList, null, getId()) );
			}

			sb.append("});");
		}

		return sb.toString();
	}

	public String createScriptAtivaAba(String aba) {
		int i = -1;
		for (Object abaT : getMap().keySet()) {
			i++;
			if (abaT.toString().equalsIgnoreCase(aba) == false) continue;
			break;
		}
		
		return "$('[href=\"#aba" + getId() + i +"\"]').tab('show');";
//		return "$('#"+ getId() +"').tabs({ active: "+ i +" });";
	}
	
	
	private String createScriptResetaForms(List<ComponentHtml> compList) {
		StringBuilder sb = new StringBuilder();
		for(ComponentHtml comp : compList) {
			
			if (comp instanceof FormHtml) {
				sb.append(((FormHtml) comp).createScriptClean());
			} else if (comp instanceof ContainerComponentHtml) {
				ContainerComponentHtml container = (ContainerComponentHtml) comp;
				sb.append(createScriptResetaForms(container.getComponentList()));
			}
		}
		
		return sb.toString();
	}
}

