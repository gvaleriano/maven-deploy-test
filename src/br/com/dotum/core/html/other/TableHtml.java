package br.com.dotum.core.html.other;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.ajax.ScriptHelper;
import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.AbstractCommon;
import br.com.dotum.core.component.AbstractView;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.core.component.DataComponentHtml;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.enuns.CssColorEnum;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.ButtonHtml;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.StringUtils;

public class TableHtml extends DataComponentHtml {
	private static Log LOG = LogFactory.getLog(TableHtml.class);

	private static final Integer DECIMAL_PLACE_DEFAULT = -1;

	// se por acaso mudar isso aqui... alterar tbm no fw-funcion.js
	// nas 3 function: tableFormatIcon, tableFormatLabel, tableFormatContent
	public static final String SEPARATOR = "|";
	
	

	private List<ButtonTable> buttonList = new ArrayList<ButtonTable>();
	private List<ButtonHtml> buttonInLineList = new ArrayList<ButtonHtml>();
	private List<ButtonHtml> buttonTopList = new ArrayList<ButtonHtml>();
	private List<WindowHtml> winList = new ArrayList<WindowHtml>();

	private List<ColumnTable> columnList = new ArrayList<ColumnTable>();
	private Map<String, Object> paramMap = new HashMap<String, Object>();

	private boolean timeline = false;
	private WindowHtml parenteWin;
	private boolean serverSide = false;
	private String classHidden = " hidden-480";
	private boolean checkbok = false;
	private boolean onLoad = true;


	public enum SummaryEnum {

		SUM("sum"), COUNT("count"), AVG("avg"),;

		private String descricao;

		SummaryEnum(String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		@Override
		public String toString() {
			return getDescricao();
		}
	}

	/**
	 * Usar o construtor com method
	 */
	@Deprecated
	public TableHtml() {
	}

	public TableHtml(String dataMethod) throws DeveloperException {
		super.setDataMethod(dataMethod);
	}

	public TableHtml(String dataMethod, Integer dataId) {
		super.setDataMethod(dataMethod);
		super.setDataId(dataId);
	}

	@Deprecated
	public ColumnTable addColumn(Class<?> type, String column, String attribute, Integer decimal, boolean hidden, SummaryEnum summary, boolean editabled) {
		if (decimal == null) decimal = DECIMAL_PLACE_DEFAULT;

		ColumnTable b = new ColumnTable();
		b.label(column);
		b.attribute(attribute);
		b.decimal(decimal);
		b.hidden(hidden);
		b.summary(summary);
		b.editable(type);

		columnList.add(b);

		return b;
	}

	@Deprecated
	public ColumnTable addColumn(String column, String attribute, Integer decimal) {
		return addColumn(null, column, attribute, decimal, false, null, false);
	}
	public ColumnTable addColumn(String column, String attribute) {
		return addColumn(null, column, attribute, null, false, null, false);
	}
	@Deprecated
	public ColumnTable addColumn(String column, String attribute, boolean hidden) {
		return addColumn(null, column, attribute, null, hidden, null, false);
	}
	@Deprecated
	public ColumnTable addColumn(String column, String attribute, Integer decimal, boolean hidden) {
		return addColumn(null, column, attribute, decimal, hidden, null, false);
	}
	@Deprecated
	public ColumnTable addColumn(String column, String attribute, SummaryEnum summary) {
		return addColumn(null, column, attribute, null, false, summary, false);
	}
	@Deprecated
	public ColumnTable addColumn(String column, String attribute, Integer decimal, SummaryEnum summary) {
		return addColumn(null, column, attribute, decimal, false, summary, false);
	}


	public List<String> getAttribute() {
		List<String> result = new ArrayList<String>();
		for (ColumnTable b : columnList) {
			result.add(b.getAttribute());
		}

		return result;
	}

	public String getComponente() throws Exception {
		StringBuilder sb = new StringBuilder();
		for (ButtonHtml btn : buttonTopList) {
			sb.append( btn.getComponente() );
		}



		sb.append("<div class=\"space-4\"></div>" + ResultHtml.PL);


		// ABRE 2 DIV ROW e COL-XS-12
		sb.append("<div class=\"row\">" + ResultHtml.PL);
		sb.append("<div class=\"col-xs-12 \">" + ResultHtml.PL);



		if (StringUtils.hasValue(getLabel())) {
			sb.append("<div class=\"table-header\">");
			sb.append(getLabel());
			sb.append("</div>");
		}

		// sb.append("<div class=\"scrollable ace-scroll\">" + ResultHtml.PL);
		String cssClasse = "class=\"table table-striped table-hover dataTable no-footer\" ";
		if (timeline == true) cssClasse = "";
		sb.append("<table id=\"" + super.getId() + "\" " + cssClasse + " cellspacing=\"0\">" + ResultHtml.PL);
		sb.append(createHtmlTableHead());
		sb.append(createHtmlTableFoot());
		sb.append("</table>" + ResultHtml.PL);
		//		sb.append("<div class=\"testesteste\">...</div>" + ResultHtml.PL);



		// FECHA 2 DIV ROW e COL-XS-12
		sb.append("</div>");// col-12
		sb.append("</div>");// row

		for (ComponentHtml win : winList) {
			sb.append(win.getComponente());
		}

		return sb.toString();
	}

	private void prepareButton() throws DeveloperException {
		for (ButtonTable btn : buttonList) {
			if (btn.parseButtonType().equals(btn.TYPEPARSEBUTTONDEFAULT)) {
				addButton(btn.getAction(), btn.getMethod(), btn.getDownload(), btn.getIcon(), btn.getColor(), btn.getLabel(), btn.getScript(), btn.getCompWindowArray(), btn.getCompDataArray(), btn.getSelecteds());
			} else {
				addButton(btn);
			}
		}
	}

	private String createHtmlTableHead() {
		StringBuilder sb = new StringBuilder();

		sb.append("<thead>");
		sb.append("<tr>");

		// COLUNA PARTE DO CHECKBOXCHECKBOX
		if (checkbok == true)
			sb.append("<th style=\"width:30px;\" class=\"center\"><input type=\"checkbox\" class=\"ace\"><span class=\"lbl\"></span></th>");


		// INICIO LABEL DAS COLUNAS
		for (ColumnTable b : columnList) {
			boolean temHidden = b.getHidden();
			String label = b.getLabel();

			String classCss = "";
			if (temHidden == true) classCss += " class=\"" + classHidden + "\"";

			sb.append("<th"+classCss+">");
			sb.append(label);
			sb.append("</th>" + ResultHtml.PL);
		}
		int size = 39 * buttonInLineList.size();
		if (size > 225) size = 225;
		if (buttonInLineList.size() > 0) {
			sb.append("<th style=\"width:"+ size +"px;\">");
			sb.append("Ações");
			sb.append("</th>");
		}
		// INICIO LABEL DAS COLUNAS



		sb.append("</tr>" + ResultHtml.PL);
		sb.append("</thead>");

		return sb.toString();
	}
	private String createHtmlTableFoot() {
		StringBuilder sb = new StringBuilder();

		// COLUNAS DA TABELA
		String styleCss = "";
		if (hasSummary())
			styleCss = " style=\"border-top: 1px solid #999;\"";



		sb.append("<tfoot>");
		sb.append("<tr class=\"well\">");

		// COLUNA PARTE DO CHECKBOX
		if (checkbok == true)
			sb.append("<th class=\"center\""+ styleCss +"></th>");


		for (ColumnTable b : columnList) {
			boolean temHidden = b.getHidden();

			String classCss = "";
			if (temHidden == true) classCss = " class=\""+classHidden+"\"";
			sb.append("<th" + styleCss + classCss+"></th>");
		}

		// COLUNA DA TABELA CASO TENHA ACOES
		if (buttonInLineList.size() > 0) {
			sb.append("<th" + styleCss + "></th>");
		}



		sb.append("</tr>");
		sb.append("</tfoot>");

		return sb.toString();
	}
	private boolean hasSummary() {
		for (ColumnTable b : columnList) {
			SummaryEnum x = b.getSummary();

			if (x != null)
				return true;
		}
		return false;
	}

	@Override
	public void createScript() throws DeveloperException {
		prepareButton();


		getHtml().addScriptLink("./components/datatables/media/js/jquery.dataTables"+ ResultHtml.MIN +".js", null);

		if (checkbok == true)
			getHtml().addScriptLink("./components/datatables.net-select/js/dataTables.select.min.js", null);

		getHtml().addScriptLink("./components/datatables.editor/js/dataTables.editor.min.js", null);
		getHtml().addStyleLink("./components/datatables.editor/css/editor.dataTables.min.css", null);

		getHtml().addScriptLink("./fw/js/jquery.datatables.impl.js", null);		
		getHtml().addScript(getScript());

		if ((getDataClass().getSuperclass().equals(AbstractView.class) == false)
				&& (getDataMethod().equals("doData"))) {
			LOG.error("Na classe "+getDataClass().getSimpleName()+", não pode usar o nome \"doData\" para o TableHtml.");
		}
	}

	@Override
	public String getScript() throws DeveloperException {
		StringBuilder sb = new StringBuilder();

		if (super.getDataClass() == null)
			throw new DeveloperException("Não foi informado a classe para buscar os dados da Table referente ao metodo \"" + super.getDataMethod() + "\" (Id: " + getId() + ").");


		if (StringUtils.hasValue(super.getEditMethod())) {
			UrlHtml url = new UrlHtml();
			url.setAction(getDataClass());
			url.setMethod(getEditMethod());

			sb.append("                                       editor_"+ getId() +" = new $.fn.dataTable.Editor({                                                 " + ResultHtml.PL);
			sb.append("                                           legacyAjax:true,                                                                               " + ResultHtml.PL);
			//			sb.append("                                           ajax: '"+ url.getComponente() +"',                                                             " + ResultHtml.PL);
			sb.append("                                           ajax:{                                                                                         ");
			sb.append("                                               url:'" + url.getComponente() + "',                                                         ");
			sb.append("                                               type:'POST',                                                                               ");
			sb.append("                                               data:function(d){                                                                          ");
			sb.append("                                                   d.data.id = d.id;                                                                      ");
			sb.append("                                                   return d.data;                                                                         ");
			sb.append("                                               },                                                                                         " + ResultHtml.PL);
			sb.append("                                               success:function(data, textStatus, request){                                               " + ResultHtml.PL);
			if (isOnLoad()) sb.append(                                                     createScriptPopulateByJson()                                                            + ResultHtml.PL);
			sb.append("                                               }                                                                                          " + ResultHtml.PL);
			sb.append("                                           },                                                                                             " + ResultHtml.PL);
			sb.append("                                           table: '#" + super.getId() + "',                                                               " + ResultHtml.PL);
			sb.append(                                            createScriptTableEditableField()                                                                 + ResultHtml.PL);
			sb.append("                                           idSrc:  'id'                                                                                   " + ResultHtml.PL);
			sb.append("                                       });                                                                                                " + ResultHtml.PL);


			if (buttonInLineList.size() > 0) 
				sb.append("                                   $('#" + super.getId() + "').on( 'click', 'tbody td:not(:first-child)', function (e) {              " + ResultHtml.PL);
			else 
				sb.append("                                   $('#" + super.getId() + "').on( 'click', 'tbody td', function (e) {                                " + ResultHtml.PL);

			sb.append("                                           editor_"+ getId() +".inline( this, {                                                           " + ResultHtml.PL);
			//			sb.append("                                               onBlur: 'submit',                                                                          " + ResultHtml.PL);
			sb.append("                                               scope: 'cell'                                                                              " + ResultHtml.PL);
			//			sb.append("                                               submit: 'allIfChanged'                                                                           " + ResultHtml.PL);
			sb.append("                                           } );                                                                                           " + ResultHtml.PL);
			sb.append("                                       } );                                                                                               " + ResultHtml.PL);
			//			sb.append("                                       var selected = [];                                                                                 " + ResultHtml.PL);
		}

		//
		// SCRIPT DA TABLE
		//
		sb.append("                      var oTable_" + getId() + "=$('#" + super.getId() + "').DataTable({                                                         " + ResultHtml.PL);
		sb.append("                          language:{url:'./fw/json/dataTables.pt_br.json'},                                                                      " + ResultHtml.PL);
		sb.append("                          dom:'t',                                                                                                               " + ResultHtml.PL);
		sb.append("                          processing: true,                                                                                                      " + ResultHtml.PL);
		if (serverSide) sb.append("          serverSide: true,                                                                                                      " + ResultHtml.PL);
		else sb.append("                     bPaginate: false,                                                                                                      " + ResultHtml.PL);
		sb.append(                           createScriptTableSummary()                                                                                               + ResultHtml.PL);
		sb.append(                           createScriptTableAjax()                                                                                                  + ResultHtml.PL);
		sb.append(                           createScriptTableColumn()                                                                                                + ResultHtml.PL);
		sb.append(                           createScriptTimeLine()                                                                                                   + ResultHtml.PL);
		// if (checkbok == true) sb.append("    select: 'single',                                                                                                       " + ResultHtml.PL);
		// if (checkbok == true) sb.append("    select: 'multi',                                                                                                       " + ResultHtml.PL);
		if (checkbok == true) sb.append("    select: {style: 'multi', selector: 'td:not(:last-child)'},                                                             " + ResultHtml.PL);
		sb.append("                          responsive: true,                                                                                                      " + ResultHtml.PL);
		sb.append("                          aaSorting:[],                                                                                                          " + ResultHtml.PL);
		sb.append("                          sScrollXInner: \"100%\",                                                                                               " + ResultHtml.PL);
		sb.append("                          bAutoWidth: false,                                                                                                     " + ResultHtml.PL);
		sb.append("                          rowId: 'id',                                                                                                           " + ResultHtml.PL);
		sb.append("                          bScrollCollapse: true                                                                                                  " + ResultHtml.PL);
		sb.append("                      });                                                                                                                        " + ResultHtml.PL);


		//
		// SCRIPT DAS ACOES
		//
		for (ButtonHtml btn : buttonInLineList) {
			sb.append(btn.getScript());
		}
		for (ButtonHtml btn : buttonTopList) {
			sb.append(btn.getScript());
		}
		sb.append("	$.fn.dataTable.ext.errMode = 'none';");


		for (WindowHtml window : winList) {
			List<ComponentHtml> compList = window.getComponentList();
			ComponentHtml.prepare(getHtml(), compList);
		}

		// BEGIN SELECTED
		//		sb.append("$('#"+ getId() +" tbody').on('click', 'tr', function () {" + ResultHtml.PL);
		//		sb.append("var id = this.id;" + ResultHtml.PL);
		//		sb.append("var index = $.inArray(id, selected);" + ResultHtml.PL);
		//		sb.append("if ( index === -1 ) {" + ResultHtml.PL);
		//		sb.append("selected.push( id );" + ResultHtml.PL);
		//		sb.append("} else {" + ResultHtml.PL);
		//		sb.append("selected.splice( index, 1 );" + ResultHtml.PL);
		//		sb.append("}" + ResultHtml.PL);
		//		sb.append("$(this).toggleClass('selected');" + ResultHtml.PL);
		//		sb.append("} );" + ResultHtml.PL);
		// END SELECTED

		return compactJs(sb);
	}

	private String createScriptTableEditableField() {
		StringBuilder sb = new StringBuilder();
		sb.append("                          fields: [                                   " + ResultHtml.PL);

		String temp = "";
		for (ColumnTable b : columnList) {
			Class<?> type = b.getType();
			String label = b.getLabel();
			String col = b.getAttribute();
			String column = col.replace(".", "\\\\.").toLowerCase();
			Integer decimal = b.getDecimal();
			boolean hidden = b.getHidden();
			boolean editable = b.getEditable();

			if (editable == false) continue;
			String typeStr = "";
			if (type != null && type.equals(Date.class))
				typeStr = ", type: 'date', opts: {dateFormat: 'dd/mm/yyyy'}";


			sb.append(temp);
			sb.append("{label: '"+ column +"', name: '" + column + "'" + typeStr + "}                       ");
			temp = ",";
		}

		sb.append("                          ],                                           " + ResultHtml.PL);

		return sb.toString();
	}
	private String createScriptTimeLine() {
		StringBuilder sb = new StringBuilder();

		if (timeline == true) {
			StringBuilder columnPosition = new StringBuilder();
			String temp = "";
			for (int i = 0; i < columnList.size(); i++) {
				columnPosition.append(temp);
				columnPosition.append(i);
				temp = ",";
			}

			sb.append("     aoColumnDefs:[                                                                                          " + ResultHtml.PL);
			sb.append("         { bSearchable:false,bSortable:false,aTargets:[" + columnPosition.toString() + "] }                  " + ResultHtml.PL);
			sb.append("     ],                                                                                                      " + ResultHtml.PL);
		}

		return sb.toString();
	}
	private String createScriptTableColumn() throws DeveloperException {
		StringBuilder sb = new StringBuilder();

		sb.append("                      columns:[" + ResultHtml.PL);

		if (checkbok == true) {
			sb.append("                                                 {                                                                                             ");
			sb.append("                                                       data: null,                                                                             ");
			sb.append("                                             defaultContent: '<input type=\"checkbox\" class=\"ace\"><span class=\"lbl\"></span>',             ");
			sb.append("                                                  className: 'center',                                                                         ");
			sb.append("                                                  orderable: false,                                                                            ");
			sb.append("                                                 searchable: false                                                                             ");
			sb.append("                                                 },                                                                                            " + ResultHtml.PL);
		}

		String temp = "";
		for (ColumnTable b : columnList) {
			String label = b.getLabel();
			String col = b.getAttribute();
			String column = col.replace(".", "\\\\.").toLowerCase();
			Integer decimal = b.getDecimal();
			String render = b.getRender();
			boolean hidden = b.getHidden();
			boolean orderable = b.getOrderable();



			String className = "";
			if (hidden == true)                   className += classHidden;
			if (decimal != DECIMAL_PLACE_DEFAULT) className += " text-right";
			if (decimal == DECIMAL_PLACE_DEFAULT) decimal = 0;


			if ((column != null) && (render == null)) { 
//				if (column.toLowerCase().startsWith("icon"))         render = "tableFormatIcon(data,type,full,meta)";
//				else if (column.toLowerCase().startsWith("label"))   render = "tableFormatLabel(data,type,full,meta)";
//				else                                                 render = "tableFormatContent(data,type,full,meta,"+ decimal +")";

				
				if (column.toLowerCase().startsWith("icon"))         render = "function(data,type,full,meta){return tableFormatIcon(data,type,full,meta);}";
				else if (column.toLowerCase().startsWith("label"))   render = "function(data,type,full,meta){return tableFormatLabel(data,type,full,meta);}";
				else                                                 render = "function(data,type,full,meta){return tableFormatContent(data,type,full,meta,"+ decimal +");}";
			}



			sb.append( temp );
			sb.append("                                                 {                                                                                             ");
			sb.append("                                                       data: '"+ column +"',                                                                   ");
			if (orderable == false) sb.append("                          orderable: "+ orderable +",                                                                  ");
			if (StringUtils.hasValue(className)) sb.append("             className: '"+ className +"',                                                                ");
			if (StringUtils.hasValue(render)) sb.append("                   render: "+ render +"                                                                      ");
			sb.append("                                                 }                                                                                             " + ResultHtml.PL);

			temp = ",";
		}



		StringBuilder btnStr = new StringBuilder();
		btnStr.append("<div class=\"hidden-sm hidden-xs action-buttons\">");
		for (ButtonHtml btn : buttonInLineList) {
			btn.addCssClass("zoom");
			
			// TODO to mexendo aqui!!!
			// para fazer aparecer ou nao o botao na tela
			// ja sei que é com o render
			
			Object value = btn.getAttr("data-showwhentab");
			
			if (value != null) {
				btnStr.append("' +  ($(\"#screenViewTab\").find(\"li.active\").children(\"a\").attr(\"data-showwhentab\") == '"+value+"' ? '"+ btn.getComponente() + "' : '') + '");
			} else {
				btnStr.append(btn.getComponente());
			}
			
			
			
		}
		btnStr.append("</div>");

		// INI aqui é a listagem para quando a tela é pequena
		btnStr.append("<div class=\"hidden-md hidden-lg\">");
		btnStr.append("<div class=\"inline pos-rel\">");
		btnStr.append("<button class=\"btn btn-minier btn-primary dropdown-toggle\" data-toggle=\"dropdown\" data-position=\"auto\" aria-expanded=\"false\">");
		btnStr.append("<i class=\"ace-icon fa fa-caret-down icon-only bigger-120\"></i>");
		btnStr.append("</button>");
		btnStr.append("<ul class=\"dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close\">");
		for (ButtonHtml btn : buttonInLineList) {
			btnStr.append("<li>");
			btnStr.append(btn.getComponente());
			btnStr.append("</li>");
		}
		btnStr.append("</ul>");
		btnStr.append("</div>");
		btnStr.append("</div>");


		if (buttonInLineList.size() > 0) {
			sb.append("                                                 ,{                                                                                            ");
			sb.append("                                                       data: null,                                                                             ");
//			sb.append("                                             defaultContent: '"+ btnStr.toString() +"',                                                        ");
			sb.append("                                                     render: function(data,type,row){ return '"+ btnStr.toString() +"';},                      ");
			sb.append("                                                  orderable: false,                                                                            ");
			sb.append("                                                 searchable: false                                                                             ");
			sb.append("                                                 }                                                                                             " + ResultHtml.PL);
		}

		sb.append("                      ]," + ResultHtml.PL);


		return sb.toString();
	}
	private Object createScriptTableAjax() {
		StringBuilder sb = new StringBuilder();

		paramMap.put( FWConstants.PARAM_FW_CLASS, ClassUtils.getCodeTask(super.getDataClass()));
		paramMap.put( FWConstants.PARAM_FW_METHOD, super.getDataMethod());


		sb.append("                                          ajax:{                                                                                                                         ");
		sb.append("                                              headers: {\""+ FWConstants.PARAM_FW_TOKEN_AUTHORIZATION +"\": localStorage.getItem('"+ FWConstants.PARAM_FW_TOKEN_AUTHORIZATION +"')},");
		sb.append("                                              url:'" + FWConstants.URL_DO + "',                                                                                          ");
		sb.append("                                              type:'GET',                                                                                                               ");
		sb.append("                                              data:function(d){                                                                                                          ");

		// XXX posso colocar aqui
		// o ID principal e SLAVE
		// dessa forma elimina o _id...


		sb.append("                                                  return{                                                                                                                ");
		sb.append("                                                      table:d,                                                                                                           ");
		for (String key : paramMap.keySet()) sb.append(                  key + ":'" + paramMap.get(key)+"',                                                                                 ");
		sb.append(                                                       FWConstants.PARAM_FW_MASTER_ID + ":(" + super.getDataId() + "||_id),                                               ");
		sb.append(                                                       FWConstants.PARAM_FW_SLAVE_ID + ": (_idSlave)                                                                      ");

		// no caso da tabela secundaria (dentro da window)
		// nao esta indo o _slave... por isso nao achou os dados
		// pois o ID esta indo null do master
		// e nao esta passando o _salve
		//

		sb.append("                                                   };                                                                                                                    ");
		sb.append("                                               },                                                                                                                        " + ResultHtml.PL);

		if (super.getDataClass().getSuperclass().equals(AbstractView.class)) {
			sb.append("                                           dataSrc:function(json){                                                                                                   " + ResultHtml.PL);
			sb.append("                                               $('" + FWConstants.VIEW_TIME_PERFORMANCE + "').html(json.time);                                                      ");
			sb.append("                                               $('li.active > a > ." + FWConstants.VIEW_TAB_QTD + "').html(json.sizeStr);                                           ");
			sb.append("                                               return json.data;                                                                                                     ");
			sb.append("                                           }                                                                                                                         " + ResultHtml.PL);
		}
		sb.append("                                           },                                                                                                                            " + ResultHtml.PL);


		return sb.toString();
	}
	private String createScriptTableSummary() {
		StringBuilder sb = new StringBuilder();

		sb.append("                                          footerCallback:function(row,data,start,end,display){                                                                           " + ResultHtml.PL);
		sb.append("                                              var api=this.api(),data;                                                                                                   " + ResultHtml.PL);

		for (int i = 0; i < columnList.size(); i++) {
			int column = i;
			if (checkbok == true) column++;

			ColumnTable b = columnList.get(i);
			SummaryEnum s = b.getSummary();
			if (s == null) continue;

			sb.append("                                            var result = null;                                                                                                       " + ResultHtml.PL);
			if (s.equals(SummaryEnum.SUM)) {
				sb.append("                                        result='<div title=\"Soma\">' + api.column(" + column + ",{page:'current'}).data().reduce(function(a,b){                      ");
				sb.append("                                            return formatDecimal(parseDecimal(a)+parseDecimal(b));                                                               ");
				sb.append("                                        },0);                                                                                                                    " + ResultHtml.PL);
			} else if (s.equals(SummaryEnum.AVG)) {
				sb.append("                                        result='<div title=\"Média\">' + api.column(" + column + ",{page:'current'}).data().reduce(function(a,b){                     ");
				sb.append("                                            return formatDecimal(parseDecimal(a)+parseDecimal(b) / data.length);                                                 ");
				sb.append("                                        },0);                                                                                                                    " + ResultHtml.PL);
			} else if (s.equals(SummaryEnum.COUNT)) {
				sb.append("                                        result='<div title=\"Contagem\">' + data.length;                                                                         " + ResultHtml.PL);
			}
			sb.append("                                            $(api.column(" + column + ").footer()).html(''+result+'</div>');                                                              " + ResultHtml.PL);
		}
		sb.append("                                          },                                                                                                                             " + ResultHtml.PL);

		return sb.toString();
	}

	private String compactJs(StringBuilder sb) {
		String result = sb.toString();
		while (result.contains("  "))
			result = result.replaceAll("  ", " ");

		return result;
	}


	public String createScriptClean() {
		return "$P{}";
	}

	public String createScriptPopulateByJson() {
		return createScriptPopulateByJson(getId());
	}
	public static String createScriptPopulateByJson(String id) {
		return createScriptPopulateByJson(id, null);
	}
	public static String createScriptPopulateByJson(String id, String callback) {
		StringBuilder sb = new StringBuilder();
		
		String part = "";
		if (StringUtils.hasValue(callback)) part = callback;
		
		sb.append("oTable_" + id + ".ajax.reload("+ part +");" + ResultHtml.PL);
		return sb.toString();
	}

	public String createScriptDataRow() {
		return createScriptDataRow(super.getDataClass().getSuperclass().equals(AbstractView.class), getId(), false);
	}
	public String createScriptDataRow(Integer selecteds) {
		//		String script = createScriptDataRow(super.getDataClass().getSuperclass().equals(AbstractView.class), getId(), multi);
		boolean multi = false;
		if(selecteds == Menu.MANY)
			multi = true;

		String script = "";
		if(selecteds != Menu.ZERO) {
			// VER O QUE ACONTECEU COM O "PROJETOS" (CREDIP)
			script = createScriptDataRow(super.getDataClass().getSuperclass().equals(AbstractView.class), getId(), multi);
		}

		if (multi) {
			script += createScriptSelectedLine(selecteds);
		}
		return script;
	}
	public static String createScriptDataRow(boolean mastarTable, String id, boolean multi) {
		if ((mastarTable == true) && (StringUtils.hasValue(id) == false)) id = AbstractView.VIEW_TABLE_ID;

		StringBuilder sb = new StringBuilder();
		sb.append("var result = " + createVarResultFromTable(id, multi) + ";");

		if (mastarTable == true) {
			sb.append("_id = result;" + ResultHtml.PL);
			sb.append("_idSlave = null;" + ResultHtml.PL);


			//VEEERRRR COM KEYNES POIS PAROU OS BOTOES DE DOWNLOAD DA CREDIPNET 
			//			sb.append("_idSlave = result;" + ResultHtml.PL);
		} else {
			sb.append("_idSlave = result;" + ResultHtml.PL);
		}
		return sb.toString();
	}

	public static String createVarResultFromTable(String id, boolean multi) {
		StringBuilder sb = new StringBuilder();
		if (multi == true) {
			// esse codigo é para funcionar muitos registros selecionados ao mesmo tempo
			sb.append("oTable_" + id + ".rows('.selected').ids().join(',')" + ResultHtml.PL);
		} else {
			// este codigo é para apenas a linha que estou clicando o botao
			sb.append("oTable_" + id + ".row($(this).parents('tr')).data().id" + ResultHtml.PL);
		}

		return sb.toString();
	}

	private String createScriptSelectedLine(Integer selecteds) {
		StringBuilder sb = new StringBuilder();

		String mensagem2 = MessageHtml.createScript(null, FWConstants.JSON_ATTR_TYPE_WARNING, "Selecione pelo menos uma linha da tabela");
		sb.append("if (oTable_" + getId() + ".rows('.selected').data().length == 0) {");
		sb.append("if ("+selecteds+" > 1) {");
		sb.append(mensagem2 + ResultHtml.PL);
		sb.append("}");
		sb.append("return false;");
		sb.append("}");

		return sb.toString();
	}

	public List<ButtonHtml> getAllButtonList() {
		return buttonInLineList;
	}


	public ButtonHtml addButton(ButtonHtml button) {
		buttonInLineList.add(button);

		return button;
	}

	public ButtonTable addButton(Class action, String doProccess) throws DeveloperException {
		setDataClass(action);

		ButtonTable btn = new ButtonTable(action, doProccess);
		buttonList.add(btn);
		return btn;
	}

	// GENERICO
	private ButtonHtml addButton(Class action, String method, Boolean download, CssIconEnum cssIcon, CssColorEnum cssColor, String label, String scriptBtn, ComponentHtml[] compWindowArray, ComponentHtml[] compDataArray, Integer selecteds) throws DeveloperException {
		setDataClass(action);

		if (cssColor == null) cssColor = CssColorEnum.GREEN;
		
		String iconDescricao = cssIcon.getDescricao(); 
		iconDescricao = iconDescricao.replaceAll(" white", "");
		if (selecteds != Menu.ONE) {
			iconDescricao += " white";
			cssIcon.setDescricao(iconDescricao);
		}
		
		
//		if(selecteds == Menu.ONE) cssIcon.setDescricao(iconDescricao+" white");
		
		ButtonHtml button = new ButtonHtml(cssIcon, cssColor, null, null);
		if(selecteds == Menu.ONE) button.addCssClass("btn-link ");
		button.setHtml(getHtml());

		
		if (StringUtils.hasValue((String)button.getAttr("title")) == false) button.addAttr("title", label);
		if (button.getCssClassComponent().toString().contains("btn-link") == false) {
			button.setLabel(label);
		}


		StringBuilder script = new StringBuilder();
		script.append(createScriptDataRow(selecteds));

		if (compWindowArray!= null) {
			WindowHtml window = new WindowHtml(label, cssIcon);
			window.setSize(8);

			for (ComponentHtml comp : compWindowArray) {
				AbstractCommon.setRota(getHtml(), action, comp);
				window.addContent(comp);

				//
				// ESTE IF AQUI DE BAIXO É MUITO IMPORTANTE
				// NUNCA TIRE ELE...
				// SERVE NA HORA DE ABRIR UMA WINDOW DENTRO DE WINDOW!!!!
				//


				// vamos fazer um bem bolado aqui
				// recursividade para encontrar o form e aplicar o ID
				recursivo(comp, script);
			}
			script.append(  DataComponentHtml.createScriptPopulateByJsonOpenWindowIfAllOk(this, compWindowArray, window, getId()) );
			winList.add(window);
		} else {


			String mensagem = MessageHtml.createScript(null, "'+data."+ FWConstants.JSON_ATTR_TYPE +"+'", "'+data.message+'");

			StringBuilder success = new StringBuilder();
			if (download == true) {
				success.append(ScriptHelper.createScriptDownladAjax());
			} else {
				// aqui vai atualizar os dados da table secundaria e primaria
				success.append(createScriptPopulateByJson());
				if (action.getSuperclass().equals(AbstractAction.class)) {
					// OLD
					// AbstractAction action2 = (AbstractAction) action;
					// success.append(action2.getView().getTable().createScriptPopulateByJson());
					success.append(TableHtml.createScriptPopulateByJson(AbstractView.VIEW_TABLE_ID));
				}

				if ((compDataArray != null) && (compDataArray.length > 0)) {

					for (ComponentHtml comp2 : compDataArray) {
						if (comp2 instanceof DataComponentHtml) {
							DataComponentHtml dc = (DataComponentHtml)comp2;
							dc.setDataClass(action);
							success.append(dc.createScriptPopulateByJson());
						}
					}
				}

				if (StringUtils.hasValue(scriptBtn) == true) {
					success.append(scriptBtn);
				}

				success.append(mensagem);
			}

			Ajax ajax = new Ajax(action, method);
			ajax.addParamJs(FWConstants.PARAM_FW_MASTER_ID, "(" + getDataId() + " || _id)");
			ajax.addParamJs(FWConstants.PARAM_FW_SLAVE_ID, "(_idSlave)");

			for (String key : paramMap.keySet()) {
				ajax.addParam(key, paramMap.get(key));
			}

			ajax.setSuccessOrWarningMessage(success.toString());

			script.append(ajax.getComponent());

		}


		button.onClick(script.toString());
		if(selecteds != Menu.ONE) 
			buttonTopList.add(button);

		if(selecteds == Menu.ONE) 
			buttonInLineList.add(button);

		return button;
	}

	private ButtonHtml addButton(ButtonTable btn) throws DeveloperException {
		setDataClass(btn.getAction());

		ButtonHtml button = new ButtonHtml(btn.getIcon(), btn.getColor(), null, null);
		button.setHtml(getHtml());

		String iconDescricao = btn.getIcon().getDescricao(); 
		iconDescricao = iconDescricao.replaceAll(" white", "");
		if (btn.getSelecteds() != Menu.ONE) {
			iconDescricao += " white";
			btn.getIcon().setDescricao(iconDescricao);
		}
		
		if (btn.getColor() == null) btn.color(CssColorEnum.GREEN);
		if (StringUtils.hasValue((String)button.getAttr("title")) == false) button.addAttr("title", btn.getLabel());
		if(btn.getSelecteds() == Menu.ONE) button.addCssClass("btn-link ");
		if (button.getCssClassComponent().toString().contains("btn-link") == false) {
			button.setLabel(btn.getLabel());
		}

		StringBuilder script = new StringBuilder();
		script.append(createScriptDataRow(btn.getSelecteds()));

		if (btn.getCompWindowArray() != null) {
			WindowHtml window = new WindowHtml(btn.getLabel(), btn.getIcon());
			window.setSize(8);

			for (ComponentHtml comp : btn.getCompWindowArray()) {
				AbstractCommon.setRota(getHtml(), btn.getAction(), comp);
				window.addContent(comp);

				//
				// ESTE IF AQUI DE BAIXO É MUITO IMPORTANTE
				// NUNCA TIRE ELE...
				// SERVE NA HORA DE ABRIR UMA WINDOW DENTRO DE WINDOW!!!!
				//


				// vamos fazer um bem bolado aqui
				// recursividade para encontrar o form e aplicar o ID
				recursivo(comp, script);
			}
			script.append(  DataComponentHtml.createScriptPopulateByJsonOpenWindowIfAllOk(this, btn.getCompWindowArray(), window, getId()) );
			winList.add(window);
		} else {


			String mensagem = MessageHtml.createScript(null, "'+data."+ FWConstants.JSON_ATTR_TYPE +"+'", "'+data.message+'");

			StringBuilder success = new StringBuilder();
			if (btn.getDownload() == true) {
				success.append(ScriptHelper.createScriptDownladAjax());
			} else {
				// aqui vai atualizar os dados da table secundaria e primaria
				success.append(createScriptPopulateByJson());
				if (btn.getAction().getSuperclass().equals(AbstractAction.class)) {
					// OLD
					// AbstractAction action2 = (AbstractAction) action;
					// success.append(action2.getView().getTable().createScriptPopulateByJson());
					success.append(TableHtml.createScriptPopulateByJson(AbstractView.VIEW_TABLE_ID));
				}

				if ((btn.getCompDataArray() != null) && (btn.getCompDataArray().length > 0)) {

					for (ComponentHtml comp2 : btn.getCompDataArray()) {
						if (comp2 instanceof DataComponentHtml) {
							DataComponentHtml dc = (DataComponentHtml)comp2;
							dc.setDataClass(btn.getAction());
							success.append(dc.createScriptPopulateByJson());
						}
					}
				}

				if (StringUtils.hasValue(btn.getScript()) == true) {
					success.append(btn.getScript());
				}

				success.append(mensagem);
			}

			Ajax ajax = new Ajax(btn.getAction(), btn.getMethod());
			ajax.addParamJs(FWConstants.PARAM_FW_MASTER_ID, "(" + getDataId() + " || _id)");
			ajax.addParamJs(FWConstants.PARAM_FW_SLAVE_ID, "(_idSlave)");

			for (String key : paramMap.keySet()) {
				ajax.addParam(key, paramMap.get(key));
			}

			ajax.setSuccessOrWarningMessage(success.toString());

			script.append(ajax.getComponent());

		}


		script.append(btn.getScriptEvent());
		button.onClick(script.toString());
		if (btn.getSelecteds() != Menu.ONE) 
			buttonTopList.add(button);

		if (btn.getSelecteds() == Menu.ONE) 
			buttonInLineList.add(button);

		return button;
	}



	private StringBuilder recursivo(ComponentHtml comp, StringBuilder script) {
		if (comp instanceof DataComponentHtml) {
			if (comp instanceof FormHtml) {
				FormHtml form = (FormHtml) comp;
				script.append("$('#" + form.getId() + " [name=\"" + FWConstants.PARAM_FW_MASTER_ID + "\"]').val((" + getDataId() + " || _id));" + ResultHtml.PL);
				script.append("$('#" + form.getId() + " [name=\"" + FWConstants.PARAM_FW_SLAVE_ID + "\"]').val(_idSlave);" + ResultHtml.PL);
			}
		} else if (comp instanceof ContainerComponentHtml) {
			ContainerComponentHtml c2 = (ContainerComponentHtml)comp;
			List<ComponentHtml> c2List = c2.getComponentList();
			for (ComponentHtml c3: c2List) {
				return recursivo(c3, script);
			}
		}

		return script;
	}

	public static List<Bean> formatTimeline(List<? extends Bean> beanList, String... param) throws DeveloperException {
		return formatTimeline(null, null, beanList, param);
	}
	public static List<Bean> formatTimeline(Class<?> action, String downloadMethodName, List<? extends Bean> beanList, String... param) throws DeveloperException {
		String nome = param[0];
		String observacao = param[1];
		String data = param[2];
		String hora = param[3];
		String tipo = (param.length >= 5) ? param[4] : null;
		String anexoId = (param.length >= 6) ? param[5] : null;
		String anexoName = (param.length >= 7) ? param[6] : null;
		String idStr = (param.length >= 8) ? param[7] : null;

		List<Bean> list = new ArrayList<Bean>();
		for (Bean bean : beanList) {
			StringBuilder sb = new StringBuilder();
			sb.append("<div class=\"profile-activity clearfix\">");
			sb.append("<span class=\"badge badge-success\">" + bean.get(tipo) + "</span>");
			sb.append("<a class=\"user\" href=\"#\"> " + bean.get(nome) + " </a>");
			sb.append("" + StringUtils.parseStrToHtml((String) bean.get(observacao)));
			sb.append(" <div class=\"time\">");
			sb.append("<i class=\"ace-icon fa fa-clock-o bigger-110\"></i>");

			sb.append(" " + DateUtils.formatDate((Date) bean.get(data)) + " " + bean.get(hora));
			sb.append("</div>");
			sb.append("</div>");

			Integer aneId = bean.get(anexoId);
			Integer id = bean.get(idStr);

			Bean b = new Bean();
			b.set("timeline", sb.toString());
			if(id == null) {
				id = aneId;
			}
			b.set("id", id);

			if (aneId != null) {
//				UrlHtml url = new UrlHtml();
//				url.setAction(action.getClass());
//				url.setLabel((String) bean.get(anexoName));
//				url.setLink(FWConstants.URL_DO);
//
//				url.addParam(FWConstants.PARAM_FW_CLASS, ClassUtils.getCodeTask(action.getClass()));
//				url.addParam(FWConstants.PARAM_FW_CLASS, action.getClass().getName());
//				url.setMethod(downloadMethodName);
//				url.addParam("aneId", aneId);

				b.set("download", (String) bean.get(anexoName));
			} else {
				b.set("download", "");
			}
			list.add(b);
		}

		return list;
	}
	public void setTimeline(boolean timeline) {
		this.timeline = timeline;
	}

	public static void formatLabelSituacao(List<Bean> beanList) {
		for (Bean b : beanList) {
			Integer sitId = b.getAttribute("Situacao.Id");
			if (sitId.equals(1)) {
				b.setAttribute("labelSituacaoStr", "warning" + SEPARATOR + b.getAttribute("Situacao.Descricao"));
			} else if (sitId.equals(2)) {
				b.setAttribute("labelSituacaoStr", "danger" + SEPARATOR + b.getAttribute("Situacao.Descricao"));
			} else if (sitId.equals(4)) {
				b.setAttribute("labelSituacaoStr", "success" + SEPARATOR + b.getAttribute("Situacao.Descricao"));
			} else if (sitId.equals(5)) {
				b.setAttribute("labelSituacaoStr", "success" + SEPARATOR + b.getAttribute("Situacao.Descricao"));
			}
		}
	}

	public void setParent(WindowHtml win) {
		this.parenteWin = win;
	}

	public void addParam(String key, Object value) {
		paramMap.put(key, value);
	}

	public boolean isServerSide() {
		return serverSide;
	}
	public void setServerSide(boolean serverSide) {
		this.serverSide = serverSide;
	}
	public boolean isCheckbok() {
		return checkbok;
	}
	public void setCheckbok(boolean checkbok) {
		this.checkbok = checkbok;
	}

	public boolean isOnLoad() {
		return onLoad;
	}

	public void setOnLoad(boolean onLoad) {
		this.onLoad = onLoad;
	}
	
	
}

