package br.com.dotum.core.html.other;

import java.util.List;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.core.html.form.ButtonHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public class ToolbarHtml extends ContainerComponentHtml {

	public void add(ButtonHtml btn) throws DeveloperException {
		add(btn.getLabel(), btn);
	}	

	public String getComponente() throws Exception {
		createScript();

		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"col-xs-12\">" + ResultHtml.PL);
		sb.append("  <div id=\""+ getId() +"\" class=\"btn-group pull-left no-padding\">" + ResultHtml.PL);
		sb.append("   <ul class=\"nav navbar-nav\">");
		for (Object aba : getMap().keySet()) {
			List<ComponentHtml> cList = getMap().get(aba);
			for (ComponentHtml comp : cList) {
				sb.append( "<li>");
				sb.append( comp.getComponente() );
				sb.append( "</li>");
			}

		}
		sb.append(" </ul>" + ResultHtml.PL);
		sb.append(" </div>" + ResultHtml.PL);
		sb.append("</div>" + ResultHtml.PL);
		sb.append("<div class=\"clearfix\"></div>" + ResultHtml.PL);
		return sb.toString();
	}

	public void createScript() throws DeveloperException {
		ComponentHtml.prepare2(getHtml(), getComponentList());
	}

	public String getScript() throws DeveloperException {
		return "";
	}
}

