package br.com.dotum.core.html.other;

import java.net.URLEncoder;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.security.SecurityUtil;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;

public class UrlHtml {

	
	private Class<?> clazz;
	private Map<String, String> param = new LinkedHashMap<String, String>();
	private String link;
	private String target;
	private String bookmark;
	private String label;
	private boolean token = false;

	public String getAction() {
		if (link != null) {
			return link;
		} else {
			return FWConstants.URL_DO;
		}
	}
	public void setAction(Class<?> action) {
		if (action == null) return;
		this.clazz = action;
		addParam(FWConstants.PARAM_FW_CLASS, ClassUtils.getCodeTask(clazz));
	}

	public Map<String, String> getParam() {
		return param;
	}

	public void addParam(String key, String value) {
		param.put(key, value);
	}

	public void addParam(String key, Object value) {
		String result = null;

		if (value instanceof Integer) {
			result = (Integer)value + "";
		} else if (value instanceof Double) {
			result = FormatUtils.formatDouble((Double)value);
		} else if (value instanceof Date) {
			result = FormatUtils.formatDate((Date)value);
		} else if (value instanceof String) {
			result = (String)value;
		}

		param.put(key, result);
	}


	public String getFullLink() throws DeveloperException {

		String targetTemp = StringUtils.hasValue(getTarget()) ? " target=\""+ getTarget() +"\" " : "";
		return "<a href=\"" + getComponente() + "\""+ targetTemp +">" + this.label + "</a>";
	}

	public String getComponente() throws DeveloperException {
		String temp = "";
		StringBuilder sb = new StringBuilder();
		sb.append( getAction() );

		if (param.size() > 0) {
			sb.append( "?" );
		}
		for (String key : param.keySet()) {
			sb.append(temp);
			sb.append(key);
			sb.append("=");
			sb.append(param.get(key));
			temp = "&";
		}
		if (StringUtils.hasValue(bookmark)) 
			sb.append("#" + bookmark);
		
			
		if (token == true) {
			try {
				String urlFull = sb.toString();
				String[] partArray = urlFull.split("\\?");
				
				return partArray[0] + "?"+ FWConstants.URL_TOKEN +"=" + URLEncoder.encode(SecurityUtil.encrypt(SecurityUtil.MOD2, partArray[1]), "UTF-8");
			} catch (Exception e) {
				throw new DeveloperException(e.getMessage());
			}
        } else {
        	return sb.toString();
        }
	}

	public void setMethod(String method) {
		addParam(FWConstants.PARAM_FW_METHOD, method);
	}
	public void setLink(String link) {
		String[] url = link.split("\\?");
		this.link = url[0];

		if (url.length > 1) {
			String paramsStr = url[1];
			String[] paramArray = paramsStr.split("&");
			for (String str : paramArray) {
				String[] x = str.split("=");
				addParam(x[0], x[1]);
			}
		}
	}

	public void setLabel(String label) {
		this.label = label;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getBookmark() {
		return bookmark;
	}
	public void setBookmark(String bookmark) {
		this.bookmark = bookmark;
	}
	public boolean isToken() {
		return token;
	}
	public void setToken(boolean token) {
		this.token = token;
	}
}

