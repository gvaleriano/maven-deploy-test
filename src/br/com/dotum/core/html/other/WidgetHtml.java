package br.com.dotum.core.html.other;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public class WidgetHtml extends ContainerComponentHtml {

	
	public WidgetHtml(String title, CssIconEnum icon) {
		super.setLabel( title );
		super.setIcon(icon);
	}

	@Override
	public String getComponente() throws Exception {
		StringBuilder sb = new StringBuilder();

		sb.append("<div class=\"widget-box\">");
		sb.append("<div class=\"widget-header widget-header-small\">");
		sb.append("<h4 class=\"widget-title lighter smaller orange\">");
		sb.append("<i class=\""+ getIcon().getDescricao() +"\"></i>");
		sb.append( getLabel() );
		sb.append("</h4>");
		
		
		// aqui vamos montar o toolbar
//		sb.append("   <div class=\"widget-toolbar\">            ");
//		sb.append("   	<div class=\"widget-menu\">            ");
//		sb.append("   		<a href=\"#\" data-action=\"settings\" data-toggle=\"dropdown\" aria-expanded=\"false\">            ");
//		sb.append("   			<i class=\"ace-icon fa fa-bars\"></i>            ");
//		sb.append("   		</a>            ");
//		sb.append("   		<ul class=\"dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer\">            ");
//		sb.append("   			<li>            ");
//		sb.append("   				<a data-toggle=\"tab\" href=\"#dropdown1\">Option#1</a>            ");
//		sb.append("   			</li>            ");
//		sb.append("               ");
//		sb.append("   			<li>            ");
//		sb.append("   				<a data-toggle=\"tab\" href=\"#dropdown2\">Option#2</a>            ");
//		sb.append("   			</li>            ");
//		sb.append("   		</ul>            ");
//		sb.append("   	</div>            ");

//		sb.append("   	<a href=\"#\" data-action=\"fullscreen\" class=\"orange2\">            ");
//		sb.append("   		<i class=\"ace-icon fa fa-expand\"></i>            ");
//		sb.append("   	</a>");
		
//		sb.append("   	<a href=\"#\" data-action=\"reload\">            ");
//		sb.append("   		<i class=\"ace-icon fa fa-refresh\"></i>            ");
//		sb.append("   	</a>            ");

//		sb.append("   	<a href=\"#\" data-action=\"collapse\">            ");
//		sb.append("   		<i class=\"ace-icon fa fa-chevron-up\"></i>            ");
//		sb.append("   	</a>            ");

//		sb.append("   	<a href=\"#\" data-action=\"close\">            ");
//		sb.append("   		<i class=\"ace-icon fa fa-times\"></i>            ");
//		sb.append("   	</a>            ");
//		sb.append("   </div>            ");
		
		

		sb.append("</div>");
		sb.append("<div class=\"widget-body\">");
		sb.append("<div class=\"widget-main\">");
		for (ComponentHtml compHtml : super.getComponentList()) {
			sb.append(compHtml.getComponente());
		}
		sb.append("</div>" + ResultHtml.PL);
		sb.append("</div>" + ResultHtml.PL);
		sb.append("</div>" + ResultHtml.PL);

		return sb.toString();
	}
	
	
	public void add(ComponentHtml comp) {
		super.add("wid", comp);
	}

	@Override
	public String getScript() throws DeveloperException {
		return null;
	}

	@Override
	public void createScript() throws DeveloperException {
		getHtml().addScriptLink("./assets/js/src/ace.widget-box.js", null);
	}

}

