package br.com.dotum.core.html.other;

import java.util.List;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.core.enuns.CssIconEnum;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.view.ViewHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.StringUtils;

public class WindowHtml extends ContainerComponentHtml {
	private boolean window = true;
	private StringBuilder scriptOnClose = new StringBuilder();
	

	public WindowHtml(String title, CssIconEnum cssIcon) {
		super.setLabel( title );
		super.setIcon(cssIcon);
	}

	public void addContent( ComponentHtml comp ) {
		addContent(comp, "win");
	}
	public void addContent( ComponentHtml comp, String name) {
		comp.setHtml(getHtml());
		if (comp instanceof FormHtml) {
			FormHtml form = (FormHtml) comp;
			form.setParent(this);
		} else if (comp instanceof TableHtml) {
			TableHtml table = (TableHtml) comp;
			table.setParent(this);
		} else if (comp instanceof ViewHtml) {
			ViewHtml view = (ViewHtml) comp;
			view.setParent(this);
		} else if (comp instanceof ContainerComponentHtml) {
			ContainerComponentHtml column = (ContainerComponentHtml)comp;
			List<ComponentHtml> cList = column.getComponentList();

			for (ComponentHtml c : cList) {
				addContent(c, null);
			}
		}

		if (name != null) super.add(name, comp);
	}

	public String getComponente() throws Exception {
		StringBuilder sb = new StringBuilder();

		if (window == true) {

			if (getSizeInt() == null) {
				super.setSize(10);
			}

			int resto = 12 - super.getSizeInt();
			int center = resto/2;

			sb.append("<div id=\""+super.getId() +"\" class=\"modal row fade "+ getId() +"\" tabindex=\"-1\" role=\"dialog\">" + ResultHtml.PL);
			sb.append("<div class=\"col-sm-"+ center +"\"></div>" + ResultHtml.PL);
			sb.append("<div class=\"widget-container-col"+ super.getSize() +"\">" + ResultHtml.PL);
			sb.append("<div class=\"space-32\"></div>" + ResultHtml.PL);

			if (getSizeInt() < 10) 	sb.append("<div class=\"space-32\"></div>" + ResultHtml.PL);

			sb.append("<div class=\"widget-box widget-color-blue no-border sombra\">" + ResultHtml.PL);
			if (StringUtils.hasValue(getLabel())) {
				sb.append("<div class=\"widget-header\">" + ResultHtml.PL);
				sb.append("<h5 class=\"widget-title lighter\">" + ResultHtml.PL);
				sb.append("<i class='"+super.getIcon()+"'></i>" + ResultHtml.PL);
				sb.append( getLabel() + ResultHtml.PL);
				sb.append("</h5>" + ResultHtml.PL);
				sb.append("<div class=\"widget-toolbar\">" + ResultHtml.PL);

				sb.append("<a href=\"#"+super.getId()+"\"  class=\"close-"+getId()+"\" data-dismiss=\"modal-"+getId()+"\" aria-hidden=\"true\">" + ResultHtml.PL);

				sb.append("<i class=\"ace-icon fa fa-times white\"></i>" + ResultHtml.PL);
				sb.append("</a>" + ResultHtml.PL);
				sb.append("</div>" + ResultHtml.PL);
				sb.append("</div>" + ResultHtml.PL);
			}
			sb.append("<div class=\"widget-body\">" + ResultHtml.PL);
			sb.append("<div class=\"widget-main padding-16\">" + ResultHtml.PL);
		}
		for (ComponentHtml comp : getComponentList() ) {
			sb.append( comp.getComponente() );
		}
		if (window == true) {
			sb.append("</div>" + ResultHtml.PL);
			sb.append("</div>" + ResultHtml.PL);
			sb.append("</div>" + ResultHtml.PL);
			sb.append("</div>" + ResultHtml.PL);
			sb.append("</div>" + ResultHtml.PL);
		}
		return sb.toString();
	}

	public String getScript() {
		StringBuilder sb = new StringBuilder();
		//só deve adicionar a pagina quando for uma windowns interna
//		if (getId().contains("W")) {
//			sb.append("$('."+getId()+"').on('hidden.bs.modal', function (e) {");
//			sb.append(" $('body').addClass('modal-open'); ");
//			sb.append("}); ");
//		} 
		
		
		sb.append("$('.modal').on('hidden.bs.modal', function (e) {");
		sb.append("if($('.modal').hasClass('in')) {");
		sb.append("$('body').addClass('modal-open');");
		sb.append("}");
		sb.append("});");
		
		sb.append("$('.close-"+getId()+"').click(function() { ");
		sb.append("    $('#"+getId()+"').modal('hide');");
		sb.append("  }); ");



		return sb.toString();
	}

	public String createScriptAbreWindow() {
		return createScriptAbreWindow(super.getId()) + getScript();
	}
	public static String createScriptAbreWindow(String id) {
		StringBuilder sb = new StringBuilder();
		sb.append("$('#"+ id +"').modal({show:true});");
		return sb.toString();
	}

	public String createScriptCloseWindow() {
		return "$('#"+ getId() +"').modal('hide');";
	}

	@Override
	public void createScript() throws DeveloperException {
		ComponentHtml.prepare2(getHtml(), getComponentList());
	}
	
	public String xxx() {
		StringBuilder sb = new StringBuilder();
		
		String script = scriptOnClose.toString();
		if (StringUtils.hasValue(script)) {
			//Favor mante isso padrão, pois na credip, usamos fechar tela conforma a condicional do formulario
			sb.append("$('#"+ getId() +"').on('hide.bs.modal hidden.bs.modal', function(e) {");
			sb.append( script );
			sb.append("});");
		}
		return sb.toString();
	}

	public void setWindow(boolean window) {
		this.window = window;
	}
	
	public boolean isWindow() {
		return this.window;
	}
	
	public void setScriptOnClose(String script) {
		this.scriptOnClose.append( script );
	}
}

