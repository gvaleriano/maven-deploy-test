package br.com.dotum.core.html.view;

import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;

public class FieldViewHtml extends ComponentHtml{
    
	private String label;
    private String attribute;
    private String cssAttribute;
    private boolean remove = false;
    
    
    public FieldViewHtml(String label, String attribute) {
        this.label = label;
        this.attribute = attribute.toLowerCase();
    }

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getComponente() throws Exception {
		return null;
	}

	public String getScript() throws DeveloperException {
		return null;
	}

	public void createScript() throws DeveloperException {
	}
	
	public void removeCssPadraoLabel(boolean remove) {
		this.remove = remove;
	}
	public boolean isRemoveCssPadrao() {
		return this.remove;
	}
	
	public void addCssAttribute(String cssAttribute) {
		this.cssAttribute = cssAttribute;
	}
	
	public String getCssAttribute() {
		return this.cssAttribute;
	}
}
