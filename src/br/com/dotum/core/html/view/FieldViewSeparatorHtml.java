package br.com.dotum.core.html.view;

public class FieldViewSeparatorHtml extends FieldViewHtml {
    
    public FieldViewSeparatorHtml() {
        super(HtmlHelper.CHAR_SPACE, HtmlHelper.CHAR_SPACE);
    }

}
