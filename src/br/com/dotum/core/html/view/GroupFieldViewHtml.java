package br.com.dotum.core.html.view;

import java.util.ArrayList;
import java.util.List;

public class GroupFieldViewHtml {
    
	private String label;
    private List<FieldViewHtml> fields = new ArrayList<FieldViewHtml>();    
    
    public GroupFieldViewHtml() {
    	
    }
    public GroupFieldViewHtml(String label) {
    	this.label = label;
    }
    
    public String getLabel() {
    	return this.label;
    }
    
    public List<FieldViewHtml> getFields() {
        return fields;
    }

    public void addField(FieldViewHtml value) {
        this.fields.add(value);
    }
    public void addSeparator() {
        this.fields.add(new FieldViewSeparatorHtml());
    }
}