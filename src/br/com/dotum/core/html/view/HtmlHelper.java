package br.com.dotum.core.html.view;

public class HtmlHelper {
	public static final String VAZIO = "";
	public static final String ESPACO = " ";
	public static final String IGUAL = "=";
	public static final String E = "&";
	public static final String CHAR_SPACE = "&nbsp;"; 

	public static final String SEPARADOR_FIELDS = ":";
	public static final String SEPARADOR_LOCALIZATION = " <i class=\"ace-icon fa fa-angle-double-right\"></i> ";

}