package br.com.dotum.core.html.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.component.DataComponentHtml;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.core.html.other.WindowHtml;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;

public class ViewHtml extends DataComponentHtml {
	private static Log LOG = LogFactory.getLog(ViewHtml.class);


	private static final String SEPARATOR = "#"; 
	private Map<String, List<FieldViewHtml>> map = new LinkedHashMap<String, List<FieldViewHtml>>();
	private WindowHtml win;

	/**
	 * Usar o construtor com parametro
	 */
	@Deprecated
	public ViewHtml() {
	}

	public ViewHtml(String dataMethod) throws DeveloperException {
		if (dataMethod.equals("doData")) {
			LOG.error("Não pode usar o nome \"doData\" para o ViewHtml.");
			//			throw new DeveloperException("Não pode usar o nome \"doData\" para o ViewHtml.");
		}

		super.setDataMethod( dataMethod );
	}

	public ViewHtml(String method, Integer dataId) {
		super.setDataMethod( method );
		super.setDataId(dataId);
	}

	public void addGroup(String group, FieldViewHtml ... field) {
		addGroup(group, 2, field);
	}
	public void addGroup(String group, Integer column, FieldViewHtml ... field) {
		String key = group + SEPARATOR + column;

		List<FieldViewHtml> fieldList = map.get(key);
		if (fieldList == null) 
			fieldList = new ArrayList<FieldViewHtml>();

		for (int i = 0; i < field.length; i++) {

			fieldList.add(field[i]);
		}
		map.put(key, fieldList);
	}	
	public static String format(Object value) throws DeveloperException {
		String result = "";
		if (value instanceof Object) result = value.toString();
		if (value instanceof Date) result = FormatUtils.formatDate((Date)value);
		if (value instanceof Integer) result = Integer.toString((Integer)value);
		if (value instanceof Double) result = FormatUtils.formatDouble((Double)value);
		if (value instanceof UrlHtml) result = "<a href=\""+ ((UrlHtml)value).getComponente() +"\" targer=\"_blank\">Download</a>";

		return result;
	}

	public String getComponente() throws DeveloperException {
		StringBuilder component = new StringBuilder();

		component.append("<div class=\"space-4\"></div>" + ResultHtml.PL);
		component.append("<div class=\"clearfix\"></div>" + ResultHtml.PL);
		component.append("<div id=\""+ getId() +"\">" + ResultHtml.PL);

		for (String key : map.keySet()) {
			List<FieldViewHtml> fieldArray = map.get(key);

			String[] parts = key.split(SEPARATOR);
			String group = parts[0];
			Integer column = Integer.parseInt(parts[1]);

			if (StringUtils.hasValue(group) && group.equals("null") == false) {
				component.append( "<div class=\"page-header no-padding\">" + ResultHtml.PL);
				component.append(  "<h3 class=\"widget-title blue\">"+ group +"</h3>" + ResultHtml.PL);
				component.append( "</div>" + ResultHtml.PL);
			} else {
				component.append("<div class=\"space-4\"></div>" + ResultHtml.PL);
			}

			Integer size = 12 / column;
			Integer col = 8;
			if(column>1) col = 6;
			component.append( "<div class=\"profile-user-info profile-user-info-striped no-margin\" style=\"width: 100%;\">" + ResultHtml.PL);
			for (FieldViewHtml field : fieldArray) {

				String classs = "profile-info-name"; 
				if(field.isRemoveCssPadrao() == true) {
					classs = "";
				}

				component.append(  "<div class=\"col-sm-"+ size +" profile-info-row no-padding \">" + ResultHtml.PL);
				if(field.getLabel() != null) {
					component.append(   "<div class=\"col-sm-4 "+classs+"\">" + ResultHtml.PL);
					component.append(      field.getLabel() + ResultHtml.PL);
					component.append(   "</div>" + ResultHtml.PL);
				}
				component.append(   "<div class=\"col-sm-"+col+" profile-info-value "+field.getCssAttribute()+"\">" + ResultHtml.PL);
				component.append(    "<span class=\"view-field\" name=\""+ field.getAttribute() +"\">&nbsp;</span>" + ResultHtml.PL);
				component.append(   "</div>" + ResultHtml.PL);
				component.append(  "</div>" + ResultHtml.PL);

			}
			component.append( "</div>" + ResultHtml.PL);//main
		}
		component.append("</div>" + ResultHtml.PL);
		component.append("<div class=\"space-4\"></div>" + ResultHtml.PL);
		return component.toString();
	}


	public void createScript() {
		getHtml().addScript( getScript() );
	}

	public String getScript() {
		return "";
	}

	public String createScriptPopulateByJson() throws DeveloperException {
		
		if ((super.getDataClass() != null) && (StringUtils.hasValue(super.getDataMethod()))) {

			Ajax ajax = new Ajax(super.getDataClass(), super.getDataMethod());
			ajax.addProperty("type", "GET");
			ajax.setNameVar(getId());
			ajax.setSuccessOrWarningMessage("populateView('#"+ getId() +"', data.data);");
			ajax.addParamJs(FWConstants.PARAM_FW_MASTER_ID, "("+ super.getDataId() +" || _id)");
			//adicionei essa linha para usar no Controller
			// pois não estava carregando dados do doData;
			ajax.addParamJs(FWConstants.PARAM_FW_SLAVE_ID, "(_idSlave)");
			return ajax.getComponent();
		}

		return "";
	}

	public void setParent(WindowHtml win) {
		this.win = win;
	}

}