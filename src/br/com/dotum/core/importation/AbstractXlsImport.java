package br.com.dotum.core.importation;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;

public class AbstractXlsImport {

	private Class<? extends Bean> classe;
	private ORA ora;
	private FileMemory fm;

	public <T> T imp(Class<? extends Bean> classe, ORA ora, FileMemory fm, Integer sheetNumber) throws Exception {
		this.classe = classe;
		this.ora = ora;
		this.fm = fm;

		List<Bean> beanList = new ArrayList<Bean>();

		Workbook workbook = null;
		Sheet sheet = null;
		Iterator<Row> rowIterator = null;
		if (fm.getExtension().equals("xls")) {
			workbook = new HSSFWorkbook(fm.getStream());
			sheet = workbook.getSheetAt(sheetNumber);
			rowIterator = sheet.iterator();
		} else {
			workbook = new XSSFWorkbook(fm.getStream());
			sheet = workbook.getSheetAt(sheetNumber);
			rowIterator = sheet.iterator();
		}

		int r = 0;
		while (rowIterator.hasNext()) {

			Bean b = classe.newInstance();
			beanList.add(b);

			boolean temValor = false;
			for (int c = 0; c < ora.size(); c++) {
				List<ORA> oraList = ora.getORA();
				ORA orat = oraList.get(c);

				Object value = getCell(sheet, orat.getTypeProperty(), orat.getNameColumn(), r);
				if (value != null)
					temValor = true;

				if (orat.getTypeProperty().equals(Date.class)) {
					if (value instanceof Date) {
						b.set(orat.getNameProperty(), value);
					} else if (value instanceof String) {
						b.set(orat.getNameProperty(), FormatUtils.parseDate((String)value));
					}

				} else if (orat.getTypeProperty().equals(Integer.class)) {
					if (value instanceof Double) {
						b.set(orat.getNameProperty(), ((Double)value).intValue());
					} else if (value instanceof Integer) {
						b.set(orat.getNameProperty(), value);
					} 
				} else if (orat.getTypeProperty().equals(BigDecimal.class)) {
					if (value instanceof BigDecimal) {
						b.set(orat.getNameProperty(), ((BigDecimal)value));
					} 
				} else {
					b.set(orat.getNameProperty(), value);
				}

			}
			r++;

			if (temValor == false) 
				break; 
		}
		workbook.cloneSheet(sheetNumber);

		return (T)beanList;
	}

	private Object getCell(Sheet s, Class<?> type, String coluna, int linha) {
		Row r = s.getRow(linha);
		if (r == null) return null;
		Cell c = r.getCell(getColumnByLetter(coluna));
		if (c == null) return null;

		if (type.equals(Integer.class)) {
			Integer value = null;
			try {
				value = ((Double)c.getNumericCellValue()).intValue();
			} catch (Exception e) {
				throw new WarningException("[Integer] Nao é possivel converter o valor \"" + c.toString() + "\" para data na celula " + coluna + linha);
			}
			return value;
		} else if (type.equals(Double.class)) {
			Double value = null;
			try {
				value = c.getNumericCellValue();
			} catch (Exception e) {
				throw new WarningException("[Double] Nao é possivel converter o valor \"" + c.toString() + "\" para data na celula " + coluna + linha);
			}
			return value;
		} else if (type.equals(Date.class)) {
			Date d = null;
			try {
				d = c.getDateCellValue();
			} catch (Exception e) {
				String value = c.getStringCellValue();
				if (StringUtils.hasValue(value)) {
					try {
						d = FormatUtils.parseDate(value.trim());
					} catch (ParseException e2) {
						throw new WarningException("[Date] Nao é possivel converter o valor \"" + c.toString() + "\" para data na celula " + coluna + linha);
					}
				}
			}

			return d;
		} else if (type.equals(String.class)) {
			Object value = getValue(c);

			if (value instanceof Double) {
				return ""+(Double)value;
			} else {
				return c.getStringCellValue();
			}
		}

		return null;
	}

	private Object getValue(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();
		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue();
		case Cell.CELL_TYPE_BLANK:
			return null;
		}

		return null;
	}

	private Integer getColumnByLetter(String letter) {
		switch(letter) {
		case "A": return 0;
		case "B": return 1;
		case "C": return 2;
		case "D": return 3;
		case "E": return 4;
		case "F": return 5;
		case "G": return 6;
		case "H": return 7;
		case "I": return 8;
		case "J": return 9;
		case "K": return 10;
		case "L": return 11;
		case "M": return 12;
		case "N": return 13;
		case "O": return 14;
		case "P": return 15;
		case "Q": return 16;
		case "R": return 17;
		case "S": return 18;
		case "T": return 19;
		case "U": return 20;
		case "V": return 21;
		case "W": return 22;
		case "X": return 23;
		case "Y": return 24;
		case "Z": return 25;
		case "AA": return 26;
		case "AB": return 27;
		case "AC": return 28;
		case "AD": return 29;
		case "AE": return 30;
		case "AF": return 31;
		case "AG": return 32;
		case "AH": return 33;
		case "AI": return 34;
		case "AJ": return 35;
		case "AK": return 36;
		case "AL": return 37;
		case "AM": return 38;
		case "AN": return 39;
		case "AO": return 40;
		case "AP": return 41;
		case "AQ": return 42;
		case "AR": return 43;
		case "AS": return 44;
		case "AT": return 45;
		case "AU": return 46;
		case "AV": return 47;
		case "AW": return 48;
		case "AX": return 49;
		case "AY": return 50;
		case "AZ": return 51;
		}
		return -1;
	}
}

