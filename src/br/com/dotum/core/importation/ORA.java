package br.com.dotum.core.importation;

import java.util.ArrayList;
import java.util.List;

public class ORA {
	
	private List<ORA> oraList = new ArrayList<ORA>();
	private Class<?> typeProperty;
	private String nameProperty;
	private String nameColumn;
	
	public ORA() {
	}
	
	public List<ORA> getORA() {
		return oraList;
	}

	public int size() {
		return oraList.size();
	}

	private ORA(Class<?> typeProperty, String nameProperty, String nameColumn) {
		super();
		this.typeProperty = typeProperty;
		this.nameProperty = nameProperty;
		this.nameColumn = nameColumn;
	}

	public void add(Class<?> typeProperty, String nameProperty, String nameColumn) {
		oraList.add(new ORA(typeProperty, nameProperty, nameColumn));
	}

	public Class<?> getTypeProperty() {
		return typeProperty;
	}

	public void setTypeProperty(Class<?> typeProperty) {
		this.typeProperty = typeProperty;
	}

	public String getNameProperty() {
		return nameProperty;
	}

	public void setNameProperty(String nameProperty) {
		this.nameProperty = nameProperty;
	}

	public String getNameColumn() {
		return nameColumn;
	}

	public void setNameColumn(String nameColumn) {
		this.nameColumn = nameColumn;
	}
	
}