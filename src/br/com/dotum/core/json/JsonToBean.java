package br.com.dotum.core.json;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.StringUtils;

public class JsonToBean {

	private String json;

	public JsonToBean(String json) {
		if (StringUtils.hasValue(json) == false) return;
		this.json = json.trim();
	}

	public Bean getBean() throws ParseException {
		List<Bean> list = getBeanList();
		if (list.size() == 0) {
			return new Bean();
		} else {
			return list.get(0);
		}
	}
	public List<Bean> getBeanList() throws ParseException {
		List<Bean> list = new ArrayList<Bean>();
		if (json == null) return list;
		
		
		String temp = null;
		boolean inicioArray = json.startsWith("[");
		boolean fimArray = json.endsWith("]");
		int size = json.length();

		boolean inicioObject = json.startsWith("{");
		boolean fimObject = json.endsWith("}");

		if (size > 2 && inicioArray && fimArray) {
			temp = json.substring(1, json.length()-1);

			String[] parts = temp.split("\\},\\{");

			for (int i = 0; i < parts.length; i++) {
				String part = parts[i];

				if ((parts.length > 1) && (i == 0)) part = part+"}";
				if ((parts.length > 1) && (i == (parts.length-1))) part = "{" + part;

				Bean b = parse(part);
				list.add(b);
			}

		} else if (size > 2 && inicioObject && fimObject) {
			Bean b = parse(json);
			list.add(b);
		}

		return list;
	}

	public static Bean parse(String jsonObject) throws ParseException {
		JSONParser parser = new JSONParser();

		ContainerFactory containerFactory = new ContainerFactory(){
			public List creatArrayContainer() {
				return new LinkedList();
			}

			public Map createObjectContainer() {
				return new LinkedHashMap();
			}                     
		};

		Map json = (Map)parser.parse(jsonObject, containerFactory);
		Iterator iter = json.entrySet().iterator();
		
		Bean b = new Bean();
		while(iter.hasNext()) {
			Map.Entry entry = (Map.Entry)iter.next();
			
			
			if (entry.getValue() instanceof Long) {
				b.set((String)entry.getKey(), ((Long)entry.getValue()).intValue());
			} else {
				b.set((String)entry.getKey(), entry.getValue());
			}
		}

		return b;
	}


	public static void main(String[] args) throws ParseException {
		
		String s = "{\"id\":1, \"name\":\"isac\"}";
		
		parse(s);




	}
}

