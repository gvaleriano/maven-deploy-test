package br.com.dotum.core.listener;


import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import br.com.dotum.core.component.AbstractJob;
import br.com.dotum.core.component.ComponentHelper;
import br.com.dotum.core.component.Job;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.ListDD;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import it.sauronsoftware.cron4j.Scheduler;

@WebListener
public class StartListener implements ServletContextListener {
	private static Log LOG = LogFactory.getLog(StartListener.class);

	public static Scheduler s = new Scheduler();

	public void contextInitialized(ServletContextEvent sce) {
//		String preFixPackage = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
//		if (StringUtils.hasValue(preFixPackage) == false) preFixPackage = "dotum";
//		String title = PropertiesUtils.getString(PropertiesConstants.SYSTEM_TITLE);
//		if (StringUtils.hasValue(title) == false) title = "DotumCore";
//
//		Boolean startDD = PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_START_DD);
//		Boolean startJob = PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_START_JOB);
//		Boolean startComponent = PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_START_COMPONENT);
//
//
//		LOG.info("Iniciando o sistema " + title + "...");
//
//		TransactionDB trans = null;
//		try {
//			trans = TransactionDB.getInstance(null);
//			UserContext userContext = new UserContext();
//
//			// Colocar as classes em cache
//			ComponentHelper.doCacheClass(trans, sce.getServletContext(), preFixPackage, false);
//
//			// ativando o job
//			if (startJob == true)
//				doJobStart(sce.getServletContext());
//
//			// TODO verificar se a estrutura esta atualizada para não passar raiva
//
//
//			// rodar o atualizarDB aqui
//			if (startDD == true) {
//				doUpdateDD(trans, preFixPackage);
//			}
//
//			// agora vamos criar a estrutura de componente e carregar todos automaticamente!!!
//			if (startComponent == true) {
//				Map<String, Class<?>> classeFullMap = (Map<String, Class<?>>)sce.getServletContext().getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_FULL_MAP);
//				Map<String, DotumSmartReport> smartReportMap = (LinkedHashMap<String, DotumSmartReport>)sce.getServletContext().getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_SMARTREPORT_MAP);
//
//				if (trans == null) trans = TransactionDB.getInstance(null);
//				ComponentHelper.createTableComponente(trans);
//				ComponentHelper.mergeComponente(trans, classeFullMap, smartReportMap);
//			}
//			ComponentHelper.doCacheCustom(trans, sce.getServletContext());
//
//			// componente
//			// ** criar tabela;
//			// ** inserir componente
//			// ** apagar componente
//
//			// alerta
//			// ** criar tabela;
//
//			// configuracao
//			// ** criar tabela;
//
//			// usuario
//			// ** criar tabela;
//
//
//			// tarefa de acesso;
//
//			// tutorial
//
//			if (trans != null) trans.commit();
//			LOG.info("Iniciado o sistema " + title + " com sucesso.");
//		} catch (Exception e) {
//			LOG.error(e.getMessage(), e);
//			try {
//				if (trans != null)
//					trans.rollback();
//			} catch (Exception e1) {
//				e1.printStackTrace();
//			}
//		} finally {
//			try {
//				if (trans != null)
//					trans.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}

	} 


	public void contextDestroyed(ServletContextEvent sce) {
		//doJobStop();
	}

	private static void doJobStart(ServletContext sc) {
//		LOG.info("Agendando os JOB's...");
//
//		try {
//			boolean iniciar = false;
//
//			Map<String, Class<? extends AbstractJob>> jobMap = (LinkedHashMap<String, Class<? extends AbstractJob>>)sc.getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_JOB_MAP);
//			for (String key : jobMap.keySet()) {
//				Class<?> classe = jobMap.get(key);
//				if (classe.getSuperclass().equals(AbstractJob.class) == false) continue;
//
//				AbstractJob job = (AbstractJob)classe.newInstance();
//				try {
//					Job jobAnn = classe.getDeclaredAnnotation(Job.class);
//					if (jobAnn == null) continue;
//
//					iniciar = true;
//					s.schedule(jobAnn.frequency(), job);
//					LOG.info("Agendado o JOB \""+ classe.getName() +"\" com sucesso.");
//				} catch (Exception e) {
//					String nameJob = (job != null ? classe.getName() : "não identificado");
//					LOG.error("Não foi possivel agendar o JOB \""+ nameJob +"\" motivo: " + e.getLocalizedMessage(), e);
//				}
//			}
//
//			if (iniciar == true)
//				s.start();
//		} catch (Exception e) {
//			LOG.error("Problema com o JOB", e);
//		}

	}
	private void doJobStop() {
//		if(s.isStarted()){
//			s.stop();
//		}
	}

	/**
	 * Rodar automaticamente os DD quando inicia a aplicacao
	 * Apos cada DD ele ira dar o commit
	 */
	private void doUpdateDD(TransactionDB trans, String preFixPackage) {
//		LOG.info("Atualizando os DD...");
//
//		try {
//			Class<?> classe = Class.forName(TransactionDB.PACKAGE_DD + ".ApplicationDD");
//			ListDD dd = (ListDD)classe.newInstance();
//			dd.run(trans);
//		} catch (Exception e) {
//			LOG.error("Não foi possivel atualizar o DD.", e);
//		}
	}
}

