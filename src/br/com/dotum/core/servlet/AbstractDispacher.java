package br.com.dotum.core.servlet;

import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.AbstractCommon;
import br.com.dotum.core.component.AbstractView;
import br.com.dotum.core.component.ComponentHelper;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.ContainerComponentHtml;
import br.com.dotum.core.component.DataComponentHtml;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Task;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.core.html.other.WindowHtml;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.bean.ComponenteEstatisticaBean;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

public abstract class AbstractDispacher {
	private static Log LOG = LogFactory.getLog(AbstractDispacher.class);

	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected TransactionDB trans;
	protected UserContext userContext;
	protected ResultHtml html;
	protected Map<String, Object> map;
	protected boolean runEver = true;

	protected AbstractDispacher(HttpServletRequest request, HttpServletResponse response, 
			TransactionDB trans, ResultHtml html, Map<String, Object> map, 
			boolean runEver) {
		this.request = request;
		this.response = response;
		this.trans = trans;
		this.html = html;
		this.map = map;
		this.runEver = runEver;

		this.userContext = ServletHelper.getUsuarioSession(request);
		if (userContext != null)
			this.userContext.setServerPath(ServletHelper.getServerPath(request));
	}

	protected abstract void run(Class<?> fwClass, String fwMethodStr, Object fwAbstractObj) throws Exception;
	protected void doEstatistica(Class<?> clazz, UserContext userContext, TransactionDB trans) throws SQLException, Exception {
		if (userContext == null) return;
		if (trans == null) return;

		try {
			Date d1 = trans.getDataAtual();
			Date finishPerfomance = new Date();
			Long tempoProcessamento = finishPerfomance.getTime() - d1.getTime(); 

			ComponenteEstatisticaBean cptcBean = new ComponenteEstatisticaBean();
			cptcBean.setUnidade(userContext.getUnidade().getId());
			cptcBean.setUsuario(userContext.getUser().getId());
			cptcBean.setPessoa(userContext.getUser().getId());
			cptcBean.getComponente().setId(ComponentHelper.getIdByClass(clazz));
			cptcBean.setData(trans.getDataAtual());
			cptcBean.setHora(trans.getHoraAtual());
			cptcBean.setTransacao(trans.getId());
			cptcBean.setTempoProcessamento(tempoProcessamento.intValue());
			trans.insert(cptcBean);

		} catch (Exception e) {
			LOG.debug("Falta a estatistica do componente " + ComponentHelper.getIdByClass(clazz));
		}
	}

	public static boolean temAcesso(Class<?> classe, UserContext userContext) throws DeveloperException {
		Menu m = (Menu)classe.getAnnotation(Menu.class);
		Task t = (Task)classe.getAnnotation(Task.class);
		if (userContext == null) return false;
		if (m == null) return false;

		boolean temAcesso = false;
		if (userContext.isDeveloper() == true) {
			temAcesso = true;
		} else if ((m.publico() == false) && (t == null)) {
			String code = ClassUtils.getCodeTask(classe);

			//TODO
			//essa linha está dando erro sempre que o getString retorna nulo (suponho que por ausencia de sessão)
			//o log do erro é grande e desnecessário se o motivo for realmente a sessão
			boolean sicoob = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE).equals("sicoob");
			if (sicoob == true || userContext.getUser().getTask(code) != null) {
				temAcesso = true;
			}
		} else if ((m.publico() == false) && (t != null)) {
			for (String code : t.value()) {
				if (userContext.getUser().hasAccessTaskByCode(code) == true) {
					temAcesso = true;
					break;
				}
			}
		} else if (m.publico() == true) {
			temAcesso = true;
		}

		return temAcesso;
	}

	protected Object invokeMetodoDoProgramador(AbstractCommon ac, String methodStr) throws Exception {
		Object objResult = null;


		Menu m = ac.getClass().getDeclaredAnnotation(Menu.class);
		String masterIdStr = (String) ServletHelper.getParameter(ac.getRequest(), ac.getList(), FWConstants.PARAM_FW_MASTER_ID, false);

		Integer[] masterIdArray = null;
		if (masterIdStr != null) {
			masterIdArray = ArrayUtils.parseToInt(masterIdStr.split(","));
		} else {
			if (methodStr.startsWith("doData") && m.selecteds() > 0) {

				// nao tem o ID entao nao é para filtrar e buscar nenhuma tabela
				// isso foi criado com o objetivo de nao fazer a requisição quando abre a tela pela primeira vez
				// e nao ha a necessidade de mandar dados!!!
				
				return null;

			} else {

				masterIdArray = new Integer[] {null};

			}
		}


		boolean menuZero = (masterIdArray == null);
		boolean menuUm = ((masterIdArray != null) && (masterIdArray.length == 1));
		boolean menuVarios = ((masterIdArray != null) && (masterIdArray.length > 1));

		boolean methodZero = false;
		boolean methodUm = false;
		boolean methodVarios = false;

		Method method = null;
		Method[] methodArray = ac.getClass().getDeclaredMethods();
		for (Method temp : methodArray) {
			if (temp.getName().equalsIgnoreCase(methodStr) == false) continue;

			method = temp;
			if (method.getParameterTypes().length == 0) {
				methodZero = true;
			} else if (method.getParameterTypes().length == 1) {
				if (method.getParameterTypes()[0].equals(Integer.class)) methodUm = true;
				if (method.getParameterTypes()[0].equals(Integer[].class)) methodVarios = true;
			} else if (method.getParameterTypes().length > 1) {
				throw new DeveloperException("O metodo foi montado errado... So é possivel ter um parametro do tipo Integer ou um Integer[]");
			}

			break;
		}

		try {
			if (methodZero == true) {
				method = ac.getClass().getMethod(methodStr);
				objResult = method.invoke(ac);
			} else if (methodUm == true) {
				method = ac.getClass().getMethod(methodStr, Integer.class);
				objResult = method.invoke(ac, masterIdArray[0]);
			} else if (methodVarios == true) {
				method = ac.getClass().getMethod(methodStr, Integer[].class);
				objResult = method.invoke(ac, new Object[] {masterIdArray});
			}

		} catch (java.lang.reflect.InvocationTargetException e) {

			// deu qualquer tipo de erro ou msg de erro para o usuario...
			// rollback na certa

			ac.getTransaction().rollback();

			if (e.getTargetException() instanceof WarningException) {
				// aqui eu crio a msg do throw warningExecption
				ac.setMessageWarning(e.getTargetException().getLocalizedMessage());

			} else if (e.getTargetException() instanceof NotFoundException) {

				if (methodStr.toLowerCase().startsWith("dodata")) {
					objResult = new Bean();
				} else {
					ac.setMessageWarning(e.getCause().getLocalizedMessage());
				}

			} else {
				ac.setMessageError(e.getCause().getLocalizedMessage());
				LOG.error(userContext, e.getCause().getLocalizedMessage(),e);
			}
		}

		
		// como nao achou o metodo e comeca com doData
		// basta criar um bean q no final ja coloca o ID que chegou aqui
		if ((objResult == null) && (methodStr.toLowerCase().startsWith("dodata"))) {
			objResult = new Bean();
		}

		
		// precisa ser aqui no final..
		// vou mandar sempre o ID da tela principal
		if (masterIdStr != null)
			if (objResult instanceof Bean) {
				((Bean)objResult).set(FWConstants.PARAM_FW_MASTER_ID, masterIdStr);
			} else if (objResult instanceof List<?>) {
				for (Object b : (List<?>)objResult) {
					if (b instanceof Bean) 
						((Bean)b).set(FWConstants.PARAM_FW_MASTER_ID, masterIdStr);
				}
			}

		return objResult;
	}

	/**
	 * Este metodo retorna os dados em formato de JSON
	 * Aqui basicamente será usado no Action e Controller
	 * 
	 * @param abstractCommon
	 * @param methodStr
	 * @throws Exception
	 */
	protected void doDataAbstractCommon(AbstractCommon abstractCommon, String methodStr) throws Exception {
		Object beanListOrBean_retornoDoProgramador = invokeMetodoDoProgramador(abstractCommon, methodStr);
		String result = createJsonFromBean(abstractCommon, methodStr, beanListOrBean_retornoDoProgramador);

		ResultHtml html = abstractCommon.getResultHtml();
		html.addComponent( new Html(result) );
	}

	protected String getResultFinal(AbstractCommon ac, Object obj) throws Exception {
		String result = null;

		if (obj != null) {
			result = createJsonFromBean(ac, "xxxx", obj);
		}

		// nao pode ter o else aqui mesmo...
		if (StringUtils.hasValue(result) == false) {
			result = getDownloadJson(ac);
		}

		if (StringUtils.hasValue(result) == false) {
			// TODO isso ainda nao funciona... é para o caso da action
			// montar o HTML dinamicamente
			result = DispacherAction.getComponentDinamico(ac, ""+ac.getParamInteger("id"));
		}

		// nao pode ter o else aqui mesmo...
		if (StringUtils.hasValue(result) == false) {
			result = getMessageJson(ac, true);
		}

		// nao pode ter o else aqui mesmo...
		if (StringUtils.hasValue(result) == false) {
			result = createMessage(MessageHtml.Type.SUCCESS, null);
		}

		return result;
	}

	/**
	 * Este metodo serve para carregar os dados dos componentes das janelas das actions
	 * da view ou global action
	 * porem a tela so sera aberta mediante a todos os componentes responderem com mensagem de sucesso
	 * ou sem dados
	 * 
	 * no caso de enviar um WarningException ou super.setMessageWarning... 
	 * a abertura da tela será abortada
	 * @param view
	 * @param action
	 * @param win
	 * @return
	 * @throws DeveloperException
	 */
	protected String createScriptPopulateComponentes(AbstractView view, AbstractAction action, WindowHtml win) throws DeveloperException {
		TableHtml table = null;
		if (view != null) {
			table = view.getTable();
		}
		return DataComponentHtml.createScriptPopulateByJsonOpenWindowIfAllOk(table, action.getBodyCompList(), win, action.getClass().getName());
	}

	protected String createJsonFromBean(AbstractCommon ac, String methodStr, Object beanListOrBean_retornoDoProgramador) throws Exception {
		String result = null;

		String origem = "";
		if (PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_DEBUGCODE) == true) { 
			origem = "\"from\": \"" + ac.getClass().getName() + "." + methodStr + "\", ";
		}
		
		String messageJson = getMessageJson(ac, false);
		if (StringUtils.hasValue(messageJson)) {
			result = messageJson;
		} else {
			StringBuilder sbb = new StringBuilder();


			if (ac.isApi()) {
				// no caso de API do jeito que o programador mandar o Obj do tipo bean.. vamos mandar para frente
				if (beanListOrBean_retornoDoProgramador instanceof Bean == false)
					throw new DeveloperException("O objeto que esta sendo retornado nao é do tipo Bean");

				Bean bean = (Bean)beanListOrBean_retornoDoProgramador;
				result = bean.toJson2();

			} else if (beanListOrBean_retornoDoProgramador instanceof Bean) {

				Bean bean = (Bean)beanListOrBean_retornoDoProgramador;

				int size = 0;
				if (bean.sizeAttributes() > 0) size = 1;

				sbb.append("{"+ origem +"\""+ FWConstants.JSON_ATTR_TYPE +"\": \""+ FWConstants.JSON_ATTR_TYPE_SUCCESS +"\", \""+ FWConstants.JSON_ATTR_SIZE +"\": "+ size +", ");
				sbb.append("\""+ FWConstants.JSON_ATTR_DATA +"\": " + bean.toJson());
				sbb.append("}");

				result = sbb.toString();
			} else {
				List<Bean> beanList = (List<Bean>)beanListOrBean_retornoDoProgramador;
				if (beanList == null) beanList = new ArrayList<Bean>();

				String temp = "";


				sbb.append("{"+ origem +"\""+ FWConstants.JSON_ATTR_TYPE +"\": \""+ FWConstants.JSON_ATTR_TYPE_SUCCESS +"\", \""+ FWConstants.JSON_ATTR_SIZE +"\": "+ beanList.size() +", ");
				sbb.append("\"data\": [");

				if ( beanList != null) {
					for (Bean bean : beanList) {
						if (bean == null) continue;

						sbb.append( temp );
						sbb.append( bean.toJson() );
						temp = ",";
					}
				}
				sbb.append("]");
				sbb.append("}" + ResultHtml.PL);

				result = sbb.toString();
			}
		}		
		return result;
	}


	protected String getDownloadJson(AbstractCommon ac) throws IOException, DeveloperException {
		if (ac.getDownloadFileMemoryList().size() == 0) return "";

		StringBuilder sb = new StringBuilder();
		sb.append("{\""+ FWConstants.JSON_ATTR_TYPE +"\": \"success\",");
		sb.append( "\""+ FWConstants.JSON_ATTR_REPORT +"\":[");


		String temp = "";
		for (int i = 0; i < ac.getDownloadFileMemoryList().size(); i++) {
			FileMemory fm = ac.getDownloadFileMemoryList().get(i);
			Integer sizeZerado = ac.getDownloadSizeList().get(i);
			boolean required = ac.getDownloadRequiredList().get(i);

			int size = fm.getBytes().length;
			if (size > sizeZerado) {
				sb.append( temp );
				sb.append("{\"size\": "+ size +", \"fileName\": \""+ fm.getName() +"\", \"contentType\":\""+ fm.getContentType() +"\", \"data\": \""+fm.getBase64()+"\"}");
				temp = ", ";
			} else if (required == true) {
				return createMessage(MessageHtml.Type.WARNING, "Nenhuma informação encontrada para este relatório.");
			}
		}
		sb.append("]}");

		return sb.toString();
	}


	protected void createMessage(AbstractCommon ac, boolean success) {
		MessageHtml message = null;
		if (StringUtils.hasValue(ac.getMessageError())) {
			message = new MessageHtml(MessageHtml.Type.ERROR, ac.getMessageError());
			message.setFlutuante(true);
		} else if (StringUtils.hasValue(ac.getMessageWarning())) {
			message = new MessageHtml(MessageHtml.Type.WARNING, ac.getMessageWarning());
			message.setFlutuante(true);
		} else if (StringUtils.hasValue(ac.getMessageSuccess())) {
			message = new MessageHtml(MessageHtml.Type.SUCCESS, ac.getMessageSuccess());
			message.setFlutuante(true);
		}			
		if (message != null) ac.addScript(message.getScript());
	}

	protected String getMessageJson(AbstractCommon ac, boolean success) {
		String result = "";

		Date d1 = trans.getDataAtual();
		Date finishPerfomance = new Date();

		// TODO voltar isso aqui de forma correta...
		// tem que na resposta pegar o valor do tempo e apresnetar no HTML conforme precisa
		// aqui so pode voltar json
		String processo = "<br /><div class=\"pull-right\">(" + DateUtils.diffTimeSimple(d1, finishPerfomance) + ")</div>";
		processo = "";

		if (StringUtils.hasValue( ac.getMessageError() )) {
			result = createMessage(MessageHtml.Type.ERROR, ac.getMessageError() + processo );
		} else if (StringUtils.hasValue( ac.getMessageWarning() )) {
			result = createMessage(MessageHtml.Type.WARNING, ac.getMessageWarning() + processo);
		} else if ((success == true) && (StringUtils.hasValue( ac.getMessageSuccess() ))) {
			result = createMessage(MessageHtml.Type.SUCCESS, ac.getMessageSuccess() + processo);
		}

		return result;
	}


	protected static String getComponentDinamico(AbstractCommon ac, String id) throws Exception {
		String result = "";

		List<ComponentHtml> list = ac.getBodyCompList();
		if (list.size() > 0) {
			StringBuilder sb = new StringBuilder();
			for (ComponentHtml bean : list) {
				bean.setPrefix("d");
				sb.append( bean.getComponente() );

				if (bean.getClass().getSuperclass().equals(DataComponentHtml.class)) {
					DataComponentHtml xxx = (DataComponentHtml)bean;
					sb.append("<script>");

					// TODO aqui eu preciso colocar o ID da tabela principal!!
					//					sb.append( TableHtml.createScriptDataRow(true, ScreenView.VIEW_TABLE_ID, true) );
					sb.append("var _id = " + id+ ";");
					sb.append("var _idSlave = null;");
					sb.append(xxx.createScriptPopulateByJson());
					sb.append("</script>");
				}

			}
			sb.append("<script>");
			criaScriptRecursivo( list, sb);
			sb.append("</script>");

			Bean b = new Bean();
			b.set("html", sb.toString());
			b.set(FWConstants.JSON_ATTR_STATUS, FWConstants.JSON_ATTR_TYPE_SUCCESS);

			result = b.toJson();
		}

		return result;
	}
	
	private static String criaScriptRecursivo(List<ComponentHtml> list, StringBuilder sb) throws DeveloperException {
		
		for (ComponentHtml c : list) {
			if (c.getClass().getSuperclass().equals(ContainerComponentHtml.class)) {
				criaScriptRecursivo(((ContainerComponentHtml) c).getComponentList(), sb);
			}
			if (c.getClass().equals(FormHtml.class)) {
				FormHtml f = (FormHtml)c;
				
				
				for (AbstractFieldHtml field : f.getAllFieldList()) {
					sb.append( field.getScript() );
				}
			}
			sb.append( c.getScript() );
		}
		
		return sb.toString();
	}

	// HELPER
	public static String createMessage(MessageHtml.Type type, String message) {
		return "{\""+ FWConstants.JSON_ATTR_TYPE +"\": \""+ type.toString() +"\", \""+ FWConstants.JSON_ATTR_STATUS +"\": \""+ type.toString() +"\", \"message\": \""+ MessageHtml.prepareMessageHtml(message) +"\"}";
	}
}


