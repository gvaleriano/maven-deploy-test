package br.com.dotum.core.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.util.PropertiesUtils;

public class ApplicationContext {

	private HttpServletRequest request;
	private HttpServletResponse response;
	private Class classRun;
	private String serverPath;
	private String packageBase;
	
	
	public HttpServletRequest getRequest() {
		return request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public Class getClassRun() {
		return classRun;
	}
	public String getServerPath() {
		return serverPath;
	}
	public String getPackageBase() {
		return packageBase;
	}
	public String getFullPathOfClassRun() {
		return getServerPath() + FWConstants.TOMCAT_PATH_CLASSES + getClassRun().getPackage().getName().replaceAll("\\.", "/") + "/";
	}
	public void prepare(HttpServletRequest request, HttpServletResponse response, Class classRun) {
		this.request = request;
		this.response = response;
		this.classRun = classRun;
		
		this.serverPath = ServletHelper.getServerPath(request);
		this.packageBase = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
	}
}
