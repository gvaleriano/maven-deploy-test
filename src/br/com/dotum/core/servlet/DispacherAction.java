package br.com.dotum.core.servlet;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

public class DispacherAction extends AbstractDispacher {
	private static Log LOG = LogFactory.getLog(DispacherAction.class);

	protected DispacherAction(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, ResultHtml html, Map<String, Object> map) {
		super(request, response, trans, html, map, false);
	}
	

	@Override
	protected void run(Class fwClass, String fwMethodStr, Object fwAbstractObj) throws Exception {
		AbstractAction action = (AbstractAction)fwAbstractObj;
		action.prepare(request, response, trans, html, map);
		html.setAjax();

		if (fwMethodStr.startsWith("doData")) {
			
			doDataAbstractCommon(action, fwMethodStr);
			
		} else {
			
			doCreateAbstractActionProcess(action, fwMethodStr);
			doEstatistica(fwClass, userContext, trans);
			
		}

	}

	private void doCreateAbstractActionProcess(AbstractAction action, String methodStr) throws Exception {
		ResultHtml html = action.getResultHtml();
		UserContext userContext = action.getUserContext();
		
		
		String result = null;
		try {
			
			Object beanListOrBean_retornoDoProgramador = invokeMetodoDoProgramador(action, methodStr);
			result = getResultFinal(action, beanListOrBean_retornoDoProgramador);

		} catch (Exception e) {
			String mensagem = e.getMessage();

			if (mensagem == null) {
				if (e.getCause() == null) {
					mensagem = e.getLocalizedMessage();
				} else {
					mensagem = e.getCause().getMessage();
				}
				
			}
			if (mensagem == null) {
				mensagem = "Aconteceu algum problema que não sei resolver.\n";
				mensagem += "Já notifiquei a equipe responsável";
			}

			String unidade = "";
			Integer unidadeId = -1;
			String usuario = "";
			Integer usuarioId = -1;
			if (userContext != null) {
				unidade = userContext.getUnidade().getNome();
				unidadeId = userContext.getUnidade().getId();
				
				usuario = userContext.getUser().getNome();
				usuarioId = userContext.getUser().getId();
			}
			

			if (e.getCause() instanceof WarningException) {
				result = AbstractDispacher.createMessage(MessageHtml.Type.WARNING, mensagem);
				LOG.error(mensagem + ". Usuário \"" + usuario + "\" (Id: "+ usuarioId +") da unidade \""+ unidade +"\" (Id: "+ unidadeId +")");
			} else {
				result = AbstractDispacher.createMessage(MessageHtml.Type.ERROR, mensagem);
				LOG.error(mensagem + ". Usuário \"" + usuario + "\" (Id: "+ usuarioId +") da unidade \""+ unidade +"\" (Id: "+ unidadeId +")", e);
			}

			action.getTransaction().rollback();
		}
		html.addComponent( new Html(result) );
	}

}


