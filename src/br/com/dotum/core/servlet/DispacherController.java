package br.com.dotum.core.servlet;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.StringUtils;

public class DispacherController extends AbstractDispacher {
	private static Log LOG = LogFactory.getLog(DispacherController.class);

	protected DispacherController(HttpServletRequest request, HttpServletResponse response, 
			TransactionDB trans, ResultHtml html, 
			Map<String, Object> map) {
		super(request, response, trans, html, map, false);
	}

	@Override
	protected void run(Class fwClass, String fwMethodStr, Object fwAbstractObj) throws Exception {
		AbstractController controller = (AbstractController)fwAbstractObj;
		String param = request.getHeader("x-requested-with");
		if (param != null) {
			boolean ajax = param.equalsIgnoreCase("XMLHttpRequest");
			if (ajax == true) html.setAjax();
		}
		
		if (ResultHtml.DEBUG == true) {
			LOG.error(userContext, "Classe: " + fwClass + ". Metodo: " + fwMethodStr + ". Parametros: " + ServletHelper.showAllParameter(request, map) );
		}	


		controller.prepare(request, response, trans, html, map);

		String part1 = StringUtils.getNumberOnly(controller.getClass().getPackage().getName());
		String part2 = StringUtils.getNumberOnly(controller.getClass().getSimpleName());
		if (StringUtils.hasValue(part2)) part2 = "." + part2;
		html.setCode(part1 + part2);

		if (fwMethodStr.startsWith("doData")) {

			doDataAbstractCommon(controller, fwMethodStr);

		} else {

			// ir para o controller
			doCreateAbstractController(controller, fwMethodStr);
			doEstatistica(fwClass, userContext, trans);

		}

	}

	private void doCreateAbstractController(AbstractController controller, String methodStr) throws Exception {
		ResultHtml html = controller.getResultHtml();
		html.setUserContext(userContext);

		Menu m = (Menu)controller.getClass().getAnnotation(Menu.class);

		Object beanListOrBean_retornoDoProgramador = invokeMetodoDoProgramador(controller, methodStr);

		// TODO colocar o esquema magico que o Keynes e o Isac estava fazendo la no controller aqui!!!
		List<ComponentHtml> compList = controller.getBodyCompList();
		html.addComponent(compList.toArray(new ComponentHtml[compList.size()]));;

		
		if (controller.isApi()) {
			controller.getResultHtml().setAjax();
			String result = createJsonFromBean(controller, methodStr, beanListOrBean_retornoDoProgramador);
			html.addComponent( new Html(result) );
		} else if (html.isAjax() == false) {
			String title = "";
			if(StringUtils.hasValue(html.getTitle()) == false){
				if(StringUtils.hasValue(m.group())) title += m.group()+": ";
				title += m.label();
				html.setTitle(title);

				String part1 = StringUtils.getNumberOnly(controller.getClass().getPackage().getName());
				String part2 = StringUtils.getNumberOnly(controller.getClass().getSimpleName());
				html.setCode("controller " + part1 + "." + part2);
			}

			// criar ou nao as paradas todas do topo :)
			html.setInterno(!m.publico());
			if (userContext != null) {
				html.setInterno(true);
			}

			createMessage(controller, true);
		} else {
			String result = null;
			if (compList.size() > 0) {
				// nao faz nada por que o programador ja colocou
				result = "";
			} else {
				result = getResultFinal(controller, beanListOrBean_retornoDoProgramador);
			}

			html.addComponent( new Html(result) );
		}
	}


}


