package br.com.dotum.core.servlet;

import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.Show;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.WindowHtml;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ClassUtils;

public class DispacherGlobalAction extends AbstractDispacher {
	private static Log LOG = LogFactory.getLog(DispacherGlobalAction.class);

	protected DispacherGlobalAction(HttpServletRequest request, HttpServletResponse response, 
			TransactionDB trans, ResultHtml html, 
			Map<String, Object> map) {
		super(request, response, trans, html, map, true);
	}
	@Override
	protected void run(Class fwClass, String fwMethodStr, Object fwAbstractObj) throws Exception {
		ServletContext sc = request.getServletContext();
		
		if (html.isAjax() == true) return;
		UserContext userContext = ServletHelper.getUsuarioSession(request);

		for (String classeStr : ServletHelper.getClasseMap(sc).keySet()) {
			Class<?> classe = ServletHelper.getClasse(sc, classeStr);

			if (classe.getSuperclass().equals(AbstractAction.class)) {
				Menu m = (Menu)classe.getAnnotation(Menu.class);
				if (m == null) continue;
				if (m.inMenu() == -1) continue;
				if ((m.publico() == false) && (temAcesso(classe, userContext) == false)) continue;

				AbstractAction action = (AbstractAction) classe.newInstance();
				action.prepare(request, response, trans, html, map);
				action.doScreen();
				
				
				// ver se tem a annotation DotumShow
				// caso tenha executar e pegar o retorn..
				// so pode ser boolean
				// se for true exibe essa porra toda!!!
				// isso no futuro vai servir tbm para exibir o botao na table!!!
				Boolean show = null;
				
				Method xxx = ClassUtils.getMethodNameByAnnotation(action.getClass(), Show.class);
				if (xxx != null) {
					try {
						show = (Boolean)xxx.invoke(action);
					} catch (Exception e) {
						LOG.error(userContext, "Problema para detectar se eh para exibir ou nao a ação. Motivo: " + e.getMessage(), e);
					}
				}
				
				if ((show != null) && (show == false)) continue;


				// AQUI É PRECISO SABER SE VAI SER CRIADO A WINDOW OU 
				// SE VAI COLOCAR OS COMPONENTES QUE SELECIONOU LA... 
				// DIRETO NA JANELINHA

				if (m.window()) {
					//
					// HTML das actions avulsas
					//
					WindowHtml win = new WindowHtml(m.label(), m.icon());
					win.setHtml(action.getResultHtml());
					win.setSize( m.windowSize() );
					for (ComponentHtml comp : action.getBodyCompList()) {
						win.addContent( comp );
					}
					if ((show != null) && (show == true)) {
						html.addScript(win.createScriptAbreWindow());
					}

					html.addComponent(win);

					//
					// SCRIPT das actions avulsas
					//
					StringBuilder script = new StringBuilder();
					script.append(ResultHtml.PL);
					if (ResultHtml.DEBUG == true) script.append("/*SCRIPT DA ACAO "+ m.label() +"*/" + ResultHtml.PL);
					script.append("$(document).on('click','."+ classe.getSimpleName() +"',function(){");
					script.append( createScriptPopulateComponentes(null, action, null) );
					script.append( win.createScriptAbreWindow() );
					script.append( "});" + ResultHtml.PL);
					script.append(ResultHtml.PL);
					html.addScript(script.toString());
				} else {
					Double position = ((m.inMenu()));
					if (position.intValue() != 4) continue;




					html.addComponentTop(position, action.getBodyCompList());


					StringBuilder script = new StringBuilder();
					script.append(ResultHtml.PL);
					if (ResultHtml.DEBUG == true) script.append("/*SCRIPT DA ACAO "+ m.label() +"*/" + ResultHtml.PL);
					script.append("$(document).on('click','.BTN_"+ classe.getSimpleName() +"',function(){");
					script.append( createScriptPopulateComponentes(null, action, null) );
					// aqui nao abre a window pois é na janelinha mesmo
					script.append( "});" + ResultHtml.PL);
					script.append(ResultHtml.PL);
					html.addScript(script.toString());


				}
			}
		}
		
	}

}


