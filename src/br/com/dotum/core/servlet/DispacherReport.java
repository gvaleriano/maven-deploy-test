package br.com.dotum.core.servlet;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.Connection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.basic999.Controller900Report;
import br.com.dotum.core.component.AbstractReport;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.form.field.FieldFactory;
import br.com.dotum.core.html.other.ColumnHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

public class DispacherReport extends AbstractDispacher {
	private static Log LOG = LogFactory.getLog(DispacherReport.class);

	protected DispacherReport(HttpServletRequest request, HttpServletResponse response, 
			TransactionDB trans, ResultHtml html, 
			Map<String, Object> map) {
		super(request, response, trans, html, map, false);
	}
	@Override
	protected void run(Class fwClass, String fwMethodStr, Object fwAbstractObj) throws Exception {
		AbstractReport report = (AbstractReport)fwAbstractObj;
		report.prepare(request, response, trans, html, map);

		if (fwMethodStr.startsWith("doScreen")) {
			doCreateAbstractReportScreen(report, fwMethodStr);
			doEstatistica(fwClass, userContext, trans);
		} else {
			doCreateAbstractReportProcess(report, fwMethodStr);
			doEstatistica(fwClass, userContext, trans);
		}

	}

	private void doCreateAbstractReportScreen(AbstractReport report, String methodStr) throws Exception {
		UserContext userContext = report.getUserContext();
		TransactionDB trans = report.getTransaction();
		
		String codigoReport = report.getNameFile();
		boolean emitir = false;
		
		
		ComponentHtml wid1 = Controller900Report.createHtmlForm(report, codigoReport);
//		ComponentHtml wid2 = Controller010Relatorio.doProcess(userContext, trans, report, codigoReport, emitir);
		report.setType(AbstractReport.TYPE_HTML);
		
//		report.addParamField("uniId", 62);
//		report.addParamField("id", 25529);
//		FileMemory fm = getBytesByIReport(trans, report);
//		ComponentHtml wid2 = new Html(fm.getText());
		ComponentHtml wid2 = Controller900Report.createHtmlMessageFiltarAoLado();
		
		ColumnHtml column = Controller900Report.createHtml(codigoReport, wid1, wid2, emitir);
		
		html.addComponent(column);

	}
	private void doCreateAbstractReportProcess(AbstractReport report, String methodStr) throws Exception {
		UserContext userContext = report.getUserContext();
		TransactionDB trans = report.getTransaction();
		
		if (report.getField().size() == 0)
			report.doExecute();
		
		String codigoReport = report.getNameFile();
		boolean emitir = true;

		
		// vou carregar os parametros
		for (AbstractFieldHtml field : report.getField()) {
			String value = (String)ServletHelper.getParameter(report.getRequest(), report.getList(), field.getName(), false);
			report.addParamField(field.getName(), FieldFactory.parseValue(field, value));
		}

		ComponentHtml wid1 = Controller900Report.createHtmlForm(report, codigoReport);
		report.setType(AbstractReport.TYPE_HTML);
		FileMemory fm = getBytesByIReport(trans, report);
		ComponentHtml wid2 = new Html(fm.getText());
		
		ColumnHtml column = Controller900Report.createHtml(codigoReport, wid1, wid2, emitir);
		html.addComponent(column);
	}

	/**
	 * Não há mais necessidade de chamar este metodo com 3 parametros<br />
	 * a partir de agora invoke o metodo com 2 parametros
	 * @param response
	 * @param trans
	 * @param userContext
	 * @param report
	 * @throws Exception
	 */
	private void exportReportIReport(HttpServletResponse response, TransactionDB trans, AbstractReport report) throws Exception {
		FileMemory fm = null;
		try {
			fm = getBytesByIReport(trans, report);


			PrintWriter out = response.getWriter();
			StringBuilder sb = new StringBuilder();

			int size = fm.getBytes().length;
			if (size > 910) {
				sb.append("{\"type\": \"success\", \"size\": "+ size +", \"data\": {\"reportBase64Bytes\": \""+fm.getBase64()+"\"}}");
			} else {
				sb.append( AbstractDispacher.createMessage(MessageHtml.Type.WARNING, "Nenhuma informação encontrata para este relatorio.") );
			}
			out.print(sb.toString());


			//			response.setContentType("application/pdf");
			//			response.setHeader("Content-disposition","attachment; filename=\""+fm.getName()+"\"");
			//			OutputStream ops = response.getOutputStream();
			//			ops.write(fm.getBytes());
			//			ops.flush();
			//			ops.close();

		} catch (Exception fnf) {
			LOG.error(fnf.getMessage(), fnf);
			throw new DeveloperException("Não foi encontrado o jar onde esta o relatorio. <br>Possibilidades de correção: <br>1 - Verifique se o relatorio " + fm.getName() + " existe. <br>2 - Reinicie o servidor (tomcat).");
		}	
	}
	public static FileMemory getBytesByIReport(TransactionDB trans, AbstractReport report) throws Exception {
		return getBytesByIReport(trans, null, report);
	}

	/**
	 * Não há mais necessidade de chamar este metodo com 3 parametros<br />
	 * a partir de agora invoke o metodo com 2 parametros
	 * 
	 * @param trans
	 * @param userContext
	 * @param report
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	public static FileMemory getBytesByIReport(TransactionDB trans, UserContext userContext, AbstractReport report) throws Exception {
		// pegar o nome do jasper
		Map<String, Object> parms = report.getMap();
		if (report.getField().size() == 0) 
			report.doExecute();

		String path = DispacherReport.class.getResource("/").getPath() + report.getPath();
		String jasperName = path + report.getNameFile();

		if (parms.get("PARAM_LOGO") == null) {
			String pathLogoDefaultRelatorio = "../../custom/images/logoreport.png";
			URL logo = DispacherReport.class.getResource("/" + pathLogoDefaultRelatorio);
			parms.put("PARAM_LOGO", logo); 
		}


		FileMemory fm = new FileMemory(jasperName);

		// TODO descontinuar esse aqui no futuro!!!
		parms.put("SUBREPORT_DIR", path ); 
		parms.put("PARAM_PATH", path ); 
		parms.put("PARAM_FILE_NAME", jasperName); 

		// pegando a conexao com o banco
		Connection conn = (Connection) trans.getConnection();
		// se nao informou nome, vou salvar como o nome do jasper
		JasperPrint jasperPrint = JasperFillManager.fillReport(fm.getStream(), parms, conn);

		byte[] bytes = null;
		String extensao = null;
		if (report.getType().equals(AbstractReport.TYPE_PDF)) {
			bytes = JasperExportManager.exportReportToPdf(jasperPrint);
			extensao = "pdf";
		} else if (report.getType().equals(AbstractReport.TYPE_DOCX)) {
			JRDocxExporter exporter = new JRDocxExporter();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			bytes = baos.toByteArray();
			extensao = "docx";
		} else if (report.getType().equals(AbstractReport.TYPE_HTML)) {
			HtmlExporter exporter = new HtmlExporter();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			bytes = baos.toByteArray();
			extensao = "html";
			
		} else if (report.getType().equals(AbstractReport.TYPE_XLS)) {
			JRXlsxExporter exporter = new JRXlsxExporter();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			bytes = baos.toByteArray();
			extensao = "xlsx";
		}


		FileMemory fm2 = new FileMemory(bytes);
		fm2.setName(jasperPrint.getName() + "." + extensao);

		return fm2;
	}

}


