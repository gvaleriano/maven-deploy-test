package br.com.dotum.core.servlet;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.table.Bean;

public class DispacherTutorial extends AbstractDispacher {

	protected DispacherTutorial(HttpServletRequest request, HttpServletResponse response, 
			TransactionDB trans, ResultHtml html, 
			Map<String, Object> map) {
		super(request, response, trans, html, map, true);
	}
	@Override
	protected void run(Class fwClass, String fwMethodStr, Object fwAbstractObj) throws Exception {
		if (html.isAjax() == true) return;
		if (userContext == null) return;

		List<Bean> model = userContext.getUser().getAttribute("Tutorial");
		if (model != null) {
			for (Bean b : model) {
				Integer tutId = b.getAttribute("id");
				String descricao = b.getAttribute("descricao");
				Integer pessoaId = b.getAttribute("pessoaId");

				if (fwClass.getName().equals(descricao) == false) continue;
				if (pessoaId != null) continue;
				b.setAttribute("pessoaId", userContext.getUser().getId());

				String sql = "insert into tutorialpessoa (tupe_id, uni_id, tut_id, pes_id) values (s_tupe.nextval, ?, ?, ?)";
				trans.execUpdate(sql, new Object[] {userContext.getUnidade().getId(), tutId, userContext.getUser().getId()});
				
				UrlHtml url = new UrlHtml();
				url.addParam(FWConstants.PARAM_FW_CLASS, "1.006.020");
				url.addParam(FWConstants.PARAM_FW_METHOD, "doDataEspecial");
				url.addParam(FWConstants.PARAM_FW_MASTER_ID, tutId);
				String urlStr = "./fw/json/bootstro.json";
				urlStr = url.getComponente();
				
				
				String scriptHelp = "$(document).ready(function(){bootstro.start('',{nextButtonText:'Proximo',prevButtonText:'Voltar',finishButtonText:'Sair',stopOnEsc:false,stopOnBackdropClick:false,onComplete:function(params){},onExit:function(params){},url:'"+ urlStr +"'});});";
				html.addScript(scriptHelp);
				break;
			}
		}

	}

}


