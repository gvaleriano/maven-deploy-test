package br.com.dotum.core.servlet;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.ajax.Ajax;
import br.com.dotum.core.ajax.ScriptHelper;
import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.AbstractView;
import br.com.dotum.core.component.ComponentHtml;
import br.com.dotum.core.component.Help;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.component.OnCloseWindow;
import br.com.dotum.core.component.ShowWhenTab;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.form.ButtonHtml;
import br.com.dotum.core.html.form.FormHtml;
import br.com.dotum.core.html.form.field.AbstractFieldHtml;
import br.com.dotum.core.html.form.field.AutoCompleteFieldHtml;
import br.com.dotum.core.html.form.field.DateRangeFieldHtml;
import br.com.dotum.core.html.form.field.FieldFactory;
import br.com.dotum.core.html.other.ColumnHtml;
import br.com.dotum.core.html.other.Html;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.TabHtml;
import br.com.dotum.core.html.other.TableHtml;
import br.com.dotum.core.html.other.WindowHtml;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.ORM;
import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

public class DispacherView extends AbstractDispacher {
	private static Log LOG = LogFactory.getLog(DispacherView.class);

	private static Map<String, Object[]> actionCacheMap = new LinkedHashMap<String, Object[]>();

	protected DispacherView(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, ResultHtml html, Map<String, Object> map) {
		super(request, response, trans, html, map, false);
	}
	protected void run(Class fwClass, String fwMethodStr, Object fwAbstractObj) throws Exception {
		AbstractView view = (AbstractView)fwAbstractObj;
		view.prepare(request, response, trans, html, map);


		String part1 = StringUtils.getNumberOnly(view.getClass().getPackage().getName());
		html.setCode(part1);



		if (fwMethodStr.equals("doData")) {
			doCreateAbstractViewData(view);
		} else if (fwMethodStr.equals("doDataFilterField")) {
			doDataFilterField(view);
		} else if (fwMethodStr.equals("doFilterAba")) {
			doFilterByAba(view);
		} else if (fwMethodStr.equals("doFilterField")) {
			doFilterByField(view);
		} else if (fwMethodStr.equals(FWConstants.PARAM_FW_FILTER_CLEAN)) {

			// aqui vou limpar os filtros e voltar tudo ao padrao

			doFilterClean(view);

		} else {

			// aqui vamos criar os componentes da tela
			// TODO congelar o HTML para o usuario nao ter que carregar tudo toda vez
			// isso torna o sistema extremamente mais rapido
			// 
			doCreateAbstractViewScreen(view);
			doEstatistica(fwClass, userContext, trans);
		}
	}

	//
	//
	//
	private void doCreateAbstractViewData(AbstractView view) throws Exception {
		// isso sera ajax se Deus QuiZer!!!
		ResultHtml html = view.getResultHtml();
		html.setAjax();

		view.doExecute();
		view.doFilter();
		TransactionDB trans = view.getTransaction();
		List<String> attibuteList = view.getTable().getAttribute();

		String column = view.getRequest().getParameter("table[order][0][column]");
		String dir = view.getRequest().getParameter("table[order][0][dir]");
		String term = view.getRequest().getParameter("table[search][value]");


		StringBuilder sb = new StringBuilder();
		List<? extends Bean> model = null;
		Integer limit = null;
		try {
			Object[] objArray = view.doData();

			ORM orm = null;
			String sql = null;
			WhereDB where = null;
			for (int i = 0; i < objArray.length; i++) {
				Object obj = objArray[i];
				if (obj instanceof ORM) orm = (ORM) obj;
				if (obj instanceof String) sql = (String) obj;
				if (obj instanceof WhereDB) where = (WhereDB) obj;
				if (obj instanceof Integer) limit = ((Integer) obj) + 1;
			}

			if (orm == null)
				throw new DeveloperException("Não foi informado o ORM para executar na view \"" + view.getClass().getName() + "\".");
			if (sql == null) 
				throw new DeveloperException("Não foi informado o select para executar na view \"" + view.getClass().getName() + "\".");
			if (where == null) where = new WhereDB();
			if (limit == null) limit = 251;
			if (sql.contains(Bean.RNUM))
				throw new DeveloperException("Não pode inserir o apelido \"" + Bean.RNUM + "\" para as colunas no select da view \""+ view.getClass().getName() + "\".");

			boolean achouId = false;
			for (ORM o : orm.getList()) {
				if (o.getNameProperty().equalsIgnoreCase("id") == false) continue;
				achouId = true;
				break;
			}
			if (achouId == false)
				throw new DeveloperException("Sua view esta sem ID");

			//
			// ISSO AQUI È PARA DEIXAR MAIS INTELIGENTE
			// HOJE É ESCOLHIDO UM CAMPO PARA FILTRAR.. O SONHO É FILTRAR POR QUALQUER CAMPO
			//
			String filterProperty = view.getObjectSession(view.getNameSessionFilterAbaProperty());
			String filterValue = view.getObjectSession(view.getNameSessionFilterAbaValue());
			if ((StringUtils.hasValue( filterProperty) && (StringUtils.hasValue(filterValue)))) {
				
				if (filterProperty.equals(FWConstants.TAB_CESTA)) {
					
					List<Integer> idList = view.getObjectSession("cesta");
					if (idList != null && idList.size() > 0) {
						where.add("id", Condition.IN, idList.toArray(new Integer[idList.size()]));
					} else {
						where.add("id", Condition.EQUALS, -999999);
					}

				} else {
					where.add(filterProperty, Condition.EQUALS, filterValue);
				}
				
			} else {
				Bean b = view.getAba1();
				if (b != null) where.add((String)b.getAttribute("property"), Condition.EQUALS, b.getAttribute("value"));
			}


			//
			// filter advanced
			//
			List<Boolean> destinationField = view.getFilterDestination();
			List<AbstractFieldHtml> filterField = view.getFilterField();
			List<Condition> filterCondition = view.getFilterCondition();
			boolean foiUsadoOFiltroAvancado = false;
			if (filterField.size() > 0) {
				for (int i = 0; i < filterField.size(); i++) {
					Boolean dest = destinationField.get(i);
					AbstractFieldHtml x = filterField.get(i);
					Condition c = filterCondition.get(i);

					Object xx = (view.getObjectSession(view.getNameSessionFilterField(x.getName())));
					if (xx == null) continue;
					if ((xx instanceof String) && (StringUtils.hasValue((String)xx) == false)) continue;
					foiUsadoOFiltroAvancado = true;


					if (dest == false) continue;
					if (x instanceof DateRangeFieldHtml) {

						Date d0 = ((Date[])xx)[0];
						Date d1 = ((Date[])xx)[1];
						if (d0 != null) where.add(x.getName(), Condition.GREATOREQUALS, d0);
						if (d1 != null) where.add(x.getName(), Condition.LESSOREQUALS, d1);
					} else {

						String param = x.getName();
						if (x instanceof AutoCompleteFieldHtml) {
							param += "Id";
						}

						where.add(param, c, xx);
					}

				}
			}

			boolean filter = view.isDataClosed();
			if ((filter == true) && (foiUsadoOFiltroAvancado == false))
				throw new NotFoundException("");




			//			if (StringUtils.hasValue(term)) {
			//				for (int i = 0; i < orm.getList().size(); i++) {
			//					ORM oT = orm.getList().get(i);
			//					
			//					Operation xxx = WhereDB.Operation.OR;
			//					if (i == 0) {
			//						xxx = WhereDB.Operation.AND;
			//					}
			//					where.add(999, xxx, oT.getNameProperty(), Condition.LIKE, term);
			//				}
			//			}


			// isto aqui para usar o order da tabela...
			// so se o cara clicar em uma coluna
			OrderDB order = null;
			if (column != null) {
				order = new OrderDB();


				// esta conta é por que a tabela tem um checkbox agora!!!
				int temCheck = 0;
				if (view.getTable().isCheckbok() == true)
					temCheck = 1;

				int columnPosicao = NumberUtils.parseInt(column) - temCheck;

				String property = attibuteList.get(columnPosicao);
				ORM o = null;
				for (ORM oT : orm.getList()) {
					if (oT.getNameProperty().equalsIgnoreCase(property) == false) continue;
					o = oT;
					break;
				}
				OrderDB.Orientation x = OrderDB.Orientation.DESC;
				if (dir.equals("asc"))
					x = OrderDB.Orientation.ASC;
				order.add(o.getNameProperty(), x);
			}
			model = trans.select(Bean.class, orm, sql, where, order, 0, limit);
		} catch (NotFoundException nfe) {
			model = new ArrayList<Bean>();	
		} catch (Exception e) {
			LOG.error(view.getClass().getName() + ": " + e.getMessage(), e);
			model = new ArrayList<Bean>();	
		}


		String qtd = (limit == model.size() ? ""+(limit-1) + "+" : ""+model.size());

		String origem = "";
		if (PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_DEBUGCODE) == true) { 
			origem = "\"from\": \"" + view.getClass().getName() + ".doData\", ";
		}
		sb.append("{");
		sb.append( origem ); 
		sb.append("\"type\": \"success\","); 
		sb.append("\"sizeStr\": \""+ qtd +"\","); 
		sb.append("\"size\": "+ model.size() +","); 
		sb.append("\"time\": \""+ trans.getLastTimeRun() +"\","); 
		sb.append("\"data\": [");
		String temp = "";
		for (Bean bean : model) {
			bean.setAttributeToJson(attibuteList);

			sb.append( temp );
			sb.append( bean.toJson() );
			temp = "," + ResultHtml.PL;
		}
		sb.append("]" + ResultHtml.PL);
		sb.append("}" + ResultHtml.PL);

		html.addComponent( new Html( sb.toString() ));
	}
	private void doCreateAbstractViewScreen(AbstractView view) throws Exception {
		Help hAnn = (Help) view.getClass().getAnnotation(Help.class);
		
		List<Class<?>> classeList = new ArrayList<Class<?>>();
		if (view.isBasket()) {
			classeList.addAll( ServletHelper.getClassByPackageCache(request.getServletContext(), "br.com.dotum.core.component.basket" ) );
			view.basket(true);
		}

		classeList.addAll( ServletHelper.getClassByPackageCache(request.getServletContext(), view.getClass().getPackage().getName() ) );
		classeList.remove(view.getClass());
		view.addHelp(hAnn);

		// aqui vou colocar as 3 acoes padroes
		// para a cesta

		// AQUI é para montar a tela inicial 
		// AQUI é para montar a tela inicial 
		// AQUI é para montar a tela inicial 
		for (Class<?> classe : classeList) {
			Help helpAnn = (Help)classe.getAnnotation(Help.class);
			Menu m = (Menu)classe.getAnnotation(Menu.class);

			ShowWhenTab sw = (ShowWhenTab)classe.getAnnotation(ShowWhenTab.class);
			if (m.inMenu() != -1) continue;
			if ((m.publico() == false) && (temAcesso(classe, userContext) == false)) continue;
			
			Object[] obj = actionCacheMap.get(classe.getSimpleName());
			// para ativar o cache tem que se atentar a 2 coisas (por enquanto)
			// 1º o objeto TableHtml esta dentro do ViewAbstract logo o ID muda a cada instancia;
			// 2º a classe de acesso... nao pode ser colocado o view na sessao visto que vai armazenar os botoes junto
			//    com isso vai bagunçar a tarefa.. montando o html total e colocando na sessao 
			//    o cara que tem acesso a mais coisas nao vai exibir (ou vice e versa)
			//    por isso o null aqui em baixo ainda!!!
			obj = null;
			if (obj == null) {
				obj = new Object[4];
				AbstractAction action = (AbstractAction) classe.newInstance();
				action.setView(view);
				action.prepare(view.getRequest(), view.getResponse(), view.getTransaction(), view.getResultHtml(), view.getList());
				if (m.dynamic() == false) action.doScreen();
				boolean window = m.window();
				Integer windowSize = m.windowSize();

				//
				// CRIANDO AS TELAS DOS COMPOnENTES
				//
				String iconDescricao = m.icon().getDescricao(); 
				iconDescricao = iconDescricao.replaceAll(" white", "");
				if (m.selecteds() != Menu.ONE) {
					iconDescricao += " white";
					m.icon().setDescricao(iconDescricao);
				}

				WindowHtml win = null;
				ButtonHtml btn = new ButtonHtml( m.icon(), m.color(), null, m.label());
				if (m.selecteds() == Menu.ONE) {
					btn.addCssClass("btn-link ");
				}
				btn.setHtml(view.getResultHtml());

				if(StringUtils.hasValue(m.group())) {
					btn.setGroup(m.group());	
				}

				String part1 = StringUtils.getNumberOnly(classe.getPackage().getName());
				String part2 = StringUtils.getNumberOnly(classe.getSimpleName());
				String code = part1 + "." + part2;
				btn.addAttr("title", m.label() + " ["+code+"]");

				if ((action.getBodyCompList().size() > 0) || (m.dynamic() == true)) {
					win = new WindowHtml(m.label(), m.icon());
					win.setId(classe.getSimpleName());
					win.setHtml(view.getResultHtml());
					win.setWindow(window);
					win.setSize(windowSize);
					btn.setWindown(win);
					obj[3] = win;

					if (m.dynamic() == false) {
						for (ComponentHtml comp : action.getBodyCompList()) {
							win.addContent( comp );
						}					
					}
				}

				//
				// CRIANDO OS SCRIPTS DE CADA BOTAO
				//
				StringBuilder script = new StringBuilder();
				if (m.dynamic() == true) {
					// TODO isso aqui é por causa do rodrigo
					// que ficou dando azia
					// para colcoar uma tela dinamica!!!
					// com isso é possivel até mesmo fazer uma wizzard
					// com a action do caralho!!!

					// nao faça nada porra!!!
					// coloca a windows
					// me da um ID da WINDOWS
					// e um ajax para trazer a tela e aplicar atraves
					// do jquery na janela...
					// e ver o que vai dar depois
					if((m.selecteds() == Menu.ZERO) == false)
						script.append( view.getTable().createScriptDataRow() );

					Ajax ajax = new Ajax(action.getClass(), "doScreen");
					ajax.addProperty("dataType", "json");
					if(m.selecteds() == Menu.ONE)
						ajax.addParamJs("id", "_id");
					// nao funciona aqui...
					// uma vez que o componete nao foi criado em tela...
					// logo o ID do futuro é diferente do ID do presente
					// infelizmente nao inventaram a maquina do tempo...
					// para saber o que vai ser de ID no futuro...
					// se souber da para usar a linha abaixo...
					// sb.append( createScriptPopulateComponentes(view, action, win) );
					ajax.addSuccess( win.createScriptAbreWindow() );
					ajax.addSuccess( "$('#"+ win.getId() +"').find('div.widget-main').html(data.html);");

					script.append( ajax.getComponent() );

				} else if (win == null) {

					String methodName = null;
					try {
						action.getClass().getDeclaredMethod("doDownload", Integer.class);
						methodName = "doDownload";
					} catch (NoSuchMethodException e) {
						try {
							action.getClass().getDeclaredMethod("doProcess", Integer.class);
							methodName = "doProcess";
						} catch (NoSuchMethodException e2) {
							LOG.error("Não foi adicionado a ação \"" + m.label() + "\" por que nao foi possivel identicar se é um processo ou um download");
							continue;
						}
					}

					// criando o script de download
					Ajax ajax = new Ajax(action.getClass(), methodName);

					if (m.selecteds() == Menu.ZERO) {
						// aqui eu nao passo parametro
					} else {
						// aqui serve para pegar o ID da tabela principal _fwmId
						script.append( view.getTable().createScriptDataRow() );

						// estou passando dois parametros para poder usar o getParamInteger("id") e pegar tbm no parametro do metodo (assinatura do metodo) 
						ajax.addParamJs("id", "_id");
						ajax.addParamJs(FWConstants.PARAM_FW_MASTER_ID, "_id");
					}

					if (methodName.equals("doDownload")) {
						ajax.setSuccessOrWarningMessage(  ScriptHelper.createScriptDownladAjax() );
					} else {
						ajax.setMessage(true);
					}
					script.append(ajax.getComponent());


				} else if (m.selecteds() == Menu.ZERO) {
					Ajax ajax = new Ajax(action.getClass(), "doData");
					ajax.addProperty("type", "GET");
					// infelizmente precisa ser async == false (é mais lento)
					// por que preciso buscar os dados de todos os componentes antes de abrir a janela
					// visto que algum deles pode ser que bloqueie a abertura da janela por alguma restricao
					ajax.addProperty("async", "false");

					ajax.addSuccess( createScriptCleanComponentes(view, action, win) );
					ajax.addSuccess( createScriptPopulateComponentes(view, action, win) );
					ajax.addSuccess( win.createScriptAbreWindow() );
					script.append( ajax.getComponent() );

				} else if ((m.selecteds() == Menu.ONE) || (m.selecteds() == Menu.MANY)) {
					// id da tabela principal na variavel _id do javascript;


					if (m.selecteds() == Menu.ONE) {
						script.append( view.getTable().createScriptDataRow() );
					} else {
						script.append( view.getTable().createScriptDataRow(m.selecteds()) );
					}

					// para cada componente da tela irá ocorrer uma requisição paralela..
					// pois eles sao componente HTML com JAVASCRIPT autonomos;
					// teoricamente so pode abrir a janela se todos eles estiverem resultado POSITIVO
					// assim como eh o script quando nao seleciona nenhum;
					script.append( createScriptPopulateComponentes(view, action, win) );

				}
				
				// aqui vou criar um evento que sera invocado quando fechar a windows de qualquer forma!!!
				// e existir o metodo que precisa ser chamado... com uma annotation especifica
				Method met = ClassUtils.getMethodNameByAnnotation(classe, OnCloseWindow.class);
				if (met != null) {
					Ajax ajax = new Ajax(classe, met.getName());
					ajax.addParamJs(FWConstants.PARAM_FW_MASTER_ID, "_id");
					
					win.setScriptOnClose(ajax.getComponent());
				}

				if (m.selecteds() == Menu.ZERO) {
					if (window == true)  
						obj[0] = btn;
				} else if (m.selecteds() == Menu.ONE) {
					obj[1] = btn;
					if ((sw != null) && StringUtils.hasValue(sw.toString())) {
						btn.addAttr("data-showwhentab", sw.value());
					}
				}
				if (m.selecteds() == Menu.MANY) {
					obj[0] = btn;
					if ((sw != null) && StringUtils.hasValue(sw.toString())) {
						btn.addAttr("data-showwhentab", sw.value());
					}
				}
				btn.onClick(script.toString());

				obj[2] = helpAnn;
				actionCacheMap.put(classe.getSimpleName(), obj);


				if ((win != null) && (view.getTable() != null)) {

					String scriptReloadTableMain = view.getTable().createScriptPopulateByJson();
					// estou trocando o momento do evento que será invocado..
					// nao quando interage em um form e sim quando fecha a window
					// com isso o fw irá reduzir o numero de requisições desnecessarias

					// TODO observar isso na table tbm 
					// e em telas que o window na ann é false
					win.setScriptOnClose( scriptReloadTableMain );


					
				}
				if (win != null)
					html.addScript(win.xxx());
			}

			if (obj[0] != null) view.addActionToolbar((ButtonHtml)obj[0]);
			if (obj[1] != null) view.addActionTable((ButtonHtml)obj[1]);
			if (obj[2] != null) view.addHelp(m, (Help)obj[2]);
			if (obj[3] != null) view.addComponente((WindowHtml)obj[3]);

		}


		view.doPreExecute();
		view.doExecute();
		view.doFilter();
		view.doPosExecute();

	}

	private void doDataFilterField(AbstractView view) throws Exception {
		view.doExecute();
		view.doFilter();

		List<Boolean> destinationField = view.getFilterDestination();
		List<Condition> cList = view.getFilterCondition();
		List<AbstractFieldHtml> fieldList = view.getFilterField();

		Bean b = new Bean();
		for (int i = 0; i < fieldList.size(); i++) {
			AbstractFieldHtml field = fieldList.get(i);
			Condition c = cList.get(i);
			Boolean dest = destinationField.get(i);

			Object xx = (view.getObjectSession(view.getNameSessionFilterField(field.getName())));
			if ((xx instanceof String) && (StringUtils.hasValue((String)xx) == false)) continue;

			//			if (dest == false) continue;
			if (field instanceof DateRangeFieldHtml) {

				b.set(field.getName(), ((Date[])xx));

			} else {

				String param = field.getName();
				if (field instanceof AutoCompleteFieldHtml) {
					AutoCompleteFieldHtml f = (AutoCompleteFieldHtml) field;
					f.setValue(xx);
					f.prepareValues();
					b.set(f.getNamePog(), f.getValueToHtml());
					param += "id";
				}
				b.set(param, xx);
			}

		}
		StringBuilder sb = new StringBuilder();
		sb.append("{\"type\": \"success\", \"size\": 1, \"data\": ");
		sb.append(b.toJson());
		sb.append("}");

		ResultHtml html = view.getResultHtml();
		html.setAjax();
		html.addComponent( new Html(sb.toString()) );
	}

	//
	// FILTER
	//
	private void doFilterByAba(AbstractView view) {
		String filterAbaProperty = view.getRequest().getParameter(FWConstants.PARAM_FW_FILTER_ABA_PROPERTY);
		String filterAbaValue = view.getRequest().getParameter(FWConstants.PARAM_FW_FILTER_ABA_VALUE);
		view.setObjectSession(view.getNameSessionFilterAbaProperty(), filterAbaProperty);
		view.setObjectSession(view.getNameSessionFilterAbaValue(), filterAbaValue);

		StringBuilder sb = new StringBuilder();

		String origem = "";
		if (PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_DEBUGCODE) == true) { 
			origem = "\"from\": \"" + view.getClass().getName() + ".doFilterAba\", ";
		}
		sb.append("{");
		sb.append( origem );
		sb.append("\"type\": \"success\"");
		sb.append("}");

		ResultHtml html = view.getResultHtml();
		html.setAjax();
		html.addComponent( new Html(sb.toString()) );


	}
	private void doFilterByField(AbstractView view) throws Exception {
		view.doExecute();
		view.doFilter();

		List<Condition> cList = view.getFilterCondition();
		List<AbstractFieldHtml> fieldList = view.getFilterField();
		for (int i = 0; i < fieldList.size(); i++) {
			AbstractFieldHtml field = fieldList.get(i);
			Condition c = cList.get(i);

			boolean array = (c.equals(Condition.IN) || c.equals(Condition.NOTIN));

			String param = field.getName();
			if (field instanceof AutoCompleteFieldHtml) {
				param += "id";
			}

			Object obj = ServletHelper.getParameter(request, map, param, array);
			Object value = FieldFactory.parseValue(field, obj);

			view.setObjectSession(view.getNameSessionFilterField(field.getName()), value);
			view.setObjectSession(view.getNameSessionFilterField(param), value);
		}

		ResultHtml html = view.getResultHtml();
		html.setAjax();
		html.addComponent( new Html(AbstractDispacher.createMessage(MessageHtml.Type.SUCCESS, "Filtrado")) );
	}
	private void doFilterClean(AbstractView view) throws Exception {
		view.doExecute();
		view.doFilter();

		List<Condition> cList = view.getFilterCondition();
		List<AbstractFieldHtml> fieldList = view.getFilterField();
		for (int i = 0; i < fieldList.size(); i++) {
			AbstractFieldHtml field = fieldList.get(i);
			Condition c = cList.get(i);
			Object obj = field.getValue();
			String param = field.getName();


			view.setObjectSession(view.getNameSessionFilterField(field.getName()), obj);
		}

		html.setAjax();
		html.addComponent( new Html(AbstractDispacher.createMessage(MessageHtml.Type.SUCCESS, "Limpado o filtro com sucesso.")) );
	}

	private String createScriptCleanComponentes(AbstractView view, AbstractAction action, WindowHtml win) {
		StringBuilder script = new StringBuilder();


		for (ComponentHtml comp : action.getBodyCompList()) {
			if (comp instanceof ColumnHtml) {
				ColumnHtml column = (ColumnHtml)comp;
				List<ComponentHtml> cList = column.getComponentList();

				for (ComponentHtml c : cList) {
					script.append( createScriptClearByComponent(view, action, c, win) );
				}
			} else if (comp instanceof TabHtml) {
				TabHtml tab = (TabHtml)comp;
				List<ComponentHtml> cList = tab.getComponentList();

				for (ComponentHtml c : cList) {
					script.append( createScriptClearByComponent(view, action, c, win) );
				}
			} else {
				script.append( createScriptClearByComponent(view, action, comp, win) );
			}
		}

		return script.toString();
	}
	private String createScriptClearByComponent(AbstractView view, AbstractAction action, ComponentHtml comp, WindowHtml win) {
		StringBuilder script = new StringBuilder();

		// mudar isso aqui
		if (comp instanceof FormHtml){
			FormHtml form = (FormHtml)comp;
			//			if (view != null) {
			//				String scriptReloadTableMain = view.getTable().createScriptPopulateByJson();
			//				
			//				// estou trocando o momento do evento que será invocado..
			//				// nao quando interage em um form e sim quando fecha a window
			//				// com isso o fw irá reduzir o numero de requisições desnecessarias
			//				
			//				// TODO observar isso na table tbm 
			//				// e em telas que o window na ann é false
			//				win.setScriptOnClose( scriptReloadTableMain );
			//				form.setScriptSuccess( scriptReloadTableMain );
			//			}
			script.append( form.createScriptClean() );							
		} else if (comp instanceof TableHtml){
			TableHtml table = (TableHtml)comp;
			script.append( table.createScriptPopulateByJson() );

		}

		return script.toString();
	}

}


