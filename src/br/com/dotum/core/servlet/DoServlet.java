package br.com.dotum.core.servlet;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.basic999.Controller999Exit;
import br.com.dotum.core.component.AbstractAction;
import br.com.dotum.core.component.AbstractCommon;
import br.com.dotum.core.component.AbstractController;
import br.com.dotum.core.component.AbstractReport;
import br.com.dotum.core.component.AbstractView;
import br.com.dotum.core.component.GlobalView;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.other.ResultHtml;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumReportInfo;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.LoginException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

@WebServlet("/" + FWConstants.URL_DO)
public class DoServlet extends HttpServlet {
	private static Log LOG = LogFactory.getLog(DoServlet.class);

	private boolean POST = true;
	static final long expires = DateUtils.getCurrentTimeMillis() + 2592000000L; // 30 dias
	private static final long serialVersionUID = 971113289746116394L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		POST = false;
		doExecute(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		doExecute(request, response);
	}
	protected void doExecute(HttpServletRequest request, HttpServletResponse response) {
		TransactionDB trans = null;
		UserContext userContext = null;
		try {
			
			trans = (TransactionDB) request.getAttribute("trans");
			userContext = (UserContext) request.getAttribute("userContext");
			Map<String, Object> map = (Map<String, Object>)request.getAttribute("map");
			Class fwClass = (Class)request.getAttribute("fwClass");
			String fwMethodStr = (String)request.getAttribute("fwMethodStr");
			boolean api = (boolean)request.getAttribute("capital/api");
			
			if (userContext != null) {
				userContext.setDataLastInterationAt(trans.getDataAtual());
				userContext.setHoraLastInterationAt(trans.getHoraAtual());
				userContext.getUser().setCurrentTask(fwClass);
			}
			
			ApplicationContext application = new ApplicationContext();
			application.prepare(request, response, fwClass);
			ResultHtml html = new ResultHtml(application);
			Object fwAbstractObj = fwClass.newInstance();
			if (fwAbstractObj instanceof AbstractView) {
				DispacherView d = new DispacherView(request, response, trans, html, map);
				d.run(fwClass, fwMethodStr, fwAbstractObj);
			} else if (fwAbstractObj instanceof AbstractAction) {
				((AbstractCommon)fwAbstractObj).setApi(api);
				DispacherAction d = new DispacherAction(request, response, trans, html, map);
				d.run(fwClass, fwMethodStr, fwAbstractObj);
			} else if (fwAbstractObj instanceof AbstractController) {
				((AbstractCommon)fwAbstractObj).setApi(api);
				DispacherController d = new DispacherController(request, response, trans, html, map);
				d.run(fwClass, fwMethodStr, fwAbstractObj);
			} else if (fwAbstractObj instanceof AbstractReport) {
				DispacherReport d = new DispacherReport(request, response, trans, html, map);
				d.run(fwClass, fwMethodStr, fwAbstractObj);
			}

			DispacherGlobalAction d1 = new DispacherGlobalAction(request, response, trans, html, map);
			d1.run(fwClass, fwMethodStr, fwAbstractObj);

			DispacherTutorial d2 = new DispacherTutorial(request, response, trans, html, map);
			d2.run(fwClass, fwMethodStr, fwAbstractObj);

			try {
				Menu m = (Menu)fwClass.getDeclaredAnnotation(Menu.class);
				createScriptTimeOutSession(userContext, html, m.publico());

				if (html.isAjax()) ServletHelper.setCharSetJson(request, response);
				
				
				String jwtstr = ServletHelper.geraToken(-1, -1);
				if (userContext != null) {
					// seta o token na response
					jwtstr = ServletHelper.geraToken(userContext.getUnidade().getId(), userContext.getUser().getId());
				}
				response.setHeader(FWConstants.PARAM_FW_TOKEN_AUTHORIZATION, FWConstants.PARAM_FW_TOKEN_TYPE + jwtstr);
				
				
				PrintWriter out = response.getWriter();
				out.print(html.getComponente());
			} catch (IllegalStateException e){
				// aqui é quando a acao envia um relatorio para o cliente...!!!
				// nao tem problema nao exibir esse aqui
			}

			if (trans != null)
				trans.commit();

			//			DateUtils.diffTime("Processo ["+fwClassStr + "." + fwMethodStr+"]: ", data);
		} catch (LoginException e) {
			LOG.warn(userContext, e.getLocalizedMessage(), e);
			try {
				if (trans != null)
					trans.rollback();
				PrintWriter out = response.getWriter();
				response.addHeader("LOGIN", "LOGIN");


				String xxx = PropertiesUtils.getString(PropertiesConstants.SYSTEM_LINK_LOGIN);
				if (POST) {
					String json = "{\"type\": \"timeout\", \"location\": \""+ xxx +"\"}";
					out.print( json );
				} else {
					response.sendRedirect( xxx );
				}
			} catch (Exception e1) {
				LOG.error(userContext, e.getMessage(), e);
			}
		} catch (Exception e) {
			LOG.error(userContext, e.getMessage(), e);

			if (e.getCause() instanceof WarningException) {

				System.out.println( request.getContextPath() );

			} else {
				try {
					if (trans != null)
						trans.rollback();
					PrintWriter out = response.getWriter();
					out.print( GlobalView.criarHtmlRedirecionarLogin() );
				} catch (Exception e1) {
					LOG.error(userContext, e.getMessage(), e);
				}
			}
		} finally {
			try {
				if (trans != null)
					trans.close();
			} catch (Exception e) {
				LOG.error(userContext, e.getMessage(), e);
			}
		}
	}

	private void createScriptTimeOutSession(UserContext userContext, ResultHtml html, boolean publico) throws DeveloperException {
		if ((publico == false) && (userContext != null)) {

			UrlHtml url = new UrlHtml();
			url.setAction(Controller999Exit.class);
			String link = url.getComponente();

			String timeoutStr = PropertiesUtils.getString(PropertiesConstants.SYSTEM_TIMEOUT);
			if (StringUtils.hasValue(timeoutStr) == false) timeoutStr = "20";
			Integer timeout = Integer.parseInt(timeoutStr);
			Integer timeAlerta = null;
			if (timeout < 2) {
				timeout = 2;
				timeAlerta = 1;
			} else {
				timeAlerta = timeout - 2;
			}

			StringBuilder timeOutSb = new StringBuilder();
			timeOutSb.append("$.sessionTimeout({");
			timeOutSb.append("logoutUrl: '"+ link +"',");
			timeOutSb.append("redirUrl: '"+ link +"',");
			timeOutSb.append("onStart: true,");
			timeOutSb.append("title: 'Sua sessão está prestes a expirar',");
			timeOutSb.append("message: '',");
			timeOutSb.append("logoutButton: 'Sair',");


			timeOutSb.append("keepAliveUrl: '"+ FWConstants.URL_DO +"',");
			timeOutSb.append("keepAlive: false,");
			timeOutSb.append("keepAliveButton: 'Manter conectado',");

			timeOutSb.append("countdownMessage: 'Saindo do sistema em {timer}.',");
			timeOutSb.append("warnAfter: "+ ((timeAlerta*1000)*60) +",");
			timeOutSb.append("redirAfter: "+ ((timeout*1000)*60) );
			timeOutSb.append("});");

			html.addScript( timeOutSb.toString() );
		}		
	}
	public static void exportReportDSR(HttpServletResponse response, TransactionDB trans, UserContext userContext, DotumSmartReport report, HashMap<String, Object> parms, String type) throws Exception {
		exportReportDSR(response, trans, userContext, report, parms, type, null);
	}
	public static void exportReportDSR(HttpServletResponse response, TransactionDB trans, UserContext userContext, DotumSmartReport report, Map<String, Object> parms, String type, String nameFile) throws Exception {
		if (nameFile == null)
			nameFile = "relatorio";

		// pega o nome da classe do relatorio...

		if (type != null) report.setType(type);

		DotumSmartReportBuilder reportBuilder = new DotumSmartReportBuilder(trans);
		reportBuilder.setUserContext(userContext);

		String codigo = "";

		DotumReportInfo dri = new DotumReportInfo(codigo.toUpperCase(), "Dotum", report.getVersion());
		reportBuilder.setReportInfo(dri);

		// mapa de parametros
		HashMap<String, String> parmsStr = new HashMap<String, String>();

		reportBuilder.setParmsValues( parms );
		reportBuilder.setParmsStr( parmsStr );
		LOG.debug("Usuario:"+userContext.getUser().getLogin()+". Componente:"+codigo);
		LOG.debug("Filtros:");

		reportBuilder.builder(codigo, report);

		if (report.getType().equalsIgnoreCase("XLS")) {
			response.setHeader("Content-disposition","attachment; filename=\""+ nameFile +".xls\"");
		} else {
			response.setContentType("application/pdf");
			response.setHeader("Content-disposition","attachment; filename=\""+ nameFile +".pdf\"");
		}
		FileMemory fm = reportBuilder.getFileMemory();

		OutputStream ops = response.getOutputStream();
		ops.write( fm.getBytes() );
		ops.flush();
		ops.close();
	}

	public static void exportFileMemory(HttpServletResponse response, UserContext userContext, FileMemory fm) throws DeveloperException {
		try {
			response.setHeader("Content-Disposition", "attachment;filename=\"" + fm.getName() + "\"");
			response.setContentLength(fm.getBytes().length);
			response.setDateHeader("Expires", expires);
			response.setContentType("application/octet-stream;charset=UTF-8");

			ServletOutputStream out = response.getOutputStream();
			out.write(fm.getBytes());
			out.flush();
			out.close();

		} catch (Exception e) {
			LOG.error(userContext, "Ocorreu um erro ao enviar um arquivo ao solicitante. ("+ fm.getName() +")", e);
		}
	}

}

