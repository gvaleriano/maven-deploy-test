package br.com.dotum.core.servlet;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.protocol.FTPService;
import br.com.dotum.jedi.util.PropertiesUtils;

@WebServlet("/file")
public class FileServlet extends HttpServlet {
	private static Log LOG =  LogFactory.getLog(FileServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		doExecute(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		doExecute(request, response);
	}

	protected void doExecute(HttpServletRequest request, HttpServletResponse response) {
		TransactionDB trans = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String anexoIdStr = request.getParameter("anexoId");
			if (anexoIdStr == null) anexoIdStr = request.getParameter("key");
			Integer anexoId = Integer.parseInt(anexoIdStr);

			trans = TransactionDB.getInstance(null);

			StringBuilder sb = new StringBuilder();
			sb.append("          select ane.ane_id,                       "); 
			sb.append("                 ane.ane_cadastro,                 "); 
			sb.append("                 ane.ane_nome,                     "); 
			sb.append("                 ane.ane_extensao,                 ");
			sb.append("                 ane.ane_mime,                     ");
			sb.append("                 anet.anet_id,                     ");  
			sb.append("                 anet.anet_descricao,              ");
			sb.append("                 anet.anet_diretorio               ");
			sb.append("            from anexo ane,                        ");
			sb.append("                 anexotipo anet                    ");
			sb.append("           where ane.anet_id = anet.anet_id        ");
			sb.append("             and ane.ane_id = ?                    ");

			ps = trans.prepareStatement(sb.toString());
			ps.setInt(1, anexoId);
			rs = ps.executeQuery();

			Bean b = new Bean();
			while (rs.next()) {

				b.set("aneId", rs.getInt("ane_id"));
				b.set("aneCadastro", rs.getDate("ane_cadastro"));
				b.set("aneNome", rs.getString("ane_nome"));
				b.set("aneExtensao", rs.getString("ane_extensao"));
				b.set("aneMime", rs.getString("ane_mime"));

				b.set("anetId", rs.getInt("anet_id"));
				b.set("anetDescricao", rs.getString("anet_descricao"));
				b.set("anetDiretorio", rs.getString("anet_diretorio"));
			}
			rs.close();
			rs = null;
			ps.close();
			ps = null;


			String server = PropertiesUtils.getString("ftp.server");
			String username = PropertiesUtils.getString("ftp.username");
			String password = PropertiesUtils.getString("ftp.password");

			FTPService ftp = new FTPService(server, username, password);
			ftp.connect();
			ftp.changeDirectory((String)b.get("anetDiretorio"));


			String mime = (String)b.get("aneMime");
			String nameSource = b.get("aneId") + "." + b.get("aneExtensao");
			String nameTarget = b.get("aneNome") + "." + b.get("aneExtensao");

			response.setHeader("Content-Transfer-Encoding", "binary");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + nameTarget + "\"");
			response.setContentType(mime);

			ServletOutputStream out = response.getOutputStream();
			ftp.download(nameSource, out);
			ftp.disconnect();
			out.flush();
			out.close();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {

			if(rs != null){

				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				rs = null;	
			}

			if(ps != null){
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ps = null;	
			}



			try {
				trans.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}


