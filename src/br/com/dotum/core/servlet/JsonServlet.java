package br.com.dotum.core.servlet;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.other.MessageHtml;
import br.com.dotum.jedi.core.db.AbstractJson;
import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.ORM;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.db.WhereDB.Operation;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;

@WebServlet("/" + FWConstants.URL_JSON)
public class JsonServlet extends HttpServlet {
	private static Log LOG = LogFactory.getLog(JsonServlet.class);

	private static final long serialVersionUID = -8369790416647619160L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		doExecute(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		doExecute(request, response);
	}

	protected void doExecute(HttpServletRequest request, HttpServletResponse response) {
		try {
//			ServletHelper.setCharSetHtml(request, response);

			HashMap<String, Object> list = ServletHelper.getList2(request);
			String beanClassStr = (String)ServletHelper.getParameter(request, list, FWConstants.PARAM_BEAN_CLASS, false);
			String jsonClassStr = (String)ServletHelper.getParameter(request, list, FWConstants.PARAM_JSON_CLASS, false);

			Boolean tag = false;
			Boolean hasSameValueDB = false;
			String name = (String)ServletHelper.getParameter(request, list, "name", false);
			String tagStr = (String)ServletHelper.getParameter(request, list, "tag", false);
			if (tagStr != null) tag = true;
			String hasSameValueDBStr = (String)ServletHelper.getParameter(request, list, FWConstants.PARAM_HAS_SAME_VALUE_DB, false);
			if (hasSameValueDBStr != null) hasSameValueDB = true;
			String filterBy = (String)ServletHelper.getParameter(request, list, "filterBy", false);
			if (StringUtils.hasValue(filterBy) == false) filterBy = "descricao";
			String filter = (String)ServletHelper.getParameter(request, list, "filter", false);
			String term = (String)ServletHelper.getParameter(request, list, FWConstants.PARAM_TERM, false);
			term = StringUtils.removeAccent(term);
			String key = (String)ServletHelper.getParameter(request, list, "key", false);
			if (StringUtils.hasValue(key) == false) key = "id";
			String displayStr = (String)ServletHelper.getParameter(request, list, FWConstants.PARAM_DISPLAY, false);
			if (StringUtils.hasValue(displayStr) == false) displayStr = "descricao";
			final String[] display = displayStr.split(",");

			String result = null;
			if (tag == true) {

				AbstractJson json = doJsonTag(request, response, beanClassStr, display);
				result = doJsonClass(request, response, json, filterBy, filter, term, key, display, name);

			} else if (hasSameValueDB == true) {

				result = doHasSameValueDB(request, response, beanClassStr, display, term);

			} else if (beanClassStr != null) {

				result = doBeanClass(beanClassStr, filterBy, filter, term, key, display);

			} else if (jsonClassStr != null) {

				Class<? extends AbstractJson> jsonClass = (Class<? extends AbstractJson>)Class.forName(jsonClassStr);
				AbstractJson json = jsonClass.newInstance();
				result = doJsonClass(request, response, json, filterBy, filter, term, key, display, name);

			}

			ServletHelper.setCharSetJson(request, response);
			PrintWriter out = response.getWriter();
			out.print(result);		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private AbstractJson doJsonTag(HttpServletRequest request, HttpServletResponse response, String beanClassStr, String[] display) throws ClassNotFoundException {
		Class<? extends Bean> beanClass = (Class<? extends Bean>)Class.forName(beanClassStr);
		TableDB table = beanClass.getDeclaredAnnotation(TableDB.class);
		String tableName = table.name();


		String colTemp = null;
		for (Field field : beanClass.getDeclaredFields()) {
			FieldDB fielAnn = field.getDeclaredAnnotation(FieldDB.class);
			ColumnDB columnAnn = field.getDeclaredAnnotation(ColumnDB.class);
			if (fielAnn == null) continue;
			if (fielAnn.name().equalsIgnoreCase(display[0]) == false) continue;

			colTemp = columnAnn.name();
			break;
		}

		final String columnDB = colTemp;

		AbstractJson json = new AbstractJson(){
			public Object[] doExecute() throws Exception {
				UserContext userContext = super.getUserContext();

				StringBuilder sb = new StringBuilder();




				sb.append("           select tb1.tag  													       			");
				sb.append("             from (select distinct trim(regexp_substr("+ columnDB +",'[^,]+', 1, level)) as tag  	");
				sb.append("                     from "+ tableName +"  													");
				//sb.append("                    where uni_id = "+ userContext.getUnidade().getId() +" 					");
				sb.append("               connect by regexp_substr("+ columnDB +", '[^,]+', 1, level) is not null) tb1 		");
				sb.append("            where tb1.tag is not null 														");



				ORM orm = new ORM();
				orm.add(String.class,   "tag",             "tag");

				return new Object[]{Bean.class, orm, sb.toString()};
			}
		};

		return json;
	}
	private String doHasSameValueDB(HttpServletRequest request, HttpServletResponse response, String beanClassStr, String[] display, String term) throws Exception {
		Class<? extends Bean> beanClass = (Class<? extends Bean>)Class.forName(beanClassStr);
		TableDB table = beanClass.getDeclaredAnnotation(TableDB.class);
		String tableName = table.name();


		String colTemp = null;
		for (Field field : beanClass.getDeclaredFields()) {
			FieldDB fielAnn = field.getDeclaredAnnotation(FieldDB.class);
			ColumnDB columnAnn = field.getDeclaredAnnotation(ColumnDB.class);
			if (fielAnn == null) continue;
			if (fielAnn.name().equalsIgnoreCase(display[0]) == false) continue;

			colTemp = columnAnn.name();
			break;
		}


		boolean jaExiste = false;
		try {
			if (StringUtils.hasValue(term)) {
				TransactionDB trans = TransactionDB.getInstance(null);
				WhereDB where = new WhereDB();
				where.add(display[0], Condition.EQUALS, term);
				trans.select(beanClass, where);

				jaExiste = true;
			}
		} catch (NotFoundException e) {
		}

		if (jaExiste == true) {
			return AbstractDispacher.createMessage(MessageHtml.Type.WARNING, "Já tem um cadastrado com esta informação");
		} else {
			return AbstractDispacher.createMessage(MessageHtml.Type.SUCCESS, "Tudo certo");
		}
	}

	private String doJsonClass(HttpServletRequest request, HttpServletResponse response, AbstractJson json, String filterBy, String filter, String term, String key, String[] display, String name) throws Exception {
		TransactionDB trans = TransactionDB.getInstance(null);
		UserContext userContext = ServletHelper.getUsuarioSession(request);

		json.addExtra("name", name);
		json.setTransaction(trans);
		json.addFilter(filter);
		json.setUserContext(userContext);
		json.setProperty(key, display);

		json.setTerm(term);
		List<Bean> model = null;
		try {
			model = json.run(filterBy);
		} catch (NotFoundException e) {
			model = new ArrayList<Bean>();
		}

		json.addAll(model);
		trans.close();

		return json.getJson();
	}

	private String doBeanClass(String beanClassStr, String filterBy, String filter, String term, String key, String[] display) throws Exception {
		Class<? extends Bean> beanClass = (Class<? extends Bean>)Class.forName(beanClassStr);

		WhereDB where = new WhereDB();
		addFilter(beanClass, where, filter);
		String[] filterByParts = filterBy.split("#");

		Operation ope = Operation.AND;
		Integer group = 10;
		for (int i = 0; i < filterByParts.length; i++) {
			String filterByPart = filterByParts[i];
			String[] part = filterByPart.split(";");

			Field field = getField(beanClass, part[0]);
			if (((field != null) && (field.getType().equals(String.class)) 
					|| (part[1].contains("like"))) 
					&& (term == null))
				term = "%";

			if (i > 0) ope = Operation.OR;
			where.add(group, ope, part[0], Condition.getConditionByStr(part[1]), term.replaceAll(" ", "%"));
		}

		TransactionDB trans = null;
		try{
			trans = TransactionDB.getInstance(null);
			final List<Bean> list = trans.select(beanClass, where, null, 0, 20);
			AbstractJson j = new AbstractJson() {
				public Object[] doExecute() {
					return null;
				}
			};
			j.addAll(list);
			j.setProperty(key, display);
			j.doExecute();
			return j.getJson();
		} catch (NotFoundException e) {
			final Bean b = new Bean();
			b.setAttribute(key, null);
			b.setAttribute(display[0], "Nada encontrato com \"" + term + "\".");

			AbstractJson j = new AbstractJson() {
				public Object[] doExecute() {
					return null;
				}
			};
			j.setProperty(key, display);
			j.doExecute();
			return j.getJson();

		} catch (Exception e) {
			throw e;
		} finally {
			trans.close();
		}
	}

	private void addFilter(Class<? extends Bean> beanClass, WhereDB where, String filter) throws Exception {
		if (StringUtils.hasValue(filter)) {
			String[] filterPart = filter.split("#");
			for (String parts : filterPart) {
				String[] part = parts.split(";");

				if (part.length < 3)
					throw new DeveloperException("Não chegou os parametros devido para o filtro \""+ parts +"\" do campo referente ao Bean " + beanClass.getName());

				Condition c = Condition.getConditionByStr(part[1]);
				boolean array = (c.equals(Condition.IN) || c.equals(Condition.NOTIN));
				
				Object value = parseObject(beanClass, part[0], part[2], array);
				where.add(part[0], c, value);
			}
		}
	}

	private Field getField(Class<? extends Bean> beanClass, String attribute) {
		Field[] fieldArray = beanClass.getDeclaredFields();
		Field field = null;
		for (Field fieldTemp : fieldArray) {
			FieldDB ann = fieldTemp.getAnnotation(FieldDB.class);
			if (ann == null) continue;
			if ((ann.name().equalsIgnoreCase(attribute) == false) && (fieldTemp.getName().equalsIgnoreCase(attribute) == false)) continue; 

			field = fieldTemp;
			break;
		}

		return field;
	}

	private Object parseObject(Class<? extends Bean> beanClass, String attribute, String valueStr, boolean array) throws Exception {
		Field field = getField(beanClass, attribute);

		if (field.getType().equals(Integer.class)) {
			if (array == true) {
				return ArrayUtils.parseToInt(valueStr.split(","));
			} else {
				return Integer.parseInt(valueStr);
			}
		} else if (field.getType().equals(Double.class)) {
			return Double.parseDouble(valueStr);
		} else if (field.getType().equals(Date.class)) {
			return FormatUtils.parseDate(valueStr);
		} else if (field.getType().getSuperclass().equals(Bean.class)) {
			if (array == true) {
				return ArrayUtils.parseToInt(valueStr.split(","));
			} else {
				return Integer.parseInt(valueStr);
			}
		} else if (field.getType().equals(String.class)) {
			return valueStr;
		}

		return null;
	}
}

