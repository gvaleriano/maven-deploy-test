package br.com.dotum.core.servlet;

import java.io.IOException;
import java.sql.SQLRecoverableException;
import java.util.Map;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import br.com.dotum.core.basic999.Controller403Forbidden;
import br.com.dotum.core.basic999.Controller404Error;
import br.com.dotum.core.basic999.Controller500Error;
import br.com.dotum.core.basic999.Controller900Report;
import br.com.dotum.core.component.Menu;
import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.core.html.other.UrlHtml;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.security.AbstractSecurityManager;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import io.jsonwebtoken.Claims;

@WebFilter(dispatcherTypes={DispatcherType.REQUEST}, urlPatterns={"/" + FWConstants.URL_DO, "/capital/api/*"})
public final class SecurityFilter implements Filter {
	private static Log LOG = LogFactory.getLog(SecurityFilter.class);

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)res;

		ServletHelper.setCharSet((HttpServletRequest)request, (HttpServletResponse)response);
		ServletHelper.setTimeOutSession(request);
		HttpServletRequestWrapper wrapper = new HttpServletRequestWrapper((HttpServletRequest) request);

		try {
			String url = wrapper.getRequestURL().toString();

			String[] apiPartArray = null;
			if (url.contains("/capital/api/")) {
				String urlTemp = url.split("/api/")[1];
				apiPartArray = urlTemp.split("/");
			}
			boolean api = apiPartArray != null;
			String preFixPackage = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
			if (preFixPackage == null) preFixPackage = "dotum";
			Map<String, Object> map = ServletHelper.getList2(request);


			// verificar se tem o token na requisição
			String token = request.getHeader(FWConstants.PARAM_FW_TOKEN_AUTHORIZATION);


			//
			// pega qual classe/xml e qual metodo devo ir
			//
			String fwClassStr = (String) ServletHelper.getParameter(request, map, FWConstants.PARAM_FW_CLASS, false);
			if (StringUtils.hasValue(fwClassStr) == false) fwClassStr = (String) ServletHelper.getParameter(request, map, FWConstants.PARAM_FW_CLASS_OLD, false);
			String fwMethodStr = (String) ServletHelper.getParameter(request, map, FWConstants.PARAM_FW_METHOD, false);
			if (StringUtils.hasValue(fwMethodStr) == false) fwMethodStr = (String) ServletHelper.getParameter(request, map, FWConstants.PARAM_FW_METHOD_OLD, false);
			if ((fwClassStr == null) && (api == true && apiPartArray.length >= 1)) fwClassStr = apiPartArray[0];
			if ((fwMethodStr == null) && (api == true && apiPartArray.length >= 2))  fwMethodStr = apiPartArray[1];



			// SE NAO FOI PASSADO NENHUM PARAMETRO... 
			// VOU PARA A TELA DE LOGIN
			if (fwClassStr == null) {
				fwClassStr = ServletHelper.getClassInParameterOfPropertie(PropertiesConstants.SYSTEM_LINK_LOGIN);
				request.getSession().invalidate();
			}
			if (fwMethodStr == null) fwMethodStr = "doScreen";

			// parametros do FW
			UserContext userContext = ServletHelper.getUsuarioSession(request);
			TransactionDB trans = null;
			Class<?> fwClass = null;
			try {
				// por enquanto sempre precisa criar esse cara!!!!
				trans = TransactionDB.getInstance(userContext);
				fwClass = ServletHelper.getClasse(request.getServletContext(), fwClassStr);
			} catch (SQLRecoverableException e) {
				fwClass = Controller500Error.class;
			}

			// nao pode ter else mesmo
			if (fwClass == null) {
				if (ServletHelper.getDotumSmartReport(request.getServletContext(), fwClassStr) != null) {
					fwClass = Controller900Report.class;

					String reportCode = (String)ServletHelper.getParameter(request, map, FWConstants.PARAM_FW_REPORT_CODE, false);
					if (reportCode == null) reportCode = fwClassStr;
					if ((userContext == null) || (userContext.getUser().hasAccessTaskByCode(reportCode) == false))
						fwClass = Controller403Forbidden.class;
				}
			}


			// se não existe joga o 404
			if (fwClass == null) {
				LOG.error(userContext, "Não foi possivel achar uma classe através do parametro \"" + fwClassStr +"\". Provavelmente o propertie esta errado");

				fwClassStr = ServletHelper.getClassInParameterOfPropertie(PropertiesConstants.SYSTEM_LINK_404ERROR);
				if (fwClassStr != null) {
					fwClass = ServletHelper.getClasseMap(request.getServletContext()).get(fwClassStr);
				} else {
					fwClass = Controller404Error.class;
				}
			}

			Menu m = (Menu)fwClass.getAnnotation(Menu.class);
			if (m.publico() == false) {
				// validar o usuario logado
				// se for developer vai mesmo...
				// sem choro!!!
				if ((userContext == null) && (PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_DEVELOPER))) {
					String usuario = PropertiesUtils.getString(PropertiesConstants.SYSTEM_USERNAME);
					String senha = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PASSWORD);
					AbstractSecurityManager security = (AbstractSecurityManager)Class.forName(preFixPackage + ".control.Security").newInstance();
					userContext = security.doLogin(trans, usuario, senha, null);
					ServletHelper.setUsuarioSession(request, userContext);
				}


				// aqui vou pegar o token e ver se o usuario existe
				if (api == true) {
					if (token == null) {
						response.setStatus(403);
					} else {
						Claims claims = ServletHelper.parseJWT(token.replace(FWConstants.PARAM_FW_TOKEN_TYPE, ""));
					}
				}

				if ((userContext == null) || (AbstractDispacher.temAcesso(fwClass, userContext) == false)) {
					fwClass = Controller403Forbidden.class;
				}
			}

			request.setAttribute("trans", trans);
			request.setAttribute("userContext", userContext);
			request.setAttribute("map", map);
			request.setAttribute("fwClass", fwClass);
			request.setAttribute("fwMethodStr", fwMethodStr);
			request.setAttribute("capital/api", api);

			if (api == true) {
				UrlHtml urlH = new UrlHtml();
				urlH.addParam(FWConstants.PARAM_FW_METHOD, fwMethodStr);
				urlH.addParam(FWConstants.PARAM_FW_CLASS, fwClassStr);

				url = ServletHelper.getUrlBase(request);
				String fullUrl = url + urlH.getComponente();


				// queria que nao fosse um sendRedirect... e sim um esquema para reescrever o caminho da requisição
				// algo como isso...
				//				String url = wrapper.getRequestURL().toString();
				//				url = url.substring(url.lastIndexOf("/")+1);
				//				String token = request.getParameter(FWConstants.URL_TOKEN);
				//				token = SecurityUtil.decrypt(SecurityUtil.MOD2, token);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/do");
				dispatcher.forward(request, response);
			} else {
				chain.doFilter(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			// tratar qualquer tipo de erro aqui..
			// será um erro generico.. desconhecido
			// normalmente é o erro 500: erro interno do servidor
		}
	}

	public void destroy() {
	}

	public void init(FilterConfig filterConfig) {
	}
}


