package br.com.dotum.core.servlet;

import java.io.ByteArrayInputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.jedi.component.xml.smartmanager.model.DotumSmartManager;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.security.SecurityUtil;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;


public class ServletHelper {
	private static Log LOG = LogFactory.getLog(DoServlet.class);

	@Deprecated
	public final static void setCharSet(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		setCharSetHtml(request, response);
	}

	public final static void setCharSetHtml(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
	}

	public final static void setCharSetJson(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=UTF-8");
	}

	public static void limpaUsuarioSession(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute(FWConstants.PARAM_USUARIO_LOGADO, null);
	}

	public static UserContext getUsuarioSession(HttpServletRequest request) {
		if (request.isRequestedSessionIdValid() == true) {
			return (UserContext) request.getSession().getAttribute(FWConstants.PARAM_USUARIO_LOGADO);
		} 
		
		return null;
	}
	public static Map<String, UserContext> getUsuariosLogados(HttpServletRequest request) {
		Map<String, UserContext> usuarioLogado = (Map<String, UserContext>)request.getServletContext().getAttribute(FWConstants.PARAM_FW_USER_LOGGED);
		if (usuarioLogado == null) usuarioLogado = new HashMap<String, UserContext>();
		request.getServletContext().setAttribute(FWConstants.PARAM_FW_USER_LOGGED, usuarioLogado);

		return usuarioLogado;
	}


	public static void setUsuarioSession(HttpServletRequest request, UserContext userContext) {
		Map<String, UserContext> usuarioLogado = getUsuariosLogados(request);

		String sessionId = request.getSession().getId();
		String pathServer = getServerPath(request);
		if (userContext != null) {
			userContext.setServerPath(pathServer);

			usuarioLogado.put(sessionId, userContext);
		} else {
			usuarioLogado.remove(sessionId);
			request.getSession().invalidate();
		}
		request.getSession().setAttribute(FWConstants.PARAM_USUARIO_LOGADO, userContext);
	}

	private static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
//	private static String key = Base64.getEncoder().encodeToString("bolinho".getBytes());
	public static Claims parseJWT(String jwt){
//		Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
		try {
			Claims claims = Jwts.parser()         
					.setSigningKey(key)
					.parseClaimsJws(jwt).getBody();

			return claims;
		} catch (JwtException e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}
	
	public static String geraToken(Integer uniId, Integer pesId) {
		if (uniId == null) uniId = -1;
		if (pesId == null) pesId = -1;
		
//		Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
		String jwtstr = Jwts.builder()
				.setSubject(""+pesId)
				.setAudience(StringUtils.gerarToken(5))
				.claim("xyz", StringUtils.gerarToken(10))
				.claim("pesId", pesId)
				.claim("uniId", uniId)
				.compressWith(CompressionCodecs.DEFLATE)
				.signWith(key)
				.compact();
		return jwtstr;
	}

	public static String getServerPath(HttpServletRequest request) {
		String pathServer = request.getRealPath("/");
		pathServer = pathServer.replaceAll("\\\\", "/");
		if (pathServer.endsWith("/") == false) pathServer += "/";

		return pathServer;
	}
	
	public static <T> T getSessionApplication(HttpServletRequest request, String key) {
		return (T) request.getServletContext().getAttribute(key);
	}

	public static void setSessionApplication(HttpServletRequest request, String key, Object value) {
		request.getServletContext().setAttribute(key, value);
	}

	
	public static <T> T getSession(HttpServletRequest request, String key)  {
		return (T) request.getSession().getAttribute(key);
	}
	public static void setSession(HttpServletRequest request, String key, Object value) {
		request.getSession().setAttribute(key, value);
	}
	protected static Object getAttribute(HttpServletRequest request, String key)  {
		return (Object) request.getAttribute(key);
	}
	protected static void setAttribute(HttpServletRequest request, String key, Object value) {
		request.setAttribute(key, value);
	}

	public static Map<String, Object> showAllParameter(HttpServletRequest request, Map<String, Object> items) {
		if (items == null) {
			items = new HashMap<String, Object>();
			
			Enumeration<String> xxx = request.getParameterNames();
			while (xxx.hasMoreElements()) {
				String param = xxx.nextElement();
				
				String[] array = request.getParameterValues(param);
				if (array == null) {
					String str = request.getParameter(param);
					items.put(param, str);
				} else {
					items.put(param, array);
				}
				
			}		}
		
		for (String key : items.keySet()) {
			Object obj = items.get(key);
			if (obj instanceof String[]) {
				items.put(key,  ArrayUtils.stringToIn((String[])obj));
			}
		}
		return items;
	}
	
	/**
	 * Caso o servlet tenha arquivo na requisição 
	 * deverá ser usado este metodo para pegar os parametros
	 * ao inves de usar apenas request.getParameter("sss");
	 * O metodo getList(HttpServletRequest request) será auxiliar e o 
	 * retorno dele servirá como o segundo parametro deste.
	 * 
	 * @param request
	 * @param items
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public static Object getParameter(HttpServletRequest request, Map<String, Object> items, String param, boolean array) throws Exception  {
		String token = null;
		Object obj = null;
		if (items != null) {
			obj = items.get(param.toLowerCase());
			token = (String)items.get(FWConstants.URL_TOKEN.toLowerCase());
		} else {
			String paramCaseSensitive = null;
			Enumeration<String> xxx = request.getParameterNames();
			while (xxx.hasMoreElements()) {
				String temp = xxx.nextElement();
				if (param.equalsIgnoreCase(temp)) {
					paramCaseSensitive = temp;
					break;
				}
			}



			if (array == true) {
				obj = request.getParameterValues(paramCaseSensitive);
			} else { 
				obj = request.getParameter(paramCaseSensitive);
				token = request.getParameter(FWConstants.URL_TOKEN);
			}
		}
		if ((obj != null) && ((obj.equals("")) || (obj.equals("null")))) obj = null;

		// pode estar criptografada a linha aqui se o obj esta null
		if ((obj == null) && (StringUtils.hasValue(token))) {
			token = SecurityUtil.decrypt(SecurityUtil.MOD2, token);
			if (token == null) return null;

			for (String temp : token.split("&")) {
				if (temp.toLowerCase().startsWith(param.toLowerCase()) == false) continue;

				obj = temp.split("=")[1];
			}
		}

		return obj;
	}

	private static byte[] getImage(InputStream input) throws IOException { 
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); 
		try { 
			byte[] buffer = new byte[1024 * 1024]; 
			int bytesRead = input.read(buffer); 
			while (bytesRead != -1) { 
				outputStream.write(buffer, 0, bytesRead); 
				bytesRead = input.read(buffer); 
			} 
		} finally { 
			input.close(); 
		} 
		return outputStream.toByteArray(); 
	} 

	public static HashMap<String, Object> getList2(HttpServletRequest request) throws Exception {
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		// isMultipart é um item que vem no formulario html
		// <form method=\"post\" enctype=\"multipart/form-data\">
		// isso indica que o formulario terá arquivos;
		if (isMultipart == true) {
			HashMap<String, Object> result = new HashMap<String, Object>();
			ServletFileUpload upload = new ServletFileUpload();
			FileItemIterator iter = upload.getItemIterator(request);
			while (iter.hasNext()) {
				FileItemStream item = iter.next();
				String key = item.getFieldName();
				InputStream stream = item.openStream();
				Object value = null;
				if (item.isFormField()) {
					value = Streams.asString(stream);
				} else {
					int position = item.getName().lastIndexOf('.');
					String[] temp = null;

					if (position > 0)
						temp = new String[] {item.getName().substring(0, position), item.getName().substring(position+1)};

					byte[] baites = getImage(stream);
					ByteArrayInputStream in = new ByteArrayInputStream(baites);
					FileMemory fm = new FileMemory((InputStream)in);
					fm.setContentType( item.getContentType() );
					if ((temp != null) && (temp.length >= 2)) {
						fm.setName(temp[0]);
						fm.setExtension(temp[1]);
					}
					if (baites.length > 0)
						value = fm;
				}


				if (result.containsKey(key)) {
					Object obj = result.get(key);
					if (obj instanceof String) {
						value = new String[] {(String)obj, (String)value};
					} else if (obj instanceof String[]) {
						String[] x = (String[])obj;
						String[] y = new String[x.length+1];
						y[x.length] = (String)value;
						for (int i = 0; i < x.length; i++) {
							y[i] = x[i];
						}

						value = y;
					} else if (obj instanceof FileMemory) {
						value = new FileMemory[] {(FileMemory)obj, (FileMemory)value};
					} else if (obj instanceof FileMemory[]) {
						FileMemory[] x = (FileMemory[])obj;
						FileMemory[] y = new FileMemory[x.length+1];
						y[x.length] = (FileMemory)value;
						for (int i = 0; i < x.length; i++) {
							y[i] = x[i];
						}

						value = y;
					}
				}
				result.put(key, value);
			}
			return result;
		} else { 
			return null;
		}
	}

	public final static List getList(HttpServletRequest request) throws FileUploadException {
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		// isMultipart é um item que vem no formulario html
		// <form method=\"post\" enctype=\"multipart/form-data\">
		// isso indica que o formulario terá arquivos;
		if (isMultipart == true) {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			return upload.parseRequest(request);
		} else { 
			return null;
		}
	}

	public static String getURL(HttpServletRequest request) {
		String localName = request.getLocalName();
		if (StringUtils.hasValue(localName) == false) localName = request.getLocalAddr();
		if (StringUtils.hasValue(localName) == false) localName = "127.0.0.1";
		if (localName.equals("0:0:0:0:0:0:0:1"))      localName = "127.0.0.1";
		String result = request.getScheme() + "://" + localName + ":" + request.getLocalPort() + request.getContextPath() + "/"; 
		return result;
	}

	public static String getUrlBase(HttpServletRequest request) {
		ServletContext servletContext = request.getServletContext();

		String context = servletContext.getContextPath();
		StringBuffer url = request.getRequestURL();
		int pos = url.indexOf(context);
		String base_url = url.substring(0, pos+context.length()+1);
		return base_url;
	}

	public static void setTimeOutSession(HttpServletRequest request) {
		request.getSession().setMaxInactiveInterval(-1);
	}

	public static String getClassInParameterOfPropertie(String systemLinkLogin) {
		String fwClassStr = PropertiesUtils.getString(systemLinkLogin, true);
		if (fwClassStr == null) return null;

		String[] parts = fwClassStr.split("\\?");
		for (String part : parts) {
			if ((part.startsWith(FWConstants.PARAM_FW_CLASS)) || (part.startsWith(FWConstants.PARAM_FW_CLASS_OLD))) {
				fwClassStr = part.split("=")[1];
				break;
			}
		}

		return fwClassStr;
	}

	protected static Class<?> getClasse(ServletContext sc, String fwClassStr) {
		Map<String, Class<?>> map = getClasseMap(sc);
		Class<?> fwClass = map.get(fwClassStr);
		if (fwClass == null) 
			fwClass = map.get(ClassUtils.getCodeTask(fwClassStr));

		return fwClass;
	}
	protected static DotumSmartReport getDotumSmartReport(ServletContext sc, String fwClassStr) {
		Map<String, DotumSmartReport> map = getClasseMapSmartReport(sc);
		return map.get(fwClassStr);
	}
	protected static DotumSmartManager getDotumSmartManager(ServletContext sc, String fwClassStr) {
		Map<String, DotumSmartManager> map = getClasseMapSmartManager(sc);
		return map.get(fwClassStr);
	}
	protected static Map<String, Class<?>> getClasseMap(ServletContext sc) {
		Map<String, Class<?>> classCacheMap = (Map<String, Class<?>>)sc.getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_FULL_MAP);
		return classCacheMap;
	}
	protected static Map<String, DotumSmartReport> getClasseMapSmartReport(ServletContext sc) {
		Map<String, DotumSmartReport> classCacheMap = (Map<String, DotumSmartReport>)sc.getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_SMARTREPORT_MAP);
		return classCacheMap;
	}
	protected static Map<String, DotumSmartManager> getClasseMapSmartManager(ServletContext sc) {
		Map<String, DotumSmartManager> classCacheMap = (Map<String, DotumSmartManager>)sc.getAttribute(FWConstants.ATTRIBUTE_SESSION_CACHE_SMARTMANAGER_MAP);
		return classCacheMap;
	}
	protected static List<Class<?>> getClassByPackageCache(ServletContext sc, String pack) {
		Map<String, Class<?>> classCacheMap = getClasseMap(sc);

		List<Class<?>> result = new ArrayList<Class<?>>();
		for (String key : classCacheMap.keySet()) {
			Class<?> c = classCacheMap.get(key);
			if (c.getPackage().getName().equals(pack) == false) continue;

			result.add( classCacheMap.get(key) );
		}
		result = ClassUtils.orderAlphaNumeric(result);
		return result;
	}

}


