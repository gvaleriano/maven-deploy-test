package br.com.dotum.jedi.component.xml;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.XMLContext;

import br.com.dotum.jedi.component.xml.action.model.DotumAction;

public class DotumActionBuilder {

	public static DotumAction prepareDotumAction(String xml) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		
		DotumAction action = (DotumAction)unmar.unmarshal(new StringReader(xml));
		return action;
	}
	
	public static String prepareXMLDotumAction(DotumAction dotumAction) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );

		XMLContext context = new XMLContext();
		context.addMapping(mapping);
		Marshaller mar = context.createMarshaller();
		StringWriter sw = new StringWriter();
		mar.setWriter(sw);
		mar.marshal(dotumAction);
		
		return sw.toString();
	}
	
	public static URL getMappingURL() throws ClassNotFoundException {
		Class<?> klass = Class.forName(DotumAction.class.getName());
		ClassLoader classLoader = klass.getClassLoader();
		String pathMapping = DotumAction.class.getPackage().getName().replaceAll("\\.", "/");
		URL mappingURL = classLoader.getResource(pathMapping + "/mapping.xml");
		return mappingURL;
	}
}


