package br.com.dotum.jedi.component.xml;

import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;

import br.com.dotum.jedi.component.xml.dashboard.model.DotumDashBoard;
import br.com.dotum.jedi.component.xml.dashboard.model.DotumDashBoardColumn;
import br.com.dotum.jedi.component.xml.dashboard.model.DotumDashBoardData;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.CrossTab;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;

public class DotumDashBoardBuilder {

	protected TransactionDB trans;
	protected UserContext userContext;

	protected ResultSet model;
	protected DotumDashBoard dashboard;

	protected HashMap<String, Object> parmsValues;
	protected List<String> categoriesX = new ArrayList<String>();
	protected List<String> nameSeries = new ArrayList<String>();
	protected HashMap<String, Object[]> valuesSerie = new HashMap<String, Object[]>();
	protected List<Bean> genList = new ArrayList<Bean>();

	// objetos que representam o relatorio
	public DotumDashBoardBuilder(TransactionDB trans) {
		this.trans = trans;
	}

	public void setParms(HashMap<String, Object> parms) {
		this.parmsValues = parms;
	}

	public Object getParm(String key) {
		return this.parmsValues.get(key);
	}

	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}

	public DotumDashBoard getDotumDashBoard() {
		return dashboard;
	}

	public List<String> getCategorias() {
		return categoriesX;
	}
	public HashMap<String, Object[]> getSeries() {
		return valuesSerie;
	}
	public List<Bean> getBeanList() {
		return genList;
	}

	public void getCategoriasStr() {
		StringBuilder sb = new StringBuilder();
		sb.append(StringUtils.fillRight("Elemento", ' ', 30));
		for (String car : categoriesX) {
			sb.append(StringUtils.fillCenter(car, ' ', 15));
		}
		String temp = sb.toString();
		sb.append(StringUtils.fillRight("\n", '=', temp.length()+1));
	}

	// aqui começa a montagem do dashboard
	public void builder(String reportFileIn) throws Exception {

		this.dashboard = prepareDotumDashBoard(reportFileIn);

		// execute querie
		runQuery();

		if (dashboard.getType().equals("table")) {

			createTable();

		} else {
			// cria as categorias
			createCategories();

			// cria as series
			createSeries();

			// cria as values
			createValues();
		}

		model.getStatement().close();
		model.close();
	}

	public static DotumDashBoard prepareDotumDashBoard(String xml) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		DotumDashBoard dashBoard = null;
		dashBoard = (DotumDashBoard)unmar.unmarshal(new StringReader(xml));
		return dashBoard;
	}


	public static DotumDashBoard prepareDotumDashBoard(File reportFileIn) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		DotumDashBoard dashBoard = null;
		try {
			dashBoard = (DotumDashBoard)unmar.unmarshal(new InputSource(new FileReader(reportFileIn)));
		} catch (Exception e) {
			throw new Exception(reportFileIn.getAbsolutePath() + ". " + e.getMessage(), e);
		}
		return dashBoard;
	}

	private void runQuery() throws DeveloperException, Exception {
		// agora vou substituir os parametros pelos valores do mapa parms
		String sql = SQLUtils.parseQuery(this.dashboard.getSQL(), parmsValues);
		if (sql.toUpperCase().contains("$P{")) {
			int p_start = sql.toUpperCase().indexOf("$P{");
			int p_end = p_start+15;
			if (p_end > sql.length()) p_end = sql.length();
			String temp = sql.substring(p_start, p_end);
			throw new DeveloperException("Não foi informado todos os parâmetros para substituir na query. Verifique. Provavelmente aqui ==> "+temp);
		}
		this.model = trans.execQuery( sql, true );
	}

	private void createTable() throws Exception {

		this.model.beforeFirst();
		while (this.model.next()) {
			Bean genBean = new Bean();
			for (DotumDashBoardColumn column : dashboard.getColumns()) {
				String columnName = column.getName();
				String columnType = column.getType();

				if (columnType.equalsIgnoreCase("decimal")) {
					Object x = model.getDouble(columnName);
					//					genBean.set(columnName, FormatUtils.formatDoubleBR((Double)x));

					genBean.setAttribute(columnName, (Double)x);
				} else if (columnType.equalsIgnoreCase("date")) {
					Object x = model.getDate(columnName);
					genBean.setAttribute(columnName, FormatUtils.formatDate((java.sql.Date)x));
				} else if (columnType.equalsIgnoreCase("number")) {
					Object x = model.getInt(columnName);
					genBean.setAttribute(columnName, "" + (Integer)x);
				} else {
					Object x = model.getString(columnName);
					genBean.setAttribute(columnName, (String)x);
				}
			}
			genList.add( genBean );
		}		
	}

	public void createCategories() throws Exception {
		String nameColunmCategory = null;
		for (DotumDashBoardData d : dashboard.getData()) {
			if (d.getType().equals("category") == false) continue;
			nameColunmCategory = d.getName();
			break;
		}

		this.model.beforeFirst();
		while (this.model.next()) {
			String category = model.getString(nameColunmCategory);
			if (categoriesX.contains(category) == true) continue; 
			categoriesX.add(category);
		}
	}

	public void createSeries() throws Exception {
		String nameColunmSerie = null;
		for (DotumDashBoardData d : dashboard.getData()) {
			if (d.getType().equals("serie") == false) continue;
			nameColunmSerie = d.getName();
			break;
		}

		this.model.beforeFirst();
		while (this.model.next()) {
			String serie = model.getString(nameColunmSerie);
			if (nameSeries.contains(serie) == true) continue; 
			nameSeries.add(serie);
		}
	}

	public void createValues() throws Exception {
		String nameColunmCategory = null;
		for (DotumDashBoardData d : dashboard.getData()) {
			if (d.getType().equals("category") == false) continue;
			nameColunmCategory = d.getName();
			break;
		}

		String nameColunmSerie = null;
		for (DotumDashBoardData d : dashboard.getData()) {
			if (d.getType().equals("serie") == false) continue;
			nameColunmSerie = d.getName();
			break;
		}

		String nameColunmValue = null;
		for (DotumDashBoardData d : dashboard.getData()) {
			if (d.getType().equals("value") == false) continue;
			nameColunmValue = d.getName();
			break;
		}

		List<Bean> list = new ArrayList<Bean>();
		this.model.beforeFirst();
		while (this.model.next()) {
			Bean gBean = new Bean();
			gBean.setAttribute(nameColunmCategory, model.getString(nameColunmCategory));
			gBean.setAttribute(nameColunmSerie, model.getString(nameColunmSerie));
			gBean.setAttribute(nameColunmValue, model.getDouble(nameColunmValue));
			list.add(gBean);
		}

		valuesSerie = CrossTab.createCrossTabValues(list, nameColunmCategory, nameColunmSerie, nameColunmValue);
	}

	// *************************************************************************************************************************************
	public static URL getMappingURL() throws ClassNotFoundException {
		Class<?> klass = Class.forName(DotumDashBoard.class.getName());
		ClassLoader classLoader = klass.getClassLoader();
		String pathMapping = DotumDashBoard.class.getPackage().getName().replaceAll("\\.", "/");
		URL mappingURL = classLoader.getResource(pathMapping + "/mapping.xml");
		return mappingURL;
	}
}


