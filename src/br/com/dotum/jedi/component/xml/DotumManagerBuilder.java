package br.com.dotum.jedi.component.xml;

import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;

import br.com.dotum.jedi.component.xml.manager.model.DotumManager;
import br.com.dotum.jedi.component.xml.manager.model.DotumManagerAfterOpen;
import br.com.dotum.jedi.component.xml.manager.model.DotumManagerColumn;
import br.com.dotum.jedi.component.xml.manager.model.DotumManagerProcess;
import br.com.dotum.jedi.component.xml.manager.model.DotumManagerRelation;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.View;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.Field;
import br.com.dotum.jedi.core.table.RelationToMany;
import br.com.dotum.jedi.core.table.TableField;
import br.com.dotum.jedi.core.table.TableFieldEditType;
import br.com.dotum.jedi.core.table.TableFieldMask;
import br.com.dotum.jedi.util.SQLUtils;
import bsh.EvalError;
import bsh.Interpreter;

public class DotumManagerBuilder {

	protected TransactionDB trans;

	protected UserContext userContext;

	protected DotumManager component;

	protected HashMap<String, Object> parmsValues;
	protected List<TableFieldMask> fieldsMask = new ArrayList<TableFieldMask>(0);	 

	// objetos que representam o relatorio
	public DotumManagerBuilder(TransactionDB trans) {
		this.trans = trans;
	}

	public void setParms(HashMap<String, Object> parms) {
		this.parmsValues = parms;
	}

	public Object getParm(String key) {
		return this.parmsValues.get(key);
	}

	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}

	public DotumManager getDotumManager() {
		return component;
	}

	// aqui começa a montagem do component
	public void builder(File componentFileIn) throws Exception {

		this.component = prepareDotumManager(componentFileIn);

		// cria as colunas da view
		createColumns();

		// execute querie
		runQuery();
	}

	// aqui começa a montagem do component
	public void builder(String componentFile) throws Exception {

		this.component = prepareDotumManager(componentFile);

		// cria as colunas da view
		createColumns();

		// execute querie
		runQuery();
	}

	public static DotumManager prepareDotumManager(String xml) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		DotumManager component = null;
		component = (DotumManager)unmar.unmarshal(new StringReader(xml));
		return component;
	}


	public static DotumManager prepareDotumManager(File componentFileIn) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		DotumManager component = null;
		try {
			component = (DotumManager)unmar.unmarshal(new InputSource(new FileReader(componentFileIn)));
		} catch (Exception e) {
			throw new Exception(componentFileIn.getAbsolutePath() + ". " + e.getMessage(), e);
		}
		return component;
	}

	private void createColumns() throws DeveloperException, Exception {

		for (DotumManagerColumn column : this.component.getColumn()) {

			Field fieldTemp = TableFieldEditType.getFieldByType( column.getType() );
			Integer typeId = TableField.getFieldType( fieldTemp.getTypeClassName() );
			Integer maxlength = 15;
			Integer precision = 0;

			TableField field = new TableField(column.getNameBean(), column.getName(), column.getAliasTable(), typeId, maxlength, precision);
			TableFieldMask fieldMask = new TableFieldMask(column.getLabel(), fieldTemp.getId());
			fieldMask.setField(field);
			fieldMask.setShowInList(column.isShowInList());
			fieldMask.setVisible(column.isVisible());
			fieldsMask.add(fieldMask);        
		}
	}

	public void runQuery() throws DeveloperException, Exception {
		// agora vou substituir os parametros pelos valores do mapa parms
		String sql = SQLUtils.parseQuery(this.component.getSQL(), this.parmsValues);
		sql = SQLUtils.parseQueryParamDefault(sql, this.userContext);

		
		if (sql.toUpperCase().contains("$P{")) {
			int p_start = sql.toUpperCase().indexOf("$P{");
			int p_end = p_start+15;
			if (p_end > sql.length()) p_end = sql.length();
			String temp = sql.substring(p_start, p_end);
			throw new DeveloperException("Não foi informado todos os parâmetros para substituir na query. Verifique. Provavelmente aqui ==> "+temp);
		}

		//		this.component.setBeanList(  getTable().open(trans, 1, this.component.getLimit()) );
	}


	// *************************************************************************************************************************************
	public static URL getMappingURL() throws ClassNotFoundException {
		Class<?> klass = Class.forName(DotumManager.class.getName());
		ClassLoader classLoader = klass.getClassLoader();
		String pathMapping = DotumManager.class.getPackage().getName().replaceAll("\\.", "/");
		URL mappingURL = classLoader.getResource(pathMapping + "/mapping.xml");
		return mappingURL;
	}

	public static View instanciarTableView(final String sql, final DotumManagerProcess process, DotumManagerColumn[] columnArray) throws DeveloperException, EvalError{

		StringBuilder sb = new StringBuilder();
		sb.append(" import br.com.dotum.jedi.core.db.View;\n");
		sb.append(" import java.util.Date;\n");
		sb.append(" public class GenericView extends View {\n");

		sb.append(" public GenericView(){\n");
		for (DotumManagerColumn column : columnArray) {


			String type = "";
			String typeField = column.getType();
			if (typeField.equals("IntegerField")) {
				type = "Integer.class, ";
			} else if (typeField.equals("DecimalField")) {
				type = "Double.class, ";
			} else if (typeField.equals("DateField")) {
				type = "Date.class, ";
			} else if (typeField.equals("ComboBoxDBField")) {
				type = "Integer.class, ";
			} else if (typeField.equals("TextField")) {
				type = "String.class, ";
			}

			String key = column.getNameBean();
			String value = (column.getAliasTable() == null ? "" : column.getAliasTable() + ".") + column.getName();

			sb.append(" addField("+ type +"\""+key+"\", \""+value+"\");                \n");

		}
		sb.append("   setQuery(\""+sql.replaceAll("\n", "").replace("\r", "").replace("\t", " ").replace("\"", "\\\"")+"\");\n");
		sb.append(" }\n");

		sb.append(" \n"); 
		sb.append(" }\n");

		Interpreter i = new Interpreter();
		i.eval(sb.toString());
		i.eval("obj = new GenericView()");
		View view = (View)i.get("obj");

		return view;
	}

	public static RelationToMany createRelation(UserContext userContext, DotumManagerRelation dotumRelation, View tableView) throws Exception {
		String relationSql = SQLUtils.parseQueryParamDefault(dotumRelation.getSQL(), userContext);
		//dotumRelation.setSQL( relationSql );

		View tableViewRelation = DotumManagerBuilder.instanciarTableView(relationSql, dotumRelation.getProcess(), dotumRelation.getColumn());

		RelationToMany relation = new RelationToMany("nameMaster_"+dotumRelation.getTitle());
		relation.setMaster(tableView);
		relation.setDetail(tableViewRelation);
		relation.setTitle(dotumRelation.getTitle());

		String[] masterProperties = dotumRelation.getMasterProperties().replaceAll(" ", "").split(",");
		String[] detailProperties = dotumRelation.getDetailProperties().replaceAll(" ", "").split(",");
		relation.setMasterProperties(masterProperties);
		relation.setDetailProperties(detailProperties);

		return relation;
	}

	public static void afterSelect(DotumManagerProcess process, List<Bean> list) throws Exception{
		Map<String, Class> classeMap = new HashMap<String, Class>();
		Map<String, Object> objMap = new HashMap<String, Object>();


		if(process != null){
			for(Bean bean : list){
				DotumManagerAfterOpen[] afterOpenList = process.getAfterOpen();
				for (int l = 0; l < afterOpenList.length; l++) {
					DotumManagerAfterOpen afterOpen = afterOpenList[l]; 

					// invocar a classe e fazer o calculo
					// com reflexao
//					Class<?> clazz = classeMap.get("CLASSE_"+afterOpen.getClasse());
//					Object obj = objMap.get("OBJ_"+afterOpen.getClasse());
//					if (obj == null) {
//						clazz = Class.forName(afterOpen.getClasse());
//						obj = clazz.newInstance();
//
//						classeMap.put("CLASSE_"+afterOpen.getClasse(), clazz);
//						objMap.put("OBJ_"+afterOpen.getClasse(), obj);
//					}
					Class<?> clazz = Class.forName(afterOpen.getClasse());
					Object obj = clazz.newInstance();
//					if (obj == null) {
//						clazz = 
//						obj = 
//
//						classeMap.put("CLASSE_"+afterOpen.getClasse(), clazz);
//						objMap.put("OBJ_"+afterOpen.getClasse(), obj);
//					}

					Method method = clazz.getMethod(afterOpen.getMethod(), Bean.class);
					Object temp = method.invoke(obj, bean);
					bean.setAttribute(afterOpen.getName(), temp);
				}
			}
		}

	}
}

