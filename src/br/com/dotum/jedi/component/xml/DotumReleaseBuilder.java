package br.com.dotum.jedi.component.xml;

import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.net.URL;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;

import br.com.dotum.jedi.component.xml.release.model.DotumRelease;

public class DotumReleaseBuilder {

	protected DotumRelease releaseBean;

	public static URL getMappingURL() throws ClassNotFoundException {
		Class<?> klass = Class.forName(DotumRelease.class.getName());
		ClassLoader classLoader = klass.getClassLoader();
		String pathMapping = DotumRelease.class.getPackage().getName().replaceAll("\\.", "/");
		URL mappingURL = classLoader.getResource(pathMapping + "/mapping.xml");
		return mappingURL;
	}

	public void prepareDotumRelease(String xml) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		this.releaseBean = (DotumRelease)unmar.unmarshal(new StringReader(xml));
	}

	public void prepareDotumRelease(File fileIn) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		try {
			this.releaseBean = (DotumRelease)unmar.unmarshal(new InputSource(new FileReader(fileIn)));
		} catch (Exception e) {
			throw new Exception(fileIn.getAbsolutePath() + ". " + e.getMessage(), e);
		}
	}

	public DotumRelease getRelease() {
		return releaseBean;
	}
}

