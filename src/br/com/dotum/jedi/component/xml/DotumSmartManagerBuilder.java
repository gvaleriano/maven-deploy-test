package br.com.dotum.jedi.component.xml;

import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;

import br.com.dotum.jedi.component.xml.smartmanager.model.Column;
import br.com.dotum.jedi.component.xml.smartmanager.model.DotumSmartManager;
import br.com.dotum.jedi.component.xml.smartmanager.model.Relation;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.View;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Field;
import br.com.dotum.jedi.core.table.RelationToMany;
import br.com.dotum.jedi.core.table.TableField;
import br.com.dotum.jedi.core.table.TableFieldEditType;
import br.com.dotum.jedi.core.table.TableFieldMask;
import br.com.dotum.jedi.util.SQLUtils;
import bsh.EvalError;
import bsh.Interpreter;

public class DotumSmartManagerBuilder {

	protected TransactionDB trans;
	protected UserContext userContext;
	protected DotumSmartManager component;

	protected Map<String, Object> parmsValues;
	protected List<TableFieldMask> fieldsMask = new ArrayList<TableFieldMask>(0);	 


	public DotumSmartManagerBuilder(TransactionDB trans) {
		this.trans = trans;
	}

	public void setParms(Map<String, Object> parms) {
		this.parmsValues = parms;
	}

	public Object getParm(String key) {
		return this.parmsValues.get(key);
	}

	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}

	public DotumSmartManager getDotumSmartManager() {
		return component;
	}


	public void builder(File componentFileIn) throws Exception {
		this.component = prepareDotumSmartManager(componentFileIn);
	}
	public void builder(String componentFile) throws Exception {
		this.component = prepareDotumSmartManager(componentFile);
	}

	public static DotumSmartManager prepareDotumSmartManager(String xml) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		DotumSmartManager component = null;
		try {
			component = (DotumSmartManager)unmar.unmarshal(new StringReader(xml));
		} catch (Exception e) {
			throw new Exception(SQLUtils.formatPartStringToShow(xml, 200) + ". " + e.getMessage(), e);
		}
		return component;
	}
	public static DotumSmartManager prepareDotumSmartManager(File componentFileIn) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		DotumSmartManager component = null;
		try {
			component = (DotumSmartManager)unmar.unmarshal(new InputSource(new FileReader(componentFileIn)));
		} catch (Exception e) {
			throw new Exception(componentFileIn.getAbsolutePath() + ". " + e.getMessage(), e);
		}
		return component;
	}

	

	// helper
	public static URL getMappingURL() throws ClassNotFoundException {
		Class<?> klass = Class.forName(DotumSmartManager.class.getName());
		ClassLoader classLoader = klass.getClassLoader();
		String pathMapping = DotumSmartManager.class.getPackage().getName().replaceAll("\\.", "/");
		URL mappingURL = classLoader.getResource(pathMapping + "/mapping.xml");
		return mappingURL;
	}

	public static RelationToMany createRelation(UserContext userContext, Relation dotumRelation, View tableView) throws Exception {
		String relationSql = SQLUtils.parseQueryParamDefault(dotumRelation.getSQL(), userContext);
		//dotumRelation.setSQL( relationSql );

//		View tableViewRelation = DotumSmartManagerBuilder.instanciarTableView(relationSql, dotumRelation.getProcess(), dotumRelation.getColumn());

		RelationToMany relation = new RelationToMany("nameMaster_"+dotumRelation.getTitle());
//		relation.setMaster(tableView);
//		relation.setDetail(tableViewRelation);
//		relation.setTitle(dotumRelation.getTitle());
//
//		String[] masterProperties = dotumRelation.getMasterProperties().replaceAll(" ", "").split(",");
//		String[] detailProperties = dotumRelation.getDetailProperties().replaceAll(" ", "").split(",");
//		relation.setMasterProperties(masterProperties);
//		relation.setDetailProperties(detailProperties);

		return relation;
	}
}

