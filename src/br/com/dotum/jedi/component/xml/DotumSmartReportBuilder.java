package br.com.dotum.jedi.component.xml;


import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.xml.Unmarshaller;
import org.xml.sax.InputSource;

import br.com.dotum.jedi.component.xml.smartreport.builder.AbstractDotumSmartReport;
import br.com.dotum.jedi.component.xml.smartreport.builder.DotumSmartReportExportHTML;
import br.com.dotum.jedi.component.xml.smartreport.builder.DotumSmartReportExportPDF;
import br.com.dotum.jedi.component.xml.smartreport.builder.DotumSmartReportExportXLS;
import br.com.dotum.jedi.component.xml.smartreport.model.AfterOpen;
import br.com.dotum.jedi.component.xml.smartreport.model.Column;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumReportInfo;
import br.com.dotum.jedi.component.xml.smartreport.model.DotumSmartReport;
import br.com.dotum.jedi.component.xml.smartreport.model.Parm;
import br.com.dotum.jedi.component.xml.smartreport.model.Section;
import br.com.dotum.jedi.component.xml.smartreport.model.SectionDataBean;
import br.com.dotum.jedi.core.db.ORM;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.CrossTab;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.FileUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;

public class DotumSmartReportBuilder {
	static Log LOG = LogFactory.getLog(DotumSmartReportBuilder.class);

	private TransactionDB trans;
	private UserContext userContext;
	private String code;
	private Map<String, Object> parmsValues;
	private Map<String, String> parmsValuesStr;
	private DotumReportInfo dotumReportInfo;
	private DotumSmartReport dotumSmartReport;
	private String logotipoPath;
	private FileMemory fm;

	//
	// metodos basicos para manipular informações e gerar o relatorio
	//
	public DotumSmartReportBuilder(TransactionDB trans) {
		this.trans = trans;
	}

	public DotumSmartReport getDotumSmartReport() {
		return this.dotumSmartReport;
	}
	public void setParmsValues(Map<String, Object> parmsValues) {
		this.parmsValues = parmsValues;
	}
	public Map<String, Object> getParmsValues() {
		return parmsValues;
	}
	public void setParmsStr(HashMap<String, String> parmsValuesStr) {
		this.parmsValuesStr = parmsValuesStr;
	}
	public Map<String, String> getParmsValuesStr() {
		return parmsValuesStr;
	}

	public Object getParmValue(String key) {
		return this.parmsValues.get(key);
	}

	public void setTransaction(TransactionDB trans) {
		this.trans = trans;
	}
	public TransactionDB getTransaction() {
		return trans;
	}
	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}
	public UserContext getUserContext() {
		return userContext;
	}
	public void setReportInfo(DotumReportInfo dotumReportInfo) {
		this.dotumReportInfo = dotumReportInfo;
	}
	public DotumReportInfo getReportInfo() {
		if (dotumReportInfo == null) dotumReportInfo = new DotumReportInfo();
		return dotumReportInfo;
	}	

	public String getCode() {
		return this.code;
	}

	public void builder(String code, String xml) throws Exception {
		builder(code, prepare(xml));
	}

	//
	// RUN REPORT
	//
	public void builder(String code, DotumSmartReport dotumSmartReport) throws Exception {
		this.code = code;
		this.dotumSmartReport = dotumSmartReport;

		// faz as validacoes no relatorio
		validate();

		// execute querie
		runQuery();

		// manipulando e tratando os dados apos o select
		afterOpen();


		// cria relatorio
		createReport();
	}

	//
	// PASSO 1  
	//
	public static DotumSmartReport prepare(String xml) throws Exception {
		DotumSmartReport report = null;
		try {
			URL mappingURL = DotumSmartReportBuilder.getMappingURL();
			Mapping mapping = new Mapping();
			mapping.loadMapping( mappingURL );
			Unmarshaller unmar = new Unmarshaller(mapping);
			report = (DotumSmartReport)unmar.unmarshal(new StringReader(xml));
			report.setXml(xml);
		} catch (Exception e) {
			throw new Exception(e.getMessage() + "Motivo: Nao foi possivel converter o XML \""+ SQLUtils.formatPartStringToShow(xml, 100) +"\"", e);
		}

		return report;
	}
	public static DotumSmartReport prepare(File reportFileIn) throws Exception {
		URL mappingURL = getMappingURL();
		Mapping mapping = new Mapping();
		mapping.loadMapping( mappingURL );
		Unmarshaller unmar = new Unmarshaller(mapping);
		DotumSmartReport report = null;
		try {
			report = (DotumSmartReport)unmar.unmarshal(new InputSource(new FileReader(reportFileIn)));
		} catch (Exception e) {
			throw new Exception(reportFileIn.getAbsolutePath() + ". " + e.getMessage(), e);
		}
		return report;
	}
	private static URL getMappingURL() throws ClassNotFoundException {
		Class<?> klass = Class.forName(DotumSmartReport.class.getName());
		ClassLoader classLoader = klass.getClassLoader();
		String pathMapping = DotumSmartReport.class.getPackage().getName().replaceAll("\\.", "/");
		URL mappingURL = classLoader.getResource(pathMapping + "/mapping.xml");
		return mappingURL;
	}

	//
	// PASSO 2
	//
	private void validate() throws Exception {

		if (userContext == null)
			throw new DeveloperException("Para criar um relatório é necessário definir o 'userContext'.");

		//		if (reportInfo == null)
		//			throw new DeveloperException("Para criar um relatório é necessário definir os dados do relatório 'reportInfo'");	

	}

	//
	// PASSO 3
	//
	private void runQuery() throws DeveloperException, Exception {
		List<SectionDataBean> sectionDataList = new ArrayList<SectionDataBean>();
		for (int i = 0; i < this.dotumSmartReport.getSections().length; i++) {
			Section section = this.dotumSmartReport.getSections()[i];
			SectionDataBean sectionData = new SectionDataBean();
			sectionDataList.add(sectionData);

			sectionData.setTitle(section.getTitle());
			sectionData.setInformation(section.getInformation());
			sectionData.setType(section.getType());

			Column[] columns = null;
			if (section.getTabular() != null) {
				columns = section.getTabular().getColumns();
			} else if(section.getVariable() != null){
				columns = section.getVariable().getColumns();
			} else {
				columns = section.getCrosstab().getColumns();
			}
			for (Column column : columns) {
				if (AbstractDotumSmartReport.isColumnGroup(column.getGroup())){
					String name = column.getName();
					name = SQLUtils.parseQuery(name, parmsValues);
					column.setName(name);
				}
				sectionData.addColumn(column.getName(), column.getLabel(), column.getType(), column.getGroup(), column.getCrosstab());
			}

			// agora vou substituir os parametros pelos valores do mapa parms
			String sql = SQLUtils.parseQuery(section.getSQL(), parmsValues);
			sql = SQLUtils.parseQueryParamDefault(sql, userContext);
			if (sql.toUpperCase().contains("$P{")) {
				int p_start = sql.toUpperCase().indexOf("$P{");
				int p_end = p_start+15;
				if (p_end > sql.length()) p_end = sql.length();
				String temp = sql.substring(p_start, p_end);
				throw new DeveloperException("Componente: " + code + ". Não foi informado todos os parâmetros para substituir na query. Verifique. Provavelmente aqui ==> "+temp);
			}
			List<Object[]> result = null;
			try{
				ResultSet model = trans.execQuery(sql );
				if (section.getTabular() != null) {
					result = resultSetToTabular(model, section, sectionData);
				} else if(section.getVariable() != null){ 
					result = resultSetToVariable(model, section, sectionData);
				} else {
					result = resultSetToCrossTab(model, section, sectionData);
				}
			} catch(Exception e) {
				throw e;
			} finally {
				trans.closeQuery();
			}
			sectionData.setModel( result );
		}

		dotumSmartReport.setSectionDataList(sectionDataList.toArray(new SectionDataBean[sectionDataList.size()]));
	}
	private List<Object[]> resultSetToTabular(ResultSet model, Section section, SectionDataBean sectionData) throws SQLException {
		List<Object[]> result = new ArrayList<Object[]>();
		while (model.next()) {
			Integer size = 0;
			Column[] columns = null;
			size = section.getTabular().getColumns().length;
			columns = section.getTabular().getColumns();
			Object[] item = new Object[size];
			for (int j = 0; j < size; j++) {
				Column column = columns[j];
				if ((StringUtils.hasValue( column.getGroup() ) ) && (column.getGroup().equals("0") == false))  {
					item[j] = model.getObject( column.getName() );
				} else if (column.getType().equalsIgnoreCase("text")) {
					item[j] = model.getString( column.getName() );
				} else if (column.getType().equalsIgnoreCase("number")) {
					item[j] = model.getInt( column.getName() );
				} else if (column.getType().equalsIgnoreCase("decimal")) {
					item[j] = model.getDouble( column.getName() );
				} else if (column.getType().equalsIgnoreCase("date")) {
					item[j] = model.getDate( column.getName() );
				}
			}
			result.add(item);
		}
		return result;
	}
	private List<Object[]> resultSetToVariable(ResultSet model, Section section, SectionDataBean sectionData) throws SQLException {
		List<Object[]> result = new ArrayList<Object[]>();

		String nameColumn = null;
		try {
			while (model.next()) {
				Integer size = section.getVariable().getColumns().length;
				Column[] columns = section.getVariable().getColumns();
				Object[] item = new Object[size];
				for (int j = 0; j < size; j++) {
					Column column = columns[j];
					nameColumn = column.getName();
					if (column.getType().equalsIgnoreCase("text")) {
						item[j] = model.getString( column.getName() );
					} else if (column.getType().equalsIgnoreCase("number")) {
						item[j] = model.getInt( column.getName() );
					} else if (column.getType().equalsIgnoreCase("decimal")) {
						item[j] = model.getDouble( column.getName() );
					} else if (column.getType().equalsIgnoreCase("date")) {
						item[j] = model.getDate( column.getName() );
					}
				}
				result.add(item);
			}
		} catch (SQLException e) {
			throw new SQLException(e.getMessage() + ": " + nameColumn, e);
		}
		return result;
	}
	private List<Object[]> resultSetToCrossTab(ResultSet model, Section section, SectionDataBean sectionData) throws SQLException, DeveloperException {
		List< Object[] > result = new ArrayList<Object[]>();
		List< Object[] > modelObjects = resultSetToObjects(model, section);


		Integer[] crossTabPositionColumns = crossTabPositionByType(section, "column");
		Integer[] crossTabPositionRows = crossTabPositionByType(section, "row");
		Integer[] crossTabPositionGroups = crossTabPositionGroup(section, "1");
		Integer[] crossTabPositionData = crossTabPositionByType(section, "data");
		List< Object[] > crossTabColumnValues = CrossTab.createCrossTabColumnValues(modelObjects, crossTabPositionColumns);
		Integer columnsCount = 0;

		//
		// inputa as primeiras linhas
		//
		if (crossTabColumnCount(section) == 1) {
			Object[] rows = createDataRowOneGroup( section, sectionData, crossTabColumnValues );
			columnsCount = rows.length;
			result.add( rows );
		}
		if (crossTabColumnCount(section) == 2) {
			List< Object[] > rows = createDataRowTwoGroups( section, sectionData, crossTabColumnValues );
			columnsCount = rows.get(0).length;
			for (Object[] row : rows) {
				result.add( row );
			}
		}


		//
		// primeiro vou preparar todo o mapa ("key" e array com espaço dos dados)
		//
		List<Object[]> groups = new ArrayList<Object[]>();
		List<Object[]> lines = new ArrayList<Object[]>();
		List<Object[]> columns = new ArrayList<Object[]>();
		HashMap<Object[], Object[]> values = new HashMap<Object[], Object[]>();

		// primeiro: pega as colunas 
		for (Object[] row : modelObjects) {
			Object[] column = crossTabValueByPosition(row, crossTabPositionColumns);
			if (temNaLista(columns, column) == true) continue; 
			columns.add(column);
		}

		// segundo: pega as linhas 
		for (Object[] row : modelObjects) {
			Object[] line = crossTabValueByPosition(row, crossTabPositionRows);
			if (temNaLista(lines, line) == true) continue;
			lines.add(line);
		}
		lines = ordernarLista(lines);

		// 3: pega as grupos 
		for (Object[] row : modelObjects) {
			Object[] group = crossTabValueByPosition(row, crossTabPositionGroups);
			if (temNaLista(groups, group) == true) continue;
			if (group.length == 0) continue;
			groups.add(group);
		}
		groups = ordernarLista(groups);


		// 4: prepara o campo para colocar os valores 
		for (Object[] ser : lines) {
			Object[] valuesArray = values.get(ser);
			if (valuesArray == null) {
				valuesArray = new Object[columnsCount];
				values.put(ser, valuesArray);
				result.add(valuesArray);
			}
		}

		for (Object[] row : modelObjects) {
			Object[] column = crossTabValueByPosition(row, crossTabPositionColumns);
			Object[] line = crossTabValueByPosition(row, crossTabPositionRows);
			Object[] value = crossTabValueByPosition(row, crossTabPositionData);

			a:
				for (Object[] ser : lines) {
					Object[] valuesArray = values.get(ser);
					for (int i = 0; i < columns.size(); i++) {
						Object[] cat = columns.get(i);
						if ((Arrays.equals(column, cat) == true) && (Arrays.equals(line, ser) == true)) {

							for (int j = 0; j < line.length; j++) {
								valuesArray[j] = line[j];
							}

							valuesArray[i + line.length] = value[0];
							break a;
						}
					}
				}
		}

		// aqui vou criar o total do grupo
		List< Object[] > result1 = new ArrayList<Object[]>();
		Integer[] positionGroupCabecaArray = null;
		Integer[] positionGroupTotalArray = null;
		if (groups.size() > 0) {
			String gOld = null;
			String gAtual = null;

			// isso é para manter o label
			result1.add(result.get(0));

			int xxx = 0;
			positionGroupCabecaArray = new Integer[groups.size()];
			positionGroupTotalArray = new Integer[groups.size()-1];
			for (int line = 1; line < result.size(); line++) {
				Object[] objArray = result.get(line);
				Object[] objLineGroupArrayNew = new Object[objArray.length];

				gAtual = (String)objArray[0];
				if (gAtual.equals(gOld) == false) {
					positionGroupCabecaArray[xxx++] = line;

					objLineGroupArrayNew[0] = gAtual;
					objLineGroupArrayNew[1] = gAtual;
					result1.add(objLineGroupArrayNew);
				}
				result1.add(objArray);
				gOld = gAtual; 
			}

			for (int i = 1; i < positionGroupCabecaArray.length; i++) {
				positionGroupTotalArray[i-1] = positionGroupCabecaArray[i];
			}


		} else {
			result1.addAll( result );
		}

		// aqui criar uma coluna com o total por elemento...
		List< Object[] > result2 = new ArrayList<Object[]>();
		Object[] objColumnArrayNew = new Object[result.get(0).length+1];
		for (int line = 0; line < result1.size(); line++) {
			Object[] objArray = result1.get(line);
			Object[] objLineArrayNew = new Object[objArray.length+1];

			Double total = 0.0;
			for (int column = 0; column < objArray.length; column++) {
				Object obj = objArray[column];
				if (obj instanceof Double) {
					total += (Double)obj;

					Double temp = (Double)objColumnArrayNew[column];
					if (temp == null) temp = 0.0;
					if (line > 0) objColumnArrayNew[column] = temp + (Double)obj;
				}

				objLineArrayNew[column] = obj;
			}
			if (line == 0) objLineArrayNew[objArray.length] = "Total";
			if (line > 0) objLineArrayNew[objArray.length] = total;



			result2.add(objLineArrayNew);  
		}
		result2.add(objColumnArrayNew);  

		// aqui é o total geral (fiquei com preguiça de pensar la e achar o ponto da soma!!!
		Double total = 0.0;
		for (int i = 0; i < objColumnArrayNew.length; i++) {
			Object obj = objColumnArrayNew[i];
			if (obj instanceof Double) total += (Double)obj;
		}
		objColumnArrayNew[objColumnArrayNew.length-1] = total;

		// aqui vou colocar o nome das colunas do CrosTab
		Object[] labels = result2.get(0);
		Column[] columnsTemp = section.getCrosstab().getColumns();

		for (int i = 0; i < crossTabPositionRows.length; i++) {
			String label = columnsTemp[crossTabPositionRows[i]].getLabel();
			labels[i] = label;
		}

		// aqui preciso colocar o total de cada grupo 
		List< Object[] > result3 = new ArrayList<Object[]>();
		if (groups.size() > 0) {
			objColumnArrayNew = new Double[result2.get(0).length];
			for (int line = 0; line < result2.size()-1; line++) {
				Object[] objArray = result2.get(line);

				for (int column = 0; column < objArray.length; column++) {
					Object obj = objArray[column];
					if (obj instanceof Double) {
						Double temp = (Double)obj;
						Double temp2 = (Double)objColumnArrayNew[column];
						objColumnArrayNew[column] = NumberUtils.coalesce(temp, 0.0) + NumberUtils.coalesce(temp2, 0.0);
					}
				}


				result3.add(objArray);
				if (line == result2.size()-2) {
					result3.add(objColumnArrayNew);  
				}
				for (int k = 0; k < positionGroupTotalArray.length; k++) {
					int position = positionGroupTotalArray[k];
					if (line != position+k) continue;

					result3.add(objColumnArrayNew);  
					objColumnArrayNew = new Double[result2.get(0).length];
				}
			}
			result3.add(result2.get(result2.size()-1));
		} else {
			result3.addAll(result2);
		}

		return result3;
	}

	private List<Object[]> ordernarLista(List<Object[]> lines) {
		int size1 = lines.size();
		int size2 = (size1 > 0 ? lines.get(0).length : 0);

		Object[][] array = new Object[size1][size2];
		for (int i = 0; i < size1; i++) {
			for (int j = 0; j < size2; j++) {
				array[i][j] = lines.get(i)[j];
			}
		}

		Arrays.sort (array, new Comparator<Object[]>() {  
			public int compare (Object[] linha1, Object[] linha2) {
				String part1 = "";
				String part2 = "";
				if (linha1.length == 1) part1 = linha1[0] + "";
				if (linha1.length == 2) part1 = linha1[0] + "/" + linha1[1];
				if (linha2.length == 1) part2 = linha2[0] + "";
				if (linha2.length == 2) part2 = linha2[0] + "/" + linha2[1];
				return part1.compareTo(part2);  
			}  
		});  


		List<Object[]> result = new ArrayList<Object[]>();
		for (int i = 0; i < size1; i++) {
			result.add( array[i] );
		}

		return result;
	}
	private boolean temNaLista(List<Object[]> columnsList, Object[] column) {

		for (Object[] columns : columnsList) {
			if (Arrays.equals(columns, column) == true)
				return true;
		}

		return false;
	}
	private Object[] crossTabValueByPosition(Object[] row, Integer[] position) {
		Object[] result = new Object[position.length];

		for (int i = 0; i < position.length; i++) {
			result[i] = row[position[i]];
		}

		return result;
	}	
	private List<Object[]> resultSetToObjects(ResultSet model, Section section) throws SQLException {
		List<Object[]> result = new ArrayList<Object[]>();
		while (model.next()) {
			List<Object> row = new ArrayList<Object>(); 
			Column[] reportColumns = section.getCrosstab().getColumns();
			for (Column column : reportColumns) {
				if (column.getType() == null) {
					row.add( model.getString( column.getName() ) );
				} else if (column.getType().equalsIgnoreCase("text")) {
					row.add( model.getString( column.getName() ) );
				} else if (column.getType().equalsIgnoreCase("number")) {
					row.add( model.getInt( column.getName() ) );
				} else if (column.getType().equalsIgnoreCase("decimal")) {
					row.add( model.getDouble( column.getName() ) );
				} else if (column.getType().equalsIgnoreCase("date")) {
					row.add( model.getDate( column.getName() ) );
				}
			}
			result.add( row.toArray( new Object[row.size()]) );
		}
		return result;
	}
	private List< Object[] > createDataRowTwoGroups(Section section, SectionDataBean sectionData, List<Object[]> crossTabColumnValues) {
		Object[] colunas1 = crossTabColumnValues.get(0);
		Object[] colunas2 = crossTabColumnValues.get(1);
		List< Object[] > result = new ArrayList< Object[] >();
		List<Object> row1 = new ArrayList<Object>();
		List<Object> row2 = new ArrayList<Object>();
		int countRow = crossTabRowCount(section);
		for (int i = 0; i < countRow; i++) {
			row1.add(null);
		}
		for (Object col : colunas1) {
			row1.add(col);
			for (int i = 0; i < colunas2.length-1; i++) {
				row1.add(null);
			}
		}

		for (int i = 0; i < countRow; i++) {
			row2.add(null);
		}
		for (int i = 0; i < colunas1.length; i++) {
			for (Object col : colunas2) {
				row2.add(col);
			}
		}
		result.add( row1.toArray(new Object[row1.size()] ));
		result.add( row2.toArray(new Object[row2.size()] ));
		return result;
	}
	private Object[] createDataRowOneGroup(Section section, SectionDataBean sectionData, List<Object[]> crossTabColumnValues) {
		Object[] colunas = crossTabColumnValues.get(0);
		List<Object> row = new ArrayList<Object>(); 
		int countRow = crossTabRowCount(section);
		for (int i = 0; i < countRow; i++) {
			row.add(null);
		}
		for (Object col : colunas) {
			row.add(col);
		}
		return row.toArray(new Object[row.size()]);
	}
	private int crossTabRowCount(Section section) {
		int result = 0;
		Column[] reportColumns = section.getCrosstab().getColumns();
		for (Column column : reportColumns) {
			if (column.getCrosstab() == null) continue;
			if (column.getCrosstab().equalsIgnoreCase("row")) {
				result++;
			}
		}	
		return result;		
	}
	private int crossTabColumnCount(Section section) {
		int result = 0;
		Column[] reportColumns = section.getCrosstab().getColumns();
		for (Column column : reportColumns) {
			if (column.getCrosstab() == null) continue;
			if (column.getCrosstab().equalsIgnoreCase("column")) {
				result++;
			}
		}	
		return result;		
	}
	private Integer[] crossTabPositionByType(Section section, String type) {
		List<Integer> result = new ArrayList<Integer>();
		Column[] reportColumns = section.getCrosstab().getColumns();
		for (int i = 0; i < reportColumns.length; i++) {
			Column column = reportColumns[i];
			if (column.getCrosstab() == null) continue;

			if (column.getCrosstab().equalsIgnoreCase(type)) {
				result.add(i);
			}
		}	
		return result.toArray(new Integer[result.size()]);
	}

	private Integer[] crossTabPositionGroup(Section section, String type) {
		List<Integer> result = new ArrayList<Integer>();
		Column[] reportColumns = section.getCrosstab().getColumns();
		for (int i = 0; i < reportColumns.length; i++) {
			Column column = reportColumns[i];
			if (column.getGroup().equalsIgnoreCase(type)) {
				result.add(i);
			}
		}	
		return result.toArray(new Integer[result.size()]);
	}

	//
	// PASSO 4
	//
	private void afterOpen() throws DeveloperException, Exception {
		Section[] sectionArray = dotumSmartReport.getSections();
		SectionDataBean[] sectionDataArray = dotumSmartReport.getSectionDataList();

		for (int i = 0; i < sectionArray.length; i++) {
			Section section = sectionArray[i];
			if (section.getProcess() == null) continue;
			AfterOpen[] afterOpenList = section.getProcess().getAfterOpen();

			SectionDataBean sectionData = sectionDataArray[i];
			List<Column> columns = sectionData.getColumns();
			List<Object[]> model = sectionData.getModel();

			Map<String, Object> map = new HashMap<String, Object>();
			for (int j = 0; j < model.size(); j++) {
				Object[] bean = model.get(j);

				Bean gb = new Bean();
				for (int k = 0; k < columns.size(); k++) {
					Column column = columns.get(k);
					Object obj = bean[k];

					gb.setAttribute(column.getName(), obj);
				}

				for (int l = 0; l < afterOpenList.length; l++) {
					AfterOpen afterOpen = afterOpenList[l];
					for (int k = 0; k < columns.size(); k++) {
						Column column = columns.get(k);
						if (column.getName().equals(afterOpen.getName()) == false) continue; 

						// invocar a classe e fazer o calculo
						// com reflexao
						Class<?> clazz = Class.forName(afterOpen.getClasse());
						Object obj = map.get(afterOpen.getName());
						if (obj == null) {
							obj = clazz.newInstance();
							map.put(afterOpen.getName(), obj);
						}

						Method method = clazz.getMethod(afterOpen.getMethod(), Bean.class);
						Object temp = method.invoke(obj, gb);
						bean[k] = temp;
						gb.setAttribute(column.getName(), temp);
						break;
					}
				}
			}
		}
	}
	//
	// PASSO 5
	//

	private void createReport() throws Exception {
		if (dotumSmartReport.getType() == null) dotumSmartReport.setType("html");
		if (dotumSmartReport.getType().equalsIgnoreCase("xls")) {
			// tem que ver se tem modelo e criar o arquivo fisicamente
			FileMemory fm = createPlanilhaModelo(trans, code);
			DotumSmartReportExportXLS xls = new DotumSmartReportExportXLS(this, fm);
			this.fm = xls.getFileMemory();
		} else if (dotumSmartReport.getType().equalsIgnoreCase("pdf")) {
			try {
				createLogotipo(trans, userContext);
			} catch (Exception e) {
				LOG.error(userContext, "Não encontrei a configuração:", e);
			}
			DotumSmartReportExportPDF pdf = new DotumSmartReportExportPDF(this);
			this.fm = pdf.getFileMemory();
		} else if (dotumSmartReport.getType().equalsIgnoreCase("html")) {
			DotumSmartReportExportHTML html = new DotumSmartReportExportHTML(this);
			this.fm = html.getFileMemory();
		}
	}
	private FileMemory createPlanilhaModelo(TransactionDB trans, String code) throws Exception {
		try {
			String sql = "select * from componente where cpt_codigo = '" + code + "'";
			ResultSet rs = trans.execQuery( sql );

			byte[] bytes = null;
			while (rs.next()) {
				bytes = rs.getBytes("cpt_modelo");
			}

			if (bytes != null) {
				FileMemory fm = new FileMemory(bytes);
				fm.setName("t" + code + ".xls");
				return fm;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		} finally {
			trans.closeQuery();
		}

	}

	private void createLogotipo(TransactionDB trans, UserContext userContext) throws Exception {

		StringBuilder sb = new StringBuilder();
		sb.append(" select                                                       ");
		sb.append("                 (case                                        ");
		sb.append("                   when cfgum.cfgum_arquivo is not null       ");
		sb.append("                     then cfgum.cfgum_arquivo                 ");
		sb.append("                     else cfgm.cfgm_arquivo                   ");
		sb.append("                  end) as cfgm_arquivo,                       ");
		sb.append("                 (case                                        ");
		sb.append("                   when cfgum.cfgum_extensao is not null      ");
		sb.append("                     then cfgum.cfgum_extensao                ");
		sb.append("                     else cfgm.cfgm_extensao                  ");
		sb.append("                  end) as cfgm_extensao,                      ");
		sb.append("                 (case                                        ");
		sb.append("                   when cfgum.cfgum_extensao is not null      ");
		sb.append("                     then 'cfgumidia'                         ");
		sb.append("                     else 'cfgmidia'                          ");
		sb.append("                  end) as path                                ");
		sb.append("            from configuracaomidia cfgm                       ");
		sb.append("       left join configuracaounidademidia cfgum on (cfgum.uni_id = "+userContext.getUnidade().getId()+" and cfgum.cfgt_id = cfgm.cfgt_id)");
		sb.append("            where cfgm.cfgt_Id = 20006                        ");


		byte[] bytes = null;
		String extensao = null;
		String path = null;

		try{
			ResultSet rs = trans.execQuery( sb.toString() );
			while (rs.next()) {
				bytes = rs.getBytes("cfgm_arquivo");
				extensao = rs.getString("cfgm_extensao");
				path = rs.getString("path");
			}
		} catch (Exception e) {
			// aqui nao precisa de log para nao registrar o erro no LOG 2x;
		}finally{
			trans.closeQuery();
		}

		logotipoPath = StringUtils.coalesce(userContext.getServerPath(), "").replace("\\", "/") + path + "/logotipo."+extensao;
		File file = new File(logotipoPath);
		if (bytes != null) {
			FileUtils.createFileFromBytes(file, bytes);
		}
	}

	public String getLogotipoPath(){
		return this.logotipoPath;
	}

	//
	// HELPER
	//
	public String getTextFilter() {
		StringBuilder filterText = new StringBuilder();
		if (getDotumSmartReport().getParms() != null) {
			String temp = "";
			for (int i = 0; i < getDotumSmartReport().getParms().length; i++ ) {
				Parm drp = getDotumSmartReport().getParms()[i];

				// só mostrar o que realmente for pra mostrar
				if (drp.getName().equalsIgnoreCase("PARAM_ORDER")) continue;
				if (drp.isShowFilterOnReport() == false) continue;
				if (getParmsValues() == null) continue;


				Object value = getParmsValues().get(drp.getName());
				String valueStr = getParmsValuesStr().get(drp.getName());

				if (StringUtils.hasValue(valueStr) == true) {
					filterText.append( temp );
					filterText.append( getDotumSmartReport().getParms()[i].getLabel() + " => ");
					filterText.append( valueStr );						
				} else {
					if (value instanceof Date) {
						if (((Date)value).equals(DateUtils.getFirstYear())) continue;
						if (((Date)value).equals(DateUtils.getLastYear())) continue;
					}
					if ((value != null) && (value.equals("null") == false)) {
						filterText.append( temp );

						String labelTemp = null;
						try {
							labelTemp = getDotumSmartReport().getParms()[i].getLabel();
						} catch (Exception e) {
							labelTemp = getDotumSmartReport().getParms()[i].getLabel();
						}
						filterText.append( labelTemp + " => ");
						String valueShow = null;
						if ( value instanceof String) {
							valueShow = (String)value;
						} else if ( value instanceof Date) {
							valueShow = FormatUtils.formatDate((Date)value);
						} else if ( value instanceof Integer) {
							valueShow = ((Integer)value).toString();
						} else if ( value instanceof Double) {
							valueShow = FormatUtils.formatDouble((Double)value);
						}
						filterText.append( valueShow );						
					}
				}
				temp = " | ";
			}
		}

		return filterText.toString();
	}

	public String getTextTitle() {
		String codigo = getCode();
		if (StringUtils.hasValue(codigo)) codigo += ": ";
		return codigo + getDotumSmartReport().getTitle();
	}

	public String getTextFooterLeft() {
		String impressao = "[impressão: "+ DateUtils.getDateAndTime(getUserContext().getDataAtual()) +"]"; 
		String unidade = "[unidade: "+getUserContext().getUnidade().getId()+"]";
		String usuario = "[usuário: "+getUserContext().getUser().getNome().toLowerCase()+"]";
		String sistema = "[programa: "+getReportInfo().getAplicacao()+"]";        
		String versao = "[versão: "+getReportInfo().getVersao()+"]";
		String relatorio = "[relatório: "+ getReportInfo().getReportName() +"]";

		StringBuilder reportInfo = new StringBuilder();
		reportInfo.append(impressao);
		reportInfo.append(unidade);
		reportInfo.append(usuario);
		//		sb.append("\n");
		reportInfo.append(sistema);
		reportInfo.append(versao);
		reportInfo.append(relatorio);

		// dados do contexto do relatorio
		return reportInfo.toString();
	}

	public FileMemory getFileMemory() {
		return fm;
	}
}


