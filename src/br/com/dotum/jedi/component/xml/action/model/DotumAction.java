package br.com.dotum.jedi.component.xml.action.model;

public class DotumAction implements java.io.Serializable  {
	
	
	private static final long serialVersionUID = 5180056036713645797L;
	
	private Integer id;
	private String packageStr;
	private String code;
	private String split;
	private String label;
	private String type;
	private Integer group;
	private String icon;
	private String quantitySelected;
	private Integer version;
	private Boolean actived;
	private Boolean onlyDotumUser;
	private String[] imports;
	private String[] steps;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPackageStr() {
		return packageStr;
	}
	public void setPackageStr(String packageStr) {
		this.packageStr = packageStr;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getSplit() {
		return split;
	}
	public void setSplit(String split) {
		this.split = split;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getGroup() {
		return group;
	}
	public void setGroup(Integer group) {
		this.group = group;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getQuantitySelected() {
		return quantitySelected;
	}
	public void setQuantitySelected(String quantitySelected) {
		this.quantitySelected = quantitySelected;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public Boolean getActived() {
		return actived;
	}
	public void setActived(Boolean actived) {
		this.actived = actived;
	}
	public Boolean getOnlyDotumUser() {
		return onlyDotumUser;
	}
	public void setOnlyDotumUser(Boolean onlyDotumUser) {
		this.onlyDotumUser = onlyDotumUser;
	}
	public String[] getImports() {
		return imports;
	}
	public void setImports(String[] imports) {
		this.imports = imports;
	}
	public String[] getSteps() {
		return steps;
	}
	public void setSteps(String[] steps) {
		this.steps = steps;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}