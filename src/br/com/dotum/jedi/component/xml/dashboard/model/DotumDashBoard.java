package br.com.dotum.jedi.component.xml.dashboard.model;

public class DotumDashBoard implements java.io.Serializable  {

	
	private static final long serialVersionUID = -6365260539349574678L;
	private String title;
	private String type;
	private Integer version;
	private String SQL;
	private String colors;
	private DotumDashBoardParm[] parmsFields;
	private DotumDashBoardData[] data;
	private DotumDashBoardColumn[] columns;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getSQL() {
		return SQL;
	}

	public void setSQL(String sQL) {
		SQL = sQL;
	}

	public DotumDashBoardParm[] getParms() {
		return parmsFields;
	}

	public void setParms(DotumDashBoardParm[] parms) {
		this.parmsFields = parms;
	}

	public DotumDashBoardData[] getData() {
		return data;
	}

	public void setData(DotumDashBoardData[] data) {
		this.data = data;
	}

	public DotumDashBoardColumn[] getColumns() {
		return columns;
	}
	
	public void setColumns(DotumDashBoardColumn[] columns) {
		this.columns = columns;
	}

	public String getColors() {
		return colors;
	}

	public void setColors(String colors) {
		this.colors = colors;
	}
	
}