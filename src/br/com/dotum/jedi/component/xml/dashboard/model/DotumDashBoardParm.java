package br.com.dotum.jedi.component.xml.dashboard.model;

import java.text.ParseException;

import br.com.dotum.jedi.util.FormatUtils;

public class DotumDashBoardParm {

	private String column;
	private String name;
	private String value;
	private Boolean prompt;
	private String label;
	private String type; /*TIPO DO CAMPO - HIDDENFIELD, COMBOBOXDBFIELD, TEXTFIELD...*/
	private Boolean required;
	private String chave;
	private Boolean showFilterOnReport = true;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Boolean isPrompt() {
		return prompt;
	}

	public void setPrompt(Boolean prompt) {
		this.prompt = prompt;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean isRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public Boolean isShowFilterOnReport() {
		return showFilterOnReport;
	}

	public void setShowFilterOnReport(Boolean showFilterOnReport) {
		this.showFilterOnReport = showFilterOnReport;
	}

	public void setColumn(String column) {
		this.column = column;
	}
	
	public Integer getColumn() throws ParseException {
		return FormatUtils.parseInt(this.column);
	}
}