package br.com.dotum.jedi.component.xml.manager.model;


import java.util.List;

import br.com.dotum.jedi.core.table.Bean;

public class DotumManager implements java.io.Serializable  {
	
	
	private static final long serialVersionUID = 5180056036713645797L;
	
	private Integer id;
	private String packageStr;
	private String code;
	private Integer padrao;
	private String title;
	private Integer version;
	private Integer limit;
	private Boolean basket;
	private Integer mode;
	private String SQL;
	private DotumManagerParm[] parmsFields;
	private DotumManagerColumn[] column;
	private DotumManagerRelation[] relation;
	private DotumManagerProcess process;
	private List<Bean> beanList;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPackageStr() {
		return packageStr;
	}
	public void setPackageStr(String packageStr) {
		this.packageStr = packageStr;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getPadrao() {
		return padrao;
	}
	public void setPadrao(Integer padrao) {
		this.padrao = padrao;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Boolean getBasket() {
		return basket;
	}
	public void setBasket(Boolean basket) {
		this.basket = basket;
	}
	public Integer getMode() {
		return mode;
	}
	public void setMode(Integer mode) {
		this.mode = mode;
	}
	public String getSQL() {
		return SQL;
	}
	public void setSQL(String sQL) {
		SQL = sQL;
	}
	public DotumManagerParm[] getParms() {
		return parmsFields;
	}
	public void setParms(DotumManagerParm[] parmsFields) {
		this.parmsFields = parmsFields;
	}
	public DotumManagerColumn[] getColumn() {
		return column;
	}
	public void setColumn(DotumManagerColumn[] column) {
		this.column = column;
	}
	public DotumManagerRelation[] getRelation() {
		return relation;
	}
	public void setRelation(DotumManagerRelation[] relation) {
		this.relation = relation;
	}
	public DotumManagerProcess getProcess() {
		return process;
	}
	public void setProcess(DotumManagerProcess process) {
		this.process = process;
	}
	public List<Bean> getBeanList() {
		return beanList;
	}

	public void setBeanList(List<Bean> beansList) {
		this.beanList = beansList;
	}

}