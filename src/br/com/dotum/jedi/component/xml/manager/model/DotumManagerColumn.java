package br.com.dotum.jedi.component.xml.manager.model;

public class DotumManagerColumn {
	
	private String data; /* key ou param */
	private String label;
	private String name;
	private String aliasTable;
	private String nameBean;
	private String type;
	private Boolean showInList;
	private Boolean visible;
	private String summary;
	
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAliasTable() {
		return aliasTable;
	}
	public void setAliasTable(String aliasTable) {
		this.aliasTable = aliasTable;
	}
	public String getNameBean() {
		return nameBean;
	}
	public void setNameBean(String nameBean) {
		this.nameBean = nameBean;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean isShowInList() {
		return showInList;
	}
	public void setShowInList(Boolean showInList) {
		this.showInList = showInList;
	}
	public Boolean isVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
}