package br.com.dotum.jedi.component.xml.manager.model;
public class DotumManagerParm {

	private String name;
	private String value;
	private String prompt;
	private String label;
	private String type; /*TIPO DO CAMPO - HIDDENFIELD, COMBOBOXDBFIELD, TEXTFIELD...*/
	private Boolean required;
	private String chave;
	private String condition;
	private Boolean dinamic;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean isRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Boolean isDinamic() {
		return dinamic;
	}

	public void setDinamic(Boolean dinamic) {
		this.dinamic = dinamic;
	}

}