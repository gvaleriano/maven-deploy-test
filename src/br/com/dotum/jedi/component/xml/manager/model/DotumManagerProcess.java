package br.com.dotum.jedi.component.xml.manager.model;

public class DotumManagerProcess {

	private DotumManagerAfterOpen[] afterOpen;
	
	public DotumManagerAfterOpen[] getAfterOpen() {
		return afterOpen;
	}

	public void setAfterOpen(DotumManagerAfterOpen[] afterOpen) {
		this.afterOpen = afterOpen;
	}
}
