package br.com.dotum.jedi.component.xml.manager.model;

public class DotumManagerRelation {

	private String title;
	private String masterProperties;
	private String detailProperties;
	private String SQL;
	private DotumManagerColumn[] column;
	private DotumManagerProcess process;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMasterProperties() {
		return masterProperties;
	}
	public void setMasterProperties(String masterProperties) {
		this.masterProperties = masterProperties;
	}
	public String getDetailProperties() {
		return detailProperties;
	}
	public void setDetailProperties(String detailProperties) {
		this.detailProperties = detailProperties;
	}
	public String getSQL() {
		return SQL;
	}
	public void setSQL(String sQL) {
		SQL = sQL;
	}
	public DotumManagerColumn[] getColumn() {
		return column;
	}
	public void setColumn(DotumManagerColumn[] column) {
		this.column = column;
	}

	public DotumManagerProcess getProcess() {
		return process;
	}
	public void setProcess(DotumManagerProcess process) {
		this.process = process;
	}
	
}
