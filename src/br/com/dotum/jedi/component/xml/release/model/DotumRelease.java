package br.com.dotum.jedi.component.xml.release.model;

import java.text.ParseException;
import java.util.Date;

import br.com.dotum.jedi.util.FormatUtils;


public class DotumRelease {

	private Integer release;
	private String type;
	private String version;
	private String application;
	private String description;
	private String observation;
	private String createStr;
	
	private String changedFile;
	private String deletedFile;
	private String scriptFile;
	
	private String dependentRelease;
	private String developerBy;
	private String visible;
	private String time;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public Integer getRelease() {
		return release;
	}
	public void setRelease(Integer release) {
		this.release = release;
	}
	public String getCreateStr() {
		return createStr;
	}
	public void setCreateStr(String createStr) {
		this.createStr = createStr;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDeveloperBy() {
		return developerBy;
	}
	public void setDeveloperBy(String developerBy) {
		this.developerBy = developerBy;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getChangedFile() {
		return changedFile;
	}
	public void setChangedFile(String changedFile) {
		this.changedFile = changedFile;
	}
	public String getDeletedFile() {
		return deletedFile;
	}
	public void setDeletedFile(String deletedFile) {
		this.deletedFile = deletedFile;
	}
	public String getScriptFile() {
		return scriptFile;
	}
	public void setScriptFile(String scriptFile) {
		this.scriptFile = scriptFile;
	}
	public String getDependentRelease() {
		return dependentRelease;
	}
	public void setDependentRelease(String dependentRelease) {
		this.dependentRelease = dependentRelease;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	public Date getCreate() throws ParseException {
		return FormatUtils.parseDateWithFormat(createStr, "yyyy-MM-dd");
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
}
