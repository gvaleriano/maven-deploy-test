package br.com.dotum.jedi.component.xml.smartmanager.model;

public class Column {
	
	private String typeProperty;
	private String nameProperty;
	private String nameColumn;
	private String label;
	private Boolean showInList;
	private Boolean showInDetail;
	private String data; /*key or param*/
	
	
	public String getTypeProperty() {
		return typeProperty;
	}
	public void setTypeProperty(String typeProperty) {
		this.typeProperty = typeProperty;
	}
	public String getNameProperty() {
		return nameProperty;
	}
	public void setNameProperty(String nameProperty) {
		this.nameProperty = nameProperty;
	}
	public String getNameColumn() {
		return nameColumn;
	}
	public void setNameColumn(String nameColumn) {
		this.nameColumn = nameColumn;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Boolean getShowInList() {
		return showInList;
	}
	public void setShowInList(Boolean showInList) {
		this.showInList = showInList;
	}
	public Boolean getShowInDetail() {
		return showInDetail;
	}
	public void setShowInDetail(Boolean showInDetail) {
		this.showInDetail = showInDetail;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}