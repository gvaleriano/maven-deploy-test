package br.com.dotum.jedi.component.xml.smartmanager.model;

public class Parm {

	private String nameProperty;
	private String label;
	private Boolean required = false;
	private String chave;
	private String condition;
	
	
	public String getNameProperty() {
		return nameProperty;
	}
	public void setNameProperty(String nameProperty) {
		this.nameProperty = nameProperty;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Boolean getRequired() {
		return required;
	}
	public void setRequired(Boolean required) {
		this.required = required;
	}
	public String getChave() {
		return chave;
	}
	public void setChave(String chave) {
		this.chave = chave;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
}