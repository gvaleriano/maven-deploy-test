package br.com.dotum.jedi.component.xml.smartmanager.model;

import br.com.dotum.jedi.component.xml.manager.model.DotumManagerProcess;

public class Relation {

	private String title;
	private String masterProperties;
	private String detailProperties;
	private String SQL;
	private Column[] column;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMasterProperties() {
		return masterProperties;
	}
	public void setMasterProperties(String masterProperties) {
		this.masterProperties = masterProperties;
	}
	public String getDetailProperties() {
		return detailProperties;
	}
	public void setDetailProperties(String detailProperties) {
		this.detailProperties = detailProperties;
	}
	public String getSQL() {
		return SQL;
	}
	public void setSQL(String sQL) {
		SQL = sQL;
	}
	public Column[] getColumn() {
		return column;
	}
	public void setColumn(Column[] column) {
		this.column = column;
	}
}
