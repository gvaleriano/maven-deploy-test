package br.com.dotum.jedi.component.xml.smartreport.builder;

import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.component.xml.smartreport.model.Column;
import br.com.dotum.jedi.component.xml.smartreport.model.SectionDataBean;
import br.com.dotum.jedi.file.layout.FileMemory;


public abstract class AbstractDotumSmartReport {

	public static String numberDefaultFormat = "#,##0";
	public static String doubleDefaultFormat = "#,##0.00";
	public static String percentDefaultFormat = "#,##0%";
	public static String dateDefaultFormat = "dd/MM/yyyy";

	
	private DotumSmartReportBuilder reportBuilder;
	private SectionDataBean[] sessionData;
	private FileMemory fm;

	public AbstractDotumSmartReport(DotumSmartReportBuilder reportBuilder) throws Exception {
		this.reportBuilder = reportBuilder;
		this.sessionData = reportBuilder.getDotumSmartReport().getSectionDataList();
	}
	
	protected abstract void createReport() throws Exception;

	public DotumSmartReportBuilder getReportBuilder() {
		return reportBuilder;
	}

	public void setReportBuilder(DotumSmartReportBuilder reportBuilder) {
		this.reportBuilder = reportBuilder;
	}

	public SectionDataBean[] getSessionData() {
		return sessionData;
	}

	public void setSessionData(SectionDataBean[] sessionData) {
		this.sessionData = sessionData;
	}

	public FileMemory getFileMemory() {
		return fm;
	}

	public void setFileMemory(FileMemory fm) {
		this.fm = fm;
	}
	
	public static boolean isColumnGroup(Column column) {
		if (column.getGroup() == null) return false;
		if (column.getGroup().equals("0")) return false;
		if (column.getGroup().equals("1")) return true;
		if (column.getGroup().equals("2")) return true;
		return false;
	}
	public static boolean isColumnGroup(String group) {
		if (group == null) return false;
		if (group.equals("0")) return false;
		if (group.equals("1")) return true;
		if (group.equals("2")) return true;
		if (group.equals("3")) return true;
		if (group.equals("4")) return true;
		if (group.equals("5")) return true;
		return false;
	}


	
}
