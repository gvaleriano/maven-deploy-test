package br.com.dotum.jedi.component.xml.smartreport.builder;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.component.xml.smartreport.model.Column;
import br.com.dotum.jedi.component.xml.smartreport.model.Section;
import br.com.dotum.jedi.component.xml.smartreport.model.SectionDataBean;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;


public abstract class DotumSmartReportExportCommons extends AbstractDotumSmartReport {

	private static final String PL = "\n";

	public DotumSmartReportExportCommons(DotumSmartReportBuilder reportBuilder) throws Exception {
		super(reportBuilder);
	}

	protected abstract void createReport() throws Exception;

	public boolean isReportText() {
		for (int i = 0; i < getSessionData().length; i++) {
			if (getSessionData()[i].getType().equals("text")) return true;
		}
		return false;
	}

	//
	// HTML
	//
	protected String createReportHtml() throws Exception {
		StringBuilder content = new StringBuilder();

		boolean temDados = false;
		boolean temCss = false;
		for (int i = 0; i < getSessionData().length; i++) {
			SectionDataBean session = getSessionData()[i];
			Section section = getReportBuilder().getDotumSmartReport().getSections()[i];
			if (session.getModel().size() > 0) temDados = true;

			
			
			if(session.getInformation().equalsIgnoreCase("startNewPage")) {
				content.append("<div class=\"breakpage\"></div>" + PL);
				content.append("<div class=\"bold\">");
				content.append(session.getTitle());
				content.append("</div>");
			} else {
				if(StringUtils.hasValue(session.getTitle())){
					content.append("<br />");
					content.append("<div class=\"bold\">");
					content.append(session.getTitle());
					content.append("</div>");
				}
			}


			//
			// TABULAR
			//
			if (session.getType().equals("tabular")) {
				createTabularSession(session, section, content);
				content.append("<br/>");
			}

			//
			// TEXT
			//
			if (session.getType().equals("text")) {
				createTextSession(session, section, content);
				content.append("<br/>");
			}

			//
			// CROSSTAB
			//
			if (session.getType().equals("crosstab")) {
				if(temCss == false){
					temCss = true;
					content.append("  <style type=\"text/css\" >" + PL);
					//					content.append(getCss());
					content.append("  </style>" + PL);
				}
				createCrossTabSession(session, section, content);
				content.append("<br/>");
			}
		}
		if (temDados == false) {
			content.append( getSemDados() );
		}
		String htmlStr = content.toString();
		return htmlStr;
	}

	public String getCss(){
		StringBuilder cssStr = new StringBuilder();

		cssStr.append("    * {font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 8px;padding:0;margin:0;} " + PL);
		cssStr.append("    table {border:0px; width: 100%;border-spacing:0;border-collapse: collapse;} " + PL);
		cssStr.append("    td {padding:3px;} " + PL);
		cssStr.append("    th {padding:3px;font-weight: bold;} " + PL);
		cssStr.append("    p {text-align: justify;}" + PL);

		cssStr.append("    .bold     {font-weight: bold;} " + PL);

		cssStr.append("    .left     {text-align: left;} " + PL);
		cssStr.append("    .center   {text-align: center;} " + PL);
		cssStr.append("    .right    {text-align: right;} " + PL);
		cssStr.append("    .justify  {text-align: justify;} " + PL);
		cssStr.append("    .breakpage {page-break-after: always} " + PL);

		cssStr.append("    .label {display:block;font-size: 9px; padding-bottom:3px;font-weight: bold;} " + PL);
		cssStr.append("    .value {display:block;font-size: 11px;padding-left:5px;} " + PL);
		cssStr.append("    .spfw {padding:2px 4px;} " + PL);


		cssStr.append("    .border-sm, .border-sm > tr > td, .border-sm > tr > th, .border-sm > tr {border: 0.1px solid #333;} " + PL);
		cssStr.append("    .border-md, .border-1 {border: 1px solid #000;} " + PL);
		cssStr.append("    .border-lg, .border-2 {border: 2px solid #000;} " + PL);

		cssStr.append("    .indent-1 {text-indent: 1em;} " + PL);
		cssStr.append("    .indent-2 {text-indent: 2em;} " + PL);
		cssStr.append("    .indent-3 {text-indent: 3em;} " + PL);
		cssStr.append("    .indent-4 {text-indent: 4em;} " + PL);
		cssStr.append("    .indent-5 {text-indent: 5em;} " + PL);

		cssStr.append("    .font-8 {font-size:8px;} " + PL);
		cssStr.append("    .font-10 {font-size:10px;} " + PL);
		cssStr.append("    .font-12 {font-size:12px;} " + PL);
		cssStr.append("    .font-14 {font-size:14px;} " + PL);
		cssStr.append("    .font-16 {font-size:16px;} " + PL);
		cssStr.append("    .font-18 {font-size:18px;} " + PL);
		cssStr.append("    .font-20 {font-size:20px;} " + PL);
		cssStr.append("    .font-22 {font-size:22px;} " + PL);
		cssStr.append("    .font-24 {font-size:24px;} " + PL);
		cssStr.append("    .font-26 {font-size:26px;} " + PL);
		cssStr.append("    .font-28 {font-size:28px;} " + PL);
		cssStr.append("    .font-30 {font-size:30px;} " + PL);

		cssStr.append("    .line-100 {line-height:100%;} " + PL);
		cssStr.append("    .line-150 {line-height:150%;} " + PL);
		cssStr.append("    .line-200 {line-height:200%;} " + PL);
		cssStr.append("    .line-250 {line-height:250%;} " + PL);
		cssStr.append("    .line-300 {line-height:300%;} " + PL);

		cssStr.append("    .margin-side-10 {padding:0 10px;} " + PL);
		cssStr.append("    .margin-side-20 {padding:0 20px;} " + PL);
		cssStr.append("    .margin-side-30 {padding:0 30px;} " + PL);
		cssStr.append("    .margin-side-40 {padding:0 40px;} " + PL);
		cssStr.append("    .margin-side-50 {padding:0 50px;} " + PL);
		cssStr.append("    .margin-side-60 {padding:0 60px;} " + PL);
		cssStr.append("    .margin-side-70 {padding:0 70px;} " + PL);
		cssStr.append("    .margin-side-80 {padding:0 80px;} " + PL);
		cssStr.append("    .margin-side-90 {padding:0 90px;} " + PL);
		cssStr.append("    .margin-side-100 {padding:0 100px;} " + PL);
		cssStr.append("    .margin-side-120 {padding:0 120px;} " + PL);
		cssStr.append("    .margin-side-140 {padding:0 140px;} " + PL);
		cssStr.append("    .margin-side-160 {padding:0 160px;} " + PL);
		cssStr.append("    .margin-side-180 {padding:0 180px;} " + PL);
		cssStr.append("    .margin-side-200 {padding:0 200px;} " + PL);

		cssStr.append("    .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12 {float: left;}" + PL);
		cssStr.append("    .col-1 {width:8.333%;} " + PL);
		cssStr.append("    .col-1 {width:8.333%;} " + PL);
		cssStr.append("    .col-2 {width:16.666%;} " + PL);
		cssStr.append("    .col-3 {width:25%;} " + PL);
		cssStr.append("    .col-4 {width:33.333%;} " + PL);
		cssStr.append("    .col-5 {width:41.666%;} " + PL);
		cssStr.append("    .col-6 {width:50%;}" + PL);
		cssStr.append("    .col-7 {width:58.333%;} " + PL);
		cssStr.append("    .col-8 {width:66.666%;} " + PL);
		cssStr.append("    .col-9 {width:75%;}" + PL);
		cssStr.append("    .col-10 {width:83.333%;} " + PL);
		cssStr.append("    .col-11 {width:91.666%;} " + PL);
		cssStr.append("    .col-12 {width:100%;} " + PL);
		cssStr.append("    .row{clear:both;} " + PL);


		for (int i = 0; i < getSessionData().length; i++) {
			Section section = getReportBuilder().getDotumSmartReport().getSections()[i];
			String style = section.getStyle();
			if (StringUtils.hasValue(style))
				cssStr.append( style + "" + PL);
		}
		return cssStr.toString();
	}



	// TEXT
	private void createTextSession(SectionDataBean session, Section section, StringBuilder content) throws Exception {

		Integer cloneContent = section.getCloneContent();
		if(cloneContent == null) cloneContent = 0;
		List<Object[]> model = session.getModel();
		Column[] columns  = section.getVariable().getColumns();
		//
		String group = "";
		String labelSize = section.getTable();
		int column = 1;
		int qtdRegisters = -1;
		if (labelSize != null) {
			column = FormatUtils.parseInt(StringUtils.getNumberOnly(labelSize.split("x")[0]));
			qtdRegisters = (FormatUtils.parseInt(StringUtils.getNumberOnly(labelSize.split("x")[1])) * column);
		}

		int size = 100 / column;
		int pStartGroup = -1;
		int pEndGroup = -1;


		int k = 0;
		HashMap<String, Object> variableValues = null;

		if (column > 1) {
			content.append("<table cellpadding=\"8\">" + PL);
		}

		for(int x = 0; x < model.size(); x++){
			Object[] row = model.get(x);
			HashMap<String, Object> currentVariableValues = new HashMap<String, Object>();
			Object[] objs = (Object[]) setVariablesValues(columns, row);
			currentVariableValues = (HashMap<String, Object>) objs[1];
			//
			String currentGroup = (String) objs[0];
			if(StringUtils.hasValue(currentGroup) == false) currentGroup = ""+x;


			String text = null;
			if(x == 0){
				if (StringUtils.hasValue(section.getHeaderText())) {
					text = SQLUtils.parseQuery(section.getHeaderText(), currentVariableValues);
					text = replaceSpecialCharacterToHtml(text);
					content.append("   <div class=\"header\">" + PL);
					content.append(      text);
					content.append(PL + "</div>" + PL);
				}
				if (StringUtils.hasValue(section.getFooterText())) {
					text = SQLUtils.parseQuery(section.getFooterText(), currentVariableValues);
					text = replaceSpecialCharacterToHtml(text);
					content.append("   <div class=\"footer\">" + PL);
					content.append(      text);
					content.append(PL + "</div>" + PL);
				}
			}

			if((x > 0) && (currentGroup.equals(group) == false)){
				text = SQLUtils.parseQuery(StringUtils.coalesce(section.getContentFooterText(), ""), variableValues);
				text = replaceSpecialCharacterToHtml(text);
				content.append(text+ PL);
				content.append("</div>" + PL);
				//
				pEndGroup = (x-1);
				createCloneContent(section, columns, model, content, cloneContent, pStartGroup, pEndGroup);
				//
				pStartGroup = -1;
				pEndGroup = -1;
				//
				content.append("<div class=\"breakpage\"></div>" + PL);
			}

			if(currentGroup.equals(group) == false){
				text = SQLUtils.parseQuery(StringUtils.coalesce(section.getContentHeaderText(), ""), currentVariableValues);
				text = replaceSpecialCharacterToHtml(text);

				if (column == 1)
					content.append("<div class=\"content\">" + PL);

				content.append(    text);
				content.append(    PL);
				group = currentGroup;
				pStartGroup = x;
			}

			if (column > 1) {

				if (x % column == 0) {
					if (x == 0) {
						content.append("<tr>");
					} else {
						content.append("</tr><tr>");
					}
				}

				content.append("<td width=\"" +size + "%\">" + PL);

				// content.append("<div style=\"display:block;float:left;width: "+ size +"%\">" + PL);
			}

			//ADICIONA O DETAIL SE O VALOR DE GROUP FOR IGUAL AO ANTERIOR
			if(currentGroup.equals(group)){
				text = SQLUtils.parseQuery(StringUtils.coalesce(section.getDetailText(), ""), currentVariableValues);
				text = replaceSpecialCharacterToHtml(text);
				content.append(    text);
				content.append(    PL);
			}

			if (column > 1) {
				content.append("</td>" + PL);
				// content.append("</div>" + PL);
			}

			++k;
			if ((column > 1) && (k % qtdRegisters == 0) && (k < model.size())) {
				content.append("</tr></table>" + PL);
				content.append("<div class=\"breakpage\" style=\"clear:both;\"></div>" + PL);
				content.append("<table><tr>" + PL);
			}

			//ISSO AQUI É QUANDO FOR A ULTIMA LINHA ELE FINALIZA O DOCUMENTO
			if( x == (model.size()-1) ){
				text = SQLUtils.parseQuery(StringUtils.coalesce(section.getContentFooterText(), ""), currentVariableValues);
				text = replaceSpecialCharacterToHtml(text);
				content.append(text+ PL);

				if (column == 1) 
					content.append("</div>" + PL);
				//
				pEndGroup = x;
				createCloneContent(section, columns, model, content, cloneContent, pStartGroup, pEndGroup);
			}

			variableValues = currentVariableValues;
		}

		if (column > 1) {
			content.append("</tr></table>" + PL);
		}

	}

	// TABULAR
	private HashMap<String, Double> summaryReport = new HashMap<String, Double>();
	private HashMap<String, Double> summaryGroup1 = new HashMap<String, Double>();
	private HashMap<String, Double> summaryGroup2 = new HashMap<String, Double>();
//	private HashMap<String, Double> summaryGroup3 = new HashMap<String, Double>();
//	private HashMap<String, Double> summaryGroup4 = new HashMap<String, Double>();
//	private HashMap<String, Double> summaryGroup5 = new HashMap<String, Double>();

	private void createTabularSession(SectionDataBean session, Section section, StringBuilder content) throws Exception {
		List<Object[]> model = session.getModel();


		//		content.append(addTopBottomReport());

		Column[] columns = section.getTabular().getColumns();
		int colSpanInt = 0;
		for (int j = 0; j < columns.length; j++) {
			Column col = columns[j];

			String preview = col.getPreview();
			boolean visible = col.getVisible();
			String group = col.getGroup();

			if  (visible == false) continue;
			if  (preview.equalsIgnoreCase("true")) continue;
			if  (super.isColumnGroup(group)) continue;

			colSpanInt++;
		}

		boolean hasPreview = section.hasPreview();
		if (hasPreview) {
			Integer previewParm = 0;

			// TODO ver isto
			Object obj = getReportBuilder().getParmsValues().get("preview");
			if (obj instanceof Integer) {
				previewParm = (Integer)obj;				
			} else if (obj instanceof String) {
				String previewParmStr = (String)obj;
				if (previewParmStr.equals("1")) {
					previewParm = 1;
				} 
			} 

			hasPreview = previewParm.equals(1);
		}


		int qtdGroups = 0;
		int qtdGroupsToSummary = 0;
		for (int j = 0; j < columns.length; j++) {
			Column col = columns[j];
			String group = col.getGroup();
			if  (super.isColumnGroup(group) == false) continue;

			qtdGroups++;
			qtdGroupsToSummary++;
		}
		qtdGroups *= 15;

		int firstColumn = 0;
		for (int j = 0; j < columns.length; j++) {
			if (firstColumn == 0) firstColumn = j;
		}



		content.append("<table class=\"table table-striped table-hover dataTable no-footer\">" + PL);
		content.append("<tr>");
		content.append( criaTituloColuna(model, columns)  );
		content.append("</tr>");


		String columnGroup1 = null;
		String columnGroup2 = null;
		String lastDataRowGroup1 = "null";
		String lastDataRowGroup2 = "null";
		boolean firstRow = true;
		boolean firstRowGroup1 = true;
		for (int i = 0; i < model.size(); i++) {
			boolean zebra = !(i % 2 == 0);
			String cssStyleTR = "";
			if (zebra == false) {
				cssStyleTR += "background:#eee;";
			}
			content.append("<tr style=\""+cssStyleTR+"\">");

			Object[] line = model.get(i);

			// 
			// AQUI ESTAMOS MONTANDO AS COLUNAS  
			// 
			String previewValue = null;
			for (int j = 0; j < columns.length; j++) {
				Column col = columns[j];
				Object value = line[j];

				String column = col.getName();
				String format = col.getFormat();
				String summaryType = col.getSummary();
				String group = col.getGroup();
				String preview = col.getPreview();
				boolean visible = col.getVisible();
				String startNewPage = col.getStartNewPage();

				if  (visible == false) continue;
				if  (preview.equalsIgnoreCase("true")) {
					previewValue = (String) value;
					continue;
				}

				// AQUI VOU CRIAR OS GRUPOS CASO TENHA E SEJA ELE
				String valueGroup = "";

				// eh coluna de grupo
				boolean isColumnGroup = super.isColumnGroup(group);
				if (isColumnGroup == true) {
					// converter o valor para string
					valueGroup = formatShowValue(value, format);

					// se for grupo 1
					if (group.equals("1")) {
						columnGroup1 = column;
						if ((lastDataRowGroup1 != null) || (firstRow == true)) {

							if (lastDataRowGroup1.equals(valueGroup) == false) {
								lastDataRowGroup2 = "null";

								// cria o sumario do grupo quando nao for a primeira linha
								// pois no caso da primeiralinha eh só o cabeçalho
								if (firstRow == false) {
									content.append("</tr>\n\n" + PL);
									content.append( createGroupSummary(2, columns, summaryGroup2) );
									content.append("<tr style=\"padding-left: "+qtdGroups+"px;"+cssStyleTR+"\">");
									//									document.add( createGroupSummary("a", "100", summaryGroup2) );
									// somente se tem um segundo grupo
									if (columnGroup2 != null) {
										content.append("</tr>\n\n" + PL);
										content.append( createGroupSummary(1, columns, summaryGroup1) );
										content.append("<tr style=\"padding-left: "+qtdGroups+"px;"+cssStyleTR+"\">");
									}

									if(startNewPage.equalsIgnoreCase("true")){
										content.append("<div class=\"breakpage\"></div>" + PL);
										content.append( criaTituloColuna(model, columns)  );
									}
								}	

								// cria o header do novo grupo
								content.append("</tr>\n\n" + PL);
								content.append("<tr><td colspan=\""+colSpanInt+"\" style=\"padding:10px 0 0 0;\" class=\"bold grupo1\">");
								content.append(valueGroup);
								content.append("</td></tr>" + PL);
								content.append("<tr style=\"padding-left: "+qtdGroups+"px;"+cssStyleTR+"\">");

								firstRowGroup1 = true;
							}
						}
						//
						// se for grupo 2
						//
					} else if (group.equals("2")) {
						columnGroup2 = column;
						if ((lastDataRowGroup2 != null) || (firstRow == true)) {

							if (lastDataRowGroup2.equals(valueGroup) == false) {

								// cria o sumario do grupo quando nao for a primeira linha
								// pois no caso da primeiralinha eh só o cabeçalho
								if (firstRowGroup1 == false) {
									content.append("</tr>\n\n" + PL);
									content.append( createGroupSummary(2, columns, summaryGroup2) );
									content.append("<tr style=\"padding-left: "+qtdGroups+"px;"+cssStyleTR+"\">");
								}	
								if(firstRow == false && startNewPage.equalsIgnoreCase("true")){
									content.append("<div class=\"breakpage\"></div>" + PL);
								}

								// cria o header do novo grupo
								content.append("</tr>\n\n" + PL);
								content.append("<tr><td colspan=\""+colSpanInt+"\" style=\"padding:2px 0 0 15px;\" class=\"bold grupo2\">");
								content.append(valueGroup);
								content.append("</td></tr>" + PL);
								content.append("<tr style=\"padding-left: "+qtdGroups+"px;"+cssStyleTR+"\">");
							}
						}
					}
				}

				// EM SEGUIDA COLOCA A INFORMACAO NA CELULA
				if (isColumnGroup == false) {
					String cssAlign = "";
					if (value instanceof Double) {
						cssAlign = "right";
					} else if (value instanceof Integer) {
						cssAlign = "right";
					} else if (value instanceof Date) {
						cssAlign = "center";
					} else {
						cssAlign = "left";
					}

					String xxx = "";
					if (j == firstColumn) {
						xxx = " style=\"padding-left: "+qtdGroups+"px;\"";
					}
					content.append( "<td class=\"spfw "+ cssAlign +"\""+xxx+">" );
					calculateRow( column, summaryType, value );
					content.append( formatShowValue(value, format) );
					content.append( "</td>" );
				}

				// AGORA VOU VER SE MUDOU A INFORMACAO DO GRUPO
				if (isColumnGroup == true) {
					if (group.equals("1")) lastDataRowGroup1 = valueGroup;
					if (group.equals("2")) lastDataRowGroup2 = valueGroup;
				}

			}
			content.append("</tr>" + PL);


			firstRow = false;
			firstRowGroup1 = false;

			//
			// AQUI É O PREVIEW
			//
			if (hasPreview == true){
				if (StringUtils.hasValue(previewValue)) {
					content.append("<tr style=\""+cssStyleTR+"\">");
					content.append("<td style=\"padding-left: "+qtdGroups+"px;\" colspan=\""+ colSpanInt +"\">");
					content.append( formatShowValue(previewValue, null) );
					content.append("</td>");
					content.append("</tr>" + PL);
				}
			}
		}


		// soma do grupo 2
		if (qtdGroupsToSummary == 2) {
			content.append( createGroupSummary(3, columns, summaryGroup2) );
			content.append( createGroupSummary(2, columns, summaryGroup1) );
			content.append( createGroupSummary(1, columns, summaryReport) );
		} else if (qtdGroupsToSummary == 1) {
			content.append( createGroupSummary(2, columns, summaryGroup2) );
			content.append( createGroupSummary(1, columns, summaryGroup1) );
			// isso aqui nao aparece para nao duplicar os somatorios dos grupos!!!!!
			// content.append( createGroupSummary(1, columns, summaryReport) );
		} else if (qtdGroupsToSummary == 0) {
			content.append( createGroupSummary(1, columns, summaryReport) );
		}


		content.append("</table>" + PL);
	}
	private String criaTituloColuna(List<Object[]> model, Column[] columns) {

		StringBuilder content = new StringBuilder();
		List<String> alignList = getAlignList(model, columns);
		int xuxa = 0;
		for (int j = 0; j < columns.length; j++) {
			Column col = columns[j];

			String preview = col.getPreview();
			boolean visible = col.getVisible();
			String group = col.getGroup();

			if  (visible == false) continue;
			if  (preview.equalsIgnoreCase("true")) continue;
			if  (super.isColumnGroup(group)) continue;
			String cssAlign = "";
			if (alignList.size() > j) cssAlign = alignList.get(xuxa++);

			content.append("<td class=\"spfw bold "+ cssAlign +"\" style=\"border-bottom: 1px solid #000; width:"+ (col.getSize()*2) +"px;\">"+ col.getLabel() +"</td>");
		}

		return content.toString();
	}

	private List<String> getAlignList(List<Object[]> model, Column[] columns) {
		List<String> alignList = new ArrayList<String>();
		if (model.size() > 0) {
			Object[] temp = model.get(0);
			for (int j = 0; j < columns.length; j++) {
				Column col = columns[j];

				String preview = col.getPreview();
				boolean visible = col.getVisible();
				String group = col.getGroup();

				if  (visible == false) continue;
				if  (preview.equalsIgnoreCase("true")) continue;
				if  (super.isColumnGroup(group)) continue;

				Object value = temp[j];
				String cssAlign = "";
				if (value instanceof Double) {
					cssAlign = "right";
				} else if (value instanceof Integer) {
					cssAlign = "right";
				} else if (value instanceof Date) {
					cssAlign = "center";
				} else {
					cssAlign = "left";
				}
				alignList.add(cssAlign);
			}
		}
		return alignList;
	}

	private String createGroupSummary(Integer groupInt, Column[] columns, HashMap<String, Double> summaryGroup) throws Exception {
		StringBuilder content = new StringBuilder();


		String background = "background:";
		if (groupInt.equals(1)) background += "#bbb;";
		if (groupInt.equals(2)) background += "#ccc;";
		if (groupInt.equals(3)) background += "#ddd;";

		content.append("<tr style=\""+ background +"\">");

		int firstColumn = 0;
		for (int j = 0; j < columns.length; j++) {
			Column col = columns[j];

			String column = col.getName();
			String format = col.getFormat();
			String preview = col.getPreview();
			boolean visible = col.getVisible();
			String group = col.getGroup();
			String summaryType = col.getSummary();

			if  (visible == false) continue;
			if  (preview.equalsIgnoreCase("true")) continue;
			if  (super.isColumnGroup(group)) continue;
			if (firstColumn == 0) firstColumn = j;

			content.append("<td class=\"spfw bold right\" style=\"width:"+ (col.getSize()*2) +"px;\">");
			if (summaryType != null) {
				content.append( formatShowValue(summaryGroup.get(column), format) );
			} 


			content.append("</td>");
		}
		content.append("</tr>" + PL);
		resetGroup(columns, summaryGroup);
		return content.toString();
	}
	private void calculateRow(String column, String summaryType, Object valueParam) {
		if (summaryType != null) {
			if (summaryType.equalsIgnoreCase("sum")) {

				Double value = 0.0;
				if (valueParam instanceof Integer) {
					value= NumberUtils.coalesce((Integer)valueParam, 0) * 1.0;
				} else if (valueParam instanceof Double) {
					value= NumberUtils.coalesce((Double)valueParam, 0.0);
				}


				Double soma = summaryReport.get(column);
				if (soma == null) soma = 0.0;
				soma += value;
				summaryReport.put(column, soma);

				soma = summaryGroup1.get(column);				
				if (soma == null) soma = 0.0;
				soma += value;
				summaryGroup1.put(column, soma);

				soma = summaryGroup2.get(column);				
				if (soma == null) soma = 0.0;
				soma += value;
				summaryGroup2.put(column, soma);

			} else if (summaryType.equalsIgnoreCase("count")) {

				Double count = summaryReport.get(column);
				if (count == null) count = 0.0;
				count++;
				summaryReport.put(column, count);

				count = summaryGroup1.get(column);				
				if (count == null) count = 0.0;
				count++;
				summaryGroup1.put(column, count);

				count = summaryGroup2.get(column);				
				if (count == null) count = 0.0;
				count++;
				summaryGroup2.put(column, count);

			} else if (summaryType.equalsIgnoreCase("avg")) {

			}
		}
	}
	private void resetGroup(Column[] columns, HashMap<String, Double> summaryGroup) {
		for (int i = 0; i < columns.length; i++) {
			String column = columns[i].getName();
			summaryGroup.put(column, 0.00);
		}
	}


	private String formatShowValue(Object value, String format) {
		String result = null;
		if (value == null) {
			result = "";
		} else if (value instanceof Integer) {
			if (format == null) format = AbstractDotumSmartReport.numberDefaultFormat;
			result = (Integer)value+"";
		} else if (value instanceof Double) {
			if (format == null) format = AbstractDotumSmartReport.doubleDefaultFormat;
			DecimalFormat df = new DecimalFormat(format);
			result = df.format((Double)value);
		} else if (value instanceof Date) {
			if (format == null) format = AbstractDotumSmartReport.dateDefaultFormat;
			result = FormatUtils.formatDate((Date)value, format);
		} else {
			result = replaceSpecialCharacterToHtml((String)value);
		}
		return result;
	}

	// CROSSTAB
	private void createCrossTabSession(SectionDataBean session, Section section, StringBuilder content) throws Exception {
		List<Object[]> model = session.getModel();

		//		content.append(addTopBottomReport());

		Column[] columns = section.getCrosstab().getColumns();
		Object[] data = model.get(0);
		Boolean[] visibleArray = new Boolean[data.length];
		String[] typeArray = new String[data.length];

		if (columns.length <= data.length) {
			for (int i = 0; i < columns.length; i++) {
				Column column = columns[i];
				visibleArray[i] = column.getVisible();

				if (column.getCrosstab() == null) continue;
				if (column.getCrosstab().equals("column")) continue;
				if (column.getGroup().equals("1")) {
					continue;
				}
				typeArray[i] = column.getType();
			}

			//			content.append("   <table class=\"crostab\">" + PL);
			content.append("  <table class=\"table table-striped table-hover dataTable no-footer\">" + PL);
			for (int i = 0; i < model.size(); i++) {
				content.append( abreLinhaZebra(i) );

				Boolean visible = true;
				String type = null;
				data = model.get(i);

				for (int j = 0; j < data.length; j++) {
					Object obj = data[j];
					visible = visibleArray[j];
					if (visible == null) visible = true;
					type = typeArray[j];
					if (type == null) type = "decimal";

					//Integer size = (j == (data.length-1)) ? 10 : columns[j].getSize();
					Integer size = 0;
					String classCss = "";


					// tenho que pegar da primeira linha pois o relatorio pode nao ter dados
					if (type.equals("decimal")) {
						classCss += "right";
					} else if (type.equals("number")) {
						classCss += "right";
					} else if (type.equals("date")) {
						classCss += "center";
					} else  {
						classCss += "left";
					}

					if (i == 0) {
						classCss += " bold";
					}

					String cssSize = "style=\"width:"+size+"px;\" ";
					cssSize = "";

					if (visible == false) continue;
					if (obj == null) {
						content.append("<td "+cssSize+"></td>");
					} else if (obj instanceof Double) {
						content.append("<td "+cssSize+"class=\""+ classCss +"\">" + FormatUtils.formatDouble((Double)obj, 3) + "</td>");
					} else if (obj instanceof BigDecimal) {
						content.append("<td "+cssSize+"class=\""+ classCss +"\">" + FormatUtils.formatBigDecimal((BigDecimal)obj, 3) + "</td>");
					} else if (obj instanceof Date) {
						content.append("<td "+cssSize+"class=\""+ classCss +"\">" + FormatUtils.formatDate((Date)obj) + "</td>");
					} else if (obj instanceof String) {
						content.append("<td "+cssSize+"class=\""+ classCss +"\">" + replaceSpecialCharacterToHtml((String)obj) + "</td>");
					} else {
						content.append("<td "+cssSize+"class=\""+ classCss +"\">" + obj + "</td>");
					}
				}
				content.append("</tr>" + PL);
			}
			content.append("</table>" + PL);
		} else {
			content.append( getSemDados() );
		}
	}

	private String abreLinhaZebra(int i) {
		boolean zebra = (i % 2 == 0);
		boolean cabeca = (i == 0);

		String result = "";


		if (cabeca == true) {
			result = "<tr style=\"background:#ddd;\">";
		} else if (zebra == true) {
			result = "<tr>";
		} else {
			result = "<tr style=\"background:#eee;\">";
		}
		return result;
	}

	//
	// HELPER
	//
	protected Object setVariablesValues(Column[] columns, Object[] row){
		HashMap<String, Object> variableValues = new HashMap<String, Object>();
		variableValues.put("LOGOTIPO", getReportBuilder().getLogotipoPath());
		String currentGroup = "";
		for(int h = 0; h < row.length; h++){
			Object col = row[h];
			Column column = columns[h];
			if(column.getGroup().equals("1")){
				currentGroup = String.valueOf(col);
			}
			if(col instanceof String){
				String value = (String) col;
				value = replaceSpecialCharacterToQueryHelper(value);
				col = value;
			}
			variableValues.put(column.getName(), col);
		}

		return new Object[]{currentGroup, variableValues};
	}
	protected void createCloneContent(Section section, Column[] columns, List<Object[]> model, StringBuilder content, int cloneContent, int pStartGroup, int pEndGroup) throws Exception{
		if(cloneContent > 0){
			for(int c = 0; c < cloneContent; c++){
				for(int t = pStartGroup; t <= pEndGroup; t++){
					Object[] rowTemp = model.get(t);
					HashMap<String, Object> currentVariableValuesTemp = new HashMap<String, Object>();
					Object[] objsTemp = (Object[]) setVariablesValues(columns, rowTemp);
					currentVariableValuesTemp = (HashMap<String, Object>) objsTemp[1];

					//CONTENT HEADER TEXT
					String textTemp = null;
					if(t == pStartGroup){
						textTemp = SQLUtils.parseQuery(section.getContentHeaderText(), currentVariableValuesTemp);
						textTemp = replaceSpecialCharacterToHtml(textTemp);
						content.append("<div class=\"content\">" + PL);
						content.append(    textTemp);
						content.append(    "" + PL);
					}
					//
					//DETAIL
					textTemp = SQLUtils.parseQuery(StringUtils.coalesce(section.getDetailText(), ""), currentVariableValuesTemp);
					textTemp = replaceSpecialCharacterToHtml(textTemp);
					content.append(    textTemp);
					content.append(    "" + PL);
					//
					//CONTENT FOOTER TEXT
					if(t == pEndGroup){
						textTemp = SQLUtils.parseQuery(StringUtils.coalesce(section.getContentFooterText(), ""), currentVariableValuesTemp);
						textTemp = replaceSpecialCharacterToHtml(textTemp);
						content.append(textTemp+"" + PL);
						content.append("</div>" + PL);
					}
					//
				}
			}
		}
	}
	protected String replaceSpecialCharacterToQueryHelper(String text){
		text = text.replaceAll("\\$", "\\\\\\$");

		return text;
	}
	protected String replaceSpecialCharacterToHtml(String text){
		text = text.replaceAll("/\\*", "/ *");
		text = text.replaceAll("\\&", "&amp;");
		text = text.replaceAll("\\$", "&#36;");
		//		text = text.replaceAll("<", "&lt;");
		//		text = text.replaceAll(">", "&gt;");

		return text;
	}

	public String getSemDados() {
		StringBuilder content = new StringBuilder();
		content.append("<br/><br/><br/><br/>");
		content.append("<p style=\"font-size:16px;\" class=\"center\">Não foi encontrado dados com os filtros informado.</p>");
		content.append("<p style=\"font-size:16px;\" class=\"center\">Duvidas entrar em contato com sua equipe de TI.</p>");
		return content.toString();
	}
}