package br.com.dotum.jedi.component.xml.smartreport.builder;

import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.file.layout.FileMemory;


public class DotumSmartReportExportHTML extends DotumSmartReportExportCommons {

	public DotumSmartReportExportHTML(DotumSmartReportBuilder reportBuilder) throws Exception {
		super(reportBuilder);
		createReport();
	}
	//
	// PDF
	//
	protected void createReport() throws Exception {
//		ByteArrayOutputStream out = new ByteArrayOutputStream();

		String htmlStr = createReportHtml();
		super.setFileMemory( new FileMemory(htmlStr, "conteudo.html") ) ;
		
//		int altura = 50;
//		if (isReportText() == true) altura = 10;
//		
//		
//		Document doc = null;
//		// colocar o paisagem aqui
//		if (getReportBuilder().getDotumSmartReport().getOrientation().equalsIgnoreCase("LANDSCAPE") && getReportBuilder().getDotumSmartReport().getPageSize().equalsIgnoreCase("A4")) {
//			doc = new Document(PageSize.A4.rotate(), 20, 20, altura, 40);
//		} else {
//			doc = new Document(PageSize.A4, 20, 20, altura, 40);
//		}
//		PdfWriter writer = PdfWriter.getInstance(doc, out);
//		writer.setInitialLeading(12);
//
//		if (isReportText() == false) {
//			String textTitle = super.replaceSpecialCharacterToHtml(getReportBuilder().getTextTitle());
//			String textFilter = super.replaceSpecialCharacterToHtml(getReportBuilder().getTextFilter());
//			String textFooter = super.replaceSpecialCharacterToHtml(getReportBuilder().getTextFooterLeft());
//			HeaderFooter event = new HeaderFooter(textTitle, textFilter, textFooter);
//			writer.setPageEvent(event);
//		}
//
//		doc.open();
//
//		/**
//		 * TODO: não funciona a tag 'breakpage' que faz os relatórios aparecerem sem quebrar pagina
//		 * mas deixa a emissão mais rápida de pdf. 
//		 */
//		//		XMLWorkerHelper.getInstance().parseXHtml(writer, doc, new StringReader(content.toString()));
//		
//		if(htmlStr.contains("breakpage")){
//			// este fica rapido apenas para o HTML
//			
//			FileMemory cssFm = new FileMemory(getCss(), "css.css");
//
//			CSSResolver cssResolver = new StyleAttrCSSResolver();
//			
//			BufferedInputStream bis1 = new BufferedInputStream(cssFm.getStream());
//			CssFile cssFile = XMLWorkerHelper.getCSS(bis1);
//			bis1.close();
//			
//			cssResolver.addCss(cssFile);
//			// HTML
//			XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
//			fontProvider.addFontSubstitute("lowagie", "cardo");
//			
//			CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
//			HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
//			htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
//			htmlContext.setAcceptUnknown(true).autoBookmark(false);
//			
//			
//			PdfWriterPipeline pdf = new PdfWriterPipeline(doc, writer);
//			HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
//			CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
//			// XML Worker
//			XMLWorker worker = new XMLWorker(css, true);
//			XMLParser p = new XMLParser(worker);
//			
//			FileMemory htmlFm = new FileMemory(htmlStr, "content.html");
//			
//			BufferedInputStream bis = new BufferedInputStream(htmlFm.getStream());
//			p.parse(bis);
//			bis.close();
//		} else {
//			
//			// este esta rapido pois criou uma celular
//			// funciona 100% para tabular e crosstab
//			// aqui falta a quebra de pagina no tipo html/text
//			ElementList elements = XMLWorkerHelper.parseToElementList(htmlStr, getCss());
//			PdfPTable table = new PdfPTable(1);
//			table.setWidthPercentage(new float[]{PageSize.A4.getWidth()}, PageSize.A4);
//
//			PdfPCell cell = new PdfPCell();
//			for (Element e : elements) {
//				cell.setBorder(Rectangle.NO_BORDER);
//				cell.addElement(e);
//			}
//			table.addCell(cell);
//			doc.add(table);
//		}
//		doc.close();
//		
//		
//		super.setFileMemory( new FileMemory(out.toByteArray()));
//		out.close();
	}
}

