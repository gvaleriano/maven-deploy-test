package br.com.dotum.jedi.component.xml.smartreport.builder;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.dotum.jedi.component.xml.DotumSmartReportBuilder;
import br.com.dotum.jedi.component.xml.smartreport.model.Column;
import br.com.dotum.jedi.component.xml.smartreport.model.SectionDataBean;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;
import jxl.Workbook;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class DotumSmartReportExportXLS extends AbstractDotumSmartReport {
	private static WritableCellFormat titleFont = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD));
	private static WritableCellFormat columnsFont = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD));
	private static WritableCellFormat group1Font = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD));
	private static WritableCellFormat group2Font = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD));
	private static WritableCellFormat detailFont = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 8, WritableFont.NO_BOLD));

	private static String group1Crosstab = "column";
	private static String group2Crosstab = "row";
	private static String group3Crosstab = "data";

	private FileMemory fmModelo; 
	private WritableWorkbook wwb;
	private int qtdGroups = 0;
	private int qtdTotalCelular;
	private int linha = 0;

	public DotumSmartReportExportXLS(DotumSmartReportBuilder reportBuilder, FileMemory fmModelo) throws Exception {
		super(reportBuilder);
		this.fmModelo = fmModelo;
		createReport();
	}

	//
	// TABULAR
	//
	private void createHeadTabularSession(WritableSheet ws, SectionDataBean session) throws Exception {
		List<Column> columns = session.getColumns();
		addCell(ws, 0, linha++, session.getTitle(), titleFont);

		qtdGroups = 0;
		for (int i = 0; i < columns.size(); i++) {
			Column columnXML = columns.get(i);
			String group = columnXML.getGroup();
			if (StringUtils.hasValue(group)) {
				qtdGroups += 1;
			} 
		}
		qtdTotalCelular = columns.size() - qtdGroups - 1;

		for (int i = 0; i < columns.size(); i++) {
			Column columnXML = columns.get(i);
			String group = columnXML.getGroup();
			if (StringUtils.hasValue(group)) continue;
			int columnNumber = i - qtdGroups;
			addCell(ws, columnNumber, linha, columnXML.getLabel(), columnsFont);
		}
	}
	private void createDataTabularSession(WritableSheet ws, SectionDataBean session) throws Exception {
		List< Object[] > datas = session.getModel();
		List<String> columnsNames = session.getColumnsNames();
		List<Column> columns = session.getColumns();

		//			group=1    group=1           group=2    group=2          
		//			"BIG AÇO", "03442062000141", "AZP",    "09163638000118", "Duplicata", "1552/2010", 250.00, "20/12/10", "05/01/11", "20/02/12", 1.00, "Aditivo MO - tesouras, quadro de energia, pintura e colocaçãode luminárias"
		String lastDataRowGroup1 = "null";
		String lastDataRowGroup2 = "null";

		for (int i = 0; i < datas.size(); i++) {
			Object[] rs = datas.get(i);

			int coluna = 0;
			linha++;

			for (int j = 0; j < columns.size(); j++) {
				Column reportColumn = columns.get(j);
				Object obj = rs[j];

				String column = reportColumn.getName();
				String columnName = columnsNames.get(j);
				if (column.equals(columnName) == false) continue; 

				String group = reportColumn.getGroup();

				String valueGroup = "";
				boolean isColumnGroupFirst = (isColumnGroupFirst(columns, reportColumn));
				boolean isColumnGroup = (isColumnGroup(reportColumn));
				if (isColumnGroupFirst == true) {
					//
					// se for grupo 1
					//
					if (group.equals("1")) {
						if (lastDataRowGroup1 != null) {
							valueGroup = formatCaptionGroupStr(rs, columns, "1");
							if (lastDataRowGroup1.equals(valueGroup) == false) {
								lastDataRowGroup2 = "null";
								linha++;
								List<String> captionGroup = formatCaptionGroup(rs, columns, "1");
								for (int m = 0; m < captionGroup.size(); m++) {
									addCell(ws, m, linha, captionGroup.get(m), group1Font);
								}

								linha++;
							}
						}
						//
						// se for grupo 2
						//
					} else if (group.equals("2")) {
						if (lastDataRowGroup2 != null) {
							valueGroup = formatCaptionGroupStr(rs, columns, "2");
							if (lastDataRowGroup2.equals(valueGroup) == false) {
								List<String> captionGroup = formatCaptionGroup(rs, columns, "2");
								for (int m = 0; m < captionGroup.size(); m++) {
									addCell(ws, m, linha, captionGroup.get(m), group2Font);
								}

								linha++;
							}
						}
					}
				}

				if (isColumnGroup == false) {
					addCell(ws, coluna, linha, obj, detailFont);
					coluna++;
				}

				// agora vou ver se mudou a informacao do grupo
				if (isColumnGroupFirst == true) {
					if (group.equals("1")) lastDataRowGroup1 = valueGroup;
					if (group.equals("2")) lastDataRowGroup2 = valueGroup;
				}

			}
		}
	}
	private static boolean isColumnGroupFirst(List<Column> columns, Column column) {
		List<String> tempGroup = new ArrayList<String>();
		List<String> tempName = new ArrayList<String>();
		for (Column col : columns) {
			if (col.getGroup() == null) continue;
			if (tempGroup.contains(col.getGroup()) == false) {
				tempGroup.add(col.getGroup());
				tempName.add(col.getName());
			}
		}
		for (String string : tempName) {
			if (string.equals(column.getName())) return true;
		}
		return false;
	}
	private static String formatCaptionGroupStr(Object[] rs, List<Column> columns, String group) throws SQLException {
		List<String> xxx = formatCaptionGroup(rs, columns, group);
		
		StringBuilder sb = new StringBuilder();
		for (String x : xxx) {
			sb.append(x);
		}
		return sb.toString();		
	}
	
	private static List<String> formatCaptionGroup(Object[] rs, List<Column> columns, String group) throws SQLException {

		List<String> result = new ArrayList<String>();
		for (int i = 0; i < columns.size(); i++ ) {
			Column column = columns.get(i);
			if (column.getGroup() == null) continue;
			if (column.getGroup().equals(group)) {
				result.add( ""+rs[i] ); 
			}
		}

		return result;
	}

	//
	// CROSSTAB
	//
	private void createHeadCrosstabSession(WritableSheet ws, SectionDataBean session) throws Exception {
		List<Column> columns = session.getColumns();
		addCell(ws, 0, linha++, session.getTitle(), titleFont);

		qtdGroups = 0;
		for (int i = 0; i < columns.size(); i++) {
			Column columnXML = columns.get(i);
			String group = columnXML.getCrosstab();
			if ((StringUtils.hasValue(group)) && (group.equalsIgnoreCase(group1Crosstab)) && (group.equalsIgnoreCase(group2Crosstab))) {
				qtdGroups += 1;
			} 
		}
	}
	private void createDataCrosstabSession(WritableSheet ws, SectionDataBean session) throws Exception {
		List< Object[] > datas = session.getModel();

		for (int i = 0; i < datas.size(); i++) {
			WritableCellFormat f = (i < qtdGroups ? group1Font : detailFont);

			Object[] rs = datas.get(i);
			int coluna = 0;
			for (int j = 0; j < rs.length; j++) {
				Object obj = rs[j];
				addCell(ws, coluna++, linha, obj, f);
			}
			linha++;
		}
	}


	//
	// XLS
	//
	protected void createReport() throws Exception {
		String serverPath = getReportBuilder().getUserContext().getServerPath();
		String code = getReportBuilder().getCode();

		// tem que criar o arquivo de modelo que esta no banco caso tenha 
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		if (fmModelo == null) {
			wwb = Workbook.createWorkbook(byteOut);
			WritableSheet ws = wwb.createSheet("report", 0);

			for (SectionDataBean session : getSessionData()) {

				if (session.getType().equals("tabular")) {
					createHeadTabularSession(ws, session);
					createDataTabularSession(ws, session);
				} else if (session.getType().equals("crosstab")) {
					createHeadCrosstabSession(ws, session);
					createDataCrosstabSession(ws, session);
				}

				linha++;
				linha++;
				linha++;
				linha++;
			}

			wwb.write();
			wwb.close();
			
		} else {

			Workbook wb = Workbook.getWorkbook(fmModelo.getStream());
			wwb = Workbook.createWorkbook(byteOut, wb);

			for (int i = 0; i < getSessionData().length; i++) {
				SectionDataBean session = getSessionData()[i];
				String name = "model"+(i+1);
				WritableSheet ws = wwb.getSheet(name);
				if (ws == null) ws = wwb.createSheet(name, i);

				linha = 0;
				if (session.getType().equals("tabular")) {
					createHeadTabularSession(ws, session);
					createDataTabularSession(ws, session);
				} else if (session.getType().equals("crosstab")) {
					createHeadCrosstabSession(ws, session);
					createDataCrosstabSession(ws, session);
				}
			}

			wwb.write();
			wwb.close();

		}
		super.setFileMemory( new FileMemory(byteOut.toByteArray()) );
		byteOut.close();
	}

	public void setModelo(FileMemory fm) {
		this.fmModelo = fm;
	}
	
	//
	// HELPER
	//
	private void addCell(WritableSheet ws, int column, int line, Object value, WritableCellFormat wcf) throws RowsExceededException, WriteException {

		if (value instanceof Date) {
			jxl.write.Label o = new jxl.write.Label(column, line, FormatUtils.formatDate((Date)value), wcf);
			ws.addCell(o);
		} else if (value instanceof Double) {
			jxl.write.Number o = new jxl.write.Number(column, line, (Double)value, wcf);
			ws.addCell(o);
		} else if (value instanceof Integer) {
			jxl.write.Number o = new jxl.write.Number(column, line, (Integer)value, wcf);
			ws.addCell(o);
		} else {
			jxl.write.Label o = new jxl.write.Label(column, line, (String)value, wcf);
			ws.addCell(o);
		}
	}

}