package br.com.dotum.jedi.component.xml.smartreport.builder;

import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class HeaderFooter extends PdfPageEventHelper {

	private Font f1 = FontFactory.getFont(FontFactory.HELVETICA);
	private Font f2 = FontFactory.getFont(FontFactory.HELVETICA);

	private String title;
	private String filter;
	private String footer;
	private PdfTemplate t;
	private Image total;

	public HeaderFooter(String title, String filter, String footer) throws IOException {
		this.title = title;
		this.filter = filter;
		this.footer = footer;

		f1.setSize(12);
		f2.setSize(6);
	}

	public void onOpenDocument(PdfWriter writer, Document document) {
		t = writer.getDirectContent().createTemplate(30, 16);
		try {
			total = Image.getInstance(t);
			total.setRole(PdfName.ARTIFACT);
		} catch (DocumentException de) {
			throw new ExceptionConverter(de);
		}
	}

	public void onEndPage(PdfWriter writer, Document document) {
		try {
			float width = document.getPageSize().getWidth();
			float leftMargin = document.leftMargin();
			float rightMargin = document.rightMargin();
			width -= leftMargin + rightMargin;

			PdfPTable table = new PdfPTable(1);
			table.setTotalWidth(width);
			table.setLockedWidth(true);

			PdfPCell cell = new PdfPCell(new Phrase(title, f1));
			cell.setBorder(0);
			table.addCell(cell);
			cell = new PdfPCell(new Phrase(filter, f2));
			cell.setBorder(0);
			cell.setBorderWidthBottom(1);
			table.addCell(cell);
			table.writeSelectedRows(0, -1,
					document.left(),
					document.top() + ((document.topMargin() + table.getTotalHeight()) / 2),
					writer.getDirectContent());




			PdfPTable table2 = new PdfPTable(3);
			table2.setWidthPercentage(new float[]{88, 8, 4}, document.getPageSize());
			table2.setTotalWidth(width);
			table2.setLockedWidth(true);
			PdfPCell cell2 = new PdfPCell(new Phrase(footer, f2));
			cell2.setBorder(0);
			cell2.setBorderWidthTop(1);
			table2.addCell(cell2);
			cell2 = new PdfPCell(new Phrase("Pagina " + writer.getPageNumber() + " /", f2));
			cell2.setBorder(0);
			cell2.setBorderWidthTop(1);
			cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table2.addCell(cell2);

			cell2 = new PdfPCell(total);
			cell2.setBorder(0);
			cell2.setBorderWidthTop(1);
			//			cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table2.addCell(cell2);


			PdfContentByte canvas = writer.getDirectContent();
			canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
			table2.writeSelectedRows(0, -1, 
					document.left(), 
					document.bottom() - 10,
					canvas);
			canvas.endMarkedContentSequence();


			//			table2.writeSelectedRows(0, -1, 
			//					document.left(), 
			//					document.bottom() - 10,
			//					writer.getDirectContent());


		} catch (Exception de) {
			throw new ExceptionConverter(de);
		}

	}
	public void onCloseDocument(PdfWriter writer, Document document) {
		ColumnText.showTextAligned(t, Element.ALIGN_LEFT,
				new Phrase(String.valueOf(writer.getPageNumber()), f2),
				0, 8, 0);
	}
}
