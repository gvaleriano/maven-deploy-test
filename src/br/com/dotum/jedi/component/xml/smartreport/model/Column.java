package br.com.dotum.jedi.component.xml.smartreport.model;



public class Column {
	private String name;
	private String label;
	private int size;
	private String type;
	private String format;
	private String summary;
	private String preview = "false";
	private String group = "0";
	private String crosstab;
	private Boolean visible = true;
	private String startNewPage = "false";

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getPreview() {
		return preview;
	}
	public void setPreview(String preview) {
		this.preview = preview;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getCrosstab() {
		return crosstab;
	}
	public void setCrosstab(String crosstab) {
		this.crosstab = crosstab;
	}
	public Boolean getVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public String getStartNewPage() {
		return startNewPage;
	}
	public void setStartNewPage(String startNewPage) {
		this.startNewPage = startNewPage;
	}
}

