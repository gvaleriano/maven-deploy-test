package br.com.dotum.jedi.component.xml.smartreport.model;



public class Deprecated {
	private String code;
	private String observation;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
}