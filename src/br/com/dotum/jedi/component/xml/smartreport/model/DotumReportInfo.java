package br.com.dotum.jedi.component.xml.smartreport.model;


public class DotumReportInfo {
	
	private String reportName;
	private String aplicacao;
	private Integer versao;
	
	public DotumReportInfo() {
	}
	public DotumReportInfo(String reportName, String aplicacao, Integer versao) {
		super();
		this.reportName = reportName;
		this.aplicacao = aplicacao;
		this.versao = versao;
	}
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getAplicacao() {
		return aplicacao;
	}
	public void setAplicacao(String aplicacao) {
		this.aplicacao = aplicacao;
	}
	public Integer getVersao() {
		return versao;
	}
	public void setVersao(Integer versao) {
		this.versao = versao;
	}

}
