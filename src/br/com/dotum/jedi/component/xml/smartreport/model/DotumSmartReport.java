package br.com.dotum.jedi.component.xml.smartreport.model;


public class DotumSmartReport implements java.io.Serializable {

	
	private String xml;
	private Menu menu;

	private String title;
	private Integer version;
	private String type;
	private String pageSize = "A4"; // por padrao ** nao tire isso daqui **
	private String orientation = "portrait"; // por padrao
	private Deprecated deprecated;
	private Parm[] parmsFields;
	private Section[] sectionList;
	private SectionDataBean[] sectionDataList;

	public Menu getMenu() {
		if (menu == null) menu = new Menu();
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getOrientation() {
		return orientation;
	}
	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}
	public Parm[] getParms() {
		return parmsFields;
	}
	public void setParms(Parm[] parmsFields) {
		this.parmsFields = parmsFields;
	}
	public Section[] getSections() {
		return sectionList;
	}
	public void setSections(Section[] sections) {
		this.sectionList = sections;
	}
	public SectionDataBean[] getSectionDataList() {
		return sectionDataList;
	}
	public void setSectionDataList(SectionDataBean[] sectionDataList) {
		this.sectionDataList = sectionDataList;
	}
	public Deprecated getDeprecated() {
		return deprecated;
	}
	public void setDeprecated(Deprecated deprecated) {
		this.deprecated = deprecated;
	}
	public String getXml() {
		return xml;
	}
	public void setXml(String xml) {
		this.xml = xml;
	}
}

