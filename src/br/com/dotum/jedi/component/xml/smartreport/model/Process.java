package br.com.dotum.jedi.component.xml.smartreport.model;


public class Process {

	private AfterOpen[] afterOpen;
	
	
	public AfterOpen[] getAfterOpen() {
		return afterOpen;
	}

	public void setAfterOpen(AfterOpen[] afterOpen) {
		this.afterOpen = afterOpen;
	}
}