package br.com.dotum.jedi.component.xml.smartreport.model;


public class Section {

	private String title;

	private String information;
	private String table;
	private String type;
	private Integer cloneContent = 0;
	private String SQL;
	private Process process;
	private Tabular tabular;
	private Crosstab crosstab;
	private Variable variable;
	private String style;
	private String headerText;
	private String contentHeaderText;
	private String detailText;
	private String contentFooterText;
	private String footerText;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getCloneContent() {
		return cloneContent;
	}
	public void setCloneContent(Integer cloneContent) {
		this.cloneContent = cloneContent;
	}
	public String getSQL() {
		return SQL;
	}
	public void setSQL(String sQL) {
		SQL = sQL;
	}
	public Process getProcess() {
		return process;
	}
	public void setProcess(Process process) {
		this.process = process;
	}
	public Tabular getTabular() {
		return tabular;
	}
	public void setTabular(Tabular tabular) {
		this.tabular = tabular;
	}
	public Crosstab getCrosstab() {
		return crosstab;
	}
	public void setCrosstab(Crosstab crosstab) {
		this.crosstab = crosstab;
	}
	public Variable getVariable() {
		return variable;
	}
	public void setVariable(Variable variable) {
		this.variable = variable;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getHeaderText() {
		return headerText;
	}
	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}
	public String getContentHeaderText() {
		return contentHeaderText;
	}
	public void setContentHeaderText(String contentHeaderText) {
		this.contentHeaderText = contentHeaderText;
	}
	public String getDetailText() {
		return detailText;
	}
	public void setDetailText(String detailText) {
		this.detailText = detailText;
	}
	public String getContentFooterText() {
		return contentFooterText;
	}
	public void setContentFooterText(String contentFooterText) {
		this.contentFooterText = contentFooterText;
	}
	public String getFooterText() {
		return footerText;
	}
	public void setFooterText(String footerText) {
		this.footerText = footerText;
	}
	public boolean hasPreview() {
		if (crosstab != null) {
			for (Column column : getCrosstab().getColumns()) {
				if (column.getPreview().equalsIgnoreCase("true")) 
					return true;
			}
		} else {
			for (Column column : getTabular().getColumns()) {
				if (column.getPreview().equalsIgnoreCase("true")) 
					return true;
			}
		}
		return false;
	}
}