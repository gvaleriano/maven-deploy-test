package br.com.dotum.jedi.component.xml.smartreport.model;


import java.util.ArrayList;
import java.util.List;

public class SectionDataBean {

	private String type;
	private String title;
	private String information;
	private List<String> columnNames = new ArrayList<String>();
	private List<Column> columns = new ArrayList<Column>();
	private List< Object[] > model = new ArrayList< Object[] >();
	private List< Object[] > summary = new ArrayList< Object[] >();
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}
	public void setModel(List< Object[] > model) {
		this.model = model;
	}
	public void addData(Object ... value) {
		model.add( value );
	}
	public void addSummary(Object ... value) {
		summary.add( value );
	}
	public void addColumn(String name, String label, String type) {
		addColumn(name, label, type, null, null);		
	}
	
	public void addColumn(String name, String label, String type, String group) {
		addColumn(name, label, type, group, null);
	}
	
	public void addColumn(String name, String label, String type, String group, String crosstab) {
		Column column = new Column();
		column.setName(name);
		column.setLabel(label);
		column.setType(type);
		column.setGroup(group);
		column.setCrosstab(crosstab);
		this.columns.add( column );		
		columnNames.add( name );
	}
	public List<Column> getColumns() {
		return columns;
	}
	public List< Object[] > getModel() {
		return model;
	}
	public List<String> getColumnsNames() {
		return columnNames;
	}
}

