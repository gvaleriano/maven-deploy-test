package br.com.dotum.jedi.component.xml.smartreport.model;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SectionGroupDataBean {

	private SectionGroupDataBean parent;
	private List<Object> data = new ArrayList<Object>();

	public SectionGroupDataBean() {
	}

	public SectionGroupDataBean(SectionGroupDataBean parent) {
		this.parent = parent;
	}

	public List<Object> getData() {
		return data;
	}
	public void addData(Object ... values) {
		data.addAll(Arrays.asList(values));		
	}

	public SectionGroupDataBean getParent() {
		return parent;
	}
}

