package br.com.dotum.jedi.component.xml.smartreport.model;


public class Tabular {

	private Column[] columns;

	public Column[] getColumns() {
		return columns;
	}

	public void setColumns(Column[] columns) {
		this.columns = columns;
	}
}

