package br.com.dotum.jedi.core;

import java.io.IOException;

import br.com.dotum.jedi.util.PropertiesUtils;

public class PropertiesConstants {

	
	public static final String FTP_SERVER = "ftp.server";
	public static final String FTP_USERNAME = "ftp.username";
	public static final String FTP_PASSWORD = "ftp.password";
	
	public static final String DATABASE_TYPE = "database.type";
	public static final String DATABASE_IP = "database.ip";
	public static final String DATABASE_PORT = "database.port";
	public static final String DATABASE_NAME = "database.name";
	public static final String DATABASE_USARNAME = "database.username";
	public static final String DATABASE_PASSWORD = "database.password";
	
	//String value = "dotum.control.DotumLoginHtml";
	public static final String PAGE_LOGIN = "page.loginclass";
	//String value = "dotum.control.DotumHomeHtml";
	public static final String PAGE_HOME = "page.homeclass";
	public static final String SYSTEM_LINK_LOGIN = "system.link.login";
	public static final String SYSTEM_LINK_404ERROR = "system.link.404error";
	public static final String SYSTEM_LINK_HOME = "system.link.home";
	public static final String SYSTEM_VERSION = "system.version";
	public static final String SYSTEM_TITLE = "system.title";
	public static final String SYSTEM_ICON = "system.icon";
	public static final String SYSTEM_FAVICON = "system.favicon";
	public static final String SYSTEM_MENUFIXO = "system.menufixo";
			
	public static final String SYSTEM_START_DD = "system.start.dd";
	public static final String SYSTEM_START_JOB = "system.start.job";
	public static final String SYSTEM_START_COMPONENT = "system.start.component";

	public static final String SYSTEM_PACKAGE_BASE = "system.packagebase";
	public static final String SYSTEM_DEBUGCODE = "system.debugcode";
	public static final String SYSTEM_DEVELOPER = "system.developer";
	public static final String SYSTEM_USERNAME = "system.username";
	public static final String SYSTEM_PASSWORD = "system.password";
	public static final String SYSTEM_TIMEOUT = "system.timeout";

	public static final String APPLICATION_LOG_LEVEL = "application.log.level";
	public static final String APPLICATION_LOG_PATH = "application.log.path";
	
	public static final String RELEASE_HOST = "release.host";
	public static final String RELEASE_USERNAME = "release.username";
	public static final String RELEASE_PASSWORD = "release.password";

	public static String getLoginPage() throws IOException {
		String prop = PropertiesUtils.getString(PAGE_LOGIN);
		if (prop == null) prop = "dotumerp.control.DotumLoginHtml";
		return prop;
	}
	
	public static String getHomePage() throws IOException {
		String prop = PropertiesUtils.getString(PAGE_HOME);
		if (prop == null) prop = "dotumerp.control.DotumHomeHtml";
		return prop;
	}

}


