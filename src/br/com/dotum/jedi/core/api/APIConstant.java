package br.com.dotum.jedi.core.api;

public class APIConstant {

	public static final String APLICATION_JSON = "application/json";
	public static final String DATE_ISO= "yyyy-MM-dd HH:mm:ss";

}
