package br.com.dotum.jedi.core.api;

import java.util.Date;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.XML;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.StringUtils;

public abstract class AbstractAPI {
	private static Log LOG = LogFactory.getLog(AbstractAPI.class);

	private boolean debug;
	private ApiBean request;
	private ApiBean response;
	private ApiBean headers = new ApiBean();
	private String url;
	private Integer status;
	
	public abstract ApiBean doProcessResponse(ApiBean response) throws Exception;
	
	protected ApiBean post(String URL, String bodyJson) throws Exception {
		this.url = URL;
		HttpPost request = new HttpPost(this.url);
		doBody(request, bodyJson);
		return doRequest(request);
	}
	
	protected ApiBean put(String URL, String bodyJson) throws Exception {
		this.url = URL;
		HttpPut request = new HttpPut(this.url);
		doBody(request, bodyJson);
		return doRequest(request);
	}

	protected ApiBean get(String URL) throws Exception {
		this.url = URL;
		HttpGet request = new HttpGet(this.url);
		return doRequest(request);
	}
	
	private ApiBean doRequest(HttpRequestBase request) throws Exception {
		
		HttpClient  client = HttpClientBuilder.create().build();
		doFormatHeader(request);
		
		HttpResponse response = client.execute(request);
		doFormatResponse(response);
		if(isDebug()) {
			apiClientLog();
		}
		return doProcessResponse(this.response);
	}
	

	private void doBody(HttpEntityEnclosingRequestBase request, String bodyJson) throws Exception {
		if(bodyJson != null) {
			try {
				this.request = new ApiBean(bodyJson);
			} catch (Exception e) {
				this.request = new ApiBean("{\"body\": "+bodyJson+"}");
			}
			
			String payload = bodyJson;
			StringEntity entity = new StringEntity(payload, ContentType.APPLICATION_JSON);
			request.setEntity(entity);
		}
		
	}
	
	private void doFormatResponse(HttpResponse response) throws Exception {
		HttpEntity body = response.getEntity();
		String content = EntityUtils.toString(body);
		
		setStatus(response.getStatusLine().getStatusCode());
		
		JSONObject json = null;
		String contentType = response.getFirstHeader("Content-Type").getValue();
		if (contentType.contains("xml")) {
			json = XML.toJSONObject(content);
		} else if (contentType.contains("json")) {
			json = new JSONObject(content);
		} else {
			throw new DeveloperException("Não foi possivel fazer a leitura da resposta HTTP.");
		}
		
//		if(status > 399) {
//			throw new ErrorException(json.toString());
//		}
		this.response = new ApiBean(json.toString());
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public void addHeader(String key, String value) {
		if(StringUtils.hasValue(value) == false) return;
		this.headers.set(key, value);
	}
	
	public String getHeader(String key) {
		return this.headers.get(key);
	}
	
	public String removeHeader(String key) {
		return this.headers.remove(key);
	}
	
	public ApiBean getRequest() {
		return request;
	}

	public ApiBean getResponse() {
		return response;
	}

	public void setResponse(ApiBean response) {
		this.response = response;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	private void doFormatHeader(HttpRequestBase request) throws Exception {
		ApiBean headers = this.headers;
		Header[] headerList = new Header[headers.getAttributes().length];
		
		int count = 0;
		for(String key : headers.getAttributes()) {
			headerList[count] = new BasicHeader(key, headers.get(key));
			count++;
		}
		request.setHeaders(headerList);
	}
	
	private void apiClientLog() throws Exception {
		// Log do Request
		// Log do host
		String host = this.getUrl().split("\\?", 0)[0];
		// Log do param
		Integer status = getStatus();

		ApiBean header = this.headers;
		
		ApiBean params = ApiBean.queryToBean(this.getUrl());
		if(params == null) {
			params = new ApiBean();
		}
		// Log do body
		ApiBean requestBean = this.getRequest();
		if(requestBean == null) {
			requestBean = new ApiBean();
		}
		// Log do reponse
		ApiBean responseBean = this.getResponse();
		if(responseBean == null) {
			responseBean = new ApiBean();
		}
		
		String logStr = "HOST " + host + " [" + status +"] " + " HEADER:" + header + "QUERY JSON:" + params + " REQUEST JSON:" + requestBean + " RESPONSE JSON:" + responseBean;
		LOG.error(logStr);
	}
	
	
}
