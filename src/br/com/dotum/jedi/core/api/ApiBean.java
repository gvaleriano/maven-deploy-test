package br.com.dotum.jedi.core.api;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.http.client.utils.URIBuilder;
import org.exolab.castor.net.URIException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.sun.jna.platform.win32.COM.TypeLibUtil.IsName;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.StringUtils;

public class ApiBean extends Bean {
	
	public ApiBean() {
	}
	
	public ApiBean(ApiBean apiBean) {
		try {
			parseApiBean(apiBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ApiBean(String jsonStr) throws Exception {
		JSONObject json = new JSONObject(jsonStr);
		Field[] fArray = this.getClass().getDeclaredFields();
		if(fArray.length > 0) {
			this.parseApiBean(json);
		}else {
			parseJson(json);
		}
	}
	
	private ApiBean  parseJson(JSONObject json) throws Exception {
		for(String key : json.keySet()) {
			Object value = json.get(key);
			if(value instanceof JSONObject) {
				JSONObject json1 = (JSONObject) value;
				value = new ApiBean(json1.toString());
			}else if(value instanceof JSONArray) {
				List<Object> list = new ArrayList<Object>();
				JSONArray jsonArr = (JSONArray) value;
				for(Object obj: jsonArr) {
					
					if(obj instanceof JSONObject){
						JSONObject json2 = (JSONObject) obj;
						obj = new ApiBean(json2.toString());
					}

					list.add(obj);
				};
				value = list;
			}
			this.set(key, value);
		}
		
		return this;
	}
	
	public <T> T get(String attribute) {
		T value = null;
		String charSplit = ".";
		if(attribute.contains(charSplit)) {
			value = (T) this;
			String[] part = attribute.split("\\"+charSplit);
			for (String key : part) {
				value = ((ApiBean) value).get(key);
			}
		}else {
			value =  super.get(attribute);
		}
		return value;
	}
	
	
	public boolean set(String attribute, Object value) {
		if(value == null) {
			return false;
		}
		if (this.getClass().equals(ApiBean.class)) {
			super.getMap().put(attribute, value);
			return true;
		}else {
			return super.set(attribute, value);
		}
	}
	
	public boolean setAtribute(String attribute, Object value) {
		Boolean trySuper = super.set(attribute, value);
		
		if(trySuper == false) {
			super.getMap().put(attribute, value);
			return true;
		}
		return trySuper;
	}
	
	public  <T> T  remove(String attribute) {
		T attr = super.getAttribute(attribute);
		if(attr != null) {
			super.getMap().remove(attribute);
		}
		return attr;
	}
	
	public void parseApiBean(ApiBean apiBean) throws Exception {
		parseApiBean(new JSONObject(apiBean.getMap()));
	}
	private void parseApiBean(JSONObject json) throws Exception {
		Field[] fArray = this.getClass().getDeclaredFields();
			for (Field f : fArray) {
				ColumnDB cAnn = f.getAnnotation(ColumnDB.class);
				try {
					json.get(cAnn.name());
				} catch (Exception e) {
					continue;
				}
				Object obj = f.getType();
				Object attr = json.get(cAnn.name());
				
				if(obj.equals(Date.class)) {
					attr = getDate(f.getName());
				} else if(obj.equals(Integer.class)) {
					if(attr instanceof String)
						attr = Integer.parseInt((String) attr);
				} else if ( obj.equals(List.class)) {
					List<Object> list = new ArrayList<Object>();
					Type type = f.getGenericType();
					ParameterizedType pt = (ParameterizedType) type;
					
					for(Type xxx : pt.getActualTypeArguments()) {
						Class<?> klass = Class.forName(xxx.getTypeName());
						Object obj2 = klass.newInstance();
						if(obj2.getClass().getSuperclass().equals(ApiBean.class)) {
							JSONArray jsonArr = (JSONArray) attr;
							for (Object xx : jsonArr) {
								Object zz = klass.getDeclaredConstructor(new Class[] {String.class}).newInstance(xx.toString());
								list.add(zz);
							}
						}
					}
					attr = list;
				} else if (obj.equals(String.class)) {
					try {
						attr = (String) attr;
					} catch (Exception e) {
						attr = attr.toString();
					}
				}
				
				this.set(f.getName(), attr);
//				json.remove(cAnn.name());
			}
			
			// Caso tenha atrr que ainda nao tem anotation coloca no hash
			parseJson(json);		
	}

	public String toString() {
		return this.toString(0);
	}
	
	public String toString(int indentFactor) {
		String str = null;
		try {
			str = toJsonObject(false).toString(indentFactor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}
	
	public JSONObject toJsonObject(boolean required) throws Exception {
		JSONObject json = null;
		Field[] fArray = this.getClass().getDeclaredFields();
		if(fArray.length > 0) {
			json = new JSONObject();
		for (Field f : fArray) {
			Object objRelacionado = this.get(f.getName());
			ColumnDB cAnn = f.getAnnotation(ColumnDB.class);
			
			if(objRelacionado == null && cAnn == null) continue;
			
			if(cAnn.required() == true) {
				if(objRelacionado == null && required == true)
					throw new DeveloperException(cAnn.name() + " é um atributo obrigatorio para requisição");
			}
			if(objRelacionado != null && objRelacionado.getClass().equals(Date.class)) {
				objRelacionado = formatDate((Date) objRelacionado);
			}
			if(objRelacionado != null && objRelacionado.getClass().getSuperclass().equals(ApiBean.class)) {
				objRelacionado = ((ApiBean) objRelacionado).toJsonObject(required);
			}else if(objRelacionado instanceof Collection<?>) {
				JSONArray jsonArr = new JSONArray();
				for(Object obj: (Collection<?>) objRelacionado) {
					if(obj instanceof ApiBean) {
						JSONObject json2  = new JSONObject(obj.getClass().getDeclaredConstructor(String.class).newInstance(obj.toString()).toString());
						obj = json2;
					}
					jsonArr.put(obj);
				}
				objRelacionado = jsonArr;
			}
			json.put(cAnn.name(), objRelacionado);
		  }
		}else {
			json = new JSONObject();
			for(String key : this.getAttributes()) {
				Object value = this.get(key);
				if(value instanceof ApiBean) {
					JSONObject json2  = new JSONObject(value.getClass().getDeclaredConstructor(String.class).newInstance(value.toString()).toString());
					value = json2;
				}else if(value instanceof Collection<?>) {
					JSONArray jsonArr = new JSONArray();
					for(Object obj: (Collection<?>) value) {
						if(obj instanceof ApiBean) {
							JSONObject json2  = new JSONObject(obj.getClass().getDeclaredConstructor(String.class).newInstance(obj.toString()).toString());
							obj = json2;
						}
						jsonArr.put(obj);
					}
					value = jsonArr;
				}else if( value instanceof Date) {
					value = formatDate((Date) value);
				}
				json.put(key, value);
			}
		}
		return json;
	}
	
	public String toStringOrder() throws Exception {
		return this.toStringOrder(0);
	}
	
	public String toStringOrder(int indentFactor) throws Exception {
		
		String[] atrrArray = this.getAttributes();
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		String temp = "";
		for (String key: atrrArray) {
			ApiBean b = new ApiBean();
			Object obj = this.get(key);
			if(obj == null) continue;
			if(obj instanceof ApiBean) {
				obj = ((ApiBean)obj).toString(indentFactor);
			}
			b.set(key, obj);
			String kv = b.toJsonObject(false).toString();
			kv = kv.substring(1, kv.length() -1);
			sb.append(temp);
			sb.append(kv);
			temp = ",";
		}
		sb.append("}");
		String newJsonStr = sb.toString();
		
		return newJsonStr;
	}
	
	public String toUrlQuery() throws URIException, NullPointerException, DeveloperException, URISyntaxException {
		URIBuilder uri = new URIBuilder();
		Field[] fArray = this.getClass().getDeclaredFields();
		if((fArray != null) && fArray.length != 0) {
			for (Field f : fArray) {
				Object objRelacionado = this.getAttribute(f.getName());
				if(objRelacionado == null) continue;
				ColumnDB cAnn = f.getAnnotation(ColumnDB.class);
				if(objRelacionado instanceof Date){
					objRelacionado = formatDate((Date) objRelacionado);
				}
				uri.setParameter(cAnn.name(), ""+objRelacionado);
			}
		}else {
			for(String key : this.getAttributes()) {
				uri.setParameter(key, this.getString(key));
			}
		}
		return uri.build().toString();
	}
	
	public static ApiBean queryToBean(String url) throws Exception {
		ApiBean bean = new ApiBean();
		String[] part = url.split("\\?", 0);
		if((part.length <= 1) || part == null) {
			return null;
		}
		String[] nameValue = part[1].split("&");
		for (String temp : nameValue) {
			String[] xx = temp.split("=");
			String name = URLDecoder.decode(xx[0], "UTF-8");
			String value = URLDecoder.decode(xx[1], "UTF-8");
			bean.set(name, value);
		}
		return bean;
	}

	public ApiBean getBean(String attribute) {
		return (ApiBean) getAttribute(attribute);
	}

	public <T> T getList(String attribute) {
		return (T) getAttribute(attribute);
	}

	public String getString(String attribute) throws DeveloperException {
		Object object = this.get(attribute);
        if (object instanceof String) {
            return (String) object;
        }else if (object instanceof Integer) {
        	return Integer.toString((int) object);
        }
        throw new DeveloperException("ApiBean[" + attribute + "] not a string.");
       
	}

	public Date getDate(String attribute) throws DeveloperException {	
		return this.getDate(attribute, DateUtils.ISO_8601_TIME);
	}
	public Date getDate(String attribute, String format) throws DeveloperException {
		Date date = null;
		String dataStr = get(attribute);
		try {
			date = DateUtils.parseDate(dataStr, format);
		} catch (ParseException e) {
			throw new DeveloperException("Não foi possivel converter a data " + dataStr + " usando o formato " + format + ". Caso seja necessario reescreva os metodos getDate() e formatDate() do seu Bean");
		}
		
		return date;
	}
	
	public String formatDate(Date date, String format) {
		return DateUtils.formatDate(date, format);
	}
	
	public String formatDate(Date date) {
		return DateUtils.formatDate(date, DateUtils.ISO_8601_TIME);
	}
	
	public <T> T getList(Class<? extends Bean> classe, String attribute) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		List<Object> list = (List<Object>) getList(attribute);
		List<Bean> listBean = new ArrayList<Bean>();

		// Se mudar para o Bean isso tem que mudar
//		if (classe.getSuperclass().getSuperclass().equals(Bean.class)) {
//			System.out.println("Se mudar para o Bean isso tem que mudar");
//		}
		
		for (Object obj : list) {
			Bean b = classe.getDeclaredConstructor(String.class).newInstance(obj.toString());
			listBean.add(b);
		}
		
		return (T) listBean;
	}
	
	public boolean getBoolean(String attribute) {
		return this.get(attribute);
	}
	
	public Set<String> keySet() {
		return super.getMap().keySet();
	}

	public <T> T get(Class<? extends ApiBean> bean, String attribute) throws Exception {
		// TODO Auto-generated method stub
		return (T) bean.getDeclaredConstructor(String.class).newInstance(this.get(attribute).toString());
	}
	
	public <T> T convert(Class<? extends ApiBean> bean) throws Exception {
		return convert(bean,null, false);
	}
	public <T> T convert(Class<? extends ApiBean> bean, String attribute) throws Exception {
		return convert(bean,attribute, true);
	}
	public <T> T convert(Class<? extends ApiBean> bean, String attribute, boolean lazzy) throws Exception {
		// TODO Auto-generated method stub
		if(lazzy) {
			return (T) bean.getDeclaredConstructor(String.class).newInstance(this.get(attribute).toString());
		}else {
			return (T) bean.getDeclaredConstructor(String.class).newInstance(this.toString());
		}
	}

	public String toXML() throws JSONException, Exception {
		String xml = XML.toString(this.toJsonObject(false));
		return xml;
	}
	
}
