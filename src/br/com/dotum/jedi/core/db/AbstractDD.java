package br.com.dotum.jedi.core.db;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

public abstract class AbstractDD {
	static Log LOG = LogFactory.getLog(AbstractDD.class);

	private static HashMap<Class<? extends Bean>, List<Bean>> map = new HashMap<Class<? extends Bean>, List<Bean>>();
	private Class<? extends Bean> classBeanKey;
	private boolean alterar;
	
	public static void add(Bean b) {
		List<Bean> list = map.get(b.getClass());
		if (list == null) {
			list = new ArrayList<Bean>();
			map.put(b.getClass(), list);
		}
		list.add(b);
	}
	
	
	public AbstractDD(Class<? extends Bean> classBeanKey, boolean alterar) {
		this.classBeanKey = classBeanKey;
		this.alterar = alterar;
	}

	public boolean isAlterar() {
		return alterar;
	}

	public void setAlterar(boolean alterar) {
		this.alterar = alterar;
	}

	public Class<? extends Bean> getClassBean() {
		return classBeanKey;
	}

	public void atualizar(TransactionDB trans) throws Exception {
		List<? extends Bean> list = map.get(classBeanKey);
		if (list == null) {
			throw new DeveloperException("Deu problema ao carregar o DD da classe " + classBeanKey);
		}
		int size = list.size();

		if (alterar == false) {
			int qtdDB = countRegister(trans);
			int qtdBe = size;
			if (qtdDB == qtdBe) return;
		}

		Integer v1 = 0;
		Integer v2 = 0;
		for (int i = 0 ; i < size; i++) {
			Bean bean =  list.get(i);
			if (alterar == false) {

				if (trans.exists(bean)) continue;
				trans.merge(bean);
			} else {
				trans.merge(bean);
			}
			Double t = ((((i*1.0) / (size*1.0))*100.0));
			v2 = t.intValue();
			if (v2.equals(v1) == false) {
				System.out.print( v2 + "% " );
			}
			v1 = v2;
		}
		System.out.println( "100%" );
	}

	public static <T> T getList(Class typeClass) {
		List<Bean> list = map.get(typeClass);
		return (T)list; 
	}
	public static <T> T getBy(Class typeClass, String attribute, Object value) {
		List<Bean> list = map.get(typeClass);
		
		for (Bean bean : list) {
			Object idBean = (Object)bean.getAttribute(attribute);
			if (idBean.equals(value) == false) continue;

			return (T)bean;
		}
		return null;
	}

	private int countRegister(TransactionDB trans) throws Exception {
		List<? extends Bean> list = map.get(classBeanKey);
		if (list.size() == 0) return 0;
		Class<?> beanClass = list.get(0).getClass();

		Field[] pkFields = DBHelper.getFieldPrimaryKey(beanClass);
		List<Integer> ids = new ArrayList<Integer>();

		WhereDB where = new WhereDB();
		for (int i = 0; i < pkFields.length; i++) {
			Field field = pkFields[i];
			FieldDB fAnn = field.getAnnotation(FieldDB.class);
			if (fAnn == null) continue;
			for (Bean bean : list) {
				Integer result = (Integer) bean.getAttribute(fAnn.name());
				if (result != null) {
					ids.add(result); 
				} else {
					throw new DeveloperException("A classe " + beanClass + " esta com informacao nula na propriedade " + field.getName());
				}
			}
			where.add(fAnn.name(), Condition.IN, ids.toArray(new Integer[ids.size()]));
		}
		return trans.count(beanClass, where);
	}
}	
