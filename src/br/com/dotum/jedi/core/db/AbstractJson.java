package br.com.dotum.jedi.core.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.db.WhereDB.Operation;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;

public abstract class AbstractJson {

	private TransactionDB trans;
	private WhereDB where;
	private UserContext userContext;
	private Map<String, Object> map = new HashMap<String, Object>();

	
	private List<Bean> list = new ArrayList<Bean>();
	private String key;
	private String[] display;
	private String filter;
	private String term;


	public final void setProperty(String key, String ... display) {
		this.key = key;
		this.display = display;
	}

	public abstract Object[] doExecute() throws Exception;

	public final void addAll(List<Bean> list) {
		this.list = list;
	}

	public final void add(Bean value) {
		list.add(value);
	}

	public final String getJson() throws Exception {
		if (key == null)
			throw new DeveloperException("Não foi especificado o Key");

		if (display == null)
			throw new DeveloperException("Não foi especificado o Display");

		StringBuilder sb = new StringBuilder();
		sb.append("{\"result\":[" );

		String temp = "";
		for (Bean bean : list) {
			bean.setAttributeToJson(display);
			sb.append( temp );
			sb.append( bean.toJson() );
			temp = ",";
		}
		sb.append("]}" );

		return sb.toString();
	}

	public final void setTerm(String term) {
		this.term = term;
	}

	public final String getTerm() {
		return term;
	}

	public final void addFilter(String filter){
		this.filter = filter;
	}

	public final String getFilter() {
		return filter;
	}

	public final TransactionDB getTransaction() {
		return trans;
	}

	public final void setTransaction(TransactionDB trans) {
		this.trans = trans;
	}

	public UserContext getUserContext() {
		return userContext;
	}

	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}

	public List<Bean> run() throws Exception {
		return run(null);
	}
	
	public List<Bean> run(String filterBy) throws Exception {
		Object[] objArray = doExecute();

		ORM orm = null;
		String sql = null;
		WhereDB where = null;
		Integer limit = null;
		for (int i = 0; i < objArray.length; i++) {
			Object obj = objArray[i];
			if (obj instanceof ORM) orm = (ORM) obj;
			if (obj instanceof String) sql = (String) obj;
			if (obj instanceof WhereDB) where = (WhereDB) obj;
			if (obj instanceof Integer) limit = (Integer) obj;
		}

		if (orm == null)
			throw new DeveloperException("Não foi informado o ORM para executar no json \"" + getClass().getName() + "\".");
		if (sql == null) 
			throw new DeveloperException("Não foi informado o select para executar no json \"" + getClass().getName() + "\".");
		if (where == null) where = new WhereDB();
		if (limit == null) limit = 15;
		if (sql.contains(Bean.RNUM))
			throw new DeveloperException("Não pode inserir o apelido \"" + Bean.RNUM + "\" para as colunas no select no json \""+ getClass().getName() + "\".");

		if (StringUtils.hasValue(filter)) {
			String[] filterPart = filter.split("#");
			for (String parts : filterPart) {
				String[] part = parts.split(";");


				if (part.length < 3)
					throw new DeveloperException("O campo \""+ map.get("name") +"\" que contem o filtro pela propriedade \"" + part[0] + "\" e condição \""+ part[1] +"\" esta com o valor null para filtrar no json \""+ this.getClass().getSimpleName() +"\".");
				
				Object value = parseObject(orm, part[0], part[2]);
				if (value == null) continue;
				where.add(part[0], Condition.getConditionByStr(part[1]), value);
			}
		}
		
		if (this.where != null) {
			for (WhereDB w : this.where.getList()) {
				where.add(w.getGroup(), w.getField(), w.getCondition(), w.getValue());
			}
		}
		
		String[] filterArray = filterBy.split("#");
		for (int i = 0; i < filterArray.length; i++) {
			String filter = filterArray[i];
			if (StringUtils.hasValue(filter) == false) continue;
			
			String[] part = filter.split(";");
			
			Operation xxx = Operation.OR;
			if (i == 0) xxx = Operation.AND;
			if (term == null) term = "%";

			where.add(10, xxx, part[0], Condition.getConditionByStr(part[1]), term.replaceAll(" ", "%"));
			
		}


		
		List<Bean> model = trans.select(Bean.class, orm, sql, where, null, 0, limit);
		return model;
	}
	private Object parseObject(ORM orm, String attribute, String valueStr) throws Exception {
		
		for (ORM o : orm.getList()) {
			if (o.getNameProperty().equals(attribute) == false) continue;

			if (o.getTypeProperty().equals(Integer.class)) {
				if (valueStr.contains(",")) {
					return ArrayUtils.parseToInt(valueStr.split(","));
				} else {
					return Integer.parseInt(valueStr);
				}
			} else if (o.getTypeProperty().equals(Double.class)) {
				return Double.parseDouble(valueStr);
			} else if (o.getTypeProperty().equals(Date.class)) {
				return FormatUtils.parseDate(valueStr);
			} else if (o.getTypeProperty().getSuperclass().equals(Bean.class)) {
				return Integer.parseInt(valueStr);
			} else if (o.getTypeProperty().equals(String.class)) {
				return valueStr;
			}
		}
		
		return null;
	}

	public void setWhere(WhereDB where) {
		this.where = where;
	}

	public void addExtra(String key, Object value) {
		map.put(key, value);		
	}

}