package br.com.dotum.jedi.core.db;


public interface AbstractTableEnum {


	public String getName();
	public String getProperty();
	public String getValue();
	
};


