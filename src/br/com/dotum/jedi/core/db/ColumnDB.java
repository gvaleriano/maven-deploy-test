package br.com.dotum.jedi.core.db;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnDB {

	
	public String name();
	public boolean pk() default false;
	public String fkName() default "";
	public String fk() default "";
	public int length() default -1;
	public int precision() default 0;
	boolean required() default false;
	
	
}


