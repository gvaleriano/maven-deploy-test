package br.com.dotum.jedi.core.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ConnectionDB {
	private static Log LOG = LogFactory.getLog(ConnectionDB.class);

	private static ConnectionDB me;
	public static final String DEFAULT_TRANS_MAP = "_defaultTrans";
	public static final String DEFAULT_CONN_MAP = "_defaultConn";

	public ConnectionDB() {
	}

	public static ConnectionDB getInstance() throws Exception {
		if (me == null) me = new ConnectionDB();
		return me;
	}

	public static String getNamePropertyDB(String property, String nameConn) {
		if (nameConn.equalsIgnoreCase(DEFAULT_TRANS_MAP) == false) nameConn = "." + nameConn + ".";
		if (nameConn.equalsIgnoreCase(DEFAULT_TRANS_MAP)) nameConn = ".";
		return "database"+ nameConn + property;
	}

	protected Connection createConnection(String nameTrans) throws Exception {
		String typeStr = getNamePropertyDB("type", nameTrans);
		String poolStr = getNamePropertyDB("pool", nameTrans);
		String hostStr = getNamePropertyDB("ip", nameTrans);
		String portStr = getNamePropertyDB("port", nameTrans);
		String nameStr = getNamePropertyDB("name", nameTrans);
		String usernameStr = getNamePropertyDB("username", nameTrans);
		String passwordStr = getNamePropertyDB("password", nameTrans);

		String type = PropertiesUtils.get(typeStr, true);
		if (type == null) type = TypeDB.ORACLE;
		boolean pool = Boolean.parseBoolean(PropertiesUtils.get(poolStr, false));
		String host = PropertiesUtils.get(hostStr, false);
		String port = PropertiesUtils.get(portStr, false);
		String name = PropertiesUtils.get(nameStr, false);
		String username = PropertiesUtils.get(usernameStr, false);
		String password = PropertiesUtils.get(passwordStr, false);
		if (StringUtils.hasValue(password) == false) password = "2345vc78";

		if (host == null) throw new DeveloperException("Não foi encontrado a propriedade " + hostStr);
		if (port == null) throw new DeveloperException("Não foi encontrado a propriedade " + portStr);
		if (name == null) throw new DeveloperException("Não foi encontrado a propriedade " + nameStr);
		if (username == null) throw new DeveloperException("Não foi encontrado a propriedade " + usernameStr);

		return createConnection(type, host, port, name, username, password, pool);
	} 

	public static Connection createConnection(String type, String host, String port, String name, String username, String password, boolean pool) throws ClassNotFoundException, SQLException, Exception {
		LOG.debug("Conectando em " + host + ":" + port + " com sid " + name + " - usuario " + username);

		Connection conn = null;

		// Google (APP ENGINE)	
		// if(SystemProperty.environment.value() == SystemProperty.Environment.Value.Production){
		//     Class.forName("com.mysql.jdbc.GoogleDriver");
		//     conn = DriverManager.getConnection("jdbc:google:mysql://"+ host +":"+ port +"/"+ name, username, password);
		// } else
		try {
//			if (type.equalsIgnoreCase(TypeDB.POOL)) {
			if (pool == true) {
				LOG.debug("Pool connection...");
				InitialContext initContext = new InitialContext();
				DataSource datasource = (DataSource)initContext.lookup("java:/comp/env/jdbc/dotumPool");
				conn = datasource.getConnection();
			} else if (type.equalsIgnoreCase(TypeDB.MYSQL)) {
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://"+ host +":"+ port +"/"+ name, username, password);
			} else if (type.equalsIgnoreCase(TypeDB.ORACLE)) {
				LOG.debug("Oracle connection...");
				Class.forName("oracle.jdbc.driver.OracleDriver");
				conn = DriverManager.getConnection("jdbc:oracle:thin:@"+ host +":"+ port +":"+ name, username, password);
			} else if (type.equalsIgnoreCase(TypeDB.POSTGRES)) {
				Class.forName("org.postgresql.Driver");
				conn = DriverManager.getConnection("jdbc:postgresql://"+ host +":"+ port +"/"+ name, username, password);
			}
		} catch (Exception e) {
			LOG.error("Não foi possivel conectar no banco de dados "+ host +":"+ port +"/"+ name + " com o usuario: " + username + ". Se os dados estão Ok verifique o listener :D");
			throw e;
		}
		conn.setAutoCommit(false);
		return conn;
	}
}

