package br.com.dotum.jedi.core.db;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.StringUtils;


public class DBHelper {

	public static String[] getNamePrimaryKey(Class<?> beanClass) {
		Field[] fields = getFieldPrimaryKey(beanClass);
		String[] result = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			result[i] = fields[i].getName();
		}
		return result;

	}
	public static Field[] getFieldPrimaryKey(Class<?> beanClass) {
		List<Field> result = new ArrayList<Field>();

		for (Field field : beanClass.getDeclaredFields()) {
			ColumnDB columnDB = (ColumnDB)field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;
			if (columnDB.pk() == false) continue;

			result.add(field);
		}

		return result.toArray(new Field[result.size()]);
	}

	public static Field[] getFieldForingKey(Class<?> beanClass) {
		List<Field> result = new ArrayList<Field>();
		
		for (Field field : beanClass.getDeclaredFields()) {
			ColumnDB columnDB = (ColumnDB)field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;
			if (StringUtils.hasValue(columnDB.fk()) == false) continue;
			
			result.add(field);
		}
		
		return result.toArray(new Field[result.size()]);
	}
	
	public static String[] getDescriptionDefault(Class<?> beanClass) {
		if (hasField(beanClass, "descricao")) return new String[] {"descricao"};
		if (hasField(beanClass, "fantasia")) return new String[] {"fantasia"};
		if (hasField(beanClass, "nome"))  return new String[] {"nome"};
		if (hasField(beanClass, "nomeRazao")) return new String[] {"nomeRazao"};
		if (hasField(beanClass, "razao")) return new String[] {"razao"};

		return null;
	}
	
	public static boolean hasField(Class<?> beanClass, String property) {
		for (Field field : beanClass.getDeclaredFields()) {

			ColumnDB columnDB = (ColumnDB)field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;
			if (columnDB.pk() == true) continue;

			if (field.getName().equalsIgnoreCase(property)) return true;
		}
		return false;
		
	}

	public static Field getFieldByName(Class<?> classe, String field) {
		for (Field f : classe.getDeclaredFields()) {
			if (f.getName().equalsIgnoreCase(field) == false) continue;
			return f;
		}
		return null;
	}

	public static String getFilter(Class<?> view, String filter) throws Exception {
		try {
			if (view.getSuperclass().equals(View.class)) {
				View v = (View)view.newInstance();
				Field f = view.getDeclaredField(filter);
				filter = (String)f.get(v);
			}
		} catch (NoSuchFieldException e) {
		}
		return filter;
	}

	public static Field getFieldByAnnotation(Class<?> classe, String field) {
		for (Field f : classe.getDeclaredFields()) {
			FieldDB fieldDF = f.getAnnotation(FieldDB.class);
			if (fieldDF  == null) continue;
			if ((fieldDF.name().equalsIgnoreCase(field) == false) && (f.getName().equalsIgnoreCase(field) == false)) continue;
			return f;
		}
		return null;
	}

	public static String getWhereStr(String sql, Class viewClass) throws DeveloperException {
		int end1 = sql.indexOf("/*where*/");
		int end2 = sql.indexOf("/*whereadd*/");
		int end = (end1 == -1) ? end2+12 : end1+9;
		if (end == -1)
			if (viewClass != null) {
				throw new DeveloperException("Falta a tag especial de /*columns*/ ou /*from*/ na view " + viewClass.getName() + ".");
			} else {
				throw new DeveloperException("Falta a tag especial de /*columns*/ ou /*from*/ no select.");
			}

		String whereStr = sql.substring(end);
		return whereStr;
	}
	public static String getFromStr(String sql, Class viewClass) throws DeveloperException {
		int start = sql.indexOf("/*from*/");
		int end1 = sql.indexOf("/*where*/");
		int end2 = sql.indexOf("/*whereadd*/");
		int end = (end1 == -1) ? end2+12 : end1+9;
		if ((start == -1) || (end == -1))
			if (viewClass != null) {
				throw new DeveloperException("Falta a tag especial de /*columns*/ ou /*from*/ na view " + viewClass.getName() + ".");
			} else {
				throw new DeveloperException("Falta a tag especial de /*columns*/ ou /*from*/ no select.");
			}

		String fromStr = sql.substring(start, end); 

		return fromStr;
	}
	public static String getColumnsStr(String sql, Class viewClass) throws DeveloperException {
		int start = sql.indexOf("/*columns*/");
		int end = sql.indexOf("/*from*/");

		if ((start == -1) || (end == -1))
			if (viewClass != null) {
				throw new DeveloperException("Falta a tag especial de /*columns*/ ou /*from*/ na view " + viewClass.getName() + ".");
			} else {
				throw new DeveloperException("Falta a tag especial de /*columns*/ ou /*from*/ no select.");
			}

		String columnStr = sql.substring(start, end+8);

		return columnStr;
	}
	public static String getSelectStr(String sql) {
		String selectStr = sql.split("\\/\\*columns\\*\\/")[0];
		return selectStr;
	}
	
}


