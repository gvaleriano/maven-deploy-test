package br.com.dotum.jedi.core.db;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldDB {
	
	public int type();
	public String name();
	public String label();
	public boolean required() default false;
	public String chave() default "";
	
}


