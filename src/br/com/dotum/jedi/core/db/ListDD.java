package br.com.dotum.jedi.core.db;

import java.util.List;

import br.com.dotum.jedi.util.SQLUtils;

public abstract class ListDD {


	public abstract List<Class<?>> getFullList() throws Exception;

	public String run(TransactionDB trans) throws Exception {
		StringBuilder sb = new StringBuilder();

		List<Class<?>> list = getFullList();

		int i = 0;
		int size = list.size();
		for (Class<?> klass : list) {
			String temp = "["+(++i)+"/"+ size +"] DD " + klass.getSimpleName();
			try {
				AbstractDD abs = (AbstractDD)klass.newInstance();
				abs.atualizar(trans);
				trans.commit();

				sb.append("[OK]" + temp + "\n");
				System.out.print( temp + ": ");
			} catch (Exception e) {
				sb.append("[ERRO] " + temp + ". Motivo: "+ SQLUtils.formatPartStringToShow(e.getMessage()) +"\n");
				trans.rollback();
			}
		}

		return sb.toString();
	}

}
