package br.com.dotum.jedi.core.db;


import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.SelectException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;

public class MySQLDB {
	private static Log LOG = LogFactory.getLog(MySQLDB.class);
	private static final Integer NONE = 1;
	private static final Integer LIMIT = 2;
	private static final Integer COUNT = 3;
	private static final Integer PADRAO = 4;

	protected static <T> T selectByBean(UserContext userContext, Connection c, Class<? extends Bean> beanClass, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength, boolean count) throws NotFoundException, Exception {
		
		
		TableDB tableDB = (TableDB) beanClass.getAnnotation(TableDB.class);
		if (tableDB == null) 
			throw new DeveloperException("O Bean ("+ beanClass.getSimpleName() +") não possui a annotation TableDB.");

		Field[] fieldList = beanClass.getDeclaredFields();

		
		List<Field> fieldBeanList = new ArrayList<Field>();
		for (Field field : fieldList) {
			if (field.getType().getSuperclass() == null) continue;
			if (field.getType().getSuperclass().equals(Bean.class) == false) continue;

			fieldBeanList.add(field);
		}

		StringBuilder sb = new StringBuilder();
		
		
		boolean limit = false;
		if ((iDisplayStart != null) && (iDisplayLength != null)) limit = true; 


		if (count == false) {
			if (limit == true) {
				sb.append( getDisplayRowBegin(LIMIT) );
				sb.append( getColunas(fieldBeanList, fieldList)  );
			} else {
				sb.append( getDisplayRowBegin(PADRAO) );
				sb.append( getColunas(fieldBeanList, fieldList)  );
			}
		} else {
			sb.append( getDisplayRowBegin(COUNT) );
		}

		
		//
		// from da tabela primaria
		//
		sb.append( " from " );
		sb.append( tableDB.name() + " t_");

		//
		// from das tabelas secundarias
		//

		for (int i = 0; i < fieldBeanList.size(); i++) {
			Field field = fieldBeanList.get(i);
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;

			if (fieldDB.required() == false) {
				sb.append( " left outer ");
			}

			Class superBean = field.getType();
			TableDB table = (TableDB)superBean.getAnnotation(TableDB.class);
			if (table == null) 
				throw new DeveloperException("O Bean ("+ superBean.getSimpleName() +") não possui a annotation TableDB.");

			sb.append( " join " + table.name() + " t"+i+" on (");


			if (StringUtils.hasValue(columnDB.fk()) == false) {
				String error = "O \"" + tableDB.name() + "Bean\" esta com a FK para a tabela \"" + field.getName() + "Bean\" montado errado.\n";
				error += "Sugestão de comando: alter table "+ tableDB.name() +" add constraint fk_... foreign key (...) references "+ field.getName() +" (...);\n";
				error += "Apos criar a FK no banco de dados recrie o Bean";

				throw new DeveloperException(error);
			}
			String temp = "";
			String[] joinArray = columnDB.fk().split(" and ");
			for (String join : joinArray) {
				String[] parts = join.split(" = ");


				sb.append( temp );
				sb.append(" t"+i + "." + parts[1] + " = t_." + parts[0]);
				temp = " and ";
			}

			sb.append( ") ");
		}


		//
		// AQUI COLOCAR O WHERE caso exista
		//
		boolean adicinado = false;
		if ((where != null) && (where.size() > 0)) {
			Integer group = null;
			Integer groupOld = null;
			sb.append( " where " );
			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				group = w.getGroup();
				if ((group != null) && (groupOld != null) && (group.equals(groupOld) == false)) {
					sb.append(")");					
				}
				if ((i != 0) && (adicinado == true))  {
					sb.append( " " );
					sb.append( w.getOperation() );
					sb.append( " " );
				}
				if ((group != null) && (group.equals(groupOld) == false)) {
					sb.append("( /*"+ group +"*/ ");					
				}

				// pegar o field do ultimo bean
				Field field = DBHelper.getFieldByAnnotation(beanClass, w.getField());
				String table = "t_";
				if (field == null) {
					int nVezesDoPontos = StringUtils.occurrencesCount( w.getField(), "." );

					String part1 = null;
					String part2 = null;
					if (nVezesDoPontos == 1) {
						part1 = w.getField().split("\\.")[0]; // unidadeOrigem
						part2 = w.getField().split("\\.")[1]; // id ou razao
					}
					Field fieldTemp = DBHelper.getFieldByAnnotation(beanClass, part1 + ".Id");
					for (int k = 0; k < fieldBeanList.size(); k++) {
						Field fTemp = fieldBeanList.get(k);
						
						if (fieldTemp == null)
							throw new DeveloperException("Não foi possivel achar a propriedade " + part1 + ".Id");
						
						if (fieldTemp.equals(fTemp) == false) continue;
						table = "t" + k;
						field = DBHelper.getFieldByName(fTemp.getType(), part2);
						break;
					}



				}
				if (field == null) throw new DeveloperException("Não foi encontrado atributo \""+ w.getField() +"\" para a classe \"" + beanClass.getSimpleName() + "\"");

				boolean ehString = ((w.getValue() instanceof String) || (w.getValue() instanceof String[]));
				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				if (columnDB == null) continue;


				String part = "";
				if (ehString == true) {
					part += "f_removeaccent(" + table + "." + columnDB.name() + ")";
				} else {
					part += table + "." + columnDB.name();
				}
				part += " " + w.getCondition();


				if ((w.getCondition().equals(Condition.NOTIN)) || (w.getCondition().equals(Condition.IN))) {
					int size = 0;
					if (w.getValue() instanceof Integer[]) {
						size = ((Integer[])w.getValue()).length;
					} else {
						size = ((String[])w.getValue()).length;
					}

					String tempVirgula = "";
					part += " (";
					for (int p = 0; p < size; p++) {
						part += tempVirgula;
						part += "?";
						tempVirgula = ",";
					}
					part += ")";
				} else if ((w.getCondition().equals(Condition.ISNOTNULL)) || (w.getCondition().equals(Condition.ISNULL))) {
					// nao precisa de ? pq to passando direto no Codition...
					// logo naodeve entrar no prepareStatement
				} else {
					part += " ?";
				}


				groupOld = group;
				adicinado = true;
				sb.append(part);
			}
			if ((group != null) && (groupOld != null)) {
				sb.append(")");					
			}

			// TODO se tiver algum order by tem que vir aqui

			if ((order != null) && (order.size() > 0)) {
				sb.append( " order by ");
				String temp = "";
				for (OrderDB o : order.getList()) {
					if (o.getField() == null) continue;

					sb.append( temp );

					Field field = DBHelper.getFieldByAnnotation(beanClass, o.getField());
					if (field == null) 
						throw new DeveloperException("Não foi encontrado a propriedade " + o.getField() + " no bean " + beanClass.getName() + " para ordenar");

					ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
					if (columnDB == null) continue;
					sb.append( "t_." + columnDB.name() );
					sb.append( " " );
					sb.append( o.getAsc_desc() );
					temp = ", ";

				}
			}
		}

		if (limit == true) { 
			sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, LIMIT) );
		} else if (count == true){
			sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, COUNT) );
		} else {
			sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, PADRAO) );
		}
		
		String sql = sb.toString();
		LOG.debug( sql );
		
		PreparedStatement ps = TransactionDB.prepareStatement(c, sql, false);
		setParameterByWhere(beanClass, where, ps);
		
		ResultSet rs = null;
		try {
			rs = ps.executeQuery();
		} catch (SQLException e) {
			ps.close();
			throw new SelectException(e.getMessage() + " - " + sql, e);
		}
		
		Integer size = 0;
		List<Bean> result = new ArrayList<Bean>();
		while (rs.next()) {
			if (count == true) {
				size = rs.getInt(Bean.QTD);
			} else {
				Bean obj = beanClass.newInstance();
				result.add( obj );
				obj.set(Bean.RNUM, rs.getInt(Bean.RNUM));

				for (int i = 0; i < fieldList.length; i++) {
					Field field = fieldList[i];
					ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
					if (columnDB == null) continue;

					if (field.getType().equals(Integer.class)) {
						Integer valueTemp = rs.getInt("t_" + columnDB.name());
						if (rs.wasNull() == false) {
							obj.setAttribute(field.getName(), valueTemp);
						}
					} else if (field.getType().equals(Long.class)) {
						Long valueTemp = rs.getLong("t_" + columnDB.name());
						if (rs.wasNull() == false) {
							obj.setAttribute(field.getName(), valueTemp);
						}
					} else if (field.getType().equals(Double.class)) {
						Double valueTemp = rs.getDouble("t_" + columnDB.name());
						if (rs.wasNull() == false) {
							obj.setAttribute(field.getName(), valueTemp);
						}
					} else if (field.getType().equals(Date.class)) {
						java.sql.Date x = rs.getDate("t_" + columnDB.name());
						if (x != null) {
							obj.setAttribute(field.getName(), new Date(x.getTime()));
						}
					} else if (field.getType().equals(String.class)) {
						obj.setAttribute(field.getName(), rs.getString("t_" + columnDB.name()));
					} else if (field.getType().equals(BigDecimal.class)) {
						obj.setAttribute(field.getName(), rs.getBigDecimal("t_" + columnDB.name()));
					} else if (field.getType().equals(BigInteger.class)) {
						obj.setAttribute(field.getName(), new BigInteger(Integer.valueOf(rs.getInt("t_" + columnDB.name())).toString()));
					} else if (field.getType().equals(Blob.class)) {
						byte[] b = rs.getBytes("t_" + columnDB.name());
						if (b != null) {
							Blob blob = new Blob(b);
							obj.setAttribute(field.getName(), blob);
						}
					} else if (field.getType().getSuperclass().equals(Bean.class)) {
						Field[] fields = field.getType().getDeclaredFields();

						int positionTableId = -1;
						for (int j = 0; j < fieldBeanList.size(); j++) {
							Field fieldTemp = fieldBeanList.get(j);
							if (fieldTemp.equals(field) == false) continue;
							positionTableId = j;
							break;
						}

						for (int j = 0; j < fields.length; j++) {
							Field fieldBean = fields[j];
							// 
							ColumnDB annOuter = field.getAnnotation(ColumnDB.class);
							ColumnDB annInner = fieldBean.getAnnotation(ColumnDB.class);
							if (annInner == null) continue;

							String newName = field.getName() + "." + fieldBean.getName();
							if (fieldBean.getType().equals(Integer.class)) {
								Integer valueTemp = rs.getInt("t" + positionTableId + "_" + annInner.name());
								if (rs.wasNull() == false) {
									obj.setAttribute(newName, valueTemp);
								}
							} else if (fieldBean.getType().equals(Double.class)) {
								obj.setAttribute(newName, rs.getDouble("t" + positionTableId + "_" + annInner.name()));
							} else if (fieldBean.getType().equals(Long.class)) {
								obj.setAttribute(newName, rs.getLong("t" + positionTableId + "_" + annInner.name()));
							} else if (fieldBean.getType().equals(Date.class)) {
								java.sql.Date tempDate = rs.getDate("t" + positionTableId + "_" + annInner.name());
								if (tempDate != null) {
									obj.setAttribute(newName, new Date(tempDate.getTime()));
								}
							} else if (fieldBean.getType().equals(String.class)) {
								obj.setAttribute(newName, rs.getString("t" + positionTableId + "_" + annInner.name()));
							} else if (fieldBean.getType().equals(BigDecimal.class)) {
								obj.setAttribute(newName, rs.getBigDecimal("t" + positionTableId + "_" + annInner.name()));
							} else if (fieldBean.getType().equals(BigInteger.class)) {
								obj.setAttribute(newName, new BigInteger(Integer.valueOf(rs.getInt("t" + positionTableId + "_" + annInner.name())).toString()));
							} else if (fieldBean.getType().equals(Blob.class)) {
								// TODO implementar o array de bytes aqui...
								// mas fazer de forma inteligente....
								// colocar opcao para o programador carregar ou nao essa budega
							} else if (fieldBean.getType().getSuperclass().equals(Bean.class)) {
								// aqui vou setar o ID da super classe
								// para ficar igual o JEDI
								Integer valueTemp = rs.getInt("t" + positionTableId + "_" + annInner.name());
								if (rs.wasNull() == false) {
									boolean xxx = obj.setAttribute(newName + ".id", valueTemp);
									if (xxx == false)
										throw new Exception("1 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + newName + ".id");
								}
							} else {
								throw new Exception("2 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
							}
						}
					} else {
						throw new Exception("3 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
					}
				}
			}
		}
		rs.close();
		ps.close();

		
		if (count == true) {
			return ( T ) size;		
		} else {
			if (result.size() == 0)
				throw new NotFoundException("Nenhum "+ tableDB.label().toLowerCase() +" encontrado.");
		}

		return (T) result;		
	}
	protected static <T> T selectByView(UserContext userContext, Connection c, Class<? extends View> viewClass, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength, boolean count) throws NotFoundException, Exception {
		if ((order != null) && (order.size() > 0)) 
			throw new DeveloperException("A empresa dotum ainda nao implementou Order para View. Solicite desenvolvimento");


		View tempView = (View)viewClass.newInstance();
		String sql = tempView.getQuery();
		if (sql == null)
			throw new DeveloperException("Não foi invocado o metodo super.setQuery(String) no construtor da View. Verifique!!!");

		sql = SQLUtils.parseQueryParamDefault(sql, userContext);
		if (sql.contains("$V")) {
			throw new DeveloperException("Não foi especificado o UserContext para o TransactionDB e esta usando variaveis no select");
		}


		//CODIGO TEMPORARIO
		int x = 0;
		int y = 0;
		do{
			x = sql.indexOf("/*[dinamic]");
			y = sql.indexOf("[/dinamic]*/");

			if(x != -1){
				if(y == -1) throw new Exception("Não foi encontrado a tag de fechamento [/dinamic]*/");
				sql = sql.substring(0, x)+sql.substring(y+12, sql.length());
			}
		}while(x != -1);

		// preciso manipular a string do sql para colocar o where que vem em formato de obj.
		StringBuilder sb = new StringBuilder();


		String selectStr = DBHelper.getSelectStr(sql);
		String columnStr = DBHelper.getColumnsStr(sql, viewClass);
		String fromStr = DBHelper.getFromStr(sql, viewClass);
		String whereStr = DBHelper.getWhereStr(sql, viewClass);
		int positionWhere = sql.indexOf("/*where*/");
		int positionWhereAdd = sql.indexOf("/*whereadd*/");




		boolean limit = false;
		if ((iDisplayStart != null) && (iDisplayLength != null)) limit = true; 


		if (count == false) {
			if (limit == true) {
				sb.append( getDisplayRowBegin(LIMIT) );
				sb.append(columnStr + " ");
			} else {
				sb.append( getDisplayRowBegin(PADRAO) );
				sb.append(columnStr + " ");
			}
		} else {
			sb.append( getDisplayRowBegin(COUNT) );
		}
		sb.append(fromStr + " ");

		if ((positionWhere != -1) && (where != null) && (where.size() > 0)) {
			sb.append(" where ");
		} 

		if ((positionWhereAdd != -1) && (where != null) && (where.size() > 0)) {
			sb.append(" and ");
		}

		boolean adicinado = false;
		if ((where != null) && (where.size() > 0)) {
			Integer group = null;
			Integer groupOld = null;

			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				group = w.getGroup();
				if ((group != null) && (groupOld != null) && (group.equals(groupOld) == false)) {
					sb.append(")");					
				}
				if ((i != 0) && (adicinado == true))  {
					sb.append( " " );
					sb.append( w.getOperation() );
					sb.append( " " );
				}
				if ((group != null) && (group.equals(groupOld) == false)) {
					sb.append("( /*"+ group +"*/ ");					
				}


				// aqui tenho que pegar a coluna do field da view
				Class<?> typeColumn = tempView.getTypeFieldByKey(w.getField());
				String nameColumn = tempView.getFieldByKey(w.getField());
				if (nameColumn == null) 
					throw new DeveloperException("A view " + viewClass + " não possui o campo " + w.getField() + ". Use o metodo addField(String, String);");

				boolean ehString = false;
				if (typeColumn == null) ehString = ((w.getValue() instanceof String) || (w.getValue() instanceof String[]));
				if (typeColumn != null) ehString = typeColumn.equals(String.class);

				String part = "";
				if (ehString == true) {
					part += "f_removeaccent(" + nameColumn + ")";
				} else {
					part += nameColumn;
				}
				part += " " + w.getCondition();


				if ((w.getCondition().equals(Condition.NOTIN)) || (w.getCondition().equals(Condition.IN))) {
					int size = 0;
					if (w.getValue() instanceof Integer[]) {
						size = ((Integer[])w.getValue()).length;
					} else {
						size = ((String[])w.getValue()).length;
					}

					String tempVirgula = "";
					part += " (";
					for (int p = 0; p < size; p++) {
						part += tempVirgula;
						part += "?";
						tempVirgula = ",";
					}
					part += ")";
				} else if ((w.getCondition().equals(Condition.ISNOTNULL)) || (w.getCondition().equals(Condition.ISNULL))) {
					// nao precisa de ? pq to passando direto no Codition...
					// logo naodeve entrar no prepareStatement
				} else {
					part += " ?";
				}

				groupOld = group;
				adicinado = true;
				sb.append(part);
			}
			if ((group != null) && (groupOld != null)) {
				sb.append(")");					
			}

		}

		whereStr = addRowNumGroupByIfNecessary(whereStr);
		sb.append(" " + whereStr);
		
		
		if (limit == true) { 
			sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, LIMIT) );
		} else if (count == true){
			sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, COUNT) );
		} else {
			sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, PADRAO) );
		}

		// roda o select propriamente dito
		String sqlTemp = sb.toString();
		LOG.debug(sqlTemp);

		
// 		mudei aqui qualquer coisa volta
//		PreparedStatement ps = conn.prepareStatement(sqlTemp);
		PreparedStatement ps = TransactionDB.prepareStatement(c, sqlTemp, false);
		setParameterByWhere(viewClass, where, ps);

		ResultSet rs = null;
		try {
			rs = ps.executeQuery();
		} catch (SQLException e) {
			ps.close();
			throw new SelectException(e.getMessage() + " - " + sqlTemp, e);
		}

		// montar a lista de bean agora
		Integer size = 0;
		List<Bean> result = new ArrayList<Bean>();
		while (rs.next()) {
			if (count == true) {
				size = rs.getInt(Bean.QTD);
			} else {
				Bean b = new Bean();
				result.add(b);


				String[] field = tempView.getFields();
				for (String key : field) {
					String columnName = tempView.getFieldByKey(key);
					if (columnName.contains(".")) columnName = columnName.split("\\.")[1];
					Object obj = rs.getObject(columnName);
					Class<?> typeColumn = tempView.getTypeFieldByKey(key);
					ResultSetMetaData rsMD = rs.getMetaData();

					int i = 1;
					for (; i <= rsMD.getColumnCount(); i++) {
						String xxx = rsMD.getColumnName(i);
						if (columnName.equalsIgnoreCase(xxx) == false) continue;
						break;
					}
					int scale = rsMD.getScale(i);

					if (obj == null) {
						b.set(key, null);
					} else if (obj instanceof String) {
						b.set(key, (String)obj);
					} else if (obj instanceof Integer) {
						b.set(key, (Integer)obj);
					} else if ((scale == 0) && (obj instanceof BigInteger)) {
						b.set(key, ((BigInteger)obj).intValue());
					} else if ((scale == 0) && (obj instanceof BigDecimal)) {
						b.set(key, ((BigDecimal)obj).intValue());
					} else if (obj instanceof BigDecimal) {
						if ((typeColumn != null) && (typeColumn.equals(Integer.class))) {
							b.set(key, ((BigDecimal)obj).intValue());
						} else {
							b.set(key, ((BigDecimal)obj).doubleValue());
						}
					} else if (obj instanceof java.sql.Date) {
						b.set(key, new java.util.Date(((java.sql.Date) obj).getTime()));
					} else if (obj instanceof GregorianCalendar) {
						b.set(key, ((GregorianCalendar) obj).getTime());
					} else {
						throw new DeveloperException("Tipo de dados desconhecido. Solicitar desenvolvimento. para " + obj.getClass());
					}
				}
			}
		}
		rs.close();
		ps.close();

		if (count == true) {
			return ( T ) size;		
		} else {
			if (result.size() == 0)
				throw new NotFoundException("Nenhum registro da view "+ viewClass.getSimpleName().toLowerCase() +" encontrado.");
		}

		return (T) result;
	}

	private static String addRowNumGroupByIfNecessary(String whereStr) {
		return whereStr;
	}
	protected static int countByBean(UserContext userContext, Connection c, Class<? extends Bean> bean, WhereDB where) throws NotFoundException, Exception {
		int result = selectByBean(userContext, c, bean, where, null, null, null, true);
		return result;
	}
	protected static int countByView(UserContext userContext, Connection c, Class<? extends View> view, WhereDB where) throws NotFoundException, Exception {
		int result = selectByView(userContext, c, view, where, null, null, null, true);
		return result;
	}


	private static void setParameterByWhere(Class<?> classe, WhereDB where, PreparedStatement ps) throws SQLException {
		if ((where != null) && (where.size() > 0)) {

			int position = 1;
			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				if ((w.getCondition().equals(Condition.ISNOTNULL)) || (w.getCondition().equals(Condition.ISNULL))) continue;

				if (w.getValue() == null) {
					ps.setNull(position++, Types.NULL);
				} else if (w.getValue() instanceof Integer) {
					ps.setInt(position++, (Integer)w.getValue());
				} else if (w.getValue() instanceof Double) {
					ps.setDouble(position++, (Double)w.getValue());
				} else if (w.getValue() instanceof Long) {
					ps.setLong(position++, (Long)w.getValue());
				} else if (w.getValue() instanceof Date) {
					java.sql.Date d = new java.sql.Date(((Date)w.getValue()).getTime());
					ps.setDate(position++, d);
				} else if (w.getValue() instanceof String) {
					if (w.getCondition().equals(Condition.LIKE)) {
						ps.setString(position++, "%"+StringUtils.removeAccent(((String)w.getValue()).toLowerCase())+"%");
					} else if (w.getCondition().equals(Condition.LIKEBEGIN)) {
						ps.setString(position++, StringUtils.removeAccent(((String)w.getValue()).toLowerCase())+"%");
					} else if (w.getCondition().equals(Condition.LIKEEND)) {
						ps.setString(position++, "%"+StringUtils.removeAccent(((String)w.getValue()).toLowerCase()));
					} else {
						ps.setString(position++, StringUtils.removeAccent(((String)w.getValue()).toLowerCase()));
					}
				} else if (w.getValue() instanceof Integer[]) {
					Integer[] values = (Integer[])w.getValue();
					for (Integer value : values) {
						ps.setInt(position++, value);
					}
				} else if (w.getValue() instanceof String[]) {
					String[] values = (String[])w.getValue();
					for (String value : values) {

						if (w.getCondition().equals(Condition.LIKE)) {
							ps.setString(position++, "%"+StringUtils.removeAccent(value.toLowerCase())+"%");
						} else if (w.getCondition().equals(Condition.LIKEBEGIN)) {
							ps.setString(position++, StringUtils.removeAccent(value.toLowerCase())+"%");
						} else if (w.getCondition().equals(Condition.LIKEEND)) {
							ps.setString(position++, "%"+StringUtils.removeAccent(value.toLowerCase()));
						} else {
							ps.setString(position++, StringUtils.removeAccent(value.toLowerCase()));
						}

					}
				}
				LOG.debug( "Param[" + i + "] " + w.getValue() );
			}
		}		
	}
	private static Object getColunas(List<Field> fieldBeanList, Field[] fieldList) {
		StringBuilder sb = new StringBuilder();

		//
		// colunas da tabelas primaria
		//
		String temp = "";
		for (Field field : fieldList) {
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;

			sb.append( temp );
			sb.append( "t_." + columnDB.name() + " as t_" + columnDB.name() );
			temp = ", ";
		}

		//
		// colunas das tabelas relacionadas
		//
		for (int i = 0; i < fieldBeanList.size(); i++) {
			Field field = fieldBeanList.get(i);
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;
			Class superBean = field.getType();

			for (Field superField : superBean.getDeclaredFields()) {
				ColumnDB superColumn = (ColumnDB)superField.getAnnotation(ColumnDB.class);
				if (superColumn == null) continue;

				sb.append( temp );
				sb.append( " t"+i+ "." + superColumn.name() + " as t"+ i + "_" + superColumn.name());
				temp = ", ";
			}
		}

		return sb.toString();
		
	}

	public static String prepareQuery(String sql, Integer iDisplayStart, Integer iDisplayLength, boolean count) throws DeveloperException {

		StringBuilder sb = new StringBuilder();
		sql = sql.trim();

		if (sql.startsWith("select")) {
			boolean limit = false;
			if ((iDisplayStart != null) && (iDisplayLength != null)) limit = true; 


			if (count == false) {
				if (limit == true) {
					sb.append( getDisplayRowBegin(LIMIT) );
					sb.append( sql.substring(6) );
					sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, LIMIT) );
				} else {
					sb.append( getDisplayRowBegin(NONE) );
					sb.append( sql );
					sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, NONE) );
				}
			} else {
				String selectStr = DBHelper.getSelectStr(sql);
				String columnStr = DBHelper.getColumnsStr(sql, null);
				String fromStr = DBHelper.getFromStr(sql, null);
				String whereStr = DBHelper.getWhereStr(sql, null);

				sb.append( getDisplayRowBegin(COUNT) );
				sb.append( " " + fromStr + " " + whereStr);
				sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength, NONE) );
			}
		} else {
			sb.append(sql);
		}

		return sb.toString();
	}
	private static String getDisplayRowBegin(Integer tipo) {
		StringBuilder sb = new StringBuilder();
		
		
		if (tipo.equals(LIMIT)) {
			sb.append("select @rownum:=@rownum+1 as "+ Bean.RNUM +", tb1.*");
			sb.append("  from ( select /*+ FIRST_ROWS(n) */" );
		} else if (tipo.equals(COUNT)) {
			sb.append("  select count(1) as qtd ");
		} else if (tipo.equals(PADRAO)) {
			sb.append("select @rownum:=@rownum+1 as "+ Bean.RNUM +", tb1.*");
			sb.append("  from ( select /*+ FIRST_ROWS(n) */" );
		} else if (tipo.equals(NONE)) {
			sb.append("  ");
		}
		
		return sb.toString();
	}
	private static String getDisplayRowEnd(Integer iDisplayStart, Integer iDisplayLength, Integer tipo) {
		if ((iDisplayStart == null) || (iDisplayStart < 0)) iDisplayStart = 0;
		// aqui coloca o limite se for o caso

		StringBuilder sb = new StringBuilder();
		if (tipo.equals(LIMIT)) {
			sb.append(" ) tb1, (SELECT @rownum:=0) r");
			sb.append( " limit "+ iDisplayStart + ", " + iDisplayLength);
		} else if (tipo.equals(COUNT)) {
			
		} else if (tipo.equals(PADRAO)) {
			sb.append(" ) tb1, (SELECT @rownum:=0) r");
		} else if (tipo.equals(NONE)) {
			
		}

		return sb.toString();
	}	


	public static Integer selectMax(UserContext userContext, Connection conn, Bean bean, TableDB tableDB) throws SQLException, Exception {
		Integer result = 0;
		
		Field[] fieldList = bean.getClass().getDeclaredFields();

		StringBuilder sb = new StringBuilder();
		sb.append( "select " );

		boolean hasUnidade = false;
		int count = 0;
		for (Field field : fieldList) {
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);

			if (columnDB == null) continue;
			if (columnDB.pk() == false) continue;
			if (field.getType().getSuperclass().equals(Bean.class)) continue;

			sb.append( "max(" );
			sb.append( columnDB.name() );
			sb.append( ") as id" );
			count++;
		}
		if (count != 1)
			throw new DeveloperException("Situação não prevista no banco de dados. Pk da tabela ("+ bean.getClass().getSimpleName() +") esta errada.");

		sb.append( " from " );
		sb.append( tableDB.name() );
		if (hasUnidade == true) sb.append( " where uni_id = " + userContext.getUnidade().getId());
		

		LOG.debug( sb.toString() );
		//	 		mudei aqui qualquer coisa volta
		//			PreparedStatement ps = conn.prepareStatement(sb.toString());
		PreparedStatement ps = TransactionDB.prepareStatement(conn, sb.toString(), false);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			result = rs.getInt("id");
		}
		rs.close();
		ps.close();
		result++;
		
		if (result <= 0) result = 1;
		return result;
	}
}




