package br.com.dotum.jedi.core.db;

import java.util.ArrayList;
import java.util.List;

public class ORM {

	private List<ORM> orms = new ArrayList<ORM>();
	private Class<?> typeProperty;
	private String acronym;
	private String nameProperty;
	private String nameColumn;
	
	public ORM() {
	}
	
	private ORM(Class<?> typeProperty, String nameProperty, String nameColumn) {
		this(typeProperty, nameProperty, null, nameColumn);
	}
	private ORM(Class<?> typeProperty, String nameProperty, String acronym, String nameColumn) {
		super();
		this.acronym = acronym;
		this.typeProperty = typeProperty;
		this.nameProperty = nameProperty;
		this.nameColumn = nameColumn;
	}
	
	
	public void add(Class<?> typeProperty, String nameProperty, String nameColumn) {
		orms.add(new ORM(typeProperty, nameProperty, nameColumn));
	}
	
	public void add(Class<?> typeProperty, String nameProperty, String acronym, String nameColumn) {
		orms.add(new ORM(typeProperty, nameProperty, acronym, nameColumn));
	}
	
	public Class<?> getTypeProperty() {
		return typeProperty;
	}
	public void setTypeProperty(Class<?> typeProperty) {
		this.typeProperty = typeProperty;
	}
	public String getNameProperty() {
		return nameProperty;
	}
	public void setKey(String nameProperty) {
		this.nameProperty = nameProperty;
	}
	public String getNameColumn() {
		return nameColumn;
	}
	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public void setNameColumn(String nameColumn) {
		this.nameColumn = nameColumn;
	}

	public List<ORM> getList() {
		return orms;
	}

	public boolean has(String property) {
		for (ORM orm : getList()) {
			if (orm.getNameProperty().equalsIgnoreCase(property) == false) continue;
			return true;
		}
		return false;
	}
	
	
	
}


