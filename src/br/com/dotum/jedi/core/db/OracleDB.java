package br.com.dotum.jedi.core.db;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.db.bean.AuditoriaBean;
import br.com.dotum.jedi.core.db.bean.AuditoriaTipoBean;
import br.com.dotum.jedi.core.db.bean.ComponenteBean;
import br.com.dotum.jedi.core.db.bean.ComponenteEstatisticaBean;
import br.com.dotum.jedi.core.db.bean.ComponenteTipoBean;
import br.com.dotum.jedi.core.db.bean.GrupoAcessoBean;
import br.com.dotum.jedi.core.db.bean.GrupoAcessoTarefaBean;
import br.com.dotum.jedi.core.db.bean.FwReleaseBean;
import br.com.dotum.jedi.core.db.bean.TarefaBean;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.SelectException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.gen.GeneratorModel;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;
import oracle.sql.CLOB;


public class OracleDB {
	private static Log LOG = LogFactory.getLog(OracleDB.class);
	private static final String PL = "\n";
	private static final Integer NONE = 1;
	private static final Integer LIMIT = 2;
	private static final Integer COUNT = 3;
	private static final Integer PADRAO = 4;

	private String lastTimeRun;

	public static String getSql(UserContext userContext, Connection c, Class<? extends Bean> beanClass, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength, boolean lazy, boolean count) throws NotFoundException, Exception {
		TableDB tableDB = (TableDB) beanClass.getAnnotation(TableDB.class);
		if (tableDB == null) 
			throw new DeveloperException("O Bean ("+ beanClass.getSimpleName() +") não possui a annotation TableDB.");

		Field[] fieldList = beanClass.getDeclaredFields();


		List<Field> fieldBeanList = new ArrayList<Field>();
		for (Field field : fieldList) {
			if (field.getType().getSuperclass() == null) continue;
			if (field.getType().getSuperclass().equals(Bean.class) == false) continue;

			fieldBeanList.add(field);
		}

		String temp = "";
		StringBuilder sb = new StringBuilder();

		boolean limit = false;
		if ((iDisplayStart != null) && (iDisplayLength != null)) limit = true; 


		if (count == false) {
			if (limit == true) {
				sb.append( getDisplayRowBegin(LIMIT) );
				sb.append( getColunas(fieldBeanList, fieldList, lazy)  );
			} else {
				sb.append( getDisplayRowBegin(PADRAO) );
				sb.append( getColunas(fieldBeanList, fieldList, lazy)  );
			}
		} else {
			sb.append( getDisplayRowBegin(COUNT) );
		}



		//
		// from da tabela primaria
		//
		sb.append( PL );
		sb.append( "       from " );
		sb.append( tableDB.name() + " t_"+PL);

		//
		// from das tabelas secundarias
		//

		for (int i = 0; i < fieldBeanList.size(); i++) {
			Field field = fieldBeanList.get(i);
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;

			if (fieldDB.required() == false) {
				sb.append( "  left ");
			} else {
				sb.append( "       ");
			}

			Class superBean = field.getType();
			TableDB table = (TableDB)superBean.getAnnotation(TableDB.class);
			if (table == null) 
				throw new DeveloperException("O Bean ("+ superBean.getSimpleName() +") não possui a annotation TableDB.");

			sb.append( "join " + table.name() + " t"+i+" on (");


			if (StringUtils.hasValue(columnDB.fk()) == false) {
				String error = "O \"" + tableDB.name() + "Bean\" esta com a FK para a tabela \"" + field.getName() + "Bean\" montado errado.\n";
				error += "Sugestão de comando: alter table "+ tableDB.name() +" add constraint fk_... foreign key (...) references "+ field.getName() +" (...);\n";
				error += "Apos criar a FK no banco de dados recrie o Bean";

				throw new DeveloperException(error);
			}
			temp = "";
			String[] joinArray = columnDB.fk().split(" and ");
			for (String join : joinArray) {
				String[] parts = join.split(" = ");


				sb.append( temp );
				sb.append("t"+i + "." + parts[1] + " = t_." + parts[0]);
				temp = " and ";
			}

			sb.append( ") "+PL);
		}


		//
		// AQUI COLOCAR O WHERE caso exista
		//
		boolean adicinado = false;
		if ((where != null) && (where.size() > 0)) {
			Integer group = null;
			Integer groupOld = null;
			sb.append( " where " );
			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				group = w.getGroup();
				if ((group != null) && (groupOld != null) && (group.equals(groupOld) == false)) {
					sb.append(")");					
				}
				if ((i != 0) && (adicinado == true))  {
					sb.append( " " );
					sb.append( w.getOperation() );
					sb.append( " " );
				}
				if ((group != null) && (group.equals(groupOld) == false)) {
					sb.append("( /*"+ group +"*/ ");					
				}

				Object[] objs = getFieldSmart(beanClass, fieldBeanList, w.getField());
				Field field = (Field) objs[0];
				String table = (String) objs[1];


				boolean ehString = ((w.getValue() instanceof String) || (w.getValue() instanceof String[]));
				boolean ehDate = ((w.getValue() instanceof java.util.Date) || (w.getValue() instanceof java.util.Date[]));
				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				if (columnDB == null) continue;

				String part = "";
				if (ehString == true) {
					part += f_removeaccent(table + "." + columnDB.name());
				} else if (ehDate == true) {
					part += "to_date(" + table + "." + columnDB.name() + ", 'DD/MM/RR')";
				} else {
					part += table + "." + columnDB.name();
				}
				part += " " + w.getCondition();


				if ((w.getCondition().equals(Condition.NOTIN)) || (w.getCondition().equals(Condition.IN))) {
					int size = 0;
					if (w.getValue() instanceof Integer[]) {
						size = ((Integer[])w.getValue()).length;
					} else {
						size = ((String[])w.getValue()).length;
					}

					String tempVirgula = "";
					part += " (";
					for (int p = 0; p < size; p++) {
						part += tempVirgula;
						part += "?";
						tempVirgula = ",";
					}
					part += ")";
				} else if ((w.getCondition().equals(Condition.ISNOTNULL)) || (w.getCondition().equals(Condition.ISNULL))) {
					// nao precisa de ? pq to passando direto no Codition...
					// logo naodeve entrar no prepareStatement
				} else {
					part += " ?";
				}


				groupOld = group;
				adicinado = true;
				sb.append(part);
			}
			if ((group != null) && (groupOld != null)) {
				sb.append(")");					
			}
		}

		// TODO se tiver algum order by tem que vir aqui
		if ((order != null) && (order.size() > 0)) {
			sb.append( " order by ");
			temp = "";
			for (OrderDB o : order.getList()) {
				if (o.getField() == null) continue;

				sb.append( temp );


				Field field = DBHelper.getFieldByAnnotation(beanClass, o.getField());
				if (field == null) 
					throw new DeveloperException("Não foi encontrado o atributo \"" + o.getField() + "\" no bean \"" + beanClass.getName() + "\" para ordenar.");

				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				if (columnDB == null) continue;
				sb.append( "t_." + columnDB.name() );
				sb.append( " " );
				sb.append( o.getAsc_desc() );
				temp = ", ";

			}
		}

		sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength) );
		String sql = sb.toString();

		return sql;
	}

	protected <T> T selectByBean(UserContext userContext, Connection c, Class<? extends Bean> beanClass, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength, boolean lazy, boolean count) throws NotFoundException, Exception {
		Date d1 = new Date();

		TableDB tableDB = (TableDB) beanClass.getAnnotation(TableDB.class);
		if (tableDB == null) 
			throw new DeveloperException("O Bean ("+ beanClass.getSimpleName() +") não possui a annotation TableDB.");

		Field[] fieldList = beanClass.getDeclaredFields();


		List<Field> fieldBeanList = new ArrayList<Field>();
		for (Field field : fieldList) {
			if (field.getType().getSuperclass() == null) continue;
			if (field.getType().getSuperclass().equals(Bean.class) == false) continue;

			fieldBeanList.add(field);
		}

		String temp = "";
		StringBuilder sb = new StringBuilder();

		boolean limit = false;
		if ((iDisplayStart != null) && (iDisplayLength != null)) limit = true; 


		if (count == false) {
			if (limit == true) {
				sb.append( getDisplayRowBegin(LIMIT) );
				sb.append( getColunas(fieldBeanList, fieldList, lazy)  );
			} else {
				sb.append( getDisplayRowBegin(PADRAO) );
				sb.append( getColunas(fieldBeanList, fieldList, lazy)  );
			}
		} else {
			sb.append( getDisplayRowBegin(COUNT) );
		}



		//
		// from da tabela primaria
		//
		sb.append( " from " );
		sb.append( tableDB.name() + " t_");

		//
		// from das tabelas secundarias
		//

		for (int i = 0; i < fieldBeanList.size(); i++) {
			Field field = fieldBeanList.get(i);
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;

			if (fieldDB.required() == false) {
				sb.append( " left outer ");
			}

			Class superBean = field.getType();
			TableDB table = (TableDB)superBean.getAnnotation(TableDB.class);
			if (table == null) 
				throw new DeveloperException("O Bean ("+ superBean.getSimpleName() +") não possui a annotation TableDB.");

			sb.append( " join " + table.name() + " t"+i+" on (");


			if (StringUtils.hasValue(columnDB.fk()) == false) {
				String error = "O \"" + tableDB.name() + "Bean\" esta com a FK para a tabela \"" + field.getName() + "Bean\" montado errado.\n";
				error += "Sugestão de comando: alter table "+ tableDB.name() +" add constraint fk_... foreign key (...) references "+ field.getName() +" (...);\n";
				error += "Apos criar a FK no banco de dados recrie o Bean";

				throw new DeveloperException(error);
			}
			temp = "";
			String[] joinArray = columnDB.fk().split(" and ");
			for (String join : joinArray) {
				String[] parts = join.split(" = ");


				sb.append( temp );
				sb.append(" t"+i + "." + parts[1] + " = t_." + parts[0]);
				temp = " and ";
			}

			sb.append( ") ");
		}


		//
		// AQUI COLOCAR O WHERE caso exista
		//
		boolean adicinado = false;
		if ((where != null) && (where.size() > 0)) {
			Integer group = null;
			Integer groupOld = null;
			sb.append( " where " );
			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				group = w.getGroup();
				if ((groupOld != null) && (groupOld.equals(group) == false)) {
					sb.append(")");					
				}

				if ((i != 0) && (adicinado == true))  {
					sb.append( " " );
					sb.append( w.getOperation() );
					sb.append( " " );
				}
				if ((group != null) && (group.equals(groupOld) == false)) {
					sb.append("( /*"+ group +"*/ ");					
				}

				// pegar o field do ultimo bean
				Object[] objs = getFieldSmart(beanClass, fieldBeanList, w.getField());
				Field field = (Field) objs[0];
				String table = (String) objs[1];

				boolean ehString = ((w.getValue() instanceof String) || (w.getValue() instanceof String[]));
				boolean ehDate = ((w.getValue() instanceof java.util.Date) || (w.getValue() instanceof java.util.Date[]));
				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				if (columnDB == null) continue;

				String part = "";
				if (ehString == true) {
					part += f_removeaccent(table + "." + columnDB.name());
				} else if (ehDate == true) {
					part += "to_date(" + table + "." + columnDB.name() + ", 'DD/MM/RR')";
				} else {
					part += table + "." + columnDB.name();
				}
				part += " " + w.getCondition();


				if ((w.getCondition().equals(Condition.NOTIN)) || (w.getCondition().equals(Condition.IN))) {
					int size = 0;
					if (w.getValue() instanceof Integer[]) {
						size = ((Integer[])w.getValue()).length;
					} else if (w.getValue() instanceof Date[]) {
						size = ((Date[])w.getValue()).length;
					} else {
						size = ((String[])w.getValue()).length;
					}

					if (size == 0)
						throw new NotFoundException("O parametro \"" + w.getField() + "\" esta sendo filtrado sem nenhum valor para a clausula "+ w.getCondition() +".");

					String tempVirgula = "";
					part += " (";
					for (int p = 0; p < size; p++) {
						part += tempVirgula;
						part += "?";
						tempVirgula = ",";
					}
					part += ")";
				} else if ((w.getCondition().equals(Condition.ISNOTNULL)) || (w.getCondition().equals(Condition.ISNULL))) {
					// nao precisa de ? pq to passando direto no Codition...
					// logo naodeve entrar no prepareStatement
				} else {
					part += " ?";
				}


				if (group != null) 
					groupOld = group;
				adicinado = true;
				sb.append(part);
			}
			if ((group != null) && (groupOld != null)) {
				sb.append(")");					
			}
		}

		// TODO se tiver algum order by tem que vir aqui
		if ((order != null) && (order.size() > 0)) {
			sb.append( " order by ");
			temp = "";
			for (OrderDB o : order.getList()) {
				if (o.getField() == null) continue;

				sb.append( temp );


				Object[] objs = getFieldSmart(beanClass, fieldBeanList, o.getField());
				Field field = (Field) objs[0];
				String table = (String) objs[1];

				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				if (columnDB == null) continue;
				sb.append( table + "." + columnDB.name() );
				sb.append( " " );
				sb.append( o.getAsc_desc() );
				temp = ", ";

			}
		}

		sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength) );
		String sql = sb.toString();
		LOG.debug( sql );

		// 		mudei aqui qualquer coisa volta
		//		PreparedStatement ps = conn.prepareStatement(sb.toString());
		PreparedStatement ps = TransactionDB.prepareStatement(c, sql, false);
		setParameterByWhere(beanClass, where, ps);


		ResultSet rs = null;
		try {
			rs = ps.executeQuery();
		} catch (SQLException e) {
			ps.close();
			throw new SelectException(e.getMessage() + " - " + sql, e);
		}

		Integer size = 0;
		List<Bean> result = new ArrayList<Bean>();
		while (rs.next()) {
			if (count == true) {
				size = rs.getInt(Bean.QTD);
			} else {
				Bean obj = beanClass.newInstance();
				result.add( obj );
				obj.set(Bean.RNUM, rs.getInt(Bean.RNUM));

				for (int i = 0; i < fieldList.length; i++) {
					Field field = fieldList[i];
					ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
					if (columnDB == null) continue;

					if (field.getType().equals(Integer.class)) {
						Integer valueTemp = rs.getInt("t_" + columnDB.name());
						if (rs.wasNull() == false) {
							obj.setAttribute(field.getName(), valueTemp);
						}
					} else if (field.getType().equals(Long.class)) {
						Long valueTemp = rs.getLong("t_" + columnDB.name());
						if (rs.wasNull() == false) {
							obj.setAttribute(field.getName(), valueTemp);
						}
					} else if (field.getType().equals(Double.class)) {
						Double valueTemp = rs.getDouble("t_" + columnDB.name());
						if (rs.wasNull() == false) {
							obj.setAttribute(field.getName(), valueTemp);
						}
					} else if (field.getType().equals(Date.class)) {
						java.sql.Date x = rs.getDate("t_" + columnDB.name());
						if (x != null) {
							obj.setAttribute(field.getName(), new Date(x.getTime()));
						}
					} else if (field.getType().equals(String.class)) {
						obj.setAttribute(field.getName(), rs.getString("t_" + columnDB.name()));
					} else if (field.getType().equals(BigDecimal.class)) {
						obj.setAttribute(field.getName(), rs.getBigDecimal("t_" + columnDB.name()));
					} else if (field.getType().equals(BigInteger.class)) {
						obj.setAttribute(field.getName(), new BigInteger(Integer.valueOf(rs.getInt("t_" + columnDB.name())).toString()));
					} else if (field.getType().equals(Blob.class)) {
						byte[] b = rs.getBytes("t_" + columnDB.name());
						if (b != null) {
							Blob blob = new Blob(b);
							obj.setAttribute(field.getName(), blob);
						}
					} else if (field.getType().getSuperclass().equals(Bean.class)) {
						Field[] fields = field.getType().getDeclaredFields();

						int positionTableId = -1;
						for (int j = 0; j < fieldBeanList.size(); j++) {
							Field fieldTemp = fieldBeanList.get(j);
							if (fieldTemp.equals(field) == false) continue;
							positionTableId = j;
							break;
						}

						for (int j = 0; j < fields.length; j++) {
							Field fieldBean = fields[j];
							// 
							ColumnDB annOuter = field.getAnnotation(ColumnDB.class);
							ColumnDB annInner = fieldBean.getAnnotation(ColumnDB.class);
							FieldDB filOuter = field.getAnnotation(FieldDB.class);
							FieldDB filInner = fieldBean.getAnnotation(FieldDB.class);
							if (annInner == null) continue;
							if ((lazy == true) && (annInner.pk() == false)) continue; 

							String newName = field.getName() + "." + fieldBean.getName();
							if (fieldBean.getType().equals(Integer.class)) {
								Integer valueTemp = rs.getInt("t" + positionTableId + "_" + annInner.name());
								if (rs.wasNull() == false) {
									obj.setAttribute(newName, valueTemp);
								}
							} else if (fieldBean.getType().equals(Double.class)) {
								obj.setAttribute(newName, rs.getDouble("t" + positionTableId + "_" + annInner.name()));
							} else if (fieldBean.getType().equals(Long.class)) {
								obj.setAttribute(newName, rs.getLong("t" + positionTableId + "_" + annInner.name()));
							} else if (fieldBean.getType().equals(Date.class)) {
								java.sql.Date tempDate = rs.getDate("t" + positionTableId + "_" + annInner.name());
								if (tempDate != null) {
									obj.setAttribute(newName, new Date(tempDate.getTime()));
								}
							} else if (fieldBean.getType().equals(String.class)) {
								obj.setAttribute(newName, rs.getString("t" + positionTableId + "_" + annInner.name()));
							} else if (fieldBean.getType().equals(BigDecimal.class)) {
								obj.setAttribute(newName, rs.getBigDecimal("t" + positionTableId + "_" + annInner.name()));
							} else if (fieldBean.getType().equals(BigInteger.class)) {
								obj.setAttribute(newName, new BigInteger(Integer.valueOf(rs.getInt("t" + positionTableId + "_" + annInner.name())).toString()));
							} else if (fieldBean.getType().equals(Blob.class)) {
								byte[] b = rs.getBytes("t" + positionTableId + "_" + annInner.name());
								if (b != null) {
									Blob blob = new Blob(b);
									obj.setAttribute(newName, blob);
								}
							} else if (fieldBean.getType().getSuperclass().equals(Bean.class)) {
								// aqui vou setar o ID da super classe
								// para ficar igual o JEDI
								Integer valueTemp = rs.getInt("t" + positionTableId + "_" + annInner.name());
								if (rs.wasNull() == false) {


									String pk = filInner.name().split("\\.")[1];
									boolean xxx = obj.setAttribute(newName + "." + pk, valueTemp);
									if (xxx == false)
										throw new Exception("1 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + newName + "." + pk);
								}
							} else {
								throw new Exception("2 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
							}
						}
					} else {
						throw new Exception("3 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
					}
				}
			}
		}

		this.lastTimeRun = TransactionDB.logSlow(userContext, d1, sql, beanClass);
		if (rs != null) rs.close();
		if (ps != null) ps.close();

		if (count == true) {
			return ( T ) size;		
		} else {
			if (result.size() == 0)
				throw new NotFoundException("Nenhum "+ tableDB.label().toLowerCase() +" encontrado.");
		}

		return (T) result;		
	}
	private static Object[] getFieldSmart(Class<? extends Bean> beanClass, List<Field> fieldBeanList, String fieldName) throws DeveloperException {
		Object[] obj = new Object[2];

		Field field = DBHelper.getFieldByAnnotation(beanClass, fieldName);
		String table = "t_";
		if (field == null) {
			int nVezesDoPontos = StringUtils.occurrencesCount( fieldName, "." );

			String part1 = null;
			String part2 = null;
			if ((nVezesDoPontos >= 1) && (nVezesDoPontos <= 2)) {
				part1 = fieldName.split("\\.")[0]; // unidadeOrigem
				part2 = fieldName.split("\\.")[1]; // id ou razao
			}
			Field fieldTemp = DBHelper.getFieldByAnnotation(beanClass, part1 + ".Id");
			if (fieldTemp == null)
				throw new DeveloperException("Não foi encontrado o atributo \"" + fieldName + "\" no bean \""+ beanClass.getName() +"\".");

			for (int k = 0; k < fieldBeanList.size(); k++) {
				Field fTemp = fieldBeanList.get(k);
				if (fieldTemp.equals(fTemp) == false) continue;
				table = "t" + k;
				field = DBHelper.getFieldByName(fTemp.getType(), part2);
				break;
			}



		}
		if (field == null) 
			throw new DeveloperException("Não foi encontrado o atributo \""+ fieldName +"\" no bean \"" + beanClass.getName() + "\".");

		obj[0] = field;
		obj[1] = table;

		return obj;
	}

	protected <T> T selectByView(UserContext userContext, Connection c, Class<? extends View> viewClass, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength, boolean count) throws NotFoundException, Exception {
		Date d1 = new Date();

		if ((order != null) && (order.size() > 0)) 
			throw new DeveloperException("A empresa dotum ainda nao implementou Order para View. Solicite desenvolvimento");


		View tempView = (View)viewClass.newInstance();
		String sql = tempView.getQuery();
		if (sql == null)
			throw new DeveloperException("Não foi invocado o metodo super.setQuery(String) no construtor da View. Verifique!!!");

		sql = SQLUtils.parseQueryParamDefault(sql, userContext);
		if (sql.contains("$V")) {
			throw new DeveloperException("Não foi especificado o UserContext para o TransactionDB e esta usando variaveis no select");
		}


		//CODIGO TEMPORARIO
		int x = 0;
		int y = 0;
		do{
			x = sql.indexOf("/*[dinamic]");
			y = sql.indexOf("[/dinamic]*/");

			if(x != -1){
				if(y == -1) throw new Exception("Não foi encontrado a tag de fechamento [/dinamic]*/");
				sql = sql.substring(0, x)+sql.substring(y+12, sql.length());
			}
		}while(x != -1);

		// preciso manipular a string do sql para colocar o where que vem em formato de obj.
		StringBuilder sb = new StringBuilder();


		String selectStr = DBHelper.getSelectStr(sql);
		String columnStr = DBHelper.getColumnsStr(sql, viewClass);
		String fromStr = DBHelper.getFromStr(sql, viewClass);
		String whereStr = DBHelper.getWhereStr(sql, viewClass);
		int positionWhere = sql.indexOf("/*where*/");
		int positionWhereAdd = sql.indexOf("/*whereadd*/");




		boolean limit = false;
		if ((iDisplayStart != null) && (iDisplayLength != null)) limit = true; 


		if (count == false) {
			if (limit == true) {
				sb.append( getDisplayRowBegin(LIMIT) );
				sb.append(columnStr + " ");
			} else {
				sb.append( getDisplayRowBegin(PADRAO) );
				sb.append(columnStr + " ");
			}
		} else {
			sb.append( getDisplayRowBegin(COUNT) );
		}
		sb.append(fromStr + " ");

		if ((positionWhere != -1) && (where != null) && (where.size() > 0)) {
			sb.append(" where ");
		} 

		if ((positionWhereAdd != -1) && (where != null) && (where.size() > 0)) {
			sb.append(" and ");
		}

		boolean adicinado = false;
		if ((where != null) && (where.size() > 0)) {
			Integer group = null;
			Integer groupOld = null;

			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				group = w.getGroup();
				if ((group != null) && (groupOld != null) && (group.equals(groupOld) == false)) {
					sb.append(")");					
				}
				if ((i != 0) && (adicinado == true))  {
					sb.append( " " );
					sb.append( w.getOperation() );
					sb.append( " " );
				}
				if ((group != null) && (group.equals(groupOld) == false)) {
					sb.append("( /*"+ group +"*/ ");					
				}


				// aqui tenho que pegar a coluna do field da view
				Class<?> typeColumn = tempView.getTypeFieldByKey(w.getField());
				String nameColumn = tempView.getFieldByKey(w.getField());
				if (nameColumn == null) 
					throw new DeveloperException("A view " + viewClass + " não possui o campo " + w.getField() + ". Use o metodo addField(String, String);");

				boolean ehString = false;
				boolean ehDate = false;
				if (typeColumn == null) ehString = ((w.getValue() instanceof String) || (w.getValue() instanceof String[]));
				if (typeColumn != null) ehString = typeColumn.equals(String.class);
				if (typeColumn == null) ehDate = ((w.getValue() instanceof java.util.Date) || (w.getValue() instanceof java.util.Date[]));
				if (typeColumn != null) ehDate = typeColumn.equals(Date.class);

				String part = "";
				if (ehString == true) {
					part += f_removeaccent(nameColumn);
				} else if (ehDate == true) {
					part += "to_date(" + nameColumn + ", 'DD/MM/RR')";
				} else {
					part += nameColumn;
				}
				part += " " + w.getCondition();


				if ((w.getCondition().equals(Condition.NOTIN)) || (w.getCondition().equals(Condition.IN))) {
					int size = 0;
					if (w.getValue() instanceof Integer[]) {
						size = ((Integer[])w.getValue()).length;
					} else if (w.getValue() instanceof Date[]) {
						size = ((Date[])w.getValue()).length;
					} else {
						size = ((String[])w.getValue()).length;
					}

					String tempVirgula = "";
					part += " (";
					for (int p = 0; p < size; p++) {
						part += tempVirgula;
						part += "?";
						tempVirgula = ",";
					}
					part += ")";
				} else if ((w.getCondition().equals(Condition.ISNOTNULL)) || (w.getCondition().equals(Condition.ISNULL))) {
					// nao precisa de ? pq to passando direto no Codition...
					// logo naodeve entrar no prepareStatement
				} else {
					part += " ?";
				}

				groupOld = group;
				adicinado = true;
				sb.append(part);
			}
			if ((group != null) && (groupOld != null)) {
				sb.append(")");					
			}

		}

		whereStr = addRowNumGroupByIfNecessary(whereStr);
		sb.append(" " + whereStr);
		sb.append(getDisplayRowEnd(iDisplayStart, iDisplayLength));


		// roda o select propriamente dito
		String sqlTemp = sb.toString();
		LOG.debug(sqlTemp);


		// 		mudei aqui qualquer coisa volta
		PreparedStatement ps = TransactionDB.prepareStatement(c, sqlTemp, false);
		setParameterByWhere(viewClass, where, ps);

		ResultSet rs = null;
		try {
			rs = ps.executeQuery();
		} catch (SQLException e) {
			ps.close();
			throw new SelectException(e.getMessage() + " - " + sqlTemp, e);
		}

		// montar a lista de bean agora
		Integer size = 0;
		List<Bean> result = new ArrayList<Bean>();
		String columnName = null;
		try {

			while (rs.next()) {
				if (count == true) {
					size = rs.getInt(Bean.QTD);
				} else {
					Bean b = new Bean();
					result.add(b);


					String[] field = tempView.getFields();
					for (String key : field) {
						columnName = tempView.getFieldByKey(key);
						if (columnName.contains(".")) columnName = columnName.split("\\.")[1];
						Object obj = rs.getObject(columnName);
						Class<?> typeColumn = tempView.getTypeFieldByKey(key);
						ResultSetMetaData rsMD = rs.getMetaData();

						int i = 1;
						for (; i <= rsMD.getColumnCount(); i++) {
							String xxx = rsMD.getColumnName(i);
							if (columnName.equalsIgnoreCase(xxx) == false) continue;
							break;
						}
						int scale = rsMD.getScale(i);

						if (obj == null) {
							b.set(key, null);
						} else if (obj instanceof String) {
							b.set(key, (String)obj);

						} else if (obj instanceof oracle.sql.CLOB) {
							CLOB notes = (CLOB)obj;
							long len = notes.length();
							b.set(key, notes.getSubString(1, (int) len));
						} else if (obj instanceof Integer) {
							b.set(key, (Integer)obj);
						} else if ((scale == 0) && (obj instanceof BigInteger)) {
							b.set(key, ((BigInteger)obj).intValue());
						} else if ((scale == 0) && (obj instanceof BigDecimal)) {
							b.set(key, ((BigDecimal)obj).intValue());
						} else if (obj instanceof BigDecimal) {
							if ((typeColumn != null) && (typeColumn.equals(Integer.class))) {
								b.set(key, ((BigDecimal)obj).intValue());
							} else {
								b.set(key, ((BigDecimal)obj).doubleValue());
							}
						} else if (obj instanceof java.sql.Date) {
							b.set(key, new java.util.Date(((java.sql.Date) obj).getTime()));
						} else if (obj instanceof java.sql.Timestamp) {
							b.set(key, new java.util.Date(((java.sql.Timestamp) obj).getTime()));
						} else if (obj instanceof GregorianCalendar) {
							b.set(key, ((GregorianCalendar) obj).getTime());
						} else {
							throw new DeveloperException("Tipo de dados desconhecido. Solicitar desenvolvimento. para " + obj.getClass());
						}
					}
				}
			}
		} catch (SQLException e) {
			throw new DeveloperException(columnName + ": " + e.getMessage() );
		}

		this.lastTimeRun = TransactionDB.logSlow(userContext, d1, sql, viewClass);
		if (rs != null) rs.close();
		if (ps != null) ps.close();

		if (count == true) {
			return ( T ) size;		
		} else {
			if (result.size() == 0)
				throw new NotFoundException("Nenhum registro da view "+ viewClass.getSimpleName().toLowerCase() +" encontrado.");
		}

		return (T) result;
	}

	private String addRowNumGroupByIfNecessary(String whereStr) {
		return whereStr;
	}
	protected int countByBean(UserContext userContext, Connection c, Class<? extends Bean> bean, WhereDB where) throws NotFoundException, Exception {
		int result = selectByBean(userContext, c, bean, where, null, null, null, true, true);
		return result;
	}
	protected int countByView(UserContext userContext, Connection c, Class<? extends View> view, WhereDB where) throws NotFoundException, Exception {
		int result = selectByView(userContext, c, view, where, null, null, null, true);
		return result;
	}


	private static void setParameterByWhere(Class<?> classe, WhereDB where, PreparedStatement ps) throws Exception {
		if ((where != null) && (where.size() > 0)) {

			int position = 1;
			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				if ((w.getCondition().equals(Condition.ISNOTNULL)) || (w.getCondition().equals(Condition.ISNULL))) continue;

				if (w.getValue() == null) {
					ps.setNull(position++, Types.NULL);
				} else if (w.getValue() instanceof Integer) {
					ps.setInt(position++, (Integer)w.getValue());
				} else if (w.getValue() instanceof Double) {
					ps.setDouble(position++, (Double)w.getValue());
				} else if (w.getValue() instanceof BigDecimal) {
					ps.setBigDecimal(position++, (BigDecimal)w.getValue());
				} else if (w.getValue() instanceof Long) {
					ps.setLong(position++, (Long)w.getValue());
				} else if (w.getValue() instanceof Date) {
					String d1 = FormatUtils.formatDate( (Date) w.getValue() );
					Date d2 = FormatUtils.parseDate( d1 );
					java.sql.Date d = new java.sql.Date(d2.getTime());
					ps.setDate(position++, d);
				} else if (w.getValue() instanceof String) {
					if (w.getCondition().equals(Condition.LIKE)) {
						ps.setString(position++, "%"+StringUtils.removeAccent(((String)w.getValue()).toLowerCase())+"%");
					} else if (w.getCondition().equals(Condition.LIKEBEGIN)) {
						ps.setString(position++, StringUtils.removeAccent(((String)w.getValue()).toLowerCase())+"%");
					} else if (w.getCondition().equals(Condition.LIKEEND)) {
						ps.setString(position++, "%"+StringUtils.removeAccent(((String)w.getValue()).toLowerCase()));
					} else if (w.getCondition().equals(Condition.NOTLIKE)) {
						ps.setString(position++, "%"+StringUtils.removeAccent(((String)w.getValue()).toLowerCase())+"%");
					} else {
						ps.setString(position++, StringUtils.removeAccent(((String)w.getValue()).toLowerCase()));
					}
				} else if (w.getValue() instanceof Integer[]) {
					Integer[] values = (Integer[])w.getValue();
					for (Integer value : values) {
						ps.setInt(position++, value);
					}
				} else if (w.getValue() instanceof Date[]) {
					Date[] values = (Date[])w.getValue();
					for (Date value : values) {
						java.sql.Date d = new java.sql.Date(value.getTime());
						ps.setDate(position++, d);
					}
				} else if (w.getValue() instanceof String[]) {
					String[] values = (String[])w.getValue();
					for (String value : values) {

						if (w.getCondition().equals(Condition.LIKE)) {
							ps.setString(position++, "%"+StringUtils.removeAccent(value.toLowerCase())+"%");
						} else if (w.getCondition().equals(Condition.LIKEBEGIN)) {
							ps.setString(position++, StringUtils.removeAccent(value.toLowerCase())+"%");
						} else if (w.getCondition().equals(Condition.LIKEEND)) {
							ps.setString(position++, "%"+StringUtils.removeAccent(value.toLowerCase()));
						} else if (w.getCondition().equals(Condition.NOTLIKE)) {
							ps.setString(position++, "%"+StringUtils.removeAccent(value.toLowerCase())+"%");
						} else {
							ps.setString(position++, StringUtils.removeAccent(value.toLowerCase()));
						}

					}
				}
				LOG.debug( "Param[" + i + "] " + w.getValue());
			}
		}		
	}
	private static String getColunas(List<Field> fieldBeanList, Field[] fieldList, boolean lazy) {
		StringBuilder sb = new StringBuilder();

		//
		// colunas da tabelas primaria
		//
		String temp = "";
		for (Field field : fieldList) {
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;

			sb.append( temp );
			sb.append( "            t_." + columnDB.name() + " as t_" + columnDB.name() );
			temp = ", "+PL;
		}

		//
		// colunas das tabelas relacionadas
		//
		for (int i = 0; i < fieldBeanList.size(); i++) {
			Field field = fieldBeanList.get(i);
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;
			Class superBean = field.getType();

			for (Field superField : superBean.getDeclaredFields()) {
				ColumnDB superColumn = (ColumnDB)superField.getAnnotation(ColumnDB.class);
				if (superColumn == null) continue;
				if ((lazy == true) && (superColumn.pk() == false)) continue;

				sb.append( temp );
				sb.append( "            t"+i+ "." + superColumn.name() + " as t"+ i + "_" + superColumn.name());
				temp = ", "+PL;
			}
		}

		return sb.toString();

	}

	public static String prepareQuery(String sql, Integer iDisplayStart, Integer iDisplayLength, boolean count) throws DeveloperException {

		StringBuilder sb = new StringBuilder();
		sql = sql.trim();

		if (sql.startsWith("select")) {
			boolean limit = false;
			if ((iDisplayStart != null) && (iDisplayLength != null)) limit = true; 


			if (count == false) {
				if (limit == true) {
					sb.append( getDisplayRowBegin(LIMIT) );
					sb.append( sql.substring(6) );
				} else {
					sb.append( getDisplayRowBegin(NONE) );
					sb.append( sql );
				}
			} else {
				String selectStr = DBHelper.getSelectStr(sql);
				String columnStr = DBHelper.getColumnsStr(sql, null);
				String fromStr = DBHelper.getFromStr(sql, null);
				String whereStr = DBHelper.getWhereStr(sql, null);

				sb.append( getDisplayRowBegin(COUNT) );
				sb.append( " " + fromStr + " " + whereStr);
			}
			sb.append( getDisplayRowEnd(iDisplayStart, iDisplayLength) );
		} else {
			sb.append(sql);
		}

		return sb.toString();
	}
	private static String getDisplayRowBegin(Integer tipo) {
		StringBuilder sb = new StringBuilder();


		if (tipo.equals(LIMIT)) {
			sb.append("     select tb1.*"+PL);
			sb.append("       from ( select /*+ FIRST_ROWS(n) */"+PL);
			sb.append("                     topn.*, ROWNUM "+ Bean.RNUM +""+PL);
			sb.append("                from (select "+PL);
		} else if (tipo.equals(COUNT)) {
			sb.append("     select count(1) as " + Bean.QTD + PL);
		} else if (tipo.equals(PADRAO)) {
			sb.append("     select ROWNUM "+ Bean.RNUM +", "+PL);
		} else if (tipo.equals(NONE)) {
			sb.append("    "+PL);
		}

		return sb.toString();
	}
	private static String getDisplayRowEnd(Integer iDisplayStart, Integer iDisplayLength) {
		// aqui coloca o limite se for o caso

		StringBuilder sb = new StringBuilder();
		if ((iDisplayStart != null) && (iDisplayLength != null)) {
			sb.append( "                     ) topn"+PL);
			sb.append( "               where ROWNUM <= "+ (iDisplayStart + iDisplayLength) +" ) tb1"+PL);
			sb.append( "      where "+ Bean.RNUM +"  > " + iDisplayStart+PL);
		}
		return sb.toString();
	}
	public static String f_removeaccent(String column) {
		return " TRANSLATE(lower("+ column +") ,'áàâãäéèêëíìïóòôõöúùûüÁÀÂÃÄÉÈÊËÍÌÏÓÒÔÕÖÚÙÛÜçÇ','aaaaaeeeeiiiooooouuuuAAAAAEEEEIIIOOOOOUUUUcC') ";
	}

	public static String createDataBase(TransactionDB trans) throws SQLException, Exception {
		List<Class<?>> list = ClassUtils.getClasses(TransactionDB.PACKAGE_BEAN);
		list.add(AuditoriaBean.class);
		list.add(AuditoriaTipoBean.class);
		list.add(ComponenteBean.class);
		list.add(ComponenteEstatisticaBean.class);
		list.add(ComponenteTipoBean.class);
		list.add(FwReleaseBean.class);
		list.add(TarefaBean.class);
		list.add(GrupoAcessoBean.class);
		list.add(GrupoAcessoTarefaBean.class);

		Map<String, List<String>> commandList = GeneratorModel.genScriptCreateTable(trans, null, null, list);
		StringBuilder sb = new StringBuilder();
		for ( String key : commandList.keySet()) {
			List<String> command = commandList.get(key);

			for (String c : command) {
				sb.append( execute(trans.getConnection(), key, c) );
			}
		}

		String result = sb.toString();
		while (result.contains("  ")) result = result.replaceAll("  ", " ");
		return result;		
	}
	public static String createFunction(Connection c) throws SQLException, Exception {
		String removeAccent = functionRemoveAccent();
		String formatCpfCnpjStr = functionFormatCpfCnpjStr();
		String formatSimNaoStr = functionFormatSimNaoStr();
		String formatAtivoInativoStr = functionFormatAtivoInativoStr();
		String valorJurosAtual = functionValorJurosAtual();
		String valorMultaAtual = functionValorMultaAtual();
		String jurosComposto = functionJurosComposto();
		String jurosSimples = functionJurosSimples();
		String valorAtual = functionValorAtual();
		String extensoReal = functionExtensoReal();
		String mesExtenso = functionMesExtenso();
		String dataExtenso = functionDataExtenso();
		String diasUteisCount = functionDiasUteisCount();

		String alterColToNumber = procedureAlterColToNumber();
		String alterColToString = procedureAlterColToString();

		StringBuilder sb = new StringBuilder();
		String funcao = "função";
		String procedure = "procedure";
		sb.append( execute(c, funcao, removeAccent) );
		sb.append( execute(c, funcao, formatCpfCnpjStr) );
		sb.append( execute(c, funcao, formatSimNaoStr) );
		sb.append( execute(c, funcao, formatAtivoInativoStr) );
		sb.append( execute(c, funcao, jurosSimples) );
		sb.append( execute(c, funcao, jurosComposto) );
		sb.append( execute(c, funcao, valorJurosAtual) );
		sb.append( execute(c, funcao, valorMultaAtual) );
		sb.append( execute(c, funcao, valorAtual) );
		sb.append( execute(c, funcao, extensoReal) );
		sb.append( execute(c, funcao, mesExtenso) );
		sb.append( execute(c, funcao, dataExtenso) );
		sb.append( execute(c, funcao, diasUteisCount) );

		sb.append( execute(c, procedure, alterColToNumber) );
		sb.append( execute(c, procedure, alterColToString) );

		String result = sb.toString();
		while (result.contains("  ")) result = result.replaceAll("  ", " ");
		return result;		
	}


	public static String functionMesExtenso() {
		StringBuilder sb = new StringBuilder();
		sb.append("create or replace function f_mesextenso(p_data date) " + PL);
		sb.append(" return varchar2 is" + PL);
		sb.append(" mes number;" + PL);
		sb.append(" mesextenso varchar2(20);" + PL);
		sb.append("begin" + PL);
		sb.append(" mes := extract(month from p_data);" + PL);
		sb.append(" if (mes = 1) then mesextenso := 'Janeiro'; end if;" + PL);
		sb.append(" if (mes = 2) then mesextenso := 'Fevereiro'; end if;" + PL);
		sb.append(" if (mes = 3) then mesextenso := 'Março'; end if;" + PL);
		sb.append(" if (mes = 4) then  mesextenso := 'Abril'; end if;" + PL);
		sb.append(" if (mes = 5) then  mesextenso := 'Maio'; end if;" + PL);
		sb.append(" if (mes = 6) then  mesextenso := 'Junho'; end if;" + PL);
		sb.append(" if (mes = 7) then  mesextenso := 'Julho'; end if;" + PL);
		sb.append(" if (mes = 8) then  mesextenso := 'Agosto'; end if;" + PL);
		sb.append(" if (mes = 9) then  mesextenso := 'Setembro'; end if;" + PL);
		sb.append(" if (mes = 10) then  mesextenso := 'Outubro'; end if;" + PL);
		sb.append(" if (mes = 11) then  mesextenso := 'Novembro'; end if;" + PL);
		sb.append(" if (mes = 12) then  mesextenso := 'Dezembro'; end if;" + PL);
		sb.append( "return mesextenso;" + PL);
		sb.append("end f_mesextenso;" + PL);
		return sb.toString();
	}

	public static String functionDataExtenso() {
		StringBuilder sb = new StringBuilder();

		sb.append("create or replace function f_dataextenso(p_data date)" + PL);
		sb.append("  return varchar2 is" + PL);
		sb.append("  dia number;" + PL);
		sb.append("  mes number;" + PL);
		sb.append("  ano number;" + PL);
		sb.append("  mesextenso varchar2(20);" + PL);
		sb.append("begin" + PL);
		sb.append("  dia := extract(day from p_data);" + PL);
		sb.append("  mes := extract(month from p_data);" + PL);
		sb.append("  ano := extract(year from p_data);" + PL);
		sb.append("  if (mes = 1) then mesextenso := 'Janeiro'; end if;" + PL);
		sb.append("  if (mes = 2) then mesextenso := 'Fevereiro'; end if;" + PL);
		sb.append("  if (mes = 3) then mesextenso := 'Março'; end if;" + PL);
		sb.append("  if (mes = 4) then  mesextenso := 'Abril'; end if;" + PL);
		sb.append("  if (mes = 5) then  mesextenso := 'Maio'; end if;" + PL);
		sb.append("  if (mes = 6) then  mesextenso := 'Junho'; end if;" + PL);
		sb.append("  if (mes = 7) then  mesextenso := 'Julho'; end if;" + PL);
		sb.append("  if (mes = 8) then  mesextenso := 'Agosto'; end if;" + PL);
		sb.append("  if (mes = 9) then  mesextenso := 'Setembro'; end if;" + PL);
		sb.append("  if (mes = 10) then  mesextenso := 'Outubro'; end if;" + PL);
		sb.append("  if (mes = 11) then  mesextenso := 'Novembro'; end if;" + PL);
		sb.append("  if (mes = 12) then  mesextenso := 'Dezembro'; end if;" + PL);
		sb.append("  return dia||' de '||mesextenso||' de '||ano;" + PL);
		sb.append("end f_dataextenso;" + PL);

		return sb.toString();
	}

	public static String functionFormatSimNaoStr() {
		StringBuilder sb = new StringBuilder();
		sb.append("create or replace function f_simnaostr(ativo number)" + PL);
		sb.append(" return nvarchar2 as" + PL);
		sb.append("begin" + PL);
		sb.append(" if (ativo = 1) then " + PL);
		sb.append("  return 'sim';" + PL);
		sb.append(" end if;" + PL);
		sb.append(" if (ativo = 0) then" + PL);
		sb.append("  return 'não';" + PL);
		sb.append(" end if;" + PL);
		sb.append("return 'inválido';" + PL);
		sb.append("end f_simnaostr;" + PL);

		return sb.toString();
	}
	public static String functionFormatAtivoInativoStr() {
		StringBuilder sb = new StringBuilder();

		sb.append("create or replace function f_ativoinativostr(ativo number)            " + PL);
		sb.append("return nvarchar2 as                                                   " + PL);
		sb.append("begin                                                                 " + PL);
		sb.append("if (ativo = 1) then                                                   " + PL);
		sb.append("return 'ativo';                                                       " + PL);
		sb.append("end if;                                                               " + PL);
		sb.append("if (ativo = 0) then                                                   " + PL);
		sb.append("return 'inativo';                                                     " + PL);
		sb.append("end if;                                                               " + PL);
		sb.append("return 'inválido';                                                    " + PL);
		sb.append("end f_ativoinativostr;                                                " + PL);


		return sb.toString();
	}
	public static String functionFormatCpfCnpjStr() {
		StringBuilder sb = new StringBuilder();
		sb.append("create or replace function f_formatcpfcnpjstr(cpfcnpj varchar2)                                                                                                             " + PL);
		sb.append(" return nvarchar2 as                                                                                                                                                        " + PL);
		sb.append(" v_cpfcnpj varchar2(50);                                                                                                                                                    " + PL);
		sb.append("begin                                                                                                                                                                       " + PL);
		sb.append(" v_cpfcnpj := regexp_replace(trim(cpfcnpj), '[^0-9]', '');                                                                                                                  " + PL);
		sb.append(" if ((v_cpfcnpj is null) or (length(v_cpfcnpj) = 0)) then                                                                                                                   " + PL);
		sb.append("  return '';                                                                                                                                                                " + PL);
		sb.append(" end if;                                                                                                                                                                    " + PL);
		sb.append(" if (length(v_cpfcnpj) = 11) then                                                                                                                                           " + PL);
		sb.append("  return substr(v_cpfcnpj, 0, 3) || '.' || substr(v_cpfcnpj, 4, 3) || '.' || substr(v_cpfcnpj, 7, 3) || '-' || substr(v_cpfcnpj, 10, 2);                                    " + PL);
		sb.append(" end if;                                                                                                                                                                    " + PL);
		sb.append(" if (length(v_cpfcnpj) = 14) then                                                                                                                                           " + PL);
		sb.append("  return substr(v_cpfcnpj, 0, 2) || '.' || substr(v_cpfcnpj, 3, 3) || '.' || substr(v_cpfcnpj, 6, 3) || '/' || substr(v_cpfcnpj, 9, 4) || '-' || substr(v_cpfcnpj, 13, 2);  " + PL);
		sb.append(" end if;                                                                                                                                                                    " + PL);
		sb.append(" return 'invalido';                                                                                                                                                         " + PL);
		sb.append("end f_formatcpfcnpjstr;                                                                                                                                                     " + PL);

		return sb.toString();
	}

	public static String functionExtensoReal() {
		StringBuilder sb = new StringBuilder();
		sb.append("create or replace function f_extenso_real( valor number )                                                                         " + PL);
		sb.append(" return varchar2 IS                                                                                                               " + PL);
		sb.append(" valor_string varchar2( 256 );                                                                                                    " + PL);
		sb.append(" valor_conv VARCHAR2(25);                                                                                                         " + PL);
		sb.append(" ind NUMBER;                                                                                                                      " + PL);
		sb.append(" tres_digitos VARCHAR2(3);                                                                                                        " + PL);
		sb.append(" texto_string varchar2(256);                                                                                                      " + PL);
		sb.append("begin                                                                                                                             " + PL);
		sb.append(" valor_conv := to_char( trunc((abs(valor) * 100),0) , '0999999999999999999' );                                                    " + PL);
		sb.append(" valor_conv := substr( valor_conv , 1 , 18 ) || '0' || substr( valor_conv , 19, 2 );                                              " + PL);
		sb.append(" if to_number( valor_conv ) = 0 then                                                                                              " + PL);
		sb.append("  return( 'Zero ' );                                                                                                              " + PL);
		sb.append(" end if;                                                                                                                          " + PL);
		sb.append(" for ind in 1..7 loop                                                                                                             " + PL);
		sb.append("  tres_digitos := substr( valor_conv , (((ind-1)*3)+1) , 3 );                                                                     " + PL);
		sb.append("  texto_string := '' ;                                                                                                            " + PL);

		// Extenso para Centena
		sb.append("if substr(tres_digitos,1,1) = '2' then                                                                                            " + PL);
		sb.append("texto_string := texto_string || 'Duzentos ' ;                                                                                     " + PL);
		sb.append("    elsif substr(tres_digitos,1,1) = '3' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string || 'Trezentos ' ;                                                                              " + PL);
		sb.append("    elsif substr(tres_digitos,1,1) = '4' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string || 'Quatrocentos ' ;                                                                           " + PL);
		sb.append("    elsif substr(tres_digitos,1,1) = '5' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string || 'Quinhentos ' ;                                                                             " + PL);
		sb.append("    elsif substr(tres_digitos,1,1) = '6' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string || 'Seiscentos ' ;                                                                             " + PL);
		sb.append("    elsif substr(tres_digitos,1,1) = '7' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string || 'Setecentos ' ;                                                                             " + PL);
		sb.append("    elsif substr(tres_digitos,1,1) = '8' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string || 'Oitocentos ' ;                                                                             " + PL);
		sb.append("    elsif substr(tres_digitos,1,1) = '9' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string || 'Novecentos ' ;                                                                             " + PL);
		sb.append("    end if;                                                                                                                       " + PL);
		sb.append("    if substr(tres_digitos,1,1) = '1' then                                                                                        " + PL);
		sb.append("      if substr(tres_digitos,2,2) = '00' then                                                                                     " + PL);
		sb.append("        texto_string := texto_string || 'Cem ' ;                                                                                  " + PL);
		sb.append("      else                                                                                                                        " + PL);
		sb.append("        texto_string := texto_string || 'Cento ' ;                                                                                " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("    end if;                                                                                                                       " + PL);

		//Extenso para Dezena
		sb.append("    if substr(tres_digitos,2,1) <> '0' and texto_string is not null then                                                          " + PL);
		sb.append("      texto_string := texto_string || 'e ';                                                                                       " + PL);
		sb.append("    end if;                                                                                                                       " + PL);
		sb.append("    if substr(tres_digitos,2,1) = '2' then                                                                                        " + PL);
		sb.append("      texto_string := texto_string ||'Vinte ';                                                                                    " + PL);
		sb.append("    elsif substr(tres_digitos,2,1) = '3' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string ||'Trinta ';                                                                                   " + PL);
		sb.append("    elsif substr(tres_digitos,2,1) = '4' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string ||'Quarenta ';                                                                                 " + PL);
		sb.append("    elsif substr(tres_digitos,2,1) = '5' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string ||'Cinquenta ';                                                                                " + PL);
		sb.append("    elsif substr(tres_digitos,2,1) = '6' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string ||'Sessenta ';                                                                                 " + PL);
		sb.append("    elsif substr(tres_digitos,2,1) = '7' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string ||'Setenta ';                                                                                  " + PL);
		sb.append("    elsif substr(tres_digitos,2,1) = '8' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string ||'Oitenta ';                                                                                  " + PL);
		sb.append("    elsif substr(tres_digitos,2,1) = '9' then                                                                                     " + PL);
		sb.append("      texto_string := texto_string ||'Noventa ';                                                                                  " + PL);
		sb.append("    end if;                                                                                                                       " + PL);
		sb.append("    if substr(tres_digitos,2,1) = '1' then                                                                                        " + PL);
		sb.append("      if substr(tres_digitos,3,1) <> '0' then                                                                                     " + PL);
		sb.append("        if substr(tres_digitos,3,1) = '1' then                                                                                    " + PL);
		sb.append("          texto_string := texto_string ||'Onze ';                                                                                 " + PL);
		sb.append("        elsif substr(tres_digitos,3,1) = '2' then                                                                                 " + PL);
		sb.append("          texto_string := texto_string ||'Doze ';                                                                                 " + PL);
		sb.append("        elsif substr(tres_digitos,3,1) = '3' then                                                                                 " + PL);
		sb.append("          texto_string := texto_string ||'Treze ';                                                                                " + PL);
		sb.append("        elsif substr(tres_digitos,3,1) = '4' then                                                                                 " + PL);
		sb.append("          texto_string := texto_string ||'Catorze ';                                                                              " + PL);
		sb.append("        elsif substr(tres_digitos,3,1) = '5' then                                                                                 " + PL);
		sb.append("          texto_string := texto_string ||'Quinze ';                                                                               " + PL);
		sb.append("        elsif substr(tres_digitos,3,1) = '6' then                                                                                 " + PL);
		sb.append("          texto_string := texto_string ||'Dezesseis ';                                                                            " + PL);
		sb.append("        elsif substr(tres_digitos,3,1) = '7' then                                                                                 " + PL);
		sb.append("          texto_string := texto_string ||'Dezessete ';                                                                            " + PL);
		sb.append("        elsif substr(tres_digitos,3,1) = '8' then                                                                                 " + PL);
		sb.append("          texto_string := texto_string ||'Dezoito ';                                                                              " + PL);
		sb.append("        elsif substr(tres_digitos,3,1) = '9' then                                                                                 " + PL);
		sb.append("          texto_string := texto_string ||'Dezenove ';                                                                             " + PL);
		sb.append("        end if;                                                                                                                   " + PL);
		sb.append("      else                                                                                                                        " + PL);
		sb.append("        texto_string := texto_string ||'Dez ' ;                                                                                   " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("    else                                                                                                                          " + PL);

		// Extenso para Unidade
		sb.append("      if substr(tres_digitos,3,1) <> '0' and texto_string is not null then                                                        " + PL);
		sb.append("        texto_string := texto_string || 'e ';                                                                                     " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("      if substr(tres_digitos,3,1) = '1' then                                                                                      " + PL);
		sb.append("        texto_string := texto_string ||'Um ';                                                                                     " + PL);
		sb.append("      elsif substr(tres_digitos,3,1) = '2' then                                                                                   " + PL);
		sb.append("        texto_string := texto_string ||'Dois ';                                                                                   " + PL);
		sb.append("      elsif substr(tres_digitos,3,1) = '3' then                                                                                   " + PL);
		sb.append("        texto_string := texto_string ||'Tres ';                                                                                   " + PL);
		sb.append("      elsif substr(tres_digitos,3,1) = '4' then                                                                                   " + PL);
		sb.append("        texto_string := texto_string ||'Quatro ';                                                                                 " + PL);
		sb.append("      elsif substr(tres_digitos,3,1) = '5' then                                                                                   " + PL);
		sb.append("        texto_string := texto_string ||'Cinco ';                                                                                  " + PL);
		sb.append("      elsif substr(tres_digitos,3,1) = '6' then                                                                                   " + PL);
		sb.append("        texto_string := texto_string ||'Seis ';                                                                                   " + PL);
		sb.append("      elsif substr(tres_digitos,3,1) = '7' then                                                                                   " + PL);
		sb.append("        texto_string := texto_string ||'Sete ';                                                                                   " + PL);
		sb.append("      elsif substr(tres_digitos,3,1) = '8' then                                                                                   " + PL);
		sb.append("        texto_string := texto_string ||'Oito ';                                                                                   " + PL);
		sb.append("      elsif substr(tres_digitos,3,1) = '9' then                                                                                   " + PL);
		sb.append("        texto_string := texto_string ||'Nove ';                                                                                   " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("    end if;                                                                                                                       " + PL);
		sb.append("    if to_number( tres_digitos ) > 0 then                                                                                         " + PL);
		sb.append("      if to_number( tres_digitos ) = 1 then                                                                                       " + PL);
		sb.append("        if ind = 1 then                                                                                                           " + PL);
		sb.append("          texto_string := texto_string || 'Quatrilhão ' ;                                                                         " + PL);
		sb.append("        elsif ind = 2 then                                                                                                        " + PL);
		sb.append("          texto_string := texto_string || 'Trilhão ' ;                                                                            " + PL);
		sb.append("        elsif ind = 3 then                                                                                                        " + PL);
		sb.append("          texto_string := texto_string || 'Bilhão ' ;                                                                             " + PL);
		sb.append("        elsif ind = 4 then                                                                                                        " + PL);
		sb.append("          texto_string := texto_string || 'Milhão ' ;                                                                             " + PL);
		sb.append("        elsif ind = 5 then                                                                                                        " + PL);
		sb.append("          texto_string := texto_string || 'Mil ' ;                                                                                " + PL);
		sb.append("        end if;                                                                                                                   " + PL);
		sb.append("      else                                                                                                                        " + PL);
		sb.append("        if ind = 1 then                                                                                                           " + PL);
		sb.append("          texto_string := texto_string || 'Quatrilhões ' ;                                                                        " + PL);
		sb.append("        elsif ind = 2 then                                                                                                        " + PL);
		sb.append("          texto_string := texto_string || 'Trilhões ' ;                                                                           " + PL);
		sb.append("        elsif ind = 3 then                                                                                                        " + PL);
		sb.append("          texto_string := texto_string || 'Bilhões ' ;                                                                            " + PL);
		sb.append("        elsif ind = 4 then                                                                                                        " + PL);
		sb.append("          texto_string := texto_string || 'Milhões ' ;                                                                            " + PL);
		sb.append("        elsif ind = 5 then                                                                                                        " + PL);
		sb.append("          texto_string := texto_string || 'Mil ' ;                                                                                " + PL);
		sb.append("        end if;                                                                                                                   " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("    end if;                                                                                                                       " + PL);
		sb.append("    valor_string := valor_string || texto_string;                                                                                 " + PL);

		// Escrita da Moeda Corrente
		sb.append("    if ind = 5 then                                                                                                               " + PL);
		sb.append("      if to_number( substr( valor_conv , 16 , 3 )) > 0 and valor_string is                                                        " + PL);
		sb.append("          not null then                                                                                                           " + PL);
		sb.append("        valor_string := rtrim(valor_string) || ', ';                                                                              " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("    else                                                                                                                          " + PL);
		sb.append("      if ind < 5 and valor_string is not null then                                                                                " + PL);
		sb.append("        valor_string := rtrim(valor_string) || ', ';                                                                              " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("    end if;                                                                                                                       " + PL);
		sb.append("    if ind = 6 then                                                                                                               " + PL);
		sb.append("      if to_number( substr( valor_conv , 1 , 18 ) ) > 1 then                                                                      " + PL);
		sb.append("        valor_string := valor_string || 'Reais ';                                                                                 " + PL);
		sb.append("      elsif to_number( substr( valor_conv , 1 , 18 ) ) = 1 then                                                                   " + PL);
		sb.append("        valor_string := valor_string || 'Real ';                                                                                  " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("                                                                                                                                  " + PL);
		sb.append("      if to_number( substr( valor_conv , 20 , 2 ) ) > 0 and                                                                       " + PL);
		sb.append("           length(valor_string) > 0  then                                                                                         " + PL);
		sb.append("        valor_string := valor_string || 'e ';                                                                                     " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("    end if;                                                                                                                       " + PL);

		// Escrita para Centavos
		sb.append("    if ind = 7 then                                                                                                               " + PL);
		sb.append("      if to_number( substr( valor_conv , 20 , 2 ) ) > 1 then                                                                      " + PL);
		sb.append("        valor_string := valor_string  || 'Centavos ';                                                                             " + PL);
		sb.append("      elsif to_number( substr( valor_conv , 20 , 2 ) ) = 1 then                                                                   " + PL);
		sb.append("        valor_string := valor_string  || 'Centavo ';                                                                              " + PL);
		sb.append("      end if;                                                                                                                     " + PL);
		sb.append("    end if;                                                                                                                       " + PL);
		sb.append("  end loop;                                                                                                                       " + PL);
		sb.append("  return( rtrim(valor_string) );                                                                                                  " + PL);
		sb.append(" exception                                                                                                                        " + PL);
		sb.append("  when others then                                                                                                                " + PL);
		sb.append("    return( '*** VALOR INVALIDO ***' );                                                                                           " + PL);
		sb.append("end f_extenso_real;                                                                                                               " + PL);

		return sb.toString();
	}

	public static String functionValorAtual() {
		StringBuilder sb = new StringBuilder();

		sb.append("create or replace function f_valoratual(p_valortitulo number,                                                                   " + PL);
		sb.append("		 p_valorDescontoPontualidade number,                                                                                       " + PL); 
		sb.append("		 p_valorDesconto number,                                                                                                   " + PL);
		sb.append("		 p_percJurosFinanciamento number,                                                                                          " + PL); 
		sb.append("		 p_percMulta number,                                                                                                       " + PL);
		sb.append("		 p_valorMulta number,                                                                                                      " + PL);
		sb.append("		 p_percJuros number,                                                                                                       " + PL);
		sb.append("		 p_valorJuros number,                                                                                                      " + PL);
		sb.append("		 p_valorJurosDiario number,                                                                                                " + PL);
		sb.append("		 p_valorPagoJuros number,                                                                                                  " + PL);
		sb.append("		 p_valorPagoMulta number,                                                                                                  " + PL);
		sb.append("		 p_valorPago number,                                                                                                       " + PL);
		sb.append("		 p_carencia number,                                                                                                        " + PL);
		sb.append("		 p_dataVencimento date,                                                                                                    " + PL); 
		sb.append("		 p_dataPagamento date,                                                                                                     " + PL);
		sb.append("		 p_dataBasePagamento date,                                                                                                 " + PL);
		sb.append("		 p_situacao number,                                                                                                        " + PL);
		sb.append("		 p_tipojuros varchar                                                                                                       " + PL);
		sb.append("		) return number                                                                                                            " + PL);
		sb.append("		is                                                                                                                         " + PL);
		sb.append("		    v_valorPendente   number;                                                                                              " + PL);
		sb.append("		    v_valorAtual      number;                                                                                              " + PL);
		sb.append("		    v_diff            number;                                                                                              " + PL);
		sb.append("		    v_valorDesconto   number;                                                                                              " + PL);
		sb.append("		    v_x               number;                                                                                              " + PL);
		sb.append("		    v_y               number;                                                                                              " + PL);
		sb.append("		    v_txDescMensal    number;                                                                                              " + PL);
		sb.append("		    v_txDescDiaria    number;                                                                                              " + PL);
		sb.append("		    v_dias            number;                                                                                              " + PL);
		sb.append("		    v_vrDesc          number;                                                                                              " + PL);
		sb.append("		    v_valorMulta      number;                                                                                              " + PL);
		sb.append("		    v_valorJuros      number;                                                                                              " + PL);
		sb.append("		    v_taxaMes         number;                                                                                              " + PL);
		sb.append("		    v_taxaDiaria      number;                                                                                              " + PL);
		sb.append("		    v_carencia        number;                                                                                              " + PL);
		sb.append("		begin                                                                                                                      " + PL);
		sb.append("		   if (p_situacao = 9) then                                                                                                " + PL);
		sb.append("		      v_valorPendente := 0.00;                                                                                             " + PL);
		sb.append("		      return v_valorPendente;                                                                                              " + PL);
		sb.append("		   end if;                                                                                                                 " + PL);
		sb.append("		   v_valorPendente := p_valorTitulo - ( nvl(p_valorPago,0) - nvl(p_valorPagoJuros,0) - nvl(p_valorPagoMulta,0));           " + PL);
		sb.append("		   v_valorAtual := 0.00;                                                                                                   " + PL);
		sb.append("		   v_carencia := nvl(p_carencia, 0);                                                                                       " + PL);
		sb.append("		   if (p_dataPagamento is null) then                                                                                       " + PL);
		sb.append("		      v_diff := p_dataBasePagamento - p_dataVencimento;                                                                    " + PL);
		sb.append("		   else                                                                                                                    " + PL);
		sb.append("		      v_diff := p_dataPagamento - p_dataVencimento;                                                                        " + PL);
		sb.append("		   end if;                                                                                                                 " + PL);
		sb.append("		   -- se for adiantado                                                                                                     " + PL);
		sb.append("		   if (v_diff < 0) then                                                                                                    " + PL);
		sb.append("		      -- se for adiantado com desconto de pontualidade                                                                     " + PL);
		sb.append("		      if ((p_valorDescontoPontualidade is not null) and (p_valorDescontoPontualidade != 0.00)) then                        " + PL);
		sb.append("		         v_valorDesconto := nvl(p_valorDescontoPontualidade,0);                                                            " + PL);
		sb.append("		         if (v_valorDesconto < p_valorTitulo) then                                                                         " + PL);
		sb.append("			          v_valorAtual := p_valorTitulo - v_valorDesconto;                                                             " + PL);
		sb.append("		  	     else                                                                                                              " + PL); 
		sb.append("		  	        v_valorAtual := p_valorTitulo;                                                                                 " + PL);
		sb.append("		  	     end if;                                                                                                           " + PL);
		sb.append("		      else -- se for adiantado sem desconto por antecipacao e converter a taxa de juros do financiamento em desconto       " + PL);
		sb.append("		     	   v_x := (p_percJurosFinanciamento * 100.00); -- 600,00 considerando 6%                                           " + PL);
		sb.append("			       v_y := 100.00 + p_percJurosFinanciamento; -- => 106,00                                                          " + PL);
		sb.append("			       v_txDescMensal := v_x / v_y; --=> 5,66037 (taxa de desconto)                                                    " + PL);
		sb.append("			       v_txDescDiaria := v_txDescMensal / 30; -- => 0,188679                                                           " + PL);
		sb.append("			       v_dias := v_diff; --(numero de dias antecipados)                                                                " + PL);
		sb.append("			       if (v_diff < 0) then                                                                                            " + PL);
		sb.append("			          v_dias := v_diff * -1;                                                                                       " + PL);
		sb.append("			       end if;                                                                                                         " + PL);
		sb.append("		         v_vrDesc := (( v_txDescDiaria * v_dias * v_valorPendente ) / 100.00);                                             " + PL);
		sb.append("		         v_valorDesconto := v_vrDesc;                                                                                      " + PL);
		sb.append("		         if (v_valorDesconto < v_valorPendente) then                                                                       " + PL);
		sb.append("		   	        v_valorAtual := v_valorPendente - v_valorDesconto;                                                             " + PL);
		sb.append("			       else                                                                                                            " + PL);
		sb.append("			          v_valorAtual := v_valorPendente;                                                                             " + PL);
		sb.append("			       end if;                                                                                                         " + PL);
		sb.append("		      end if;                                                                                                              " + PL);
		sb.append("		      v_valorAtual := v_valorAtual - nvl(p_valorDesconto,0);                                                               " + PL);
		sb.append("		   elsif (v_diff = 0) then                                                                                                 " + PL);
		sb.append("		      v_valorDesconto := nvl(p_valorDescontoPontualidade,0);                                                               " + PL);
		sb.append("		      v_valorAtual := v_valorPendente - v_valorDesconto;                                                                   " + PL);
		sb.append("		      v_valorAtual := v_valorAtual - nvl(p_valorDesconto,0);                                                               " + PL);
		sb.append("		   elsif (v_diff <= v_carencia ) then                                                                                      " + PL);
		sb.append("		      v_valorDesconto := 0.00;                                                                                             " + PL);
		sb.append("		      v_valorAtual := v_valorPendente;                                                                                     " + PL);
		sb.append("		      v_valorAtual := v_valorAtual - nvl(p_valorDesconto,0);                                                               " + PL);
		sb.append("		   else                                                                                                                    " + PL);
		sb.append("		      -- variaveis pra armazenar juros e multa                                                                             " + PL);
		sb.append("		      v_valorMulta := 0.00;                                                                                                " + PL);
		sb.append("		      v_valorJuros := 0.00;                                                                                                " + PL);
		sb.append("		      -- se tiver multa vai prevalecer o valor digitado (p_valorMulta)                                                     " + PL);                                                
		sb.append("		      -- não mexa na ordem desses IF's                                                                                     " + PL);
		sb.append("		      if (nvl(p_valorMulta,0) != 0.00) then                                                                                " + PL);
		sb.append("		         v_valorMulta := p_valorMulta;                                                                                     " + PL);
		sb.append("		      else                                                                                                                 " + PL);
		sb.append("		         v_valorMulta := v_valorPendente * (p_percMulta / 100.00);                                                         " + PL);                                                                                                  
		sb.append("		      end if;                                                                                                              " + PL);
		sb.append("		      if (nvl(p_valorJuros,0) != 0.00) then                                                                                " + PL); 
		sb.append("		         v_valorJuros := p_valorJuros;                                                                                     " + PL);
		sb.append("		      else                                                                                                                 " + PL);
		sb.append("			       v_taxaDiaria := ( nvl(p_percJuros,0) / 30);                                                                     " + PL);
		sb.append("			       if (p_tipojuros = '01') then                                                                                    " + PL);
		sb.append("			          v_valorJuros := f_jurossimples(v_valorPendente, v_taxaDiaria, v_diff);                                       " + PL);                   
		sb.append("		         elsif (p_tipojuros = '02') then                                                                                   " + PL);
		sb.append("			          v_valorJuros := f_juroscompostos(v_valorPendente, v_taxaDiaria, v_diff);                                     " + PL);
		sb.append("		         elsif (p_tipojuros = '03') then                                                                                   " + PL);
		sb.append("		              v_valorJuros := p_valorJurosDiario * v_diff;                                                                 " + PL);
		sb.append("		         end if;                                                                                                           " + PL);
		sb.append("		      end if;                                                                                                              " + PL);
		sb.append("		      -- calcula finalmente valor atual                                                                                    " + PL);
		sb.append("		      v_valorAtual := v_valorPendente + v_valorJuros + v_valorMulta;                                                       " + PL);
		sb.append("		      v_valorAtual := v_valorAtual - nvl(p_valorDesconto,0);                                                               " + PL);
		sb.append("		   end if;                                                                                                                 " + PL);
		sb.append("		   --return round(v_valorAtual,2);                                                                                         " + PL);
		sb.append("		   return round(v_valorAtual,2);                                                                                           " + PL);
		sb.append(" end f_valoratual;                                                                                                              " + PL);

		return sb.toString();
	}
	public static String functionRemoveAccent() {
		StringBuilder sb = new StringBuilder();
		sb.append("create or replace function f_removeaccent(texto varchar2)    " + PL);
		sb.append(" return varchar2 is                                          " + PL);
		sb.append(" resultado varchar2(2000);                                   " + PL);
		sb.append("begin                                                        " + PL);
		sb.append(" resultado := trim(lower(texto));                            " + PL);
		sb.append(" resultado := replace(resultado ,'á','a');                   " + PL);
		sb.append(" resultado := replace(resultado ,'à','a');                   " + PL); 
		sb.append(" resultado := replace(resultado ,'ã','a');                   " + PL);
		sb.append(" resultado := replace(resultado ,'â','a');                   " + PL);
		sb.append(" resultado := replace(resultado ,'é','e');                   " + PL);
		sb.append(" resultado := replace(resultado ,'è','e');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ê','e');                   " + PL);
		sb.append(" resultado := replace(resultado ,'í','i');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ì','i');                   " + PL);
		sb.append(" resultado := replace(resultado ,'î','i');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ó','o');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ò','o');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ô','o');                   " + PL);
		sb.append(" resultado := replace(resultado ,'õ','o');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ú','u');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ù','u');                   " + PL);
		sb.append(" resultado := replace(resultado ,'û','u');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ü','u');                   " + PL);
		sb.append(" resultado := replace(resultado ,'ç','c');                   " + PL);
		sb.append(" return resultado;                                           " + PL);
		sb.append("end f_removeaccent;                                          " + PL);
		return sb.toString();
	}


	public static String functionJurosSimples() {
		StringBuilder sb = new StringBuilder();
		sb.append("create or replace function f_jurossimples(p_valor number,            " + PL);
		sb.append(" p_taxadiaria number, p_dias number) return number is                " + PL);
		sb.append(" v_valorjuros  number;                                               " + PL);
		sb.append(" begin                                                               " + PL);
		sb.append(" -- juros                                                            " + PL);
		sb.append(" v_valorjuros := p_valor * (p_taxadiaria/100.00) * p_dias;           " + PL);
		sb.append(" return v_valorjuros;                                                " + PL);
		sb.append(" end;                                                                " + PL);

		return sb.toString();
	}
	public static String functionJurosComposto() {
		StringBuilder sb = new StringBuilder();
		sb.append(" create or replace  function f_juroscompostos(p_valor number,                                  ");
		sb.append(" p_taxadiaria number, p_dias number) return number is                                          " + PL);
		sb.append(" 		  v_valorjuros  number;                                                               " + PL);
		sb.append(" begin                                                                                         " + PL);
		sb.append(" -- juros                                                                                      " + PL);
		sb.append(" v_valorjuros := (p_valor * power( (1 + (p_taxadiaria / 100.0000000)), p_dias)) - p_valor;     " + PL);
		sb.append(" return v_valorjuros;                                                                          " + PL);
		sb.append(" end;                                                                                          " + PL);

		return sb.toString();
	}

	public static String functionValorMultaAtual() {
		StringBuilder sb = new StringBuilder();
		sb.append("    create or replace function f_valormultaatual                                                                       " + PL);
		sb.append("    (p_valortitulo number,                                                                                             " + PL);
		sb.append("     p_percMulta number,                                                                                               " + PL);
		sb.append("     p_valorMulta number,                                                                                              " + PL);
		sb.append("     p_valorPagoMulta number,                                                                                          " + PL);
		sb.append("     p_valorPagoJuros number,                                                                                          " + PL);
		sb.append("     p_valorPago number,                                                                                               " + PL);
		sb.append("     p_carencia number,                                                                                                " + PL);
		sb.append("     p_dataVencimento date,                                                                                            " + PL);
		sb.append("     p_dataPagamento date,                                                                                             " + PL);
		sb.append("     p_dataBasePagamento date) return number                                                                           " + PL);
		sb.append("    is                                                                                                                 " + PL);
		sb.append("        v_valorPendente   number;                                                                                      " + PL);
		sb.append("        v_valorAtual      number;                                                                                      " + PL);
		sb.append("        v_diff            number;                                                                                      " + PL);
		sb.append("        v_valorMulta      number;                                                                                      " + PL);
		sb.append("        v_valorJuros      number;                                                                                      " + PL);
		sb.append("        v_taxaDiaria      number;                                                                                      " + PL);
		sb.append("        v_carencia        number;                                                                                      " + PL);
		sb.append("    begin                                                                                                              " + PL);
		sb.append("       v_valorPendente := p_valorTitulo - ( nvl(p_valorPago,0) - nvl(p_valorPagoJuros,0) - nvl(p_valorPagoMulta,0));   " + PL);
		sb.append("       v_valorAtual := 0.00;                                                                                           " + PL);
		sb.append("       v_carencia := nvl(p_carencia, 0);                                                                               " + PL);
		sb.append("       if (p_dataPagamento is null) then                                                                               " + PL);
		sb.append("          v_diff := p_dataBasePagamento - p_dataVencimento;                                                            " + PL);
		sb.append("       else                                                                                                            " + PL);
		sb.append("          v_diff := p_dataPagamento - p_dataVencimento;                                                                " + PL);
		sb.append("       end if;                                                                                                         " + PL);
		sb.append("       -- se for adiantado                                                                                             " + PL);
		sb.append("       if (v_diff < 0) then                                                                                            " + PL);
		sb.append("          v_valorMulta := 0.00;                                                                                        " + PL);
		sb.append("        elsif (v_diff = 0) then                                                                                        " + PL);
		sb.append("          v_valorMulta := 0.00;                                                                                        " + PL);
		sb.append("        elsif (v_diff <= v_carencia ) then                                                                             " + PL);
		sb.append("           v_valorMulta := 0.00;                                                                                       " + PL);
		sb.append("        else                                                                                                           " + PL);
		sb.append("           -- variaveis pra armazenar juros e multa                                                                    " + PL);
		sb.append("           v_valorMulta := 0.00;                                                                                       " + PL);
		sb.append("           -- se tiver multa vai prevalecer o valor digitado (p_valorMulta)                                            " + PL);
		sb.append("           -- não mexa na ordem desses IF's                                                                            " + PL);
		sb.append("           if (nvl(p_valorMulta,0) != 0.00) then                                                                       " + PL);
		sb.append("              v_valorMulta := p_valorMulta;                                                                            " + PL);
		sb.append("           else                                                                                                        " + PL);
		sb.append("              v_valorMulta := v_valorPendente * (p_percMulta / 100.00);	                                              " + PL);
		sb.append("           end if;                                                                                                     " + PL);
		sb.append("        end if;                                                                                                        " + PL);
		sb.append("        return round(v_valorMulta,2);                                                                                  " + PL);
		sb.append("    end f_valormultaatual;                                                                                             " + PL);

		return sb.toString();
	}

	public static String procedureAlterColToNumber() {
		StringBuilder sb = new StringBuilder();
		sb.append("           create or replace procedure SP_ALTERCOL_TONUMBER (NTABLE VARCHAR2, NCOLUMN VARCHAR2, NTYPE VARCHAR2) as          " + PL);
		sb.append("              NULLABLE NVARCHAR2(1);                                                                                        " + PL);
		sb.append("           begin                                                                                                            " + PL);
		sb.append("                                                                                                                            " + PL);
		sb.append("                 SELECT NULLABLE                                                                                            " + PL);
		sb.append("                   INTO NULLABLE                                                                                            " + PL);
		sb.append("                   FROM SYS.ALL_TAB_COLUMNS                                                                                 " + PL);
		sb.append("                  WHERE upper(TABLE_NAME) = upper(NTABLE)                                                                   " + PL);
		sb.append("                    AND upper(COLUMN_NAME) = upper(NCOLUMN);                                                                " + PL);
		sb.append("                                                                                                                            " + PL);
		sb.append("                 EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE||' ADD FW_ASDFG '||NTYPE ;                                        " + PL);
		sb.append("                 EXECUTE IMMEDIATE 'UPDATE '||NTABLE||' SET FW_ASDFG = TO_NUMBER('||NCOLUMN||')' ;                          " + PL);
		sb.append("                                                                                                                            " + PL);
		sb.append("                 IF NULLABLE = 'N' THEN                                                                                     " + PL);
		sb.append("                    EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE||' MODIFY ('||NCOLUMN ||' NULL)';                              " + PL);
		sb.append("                 END IF;                                                                                                    " + PL);
		sb.append("                                                                                                                            " + PL);
		sb.append("                 EXECUTE IMMEDIATE 'UPDATE '||NTABLE||' SET '||NCOLUMN||'= NULL' ;                                          " + PL);
		sb.append("                 EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE||' MODIFY ('||NCOLUMN ||' '||NTYPE||')';                          " + PL);
		sb.append("                 EXECUTE IMMEDIATE 'UPDATE '||NTABLE||' SET '||NCOLUMN|| ' = FW_ASDFG' ;                                    " + PL);
		sb.append("                                                                                                                            " + PL);
		sb.append("                 IF NULLABLE = 'N' THEN                                                                                     " + PL);
		sb.append("                    EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE||' MODIFY ('||NCOLUMN ||' NOT NULL)';                          " + PL);
		sb.append("                 END IF;                                                                                                    " + PL);
		sb.append("                                                                                                                            " + PL);
		sb.append("                 EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE|| ' DROP COLUMN FW_ASDFG';                                        " + PL);
		sb.append("           end SP_ALTERCOL_TONUMBER;                                                                                        " + PL);
		return sb.toString();
	}

	public static String procedureAlterColToString() {
		StringBuilder sb = new StringBuilder();
		sb.append("            create or replace procedure SP_ALTERCOL_TOSTRING (NTABLE VARCHAR2,NCOLUMN VARCHAR2,NTYPE VARCHAR2) as          " + PL);
		sb.append("                NULLABLE NVARCHAR2(1);                                                                                     " + PL);
		sb.append("                begin                                                                                                      " + PL);
		sb.append("                                                                                                                           " + PL);
		sb.append("                      SELECT NULLABLE                                                                                      " + PL);
		sb.append("                 INTO NULLABLE                                                                                             " + PL);
		sb.append("                 FROM SYS.ALL_TAB_COLUMNS                                                                                  " + PL);
		sb.append("                WHERE upper(TABLE_NAME) = upper(NTABLE)                                                                    " + PL);
		sb.append("                  AND upper(COLUMN_NAME) = upper(NCOLUMN);                                                                 " + PL);
		sb.append("                                                                                                                           " + PL);
		sb.append("                                                                                                                           " + PL);
		sb.append("                    EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE||' ADD FW_ASDFG '||NTYPE ;                                    " + PL);
		sb.append("                    EXECUTE IMMEDIATE 'UPDATE '||NTABLE||' SET FW_ASDFG = TO_CHAR('||NCOLUMN||')' ;                        " + PL);
		sb.append("                                                                                                                           " + PL);
		sb.append("                    IF NULLABLE = 'N' THEN                                                                                 " + PL);
		sb.append("                        EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE||' MODIFY ('||NCOLUMN ||' NULL)';                         " + PL);
		sb.append("                    END IF;                                                                                                " + PL);
		sb.append("                                                                                                                           " + PL);
		sb.append("                    EXECUTE IMMEDIATE 'UPDATE '||NTABLE||' SET '||NCOLUMN||'= NULL' ;                                      " + PL);
		sb.append("                       EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE||' MODIFY ('||NCOLUMN ||' '||NTYPE||')';                   " + PL);
		sb.append("                       EXECUTE IMMEDIATE 'UPDATE '||NTABLE||' SET '||NCOLUMN|| ' = FW_ASDFG' ;                             " + PL);
		sb.append("                                                                                                                           " + PL);
		sb.append("                       IF NULLABLE = 'N' THEN                                                                              " + PL);
		sb.append("                        EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE||' MODIFY ('||NCOLUMN ||' NOT NULL)';                     " + PL);
		sb.append("                       END IF;                                                                                             " + PL);
		sb.append("                                                                                                                           " + PL);
		sb.append("                       EXECUTE IMMEDIATE 'ALTER TABLE '||NTABLE|| ' DROP COLUMN FW_ASDFG';                                 " + PL);
		sb.append("                end SP_ALTERCOL_TOSTRING;                                                                                  " + PL);

		return sb.toString();
	}

	public static String functionValorJurosAtual() {
		StringBuilder sb = new StringBuilder();
		sb.append("  create or replace function f_valorjurosatual(p_valortitulo number,                                                       " + PL);
		sb.append(" p_percJuros number,                                                                                                       " + PL);
		sb.append(" p_valorJuros number,                                                                                                      " + PL);
		sb.append(" p_valorJurosDiario number,                                                                                                " + PL);
		sb.append(" p_valorPagoMulta number,                                                                                                  " + PL);
		sb.append(" p_valorPagoJuros number,                                                                                                  " + PL);
		sb.append(" p_valorPago number,                                                                                                       " + PL);
		sb.append(" p_carencia number,                                                                                                        " + PL);
		sb.append(" p_dataVencimento date,                                                                                                    " + PL);
		sb.append(" p_dataPagamento date,                                                                                                     " + PL);
		sb.append(" p_dataBasePagamento date,                                                                                                 " + PL);
		sb.append(" p_tipojuros varchar) return number                                                                                        " + PL);
		sb.append("is                                                                                                                         " + PL);
		sb.append("    v_valorPendente   number;                                                                                              " + PL);
		sb.append("    v_valorAtual      number;                                                                                              " + PL);
		sb.append("    v_diff            number;                                                                                              " + PL);
		sb.append("    v_valorMulta      number;                                                                                              " + PL);
		sb.append("    v_valorJuros      number;                                                                                              " + PL);
		sb.append("    v_taxaDiaria      number;                                                                                              " + PL);
		sb.append("    v_carencia        number;                                                                                              " + PL);
		sb.append("begin                                                                                                                      " + PL);
		sb.append("   v_valorPendente := p_valorTitulo - ( nvl(p_valorPago,0) - nvl(p_valorPagoJuros,0) - nvl(p_valorPagoMulta,0));           " + PL);
		sb.append("   v_valorAtual := 0.00;                                                                                                   " + PL);
		sb.append("   v_carencia := nvl(p_carencia, 0);                                                                                       " + PL);
		sb.append("   if (p_dataPagamento is null) then                                                                                       " + PL);
		sb.append("      v_diff := p_dataBasePagamento - p_dataVencimento;                                                                    " + PL);
		sb.append("   else                                                                                                                    " + PL);
		sb.append("      v_diff := p_dataPagamento - p_dataVencimento;                                                                        " + PL);
		sb.append("   end if;                                                                                                                 " + PL);
		sb.append("                                                                                                                           " + PL);
		sb.append("   -- se for adiantado                                                                                                     " + PL);
		sb.append("   if (v_diff < 0) then                                                                                                    " + PL);
		sb.append("      v_valorJuros := 0.00;                                                                                                " + PL);
		sb.append("    elsif (v_diff = 0) then                                                                                                " + PL);
		sb.append("       v_valorJuros := 0.00;                                                                                               " + PL);
		sb.append("    elsif (v_diff <= v_carencia ) then                                                                                     " + PL);
		sb.append("       v_valorJuros := 0.00;                                                                                               " + PL);
		sb.append("    else                                                                                                                   " + PL);
		sb.append("       -- variaveis pra armazenar juros                                                                                    " + PL);
		sb.append("       v_valorJuros := 0.00;                                                                                               " + PL);
		sb.append("       if (nvl(p_valorJuros,0) != 0.00) then                                                                               " + PL);
		sb.append("          v_valorJuros := p_valorJuros;                                                                                    " + PL);
		sb.append("       else                                                                                                                " + PL);
		sb.append("	        v_taxaDiaria := ( nvl(p_percJuros,0) / 30);                                                                       " + PL);
		sb.append("	                                                                                                                          " + PL);
		sb.append("          if (p_tipojuros = '01') then                                                                                     " + PL);
		sb.append("             v_valorJuros := f_jurossimples(v_valorPendente, v_taxaDiaria, v_diff);                                        " + PL);
		sb.append("          elsif (p_tipojuros = '02') then                                                                                  " + PL);
		sb.append("             v_valorJuros := f_juroscompostos(v_valorPendente, v_taxaDiaria, v_diff);                                      " + PL);
		sb.append("          elsif (p_tipojuros = '03') then                                                                                  " + PL);
		sb.append("             v_valorJuros := p_valorJurosDiario * v_diff;                                                                  " + PL);
		sb.append("          end if;                                                                                                          " + PL);
		sb.append("       end if;                                                                                                             " + PL);
		sb.append("    end if;                                                                                                                " + PL);
		sb.append("    return round(v_valorJuros,2);                                                                                          " + PL);
		sb.append("end f_valorjurosatual;                                                                                                     " + PL);

		return sb.toString();
	}

	public static String functionDiasUteisCount() {
		StringBuilder sb = new StringBuilder();

		sb.append("    create or replace function f_diasuteiscount(pDateFrom date, pDateTo date) return number                 " + PL);
		sb.append("    is                                                                                                      " + PL);
		sb.append("    pcount number := 0;                                                                                     " + PL);
		sb.append("    begin                                                                                                   " + PL);
		sb.append("                                                                                                            " + PL);
		sb.append("    	select (diasUteis.total - feriados.total) into pcount                                                  " + PL);
		sb.append("    	  from (SELECT count(1) as total                                                                       " + PL);
		sb.append("                 FROM (SELECT mes + LEVEL - 1 dia_semana,                                                   " + PL);
		sb.append("                              TO_CHAR(mes + LEVEL - 1, 'd') dia_util                                        " + PL);
		sb.append("                         FROM (SELECT pDateFrom mes FROM DUAL)                                              " + PL);
		sb.append("           CONNECT BY mes + LEVEL - 1 <= pDateTo)                                                           " + PL);
		sb.append("             WHERE dia_util NOT IN (1, 7)) diasUteis,                                                       " + PL);
		sb.append("                                                                                                            " + PL);
		sb.append("               (select count(1) as total                                                                    " + PL);
		sb.append("                  from (select to_date((to_char(fer_data, 'dd/MM') ||                                       " + PL);
		sb.append("                               '/' ||                                                                       " + PL);
		sb.append("                               to_char(pDateFrom, 'YYYY') ), 'DD/MM/RR' ) data                              " + PL);
		sb.append("                          from feriado) tb1                                                                 " + PL);
		sb.append("                 where tb1.data >= pDateFrom                                                                " + PL);
		sb.append("                   and tb1.data <= pDateTo                                                                  " + PL);
		sb.append("                   and to_char(data, 'd') NOT IN (1, 7)) feriados;                                          " + PL);
		sb.append("    	return pcount;                                                                                         " + PL);
		sb.append("    end f_diasuteiscount;                                                                                   " + PL);
		return sb.toString();
	}

	public static String execute(TransactionDB trans, String tipo, String comando) throws Exception {
		return execute(trans.getConnection(), tipo, comando);
	}
	private static String execute(Connection c, String tipo, String comando) {
		int size = 50;
		String part = (comando.length() > size ? comando.substring(0, size) + "..." : comando.substring(0, comando.length())).replaceAll("\n", " ");

		try {
			PreparedStatement ps = TransactionDB.prepareStatement(c, comando, false);
			ps.execute();
			ps.close();
			return "[OK] "+ tipo + " " + part +" criada com sucesso." + PL;
		} catch (Exception e) {
			return ("[ERRO] Problema ao criar " + tipo + " \""+ part +"\", motivo: " + e.getMessage()).replaceAll("\n", "") + PL;
		}

	}

	public String getLastTimeRun() {
		return this.lastTimeRun;
	}
}


