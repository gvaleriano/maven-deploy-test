package br.com.dotum.jedi.core.db;

import java.util.ArrayList;
import java.util.List;

public class OrderDB {

	private List<OrderDB> list;
	private String field;
	private Orientation asc_desc;

	public OrderDB() {
		 list = new ArrayList<OrderDB>();
	}
	
	private OrderDB(String field, Orientation asc_desc) {
		super();
		this.field = field;
		this.asc_desc = asc_desc;
	}
	
	private OrderDB(String field) {
		this.field = field;
		this.asc_desc = Orientation.ASC;
	}
	
	public void add(AbstractTableEnum field, Orientation asc_desc) {
		list.add(new OrderDB(field.getValue(), asc_desc));
	}
	
	public void add(AbstractTableEnum field) {
		list.add(new OrderDB(field.getValue()));
	}
	
	public void add(String field, Orientation asc_desc) {
		list.add(new OrderDB(field, asc_desc));
	}
	
	public void add(String field) {
		list.add(new OrderDB(field));
	}
	
	public List<OrderDB> getList() {
		return list;
	}
	
	public String getField() {
		return field;
	}
	public void setField(String  field) {
		this.field = field;
	}
	public Orientation getAsc_desc() {
		return asc_desc;
	}
	public void setAsc_desc(Orientation asc_desc) {
		this.asc_desc = asc_desc;
	}
	public int size() {
		return list.size();
	}

	public enum Orientation {
		ASC("asc"),
		DESC("desc");

		private String value;
		Orientation(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}
}


