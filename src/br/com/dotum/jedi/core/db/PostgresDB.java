package br.com.dotum.jedi.core.db;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.StringUtils;


public class PostgresDB {
	private static Log LOG = LogFactory.getLog(PostgresDB.class);
	private static final Integer NONE = 1;
	private static final Integer LIMIT = 2;
	private static final Integer COUNT = 3;
	private static final Integer PADRAO = 4;

	public static <T> T selectByBean(Connection c, Class<? extends Bean> bean, WhereDB where, OrderDB orders, Integer iDisplayStart, Integer iDisplayLength) throws NotFoundException, Exception {
		TableDB tableDB = (TableDB) bean.getAnnotation(TableDB.class);
		if (tableDB == null) 
			throw new DeveloperException("O Bean ("+ bean.getSimpleName() +") não possui a annotation TableDB.");

		Field[] fieldList = bean.getDeclaredFields();


		List<Field> fieldBeanList = new ArrayList<Field>();
		for (Field field : fieldList) {
			if (field.getType().getSuperclass() == null) continue;
			if (field.getType().getSuperclass().equals(Bean.class) == false) continue;

			fieldBeanList.add(field);
		}

		String temp = "";
		StringBuilder sb = new StringBuilder();

		boolean limit = false;
		if ((iDisplayStart != null) && (iDisplayLength != null)) limit = true; 

		if (limit == true) {
			sb.append( getDisplayRowBegin(LIMIT) );
			sb.append( getColunas(fieldBeanList, fieldList)  );
		} else {
			sb.append( getDisplayRowBegin(PADRAO) );
			sb.append( getColunas(fieldBeanList, fieldList)  );
		}




		//
		// from da tabela primaria
		//
		sb.append( " from " );
		sb.append( tableDB.name() + " t_");

		//
		// from das tabelas secundarias
		//
		for (int i = 0; i < fieldBeanList.size(); i++) {
			Field field = fieldBeanList.get(i);
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;

			if (fieldDB.required() == false) {
				sb.append( " left outer ");
			}

			Class superBean = field.getType();
			TableDB table = (TableDB)superBean.getAnnotation(TableDB.class);
			sb.append( " join " + table.name() + " t"+i+" on (");


			for (Field superField : superBean.getDeclaredFields()) {
				ColumnDB superColumn = (ColumnDB)superField.getAnnotation(ColumnDB.class);
				if (superColumn == null) continue;
				if (superColumn.pk() == false) continue;

				sb.append( " t"+i+ "." + superColumn.name() + " = t_." + columnDB.name());
			}
			sb.append( ") ");
		}

		//
		// AQUI COLOCAR O WHERE caso exista
		//
		boolean adicinado = false;
		if ((where != null) && (where.size() > 0)) {

			sb.append( " where " );
			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				if ((i != 0) && (adicinado == true))  {
					sb.append( " " );
					sb.append( w.getOperation() );
					sb.append( " " );
				}

				Field field = DBHelper.getFieldByAnnotation(bean, w.getField());
				boolean ehString = ((w.getValue() instanceof String) || (w.getValue() instanceof String[]));
				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				if (columnDB == null) continue;


				String part = "";
				if (ehString == true) {
					part += "removeaccent(lower(t_." + columnDB.name() + "))";
				} else {
					part += "t_." + columnDB.name();
				}
				part += " " + w.getCondition();

				if ((w.getCondition().equals(Condition.NOTIN)) || (w.getCondition().equals(Condition.IN))) {
					int size = 0;
					if (w.getValue() instanceof Integer[]) {
						size = ((Integer[])w.getValue()).length;
					} else {
						size = ((String[])w.getValue()).length;
					}

					String tempVirgula = "";
					part += " (";
					for (int p = 0; p < size; p++) {
						part += tempVirgula;
						part += "?";
						tempVirgula = ",";
					}
					part += ")";
				} else {
					part += " ?";
				}

				if (part.contains("?")) {
					adicinado = true;
					sb.append(part);
				}
			}
			// TODO se tiver algum order by tem que vir aqui

			if ((orders != null) && (orders.size() > 0)) {
				sb.append( " order by ");
				temp = "";
				for (OrderDB o : orders.getList()) {
					sb.append( temp );

					Field field = bean.getDeclaredField(o.getField().toString());
					ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
					if (columnDB == null) continue;
					sb.append( "t_." + columnDB.name() );
					sb.append( " " );
					sb.append( o.getAsc_desc() );
					temp = ", ";

				}
			}
		}


		// TODO aqui coloca o limite se for o caso
		if ((iDisplayStart != null) && (iDisplayLength != null)) {
			sb.append( ", ROWID ) topn");
			//			sb.append( "    where row_number() OVER (ORDER BY "+ columnName +") AS  <= "+ (iDisplayStart + iDisplayLength) +" ) tb1");
			sb.append( "    limit " + iDisplayStart + " offset " + iDisplayLength);
			//			sb.append( " where "+ Bean.RNUM +"  > " + iDisplayStart);
		}
		LOG.debug( sb.toString() );

		PreparedStatement ps = c.prepareStatement(sb.toString());


		if ((where != null) && (where.size() > 0)) {

			int position = 1;
			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);

				if (w.getValue() instanceof Integer) {
					ps.setInt(position++, (Integer)w.getValue());
				} else if (w.getValue() instanceof Double) {
					ps.setDouble(position++, (Double)w.getValue());
				} else if (w.getValue() instanceof Date) {
					java.sql.Date d = new java.sql.Date(((Date)w.getValue()).getTime());
					ps.setDate(position++, d);
				} else if (w.getValue() instanceof String) {
					if (w.getCondition().equals(Condition.LIKE)) {
						ps.setString(position++, "%"+StringUtils.removeAccent(((String)w.getValue()).toLowerCase())+"%");
					} else {
						ps.setString(position++, StringUtils.removeAccent(((String)w.getValue()).toLowerCase()));
					}
				} else if (w.getValue() instanceof Integer[]) {
					Integer[] values = (Integer[])w.getValue();
					for (Integer value : values) {
						ps.setInt(position++, value);
					}
				} else if (w.getValue() instanceof String[]) {
					String[] values = (String[])w.getValue();
					for (String value : values) {
						ps.setString(position++, StringUtils.removeAccent(value.toLowerCase()));
					}
				}
				LOG.debug( "Param[" + i + "] " + w.getValue() );
			}
		}



		ResultSet rs = ps.executeQuery();

		List<Bean> result = new ArrayList<Bean>();
		while (rs.next()) {
			Bean obj = bean.newInstance();
			obj.set(Bean.RNUM, rs.getInt(Bean.RNUM));

			for (int i = 0; i < fieldList.length; i++) {
				Field field = fieldList[i];
				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				if (columnDB == null) continue;

				if (field.getType().equals(Integer.class)) {
					Integer valueTemp = rs.getInt("t_" + columnDB.name());
					if (rs.wasNull() == false) {
						obj.setAttribute(field.getName(), valueTemp);
					}
				} else if (field.getType().equals(Double.class)) {
					Double valueTemp = rs.getDouble("t_" + columnDB.name());
					if (rs.wasNull() == false) {
						obj.setAttribute(field.getName(), valueTemp);
					}
				} else if (field.getType().equals(FileMemory.class)) {
					InputStream valueTemp = rs.getBinaryStream("t_" + columnDB.name());
					if (rs.wasNull() == false) {
						FileMemory fm = new FileMemory(valueTemp);
						obj.setAttribute(field.getName(), fm);
					}
				} else if (field.getType().equals(Date.class)) {
					java.sql.Date x = rs.getDate("t_" + columnDB.name());
					if (x != null) {
						obj.setAttribute(field.getName(), new Date(x.getTime()));
					}
				} else if (field.getType().equals(String.class)) {
					obj.setAttribute(field.getName(), rs.getString("t_" + columnDB.name()));
				} else if (field.getType().equals(BigDecimal.class)) {
					obj.setAttribute(field.getName(), rs.getBigDecimal("t_" + columnDB.name()));
				} else if (field.getType().equals(BigInteger.class)) {
					obj.setAttribute(field.getName(), new BigInteger(Integer.valueOf(rs.getInt("t_" + columnDB.name())).toString()));
				} else if (field.getType().getSuperclass().equals(Bean.class)) {
					Field[] fields = field.getType().getDeclaredFields();

					int positionTableId = -1;
					for (int j = 0; j < fieldBeanList.size(); j++) {
						Field fieldTemp = fieldBeanList.get(j);
						if (fieldTemp.equals(field) == false) continue;
						positionTableId = j;
						break;
					}

					for (int j = 0; j < fields.length; j++) {
						Field fieldBean = fields[j];
						// 
						ColumnDB annOuter = field.getAnnotation(ColumnDB.class);
						ColumnDB annInner = fieldBean.getAnnotation(ColumnDB.class);
						if (annInner == null) continue;

						String newName = field.getName() + "." + fieldBean.getName();
						if (fieldBean.getType().equals(Integer.class)) {
							Integer valueTemp = rs.getInt("t" + positionTableId + "_" + annInner.name());
							if (rs.wasNull() == false) {
								obj.setAttribute(newName, valueTemp);
							}
						} else if (fieldBean.getType().equals(Double.class)) {
							obj.setAttribute(newName, rs.getDouble("t" + positionTableId + "_" + annInner.name()));
						} else if (fieldBean.getType().equals(Long.class)) {
							obj.setAttribute(newName, rs.getLong("t" + positionTableId + "_" + annInner.name()));
						} else if (fieldBean.getType().equals(Date.class)) {
							java.sql.Date tempDate = rs.getDate("t" + positionTableId + "_" + annInner.name());
							if (tempDate != null) {
								obj.setAttribute(newName, new Date(tempDate.getTime()));
							}
						} else if (fieldBean.getType().equals(String.class)) {
							obj.setAttribute(newName, rs.getString("t" + positionTableId + "_" + annInner.name()));
						} else if (fieldBean.getType().equals(BigDecimal.class)) {
							obj.setAttribute(newName, rs.getBigDecimal("t" + positionTableId + "_" + annInner.name()));
						} else if (fieldBean.getType().equals(BigInteger.class)) {
							obj.setAttribute(newName, new BigInteger(Integer.valueOf(rs.getInt("t" + positionTableId + "_" + columnDB.name())).toString()));
						} else if (fieldBean.getType().equals(Blob.class)) {
							// TODO implementar o array de bytes aqui...
							// mas fazer de forma inteligente....
							// colocar opcao para o programador carregar ou nao essa budega
						} else if (fieldBean.getType().getSuperclass().equals(Bean.class)) {
							// aqui vou setar o ID da super classe
							// para ficar igual o JEDI
							Integer valueTemp = rs.getInt("t" + positionTableId + "_" + annInner.name());
							if (rs.wasNull() == false) {
								boolean xxx = obj.setAttribute(newName + ".id", valueTemp);
								if (xxx == false)
									throw new Exception("1 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + newName + ".id");
							}
						} else {
							throw new Exception("2 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
						}
					}
				} else {
					throw new Exception("Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
				}
			}
			result.add( obj );
		}
		rs.close();
		ps.close();

		if (result.size() == 0)
			throw new NotFoundException("Nenhum "+ tableDB.label().toLowerCase() +" encontrado.");

		return (T) result;		
	}

	private static Object getColunas(List<Field> fieldBeanList, Field[] fieldList) {
		StringBuilder sb = new StringBuilder();

		//
		// colunas da tabelas primaria
		//
		String temp = "";
		for (Field field : fieldList) {
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;

			sb.append( temp );
			sb.append( "t_." + columnDB.name() + " as t_" + columnDB.name() );
			temp = ", ";
		}

		//
		// colunas das tabelas relacionadas
		//
		for (int i = 0; i < fieldBeanList.size(); i++) {
			Field field = fieldBeanList.get(i);
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;
			Class superBean = field.getType();

			for (Field superField : superBean.getDeclaredFields()) {
				ColumnDB superColumn = (ColumnDB)superField.getAnnotation(ColumnDB.class);
				if (superColumn == null) continue;

				sb.append( temp );
				sb.append( " t"+i+ "." + superColumn.name() + " as t"+ i + "_" + superColumn.name());
				temp = ", ";
			}
		}

		return sb.toString();
	}

	private static String getDisplayRowBegin(Integer tipo) {
		StringBuilder sb = new StringBuilder();

		if (tipo.equals(LIMIT)) {
			sb.append("select tb1.*");
			sb.append("  from ( select /*+ FIRST_ROWS(n) */" );
			sb.append("                topn.*, 1 AS  "+ Bean.RNUM +"" );
			sb.append("           from (select " );
		} else if (tipo.equals(PADRAO)) {
			sb.append("  select 1 AS  "+ Bean.RNUM +", " );
		}

		return sb.toString();
	}

}



