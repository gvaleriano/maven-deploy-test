package br.com.dotum.jedi.core.db;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TableDB {
	
	
	public String label();
	public String name();
	public String acronym();
	public String typePk() default " ";
	public String sequence() default "";
	public String schema() default "";
	public boolean auditing() default false;

	
}



