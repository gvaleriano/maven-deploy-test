package br.com.dotum.jedi.core.db;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import br.com.dotum.jedi.core.DotumJedi;
import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.db.bean.AuditoriaBean;
import br.com.dotum.jedi.core.exceptions.DeleteException;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.InsertException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.UpdateException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;
import oracle.sql.CLOB;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TransactionDB {
	private static Log LOG = LogFactory.getLog(TransactionDB.class);

	public static final String PACK_MODEL_DEFAULT = "br.com.dotum.jedi.core.db.bean";
	public static final String PACKAGE_BEAN;
	public static final String PACKAGE_DD;

	static {
		PACKAGE_BEAN = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE) + ".model.bean";
		PACKAGE_DD = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE) + ".model.dd";
	}
	
	//	private static Connection connDefault;
	private Connection conn;
	private Date dataAtual;
	private String transId;
	private String lastTimeRun;

	private static int count = 0;
	private String typeDB;

	private PreparedStatement ps;
	private ResultSet rs;
	private UserContext userContext;
	private OracleDB oracle = new OracleDB();

	public static TransactionDB getInstance(UserContext userContext, String nameTrans) throws Exception {
		return new TransactionDB(userContext, null, nameTrans);
	}
	public static TransactionDB getInstance(UserContext userContext, Connection conn) throws Exception {
		return new TransactionDB(userContext, conn, ConnectionDB.DEFAULT_TRANS_MAP);
	}
	public static TransactionDB getInstance(UserContext userContext) throws Exception {
		return new TransactionDB(userContext, null, ConnectionDB.DEFAULT_TRANS_MAP);
	}

	private TransactionDB(UserContext userContext, Connection conn, String nameTrans) throws Exception {
		//		if (connDefault == null) 
		//			connDefault = ConnectionDB.getInstance().createConnection(ConnectionDB.DEFAULT_TRANS_MAP);

		String typeStr = ConnectionDB.getNamePropertyDB("type", nameTrans);
		typeDB = PropertiesUtils.getString(typeStr);
		if (typeDB == null) typeDB = TypeDB.ORACLE;
		//
		//
		++count;
		LOG.debug("Abriu Conexão ["+ count +"] ");

		if (this.conn != null) {
			try {
				throw new DeveloperException("Ja foi chamado o metod Start");
			} finally {
				this.conn.close();
			}
		}

		if (conn != null) {
			this.conn = conn;
		} else {
			this.conn = ConnectionDB.getInstance().createConnection(nameTrans);
		}
		this.userContext = userContext;
	}

	public void close() throws SQLException {
		LOG.debug("Fechou conexao ["+ count +"] ");
		--count;

		if (conn != null);
			conn.close();
			
		conn = null;
	}
	public void commit() throws Exception {
		validationConnActive();
		conn.commit();
	}
	public void rollback() throws Exception {
		validationConnActive();
		conn.rollback();
	}

	private String formatAuditoria(FieldDB fieldDB, Object value){
		String val = null;

		if (value == null) {
			return "";
		} else if (value instanceof Integer) {
			val = ""+value;
		} else if (value instanceof String) {
			val = "\""+value+ "\"";
		} else if (value instanceof Double) {
			val = FormatUtils.formatDouble((Double)value);
		} else if (value instanceof Date) {
			val = FormatUtils.formatDate((Date)value);
		} else {
			val = ""+value;
		}

		return fieldDB.label().toLowerCase() + " = " + val + ", ";
	}

	private String formatAuditoria(FieldDB fieldDB, Object value1, Object value2){
		String val1 = null;
		String val2 = null;

		if (value1 == null) {
			val1 = "null";
		} else if (value1 instanceof Integer) {
			val1 = ""+value1;
		} else if (value1 instanceof String) {
			val1 = "\""+value1+ "\"";
		} else if (value1 instanceof Double) {
			val1 = FormatUtils.formatDouble((Double)value1);
		} else if (value1 instanceof Date) {
			val1 = FormatUtils.formatDate((Date)value1);
		} else {
			val1 = ""+value1;
		}

		if (value2 == null) {
			val2 = "null";
		} else if (value2 instanceof Integer) {
			val2 = ""+value2;
		} else if (value2 instanceof String) {
			val2 = "\""+value2+ "\"";
		} else if (value2 instanceof Double) {
			val2 = FormatUtils.formatDouble((Double)value2);
		} else if (value2 instanceof Date) {
			val2 = FormatUtils.formatDate((Date)value2);
		} else {
			val2 = ""+value2;
		}

		if (val2.equals(val1)) {
			return "";
		} else {
			return fieldDB.label().toLowerCase() + " de " + val2 + " para " + val1 + ", ";
		}
	}


	public void merge(Bean bean) throws DeveloperException, Exception {
		try {
			List<Integer> ids = new ArrayList<Integer>();

			Field[] fieldList = bean.getClass().getDeclaredFields();
			for (Field field : fieldList) {
				Object obj = bean.getAttribute(field.getName());
				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				FieldDB fAnn = field.getAnnotation(FieldDB.class);
				if (columnDB == null) continue;
				if (columnDB.pk() == false) continue;

				if (obj == null) {
					ids.add( null );
				} else if (obj instanceof Integer) {
					ids.add( (Integer)obj );
				} else if (obj.getClass().getSuperclass().equals(Bean.class)) {
					obj = bean.getAttribute(fAnn.name());
					if (obj instanceof Integer) {
						ids.add( (Integer)obj );
					} else {
						throw new Exception("Situação nao prevista no banco de dados. Contacte o suporte");
					}
				}

			}
			selectById(bean.getClass(), ids.toArray(new Integer[ids.size()]));
			updateIfNotNull(bean);
		} catch (NotFoundException nfe) {
			insert(bean);
		}
	}
	public void insert(Bean bean) throws DeveloperException, Exception {
		validationConnActive();

		TableDB tableDB = (TableDB) bean.getClass().getAnnotation(TableDB.class);
		Field[] fieldList = bean.getClass().getDeclaredFields();

		setIdIfNull(bean, fieldList);
		if (bean.getAttribute("Ativo") == null)      bean.setAttribute("Ativo", 1);
		if (bean.getAttribute("Cadastro") == null)   bean.setAttribute("Cadastro", getDataAtual());
		if (bean.getAttribute("DataCadastro") == null) bean.setAttribute("DataCadastro", getDataAtual());
		if (bean.getAttribute("Atualizado") == null) bean.setAttribute("Atualizado", getDataAtual());
		if (bean.getAttribute("DataAtualizado") == null) bean.setAttribute("DataAtualizado", getDataAtual());
		if (bean.getAttribute("Hora") == null)   	 bean.setAttribute("Hora", getHoraAtual());
		if (bean.getAttribute("HoraCadastro") == null)   	 bean.setAttribute("HoraCadastro", getHoraAtual());
		if (bean.getAttribute("HoraAtualizado") == null)   	 bean.setAttribute("HoraAtualizado", getHoraAtual());

		StringBuilder auditoriaLOG = new StringBuilder();
		auditoriaLOG.append("Inserido " + tableDB.label().toLowerCase() + " (Tabela: "+tableDB.name()+"): ");

		// agora vou fazer o insert
		StringBuilder sb = new StringBuilder();
		sb.append( "insert into " );
		sb.append( tableDB.name() );
		sb.append( " (" );

		String temp = "";
		for (Field field : fieldList) {
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;

			sb.append( temp );
			sb.append( columnDB.name() );
			temp = ", ";
		}
		sb.append( ") values (" );
		temp = "";
		for (int i = 0; i < fieldList.length; i++) {
			ColumnDB columnDB = fieldList[i].getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;

			sb.append( temp );
			sb.append( "?" );
			temp = ", ";
		}
		sb.append( ")" );
		LOG.debug( sb.toString() );


		//
		// INSERT
		//
		StringBuilder parametrosLOG = new StringBuilder();
		PreparedStatement ps = prepareStatement(conn, sb.toString(), false);
		int x = 0;
		for (int i = 0; i < fieldList.length; i++) {
			Field field = fieldList[i];
			Object obj = bean.getAttribute(field.getName());
			// Temp
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;
			String nameColumn = columnDB.name();

			//
			//
			x++;

			if (field.getType().equals(Integer.class)) {
				Integer value = (Integer)obj;

				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setInt(x, value);
				}
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(BigInteger.class)) {
				BigInteger value = (BigInteger)obj;

				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setInt(x, value.intValue());
				}
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(Date.class)) {
				Date value = ((Date)obj);

				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					try {
						String dtStr = FormatUtils.formatDate(value);
						value = FormatUtils.parseDate(dtStr);
						ps.setDate(x, new java.sql.Date(value.getTime()));
					} catch (Exception e) {
						ps.setNull(x, Types.NULL);
						LOG.error("Problema com a data \"" + value + "\" no registro", e);
					}
				}
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(Double.class)) {
				Double value = (Double)obj;

				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setDouble(x, value);
				}
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(Long.class)) {
				Long value = (Long)obj;

				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setLong(x, value);
				}
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(FileMemory.class)) {
				FileMemory value = (FileMemory)obj;
				byte[] xxx = value.getBytes();
				//
				if ((value == null) || (xxx == null)) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setBytes(x, xxx);
				}
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(Blob.class)) {
				Blob value = (Blob)obj;

				//
				if ((value == null) || (value.getData() == null)) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setBytes(x, value.getData());
				}
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(BigDecimal.class)) {
				BigDecimal value = (BigDecimal)obj;

				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setBigDecimal(x, value);
				}
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(String.class)) {
				String value = (String)obj;

				//
				ps.setString(x, value);
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().getSuperclass().equals(Bean.class)) {
				Field[] fields = field.getType().getDeclaredFields();
				for (Field fieldBean : fields) {
					ColumnDB ann = fieldBean.getAnnotation(ColumnDB.class);
					if (ann == null) continue;
					if (ann.pk() == false) continue;
					if (fieldBean.getType().getSuperclass() == Bean.class) continue;

					String newName = field.getName() + "." + fieldBean.getName();
					if (fieldBean.getType().equals(Integer.class)) {
						Integer value = (Integer)bean.getAttribute(newName);

						//
						if (value == null) {
							ps.setNull(x, Types.NULL);
						} else {
							ps.setInt(x, value);
						}
						auditoriaLOG.append( formatAuditoria(fieldDB, value) );
						parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
					} else if (fieldBean.getType().equals(String.class)) {
						String value = (String)bean.getAttribute(newName);

						//
						ps.setString(x, value);

						auditoriaLOG.append( formatAuditoria(fieldDB, value) );
						parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
					} else {
						throw new Exception("4 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
					}
					break;
				}
			} else {
				throw new Exception("5 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
			}
		}
		LOG.debug(parametrosLOG.toString());

		try {
			ps.executeUpdate();
			auditar(tableDB, 51, "", auditoriaLOG.toString()); // inserir
		} catch (SQLException e) {
			throw new InsertException(e.getMessage() + ".\n" +
					"Comando: " + sb.toString() +".\n" +
					parametrosLOG.toString() + ".", e);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		} finally {
			ps.close();
		}
	}
	public void updateIfNotNull(Bean bean) throws Exception {
		update(bean, false);
	}
	public void update(Bean bean) throws Exception {
		update(bean, true);
	}
	private void update(Bean bean, boolean ifNull) throws Exception {
		validationConnActive();
		if (bean.getAttribute("Atualizado") == null) bean.setAttribute("Atualizado", getDataAtual());

		TableDB tableDB = (TableDB) bean.getClass().getAnnotation(TableDB.class);
		if (tableDB == null) throw new DeveloperException("O bean "+ bean.getClass().getName() +" pansando nao tem TableDB.");
		Field[] fieldList = bean.getClass().getDeclaredFields();

		// antes de dar o update eu vou fazer um select para pegar o valor antigo e colocar na auditoria
		// assim resolvemos nossa vida de uma vez por todas com essa bagaça
		StringBuilder auditoriaLOG = new StringBuilder();
		boolean auditar = false;

		List<Integer> ids = new ArrayList<Integer>();
		for (Field field : fieldList) {
			Object obj = bean.getAttribute(field.getName());
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;
			if (columnDB.pk() == false) continue;
			if ((ifNull == false) && (obj == null)) continue;
			if ((ifNull == false) && (obj.getClass().getSuperclass().equals(Bean.class)) && (((Bean)obj).getAttribute("id") == null)) continue;

			if (field.getType().equals(Integer.class)) {
				Integer value = (Integer)bean.getAttribute(field.getName());
				ids.add(value);
				auditoriaLOG.append( formatAuditoria(fieldDB, value) );
			} else if (field.getType().getSuperclass().equals(Bean.class)) {
				Field[] fields = field.getType().getDeclaredFields();
				for (Field fieldBean : fields) {
					ColumnDB ann = fieldBean.getAnnotation(ColumnDB.class);
					if (ann == null) continue;
					if (ann.pk() == false) continue;
					if (fieldBean.getType().getSuperclass().equals(Bean.class)) continue;

					String newName = field.getName() + "." + fieldBean.getName();
					if (fieldBean.getType().equals(Integer.class)) {
						Integer value = (Integer)bean.getAttribute(newName);
						ids.add(value);
						auditoriaLOG.append( formatAuditoria(fieldDB, value) );
					}
					break;
				}
			}
		}
		Bean beanTemp = this.selectById(bean.getClass(), ids.toArray(new Integer[ids.size()]));


		StringBuilder sb = new StringBuilder();
		sb.append( "update " );
		sb.append( tableDB.name() );
		sb.append( " set " );

		String temp = "";
		for (Field field : fieldList) {
			Object obj = bean.getAttribute(field.getName());
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;
			//			if ((ifNull == false) && (obj == null)) continue;
			//			if ((ifNull == false) && (obj.getClass().getSuperclass().equals(Bean.class)) && (((Bean)obj).getAttribute("id") == null)) continue;


			if (ifNull == false) {
				if (obj == null) continue;
				if (obj.getClass().getSuperclass().equals(Bean.class)) {

					String name = null;
					Bean b = (Bean)obj;
					Field[] fieldArray = b.getClass().getDeclaredFields();
					for (Field f : fieldArray) {
						ColumnDB cAnn = f.getDeclaredAnnotation(ColumnDB.class);
						if (cAnn.pk() == false) continue;
						name = f.getName();
						break;
					}
					if ((name != null) && (b.get(name) == null)) continue;

				}
			}


			sb.append( temp );
			sb.append( columnDB.name() );
			sb.append( " = ?" );
			temp = ", ";
		}

		sb.append(" where ");
		temp = "";
		for (Field field : fieldList) {
			Object obj = bean.getAttribute(field.getName());
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;
			if (columnDB.pk() == false) continue;
			//			if ((ifNull == false) && (obj == null)) continue;
			//			if ((ifNull == false) && (obj.getClass().getSuperclass().equals(Bean.class)) && (((Bean)obj).getAttribute("id") == null)) continue;
			boolean noValue = false;
			if(obj == null){
				noValue = true;
			} else if( (obj.getClass().getSuperclass().equals(Bean.class)) && (((Bean)obj).getAttribute("id") == null)){
				noValue = true;
			}
			if(noValue){
				throw new DeveloperException("Não foi informado valor no field "+fieldDB.name()+" para atualizar a tabela "+tableDB.name().toUpperCase());
			}

			sb.append( temp );
			sb.append( columnDB.name() );
			sb.append( " = ?" );
			temp = " and ";
		}

		// 
		// UPDATE 
		// 
		StringBuilder parametrosLOG = new StringBuilder();
		PreparedStatement ps = prepareStatement(conn, sb.toString(), false);
		int x = 0;
		for (int i = 0; i < fieldList.length; i++) {
			Field field = fieldList[i];
			Object obj = bean.getAttribute(field.getName());
			Object objTemp = beanTemp.getAttribute(field.getName());
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;
			String nameColumn = columnDB.name();

			if ((ifNull == false) && (obj == null)) continue;
			if (ifNull == false) {
				if (obj == null) continue;
				if (obj.getClass().getSuperclass().equals(Bean.class)) {

					String name = null;
					Bean b = (Bean)obj;
					Field[] fieldArray = b.getClass().getDeclaredFields();
					for (Field f : fieldArray) {
						ColumnDB cAnn = f.getDeclaredAnnotation(ColumnDB.class);
						if (cAnn.pk() == false) continue;
						name = f.getName();
						break;
					}
					if ((name != null) && (b.get(name) == null)) continue;

				}
			}


			x++;

			if (field.getType().equals(Integer.class)) {
				Integer value = (Integer)obj;
				Integer valueTemp = (Integer)objTemp;
				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setInt(x, value);
				}
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(BigInteger.class)) {
				BigInteger value = (BigInteger)obj;
				BigInteger valueTemp = (BigInteger)objTemp;
				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setInt(x, value.intValue());
				}
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(Date.class)) {
				Date value = ((Date)obj);
				Date valueTemp = ((Date)objTemp);
				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					value = FormatUtils.parseDate(FormatUtils.formatDate(value));
					ps.setDate(x, new java.sql.Date(value.getTime()));
				}
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(Double.class)) {
				Double value = (Double)obj;
				Double valueTemp = (Double)objTemp;
				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setDouble(x, value);
				}
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(Long.class)) {
				Long value = (Long)obj;
				Long valueTemp = (Long)objTemp;
				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setLong(x, value);
				}
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(FileMemory.class)) {
				FileMemory value = (FileMemory)obj;
				byte[] xxx = value.getBytes();

				FileMemory valueTemp = (FileMemory)objTemp;
				//
				if ((value == null) || (xxx == null)) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setBytes(x, xxx);
				}
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(Blob.class)) {
				Blob value = (Blob)obj;
				Blob valueTemp = (Blob)objTemp;
				//
				if ((value == null) || (value.getData() == null)) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setBytes(x, value.getData());
				}
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(BigDecimal.class)) {
				BigDecimal value = (BigDecimal)obj;
				BigDecimal valueTemp = (BigDecimal)objTemp;
				//
				if (value == null) {
					ps.setNull(x, Types.NULL);
				} else {
					ps.setBigDecimal(x, value);
				}
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().equals(String.class)) {
				String value = (String)obj;
				String valueTemp = (String)objTemp;
				//
				ps.setString(x, value);
				String audTemp = formatAuditoria(fieldDB, value, valueTemp);
				if (audTemp.length() > 0) auditar = true;
				auditoriaLOG.append( audTemp );

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().getSuperclass().equals(Bean.class)) {
				Field[] fields = field.getType().getDeclaredFields();
				for (Field fieldBean : fields) {
					ColumnDB ann = fieldBean.getAnnotation(ColumnDB.class);
					if (ann == null) continue;
					if (ann.pk() == false) continue;
					if (fieldBean.getType().getSuperclass().equals(Bean.class)) continue;

					String newName = field.getName() + "." + fieldBean.getName();
					if (fieldBean.getType().equals(Integer.class)) {
						Integer value = (Integer)bean.getAttribute(newName);
						Integer valueTemp = (Integer)beanTemp.getAttribute(newName);
						//
						if (value == null) {
							ps.setNull(x, Types.NULL);
						} else {
							ps.setInt(x, value);
						}
						String audTemp = formatAuditoria(fieldDB, value, valueTemp);
						if (audTemp.length() > 0) auditar = true;
						auditoriaLOG.append( audTemp );

						parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
					} else if (fieldBean.getType().equals(String.class)) {
						String value = (String)bean.getAttribute(newName);
						String valueTemp = (String)beanTemp.getAttribute(newName);
						//
						ps.setString(x, value);

						String audTemp = formatAuditoria(fieldDB, value, valueTemp);
						if (audTemp.length() > 0) auditar = true;
						auditoriaLOG.append( audTemp );

						parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
					} else {
						throw new Exception("4 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
					}
					break;
				}
			} else {
				throw new Exception("5 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
			}
		}


		for (Field field : fieldList) {
			Object obj = bean.getAttribute(field.getName());
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;
			String nameColumn = columnDB.name();

			if (columnDB.pk() == false) continue;
			if ((ifNull == false) && (obj == null)) continue;
			if ((ifNull == false) && (obj.getClass().getSuperclass().equals(Bean.class)) && (((Bean)obj).getAttribute("id") == null)) continue;

			x++;
			if (field.getType().equals(Integer.class)) {
				Integer value = (Integer)bean.getAttribute(field.getName());
				ps.setInt(x, value);

				parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
			} else if (field.getType().getSuperclass().equals(Bean.class)) {
				Field[] fields = field.getType().getDeclaredFields();
				for (Field fieldBean : fields) {
					ColumnDB ann = fieldBean.getAnnotation(ColumnDB.class);
					if (ann == null) continue;
					if (ann.pk() == false) continue;
					if (fieldBean.getType().getSuperclass().equals(Bean.class)) continue;

					String newName = field.getName() + "." + fieldBean.getName();
					if (fieldBean.getType().equals(Integer.class)) {
						Integer value = (Integer)bean.getAttribute(newName);
						ps.setInt(x, value);

						parametrosLOG.append( "[PARAM " + x + ": "+ nameColumn +"]" + value + "\n");
					} else {
						throw new Exception("8 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
					}
					break;
				}
			} else {
				throw new Exception("9 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
			}
		}
		LOG.debug( sb.toString() );

		try {
			ps.executeUpdate();
			String prefix = "Alterado " + tableDB.label().toLowerCase() + " (Tabela: "+tableDB.name()+"): ";
			if (auditar == true)
				auditar(tableDB, 52, prefix, auditoriaLOG.toString()); // alterar
		} catch (SQLException e) {
			throw new UpdateException(e.getMessage() + ".\n" +
					"Comando: " + sb.toString() +".\n" +
					parametrosLOG.toString() + ".", e);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		} finally {
			ps.close();
		}
	}

	public void auditar(TableDB tableDB, Integer audtId, String prefix, String auditoriaLOG) throws SQLException, Exception {
		if (tableDB.auditing() == true) {
			try {
				String aplicacao = PropertiesUtils.getString(PropertiesConstants.SYSTEM_TITLE);
				String tipo = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
				if (StringUtils.hasValue(aplicacao) == false) {
					LOG.error("Não foi definido a propriedade \""+ PropertiesConstants.SYSTEM_TITLE +"\" no arquivo de propriedade");
					aplicacao = "Undefined";
				}


				if (userContext == null) {
					userContext = new UserContext();
				}
				if (userContext.getUser().getCurrentTask() == null) {
					userContext.getUser().setCurrentTask(DotumJedi.class);
				}

				AuditoriaBean audBean = new AuditoriaBean();
				if(!tipo.equals("sicoob")) {
					audBean.setUnidade(userContext.getUnidade().getId());
					audBean.setPessoa(userContext.getUser().getId());
				}
				audBean.setUsuario(userContext.getUser().getId());
				audBean.setData(getDataAtual());
				audBean.setHora(getHoraAtual());
				audBean.setTransacao(getId());
				audBean.getAuditoriaTipo().setId(audtId);
				audBean.setAplicacao(aplicacao);
				audBean.setOrigem(userContext.getUser().getCurrentTask().getSimpleName());
				String log = auditoriaLOG;
				audBean.setLog(prefix + log.substring(0, log.length()-2));
				insert(audBean);

			} catch (Exception e)  {
				LOG.error("Erro ao auditar a tabela " + tableDB.label());
				LOG.error(e.getMessage());
			}
		}
	}
	public void delete(Bean bean) throws Exception {
		validationConnActive();

		TableDB tableDB = (TableDB) bean.getClass().getAnnotation(TableDB.class);
		Field[] fieldList = bean.getClass().getDeclaredFields();

		StringBuilder sb = new StringBuilder();
		sb.append( "delete " );
		sb.append( " from " );

		sb.append( tableDB.name() );
		sb.append( " where " );


		String temp = "";
		for (int i = 0; i < fieldList.length; i++) {
			Field field = fieldList[i];
			Object obj = bean.getAttribute(field.getName());
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			FieldDB fieldDB = field.getAnnotation(FieldDB.class);
			if (columnDB == null) continue;
			if (columnDB.pk() == false) continue;

			boolean noValue = false;
			if(obj == null){
				noValue = true;
			} else if( (obj.getClass().getSuperclass().equals(Bean.class)) && (((Bean)obj).getAttribute("id") == null)){
				noValue = true;
			}
			if(noValue){
				throw new DeveloperException("Não foi informado valor no field "+fieldDB.name()+" para deletar na tabela "+tableDB.name().toUpperCase());
			}

			sb.append( temp );
			sb.append( columnDB.name() );
			sb.append( " = ?" );
			temp = " and ";
		}

		LOG.debug( sb.toString() );

		PreparedStatement ps = prepareStatement(conn, sb.toString(), false);
		StringBuilder log = new StringBuilder();
		int x = 1;
		for (int i = 0; i < fieldList.length; i++) {
			Field field = fieldList[i];
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
			if (columnDB == null) continue;
			if (columnDB.pk() == false) continue;

			if (field.getType().equals(Integer.class)) {
				Integer value = (Integer)bean.getAttribute(field.getName());
				log.append(value + ", ");
				ps.setInt(x++, value);
			} else if (field.getType().getSuperclass().equals(Bean.class)) {
				Field[] fields = field.getType().getDeclaredFields();
				for (Field fieldBean : fields) {
					ColumnDB ann = fieldBean.getAnnotation(ColumnDB.class);
					if (ann == null) continue;
					if (ann.pk() == false) continue;
					if (fieldBean.getType().getSuperclass().equals(Bean.class)) continue;

					String newName = field.getName() + "." + fieldBean.getName();
					if (fieldBean.getType().equals(Integer.class)) {
						Integer value = (Integer)bean.getAttribute(newName);
						log.append(value + ", ");
						ps.setInt(x++, value);
					} else {
						throw new Exception("10 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
					}
					break;
				}
			} else {
				throw new Exception("11 - Situacao nao prevista no banco de dados. Tipo desconhecido: " + field.getType());
			}
		}
		try {
			ps.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new WarningException(e.getMessage(), e);
		} catch (SQLException e) {
			throw new DeleteException(e.getMessage(), e);
		} finally {
			ps.close();
		}
	}

	public int delete(Class<? extends Bean> beanClass, WhereDB where) throws Exception {
		validationConnActive();


		TableDB tableDB = (TableDB) beanClass.getAnnotation(TableDB.class);
		if (tableDB == null) 
			throw new DeveloperException("O Bean ("+ beanClass.getSimpleName() +") não possui a annotation TableDB.");

		Field[] fieldList = beanClass.getDeclaredFields();

		List<Field> fieldBeanList = new ArrayList<Field>();
		for (Field field : fieldList) {
			if (field.getType().getSuperclass() == null) continue;
			if (field.getType().getSuperclass().equals(Bean.class) == false) continue;

			fieldBeanList.add(field);
		}


		StringBuilder sb = new StringBuilder();
		sb.append( "delete " );
		sb.append( " from " );

		sb.append( tableDB.name() );

		//
		// AQUI COLOCAR O WHERE caso exista
		//
		boolean adicinado = false;
		if ((where != null) && (where.size() > 0)) {
			Integer group = null;
			Integer groupOld = null;
			sb.append( " where " );
			for (int  i = 0; i < where.size(); i++) {
				WhereDB w = where.getList().get(i);
				group = w.getGroup();
				if ((group != null) && (groupOld != null) && (group.equals(groupOld) == false)) {
					sb.append(")");					
				}
				if ((i != 0) && (adicinado == true))  {
					sb.append( " " );
					sb.append( w.getOperation() );
					sb.append( " " );
				}
				if ((group != null) && (group.equals(groupOld) == false)) {
					sb.append("( /*"+ group +"*/ ");					
				}

				// pegar o field do ultimo bean
				Field field = DBHelper.getFieldByAnnotation(beanClass, w.getField());
				if (field == null) {
					int nVezesDoPontos = StringUtils.occurrencesCount( w.getField(), "." );

					String part1 = null;
					String part2 = null;
					if (nVezesDoPontos == 1) {
						part1 = w.getField().split("\\.")[0]; // unidadeOrigem
						part2 = w.getField().split("\\.")[1]; // id ou razao
					}
					Field fieldTemp = DBHelper.getFieldByAnnotation(beanClass, part1 + ".Id");
					for (int k = 0; k < fieldBeanList.size(); k++) {
						Field fTemp = fieldBeanList.get(k);
						if (fieldTemp.equals(fTemp) == false) continue;
						field = DBHelper.getFieldByName(fTemp.getType(), part2);
						break;
					}



				}
				if (field == null) throw new DeveloperException("Não foi encontrado atributo \""+ w.getField() +"\" para a classe \"" + beanClass.getSimpleName() + "\"");

				boolean ehString = ((w.getValue() instanceof String) || (w.getValue() instanceof String[]));
				ColumnDB columnDB = field.getAnnotation(ColumnDB.class);
				if (columnDB == null) continue;

				String part = "";
				if (ehString == true) {
					part += OracleDB.f_removeaccent(columnDB.name());
				} else {
					part += columnDB.name();
				}
				part += " " + w.getCondition();


				if ((w.getCondition().equals(Condition.NOTIN)) || (w.getCondition().equals(Condition.IN))) {
					int size = 0;
					if (w.getValue() instanceof Integer[]) {
						size = ((Integer[])w.getValue()).length;
					} else {
						size = ((String[])w.getValue()).length;
					}

					String tempVirgula = "";
					part += " (";
					for (int p = 0; p < size; p++) {
						part += tempVirgula;
						part += "?";
						tempVirgula = ",";
					}
					part += ")";
				} else if ((w.getCondition().equals(Condition.ISNOTNULL)) || (w.getCondition().equals(Condition.ISNULL))) {
					// nao precisa de ? pq to passando direto no Codition...
					// logo naodeve entrar no prepareStatement
				} else {
					part += " ?";
				}


				groupOld = group;
				adicinado = true;
				sb.append(part);
			}
			if ((group != null) && (groupOld != null)) {
				sb.append(")");					
			}
		}
		LOG.debug( sb.toString() );

		PreparedStatement ps = prepareStatement(conn, sb.toString(), false);


		List<WhereDB> temp = where.getList();
		int position = 0;
		if ((where != null) && (temp.size() > 0)) {
			for (int i = 0; i < temp.size(); i++) {
				WhereDB wh = temp.get(i);

				Object obj = wh.getValue();

				if (obj instanceof Integer[]) {
					Integer[] values = (Integer[])obj;
					for (Integer value : values) {
						ps.setInt(++position, value);
					}
				} else if (obj instanceof Integer) {
					ps.setInt(++position, (Integer)obj);
				} else if (obj instanceof Date) {
					Date x = (Date)obj;
					ps.setDate(++position, new java.sql.Date(x.getTime()));
				} else {
					ps.setObject(++position, obj);
				}
			}
		}

		int result = 0;
		try {
			result = ps.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new WarningException(e.getMessage(), e);
		} catch (SQLException e) {
			throw new DeleteException(e.getMessage(), e);
		} finally {
			ps.close();
		}
		return result;
	}


	public <T> T selectById(Class<? extends Bean> bean, Integer ... ids) throws NotFoundException, DeveloperException, Exception {
		return selectById(bean, false, ids);
	}
	public <T> T selectById(Class<? extends Bean> bean, boolean lazy, Integer ... ids) throws NotFoundException, DeveloperException, Exception {
		TableDB tableDB = (TableDB) bean.getAnnotation(TableDB.class);
		if (tableDB == null)
			throw new DeveloperException("O Bean ("+ bean.getSimpleName() +") não possui a annotation TableDB.");

		Field[] fieldList = bean.getDeclaredFields();
		validationPk(tableDB, fieldList, ids);

		WhereDB where = new WhereDB();

		int p = 0;
		for (int i = 0; i < bean.getDeclaredFields().length; i++) {
			Field field = bean.getDeclaredFields()[i];
			ColumnDB column = (ColumnDB)field.getAnnotation(ColumnDB.class);
			FieldDB fAnn = field.getAnnotation(FieldDB.class);
			if (column == null) continue;
			if (column.pk() == false) continue;

			if (ids[p] == null)
				throw new NotFoundException("Não foi encontrado registro da tabela " + bean.getSimpleName() + "." + field.getName() + " com valor null.");

			where.add(fAnn.name(), Condition.EQUALS, ids[p]);
			p++;
		}

		List<T> t = select(bean, where, lazy);
		return t.get(0);
	}

	// SEM LAZY
	public <T> T select(Class<?> classe) throws NotFoundException, Exception {
		return select(classe, null, null, null, null, false);
	}
	public <T> T select(Class<?> classe, OrderDB orders) throws NotFoundException, Exception {
		return select(classe, null, orders, null, null, false);
	}
	public <T> T select(Class<?> classe, Integer iDisplayStart, Integer iDisplayLength) throws NotFoundException, Exception {
		return select(classe, null, null, iDisplayStart, iDisplayLength, false);
	}
	public <T> T select(Class<?> classe, WhereDB where) throws NotFoundException, Exception {
		return select(classe, where, null, null, null, false);
	}
	public <T> T select(Class<?> classe, WhereDB where, OrderDB order) throws NotFoundException, Exception {
		return select(classe, where, order, null, null, false);
	}

	// COM LAZY
	public <T> T select(Class<?> classe, boolean lazy) throws NotFoundException, Exception {
		return select(classe, null, null, null, null, lazy);
	}
	public <T> T select(Class<?> classe, OrderDB orders, boolean lazy) throws NotFoundException, Exception {
		return select(classe, null, orders, null, null, lazy);
	}
	public <T> T select(Class<?> classe, Integer iDisplayStart, Integer iDisplayLength, boolean lazy) throws NotFoundException, Exception {
		return select(classe, null, null, iDisplayStart, iDisplayLength, lazy);
	}
	public <T> T select(Class<?> classe, WhereDB where, boolean lazy) throws NotFoundException, Exception {
		return select(classe, where, null, null, null, lazy);
	}
	public <T> T select(Class<?> classe, WhereDB where, OrderDB order, boolean lazy) throws NotFoundException, Exception {
		return select(classe, where, order, null, null, lazy);
	}


	public <T> T select(Class<?> classe, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength) throws NotFoundException, Exception {
		return select(classe, where, order, iDisplayStart, iDisplayLength, false);
	}

	public <T> T select(Class<?> classe, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength, boolean lazy) throws NotFoundException, Exception {
		Connection c = getConnection();

		LOG.debug("Select starting ...");
		if (classe.getSuperclass().equals(Bean.class)) {
			if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) {
				T xxx = oracle.selectByBean(userContext, c, (Class<? extends Bean>)classe, where, order, iDisplayStart, iDisplayLength, lazy, false);
				this.lastTimeRun = oracle.getLastTimeRun();

				return xxx;
			} else if (typeDB.equalsIgnoreCase(TypeDB.MYSQL)) {
				return MySQLDB.selectByBean(userContext, c, (Class<? extends Bean>)classe, where, order, iDisplayStart, iDisplayLength, false);
			} else if (typeDB.equalsIgnoreCase(TypeDB.POSTGRES)) {
				return PostgresDB.selectByBean(c, (Class<? extends Bean>)classe, where, order, iDisplayStart, iDisplayLength);
			} else {
				throw new Exception("Tipo de banco de dados desconhecido. Solicite desenvolvimento.");
			}
		} else if (classe.getSuperclass().equals(View.class)) {
			if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) {
				T xxx = oracle.selectByView(userContext, c, (Class<? extends View>)classe, where, order, iDisplayStart, iDisplayLength, false);
				this.lastTimeRun = oracle.getLastTimeRun();

				return xxx;
			} else if (typeDB.equalsIgnoreCase(TypeDB.MYSQL)) {
				return MySQLDB.selectByView(userContext, c, (Class<? extends View>)classe, where, order, iDisplayStart, iDisplayLength, false);
			} else {
				throw new Exception("Tipo de banco de dados desconhecido. Solicite desenvolvimento.");
			}
		} else {
			throw new DeveloperException("A classe informada nao é do tipo Bean ou View.");
		}

	}

	public boolean exists(Bean bean) throws DeveloperException, Exception {
		Field[] pkField = DBHelper.getFieldPrimaryKey(bean.getClass());

		try {
			List<Integer> ids = new ArrayList<Integer>();
			for (Field pk : pkField) {
				if (pk.getType().equals(Integer.class)) {
					ids.add( (Integer)bean.getAttribute(pk.getName()) );
				} else {
					ids.add( (Integer)bean.getAttribute(pk.getName() + ".Id" ) );
				}

			}
			selectById(bean.getClass(), ids.toArray(new Integer[ids.size()] ));
			return true;
		} catch (NotFoundException nfe) {
			return false;
		}
	}


	public int count(Class<?> classe) throws Exception {
		return count(classe, null);

	}
	public int count(Class<?> classe, WhereDB where) throws Exception {
		Connection c = getConnection();

		if (classe.getSuperclass().equals(Bean.class)) {
			if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) {
				int xxx = oracle.countByBean(userContext, c, (Class<? extends Bean>)classe, where);
				this.lastTimeRun = oracle.getLastTimeRun();

				return xxx;				
			} else if (typeDB.equalsIgnoreCase(TypeDB.MYSQL)) {
				return MySQLDB.countByBean(userContext, c, (Class<? extends Bean>)classe, where);
			} else {
				throw new Exception("Tipo de banco de dados desconhecido. Solicite desenvolvimento.");
			}
		} else if (classe.getSuperclass().equals(View.class)) {
			if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) {
				int xxx = oracle.countByView(userContext, c, (Class<? extends View>)classe, where);
				this.lastTimeRun = oracle.getLastTimeRun();

				return xxx;
			} else if (typeDB.equalsIgnoreCase(TypeDB.MYSQL)) {
				return MySQLDB.countByView(userContext, c, (Class<? extends View>)classe, where);
			} else {
				throw new Exception("Tipo de banco de dados desconhecido. Solicite desenvolvimento.");
			}
		} else {
			throw new DeveloperException("A classe informada nao é do tipo Bean ou View.");
		}
	}

	//
	// HELPER
	private void validationPk(TableDB tableDB, Field[] fieldList, Integer ... ids) throws DeveloperException {
		String temp = "";
		String xxx = "";
		int count = 0;

		for (int i = 0; i < fieldList.length; i++) {
			Field field = fieldList[i];
			ColumnDB fieldDB = field.getAnnotation(ColumnDB.class);
			if (fieldDB == null) continue;
			if (fieldDB.pk() == false) continue;
			xxx += temp;
			xxx += ids[count];
			temp = "/";
			count++;	
		}

		if (ids.length != count) {
			throw new NotFoundException("Nenhum "+ tableDB.label().toLowerCase() +" encontrado [Id: "+ xxx +"]");
		}
	}
	private void validationConnActive() throws Exception {
		if (conn == null)
			throw new DeveloperException("Não foi chamado o metodo start do objeto transaction");
	}
	public static AbstractTableEnum getEnum(Class<? extends Bean> bean, String name) {
		AbstractTableEnum obj = null;

		Class<?>[] classes = bean.getDeclaredClasses();
		for (Class classe : classes) {
			if (classe.isEnum() == false) continue;

			Object[] fields = classe.getEnumConstants();
			for (Object field2 : fields) {

				if (field2.toString().equals(name) == false) continue;
				obj = (AbstractTableEnum)field2;
				break;
			}
		}
		return obj;
	}

	private void setIdIfNull(Bean bean, Field[] fieldList) throws Exception {
		TableDB tableDB = (TableDB) bean.getClass().getAnnotation(TableDB.class);
		if (tableDB.typePk().equals(" ")) return;

		for (int i = 0; i < fieldList.length; i++) {
			Field atribute = fieldList[i];
			ColumnDB columnDB = atribute.getAnnotation(ColumnDB.class);


			if (columnDB == null) continue;
			if (columnDB.pk() == false) continue;

			// nao posso tirar isso por causa dos DD
			// avisar o isac que precisa trabalhar uma outra forma das telas de 2 colunas...
			Object value = bean.getAttribute( atribute.getName() );
			if (value == null) {
				Integer newId = getNextId(bean);
				bean.setAttribute(atribute.getName(), newId);
			}
		}
	}
	private Integer getNextId(Bean bean) throws Exception {
		validationConnActive();

		Integer result = 1;
		TableDB tableDB = (TableDB) bean.getClass().getAnnotation(TableDB.class);
		if (tableDB.typePk().equals("M")) {

			result = selectMax(bean, tableDB);

		} else if (tableDB.typePk().equals("S")) {

			if (typeDB.equalsIgnoreCase(TypeDB.POSTGRES)) result = selectSequence(bean, tableDB);
			if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) result = selectSequence(bean, tableDB);

			//aqui tem q ser o max sempre por que o trem nao suporte sequence e o autoincrement do mysql nao permite colocar
			// tal recurso na segunda coluna de registro...
			// como por padrao a uni_id é a primeira coluna nao é possivel usar autoincrement.
			if (typeDB.equalsIgnoreCase(TypeDB.MYSQL)) result = MySQLDB.selectMax(userContext, conn, bean, tableDB);

		}
		return result;
	}

	public Integer selectSequence(Bean bean, TableDB tableDB) throws Exception {
		Integer result = null;

		String sequenceName = getNameSequence(tableDB);

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select "+ sequenceName +".nextval as id from dual";
			ps = prepareStatement(conn, sql, false);
			rs = ps.executeQuery();

			while (rs.next()) {
				result = rs.getInt("id");
			}
		} catch (SQLSyntaxErrorException e) {
			// pegar qual o ultimo id da sequencia no banco
			result = selectMax(bean, tableDB);

			String sql = "create sequence " + sequenceName + " start with " + (result+1) + " nocache order"; 
			// criar a sequencia com o start with e nocache;
			ps = prepareStatement(conn, sql, false);
			rs = ps.executeQuery();

		} finally {
			if (rs != null) rs.close();
			if (ps != null) ps.close();

		}

		return result;
	}

	private String getNameSequence(TableDB tableDB) throws DeveloperException {
		return getNameSequence(tableDB.acronym());
	}
	private String getNameSequence(String acronym) throws DeveloperException {
		String sequenceName = null;
		String tipo = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
		if (StringUtils.hasValue(tipo) == false) tipo = "dotumerp";

		if (tipo.equals("dotumerp")) {
			if ((userContext == null) || (userContext.getUnidade().getId() == null))
				throw new DeveloperException("O Usuario da sessao esta null para pegar a proxima sequence");

			Integer unidadeId = userContext.getUnidade().getId();
			sequenceName = "s" + acronym + "_id_up_" + Math.abs(unidadeId);
		} else if (tipo.equals("sicoob")) {
			sequenceName = "SEQ_" + acronym;
		} else {
			sequenceName = "S_" + acronym;
		}

		return sequenceName;
	}

	private Integer selectMax(Bean bean, TableDB tableDB) throws SQLException, Exception {
		Integer result = 0;

		Field[] fieldList = bean.getClass().getDeclaredFields();

		StringBuilder sb = new StringBuilder();
		sb.append( "select " );

		int count = 0;
		for (Field field : fieldList) {
			ColumnDB columnDB = field.getAnnotation(ColumnDB.class);

			if (columnDB == null) continue;
			if (columnDB.pk() == false) continue;
			if (field.getType().getSuperclass().equals(Bean.class)) continue;

			sb.append( "max(" );
			sb.append( columnDB.name() );
			sb.append( ") as id" );
			count++;
		}
		if (count != 1)
			throw new DeveloperException("Situação não prevista no banco de dados. Pk da tabela ("+ bean.getClass().getSimpleName() +") esta errada.");

		sb.append( " from " );
		sb.append( tableDB.name() );

		LOG.debug( sb.toString() );

		PreparedStatement ps = prepareStatement(conn, sb.toString(), false);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			result = rs.getInt("id");
		}
		rs.close();
		ps.close();
		result++;
		if (result <= 0) result = 1;
		return result;
	}

	public Connection getConnection() throws Exception {
		Connection temp = null;
		if (conn != null) {
			temp = conn;
		} else {
			//			temp = connDefault;
		}

		return temp;
	}

	public Date getDataAtual() {
		//		String timeZone = "-04:00";
		//		if ((userContext != null) && (StringUtils.hasValue(userContext.getUnidade().getTimeZone()))) 
		//			timeZone = userContext.getUnidade().getTimeZone();
		//
		//		TimeZone.setDefault(TimeZone.getTimeZone("GMT"+timeZone));
		if (dataAtual == null) dataAtual = new Date();
		return dataAtual;
	}

	public String getHoraAtual() {
		String timeZone = "-04:00";

		if ((userContext != null) && (StringUtils.hasValue(userContext.getUnidade().getTimeZone()))) 
			timeZone = userContext.getUnidade().getTimeZone();

		Date d1 = getDataAtual();
		SimpleDateFormat sdfmad = new SimpleDateFormat("HH:mm:ss");
		sdfmad.setTimeZone(TimeZone.getTimeZone("GMT"+timeZone));
		return sdfmad.format(d1);
	}


	public String getId() {
		if (transId == null) transId = ""+getDataAtual().getTime();
		return transId;
	}

	public String callProcedure(String name, Object ... values) throws Exception {
		StringBuilder sb = new StringBuilder();
		String temp = "";
		for (int i = 0; i < values.length; i++) {
			sb.append(temp);
			sb.append("?");
			temp = ", ";
		}


		String proc = "{call "+name+"("+sb.toString()+")}";
		LOG.debug("Chamada: " + proc);

		CallableStatement cs = conn.prepareCall(proc);
		cs.registerOutParameter(1, Types.INTEGER);
		cs.registerOutParameter(2, Types.VARCHAR);

		for (int i = 0; i < values.length; i++) {
			cs.setObject(i+1, values[i]);	
		}
		cs.execute();
		Integer xxx = cs.getInt(1);
		String yyy = cs.getString(2);

		LOG.debug("Resultado: ["+ xxx +"]" + yyy);
		if (xxx.equals(1)) {
			return yyy;
		} else {
			throw new WarningException(yyy);
		}
	}

	public int execUpdate(String sql) throws SQLException, Exception {
		return execUpdate(sql, null);
	}
	public int execUpdate(String sql, Object[] params) throws SQLException, Exception {
		Connection c = getConnection();
		PreparedStatement ps = prepareStatement(c, sql, false);
		setParam(ps, params);
		try {
			int result = ps.executeUpdate();
			return result;
		} catch (SQLException e) {
			throw new SQLException(e.getMessage() + " - " + sql, e);
		} finally {
			ps.close();
		}
	}
	private void setParam(PreparedStatement ps, Object[] params) throws SQLException {
		if (params != null) {

			int position = 1;
			for (int i = 0; i < params.length; i++) {

				Object obj = params[i];
				if (params[i] instanceof java.util.Date) {
					obj = new java.sql.Date(((java.util.Date)params[i]).getTime());
					ps.setObject(position++, obj);
					//				} else if (params[i] instanceof java.util.Date[]) {
					//					java.util.Date[] values = (java.util.Date[])params[i];
					//					for (java.util.Date value : values) {
					//						obj = new java.sql.Date(((java.util.Date)value).getTime());
					//						ps.setObject(position++, obj);
					//					}
				} else if (params[i] instanceof Integer[]) {
					Integer[] values = (Integer[])params[i];
					for (Integer value : values) {
						ps.setInt(position++, value);
					}
				} else if (params[i] instanceof Object[]) {
					Object[] values = (Object[])params[i];
					for (Object value : values) {
						ps.setObject(position++, value);
					}
				} else {
					ps.setObject(position++, obj);
				}
			}
		}
	}

	@Deprecated
	public ResultSet execQuery(String sql) throws SQLException, Exception {
		return execQuery(sql, null, null, null, false, false);
	}
	@Deprecated
	public ResultSet execQuery(String sql, boolean fetchDirection) throws SQLException, Exception {
		return execQuery(sql, null, null, null, false, fetchDirection);
	}
	@Deprecated
	public ResultSet execQuery(String sql, Object[] params) throws SQLException, Exception {
		return execQuery(sql, params, null, null, false, false);
	}
	@Deprecated
	private ResultSet execQuery(String sql, Object[] params, Integer iDisplayStart, Integer iDisplayLength, boolean count, boolean fetchDirection) throws SQLException, Exception {
		if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) {
			sql = OracleDB.prepareQuery(sql, iDisplayStart, iDisplayLength, count);
		} else if (typeDB.equalsIgnoreCase(TypeDB.MYSQL)) {
			sql = MySQLDB.prepareQuery(sql, iDisplayStart, iDisplayLength, count);
		} else if (typeDB.equalsIgnoreCase(TypeDB.POSTGRES)) {
			sql = OracleDB.prepareQuery(sql, iDisplayStart, iDisplayLength, count);
		} else {
			throw new Exception("Tipo de banco de dados desconhecido. Solicite desenvolvimento.");
		}

		Connection c = getConnection();
		ps = prepareStatement(c, sql, fetchDirection);
		setParam(ps, params);

		try {
			rs = ps.executeQuery();
		} catch (SQLException e) {
			throw new SQLException(e.getMessage() + " - " + sql, e);
		}

		return rs;
	}

	public <T> T select(Class<? extends Bean> type, ORM orm, String sql) throws SQLException, Exception {
		return execQuery(type, orm, sql, null, null, null, null, false);
	}
	public <T> T select(Class<? extends Bean> type, ORM orm, String sql, WhereDB where) throws SQLException, Exception {
		return execQuery(type, orm, sql, where, null, null, null, false);
	}
	public <T> T select(Class<? extends Bean> type, ORM orm, String sql, WhereDB where, OrderDB order) throws SQLException, Exception {
		return execQuery(type, orm, sql, where, order, null, null, false);
	}
	public <T> T select(Class<? extends Bean> type, ORM orm, String sql, WhereDB where, Integer iDisplayStart, Integer iDisplayLength) throws SQLException, Exception {
		return execQuery(type, orm, sql, where, null, iDisplayStart, iDisplayLength, false);
	}
	public <T> T select(Class<? extends Bean> type, ORM orm, String sql, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength) throws SQLException, Exception {
		return execQuery(type, orm, sql, where, order, iDisplayStart, iDisplayLength, false);
	}
	public <T> T execQuery(Class<? extends Bean> type, ORM orm, String sql) throws SQLException, Exception {
		return execQuery(type, orm, sql, null, null, null, null, false);
	}
	public <T> T execQuery(Class<? extends Bean> type, ORM orm, String sql, WhereDB where) throws SQLException, Exception {
		return execQuery(type, orm, sql, where, null, null, null, false);
	}
	public <T> T execQuery(Class<? extends Bean> type, ORM orm, String sql, WhereDB where, OrderDB order) throws SQLException, Exception {
		return execQuery(type, orm, sql, where, order, null, null, false);
	}
	public <T> T execQuery(Class<? extends Bean> type, ORM orm, String sql, WhereDB where, Integer iDisplayStart, Integer iDisplayLength) throws SQLException, Exception {
		return execQuery(type, orm, sql, where, null, iDisplayStart, iDisplayLength, false);
	}
	public <T> T execQuery(Class<? extends Bean> type, ORM orm, String sql, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength) throws SQLException, Exception {
		return execQuery(type, orm, sql, where, order, iDisplayStart, iDisplayLength, false);
	}
	private <T> T execQuery(Class<? extends Bean> type, ORM orm, String sql, WhereDB where, OrderDB order, Integer iDisplayStart, Integer iDisplayLength, boolean count) throws SQLException, Exception {
		if (sql.contains(Bean.RNUM))
			throw new DeveloperException("Não é permitido inserir o apelido \"" + Bean.RNUM + "\" para as colunas no select.");


		List<String> fieldsToReplace = SQLUtils.getFields(0, sql, false);
		List<String> fields = SQLUtils.getFields(1, sql, true);
		List<Object> params = new ArrayList<Object>();

		for (int i = 0; i < fields.size(); i++) {
			String str = fields.get(i);
			for (WhereDB tempWhere : where.getList()) {
				if (tempWhere.getField().equalsIgnoreCase(str) == false) continue;

				Object value = null;
				if (tempWhere.getValue() instanceof String) {
					String condicao = tempWhere.getCondition().toString();
					value = "%" + tempWhere.getValue() + "%";
					//
					if (tempWhere.getCondition().equals(Condition.EQUALS)) {
						value = StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase());
					} else if (tempWhere.getCondition().equals(Condition.LIKE)) {
						value = "%"+StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase())+"%";
					} else if (tempWhere.getCondition().equals(Condition.LIKEBEGIN)) {
						value = StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase())+"%";
					} else if (tempWhere.getCondition().equals(Condition.LIKEEND)) {
						value = "%"+StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase());
					} else {
						value = StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase());
					}

				} else {
					value = tempWhere.getValue();
				}

				params.add(value);
				break;
			}
		}

		for (int i = 0; i < fields.size(); i++) {
			String str = "$P{" + fields.get(i) + "}";
			Object obj = params.get(i);

			if (obj instanceof Integer[]) {
				Integer[] xxx = ((Integer[])obj);

				String temp = "";
				StringBuilder xxasda = new StringBuilder();
				for (int x = 0; x < xxx.length; x++) {
					xxasda.append(temp);
					xxasda.append("?");
					temp = ",";
				}
				sql = sql.replaceAll(prepare(str), "("+xxasda.toString()+")");
			} else {
				sql = sql.replaceAll(prepare(str), "?");
			}
		}


		if ((where != null) && where.getList().size() != fields.size()) {
			String partIni = "select tb_fw.* from (";
			String partFim = " ) tb_fw ";
			sql = partIni + sql + partFim;

			
			StringBuilder sb = new StringBuilder();
			boolean adicinado = false;
			Integer group = null;
			Integer groupOld = null;
			for (int j = 0; j < where.getList().size(); j++) {
				WhereDB tempWhere = where.getList().get(j);

				boolean jatem = false;
				for (int i = 0; i < fields.size(); i++) {
					String str = fields.get(i);
					if (tempWhere.getField().equalsIgnoreCase(str) == false) continue;

					jatem = true;
					break;
				}

				if (jatem == false) {

					group = tempWhere.getGroup();
					if ((group != null) && (groupOld != null) && (group.equals(groupOld) == false)) {
						sb.append(")");					
					}
					if ((j != 0) && (adicinado == true))  {
						sb.append( " " );
						sb.append( tempWhere.getOperation() );
						sb.append( " " );
					}
					if ((group != null) && (group.equals(groupOld) == false)) {
						sb.append("( /*"+ group +"*/ ");					
					}
					//					if (primeiro == false) sb.append( tempWhere.getOperation() + " ");

					String column = "";
					for (ORM oT : orm.getList()) {
						if (oT.getNameProperty().equalsIgnoreCase(tempWhere.getField()) == false) continue;

						column = oT.getNameColumn();
						break;
					}


					Object value = null;
					if (tempWhere.getValue() instanceof String) {
						String condicao = tempWhere.getCondition().toString();
						value = "%" + tempWhere.getValue() + "%";
						//
						if (tempWhere.getCondition().equals(Condition.EQUALS)) {
							value = StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase());
						} else if (tempWhere.getCondition().equals(Condition.LIKE)) {
							value = "%"+StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase())+"%";
						} else if (tempWhere.getCondition().equals(Condition.LIKEBEGIN)) {
//							condicao = condicao.substring(1);
							value = StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase())+"%";
						} else if (tempWhere.getCondition().equals(Condition.LIKEEND)) {
//							condicao = condicao.substring(0, condicao.length()-1);
							value = "%"+StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase());
						} else {
							value = StringUtils.removeAccent(((String)tempWhere.getValue()).toLowerCase());
						}

						sb.append(OracleDB.f_removeaccent(column) + condicao);
					} else {
						value = tempWhere.getValue();
						sb.append("" +column  + " " + tempWhere.getCondition());
					}
					
					if (StringUtils.hasValue(column) == false ) {
						String sqlTemp = SQLUtils.formatPartStringToShow(sql.substring(partIni.length()));
						throw new DeveloperException("Não existe a propriedade \""+ tempWhere.getField() +"\" no ORM. Verifique o select \""+ sqlTemp +"\".");
					}
					
					/////////////////////////
					String part = "";
					if ((tempWhere.getCondition().equals(Condition.NOTIN)) || (tempWhere.getCondition().equals(Condition.IN))) {
						int size = 0;
						if (tempWhere.getValue() instanceof Integer[]) {
							size = ((Integer[])tempWhere.getValue()).length;
						} else if (tempWhere.getValue() instanceof Integer) {
							size = 1;
						} else if (tempWhere.getValue() instanceof String) {
							size = 1;
						} else {
							size = ((String[])tempWhere.getValue()).length;
						}

						String tempVirgula = "";
						part += " (";
						for (int p = 0; p < size; p++) {
							part += tempVirgula;
							part += "?";
							tempVirgula = ",";
						}
						part += ")";
					} else if ((tempWhere.getCondition().equals(Condition.ISNOTNULL)) || (tempWhere.getCondition().equals(Condition.ISNULL))) {
						// nao precisa de ? pq to passando direto no Codition...
						// logo naodeve entrar no prepareStatement
					} else {
						part += " ?";
					}
					sb.append( part );
					/////////////////////////

					params.add(value);

					groupOld = group;
					adicinado = true;
				}
			}
			if ((group != null) && (groupOld != null)) {
				sb.append(")");					
			}

			sql += " where " + sb.toString();

		}


		if (order != null) {
			sql += " order by ";
			List<OrderDB> orderList = order.getList();
			for (OrderDB o : orderList) {

				String temp = "";
				for (int i = 0; i <= orm.getList().size(); i++) {
					ORM item = orm.getList().get(i);

					if (item.getNameProperty().equals(o.getField()) == false) continue;
					sql += temp;
					sql += item.getNameColumn() + " " + o.getAsc_desc();
					temp = ", ";
					break;
				}
			}
		}

		return execQuery(type, orm, sql, params.toArray(new Object[params.size()]), iDisplayStart, iDisplayLength, count);
	}
	public <T> T execQuery(Class<? extends Bean> type, ORM orms, String sql, Object[] params, Integer iDisplayStart, Integer iDisplayLength, boolean count) throws SQLException, Exception {
		Date d1 = new Date();
		if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) {
			sql = OracleDB.prepareQuery(sql, iDisplayStart, iDisplayLength, count);
		} else if (typeDB.equalsIgnoreCase(TypeDB.MYSQL)) {
			sql = MySQLDB.prepareQuery(sql, iDisplayStart, iDisplayLength, count);
		} else {
			throw new Exception("Tipo de banco de dados desconhecido ("+ typeDB +"). Solicite desenvolvimento.");
		}

		Connection c = getConnection();
		PreparedStatement ps = prepareStatement(c, sql, false);
		setParam(ps, params);

		ResultSet rs = null;
		try {
			rs = ps.executeQuery();
			List<Bean> result = new ArrayList<Bean>();
			while (rs.next()) {
				Bean b = type.newInstance();
				result.add(b);

				List<ORM> ormList = orms.getList();

				for (ORM orm : ormList) {
					String nameProperty = orm.getNameProperty();
					String nameColumn = orm.getNameColumn();
					if (nameColumn.contains(".")) nameColumn = nameColumn.split("\\.")[1];
					Object obj = null;
					try {
						obj = rs.getObject(nameColumn);
					} catch (SQLException e) {
						throw new SQLException(e.getMessage() + ". Coluna: " + nameColumn, e);
					}
					Class<?> typeProperty = orm.getTypeProperty();
					ResultSetMetaData rsMD = rs.getMetaData();

					int i = 1;
					for (; i <= rsMD.getColumnCount(); i++) {
						String temp = rsMD.getColumnName(i);
						if (nameColumn.equalsIgnoreCase(temp) == false) continue;
						break;
					}
					int scale = rsMD.getScale(i);

					if (obj == null) {
						b.set(nameProperty, null);
					} else if (obj instanceof String) {
						b.set(nameProperty, (String)obj);
					}
					else if (obj instanceof oracle.sql.CLOB) {
						CLOB notes = (CLOB)obj;
						long len = notes.length();
						b.set(nameProperty, notes.getSubString(1, (int) len));
					}
					else if (obj instanceof Integer) {
						b.set(nameProperty, (Integer)obj);
					} else if (obj instanceof Double) {
						b.set(nameProperty, (Double)obj);
						//					} else if ((scale == 0) && (obj instanceof BigInteger)) {
						//						b.set(nameProperty, ((BigInteger)obj).intValue());
						//					} else if ((scale == 0) && (obj instanceof BigDecimal)) {
						//						b.set(nameProperty, ((BigDecimal)obj).intValue());
					} else if (obj instanceof BigDecimal) {
						if ((typeProperty != null) && (typeProperty.equals(Integer.class))) {
							b.set(nameProperty, ((BigDecimal)obj).intValue());
						} else if ((typeProperty != null) && (typeProperty.equals(Double.class))) {
							b.set(nameProperty, ((BigDecimal)obj).doubleValue());
						} else {
							b.set(nameProperty, (BigDecimal)obj);
						}
					} else if (obj instanceof java.sql.Date) {
						b.set(nameProperty, new java.util.Date(((java.sql.Date) obj).getTime()));
					} else if (obj instanceof java.sql.Timestamp) {
						b.set(nameProperty, new java.util.Date(((java.sql.Timestamp) obj).getTime()));
					} else if (obj instanceof GregorianCalendar) {
						b.set(nameProperty, ((GregorianCalendar) obj).getTime());
					} else {
						throw new DeveloperException("Tipo de dados desconhecido. Solicitar desenvolvimento. para " + obj.getClass());
					}
				}
			}

			if (result.size() == 0) {
				throw new NotFoundException("Nenhum registro encontrado nesta consulta.");
			}

			return (T)result;
		} catch (SQLException e) {
			throw new SQLException(e.getMessage() + " - " + sql, e);
		} finally {

			this.lastTimeRun = logSlow(userContext, d1, sql, null);
			if (rs != null) rs.close();
			if (ps != null) ps.close();
		}

	}
	protected static String logSlow(UserContext userContext, Date d1, String sql, Class classOrigem) {
		Date d2 = new Date();
		int seg = DateUtils.secundsBetween(d1, d2);
		String lastTimeRun = DateUtils.diffTimeSimple(d1, d2);

		LOG.debug("Consulta em " + lastTimeRun);
		if (seg >= 10) {
			String sqlTemp = SQLUtils.formatPartStringToShow(sql);

			String classLocal = "";
			if (userContext != null) classLocal += "["+ userContext.getUser().getCurrentTask().getName() +"] ";
			String classOrigemStr = classOrigem != null ? " na classe \""+ classOrigem.getName() +"\" " : "";
			LOG.error(classLocal +"A consulta"+ classOrigemStr +" esta muito lenta ["+ lastTimeRun +"][select: " + sqlTemp + "]");
		}

		return lastTimeRun;
	}

	public static String prepare(String str) {
		str = str.replaceAll("\\$", "\\\\\\$");
		str = str.replaceAll("\\{", "\\\\\\{");
		str = str.replaceAll("\\}", "\\\\\\}");

		return str;
	}


	public PreparedStatement prepareStatement(String sql) throws SQLException, Exception {
		return prepareStatement(getConnection(), sql, false);
	}

	public static PreparedStatement prepareStatement(Connection conn, String sql, boolean fetchDirection) throws SQLException, Exception {
		if (fetchDirection == false) {
			PreparedStatement ps = conn.prepareStatement(sql);
			return ps;
		} else {
			PreparedStatement ps = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			return ps;
		}
	}

	public void closeQuery() throws SQLException {
		LOG.debug("Closing Query...");
		if (ps != null) ps.close();
		if (rs != null) rs.close();
		ps = null;
		rs = null;
	}


	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}

	public String getTypeDB() {
		return typeDB;
	}

	public String createDataBase() throws SQLException, Exception {
		String resultado = "";
		
		if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) {
			resultado = OracleDB.createDataBase(this);
		} else {
			throw new DeveloperException("Ainda não a function para outros bancos");
		}
		return resultado;
		
	}
	public String createFunction() throws SQLException, Exception {
		String resultado = "";

		Connection c = getConnection();
		if (typeDB.equalsIgnoreCase(TypeDB.ORACLE)) {
			resultado = OracleDB.createFunction(c);
		} else {
			throw new DeveloperException("Ainda não a function para outros bancos");
		}
		
		return resultado;

	}

	public String getLastTimeRun() {
		return StringUtils.coalesce(this.lastTimeRun, "0,0 s");
	}
}