
package br.com.dotum.jedi.core.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public abstract class View {
	private String query;
	private HashMap<String, Class<?>> typeMap = new HashMap<String, Class<?>>();
	private HashMap<String, String> valueMap = new HashMap<String, String>();
	
	public void setQuery(String query) {
		this.query = query;
	}
	
	public String getQuery() {
		return this.query;
	}
	
	public void addField(Class<?> type, String key, String value) {
		this.typeMap.put(key, type);
		this.valueMap.put(key, value);
	}
	
	public void addField(String key, String value) {
		this.valueMap.put(key, value);
	}
	
	public Class<?> getTypeFieldByKey(String key) {
		return typeMap.get(key);
	}
	
	public String getFieldByKey(String key) {
		return valueMap.get(key);
	}

	public String[] getFields() {
		Set<String> keys = this.valueMap.keySet();
		return keys.toArray(new String[keys.size()]);
	}
	
	public AbstractTableEnum[] getEnum() {
		List<AbstractTableEnum> list = new ArrayList<AbstractTableEnum>();

		Class<?>[] classes = getClass().getDeclaredClasses();
		for (Class classe : classes) {
			if (classe.isEnum() == false) continue;

			Object[] fields = classe.getEnumConstants();
			for (Object field2 : fields) {
				AbstractTableEnum obj = (AbstractTableEnum)field2;
				list.add(obj);
			}
		}
		return list.toArray(new AbstractTableEnum[list.size()]);
	}


	
}


