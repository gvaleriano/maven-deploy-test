package br.com.dotum.jedi.core.db;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;

public class WhereDB {
	private List<WhereDB> list;
	private Integer group;
	private Operation operation;
	private String field;
	private Condition condition;
	private Object value;


	public WhereDB() {
		list = new ArrayList<WhereDB>();
	}

	private WhereDB(Integer group, Operation operation, String field, Condition condition, Object value) {
		if ((condition.equals(Condition.ISNOTNULL)) || (condition.equals(Condition.ISNULL))) value = null;

		this.group = group;
		this.operation = (operation == null ? Operation.AND : operation);
		this.field = field;
		this.condition = condition;
		this.value = value;
	}

	public void add(Integer group, Operation operation, String field, Condition condition, Object value) {
		list.add(new WhereDB(group, operation, field, condition, value));
	}

	public void add(Operation operation, String field, Condition condition, Object value) {
		list.add(new WhereDB(null, operation, field, condition, value));
	}

	public void add(Integer group, String field, Condition condition, Object value) {
		list.add(new WhereDB(group, null, field, condition, value));
	}

	public void add(String field, Condition condition, Object value) {
		list.add(new WhereDB(null, null, field, condition, value));
	}

	public List<WhereDB> getList() {
		return list;
	}

	public Operation getOperation() {
		return operation;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public Condition getCondition() {
		return condition;
	}
	public void setCondition(Condition condition) {
		this.condition = condition;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public int size() {
		return list.size();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();

		try {
			for (WhereDB w : list) {
				String result = "";
				if (w.getValue() != null) {
					if (w.getValue() instanceof Integer[]) {
						result = SQLUtils.intToIn((Integer[])w.getValue());
					} else if (w.getValue() instanceof String[]) {
						result = SQLUtils.strToIn((String[])w.getValue());
					} else {
						result = ""+w.getValue();
					}
				}

				sb.append(StringUtils.coalesce(w.getGroup(), "DEFAULT") +" "+ w.getField() +" "+ w.getCondition() +" "+ result + "\n");
			}
			return sb.toString();
		} catch (Exception e) {
			return "Erro desconhecido ao efetuar o toString do WhereDB";
		}

	}

	public enum Condition {

		LESS("<"),
		LESSOREQUALS("<="),
		GREAT(">"),
		GREATOREQUALS(">="),
		EQUALS("="),
		IN("in"),
		LIKEBEGIN("/*%*/like"),
		LIKEEND("like/*%*/"),
		LIKE("like"),
		NOTLIKE("not like"),
		NOTIN("not in"),
		NOTEQUALS("!="), 
		ISNULL("is null"),
		ISNOTNULL("is not null");

		String value;

		Condition(String value) {
			this.value = value;
		}

		public static Condition getConditionByStr(String value) {
			if (value.equals(LESS.toString())) {
				return Condition.LESS;
			} else if (value.equals(LESSOREQUALS.toString())) {
				return Condition.LESSOREQUALS;
			} else if (value.equals(GREAT.toString())) {
				return Condition.GREAT;
			} else if (value.equals(GREATOREQUALS.toString())) {
				return Condition.GREATOREQUALS;
			} else if (value.equals(EQUALS.toString())) {
				return Condition.EQUALS;
			} else if (value.equals(IN.toString())) {
				return Condition.IN;
			} else if (value.equals(LIKEBEGIN.toString())) {
				return Condition.LIKEBEGIN;
			} else if (value.equals(LIKEEND.toString())) {
				return Condition.LIKEEND;
			} else if (value.equals(LIKE.toString())) {
				return Condition.LIKE;
			} else if (value.equals(NOTIN.toString())) {
				return Condition.NOTIN;
			} else if (value.equals(NOTEQUALS.toString())) {
				return Condition.NOTEQUALS;
			}
			return null;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public enum Operation {
		AND("and"),
		OR("or");

		private String value;
		Operation(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public Integer getGroup() {
		return group;
	}

}



