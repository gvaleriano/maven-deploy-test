package br.com.dotum.jedi.core.db.bean;

import java.util.Date;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Alerta", schema="sys", name="alerta", acronym="ale", typePk="S", sequence="sale_id")
public class AlertaBean extends Bean {

	@ColumnDB(name="ale_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="ale_cadastro", pk=false, length=0, precision=0)
	@FieldDB(name="Cadastro", label="Cadastro", required=false, type=TableFieldEditType.DATEFIELD)
	private Date cadastro;

	@ColumnDB(name="ale_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type=TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="ale_observacao", pk=false, length=2000, precision=0)
	@FieldDB(name="Observacao", label="Observação", required=false, type=TableFieldEditType.TEXTFIELD)
	private String observacao;

	@ColumnDB(name="ale_tipo", pk=false, length=10, precision=0)
	@FieldDB(name="Tipo", label="Tipo", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer tipo;

	@ColumnDB(name="ale_link", pk=false, length=250, precision=0)
	@FieldDB(name="Link", label="link", required=false, type=TableFieldEditType.TEXTFIELD)
	private String link;

	@ColumnDB(name="ale_action", pk=false, length=250, precision=0)
	@FieldDB(name="Action", label="Action", required=false, type=TableFieldEditType.TEXTFIELD)
	private String action;

	@ColumnDB(name="ale_window", pk=false, length=250, precision=0)
	@FieldDB(name="Window", label="Window", required=false, type=TableFieldEditType.TEXTFIELD)
	private String window;

	@ColumnDB(name="ale_sql", pk=false, length=2000, precision=0)
	@FieldDB(name="Sql", label="Sql", required=false, type=TableFieldEditType.TEXTFIELD)
	private String sql;

	public AlertaBean() {
		super();
	}

	public AlertaBean(Integer id) {
		super();
		this.id = id;
	}

	public AlertaBean(Integer id, Date cadastro, String descricao, String observacao, Integer tipo, String link, String action, String window, String sql) {
		super();
		this.id = id;
		this.cadastro = cadastro;
		this.descricao = descricao;
		this.observacao = observacao;
		this.tipo = tipo;
		this.link = link;
		this.action = action;
		this.window = window;
		this.sql = sql;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setCadastro(Date cadastro) {
		this.cadastro = cadastro;
	}

	public Date getCadastro() {
		return cadastro;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public void setWindow(String window) {
		this.window = window;
	}

	public String getWindow() {
		return window;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getSql() {
		return sql;
	}

}
