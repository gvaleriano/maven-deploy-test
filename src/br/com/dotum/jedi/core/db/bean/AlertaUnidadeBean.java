package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Alerta da unidade", schema="sys", name="alertaunidade", acronym="alun", typePk="S", sequence="salun_id")
public class AlertaUnidadeBean extends Bean {

	@ColumnDB(name="alun_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;
	
	@ColumnDB(name="uni_id", pk=false, fkName="fk_alun_uni", fk="uni_id = uni_id", length=10, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="ale_id", pk=false, fkName="fk_alun_ale", fk="ale_id = ale_id", length=10, precision=0)
	@FieldDB(name="Alerta.Id", label="Alerta", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:alerta")
	private AlertaBean alerta;

	@ColumnDB(name="alun_referencia", pk=false, length=10, precision=0)
	@FieldDB(name="Referencia", label="Referencia", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer referencia;
	
	public AlertaUnidadeBean() {
		super();
	}

	public AlertaUnidadeBean(Integer id) {
		super();
		this.id = id;
	}

	public AlertaUnidadeBean(Integer id, UnidadeBean unidade, AlertaBean alerta) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.alerta = alerta;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setAlerta(AlertaBean alerta) {
		this.alerta = alerta;
	}

	public AlertaBean getAlerta() {
		if (alerta == null) alerta = new AlertaBean();
		return alerta;
	}

	public Integer getReferencia() {
		return referencia;
	}

	public void setReferencia(Integer referencia) {
		this.referencia = referencia;
	}

}
