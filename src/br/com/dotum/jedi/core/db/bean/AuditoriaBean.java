package br.com.dotum.jedi.core.db.bean;

import java.util.Date;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Auditoria", schema="sys", name="auditoria", acronym="aud", typePk="S", sequence="saud_id")
public class AuditoriaBean extends Bean {

	@ColumnDB(name="aud_id", pk=true, length=15, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_id", pk=false, length=15, precision=0)
	@FieldDB(name="Unidade", label="Usuário", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer unidade;

	@ColumnDB(name="usu_id", pk=false, length=15, precision=0)
	@FieldDB(name="Usuario", label="Usuário", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer usuario;
	
	@ColumnDB(name="pes_id", pk=false, length=15, precision=0)
	@FieldDB(name="Pessoa", label="Pessoa", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer pessoa;

	@ColumnDB(name="audt_id", pk=false, fkName="fk_aud_audt", fk="audt_id = audt_id", length=15, precision=0)
	@FieldDB(name="AuditoriaTipo.Id", label="Auditoria Tipo", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:auditoriatipo")
	private AuditoriaTipoBean auditoriaTipo;

	@ColumnDB(name="aud_data", pk=false, length=0, precision=0)
	@FieldDB(name="Data", label="Data", required=false, type=TableFieldEditType.DATEFIELD)
	private Date data;

	@ColumnDB(name="aud_hora", pk=false, length=8, precision=0)
	@FieldDB(name="Hora", label="Hora", required=true, type=TableFieldEditType.HOURFIELD)
	private String hora;

	@ColumnDB(name="aud_origem", pk=false, length=250, precision=0)
	@FieldDB(name="Origem", label="Origem", required=false, type=TableFieldEditType.TEXTFIELD)
	private String origem;

	@ColumnDB(name="aud_aplicacao", pk=false, length=250, precision=0)
	@FieldDB(name="Aplicacao", label="Aplicação", required=false, type=TableFieldEditType.TEXTFIELD)
	private String aplicacao;

	@ColumnDB(name="aud_log", pk=false, length=999999, precision=0)
	@FieldDB(name="Log", label="Log", required=false, type=TableFieldEditType.TEXTFIELD)
	private String log;

	@ColumnDB(name="aud_transacao", pk=false, length=999999, precision=0)
	@FieldDB(name="Transacao", label="Transacao", required=false, type=TableFieldEditType.TEXTFIELD)
	private String transacao;
	
	public AuditoriaBean() {
		super();
	}

	public AuditoriaBean(Integer id) {
		super();
		this.id = id;
	}

	public AuditoriaBean(Integer id, Integer unidadeId, Integer usuarioId, Integer pessoaId, AuditoriaTipoBean auditoriaTipo, Date data, String hora, String origem, String aplicacao, String log, String transacao, String programa) {
		super();
		this.id = id;
		this.unidade = unidadeId;
		this.usuario = usuarioId;
		this.pessoa = pessoaId;
		this.auditoriaTipo = auditoriaTipo;
		this.data = data;
		this.hora = hora;
		this.origem = origem;
		this.aplicacao = aplicacao;
		this.log = log;
		this.transacao = transacao;
		this.aplicacao = programa;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public Integer getUnidade() {
		return unidade;
	}

	public void setUnidade(Integer unidadeId) {
		this.unidade = unidadeId;
	}

	public void setUsuario(Integer usuario) {
		this.usuario = usuario;
	}

	public Integer getUsuario() {
		return usuario;
	}

	public void setPessoa(Integer pessoa) {
		this.pessoa = pessoa;
	}

	public Integer getPessoa() {
		return pessoa;
	}

	public void setAuditoriaTipo(AuditoriaTipoBean auditoriaTipo) {
		this.auditoriaTipo = auditoriaTipo;
	}

	public AuditoriaTipoBean getAuditoriaTipo() {
		if (auditoriaTipo == null) auditoriaTipo = new AuditoriaTipoBean();
		return auditoriaTipo;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getData() {
		return data;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getHora() {
		return hora;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getOrigem() {
		return origem;
	}

	public void setAplicacao(String aplicacao) {
		this.aplicacao = aplicacao;
	}

	public String getAplicacao() {
		return aplicacao;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getLog() {
		return log;
	}

	public String getTransacao() {
		return transacao;
	}

	public void setTransacao(String transacao) {
		this.transacao = transacao;
	}
}
