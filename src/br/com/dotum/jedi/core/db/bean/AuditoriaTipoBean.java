package br.com.dotum.jedi.core.db.bean;

import java.util.Date;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Auditoria Tipo", schema="sys", name="auditoriatipo", acronym="audt", typePk="S", sequence="saudt_id")
public class AuditoriaTipoBean extends Bean {

	@ColumnDB(name="audt_id", pk=true, length=15, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="audt_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type=TableFieldEditType.TEXTFIELD)
	private String descricao;

	public AuditoriaTipoBean() {
		super();
	}

	public AuditoriaTipoBean(Integer id) {
		super();
		this.id = id;
	}

	public AuditoriaTipoBean(Integer id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
