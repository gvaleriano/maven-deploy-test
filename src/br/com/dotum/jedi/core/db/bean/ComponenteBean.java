package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;


@TableDB(label="Componente", schema="sys", name="componente", acronym="cpt", typePk="S", sequence="scpt_id")
public class ComponenteBean extends Bean {

	@ColumnDB(name="cpt_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="cpt_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type=TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="cpt_codigo", pk=false, length=50, precision=0)
	@FieldDB(name="Codigo", label="Codigo", required=false, type=TableFieldEditType.TEXTFIELD)
	private String codigo;

	@ColumnDB(name="cptt_id", pk=false, fkName="fk_cpt_cptt", fk="cptt_id = cptt_id", length=15, precision=0)
	@FieldDB(name="ComponenteTipo.Id", label="Componente Tipo", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:componentetipo")
	private ComponenteTipoBean componenteTipo;

	@ColumnDB(name="cpt_xml", pk=false, length=999999, precision=0)
	@FieldDB(name="Xml", label="Xml", required=false, type=TableFieldEditType.TEXTFIELD)
	private String xml;

	@ColumnDB(name="cpt_xmlcustomizado", pk=false, length=999999, precision=0)
	@FieldDB(name="XmlCustomizado", label="XmlCustomizado", required=false, type=TableFieldEditType.TEXTFIELD)
	private String xmlCustomizado;
	
	@ColumnDB(name="cpt_menu", pk=false, length=20, precision=10)
	@FieldDB(name="Menu", label="Menu", required=false, type=TableFieldEditType.DECIMALFIELD)
	private Double menu;
	
	public ComponenteBean() {
		super();
	}

	public ComponenteBean(Integer id) {
		super();
		this.id = id;
	}

	public ComponenteBean(Integer id, String descricao, String codigo, ComponenteTipoBean componenteTipo, String xml) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.codigo = codigo;
		this.componenteTipo = componenteTipo;
		this.xml = xml;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setComponenteTipo(ComponenteTipoBean componenteTipo) {
		this.componenteTipo = componenteTipo;
	}

	public ComponenteTipoBean getComponenteTipo() {
		if (componenteTipo == null) componenteTipo = new ComponenteTipoBean();
		return componenteTipo;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getXml() {
		return xml;
	}
	
	public String getXmlCustomizado() {
		return xmlCustomizado;
	}

	public void setXmlCustomizado(String xmlCustomizado) {
		this.xmlCustomizado = xmlCustomizado;
	}

	public Double getMenu() {
		return menu;
	}

	public void setMenu(Double menu) {
		this.menu = menu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComponenteBean other = (ComponenteBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
