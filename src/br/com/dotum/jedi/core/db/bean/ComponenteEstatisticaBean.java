package br.com.dotum.jedi.core.db.bean;

import java.util.Date;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;


@TableDB(label="Estatistica de componente", schema="sys", name="componenteestatistica", acronym="cpte", typePk="S", sequence="scpte_id")
public class ComponenteEstatisticaBean extends Bean {

	@ColumnDB(name="cpte_id", pk=true, length=15, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_cpte_uni", length=15, precision=0)
	@FieldDB(name="Unidade", label="Unidade", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private Integer unidade;

	@ColumnDB(name="usu_id", pk=false, fkName="fk_cpte_usu", length=15, precision=0)
	@FieldDB(name="Usuario", label="Usuário", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:usuario")
	private Integer usuario;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_cpte_pes", length=15, precision=0)
	@FieldDB(name="Pessoa", label="Pessoa", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private Integer pessoa;

	@ColumnDB(name="cpt_id", pk=false, fkName="fk_cpte_cpt", fk="cpt_id = cpt_id", length=15, precision=0)
	@FieldDB(name="Componente.Id", label="Componente", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:componente")
	private ComponenteBean componente;

	@ColumnDB(name="cpte_data", pk=false, length=0, precision=0)
	@FieldDB(name="Data", label="Data", required=true, type=TableFieldEditType.DATEFIELD)
	private Date data;

	@ColumnDB(name="cpte_hora", pk=false, length=10, precision=0)
	@FieldDB(name="Hora", label="Hora", required=true, type=TableFieldEditType.HOURFIELD)
	private String hora;

	@ColumnDB(name="cpte_transacao", pk=false, length=250, precision=0)
	@FieldDB(name="Transacao", label="Transação", required=true, type=TableFieldEditType.TEXTFIELD)
	private String transacao;
	
	@ColumnDB(name="cpte_tempoprocessamento", pk=false, length=15, precision=0)
	@FieldDB(name="TempoProcessamento", label="Tempo Processamento", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer tempoProcessamento;

	public ComponenteEstatisticaBean() {
		super();
	}

	public ComponenteEstatisticaBean(Integer id) {
		super();
		this.id = id;
	}

	public ComponenteEstatisticaBean(Integer id, Integer unidade, Integer usuario, Integer pessoa, ComponenteBean componente, Date data, String hora) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.usuario = usuario;
		this.pessoa = pessoa;
		this.componente = componente;
		this.data = data;
		this.hora = hora;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setUnidade(Integer unidade) {
		this.unidade = unidade;
	}

	public Integer getUnidade() {
		return unidade;
	}

	public void setUsuario(Integer usuario) {
		this.usuario = usuario;
	}

	public Integer getUsuario() {
		return usuario;
	}

	public void setPessoa(Integer pessoa) {
		this.pessoa = pessoa;
	}

	public Integer getPessoa() {
		return pessoa;
	}

	public void setComponente(ComponenteBean componente) {
		this.componente = componente;
	}

	public ComponenteBean getComponente() {
		if (componente == null) componente = new ComponenteBean();
		return componente;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getData() {
		return data;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getHora() {
		return hora;
	}

	public String getTransacao() {
		return transacao;
	}

	public void setTransacao(String transacao) {
		this.transacao = transacao;
	}

	public Integer getTempoProcessamento() {
		return tempoProcessamento;
	}

	public void setTempoProcessamento(Integer tempoProcessamento) {
		this.tempoProcessamento = tempoProcessamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComponenteEstatisticaBean other = (ComponenteEstatisticaBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
