package br.com.dotum.jedi.core.db.bean;

import java.util.Date;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.core.table.TableFieldEditType;


@TableDB(label="Release", schema="sys", name="fwrelease", acronym="fwr", typePk="S", sequence="sfwr_id")
public class FwReleaseBean extends Bean {

	@ColumnDB(name="fwr_id", pk=true, length=15, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="fwr_aplicado", pk=false, length=1, precision=0)
	@FieldDB(name="Aplicado", label="Aplicado", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer aplicado;

	@ColumnDB(name="fwr_datacadastro", pk=false, length=0, precision=0)
	@FieldDB(name="DataCadastro", label="Data Cadastro", required=false, type=TableFieldEditType.DATEFIELD)
	private Date dataCadastro;

	@ColumnDB(name="fwr_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type=TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="fwr_tipo", pk=false, length=50, precision=0)
	@FieldDB(name="Tipo", label="Tipo", required=false, type=TableFieldEditType.TEXTFIELD)
	private String tipo;

	@ColumnDB(name="fwr_programador", pk=false, length=50, precision=0)
	@FieldDB(name="Programador", label="programador", required=false, type=TableFieldEditType.TEXTFIELD)
	private String programador;

	@ColumnDB(name="fwr_versao", pk=false, length=50, precision=0)
	@FieldDB(name="Versao", label="Versão", required=false, type=TableFieldEditType.TEXTFIELD)
	private String versao;

	@ColumnDB(name="fwr_arquivo", pk=false, length=0, precision=0)
	@FieldDB(name="Arquivo", label="Arquivo", required=false, type=TableFieldEditType.FILEUPLOAD)
	private Blob arquivo;

	@ColumnDB(name="fwr_dataprocessamento", pk=false, length=0, precision=0)
	@FieldDB(name="DataProcessamento", label="Data Processamento", required=false, type=TableFieldEditType.DATEFIELD)
	private Date dataProcessamento;

	@ColumnDB(name="fwr_log", pk=false, length=4000, precision=0)
	@FieldDB(name="Log", label="Log", required=false, type=TableFieldEditType.TEXTFIELD)
	private String log;

	@ColumnDB(name="fwr_tamanho", pk=false, length=50, precision=0)
	@FieldDB(name="Tamanho", label="Tamanho", required=false, type=TableFieldEditType.TEXTFIELD)
	private String tamanho;

	@ColumnDB(name="fwr_visivel", pk=false, length=1, precision=0)
	@FieldDB(name="Visivel", label="visivel", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer visivel;

	@ColumnDB(name="fwr_releasedependente", pk=false, length=250, precision=0)
	@FieldDB(name="ReleaseDependente", label="Release Dependente", required=false, type=TableFieldEditType.TEXTFIELD)
	private String releaseDependente;
	
	@ColumnDB(name="fwr_time", pk=false, length=10, precision=0)
	@FieldDB(name="Time", label="Time", required=false, type=TableFieldEditType.TEXTFIELD)
	private String time;
	
	public FwReleaseBean() {
		super();
	}

	public FwReleaseBean(Integer id) {
		super();
		this.id = id;
	}

	public FwReleaseBean(Integer id, Integer aplicado, Date dataCadastro, String descricao, String tipo, String programador, String versao, Blob arquivo, Date dataProcessamento, String log, String tamanho, Integer visivel) {
		super();
		this.id = id;
		this.aplicado = aplicado;
		this.dataCadastro = dataCadastro;
		this.descricao = descricao;
		this.tipo = tipo;
		this.programador = programador;
		this.versao = versao;
		this.arquivo = arquivo;
		this.dataProcessamento = dataProcessamento;
		this.log = log;
		this.tamanho = tamanho;
		this.visivel = visivel;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public Integer getAplicado() {
		return aplicado;
	}

	public void setAplicado(Integer aplicado) {
		this.aplicado = aplicado;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setProgramador(String programador) {
		this.programador = programador;
	}

	public String getProgramador() {
		return programador;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getVersao() {
		return versao;
	}

	public void setArquivo(Blob arquivo) {
		this.arquivo = arquivo;
	}

	public Blob getArquivo() {
		return arquivo;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getLog() {
		return log;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	public String getTamanho() {
		return tamanho;
	}

	public void setVisivel(Integer visivel) {
		this.visivel = visivel;
	}

	public Integer getVisivel() {
		return visivel;
	}

	public String getReleaseDependente() {
		return releaseDependente;
	}

	public void setReleaseDependente(String releaseDependente) {
		this.releaseDependente = releaseDependente;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FwReleaseBean other = (FwReleaseBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
