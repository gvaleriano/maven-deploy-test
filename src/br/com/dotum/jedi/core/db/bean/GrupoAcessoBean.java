package br.com.dotum.jedi.core.db.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;
import br.com.dotum.jedi.core.db.AbstractTableEnum;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;


@TableDB(label="Grupo Acesso", schema="sys", name="grupoacesso", acronym="grua", typePk="S", sequence="sgrua_id")
public class GrupoAcessoBean extends Bean {

	@ColumnDB(name="grua_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="grua_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type=TableFieldEditType.TEXTFIELD)
	private String descricao;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public GrupoAcessoBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public GrupoAcessoBean(Integer id) {
		super();
		this.id = id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoAcessoBean other = (GrupoAcessoBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
