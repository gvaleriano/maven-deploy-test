package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;


@TableDB(label="Grupo Acesso Tarefa", schema="sys", name="grupoacessotarefa", acronym="grta", typePk="S", sequence="sgrta_id")
public class GrupoAcessoTarefaBean extends Bean {

	@ColumnDB(name="grta_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="grua_id", pk=false, fkName="fk_grta_grua", fk="grua_id = grua_id", length=10, precision=0)
	@FieldDB(name="GrupoAcesso.Id", label="Grupo Acesso", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:grupoacesso")
	private GrupoAcessoBean grupoAcesso;

	@ColumnDB(name="tar_id", pk=false, fkName="fk_grta_tar", fk="tar_id = tar_id", length=10, precision=0)
	@FieldDB(name="Tarefa.Id", label="Tarefa", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:tarefa")
	private TarefaBean tarefa;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public GrupoAcessoTarefaBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public GrupoAcessoTarefaBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public GrupoAcessoTarefaBean(Integer id, GrupoAcessoBean grupoAcesso, TarefaBean tarefa) {
		super();
		this.id = id;
		this.grupoAcesso = grupoAcesso;
		this.tarefa = tarefa;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setGrupoAcesso(GrupoAcessoBean grupoAcesso) {
		this.grupoAcesso = grupoAcesso;
	}

	public GrupoAcessoBean getGrupoAcesso() {
		if (grupoAcesso == null) grupoAcesso = new GrupoAcessoBean();
		return grupoAcesso;
	}

	public void setTarefa(TarefaBean tarefa) {
		this.tarefa = tarefa;
	}

	public TarefaBean getTarefa() {
		if (tarefa == null) tarefa = new TarefaBean();
		return tarefa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoAcessoTarefaBean other = (GrupoAcessoTarefaBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
