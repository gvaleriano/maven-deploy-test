package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Pessoa", schema="sys", name="pessoa", acronym="pes", typePk="S", auditing=true)
public class PessoaBean extends Bean {

	@ColumnDB(name="pes_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_pes_uni", fk="uni_id = uni_id", length=10, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="pes_nomerazao", pk=false, length=250, precision=0)
	@FieldDB(name="NomeRazao", label="Nome/Razao", required=true, type=TableFieldEditType.TEXTFIELD)
	private String nomeRazao;

	@ColumnDB(name="pes_usuario", pk=false, length=250, precision=0)
	@FieldDB(name="Usuario", label="Usuario", required=false, type=TableFieldEditType.TEXTFIELD)
	private String usuario;
	
	@ColumnDB(name="pes_senha", pk=false, length=250, precision=0)
	@FieldDB(name="Senha", label="Senha", required=false, type=TableFieldEditType.TEXTFIELD)
	private String senha;

	public PessoaBean() {
		super();
	}

	public PessoaBean(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public String getNomeRazao() {
		return nomeRazao;
	}

	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	
}
