package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;


@TableDB(label="Pessoa Tarefa", schema="sys", name="pessoatarefa", acronym="peta", typePk="S", sequence="speta_id")
public class PessoaTarefaBean extends Bean {

	@ColumnDB(name="peta_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_peta_pes", fk="pes_id = pes_id", length=10, precision=0)
	@FieldDB(name="Pessoa.Id", label="Pessoa", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoa;

	@ColumnDB(name="tar_id", pk=false, fkName="fk_peta_tar", fk="tar_id = tar_id", length=10, precision=0)
	@FieldDB(name="Tarefa.Id", label="tarefa", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:tarefa")
	private TarefaBean tarefa;

	public PessoaTarefaBean() {
		super();
	}

	public PessoaTarefaBean(Integer id) {
		super();
		this.id = id;
	}

	public PessoaTarefaBean(Integer id, PessoaBean pessoa, TarefaBean tarefa) {
		super();
		this.id = id;
		this.pessoa = pessoa;
		this.tarefa = tarefa;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPessoa(PessoaBean pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaBean getPessoa() {
		if (pessoa == null) pessoa = new PessoaBean();
		return pessoa;
	}

	public void setTarefa(TarefaBean tarefa) {
		this.tarefa = tarefa;
	}

	public TarefaBean getTarefa() {
		if (tarefa == null) tarefa = new TarefaBean();
		return tarefa;
	}

}
