package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;


@TableDB(label="tarefa", schema="sys", name="tarefa", acronym="tar", typePk="S", sequence="star_id")
public class TarefaBean extends Bean {

	@ColumnDB(name="tar_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="tar_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type=TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="tar_codigo", pk=false, length=10, precision=0)
	@FieldDB(name="Codigo", label="Codigo", required=false, type=TableFieldEditType.TEXTFIELD)
	private String codigo;

	public TarefaBean() {
		super();
	}

	public TarefaBean(Integer id) {
		super();
		this.id = id;
	}

	public TarefaBean(Integer id, String descricao, String codigo) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.codigo = codigo;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarefaBean other = (TarefaBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
