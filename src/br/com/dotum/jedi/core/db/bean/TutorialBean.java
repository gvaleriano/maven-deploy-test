package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Tutorial", schema="sys", name="tutorial", acronym="tut", typePk="S", sequence="stut_id")
public class TutorialBean extends Bean {

	@ColumnDB(name="tut_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="tut_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type=TableFieldEditType.TEXTFIELD)
	private String descricao;

	public TutorialBean() {
		super();
	}

	public TutorialBean(Integer id) {
		super();
		this.id = id;
	}

	public TutorialBean(Integer id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
