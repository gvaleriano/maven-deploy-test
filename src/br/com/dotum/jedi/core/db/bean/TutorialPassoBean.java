package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;


@TableDB(label="Passo do tutorial", schema="sys", name="tutorialpasso", acronym="tutp", typePk="S", sequence="stutp_id")
public class TutorialPassoBean extends Bean {

	@ColumnDB(name="tutp_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="tut_id", pk=false, fkName="fk_tutp_tut", fk="tut_id = tut_id", length=10, precision=0)
	@FieldDB(name="Tutorial.Id", label="Tutorial", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:tutorial")
	private TutorialBean tutorial;

	@ColumnDB(name="tutp_seletor", pk=false, length=250, precision=0)
	@FieldDB(name="Seletor", label="Seletor", required=false, type=TableFieldEditType.TEXTFIELD)
	private String seletor;

	@ColumnDB(name="tutp_titulo", pk=false, length=250, precision=0)
	@FieldDB(name="Titulo", label="Titulo", required=false, type=TableFieldEditType.TEXTFIELD)
	private String titulo;

	@ColumnDB(name="tutp_observacao", pk=false, length=250, precision=0)
	@FieldDB(name="Observacao", label="Observação", required=false, type=TableFieldEditType.TEXTFIELD)
	private String observacao;

	@ColumnDB(name="tutp_posicao", pk=false, length=20, precision=0)
	@FieldDB(name="Posicao", label="Posição", required=false, type=TableFieldEditType.TEXTFIELD)
	private String posicao;

	@ColumnDB(name="tutp_tamanho", pk=false, length=20, precision=0)
	@FieldDB(name="Tamanho", label="Tamanho", required=false, type=TableFieldEditType.TEXTFIELD)
	private String tamanho;

	public TutorialPassoBean() {
		super();
	}

	public TutorialPassoBean(Integer id) {
		super();
		this.id = id;
	}

	public TutorialPassoBean(Integer id, TutorialBean tutorial, String seletor, String titulo, String observacao, String posicao, String tamanho) {
		super();
		this.id = id;
		this.tutorial = tutorial;
		this.seletor = seletor;
		this.titulo = titulo;
		this.observacao = observacao;
		this.posicao = posicao;
		this.tamanho = tamanho;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setTutorial(TutorialBean tutorial) {
		this.tutorial = tutorial;
	}

	public TutorialBean getTutorial() {
		if (tutorial == null) tutorial = new TutorialBean();
		return tutorial;
	}

	public void setSeletor(String seletor) {
		this.seletor = seletor;
	}

	public String getSeletor() {
		return seletor;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setPosicao(String posicao) {
		this.posicao = posicao;
	}

	public String getPosicao() {
		return posicao;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	public String getTamanho() {
		return tamanho;
	}

}
