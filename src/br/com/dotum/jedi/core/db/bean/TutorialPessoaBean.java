package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Vinculo tutorial pessoa ", schema="sys", name="tutorialpessoa", acronym="tupe", typePk="S", sequence="stupe_id")
public class TutorialPessoaBean extends Bean {

	@ColumnDB(name="tupe_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_tupe_uni", fk="uni_id = uni_id", length=10, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="tut_id", pk=false, fkName="fk_tupe_tut", fk="tut_id = tut_id", length=10, precision=0)
	@FieldDB(name="Tutorial.Id", label="Tutorial", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:tutorial")
	private TutorialBean tutorial;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_tupe_pes", fk="pes_id = pes_id", length=10, precision=0)
	@FieldDB(name="Pessoa.Id", label="Pessoa", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoa;

	public TutorialPessoaBean() {
		super();
	}

	public TutorialPessoaBean(Integer id) {
		super();
		this.id = id;
	}

	public TutorialPessoaBean(Integer id, UnidadeBean unidade, TutorialBean tutorial, PessoaBean pessoa) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.tutorial = tutorial;
		this.pessoa = pessoa;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setTutorial(TutorialBean tutorial) {
		this.tutorial = tutorial;
	}

	public TutorialBean getTutorial() {
		if (tutorial == null) tutorial = new TutorialBean();
		return tutorial;
	}

	public void setPessoa(PessoaBean pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaBean getPessoa() {
		if (pessoa == null) pessoa = new PessoaBean();
		return pessoa;
	}

}
