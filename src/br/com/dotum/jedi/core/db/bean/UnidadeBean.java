package br.com.dotum.jedi.core.db.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;


@TableDB(label="Unidade", schema="sys", name="unidade", acronym="uni", typePk="S", sequence="suni_id")
public class UnidadeBean extends Bean {

	@ColumnDB(name="uni_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=true, type=TableFieldEditType.TEXTFIELD)
	private String descricao;

	public UnidadeBean() {
		super();
	}

	public UnidadeBean(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
