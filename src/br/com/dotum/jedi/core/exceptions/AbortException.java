package br.com.dotum.jedi.core.exceptions;


public class AbortException extends JediException {

	public AbortException() {
		super("Operação cancelada!");
	}

	public AbortException(String message, Throwable cause) {
		super(message, cause);
	}

	public AbortException(String message) {
		super(message);
	}

	public AbortException(Throwable cause) {
		super(cause);
	}
}
