package br.com.dotum.jedi.core.exceptions;


public class DeleteException extends JediException {

	public DeleteException() {
		super();
	}

	public DeleteException(String message, Throwable cause) {
		super(message, cause);
	}

	public DeleteException(String message) {
		super(message);
	}

	public DeleteException(Throwable cause) {
		super(cause);
	}
	

}

