package br.com.dotum.jedi.core.exceptions;


public class ErrorException extends JediException {

	public ErrorException() {
		super("Operação cancelada!");
	}

	public ErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public ErrorException(String message) {
		super(message);
	}

	public ErrorException(Throwable cause) {
		super(cause);
	}
}
