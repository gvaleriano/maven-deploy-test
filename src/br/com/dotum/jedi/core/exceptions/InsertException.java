package br.com.dotum.jedi.core.exceptions;


public class InsertException extends JediException {

	public InsertException() {
		super();
	}

	public InsertException(String message, Throwable cause) {
		super(message, cause);
	}

	public InsertException(String message) {
		super(message);
	}

	public InsertException(Throwable cause) {
		super(cause);
	}
	

}

