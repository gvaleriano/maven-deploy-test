package br.com.dotum.jedi.core.exceptions;


public class JediException extends RuntimeException {
	private static final long serialVersionUID = 169263781163847404L;

	public JediException() {
		super();
	}

	public JediException(String message, Throwable cause) {
		super(message, cause);
	}

	public JediException(String message) {
		super(message);
	}

	public JediException(Throwable cause) {
		super(cause);
	}
}
