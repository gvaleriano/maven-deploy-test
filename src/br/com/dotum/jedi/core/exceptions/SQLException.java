package br.com.dotum.jedi.core.exceptions;


public class SQLException extends JediException {

	public SQLException() {
		super();
	}

	public SQLException(String message, Throwable cause) {
		super(message, cause);
	}

	public SQLException(String message) {
		super(message);
	}

	public SQLException(Throwable cause) {
		super(cause);
	}
	

}

