package br.com.dotum.jedi.core.exceptions;


public class SelectException extends JediException {

	public SelectException() {
		super();
	}

	public SelectException(String message, Throwable cause) {
		super(message, cause);
	}

	public SelectException(String message) {
		super(message);
	}

	public SelectException(Throwable cause) {
		super(cause);
	}
	

}

