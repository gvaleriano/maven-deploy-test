package br.com.dotum.jedi.core.exceptions;


public class TimeoutSessionException extends JediException {

	public TimeoutSessionException() {
		super();
	}

	public TimeoutSessionException(String message, Throwable cause) {
		super(message, cause);
	}

	public TimeoutSessionException(String message) {
		super(message);
	}

	public TimeoutSessionException(Throwable cause) {
		super(cause);
	}
	

}

