package br.com.dotum.jedi.core.exceptions;


public class UpdateException extends JediException {

	public UpdateException() {
		super();
	}

	public UpdateException(String message, Throwable cause) {
		super(message, cause);
	}

	public UpdateException(String message) {
		super(message);
	}

	public UpdateException(Throwable cause) {
		super(cause);
	}
	

}

