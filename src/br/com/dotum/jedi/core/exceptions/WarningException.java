package br.com.dotum.jedi.core.exceptions;


public class WarningException extends JediException {

	public WarningException() {
		super("Operação cancelada!");
	}

	public WarningException(String message, Throwable cause) {
		super(message, cause);
	}

	public WarningException(String message) {
		super(message);
	}

	public WarningException(Throwable cause) {
		super(cause);
	}
}
