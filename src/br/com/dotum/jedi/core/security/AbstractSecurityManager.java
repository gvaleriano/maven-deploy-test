package br.com.dotum.jedi.core.security;

import java.util.List;
import java.util.Map;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.LoginException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

public abstract class AbstractSecurityManager {
	private static Log LOG = LogFactory.getLog(AbstractSecurityManager.class);
	
	protected static AbstractSecurityManager me;

	public static AbstractSecurityManager getInstance() {
		return me;
	}

	public abstract SecurityUserBean findUserByLogin(TransactionDB trans, String login) throws WarningException;
	public abstract List<SecurityTaskBean> getListTaskAllowedUser(TransactionDB trans, String login) throws WarningException;
	public abstract UserContext doLogin(TransactionDB trans, String login, String password, Map<String, Object> map) throws WarningException;

	private boolean isUserAndPasswordValid(SecurityUserBean bean, String login, String password) throws WarningException {
		if (bean != null) {
			String tmp = bean.getSenha();
			return tmp.trim().equals(password.trim());
		} else {
			throw new WarningException("Usuário não encontrado: "+ login);
		}
	}

	public SecurityUserBean validLogin(TransactionDB trans, String login, String password) throws LoginException, WarningException {
		// verifico se usuario existe
		if (login == null)
			throw new LoginException("O usuário não foi informado!");

		// verifico se senha existe
		if (password == null)
			throw new LoginException("A senha não foi informada! (Usuario: "+ login +")");
		
		// vejo se existe
		SecurityUserBean bean = findUserByLogin(trans, login);
			
		// verifico se a senha esta correta
		if (isUserAndPasswordValid(bean, login, password) == false)
			throw new LoginException("Usuario \""+ login +"\" informou a senha errada!");
		
		return bean;
	}

	
	public boolean isTaskAllowed(SecurityUserBean userBean, String taskCode) throws LoginException, Exception {
		TransactionDB trans = TransactionDB.getInstance(null);
		if (userBean.getTasks() == null) userBean.setTasks( getTasksFromUser(trans, userBean.getLogin()) );
		if ((userBean != null) && (userBean.getTask(taskCode) != null)) {
			return true;
		}
		return false;
	}
	
	public List<SecurityTaskBean> getTasksFromUser(TransactionDB trans, String login) throws LoginException {
		List<SecurityTaskBean> result = this.getListTaskAllowedUser(trans, login);
		return result;
	}
	public SecurityUserBean login(TransactionDB trans, String login, String password) throws LoginException {
		try {
			SecurityUserBean user = validLogin(trans, login, password);
			user.setTasks( this.getListTaskAllowedUser(trans, login) );
			
			// guarda a senha sem criptografia, precisa de outro
			// atributo senao da erro na proxima checagem da senha
			user.setSenhaDigitada(password);
			
			return user;
		} catch (LoginException ex) {
			throw ex;
		} catch (Exception ex) {
			LOG.error(ex.getLocalizedMessage(), ex);
			throw new LoginException("Não foi possível efetuar o login!", ex);
		}
	}
}

