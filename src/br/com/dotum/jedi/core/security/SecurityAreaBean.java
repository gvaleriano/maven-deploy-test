package br.com.dotum.jedi.core.security;

public class SecurityAreaBean {
	public Integer id;
	public String description;
	public boolean isDefault;
	public Integer nivelGri;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getNivelGri() {
		return nivelGri;
	}
	public void setNivelGri(Integer nivelGri) {
		this.nivelGri = nivelGri;
	}
	public boolean isDefault() {
		return isDefault;
	}
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
}

