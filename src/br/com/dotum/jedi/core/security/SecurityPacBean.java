package br.com.dotum.jedi.core.security;

import br.com.dotum.jedi.core.table.Bean;

public class SecurityPacBean extends Bean {
	public Integer id;
	public String description;
	public boolean isDefault;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isDefault() {
		return isDefault;
	}
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
}

