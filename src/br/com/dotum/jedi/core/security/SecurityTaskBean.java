package br.com.dotum.jedi.core.security;

import br.com.dotum.jedi.core.table.Bean;

public class SecurityTaskBean extends Bean {

	protected Integer id;
	protected String codigo;
	protected String descricao;
	protected String acao;
	protected String dica;
	protected Integer controla;
	protected Integer localizacao;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAcao() {
		return acao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public String getDica() {
		return dica;
	}
	public void setDica(String dica) {
		this.dica = dica;
	}
	public Integer getControla() {
		return controla;
	}
	public void setControla(Integer controlaAcesso) {
		this.controla = controlaAcesso;
	}
	public Integer getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(Integer localizacao) {
		this.localizacao = localizacao;
	}
}

