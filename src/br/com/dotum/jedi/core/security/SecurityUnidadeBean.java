package br.com.dotum.jedi.core.security;

import java.io.Serializable;

import br.com.dotum.jedi.util.StringUtils;



public class SecurityUnidadeBean implements Serializable {

	protected Integer id;
	protected String nome;
	protected String cnpj;
	protected String timeZone;
	protected String uf;
	protected boolean isMatriz = false;
	protected boolean isDotum = false;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = StringUtils.getNumberOnly(cnpj);;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public boolean isMatriz() {
		return isMatriz;
	}
	public void setMatriz(boolean isMatriz) {
		this.isMatriz = isMatriz;
	}
	public boolean isDotum() {
		if (this.cnpj == null) return false;
		return this.cnpj.equals("12498908000156");
	}
}

