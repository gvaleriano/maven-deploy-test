package br.com.dotum.jedi.core.security;

import java.util.List;



import br.com.dotum.jedi.core.table.Bean;

public class SecurityUserBean extends Bean {
	private static final long serialVersionUID = 691035363694423402L;
	
	protected Integer unidadeId;
	protected Integer id;
	protected String nome;
	protected String login;
	protected String senha;
	protected String senhaDigitada;
	protected Integer senhaTrocar;
	protected String email;
	protected String emailSenha;
	protected int ativo;
	protected String i18n;
	protected List<SecurityTaskBean> tasks;
	protected Class<?> currentTask;

	private boolean menu = true;
	
	//
	public Integer getUnidadeId() {
		return unidadeId;
	}
	public void setUnidadeId(Integer unidadeId) {
		this.unidadeId = unidadeId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getSenhaDigitada() {
		return senhaDigitada;
	}
	public void setSenhaDigitada(String senhaDigitada) {
		this.senhaDigitada = senhaDigitada;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailSenha() {
		return emailSenha;
	}
	public void setEmailSenha(String emailSenha) {
		this.emailSenha = emailSenha;
	}
	public int getAtivo() {
		return this.ativo;
	}
	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}
	public Integer getSenhaTrocar() {
		return senhaTrocar;
	}
	public void setSenhaTrocar(Integer senhaTrocar) {
		this.senhaTrocar = senhaTrocar;
	}
	public String getI18n() {
		return i18n;
	}
	public void setI18n(String i18n) {
		this.i18n = i18n;
	}
	// retorna as tarefas autorizadas para o usuario
	public List<SecurityTaskBean> getTasks() {
		return this.tasks;
	}

	public String[] getCodeTasks() {
		String[] result = new String[this.tasks.size()];
		for (int i = 0; i < this.tasks.size(); i++) {
			result[i] = this.tasks.get(i).getCodigo();
		}
		return result;
	}
	
	public boolean hasAccessTaskById(Integer tarId) {
		for (SecurityTaskBean task : this.tasks) {
			if (task.getId().equals(tarId) == false) continue;
			return true;
		}
		return false;
	}
	
	public void setTasks(List<SecurityTaskBean> tasks) {
		this.tasks = tasks;
	}
	
	public SecurityTaskBean getTask(String code) {
		if (this.tasks == null) return null;
		
		for (SecurityTaskBean task : this.tasks) {
			if (task.getCodigo().equalsIgnoreCase(code) == false) continue;
			return task;
		}
		return null;
	}
	public boolean hasAccessTaskByCode(String code) {
		if (getTask(code) != null) return true;
		return false;
	}
	public void setCurrentTask(Class<?> currentTask) {
		this.currentTask = currentTask;
	}
	
	public Class<?> getCurrentTask() {
		return this.currentTask;
	}
	public void doDesativarMenu(Double ... menuArray) {
		this.menu = false;
	}
	public void doAtivarMenu(Double ... menuArray) {
		this.menu = true;
	}
	public boolean isMenuAtivo(Double inMenu) {
		return this.menu;
	}

}