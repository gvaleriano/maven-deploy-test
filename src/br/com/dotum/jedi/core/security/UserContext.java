package br.com.dotum.jedi.core.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.PropertiesUtils;

public class UserContext extends Bean {

	private String id;
	private String name;

	private SecurityUserBean user;
	private SecurityUserBean person;

	private Date dataLoginAt;
	private String horaLoginAt;
	
	private Date dataLastInterationAt;
	private String horaLastInterationAt;
	
	private HashMap<String, Object> values = new HashMap<String, Object>();

	private String serverPath;
	private String i18n;
	private String theme;
	
	// tirar isso aqui tbm
	private SecurityUnidadeBean unidade;
	private List<SecurityUnidadeBean> unidadeList = new ArrayList<SecurityUnidadeBean>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public SecurityUserBean getUser() {
		if (user == null) user = new SecurityUserBean(); 
		return user;
	}
	public void setUser(SecurityUserBean user) {
		this.user = user;
	}
	public SecurityUserBean getPerson() {
		if (person == null) person = new SecurityUserBean(); 
		return person;
	}
	public void setPerson(SecurityUserBean person) {
		this.person = person;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * Trocar este metodo por TransactionDB.getDataAtual();
	 * @return
	 */
	@Deprecated
	public Date getDataAtual() {
		return new Date();
	}
	public Date getDataLoginAt() {
		return dataLoginAt;
	}
	public void setDataLoginAt(Date dataLoginAt) {
		this.dataLoginAt = dataLoginAt;
	}
	public String getHoraLoginAt() {
		return horaLoginAt;
	}
	public void setHoraLoginAt(String horaLoginAt) {
		this.horaLoginAt = horaLoginAt;
	}
	public Date getDataLastInterationAt() {
		return dataLastInterationAt;
	}
	public void setDataLastInterationAt(Date dataLastInterationAt) {
		this.dataLastInterationAt = dataLastInterationAt;
	}
	public String getHoraLastInterationAt() {
		return horaLastInterationAt;
	}
	public void setHoraLastInterationAt(String horaLastInterationAt) {
		this.horaLastInterationAt = horaLastInterationAt;
	}
	public boolean isDeveloper() {
		return PropertiesUtils.getBoolean(PropertiesConstants.SYSTEM_DEVELOPER);
	}

	// metodos de objetos na sessao
	public HashMap<String, Object> getValues() {
		return values;
	}
	public void setValues(HashMap<String, Object> values) {
		this.values = values;
	}

	// metodos de unidade
	public boolean isMatriz() {
		return this.getUnidade().isMatriz();
	}
	public List<SecurityUnidadeBean> getUnidadeList() {
		return unidadeList;
	}
	public void setUnidadeList(List<SecurityUnidadeBean> unidadeList) {
		this.unidadeList = unidadeList;
	}
	public Integer[] getUnidadeArray() {
		Set<Integer> unidadeArray = new HashSet<Integer>();
		// insere a padrao

		unidadeArray.add(this.getUnidade().getId());
		for (int i = 0; i < unidadeList.size(); i++) {
			boolean achou = false;
			Integer uniId = unidadeList.get(i).getId();

			for (Integer temp : unidadeArray) {
				if (temp.equals(uniId) == false) continue;
				achou = true;
			}

			if (achou == false) 
				unidadeArray.add(uniId);
		}

		return unidadeArray.toArray(new Integer[unidadeArray.size()]);
	}

	public int unidadeSize() {
		int i = 1;
		for (SecurityUnidadeBean bean :  unidadeList) {
			if (getUnidade().getId().equals(bean.getId())) continue;
			i++;
		}

		return i;		
	}


	/**
	 * Metodo que retorna uma string no seguinte formato (1, 2, 3)<br> 
	 * Muito usado na seguinte situação WHERE uni_id IN (1, 2, 3)<br><br>
	 * Logo esse metodo traz todas as unidade que o usuario tem direito.<br>
	 * Esse direito consiste em:<br>
	 * 1 - unidade padrao (unidade logada)<br>
	 * 2 - unidades vinculadas (tem essa opção dentro do cadastro de usuario)<br>
	 * 
	 * @return
	 */
	public String getUnidades() {
		StringBuilder result = new StringBuilder();
		result.append("(");
		result.append(this.unidade.getId());		
		for (SecurityUnidadeBean bean : unidadeList) {
			result.append(", ");
			result.append(bean.getId());
		}
		result.append(")");
		return result.toString();
	}
	/**
	 * Metodo que retorna o atributo que armazena a unidade que o usuario esta logado.<br />
	 * Este atributo foi populado quando o usuario logou no sistema
	 * <br />
	 * <br />
	 * @return SecurityUnidadeBean
	 */
	public SecurityUnidadeBean getUnidade() {
		if (unidade == null) unidade = new SecurityUnidadeBean(); 
		return unidade;
	}
	public void setUnidade(SecurityUnidadeBean unidade) {
		this.unidade = unidade;
	}
	public String getServerPath() {
		return serverPath;
	}
	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}
	public String getI18n() {
		return i18n;
	}
	public void setI18n(String i18n) {
		this.i18n = i18n;
	}
	public String getTheme() {
		return theme == null ? "redmond" : theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
}

