package br.com.dotum.jedi.core.table;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.DBHelper;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.MathUtils;
import br.com.dotum.jedi.util.SQLUtils;
import br.com.dotum.jedi.util.StringUtils;


public class Bean implements Serializable {
	public static final String PL = "\n";
	public static final String RNUM = "r_num_";
	public static final String LINENUMBER = "l_num_";
	public static final String QTD = "q_td_";

	private static String GET = "get";
	private static String IS = "is";	

	private String[] attributeToJson; 

	private Map<String, Object> extraMap = new LinkedHashMap<String, Object>();

	protected Map<String, Object> getMap() {
		return this.extraMap;
	}

	// TODO refactory no dotum e cloud
	public <T> T get(String attribute) {
		return getAttribute(attribute);
	}
	// TODO refactory no dotum e cloud
	public boolean set(String attribute, Object value) {
		return setAttribute(attribute, value);
	}





	public boolean setAttribute(String attribute, Object value) {
		if (attribute == null) {
			return false;
		}

		if (this.getClass().equals(Bean.class)) {
			extraMap.put(attribute.toLowerCase(), value);
			return true;
		} else {

			// Split out property on dots ( "person.name.first" -> "person","name","first" -> getPerson().getName().getFirst() )
			StringTokenizer st = new StringTokenizer(attribute, ".");

			if (st.countTokens() == 0) {
				return false;
			}

			// Holder for Object at current depth along chain.
			Object current = this;

			// count of the tokens
			int n = st.countTokens() - 1;

			try {
				// Loop through properties in chain.
				for (int i = 0; st.hasMoreTokens(); i++) {
					String currentPropertyName = st.nextToken();

					if (i < n) {
						// This is a getter
						current = invokeProperty(current, currentPropertyName);
					} else {
						// Final property in chain, hence setter
						try {
							// Call setter
							if (current == null) 
								throw new Exception("É preciso criar o objeto no get quando for referenciar um outro Bean. \nEx.: \n public UnidadeBean getUnidade() { \n\tif (unidade == null) unidade = new UnidadeBean(); \n\t return unidade; \n } ");
							PropertyDescriptor pd = new PropertyDescriptor(currentPropertyName, current.getClass());
							pd.getWriteMethod().invoke(current, new Object[] {value});

							return true;
						} catch (Exception e) {

							extraMap.put(attribute.toLowerCase(), value);
							if (this.getClass().equals(Bean.class) == false) {
								return false;
							}
							return true;
						}
					}
				}

				// Return holder Object
				return true;
			} catch (NullPointerException e) {
				// It is very likely that one of the properties returned null. If so, catch the exception and return null.
				return false;
			}
		}
	}
	public <T> T getAttribute(String attribute) {
		if (attribute == null) {
			return null;
		}

		// Split out property on dots ( "person.name.first" -> "person","name","first" -> getPerson().getName().getFirst() )
		StringTokenizer st = new StringTokenizer(attribute, ".");

		if (st.countTokens() == 0) {
			return null;
		}

		// Holder for Object at current depth along chain.
		Object result = this;

		try {
			// Loop through properties in chain.
			while (st.hasMoreTokens()) {
				String currentPropertyName = st.nextToken();

				// Assign to holder the next property in the chain.
				result = invokeProperty(result, currentPropertyName);
			}

			// Return holder Object
			return (T)result;
		} catch (NullPointerException e) {
			//			LOG.warn(e.getMessage(), e);        	
			// It is very likely that one of the properties returned null. If so, catch the exception and return null.
			return (T)extraMap.get(attribute.toLowerCase());
		}
	}


	public static Object invokeProperty(Object obj, String property) {
		if ((property == null) || (property.length() == 0)) {
			return null; // just in case something silly happens.
		}

		Class<?> cls = obj.getClass();
		Object[] oParams = {};
		Class<?>[] cParams = {};

		try {
			// First try object.getProperty()
			Method method = cls.getMethod(createMethodName(GET, property), cParams);

			return method.invoke(obj, oParams);
		} catch (Exception e1) {
			try {
				// First try object.isProperty()
				Method method = cls.getMethod(createMethodName(IS, property), cParams);

				return method.invoke(obj, oParams);
			} catch (Exception e2) {
				try {
					// Now try object.property()
					Method method = cls.getMethod(property, cParams);

					return method.invoke(obj, oParams);
				} catch (Exception e3) {
					try {
						// Now try object.property()
						// Vou ignorar o maiuculo e minusculo aqui... 
						// nao sei se sera uma boa ideia...
						// vamos ver com o tempo :)
						//						Field field = cls.getField(property);
						Field field = DBHelper.getFieldByName(cls, property);
						if (field == null) 
							throw new Exception();

						return field.get(obj);
					} catch (Exception e4) {
						try {

							for (String key : ((Bean)obj).getMap().keySet()) {
								if (key.equalsIgnoreCase(property) == false) continue;
								return ((Bean)obj).extraMap.get(key);
							}
							return null;
						} catch (Exception e5) {
							return null;
						}
					}
				}
			}
		}
	}
	private static String createMethodName(String prefix, String propertyName) {
		return prefix + propertyName.toUpperCase().charAt(0) + propertyName.substring(1);
	}

	/**
	 * Preenche o objeto com todos os atributos do objeto informado.
	 * <b>Não funciona para atributos "filhos" de Bean</b>
	 * @param b
	 */
	public void parse(Bean b) {
		String[] attrArray = getAttributes();

		for (String attr : attrArray) {
			Object attrNew = this.get(attr);
			Object attrDb = b.get(attr);

			//se o atributo atual for um Bean, ele nunca estará nulo
			if ((attrNew == null) && (attrDb != null))
				this.set(attr, attrDb);

		}
	}


	public String[] getAttributes() {
		List<String> attributes = new ArrayList<String>();

		Field[] fields = getClass().getDeclaredFields();
		for (Field f : fields) {
			String key = f.getName();
			Object obj = getAttribute(key);
			if (obj instanceof Log) continue;
			if (key.equalsIgnoreCase("extraMap") == true) continue;
			if (key.equalsIgnoreCase(RNUM)) continue;
			if (key.equalsIgnoreCase(QTD)) continue;
			if (key.equalsIgnoreCase("PL")) continue;
			if (key.equals("QTD")) continue;
			if (key.equals("LINENUMBER")) continue;
			if (key.equalsIgnoreCase("RNUM")) continue;
			if (key.equalsIgnoreCase("GET")) continue;
			if (key.equalsIgnoreCase("IS")) continue;
			if (key.equalsIgnoreCase("attributeToJson")) continue;

			attributes.add(key);
		}
		for (String key : extraMap.keySet()) {
			Object obj = extraMap.get(key);
			if (key.equalsIgnoreCase(RNUM)) continue;
			if (key.equalsIgnoreCase(QTD)) continue;
			if (key.equalsIgnoreCase("PL")) continue;
			if (key.equals("QTD")) continue;
			if (key.equals("LINENUMBER")) continue;
			if (key.equalsIgnoreCase("RNUM")) continue;
			if (key.equalsIgnoreCase("GET")) continue;
			if (key.equalsIgnoreCase("IS")) continue;
			if (key.equalsIgnoreCase("attributeToJson")) continue;

			attributes.add(key);
		}

		return attributes.toArray(new String[attributes.size()]);
	}


	/**
	 * Este metodo esta certo...
	 * mas precisa set mais esperto...
	 * qualquer nivel de bean
	 * tem que terminar!!! 
	 * @return
	 * @throws Exception
	 */
	public String toJson2() throws Exception {
		JSONObject json = vai(null, this);
		return json.toString();
	}


	private JSONObject vai(String _attr, Bean _obj) {
		JSONObject json = new JSONObject();

		String[] attrArray = _obj.getAttributes();
		for (String attr : attrArray) {
			Object obj = (Object)_obj.get(attr);

			if (obj instanceof Object[]) {
				Object[] objArray = (Object[])obj;


				if (objArray.length == 0) {
					JSONArray array = new JSONArray();
					json.put(attr, array);
				} else if (objArray[0] instanceof Bean) {
					JSONArray array = new JSONArray();
					for (Object oo : (Object[])objArray) {
						Bean b = (Bean)oo;
						array.put( vai(_attr, b) );
					}
					json.put(attr, array);
				} else {
					json.put(attr, objArray );
				}

			} else if (obj instanceof Bean) {
				json.put(attr, vai(_attr, (Bean)obj) );
			} else if (obj instanceof Object[]) {

			} else {
				json.put(attr, obj);
			}
		}

		return json;

	}

	public String toJson() throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append( "{" );

		String temp = "";
		Field[] fields = getClass().getDeclaredFields();
		for (Field f : fields) {
			String key = f.getName();
			Object obj = getAttribute(key);
			if (obj instanceof Log) continue;
			if (key.equalsIgnoreCase("extraMap") == true) continue;
			if (key.equalsIgnoreCase(RNUM)) continue;
			if (key.equalsIgnoreCase(QTD)) continue;
			if (key.equals("QTD")) continue;
			if (key.equals("LINENUMBER")) continue;
			if (key.equalsIgnoreCase("RNUM")) continue;
			if (key.equalsIgnoreCase("GET")) continue;
			if (key.equalsIgnoreCase("IS")) continue;
			if (key.equalsIgnoreCase("PL")) continue;
			if (key.equalsIgnoreCase("attributeToJson")) continue;


			if (f.getType().getSuperclass() != null && f.getType().getSuperclass().equals(Bean.class)) {

				sb.append( formatBeanRelationToJson(key, temp, obj) );

			} else {

				boolean pode = this.attributeToJson == null;
				if (this.attributeToJson != null) {
					for (String attribute : this.attributeToJson) {
						if (attribute.equalsIgnoreCase(key) == false) continue;
						pode = true;
						break;
					}
				}

				ColumnDB colAnn = f.getDeclaredAnnotation(ColumnDB.class);
				if ((colAnn != null) && (colAnn.pk() == true)) pode = true;

				if (pode == false) continue;
				sb.append( temp );
				sb.append("\"");
				sb.append(key.toLowerCase());
				sb.append("\"");
				sb.append(":");
				sb.append( formatToJson(obj) );
				sb.append(PL);
			}

			temp = ", ";
		}

		for (String key : extraMap.keySet()) {
			Object obj = extraMap.get(key);
			if (key.equalsIgnoreCase(RNUM)) continue;
			if (key.equalsIgnoreCase(QTD)) continue;
			if (key.equalsIgnoreCase("PL")) continue;
			if (key.equals("QTD")) continue;
			if (key.equals("LINENUMBER")) continue;
			if (key.equalsIgnoreCase("RNUM")) continue;
			if (key.equalsIgnoreCase("GET")) continue;
			if (key.equalsIgnoreCase("IS")) continue;
			if (key.equalsIgnoreCase("attributeToJson")) continue;

			if (obj == null) {
				sb.append( temp );
				sb.append( "\"" + key + "\": null" );
			} else if (obj instanceof Bean) {
				sb.append( formatBeanRelationToJson(key, temp, obj) );
			} else if (obj.getClass().isArray()) {
				sb.append( temp );
				sb.append( "\"" + key + "\"" );
				sb.append( ":[" );

				String tempArray = "";
				for (Object o : (Object[])obj) {
					sb.append( tempArray );
					if(o instanceof Bean) {
						Bean b = (Bean) obj;
						sb.append( b.toJson() );
					}else if(o instanceof String) {
						sb.append("\"" + obj + "\"");
					}else {
						sb.append(o);
					}
					tempArray = ", ";
				}
				sb.append( "]" );
			} else {
				sb.append( temp );
				sb.append("\"");
				sb.append(key.toLowerCase());
				sb.append("\"");
				sb.append(":");
				sb.append( formatToJson(obj) );
				sb.append(PL);
			}

			temp = ", ";
		}


		sb.append( "}" );
		return sb.toString();
	}
	private String formatBeanRelationToJson(String key, String temp, Object obj) throws DeveloperException {
		StringBuilder sb = new StringBuilder();

		Bean b = ((Bean)obj);
		if (b == null)
			throw new DeveloperException("O Bean referente ao property " + key + " esta nulo. Provavelmente o nome do atributo esta diferente do metodo!");
		Field[] fArray = b.getClass().getDeclaredFields();
		for (Field f : fArray) {
			FieldDB fAnn = f.getAnnotation(FieldDB.class);
			Object objRelacionado = b.getAttribute(f.getName());
			if (fAnn == null) continue;
			if (f.getName().equalsIgnoreCase(RNUM)) continue;
			if (f.getName().equalsIgnoreCase(QTD)) continue;
			if (f.getName().equalsIgnoreCase("PL")) continue;
			if (f.getName().equals("QTD")) continue;
			if (f.getName().equals("LINENUMBER")) continue;
			if (f.getName().equalsIgnoreCase("RNUM")) continue;
			if (f.getName().equalsIgnoreCase("GET")) continue;
			if (f.getName().equalsIgnoreCase("IS")) continue;
			if (f.getName().equalsIgnoreCase("attributeToJson")) continue;


			//			String newKey = key + fAnn.name().replaceAll("\\.", "");
			String newKey = key + "." + fAnn.name();

			boolean pode = this.attributeToJson == null;
			if (this.attributeToJson != null) {
				for (String attribute : this.attributeToJson) {
					if (attribute.equalsIgnoreCase(newKey) == false) continue;
					pode = true;
					break;
				}
			}

			ColumnDB colAnn = f.getDeclaredAnnotation(ColumnDB.class);
			if ((colAnn != null) && (colAnn.pk() == true)) {
				pode = true;


				// isso aqui é para nao precisar fazer o refactory em todos os campos de comboBox com bean
				if (newKey.contains(".")) {
					sb.append( temp );
					sb.append("\"");
					sb.append(newKey.toLowerCase().replace(".", ""));
					sb.append("\"");
					sb.append(":");
					sb.append( formatToJson(objRelacionado) );
					sb.append(PL);

					temp = ", ";
				}
			}
			if (pode == false) continue;

			sb.append( temp );
			sb.append("\"");
			sb.append(newKey.toLowerCase());
			sb.append("\"");
			sb.append(":");
			sb.append( formatToJson(objRelacionado) );
			sb.append(PL);

			temp = ", ";
		}
		return sb.toString();
	}
	private String formatToJson(Object obj) {
		String value = null;
		if (obj == null) {
			value = "null";
		} else if (obj instanceof Integer) {
			value = ""+obj;
		} else if (obj instanceof Integer[]) {
			value = "[";
			String temp = "";
			for (Integer x : (Integer[])obj) {
				value += temp;
				value += ""+x;
				temp = ",";
			}
			value += "]";
		} else if (obj instanceof BigDecimal) {
			// TODO isso aqui nao pode ser aqui.. tem q ser responsabilidade de quem for exibir...
			// logo o objeto tem q ser do tipo numerico.
			//			value = ((BigDecimal)obj).toString();

			// TODO qualquer coisa voltar para esse aqui
			//			value = "\"" + FormatUtils.formatBigDecimal(((BigDecimal)obj), 2) + "\"";
			value = MathUtils.round(((BigDecimal)obj), 5).setScale(5).toString();
		} else if (obj instanceof Double) {
			// TODO colocar isso como numerico e nao formatado como String...
			// assim como o BigDecimal!
			value = "\"" + FormatUtils.formatDouble((Double)obj) + "\"";
			//			value = ((Double)obj).toString();
		} else if (obj instanceof Date) {
			value = "\"" + FormatUtils.formatDate( (Date)obj) + "\"";
		} else if (obj instanceof String) {
			//value = "\"" + ((String)obj).replace("\\", "\\\\").replaceAll("\"", "''").replaceAll("\n", "\\\\n").replaceAll("\r", "\\\\r").replaceAll("\t", "\\\\t") + "\"";
			value = "\"" + ((String)obj).replace("\\", "\\\\").replaceAll("\"", "\\\\\"").replaceAll("\n", "\\\\n").replaceAll("\r", "\\\\r").replaceAll("\t", "\\\\t") + "\"";
		} else if (obj.getClass().getSuperclass().equals(Bean.class)) {
			Bean b = (Bean)obj;
			Field[] fieldArray = b.getClass().getDeclaredFields();
			for (Field field : fieldArray) {
				ColumnDB colAnn = field.getDeclaredAnnotation(ColumnDB.class);
				if (colAnn == null) continue;
				if (colAnn.pk() == false) continue;

				value = ""+((Bean)obj).get(field.getName());
				break;
			}
		} else {
			value = "null";
		}

		return value;
	}

	/**
	 * Este metodo converte um Json para o Bean
	 * @param str
	 * @throws Exception
	 */
	public void toObject(String str) throws Exception {
		if (str.startsWith("{") == false) {
			throw new DeveloperException("A string não esta no formato de um JSON: " + SQLUtils.formatPartStringToShow(str, 50));
		}

		JSONObject obj = new JSONObject(str);
		parse("", obj); 
	}

	private void parse(String part, Object xxx) {
		if (xxx instanceof JSONObject) {
			JSONObject j = (JSONObject)xxx;

			for (String key : j.keySet()) {
				Object obj = j.get(key);

				if (obj instanceof JSONObject) {
					parse(part + key, obj);
				} else {
					String temp = key;
					if (StringUtils.hasValue(part)) temp = part + "." + key ;

					this.set(temp, obj);
				}
			}
		} else {
			this.set(part, xxx);
		}

	}

	public void setAttributeToJson(List<String> attributeToJson) {
		this.attributeToJson = attributeToJson.toArray(new String[attributeToJson.size()]);
	}
	public void setAttributeToJson(String ... attributeToJson) {
		this.attributeToJson = attributeToJson;
	}

	/**
	 * Tem que pegar outros caras alem do size do extra
	 * @return
	 */
	public int sizeAttributes() {
		return extraMap.size();
	}

	public boolean hasAttribute(String propertie) {
		boolean result1 = hasField(propertie);
		boolean result2 = hasExtra(propertie);

		return ((result1 == true) || (result2 == true));
	}
	public boolean hasExtra(String propertie) {
		Object x = extraMap.get(propertie);
		if (x != null)
			return true;

		return false;
	}
	public boolean hasField(String propertie) {

		for (Field field : this.getClass().getDeclaredFields()) {
			if (field.getName().equalsIgnoreCase(propertie)) 
				return true;
		}
		return false;
	}

}


