package br.com.dotum.jedi.core.table;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import br.com.dotum.jedi.file.layout.FileMemory;

public class Blob {

	private byte[] data;
	private String name;

	public Blob() {
		super();
	}

	public Blob(byte[] data) {
		super();
		this.data = data;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
	public void loadFromFileMemory(FileMemory fm) throws Exception {
		data = fm.getBytes();
	}

	public void loadFromFile(File file) throws Exception {
		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			throw new Exception("O arquivo é muito grande para leitura");
		}

		byte[] bytes = new byte[(int)length];
		int offset = 0;
		int numRead = 0;
		FileInputStream fis = new FileInputStream(file);
		while (offset < bytes.length && (numRead=fis.read(bytes, offset, bytes.length-offset)) >= 0) {
			offset += numRead;
		}
		fis.close();

		if (offset < bytes.length) {
			throw new Exception("Não foi possível ler o arquivo completamente: "+file.getName());
		}
		
		data = bytes;
	}
	
	public void loadFromBytes(byte[] bytes) throws Exception {
		long length = bytes.length;		
		if (length > Integer.MAX_VALUE){
			throw new Exception("O arquivo é muito grande para leitura");
		}		
		int offset = 0;
		int numRead = 0;
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes); 
		while (offset < length && (numRead=bais.read(bytes, offset, bytes.length-offset)) >= 0) {
			offset += numRead;
		}		
		if (offset < bytes.length) {
			throw new Exception("Não foi possível ler o arquivo completamente:");
		}
		bais.close();
		data = bytes;
	}

	public void loadFromFile(String fileAndPath) throws Exception {
		loadFromFile(new File(fileAndPath));
	}
	
	public void saveToFile(String fileAndPath) throws Exception {
		if ((data == null) && (data.length == 0)) 
			throw new Exception("Não existe nenhum arquivo para ser gravado!");
		FileOutputStream fos = new FileOutputStream(new File(fileAndPath));
		fos.write(data);
		fos.flush();
		fos.close();				
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}




