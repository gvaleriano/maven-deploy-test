package br.com.dotum.jedi.core.table;



public class Clob {

	private String data;

	public Clob() {
		super();
	}

	public Clob(byte[] data) {
		super();
		this.data = new String(data);
	}

	public String getData() {
		return new String(data);
	}

}

