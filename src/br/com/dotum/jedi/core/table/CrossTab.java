package br.com.dotum.jedi.core.table;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.MathUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.StringUtils;


public class CrossTab {

	public static final String COLUMN_DESCRIPTION = "COLUMN_DESCRIPTION";

	public static String[] createCrossTabColumns(List<? extends Bean> list, String columnName) throws DeveloperException {
		List<String> columns = new ArrayList<String>();
		// primeiro: pega as colunas
		// aqui percorre o model (select) e ve quais sao os valores das colunas diferentes
		// 09/2012 10/2012 11/2012 09/2012 10/2012
		// resultado: 09/2012 10/2012 11/2012 (somente tres)
		for (Bean bean : list) {
			Object obj = bean.getAttribute(columnName);
			String temp = parseStr(obj);
			if (columns.contains(temp) == true) continue; 
			if (temp == null) continue; 
			columns.add(temp);
		}
		Collections.sort(columns);

		return columns.toArray(new String[columns.size()]);
	}

	public static String[] createCrossTabLines(List<? extends Bean> list, String lineName) throws DeveloperException {
		List<String> lines = new ArrayList<String>();

		// segundo: pega as linhas
		// aqui percorre o model (select) e ve quais sao os valores das linhas diferentes
		// E S E S E E E S S S A
		// resultado: E S A
		for (Bean bean : list) {
			Object obj = bean.getAttribute(lineName);
			String temp = parseStr(obj);
			if (lines.contains(temp) == true) continue; 
			if (temp == null) continue; 
			lines.add(temp);
		}
		return lines.toArray(new String[lines.size()]);

	}
	public static List<Bean> parseCross(List<? extends Bean> list, String columnName, String lineName, String valueName) throws Exception {
		HashMap<String, Object[]>  map = createCrossTabValues(list, columnName, lineName, valueName);
		
		List<Bean> model = new ArrayList<Bean>();
		for (String key : map.keySet()) {
			Object[] array = map.get(key);
			
			Bean b = new Bean();
			model.add(b);
					
			b.set("Descricao", key);
			for (int i = 0; i < array.length; i++) {
				b.set("Coluna" + (i+1), array[i]);
			}
		}
		
		return model;
	}

	public static HashMap<String, Object[]> createCrossTabValues(List<? extends Bean> list, String columnName, String lineName, String valueName) throws Exception {
		String[] columns = createCrossTabColumns(list, columnName);
		String[] lines = createCrossTabLines(list, lineName);
		HashMap<String, Object[]> values = new HashMap<String, Object[]>();

		// vou ordenar a lista de bean para fazer certo, pode ser que o programador esquecer de enviar a ordem correta
		// ordernar colunas
//		values.put(COLUMN_DESCRIPTION, columns);



		// terceiro: prepara o campo para colocar os valores 
		for (String ser : lines) {
			Object[] valuesArray = values.get(ser);
			if (valuesArray == null) {
				valuesArray = new Object[columns.length];
				values.put(ser, valuesArray);
			}
		}

		for (Bean bean : list) {
			Object columnObj = bean.getAttribute(columnName);
			String column = parseStr(columnObj);			
			Object lineObj = bean.getAttribute(lineName);
			String line = parseStr(lineObj);

			Object value = bean.getAttribute(valueName);

			a:
				for (String ser : lines) {
					Object[] valuesArray = values.get(ser);
					for (int i = 0; i < columns.length; i++) {
						String cat = columns[i];
						if ((column.equals(cat) == true) && (line.equals(ser) == true)) {
							if (value instanceof Double) {
								valuesArray[i] = NumberUtils.coalesce(((Double)valuesArray[i]), 0.0) + ((Double)value);
							} else if (value instanceof BigDecimal) {
								valuesArray[i] = MathUtils.add((BigDecimal)valuesArray[i], (BigDecimal)value);
							}
							
							break a;
						}
					}
				}
		}

		
		return values;
	}

	public static List< Object[] > createCrossTabColumnValues( List<Object[]> model, Integer[] positions) throws SQLException {
		List< Object[] > result = new ArrayList< Object[] >();
		for (Integer position : positions) {
			List<Object> vals = new ArrayList<Object>();
			HashMap<Object, Object> values = new HashMap<Object, Object>();
			for (int i = 0; i < model.size(); i++) {
				Object value = model.get(i)[position];
				if (values.containsKey(value) == false) {
					values.put(value, value);
					vals.add(value);
				}
			}
			result.add( vals.toArray( new Object[ vals.size() ] ));
		}
		return result;		
	}

	public static void showCrossTab(HashMap<String, Object[]> map) {
		StringBuilder sb = new StringBuilder();
		for (String car : map.keySet()) {
			sb.append(StringUtils.fillRight(car + ": ", ' ', 30)); 
			for (Object d : map.get(car)) {
				if (d == null) {
					sb.append(StringUtils.fillLeft("", ' ', 15)); 
				} else {
					sb.append(StringUtils.fillLeft((Double)d, ' ', 15)); 
				}
			}
			sb.append("\n");
		}
	}

	public static String parseStr(Object obj) throws DeveloperException {
		if (obj == null) {
			return null;
		} else if (obj instanceof Date) {
			return FormatUtils.formatDate((Date)obj);
		} else if (obj instanceof Integer) {
			return ""+(Integer)obj;
		} else if (obj instanceof String) {
			return (String)obj;
		}
		throw new DeveloperException("Tipo desconhecido");
	}

}




