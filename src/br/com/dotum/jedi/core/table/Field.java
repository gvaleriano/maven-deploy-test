package br.com.dotum.jedi.core.table;


public class Field {
	private Integer id;
	private String name;
	private String type;
	private Class typeClass;

	public Field() {
		super();
	}
	public Field(Integer id, String name, String typeField, Class typeClass) {
		super();
		this.id = id;
		this.name = name;
		this.type = typeField;
		this.typeClass = typeClass;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Class getTypeClass() {
		return typeClass;
	}
	public void setTypeClass(Class typeClass) {
		this.typeClass = typeClass;
	}
	public String getTypeClassName() {
		if (typeClass == Double.class) {
			return "DECIMAL";
		}
		return typeClass.getSimpleName().toUpperCase();
	}
}

