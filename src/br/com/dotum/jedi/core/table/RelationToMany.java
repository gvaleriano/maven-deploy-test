package br.com.dotum.jedi.core.table;

import br.com.dotum.jedi.core.db.View;



public class RelationToMany implements IRelation {
	
	private String title;
	private String listNameInMaster;
	private View master;
	private View detail;
	// columnas do relations
	// será setada automaticamente
	private String[] masterProperties;
	private String[] detailProperties;
	
	public RelationToMany(String listNameInMaster) {
		super();
		this.listNameInMaster = listNameInMaster;
	}
	public String[] getDetailProperties() {
		return detailProperties;
	}
	public void setDetailProperties(String ... detailProperties) {
		this.detailProperties = detailProperties;
	}
	public String[] getMasterProperties() {
		return masterProperties;
	}
	public void setMasterProperties(String ... masterProperties) {
		this.masterProperties = masterProperties;
	}
	public View getDetail() {
		return detail;
	}
	public void setDetail(View detail) {
		this.detail = detail;
	}
	public void setMaster(View master) {
		this.master = master;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}	
}

