package br.com.dotum.jedi.core.table;



public abstract class Table {

	private String name;
	private String owner;
	private String sigla;

	public Table() {

	}

	public String getName() {
		return name;
	}

	public String getOwner() {
		return owner;
	}

	public String getSigla() {
		return sigla;
	}

}

