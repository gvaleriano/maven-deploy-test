package br.com.dotum.jedi.core.table;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import br.com.dotum.jedi.core.exceptions.DeveloperException;



public class TableField {

	public final static int UNKNOWN = -1;
	public final static int INTEGER = 1;
	public final static int STRING = 2;
	public final static int DATE = 3;
	public final static int DECIMAL = 4;
	public final static int BLOB = 5;
	public final static int BIGINTEGER = 6;
	public final static int BIGDECIMAL = 7;
	public final static int IMAGE = 8;
	public final static int LONG = 9;
	public final static int INTEGER_ARRAY = 10;

	public final static String INTEGERARRAY_DETAILTYPE = "INTEGER[]";
	public final static String INTEGER_DETAILTYPE = "INTEGER";
	public final static String DECIMAL_DETAILTYPE = "DECIMAL";
	public final static String STRING_DETAILTYPE = "STRING";
	public final static String DATE_DETAILTYPE = "DATE";
	public final static String IMAGE_DETAILTYPE = "IMAGE";

	static HashMap<String, Integer> map = new HashMap<String, Integer>();

	static {
		map.put("INTEGER[]", INTEGER_ARRAY);
		map.put("INTEGER", INTEGER);
		map.put("STRING", STRING);
		map.put("DATE", DATE);
		map.put("DECIMAL", DECIMAL);
		map.put("BLOB", BLOB);
		map.put("BIGINTEGER", BIGINTEGER);
		map.put("BIGDECIMAL", BIGDECIMAL);
		map.put("IMAGE", IMAGE);
		map.put("LONG", LONG);
	}

	public static Integer getFieldType(String fieldType) {
		return map.get(fieldType.trim().toUpperCase());
	}

	private String nameBean;
	private String nameColumn;
	private String tableAlias;
	private String nameColumnWithFunction;
	private int type;
	private int size = 0;
	private int maxlength;
	private int precision;
	private boolean primaryKey = false;
	private boolean required = false;
	private boolean visible = true;
	private Table table;

	public TableField(String nameBean, String nameColumn, int type, int maxlength, int precision) {
		this.nameBean = nameBean;
		this.nameColumn = nameColumn;
		this.type = type;
		this.maxlength = maxlength;
		this.precision = precision;
	}

	public TableField(String nameBean, String nameColumn, String tableAlias, int type, int maxlength, int precision) {
		this.nameBean = nameBean;
		this.nameColumn = nameColumn;
		this.tableAlias = tableAlias;
		this.type = type;
		this.maxlength = maxlength;
		this.precision = precision;
	}

	public TableField(String nameBeanColumn, int type, int maxlength, int precision) {
		this.nameBean = nameBeanColumn;
		this.nameColumn = nameBeanColumn;
		this.type = type;
		this.maxlength = maxlength;
		this.precision = precision;		
	}	
	public TableField(String nameBeanColumn) {
		this.nameBean = nameBeanColumn;
	}
	public TableField(String nameBeanColumn, int type) {
		this.type = type;
		this.nameBean = nameBeanColumn;
	}

	public String getTableAlias() {
		return tableAlias;
	}

	public void setTableAlias(String tableAlias) {
		this.tableAlias = tableAlias;
	}

	public String getNameColumn() {
		return nameColumn;
	}

	public void setNameColumn(String nameColumn) {
		this.nameColumn = nameColumn;
	}	

	public String getNameBean() {
		return nameBean;
	}

	public void setNameBean(String nameBean) {
		this.nameBean = nameBean;
	}	

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean isPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public int getMaxlength() {
		return maxlength;
	}

	public void setMaxlength(int maxlength) {
		this.maxlength = maxlength;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}	

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}
	public String getNameColumnWithFunction() {
		return nameColumnWithFunction;
	}
	public void setNameColumnWithFunction(String nameColumnWithFunction) {
		this.nameColumnWithFunction = nameColumnWithFunction;
	}

	public Object getValueOfText(String textValue) {
		if (this.type == INTEGER) {
			return new Integer(textValue);
		} else if (this.type == LONG) {
			return new Long(textValue);
		} else if (this.type == DECIMAL) {
			return new Double(textValue);
		} else if (this.type == BIGDECIMAL) {
			return new BigDecimal(textValue);
		} else {
			return textValue;
		}
	}

	public Class<?> getValueClass() throws DeveloperException {
		if (this.type == INTEGER) return Integer.class;
		if (this.type == LONG) return Long.class;
		if (this.type == STRING) return String.class;
		if (this.type == DATE) return Date.class;
		if (this.type == DECIMAL) return Double.class;

		throw new DeveloperException("O campo "+this.nameColumn+" da tabela "+this.table.getName()+" não tem um datatype conhecido");	
	}
	
	public String getOracleDataType() throws Exception {
		
		String dataTypeLenght = "("+this.maxlength;
		if (this.precision != 0) dataTypeLenght += ", "+this.precision;
		dataTypeLenght += ")";
		
		if (this.type == INTEGER) return "number"+dataTypeLenght;
		if (this.type == LONG) return "number"+dataTypeLenght;
		if (this.type == STRING) return "varchar2"+dataTypeLenght;
		if (this.type == DATE) return "date";
		if (this.type == DECIMAL) return "number"+dataTypeLenght;
		if (this.type == BLOB) return "blob";
		throw new Exception("Tipo desconhecido "+this.type);
	}

}

