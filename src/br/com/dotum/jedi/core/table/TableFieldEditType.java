package br.com.dotum.jedi.core.table;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TableFieldEditType {
    public final static int HIDDENFIELD = 0;
	public final static int TEXTFIELD = 1;
	public final static int INTEGERFIELD = 2;
	public final static int DECIMALFIELD = 3;
	public final static int DATEFIELD = 4;
	public final static int DATERANGEFIELD = 5;
	public final static int TEXTAREAFIELD = 6;
	public final static int CPFFIELD = 8;
	public final static int CNPJFIELD = 9;
	public final static int CEPFIELD = 10;
	public final static int COLORFIELD = 11;
	public final static int CHECKBOXFIELD = 12;
	public final static int PASSWORDFIELD = 13;
	public final static int HOURFIELD = 14;
	public final static int BLOBFIELD = 15;
	public final static int LONGFIELD = 17;
	public final static int TELEFONEFIELD = 18;
	public final static int RADIOTEXTFIELD = 19;
	public final static int EMAILFIELD = 20;
	public final static int FILEUPLOAD = 21;
	public final static int AUTOCOMPLETEFIELD = 23;
	public final static int EDITORFIELD = 24;
	public final static int COMBOBOXDEPENDENCEFIELD = 16;
	public final static int COMBOBOXNUMBERFIELD = 25;	
	public final static int COMBOBOXTEXTFIELD = 26;	
	public final static int COMBOBOXDBFIELD = 28;
	public final static int RADIONUMBERFIELD = 29;
	public final static int COMBOMULTSELECTFIELD = 30;
	public final static int CALCFIELD = 31;
	public final static int COMBOMULTSELECTTEXTFIELD = 32;
	public final static int COMBOMULTSELECTNUMBERFIELD = 33;

	
	public static final List<Field> fields = new ArrayList<Field>();
	static {
		Field field = new Field(HIDDENFIELD, "Campo oculto (HiddenField)", "HIDDENFIELD", String.class);
		fields.add( field );

		field = new Field(TEXTFIELD, "Texto (TextField)", "TEXTFIELD", String.class);
		fields.add( field );

		field = new Field(INTEGERFIELD, "Número (NumberField)", "INTEGERFIELD", Integer.class);
		fields.add( field );

		field = new Field(DECIMALFIELD, "Decimal (DecimalField)", "DECIMALFIELD", Double.class);
		fields.add( field );

		field = new Field(DATEFIELD, "Data (DateField)", "DATEFIELD", Date.class);
		fields.add( field );

		field = new Field(DATERANGEFIELD, "Data (DateRangeField)", "DATERANGEFIELD", Date.class);
		fields.add( field );
		
		field = new Field(TEXTAREAFIELD, "Texto grande (TextAreaField)", "TEXTAREAFIELD", String.class);
		fields.add( field );

		field = new Field(CPFFIELD, "Campo de CPF (CpfField)", "CPFFIELD", String.class);
		fields.add( field );

		field = new Field(CNPJFIELD, "Campo de CNPJ (CnpjField)", "CNPJFIELD", String.class);
		fields.add( field );

		field = new Field(CEPFIELD, "Campo de CEP (CepField)", "CEPFIELD", String.class);
		fields.add( field );

		field = new Field(COLORFIELD, "Cor (ColorField)", "COLORFIELD", String.class);
		fields.add( field );

		field = new Field(CHECKBOXFIELD, "Caixa de seleção (CheckBoxField)", "CHECKBOXFIELD", Integer.class);
		fields.add( field );

		field = new Field(PASSWORDFIELD, "Senha (PasswordField)", "PASSWORDFIELD", String.class);
		fields.add( field );

		field = new Field(HOURFIELD, "Hora (HourField)", "HOURFIELD", String.class);
		fields.add( field );

		field = new Field(BLOBFIELD, "Campo blog (BlogField)", "BLOBFIELD", String.class);
		fields.add( field );

		field = new Field(LONGFIELD, "Número longo (LongField)", "LONGFIELD", Long.class);
		fields.add( field );

		field = new Field(TELEFONEFIELD, "Telefone (TelefoneField)", "TELEFONEFIELD", String.class);
		fields.add( field );

		field = new Field(RADIOTEXTFIELD, "Multipla escolha (RadioTextField)", "RADIOTEXTFIELD", String.class);
		fields.add( field );

		field = new Field(RADIONUMBERFIELD, "Multipla escolha (RadioNumberField)", "RADIONUMBERFIELD", Integer.class);
		fields.add( field );

		field = new Field(EMAILFIELD, "Email (EmailField)", "EMAILFIELD", String.class);
		fields.add( field );

		field = new Field(FILEUPLOAD, "Upload (FileUpload)", "FILEUPLOAD", String.class);
		fields.add( field );

		field = new Field(AUTOCOMPLETEFIELD, "Campo auto completar (AutoCompleteField)", "AUTOCOMPLETEFIELD", Integer.class);
		fields.add( field );

		field = new Field(EDITORFIELD, "Editor de conteudo (EditorField)", "EDITORFIELD", String.class);
		fields.add( field );

		field = new Field(COMBOBOXDEPENDENCEFIELD, "Escolha de uma lista (ComboDependenceField)", "COMBOBOXDEPENDENCEFIELD", Integer.class);
		fields.add( field );

		field = new Field(COMBOBOXNUMBERFIELD, "Escolha de uma lista (ComboNumberField)", "COMBOBOXNUMBERFIELD", Integer.class);
		fields.add( field );

		field = new Field(COMBOBOXTEXTFIELD, "Escolha de uma lista (ComboTextField)", "COMBOBOXTEXTFIELD", String.class);
		fields.add( field );

		field = new Field(COMBOBOXDBFIELD, "Escolha de uma lista (ComboDBField)", "COMBOBOXDBFIELD", Integer.class);
		fields.add( field );

		field = new Field(COMBOMULTSELECTFIELD, "Escolha de uma (ComboMultSelectField)", "COMBOMULTSELECTFIELD", Integer[].class);
		fields.add( field );
		
		field = new Field(CALCFIELD, "Campo de calculo(CalcField)", "CALCFIELD", Double[].class);
		fields.add( field );

		field = new Field(COMBOMULTSELECTTEXTFIELD, "Escolha de uma (ComboMultSelectTextField)", "COMBOMULTSELECTTEXTFIELD", String[].class);
		fields.add( field );
		
		field = new Field(COMBOMULTSELECTNUMBERFIELD, "Escolha de uma (ComboMultSelectNumberField)", "COMBOMULTSELECTNUMBERFIELD", Integer[].class);
		fields.add( field );
	}

	public static Field getFieldById(Integer id) {
		for (Field field : fields) {
			if (field.getId().equals(id) == false) continue;
			return field;
		}
		return null;
	}

	public static Field getFieldByType(String type) {
		for (Field field : TableFieldEditType.fields) {
			if (field.getType().equalsIgnoreCase(type) == false) continue;
			return field;
		}
		return null;
	}
	
	
	
}

