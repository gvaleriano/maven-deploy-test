package br.com.dotum.jedi.core.table;

public class TableFieldMask {

	
	public static enum CharCase {NORMALCASE,UPPERCASE,LOWERCASE};
	
	private TableField field;
	private String label;
	private String hint;
	private int editType;
	private CharCase charCase;
	private boolean required = false;
	private boolean visible = true;
	private boolean enabled = true;
	private boolean filter = false;
	private boolean showInList = false;
	private boolean createWindowCrud = false;
	private String chave;
	
	public TableFieldMask(TableField field, String label, int edittype, CharCase charCase) {
		super();
		this.field = field;
		this.label = label;
		this.editType = edittype;
		this.charCase = charCase;
	}
	
	public TableFieldMask(TableField field, String label, int edittype) {
		this(field, label, edittype, CharCase.NORMALCASE);
	}
	public TableFieldMask(String label, int edittype) {
		this(null, label, edittype, CharCase.NORMALCASE);
	}	
	public TableFieldMask(TableField field, int edittype) {
		this(field, "", edittype);
	}
	public TableField getField() {
		return this.field;
	}
	public void setField(TableField field) {
		this.field = field;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getHint() {
		return hint;
	}
	public void setHint(String hint) {
		this.hint = hint;
	}
	public Boolean isRequired() {
		return required;
	}
	public void setRequired(Boolean required) {
		this.required = required;
	}
	public Boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Boolean isFilter() {
		return filter;
	}
	public void setFilter(Boolean filter) {
		this.filter = filter;
	}
	public int getEditType() {
		return editType;
	}
	public void setEditType(int fieldType) {
		this.editType = fieldType;
	}
	public CharCase getCharCase() {
		return charCase;
	}
	public void setCharCase(CharCase charCase) {
		this.charCase = charCase;
	}
	public boolean isUpperCase() {
		return (this.charCase == CharCase.UPPERCASE);
	}
	public boolean isLowerCase() {
		return (this.charCase == CharCase.LOWERCASE);
	}
	public Boolean isVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public Boolean isShowInList() {
		return showInList;
	}
	public void setShowInList(Boolean showInList) {
		this.showInList = showInList;
	}	
	public String getChave() {
		return chave;
	}
	public void setChave(String chave) {
		this.chave = chave;
	}

	public boolean isCreateWindowCrud() {
		return createWindowCrud;
	}

	public void setCreateWindowCrud(boolean createWindowCrud) {
		this.createWindowCrud = createWindowCrud;
	}
}

