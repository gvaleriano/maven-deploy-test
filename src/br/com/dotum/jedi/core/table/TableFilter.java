package br.com.dotum.jedi.core.table;

import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;



public class TableFilter {

	// ****** CUIDADO ********
	// mexer nesses array pode para de funcionar todas as consultas do sistema
	//
	// para alterar/acrescentar elementos nesses array
	// verifique o metodo getConditionEqual(int fieldtype) (abaixo)
	// se tiver duvida fale com o kuesley
	// se eles tiverem mortos, fale com o keynes
	// se este tambem morreu.. fudeu!

	public static String OPERATOR_AND = "AND";
	public static String OPERATOR_OR = "OR";

	private static String NULO = "Nulo";
	private static String NAONULO = "Nao Nulo";
	private static String IGUAL = "Igual";
	private static String MAIOR = "Maior";
	private static String MENOR = "Menor";
	private static String DIFERENTE = "Diferente";
	private static String MAIORIGUAL = "Maior Igual";
	private static String MENORIGUAL = "Menor Igual";
	private static String CONTEM = "Contém";
	private static String CONTEMNOINICIO = "Contém no Inicio";
	private static String CONTEMNOFIM = "Contém no Fim";
	private static String IN = "Conjunto";
	private static String EXISTS = "Existe";

	private static String[] CONDITION_STRING = new String[] {CONTEM,                     CONTEMNOINICIO,         CONTEMNOFIM,            IGUAL,          DIFERENTE,   NULO,        NAONULO,         IN};
	private static String[] OPERATOR_STRING = new String[] {" like upper('%'||?||'%') ", " like upper(?||'%') ", " like upper('%'||?) ", " = upper(?) ", " <> (?) ",  " is null ", " is not null ", " in "};

	private static String[] CONDITION_STRING_PSQL = new String[] {CONTEM,CONTEMNOINICIO,CONTEMNOFIM,IGUAL,DIFERENTE,NULO,NAONULO,IN};
	private static String[] OPERATOR_STRING_PSQL = new String[] {" ilike '%'||?||'%'"," ilike ?||'%' "," ilike '%'||?"," = ? "," <> ? "," is null "," is not null "," in "};

	private static String[] CONDITION_NUMBER = new String[] {IGUAL,DIFERENTE,MAIOR,MENOR,MAIORIGUAL,MENORIGUAL,NULO,NAONULO,IN,EXISTS};
	private static String[] OPERATOR_NUMBER = new String[] {" = ?"," <> ?"," > ?"," < ?"," >= ?"," <= ?"," is null "," is not null "," in "," exists "};

	private static String[] CONDITION_DATE = new String[] {IGUAL,DIFERENTE,MAIOR,MENOR,MAIORIGUAL,MENORIGUAL,NULO,NAONULO,IN};
	private static String[] OPERATOR_DATE = new String[] {" = ?"," != ?"," > ?"," < ?"," >= ?", " <= ?"," is null "," is not null ", " in "};
	private Object value;
	private TableField field;
	private TableField fieldColumnSourceJoin;
	private TableField fieldColumnTargetJoin;
	private int condition;
	private boolean bind;
	private String group = "default";
	private String nextOperator = "AND";

	public TableFilter(TableField field, int condition) {
		this(field, condition, null, true);
	}

	public TableFilter(TableField field, int condition, Object value) {
		this(field, condition, value, true);
	}

	public TableFilter(TableField field, int condition, Object value, boolean bind) {
		this.field = field;
		this.condition = condition;
		this.value = value;
		this.bind = bind;
	}

	public TableFilter(TableField field, TableField fieldColumnSourceJoin, TableField fieldColumnTargetJoin, Object value) {
		this.field = field;
		this.fieldColumnSourceJoin = fieldColumnSourceJoin;
		this.fieldColumnTargetJoin = fieldColumnTargetJoin;
		this.condition = this.getConditionExists();
		this.value = value;
		this.bind = false;
	}

	public TableFilter(TableField field, TableField fieldColumnSourceJoin, TableField fieldColumnTargetJoin, Object[] value) {
		this.field = field;
		this.fieldColumnSourceJoin = fieldColumnSourceJoin;
		this.fieldColumnTargetJoin = fieldColumnTargetJoin;
		this.condition = this.getConditionExists();
		this.value = value;
		this.bind = false;
	}

	public boolean hasValue() {
		if ((((field.getType() == TableField.INTEGER) || (field.getType() == TableField.DECIMAL) || (field.getType() == TableField.BIGDECIMAL)) && (CONDITION_NUMBER[this.condition].equals(NULO) || CONDITION_NUMBER[this.condition].equals(NAONULO))) ||
				((field.getType() == TableField.STRING) && (CONDITION_STRING[this.condition].equals(NULO) || CONDITION_STRING[this.condition].equals(NAONULO))) || 
				((field.getType() == TableField.DATE) && (CONDITION_DATE[this.condition].equals(NULO) || CONDITION_DATE[this.condition].equals(NAONULO)))) {
			return false;
		} else {
			return true;
		}
	}

	private static int linearSearch(String[] array, String key) {
		for (int i = 0; i < array.length; i++) {
			if (StringUtils.removeAccent(array[i]).equals(StringUtils.removeAccent(key))) return i;
		}
		return -1;
	}


	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente é foda de lembrar isso depois
	public static int getConditionGreaterOrEqual(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:			
			return linearSearch(CONDITION_STRING, MAIORIGUAL);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, MAIORIGUAL);
		case TableField.DECIMAL:
			return linearSearch(CONDITION_NUMBER, MAIORIGUAL);
		case TableField.BIGDECIMAL:
			return linearSearch(CONDITION_NUMBER, MAIORIGUAL);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, MAIORIGUAL);
		default:
			return linearSearch(CONDITION_STRING, MAIORIGUAL);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente é foda de lembrar isso depois
	public static int getConditionGreater(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:         
			return linearSearch(CONDITION_STRING, MAIOR);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, MAIOR);
		case TableField.DECIMAL:
			return linearSearch(CONDITION_NUMBER, MAIOR);
		case TableField.BIGDECIMAL:
			return linearSearch(CONDITION_NUMBER, MAIOR);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, MAIOR);
		default:
			return linearSearch(CONDITION_STRING, MAIOR);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente é foda de lembrar isso depois
	public static int getConditionLessOrEqual(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:
			return linearSearch(CONDITION_STRING, MENORIGUAL);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, MENORIGUAL);
		case TableField.DECIMAL:
			return linearSearch(CONDITION_NUMBER, MENORIGUAL);
		case TableField.BIGDECIMAL:
			return linearSearch(CONDITION_NUMBER, MENORIGUAL);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, MENORIGUAL);
		default:
			return linearSearch(CONDITION_STRING, MENORIGUAL);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente é foda de lembrar isso depois
	public static int getConditionLess(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:
			return linearSearch(CONDITION_STRING, MENOR);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, MENOR);
		case TableField.DECIMAL:
			return linearSearch(CONDITION_NUMBER, MENOR);
		case TableField.BIGDECIMAL:
			return linearSearch(CONDITION_NUMBER, MENOR);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, MENOR);
		default:
			return linearSearch(CONDITION_STRING, MENOR);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente é foda de lembrar isso depois
	public static int getConditionLike(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:			
			return linearSearch(CONDITION_STRING, CONTEM);
		default:
			return linearSearch(CONDITION_STRING, CONTEM);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente é foda de lembrar isso depois
	public static int getConditionIn(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:
			return linearSearch(CONDITION_STRING, IN);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, IN);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, IN);
		default:
			return linearSearch(CONDITION_STRING, IN);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente é foda de lembrar isso depois
	public static int getConditionExists(int fieldtype) {
		switch (fieldtype) {
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, EXISTS);
		default:
			return linearSearch(CONDITION_NUMBER, EXISTS);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente eh foda de lembrar isso depois
	public static int getConditionLikeBegin(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:
			return linearSearch(CONDITION_STRING, CONTEMNOINICIO);
		default:
			return linearSearch(CONDITION_STRING, CONTEMNOINICIO);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente eh foda de lembrar isso depois
	public static int getConditionLikeEnd(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:
			return linearSearch(CONDITION_STRING, CONTEMNOFIM);
		default:
			return linearSearch(CONDITION_STRING, CONTEMNOFIM);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente eh foda de lembrar isso depois
	public static int getConditionEqual(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:
			return linearSearch(CONDITION_STRING, IGUAL);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, IGUAL);
		case TableField.DECIMAL:
			return linearSearch(CONDITION_NUMBER, IGUAL);
		case TableField.BIGDECIMAL:
			return linearSearch(CONDITION_NUMBER, IGUAL);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, IGUAL);
		default:
			return linearSearch(CONDITION_STRING, IGUAL);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// nao mexa nesses numeros abaixo
	// se tiver duvida fale com o kuesley
	// eu anotei isso aqui por que realmente é foda de lembrar isso depois
	public static int getConditionNotEqual(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:			
			return linearSearch(CONDITION_STRING, DIFERENTE);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, DIFERENTE);
		case TableField.DECIMAL:
			return linearSearch(CONDITION_NUMBER, DIFERENTE);
		case TableField.BIGDECIMAL:
			return linearSearch(CONDITION_NUMBER, DIFERENTE);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, DIFERENTE);
		default:
			return linearSearch(CONDITION_STRING, DIFERENTE);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// para o caso de comparacao com nulo
	public static int getConditionIsNull(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:
			return linearSearch(CONDITION_STRING, NULO);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, NULO);
		case TableField.DECIMAL:
			return linearSearch(CONDITION_NUMBER, NULO);
		case TableField.BIGDECIMAL:
			return linearSearch(CONDITION_NUMBER, NULO);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, NULO);
		default:
			return linearSearch(CONDITION_STRING, NULO);
		}
	}

	// esse metodo retorna o elemento do array de operadores
	// para o caso de comparacao com nao nulo
	public static int getConditionIsNotNull(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING:
			return linearSearch(CONDITION_STRING, NAONULO);
		case TableField.INTEGER:
			return linearSearch(CONDITION_NUMBER, NAONULO);
		case TableField.DECIMAL:
			return linearSearch(CONDITION_NUMBER, NAONULO);
		case TableField.BIGDECIMAL:
			return linearSearch(CONDITION_NUMBER, NAONULO);
		case TableField.DATE:
			return linearSearch(CONDITION_DATE, NAONULO);
		default:
			return linearSearch(CONDITION_STRING, NAONULO);
		}
	}

	// CONDICAO NULO
	public static int getConditionIsNullInteger() {
		return getConditionIsNull(TableField.INTEGER);
	}

	public static int getConditionIsNullString() {
		return getConditionIsNull(TableField.STRING);
	}

	public static int getConditionIsNullDate() {
		return getConditionIsNull(TableField.DATE);
	}

	// CONDICAO NAO NULO
	public static int getConditionIsNotNullInteger() {
		return getConditionIsNotNull(TableField.INTEGER);
	}

	public static int getConditionIsNotNullString() {
		return getConditionIsNotNull(TableField.STRING);
	}

	public static int getConditionIsNotNullDate() {
		return getConditionIsNotNull(TableField.DATE);
	}

	// CONDICAO IGUAL
	public static int getConditionEqualInteger() {
		return getConditionEqual(TableField.INTEGER);
	}

	public static int getConditionEqualLong() {
		return getConditionEqual(TableField.INTEGER);
	}


	public static int getConditionEqualDate() {
		return getConditionEqual(TableField.DATE);
	}

	public static int getConditionEqualString() {
		return getConditionEqual(TableField.STRING);
	}

	// CONDICAO MAIOR IGUAL
	public static int getConditionGreaterOrEqualDate() {
		return getConditionGreaterOrEqual(TableField.DATE);
	}

	public static int getConditionGreaterOrEqualInteger() {
		return getConditionGreaterOrEqual(TableField.INTEGER);
	}

	// CONDICAO MAIOR
	public static int getConditionGreaterDate() {
		return getConditionGreater(TableField.DATE);
	}
	public static int getConditionGreaterInteger() {
		return getConditionGreater(TableField.INTEGER);
	}

	// CONDICAO MENOR IGUAL
	public static int getConditionLessOrEqualDate() {
		return getConditionLessOrEqual(TableField.DATE);
	}
	public static int getConditionLessOrEqualInteger() {
		return getConditionLessOrEqual(TableField.INTEGER);
	}

	// CONDICAO MENOR
	public static int getConditionLessDate() {
		return getConditionLess(TableField.DATE);
	}
	public static int getConditionLessInteger() {
		return getConditionLess(TableField.INTEGER);
	}

	// CONDICAO CONTEM
	public static int getConditionLikeString() {
		return getConditionLike(TableField.STRING);
	}

	// CONDICAO IN
	public static int getConditionInString() {
		return getConditionIn(TableField.STRING);
	}
	public static int getConditionInInteger() {
		return getConditionIn(TableField.INTEGER);
	}
	public static int getConditionInDate() {
		return getConditionIn(TableField.DATE);
	}

	// EXISTS
	public static int getConditionExists() {
		return getConditionExists(TableField.INTEGER);
	}

	// CONDICAO CONTEM NO INICIO
	public static int getConditionLikeBeginString() {
		return getConditionLikeBegin(TableField.STRING);
	}

	// CONDICAO CONTEM NO FIM
	public static int getConditionLikeEndString() {
		return getConditionLikeEnd(TableField.STRING);
	}

	// CONDICAO DIFERENTE
	public static int getConditionNotEqualDate() {
		return getConditionNotEqual(TableField.DATE);
	}

	public static int getConditionNotEqualInteger() {
		return getConditionNotEqual(TableField.INTEGER);
	}	

	public static int getConditionNotEqualString() {
		return getConditionNotEqual(TableField.STRING);
	}

	public TableField getField() {
		return this.field;
	}

	public void setField(TableField field) {
		this.field = field;
	}

	public int getCondition() {
		return condition;
	}

	public void setCondition(int condition) {
		this.condition = condition;
	}

	public Object getValue() throws Exception {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public boolean isBind() {
		return bind;
	}

	public void setBind(boolean bind) {
		this.bind = bind;
	}

	public static String[] getFilters(int fieldtype) {
		switch (fieldtype) {
		case TableField.STRING: 
			return CONDITION_STRING;
		case TableField.INTEGER:
			return CONDITION_NUMBER;
		case TableField.DECIMAL:
			return CONDITION_NUMBER;
		case TableField.BIGDECIMAL:
			return CONDITION_NUMBER;
		case TableField.DATE:
			return CONDITION_DATE;
		default:
			return CONDITION_STRING;
		}
	}

	public void setNextOperator(String nextOperator) {
		this.nextOperator = nextOperator;
	}

	public String getNextOperator() {
		return this.nextOperator;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getConditionDataType() {
		if (getField().getType() == TableField.INTEGER) return "I";
		return "S";
	}

	public static TableFilter createTableFilter(TableField field, String dataType, Integer conditionIndex, String valueStr) throws Exception {
		if (dataType.equalsIgnoreCase("S")) {
			return new TableFilter(field, conditionIndex, valueStr);
		} else {
			if (CONDITION_NUMBER[conditionIndex].equalsIgnoreCase(IN) == false) {
				Integer value = FormatUtils.parseInt(valueStr);
				return new TableFilter(field, conditionIndex, value);
			} else {
				return new TableFilter(field, conditionIndex, valueStr, false);
			}
		}
	}

	public static TableFilter createTableFilter(TableField field, String dataType, String condition, String valueStr) throws Exception {
		Integer conditionIndex = -1;
		if (dataType.equals("S")) {
			conditionIndex = linearSearch(CONDITION_STRING, condition);  
		} else {
			conditionIndex = linearSearch(CONDITION_NUMBER, condition);
		}
		return createTableFilter(field, dataType, conditionIndex, valueStr);
	}
}

