package br.com.dotum.jedi.file.layout;



public class ColumnLayout {

	public static final String DIREITA = "D";
	public static final String ESQUERDA = "E";

	public static final int STRING = 1;
	public static final int NUMBER = 2;
	public static final int DATE = 3;
	public static final int DECIMAL = 4;
	public static final int LONG = 5;

	private int posicao;
	private int datatype;
	private String name;
	private int tamanho;
	private int decimais;
	private String preencher;
	private String orientacao;
	private String formato;
	private Object value = null;
	private String method;

	// Construtor generico
	
	private ColumnLayout(String name, int type, int tamanho, int decimais, String preencher, String orientacao, String formato, Object value, String method) {
		this.name = name;
		this.decimais = decimais;
		this.tamanho = tamanho;
		this.preencher = preencher;
		this.orientacao = orientacao;
		this.datatype = type;
		this.formato = formato;
		this.value = value;
		this.method = method;
	}
	
	private ColumnLayout(String name, int type, int tamanho, int decimais, String preencher, String orientacao, Object value, String method) {
		this(name, type, tamanho, decimais, preencher, orientacao, null, value, method);
	}
	
	public static ColumnLayout createColumnLayoutString(String name, int tamanho, String preencher, String orientacao) {
		return new ColumnLayout(name, STRING, tamanho, 0, preencher, orientacao, null, null);
	}
	
	public static ColumnLayout createColumnLayoutString(String name, int tamanho, String preencher, String orientacao, String method) {
		return new ColumnLayout(name, STRING, tamanho, 0, preencher, orientacao, null, method);
	}
	
	public static ColumnLayout createColumnLayoutFixoString(String name, String value) {
		return new ColumnLayout(name, STRING, value.length(), 0, "", ESQUERDA, value, null);
	}
	
	public static ColumnLayout createColumnLayoutBrancos(String name, int tamanho) {
		return new ColumnLayout(name, STRING, tamanho, 0, " ", ESQUERDA, " ", null);
	}
	
	public static ColumnLayout createColumnLayoutFill(String name, String value, int tamanho) {
		return new ColumnLayout(name, STRING, tamanho, 0, value, ESQUERDA, " ", null);
	}
	
	public static ColumnLayout createColumnLayoutFixoString(String name, String value, String method) {
		return new ColumnLayout(name, STRING, value.length(), 0, "", ESQUERDA, value, method);
	}
	
	public static ColumnLayout createColumnLayoutFixoDecimal(String name, int tamanho, int decimais, String preencher, String orientacao, Double value) {
		return new ColumnLayout(name, DECIMAL, tamanho, decimais, preencher, orientacao, value, null);
	}

	public static ColumnLayout createColumnLayoutFixoInteger(String name, int tamanho, String preencher, String orientacao, Integer value) {
		return new ColumnLayout(name, NUMBER, tamanho, 0, preencher, orientacao, value, null);
	}
	
	public static ColumnLayout createColumnLayoutDecimal(String name, int tamanho, int decimais, String preencher, String orientacao) {
		return new ColumnLayout(name, DECIMAL, tamanho, decimais, preencher, orientacao, null, null);
	}
	
	public static ColumnLayout createColumnLayoutDecimal(String name, int tamanho, int decimais, String preencher, String orientacao, String method) {
		return new ColumnLayout(name, DECIMAL, tamanho, decimais, preencher, orientacao, null, method);
	}

	public static ColumnLayout createColumnLayoutInteger(String name, int tamanho, String preencher, String orientacao) {
		return new ColumnLayout(name, NUMBER, tamanho, 0, preencher, orientacao, null, null);
	}
	
	public static ColumnLayout createColumnLayoutInteger(String name, int tamanho, String preencher, String orientacao, String method) {
		return new ColumnLayout(name, NUMBER, tamanho, 0, preencher, orientacao, null, method);
	}

	public static ColumnLayout createColumnLayoutLong(String name, int tamanho, String preencher, String orientacao, String method) {
		return new ColumnLayout(name, LONG, tamanho, 0, preencher, orientacao, null, method);
	}

	public static ColumnLayout createColumnLayoutDate(String name, String formato) {
		return new ColumnLayout(name, DATE, formato.length(), 0, "", ESQUERDA, formato, null, null);
	}
	
	public static ColumnLayout createColumnLayoutDate(String name, String formato, String method) {
		return new ColumnLayout(name, DATE, formato.length(), 0, "", ESQUERDA, formato, null, method);
	}
	
	public static ColumnLayout createColumnLayoutDate(String name, String formato, String value, String method) {
		return new ColumnLayout(name, DATE, formato.length(), 0, "", ESQUERDA, formato, value, method);
	}
	
	// metodos para facilitar a criacao de layouts para retorno
	public static ColumnLayout createColumnLayout(String name, Integer tamanho, Integer datatype, String method) {
		if (datatype.equals(ColumnLayout.DATE)) {
			return createColumnLayoutDate(name, "ddMMyyyy", method);
		} else if (datatype.equals(ColumnLayout.NUMBER)) { 
			return createColumnLayoutInteger(name, tamanho, "0", ESQUERDA, method);
		} else if (datatype.equals(ColumnLayout.LONG)) {
			return createColumnLayoutLong(name, tamanho, "0", ESQUERDA, method);
		} else {
			return createColumnLayoutString(name, tamanho, " ", ESQUERDA, method);
		}
	}
	
	public static ColumnLayout createColumnLayout(String name, Integer tamanho, Integer digitos, Integer datatype, String method) {
		return new ColumnLayout(name, datatype, tamanho, digitos, "", ESQUERDA, null, method);
	}

	public static ColumnLayout createColumnLayout(String name, Integer tamanho, Integer digitos, Integer datatype) {
		return new ColumnLayout(name, datatype, tamanho, digitos, "", ESQUERDA, null, null);
	}
	public static ColumnLayout createColumnLayout(String name, Integer tamanho, Integer datatype) {
		return new ColumnLayout(name, datatype, tamanho, 0, "", ESQUERDA, null, null);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTamanho() {
		return tamanho;
	}
	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}
	public int getDecimais() {
		return decimais;
	}
	public void setDecimais(int decimais) {
		this.decimais = decimais;
	}
	public String getPreencher() {
		return preencher;
	}
	public void setPreencher(String preencher) {
		this.preencher = preencher;
	}
	public String getOrientacao() {
		return orientacao;
	}
	public void setOrientacao(String orientacao) {
		this.orientacao = orientacao;
	}
	public int getDatatype() {
		return datatype;
	}

	public String getDatatypeDesc() {
		switch (datatype) {
		case 1: return "STRING";			
		case 2: return "NUMBER";
		case 3: return "DATE";
		case 4: return "DECIMAL";
		case 5: return "LONG";
		}
		return "NULL";
	}

	public void setDatatype(int datatype) {
		this.datatype = datatype;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public int getPosicao() {
		return posicao;
	}
	public void setPosicao(int posicao) {
		this.posicao = posicao;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
}

