package br.com.dotum.jedi.file.layout;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.StringUtils;


public class ColumnLayoutFormat {

	private static Log LOG = LogFactory.getLog(ColumnLayoutFormat.class);

	public static String TEXT_UPPER = "[A-Z]";
	public static String TEXT_LOWER = "[a-z]";
	public static String TEXT_NORMAL = "[a-Z]";

	public static String FILL_LEFT = "E";
	public static String FILL_RIGHT = "D";

	private String name;
	private Integer size;
	private Integer precision;
	private String separator = "";
	private Object value;
	private String charFill = null;
	private String orientation = "E"; // left
	private String format;

	public String parse() throws Exception {
		StringBuffer sb = new StringBuffer();

		// se for valor nulo tem que dar pau
		if (value == null) {
			throw new Exception("O campo "+this.name+" está nulo para montagem do layout e nao foi assumido um valor padrao no cadastro do layout.");
		}

		if (value instanceof String) {

			if (format == null) format = TEXT_NORMAL; 

			String temp = (String)value;
			if (format.startsWith(TEXT_UPPER)) {
				// [A-Z]{tamanho} // MAIUSCULO COM TAMANHO DE 40
				sb.append(temp.toUpperCase());
			} else if (format.startsWith(TEXT_LOWER)) {
				// [a-z]{tamanho} // minusculo COM TAMANHO DE 40
				sb.append(temp.toLowerCase());
			} else if (format.startsWith(TEXT_NORMAL)) {
				// [a-Z]{tamanho} // minusculo e MAIUSCULO COM TAMANHO DE tamanho
				// nao faz nada
				sb.append(temp);
			} else {
				// nao faz nada
				sb.append(temp);
			}
		}

		if (value instanceof Date) {
			if (format == null) 
				throw new Exception("O campo "+this.name+" está sem formato.");
			Date temp = (Date)value;
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			sb.append(sdf.format(temp));
		}

		if (value instanceof Double) {

			if (precision == null)
				throw new DeveloperException("Para o tipo de dado Double é preciso definir a precision!");

			Double temp = (Double)value;
			Double thousand = new Double("1"+ StringUtils.fillZeroRight("0", precision));
			temp = temp * thousand;
			int decimalPlace = 0;
			BigDecimal bd = new BigDecimal(temp);  
			bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);  
			temp = bd.doubleValue();
			String str = bd.toString();
			if (!getSeparator().equals("")) {
				int p = str.length() - precision - 1 ;
				sb.append(str.substring(0,p+1));
				sb.append(getSeparator());
				sb.append(str.substring(p+1,str.length()));
			} else {
				sb.append(str);
			}			
		}

		if (value instanceof Integer) {
			Integer temp = (Integer)value;
			sb.append(temp.intValue());
		}

		if (value instanceof Long) {
			Long temp = (Long)value;
			sb.append(temp.longValue());
		}

		// se tiver caracter a preencher
		if (charFill != null) {
			int pos = sb.length(); // DIREITA
			if (orientation.equals(FILL_LEFT)) {
				pos = 0;
			}
			int tam = 0;
			if (size == -1) tam = sb.length();
			if (size != -1) tam = size - sb.length();
			for (int i = 0; i < tam; i++) {
				sb.insert(pos, charFill);
			}
		}

		if ((size != -1) && (sb.toString().length() > size)) {
			LOG.warn("O campo "+this.name+" ultrapassou o tamanho informado para formatacao ["+size+"] - Valor: "+sb.toString()+" ["+sb.toString().length()+"]");
			return sb.toString().substring(0, size);
		}
		return sb.toString();
	}

	public ColumnLayoutFormat(Object value, String format, Integer size, String charFill) {
		this.value = value;
		this.format = format;
		this.size = size;
		this.charFill = charFill;
	}

	public ColumnLayoutFormat(Object value, String format, Integer size, String charFill, String orientation) {
		this.value = value;
		this.format = format;
		this.size = size;
		this.charFill = charFill;
		this.orientation = orientation;
	}

	public ColumnLayoutFormat(Object value, Integer size, Integer precision, String charFill, String orientation) {
		this.value = value;
		this.size = size;
		this.precision = precision;
		this.charFill = charFill;
		this.orientation = orientation;
	}

	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getCharFill() {
		return charFill;
	}
	public void setCharFill(String charFill) {
		this.charFill = charFill;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getOrientation() {
		return orientation;
	}
	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public String getSeparator() {
		return separator;
	}
	public void setSeparator(String separator) {
		this.separator = separator;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	

}


