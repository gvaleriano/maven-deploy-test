package br.com.dotum.jedi.file.layout;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.IOUtils;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.FileUtils;


public class FileMemory {
	private String[] rows;
	private StringBuffer buffer;
	private File file;
	private String base64;
	private long size;
	private boolean keepLineBlank = false;
	private String charSet = "UTF-8";
	private String path;
	private String name;
	private String extension;
	private String contentType = "plain/text";

	public FileMemory(File file, boolean keepLineBlank) throws FileNotFoundException, IOException {
		this.file = file;
		this.name = FileUtils.getName(file);
		this.extension = FileUtils.getExtension(file);
		this.contentType = FileUtils.getFileMime(file);
		this.keepLineBlank = keepLineBlank;
		this.size = file.length();


		FileInputStream fis = new FileInputStream(file);
		this.base64 = Base64.encode(FileUtils.getBytes(fis));
		fis.close();
		fis = null;
	}

	public FileMemory() {
	}

	public FileMemory(String file, boolean keepLineBlank) throws FileNotFoundException, IOException {
		this(new File(file), keepLineBlank);
	}

	public FileMemory(String file) throws FileNotFoundException, IOException {
		this(file, false);
	}

	public FileMemory(File file) throws FileNotFoundException, IOException {
		this(file, false);
	}
	public FileMemory(byte[] bytes) throws IOException {
		this.size = bytes.length;
		this.base64 = Base64.encode(bytes);
	}

	public FileMemory(String content, String name) throws IOException {
		this.buffer = new StringBuffer(content);
		setName(name);
		this.base64 = Base64.encode(StringUtils.getBytesUtf8(buffer.toString()));
	}

	public FileMemory(InputStream is) throws IOException {
		byte[] byteArray = FileUtils.getBytes(is);
		this.size = byteArray.length;
		this.base64 = Base64.encode(byteArray);
		is.close();
		is = null;
	}

	public String getCharSet() {
		return charSet;
	}

	public void setCharSet(String charSet) {
		this.charSet = charSet;
	}

	public boolean carregouArquivo() {
		if (file == null) return false;
		return file.exists();
	}

	public String[] getRows() throws Exception {
		if (this.rows != null) return this.rows;

		if (this.file != null) {
			this.rows = readFromFile();
		} else if (this.base64 != null) {
			this.rows = readFromBase64();
		} else if (this.buffer != null) {
			this.rows = readFromBuffer();
		}
		return this.rows;
	}

	public String getText() throws Exception {
		String[] r = getRows();
		StringBuffer sb = new StringBuffer(); 
		for (int i = 0; i < r.length; i++) {
			sb.append(r[i]);
			sb.append("\n");
		}
		return sb.toString();
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getContentType() {
		return contentType;
	}

	private String[] readFromFile() throws IOException {
		List<String> listRows = new ArrayList<String>();
		FileInputStream fis = new FileInputStream(this.file);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fis, this.getCharSet()));

		// le arquivo físico
		String linhaAtual = null;
		while ((linhaAtual = reader.readLine()) != null) {
			if ((keepLineBlank == false) && (linhaAtual.trim().equals(""))) continue;
			listRows.add(linhaAtual);
		}

		fis.close();
		reader.close();

		return listRows.toArray(new String[listRows.size()]);
	}

	private String[] readFromBase64() throws IOException {
		InputStream is = getStream();
		String theString = IOUtils.toString(is, getCharSet());
		is.close();
		String[] result = theString.split("\r\n");
		if (result.length == 1)
			result = theString.split("\n");

		if (result.length == 1)
			result = theString.split("\r");

		return result;
	}

	private String[] readFromBuffer() throws IOException {
		StringReader sreader = new StringReader(buffer.toString());
		BufferedReader reader = new BufferedReader(sreader);

		List<String> listRows = new ArrayList<String>();

		String line = null;
		while ((line = reader.readLine()) != null) {
			if ((keepLineBlank == false) && (line.trim().equals(""))) continue;
			listRows.add(line);
		}
		reader.close();

		return listRows.toArray(new String[listRows.size()]);
	}

	public byte[] getBytes() throws IOException {
		return Base64.decode(this.base64);
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	/**
	 * Use este metodo com moderação.. visto que ele abre um Stream e voce tem que fechar!!!
	 * @return
	 * @throws IOException
	 */
	public InputStream getStream() throws IOException {
		byte[] bArray = Base64.decode(this.base64);
		return new ByteArrayInputStream(bArray);
	}

	public String getName() throws DeveloperException {
		if (name == null)
			throw new DeveloperException("Não foi definidio o nome do arquivo");

		if (extension == null)
			throw new DeveloperException("Não foi definidio a extensão do arquivo");

		return name + "." + extension;
	}
	public String getSimpleName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		if (this.name.contains(".")) {
			this.name = name.substring(0, name.lastIndexOf('.'));
			this.extension = name.substring(name.lastIndexOf('.')+1);
		}
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getExtension() {
		return extension;
	}

	public void createFile() throws Exception {
		String path = getParent();
		if (path == null || path.length() <= 3) path = this.path; 
		if (path == null || path.length() <= 3) path = "";
		FileUtils.createDirectory(path);

		FileUtils.createFileFromBytes(new File(path + "/" + getName()), getBytes());
	}

	public String getParent() {
		String xxx = null;
		if (file != null) xxx = file.getParent() + "/";
		if (path != null) xxx = this.path;
		if (xxx == null) return null;
		
		xxx = xxx.replaceAll("\\\\", "/");
		if (xxx.endsWith("/") == false) xxx += "/";
		
		return xxx;
	}

	public void setPath(String path) {
		this.path = path.replaceAll("\\\\", "/");
	}
	
	public String getBase64() {
		return this.base64;
	}

	public long sizeByte() {
		return this.size;
	}
	public long sizeKByte() {
		return (this.size/1024);
	}
	public long sizeMByte() {
		return (this.size/1024/1024);
	}
	public boolean exists() {
		if (this.file != null)
			return this.file.exists();

		return false;
	}
}