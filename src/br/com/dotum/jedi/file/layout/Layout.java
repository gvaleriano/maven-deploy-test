package br.com.dotum.jedi.file.layout;


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.FileUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;

public abstract class Layout {
	private static Log LOG = LogFactory.getLog(Layout.class);

	
	
	protected List<Bean> headerBeans = new ArrayList<Bean>();
	protected List<Bean> detailBeans = new ArrayList<Bean>();
	protected List<Bean> traillerBeans = new ArrayList<Bean>();
	private String fileNameAndPath;
	private FileMemory fileMemory;
	private String charSet;

	private List<ColumnLayout[]> layoutHeader = new ArrayList<ColumnLayout[]>();
	private List<ColumnLayout[]> layoutDetail = new ArrayList<ColumnLayout[]>();
	private List<ColumnLayout[]> layoutTrailler = new ArrayList<ColumnLayout[]>();

	private List<String> details = new ArrayList<String>();
	private List<String> header = new ArrayList<String>();
	private List<String> trailler = new ArrayList<String>();

	private boolean imported = false;

	public Layout() {
		this("UTF-8");
	}

	public Layout(String charSet) {
		this.charSet = charSet;
	}

	// método abstratos
	protected abstract Bean createHeaderBean();
	protected abstract Bean createDetailBean();
	protected abstract Bean createTraillerBean();

	/*
	 * Metodos para recuperar valores fixos
	 */
	public Object getLayoutHeaderValue(int header_index, int column_index) {
		return layoutHeader.get(header_index)[column_index].getValue();
	}

	public Object getLayoutHeaderValue(int column_index) {
		return layoutHeader.get(0)[column_index].getValue();
	}

	public Object getLayoutDetailValue(int detail_index, int column_index) {
		return layoutDetail.get(detail_index)[column_index].getValue();
	}

	public Object getLayoutDetailValue(int column_index) {
		return layoutDetail.get(0)[column_index].getValue();
	}

	public Object getLayoutTraillerValue(int trailler_index, int column_index) {
		return layoutTrailler.get(trailler_index)[column_index].getValue();
	}

	public Object getLayoutTraillerValue(int column_index) {
		return layoutTrailler.get(0)[column_index].getValue();
	}

	/**
	 * Esse metodo eh chamado para saber a qual layout a linha que esta senha
	 * inserida faz parte
	 * 
	 * Esse metodo faz uma calculozinho besta mas resolvi comentar por que senao
	 * ninguem lembra essa #@@#$#@$
	 * 
	 * ele recebe dois parametros, o primeiro eh a lista de linhas dos dados e a segundo o layout,
	 * e retorna o layout da linha que esta sendo inserida corresponde.
	 * 
	 * @return
	 */
	private ColumnLayout[] getLayout(List<?> rows, List<ColumnLayout[]> layout) {
		int count = rows.size();
		int layout_count = layout.size();
		int mod = count % layout_count;
		return layout.get(mod);
	}



	public String addHeader(LayoutRow row) throws Exception {
		// guardo a linha para ser exportada posteriormente
		// aqui so vou montar e retornar a linha montada
		LayoutRow layoutrow = row;
		ColumnLayout[] layout = getLayout(this.header, layoutHeader);
		Object[] values = layoutrow.getValues().toArray(new Object[layoutrow.getValues().size()]);
		String result = createRow(values, layout, layoutrow.getCharDelimiter());
		this.header.add(result);
		return result;
	}

	public String addDetail(LayoutRow row) throws Exception {
		// guardo a linha para ser exportada posteriormente
		// aqui so vou montar e retornar a linha montada
		LayoutRow layoutrow = row;
		ColumnLayout[] layout = getLayout(this.details, layoutDetail);
		Object[] values = layoutrow.getValues().toArray(new Object[layoutrow.getValues().size()]);
		String result = createRow(values, layout, layoutrow.getCharDelimiter());
		this.details.add(result);
		return result;
	}

	public String addDetail(LayoutRow row, int LayoutIndex) throws Exception {
		// guardo a linha para ser exportada posteriormente
		// aqui so vou montar e retornar a linha montada
		LayoutRow layoutrow = row;
		ColumnLayout[] layout = layoutDetail.get(LayoutIndex);
		Object[] values = layoutrow.getValues().toArray(new Object[layoutrow.getValues().size()]);
		String result = createRow(values, layout, layoutrow.getCharDelimiter());
		this.details.add(result);
		return result;
	}

	public String addTrailler(LayoutRow row) throws Exception {
		// guardo a linha para ser exportada posteriormente
		// aqui so vou montar e retornar a linha montada
		LayoutRow layoutrow = row;
		ColumnLayout[] layout = getLayout(this.trailler, layoutTrailler);
		Object[] values = layoutrow.getValues().toArray(new Object[layoutrow.getValues().size()]);
		String result = createRow(values, layout, layoutrow.getCharDelimiter());
		this.trailler.add(result);
		return result;
	}

	public ColumnLayout getColumnDetailIndex(int detail_index, String name) {
		for (int i = 0; i < layoutDetail.get(detail_index).length; i++) {
			ColumnLayout cl = layoutDetail.get(detail_index)[i];
			if (cl.getName().equalsIgnoreCase(name) == true) {
				return cl;
			}
		}
		return null;
	}

	public List<String> getHeader() {
		return header;
	}

	public void setHeader(List<String> header) {
		this.header = header;
	}

	public List<String> getTrailler() {
		return trailler;
	}

	public void setTrailler(List<String> trailler) {
		this.trailler = trailler;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}

	public void addLayoutHeader(ColumnLayout[] columns) {
		this.layoutHeader.add(columns); 
	}

	public void addLayoutDetail(ColumnLayout[] columns) {
		this.layoutDetail.add(columns); 
	}

	public void addLayoutTrailler(ColumnLayout[] columns) {
		this.layoutTrailler.add(columns); 
	}

	private String createRow(Object[] values, ColumnLayout[] layout, String delimiter) throws Exception {
		StringBuilder sb = new StringBuilder(); 
		for (int i = 0; i < values.length; i++) {
			if (values[i] instanceof LayoutValue)
				sb.append(format(i, layout[i], ((LayoutValue)values[i]).getValue()));			
			else {
				sb.append(format(i, layout[i], values[i]));
			}			
			sb.append(delimiter);
		}
		return sb.toString();		
	}

	private String format(int i, ColumnLayout column, Object value) throws Exception {
		try {	
			
			ColumnLayoutFormat clf = null;
			if (column.getDatatype() == ColumnLayout.NUMBER) {
				// se for valor nulo
				if (value == null) value = new Integer((Integer)column.getValue());
				clf = new ColumnLayoutFormat(value, column.getTamanho(), column.getDecimais(), column.getPreencher(), column.getOrientacao());
			} else if (column.getDatatype() == ColumnLayout.LONG) {
				// se for valor nulo
				if (value == null) value = new Long((Long)column.getValue());
				clf = new ColumnLayoutFormat(value, column.getTamanho(), column.getDecimais(), column.getPreencher(), column.getOrientacao());		
			} else if (column.getDatatype() == ColumnLayout.DECIMAL) {			
				// se for valor nulo
				if (value == null) value = new Double((Double)column.getValue());
				clf = new ColumnLayoutFormat(value, column.getTamanho(), column.getDecimais(), column.getPreencher(), column.getOrientacao());
			} else if (column.getDatatype() == ColumnLayout.STRING) {
				// se for valor nulo
				if (value == null) value = (String)column.getValue();
				clf = new ColumnLayoutFormat(value, column.getFormato(), column.getTamanho(), column.getPreencher(), column.getOrientacao());
			} else if (column.getDatatype() == ColumnLayout.DATE) {
				// se for valor nulo
				if (column.getFormato() == null) LOG.warn("Campo "+column.getName()+" está sem formato.");
				if (value == null) value = column.getValue();
				clf = new ColumnLayoutFormat(value, column.getFormato(), column.getTamanho(), column.getPreencher(), column.getOrientacao());
			}
			clf.setName(column.getName());
			return clf.parse();
		} catch (Exception ex) {
			throw new WarningException("A informação \"[" + i + "] " + column.getName() + "\" esta com o valor "+ (value == null ? "vazio" : value) +"no registro "+ column.getPosicao() +". Verifique!");
		}
	}

	private Object parse(ColumnLayout column, String value) throws DeveloperException, Exception {
		try {
			if (column.getDatatype() == ColumnLayout.NUMBER) {
				Integer result = FormatUtils.parseInt(value);
				return result;
			} else if (column.getDatatype() == ColumnLayout.LONG) {
				Long result = FormatUtils.parseLong(value);
				return result;
			} else if (column.getDatatype() == ColumnLayout.DECIMAL) {
				Double result = FormatUtils.parseDouble(value);
				String dec = "1" + StringUtils.fillZeroLeft("0", column.getDecimais());
				Integer decN = FormatUtils.parseInt(dec);
				result /= decN; 
				return result;
			} else if (column.getDatatype() == ColumnLayout.STRING) {
				return value;			
			} else if (column.getDatatype() == ColumnLayout.DATE) {
				return FormatUtils.parseDateWithFormat(value.trim(), column.getFormato());			
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new WarningException("Não foi possível converter a coluna '"+ column.getName() +"' com o valor '"+value+"' dentro do arquivo. Verifique se esta usando o arquivo correto.");
		}
	}

	private Object[] createValues(ColumnLayout[] columns, String row) throws DeveloperException, Exception {
		Object[] result = new Object[columns.length];
		int beginIndex = 0;
		for (int i = 0; i < columns.length; i++) {
			ColumnLayout cl = columns[i];
			String temp = row.substring(beginIndex, beginIndex+cl.getTamanho());
			LOG.debug("Criando o valor da coluna '" + cl.getName() +"' (valor: " + temp +")");
			Object value = parse(cl, temp);
			result[i] = value;
			beginIndex += cl.getTamanho();
		}
		return result;
	}	

	public String getFileNameAndPath() {
		return fileNameAndPath;
	}

	public void setFileNameAndPath(String fileNameAndPath) {
		this.fileNameAndPath = fileNameAndPath;
	}

	public FileMemory getFileMemory() {
		return fileMemory;
	}

	public void setFileMemory(FileMemory fileMemory) {
		this.fileMemory = fileMemory;
	}

	private String newLine() {
		return "\r\n";		
	}

	public List<Bean> getHeaderBeans() {
		return headerBeans;
	}

	public void setHeaderBeans(List<Bean> headerBeans) {
		this.headerBeans = headerBeans;
	}

	public List<Bean> getDetailBeans() {
		return detailBeans;
	}

	public void setDetailBeans(List<Bean> detailBeans) {
		this.detailBeans = detailBeans;
	}

	public List<Bean> getTraillerBeans() {
		return traillerBeans;
	}

	public void setTraillerBeans(List<Bean> traillerBeans) {
		this.traillerBeans = traillerBeans;
	}

	public String getText() {
		StringBuilder sb = new StringBuilder();
		for (String str : header) {
			sb.append(str+'\r'+'\n');
		}

		// escreve details
		for (String str : details) {
			sb.append(str+'\r'+'\n');
		}

		// escreve trailler
		for (String str : trailler) {
			sb.append(str+'\r'+'\n');
		}
		return sb.toString();
	}

	/*
	 * Com objetos ao invez do ResultSet
	 */
	public void doExport() throws Exception {
		if ( FileUtils.existFile(fileNameAndPath) == false) {
			FileUtils.createDirectory(fileNameAndPath.substring(0, fileNameAndPath.lastIndexOf("/")));
		}

		//		FileUtils.createFileFromString(new File(fileNameAndPath), getText());
		//		PrintWriter writer = new PrintWriter(fileNameAndPath, charSet);
		//		writer.write( getText() );
		//		writer.flush();
		//		writer.close();


		File arq = new File(fileNameAndPath);  
		OutputStream os = new FileOutputStream(arq);
		OutputStreamWriter osw = new OutputStreamWriter(os, charSet);  
		PrintWriter print = new PrintWriter(osw);  
		print.println(getText());  
		print.close();  
		osw.close();  
		os.close();  

	}

	private ColumnLayout getColumnLayoutFromName(ColumnLayout[] list, String name) throws DeveloperException {
		for (int i = 0; i < list.length; i++) {
			if (list[i].getName().equalsIgnoreCase(name)) return list[i];
		}
		throw new DeveloperException("A propriedade "+name+ " não foi encontrada na lista de colunas.");
	}

	/**
	 * Este método carrega o arquivo e instancia benas de acordo com o layout
	 * @throws Exception
	 */
	public void doImport() throws Exception {
		this.imported = true;

		if (fileMemory == null) {
			fileMemory = new FileMemory(fileNameAndPath);
		}
		String[] rows = fileMemory.getRows();

		LOG.debug("==================== Linhas do cabecalho ==================== ");
		if (layoutHeader.size() > 0) {
			int k = 0;
			for (int i = 0; i < rows.length; i++) {
				LOG.debug("Linha " + (i+1));
				String row = rows[i];
				header.add(row);
				ColumnLayout[] columns = layoutHeader.get(k);
				Object[] values = createValues(columns, row);
				Bean bean = null;
				for (int j = 0; j < columns.length; j++) {
					if (StringUtils.hasValue(columns[j].getMethod()) == true) {
						String property = columns[j].getMethod();
						Object value = values[j];
						if (bean == null) {
							bean = this.createHeaderBean();
							this.headerBeans.add(bean);
						}
						bean.setAttribute(property, value);
						LOG.debug("Cabeçalho: Invocando o metodo do atributo " + property + " passando o valor " + value);
					}
				}
				k++;				
				if ( (i+1) == layoutHeader.size()) break;
			}
		}

		int num_rows_trailler = layoutTrailler.size();
		if (layoutTrailler.size() > 0) {
			LOG.debug("==================== Linhas do rodape ==================== ");
			int start = rows.length - num_rows_trailler;
			int k = 0;
			for (int i = start; i < rows.length; i++) {
				LOG.debug("Linha " + (i+1));
				String row = rows[i];
				trailler.add(row);
				ColumnLayout[] columns = layoutTrailler.get(k);
				Object[] values = createValues(columns, row);
				Bean bean = null;
				for (int j = 0; j < columns.length; j++) {
					if (StringUtils.hasValue(columns[j].getMethod()) == true) {
						String property = columns[j].getMethod();
						Object value = values[j];
						if (bean == null) {
							bean = this.createTraillerBean();
							this.traillerBeans.add(bean);
						}
						bean.setAttribute(property, value);
						LOG.debug("Rodapé: Invocando o metodo do atributo " + property + " passando o valor " + value);
					}
				}
				k++;
			}
		}

		// comecar a ler o detail
		int start = layoutHeader.size();
		int end = rows.length - num_rows_trailler;
		int k = 0;
		Bean bean = null;
		LOG.debug("==================== Linhas do corpo ==================== ");
		for (int i = start; i < end; i++) {
			LOG.debug("Linha " + (i+1));
			String row = rows[i];
			details.add(row);

			ColumnLayout[] columns = layoutDetail.get(k);
			Object[] values = createValues(columns, row);
			for (int j = 0; j < columns.length; j++) {
				if (StringUtils.hasValue(columns[j].getMethod()) == true) {
					String property = columns[j].getMethod();
					Object value = values[j];
					if (bean == null) {
						if (k == 0) {
							bean = this.createDetailBean();
							this.detailBeans.add(bean);
						} 
					}
					bean.setAttribute(property, value);
					LOG.debug("Corpo: Invocando o metodo do atributo " + property + " passando o valor " + value);
				}
			}
			k++;
			// TODO ver se isso esta certo, mas pelo que entendi é para pegar a segunda linha do detail
			// antes tava assim: if ( k == layoutTrailler.size()) {
			if ( k >= layoutDetail.size()) { 
				k = 0;
				bean = null;
			}
		}
	}

	public boolean isImported() {
		return imported;
	}

	public void setImported(boolean imported) {
		this.imported = imported;
	}

	public String getCharSet() {
		return charSet;
	}

	public void setCharSet(String charSet) {
		this.charSet = charSet;
	}

}