package br.com.dotum.jedi.file.layout;


import java.util.ArrayList;
import java.util.List;

public class LayoutRow {
	
	private String charDelimiter = "";
	private List<LayoutValue> values = new ArrayList<LayoutValue>();
	
	public void addValue(Object value) {
		values.add(new LayoutValue(value));
	}
	
	public List<LayoutValue> getValues() {
		return this.values;
	}
	
	public LayoutValue getValue(int index) {
		return this.values.get(index);
	}

	public String getCharDelimiter() {
		return charDelimiter;
	}

	public void setCharDelimiter(String charDelimiter) {
		this.charDelimiter = charDelimiter;
	}	
}


