package br.com.dotum.jedi.file.layout;


public class LayoutValue {
	
	private Object value;
	
	public LayoutValue(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}


