package br.com.dotum.jedi.gen;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.PropertiesUtils;


public class ColumnC {

	public static List<ColumnC> list = new ArrayList<ColumnC>();

	static {
		try {
			String pack = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
			Class<?> classe = Class.forName(pack + ".control.MapColumn");
			AbstractColumnMap acm = (AbstractColumnMap)classe.newInstance();
			list.addAll( acm.createList() );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ColumnC getColumnByName(String table, String name) throws DeveloperException {
		if (table != null) {
			for (ColumnC column : list) {
				if (column.getName().equalsIgnoreCase(table + "_" + name) == false) continue;
				return column;
			}
		}

		for (ColumnC column : list) {
			if (column.getName().equalsIgnoreCase(name) == false) continue;
			return column;
		}

		throw new DeveloperException("Falta mapear a coluna " + name);
	}


	private String name;
	private String javaType;
	private String atribute;
	private String label;

	private String fieldType;
	private String chave;
	private String tooltip;

	public ColumnC(String name, String javaType, String atribute, String label, String fieldType, String chave, String tooltip) {
		super();
		this.name = name;
		this.javaType = javaType;
		this.atribute = atribute;
		this.label = label;
		this.fieldType = fieldType;
		this.chave = chave;
		this.tooltip = tooltip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getAtribute() {
		return atribute;
	}

	public void setAtribute(String atribute) {
		this.atribute = atribute;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

}



