package br.com.dotum.jedi.gen;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

public class DicionarioGen {

	private static List<String> nDict = new ArrayList<String>();
	private static List<String> oDict = new ArrayList<String>();
	public static HashMap<String, String> tableMap = new HashMap<String, String>();

	static {

		// montando a lista de dicionario
		// todas as palavras do dicionario portugues
		oDict.add("0");
		oDict.add("1");
		oDict.add("2");
		oDict.add("3");
		oDict.add("4");
		oDict.add("5");
		oDict.add("6");
		oDict.add("7");
		oDict.add("8");
		oDict.add("9");
		oDict.add("Árvore");
		oDict.add("Área");
		oDict.add("Zona");
		oDict.add("Vínculo");
		oDict.add("Visita");
		oDict.add("Viagem");
		oDict.add("Verbetes");
		oDict.add("Verbete");
		oDict.add("Valor");
		oDict.add("Validade");
		oDict.add("Utilização");
		oDict.add("Utilizado");
		oDict.add("Util");				//sigla
		oDict.add("Usuário");
		oDict.add("Usa");				//sigla
		oDict.add("Url");	
		oDict.add("Update");
		oDict.add("Unitário");
		oDict.add("Unidade");
		oDict.add("Ultimos");
		oDict.add("Ultima");
		oDict.add("Título");
		oDict.add("Trabalho");
		oDict.add("Trabalhista");
		oDict.add("Total");
		oDict.add("Tipo");
		oDict.add("Texto");
		oDict.add("Tempo");
		oDict.add("Temp");	
		oDict.add("Telefone");
		oDict.add("Taxonomia");
		oDict.add("Taxa");
		oDict.add("Tarifário");
		oDict.add("Tamanho");
		oDict.add("Tag");				//sigla
		oDict.add("Série");
		oDict.add("Superior");
		oDict.add("Subsc");				//sigla
		oDict.add("Submodalidade");
		oDict.add("Sorteio");
		oDict.add("Solicitação");
		oDict.add("Social");
		oDict.add("Situação");
		oDict.add("Site");
		oDict.add("Sistema");
		oDict.add("SisBr");
		oDict.add("Sigla");
		oDict.add("Seção");
		oDict.add("Sexo");
		oDict.add("Separador");
		oDict.add("Senha");
		oDict.add("Seletivo");
		oDict.add("Seis");
		oDict.add("Saída");
		oDict.add("Satisfação");
		oDict.add("Saldo");
		oDict.add("Rural");
		oDict.add("Risco");
		oDict.add("Rg");
		oDict.add("Reunião");
		oDict.add("Retrato");
		oDict.add("Retorno");
		oDict.add("Resultado");
		oDict.add("Restrição");
		oDict.add("Resposta");
		oDict.add("Responsável");
		oDict.add("Responsabilidade");
		oDict.add("Respondido");
		oDict.add("Respondeu");
		oDict.add("Resp");				//sigla
		oDict.add("Residência");
		oDict.add("Requisitos");
		oDict.add("Remetente");
		oDict.add("Relatório");
		oDict.add("Relator");
		oDict.add("Relacionamento");
		oDict.add("Regularização");
		oDict.add("Registro");
		oDict.add("Rede");
		oDict.add("Redação");
		oDict.add("Rec");				//sigla
		oDict.add("Realizado");
		oDict.add("Realacu");			//sigla
		oDict.add("Real");
		oDict.add("Raça");
		oDict.add("Razão");
		oDict.add("Rateio");
		oDict.add("Ramal");
		oDict.add("Quilometro");
		oDict.add("Questões");	
		oDict.add("Questão");
		oDict.add("Quebra");
		oDict.add("Quantidade");
		oDict.add("Qtde");				//sigla
		oDict.add("Qtd");				//sigla
		oDict.add("Qt");				//sigla
		oDict.add("Público");
		oDict.add("Própria");
		oDict.add("Prêmio");
		oDict.add("Prévia");	
		oDict.add("Pré");
		oDict.add("Provisão");
		oDict.add("Provas");
		oDict.add("Prova");
		oDict.add("Protesto");
		oDict.add("Proprietário");
		oDict.add("Proposta");
		oDict.add("Proponente");
		oDict.add("Prop");				//sigla
		oDict.add("Projeto");
		oDict.add("Produtos");
		oDict.add("Produto");
		oDict.add("Prod");				//sigla
		oDict.add("Processo");
		oDict.add("Processamento");
		oDict.add("Processadas");
		oDict.add("Prioridade");
		oDict.add("Principal");
		oDict.add("Primeira");
		oDict.add("Prev");				//sigla
		oDict.add("Prestação");
		oDict.add("Prest");				//sigla
		oDict.add("Preferência");
		oDict.add("Prcata");
		oDict.add("Prc");				//sigla
		oDict.add("Prazo");
		oDict.add("Pra");				//sigla
		oDict.add("Possui");
		oDict.add("Posacu");			//sigla
		oDict.add("Planejamento");
		oDict.add("Pis");
		oDict.add("Pgto");				//sigla
		oDict.add("Pessoa");
		oDict.add("Pesquisa");
		oDict.add("Peso");
		oDict.add("Perspectiva");
		oDict.add("Periodo");
		oDict.add("Pergunta");
		oDict.add("Perfil");
		oDict.add("Perf");				//sigla
		oDict.add("Percentual");
		oDict.add("Perc");				//sigla
		oDict.add("Penhor");
		oDict.add("Pendência");
		oDict.add("Pendente");
		oDict.add("Passa");
		oDict.add("Pasep");
		oDict.add("Parâmetro");
		oDict.add("Participa");
		oDict.add("Parente");
		oDict.add("Pai");
		oDict.add("Pagamento");
		oDict.add("Padrão");
		oDict.add("Pac");
		oDict.add("Pa");				//sigla
		oDict.add("Orçado");
		oDict.add("Origem");
		oDict.add("Orgão");
		oDict.add("Ordem");
		oDict.add("Operação");
		oDict.add("Ocorrência");
		oDict.add("Observações");
		oDict.add("Observação");
		oDict.add("Obrigatório");
		oDict.add("Obj");				//sigla
		oDict.add("Números");
		oDict.add("Número");
		oDict.add("Nível");
		oDict.add("Níveis");		
		oDict.add("Num");				//sigla
		oDict.add("Nota");
		oDict.add("Nome");
		oDict.add("Nis");				//sigla
		oDict.add("Nfe");
		oDict.add("Necessidade");
		oDict.add("Natureza");
		oDict.add("Naturalização");
		oDict.add("Natural");
		oDict.add("Nascimento");
		oDict.add("Name");			
		oDict.add("Nacionalidade");
		oDict.add("Mêses");
		oDict.add("Mês");
		oDict.add("Método");
		oDict.add("Média");
		oDict.add("Mãe");
		oDict.add("Município");
		oDict.add("Mult");				//sigla
		oDict.add("Mudança");
		oDict.add("Msg");				//sigla
		oDict.add("Movomento");			//errada
		oDict.add("Movimento");
		oDict.add("Movimentação");
		oDict.add("Mov");				//sigla
		oDict.add("Mostra");
		oDict.add("Modalidade");
		oDict.add("Meta");
		oDict.add("Mensal");
		oDict.add("Mensagem");
		oDict.add("Matrícula");
		oDict.add("Matriz");
		oDict.add("Marca");
		oDict.add("Mapa");
		oDict.add("Mandatório");
		oDict.add("Lote");
		oDict.add("Lotação");
		oDict.add("Logradouro");
		oDict.add("Login");
		oDict.add("Log");
		oDict.add("Local");
		oDict.add("Linha");
		oDict.add("Limite");
		oDict.add("Ligações");
		oDict.add("Ligação");
		oDict.add("Liberado");
		oDict.add("Leitor");
		oDict.add("Layout");
		oDict.add("Lançamento");
		oDict.add("Lanc");				//sigla
		oDict.add("Justificativa");
		oDict.add("Juros");
		oDict.add("Juridica");
		oDict.add("Job");				
		oDict.add("Item");
		oDict.add("Isento");
		oDict.add("Isc");				//sigla
		oDict.add("Início");
		oDict.add("Interno");
		oDict.add("Integral");
		oDict.add("Instituição");
		oDict.add("Insert");
		oDict.add("Inscrever");
		oDict.add("Inicio");
		oDict.add("Inicial");
		oDict.add("Info");				//sigla
		oDict.add("Indicação");
		oDict.add("Indicadores");		
		oDict.add("Indicador");
		oDict.add("Index");
		oDict.add("Inclusão");		
		oDict.add("Importação");
		oDict.add("Imigração");
		oDict.add("Idioma");		
		oDict.add("Identifição");
		oDict.add("Identificação");
		oDict.add("Idade");
		oDict.add("Id"); 
		oDict.add("Icms");
		oDict.add("Ict");				//sigla
		oDict.add("Hora");
		oDict.add("Histórico");
		oDict.add("Grupo");
		oDict.add("Gri");				//sigla
		oDict.add("Grau");
		oDict.add("Graduação");
		oDict.add("Gps");
		oDict.add("Gestão");
		oDict.add("Gestor");
		oDict.add("Gerente");
		oDict.add("Gerencial");
		oDict.add("Gerenciais");
		oDict.add("Gasto");
		oDict.add("Garantia");
		oDict.add("Ganhador");
		oDict.add("Física");
		oDict.add("Função");
		oDict.add("Frete");
		oDict.add("Fornecedor");
		oDict.add("Forma");
		oDict.add("Fixo");
		oDict.add("Financeira");
		oDict.add("Finalização");
		oDict.add("Finalizado");
		oDict.add("Final");
		oDict.add("Fim");
		oDict.add("Filtro");
		oDict.add("File");
		oDict.add("Fila");
		oDict.add("Ficha");
		oDict.add("Feriado");
		oDict.add("Federação");
		oDict.add("Fax");
		oDict.add("Favorecido");
		oDict.add("Fase");
		oDict.add("Fantasia");
		oDict.add("Externo");
		oDict.add("Exprg");
		oDict.add("Exportado");
		oDict.add("Exibição");
		oDict.add("Exerce");
		oDict.add("Evento");
		oDict.add("Estrutura");
		oDict.add("Estrageiro");
		oDict.add("Estadual");
		oDict.add("Estado");
		oDict.add("Esperadas");
		oDict.add("Específico");
		oDict.add("Especial");
		oDict.add("Escolaridade");
		oDict.add("Entrevista");
		oDict.add("Entregue");
		oDict.add("Entrega");
		oDict.add("Entidade");
		oDict.add("Endereço");
		oDict.add("Encerramento");
		oDict.add("Empresa");
		oDict.add("Emissão");
		oDict.add("Emissor");
		oDict.add("Email");
		oDict.add("Eleitoral");
		oDict.add("Econômico");
		oDict.add("Economica");
		oDict.add("Documento");
		oDict.add("Dispacum");			//sigla
		oDict.add("Disp");				//sigla
		oDict.add("Disciplina");
		oDict.add("Dip");				//sigla
		oDict.add("Dinâmica");
		oDict.add("Digito");
		oDict.add("Digitador");
		oDict.add("Dica");
		oDict.add("Dias");
		oDict.add("Devolver");
		oDict.add("Devolução");
		oDict.add("Devedor");
		oDict.add("Destino");
		oDict.add("Destinatário");
		oDict.add("Despesa");
		oDict.add("Descrição");
		oDict.add("Desc");				//sigla
		oDict.add("Deposito");
		oDict.add("Depentes");			// ver se ta certio 
		oDict.add("Dependente");
		oDict.add("Deficiência");
		oDict.add("Default");
		oDict.add("Declaração");
		oDict.add("Decimal");
		oDict.add("Ddd");				//sigla
		oDict.add("Date");
		oDict.add("Data");
		oDict.add("Dados");
		oDict.add("Dado");
		oDict.add("Código");
		oDict.add("Cédula");
		oDict.add("Custo");
		oDict.add("Custas");
		oDict.add("Cursos");
		oDict.add("Curso2");				//sigla
		oDict.add("Curso");
		oDict.add("Cursando");
		oDict.add("Curriculo");
		oDict.add("Ctps");				//sigla
		oDict.add("Crédito");
		oDict.add("Cronograma");
		oDict.add("Cron");				//sigla
		oDict.add("Critério");
		oDict.add("Cpf");
		oDict.add("Cotação");
		oDict.add("Corrente");
		oDict.add("Cor");
		oDict.add("Cooperado");
		oDict.add("Convite");
		oDict.add("Contrato");
		oDict.add("Contato");
		oDict.add("Contas");
		oDict.add("Conta");
		oDict.add("Constituição");
		oDict.add("Consistência");
		oDict.add("Consg");				//sigla
		oDict.add("Concluído");
		oDict.add("Conclusão");
		oDict.add("Compra");
		oDict.add("Componente");
		oDict.add("Completo");
		oDict.add("Complemento");
		oDict.add("Competência");
		oDict.add("Compensação");
		oDict.add("Comitê");
		oDict.add("Comercial");
		oDict.add("Comentário");
		oDict.add("Column");
		oDict.add("Colaborador");
		oDict.add("Colab"); 				//sigla
		oDict.add("Coare");				//sigla
		oDict.add("Cnpj");
		oDict.add("Cnh");		
		oDict.add("Cnae");				//sigla
		oDict.add("Cliente");
		oDict.add("Civil");
		oDict.add("Cidade");
		oDict.add("Cheque");
		oDict.add("Certo");
		oDict.add("Certa");
		oDict.add("Cep");
		oDict.add("Celular");
		oDict.add("Cede");
		oDict.add("Categoria");
		oDict.add("Carteira");
		oDict.add("Carro");
		oDict.add("Cargo");
		oDict.add("Captação");
		oDict.add("Capital");
		oDict.add("Candidatos");
		oDict.add("Cancelamento");
		oDict.add("Cadastro");
		oDict.add("Bola");
		oDict.add("Bloqueio");
		oDict.add("Bean"); 				//sigla
		oDict.add("Bancários");
		oDict.add("Bancário");
		oDict.add("Banco");
		oDict.add("Baixa");
		oDict.add("Bairro");
		oDict.add("Ação");
		oDict.add("Avalista");
		oDict.add("Avaliada");
		oDict.add("Autorização");
		oDict.add("Auditoria");
		oDict.add("Atualização");
		oDict.add("Atual");
		oDict.add("Atributo");
		oDict.add("Ativo");
		oDict.add("Atividade");
		oDict.add("Atendimento");
		oDict.add("Ata");
		oDict.add("Assunto");
		oDict.add("Associação");
		oDict.add("Assinaturas");
		oDict.add("Arquivo");
		oDict.add("Aprovador");
		oDict.add("Aprendizado");		
		oDict.add("Aplicativo");
		oDict.add("Aplicacao");
		oDict.add("Apelido");
		oDict.add("Anônimo");
		oDict.add("Anexo");
		oDict.add("Analista");
		oDict.add("Alterou");
		oDict.add("Alternativo");
		oDict.add("Alternativa");
		oDict.add("Alteração");
		oDict.add("Alterado");
		oDict.add("Altera");
		oDict.add("Alinea");
		oDict.add("Agência");
		oDict.add("Agendamento");
		oDict.add("Admissão");
		oDict.add("Adevacu");		    //sigla	
		oDict.add("Action");
		oDict.add("Acompanhamento");
		oDict.add("Acesso");
		oDict.add("About");		
		oDict.add("Abertura");


		
		
		
		// montando o mapeamento que o usuario fez em relacao as tabelas
		// tabela e sigla
		try {
			String pack = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
			Class classe = Class.forName(pack + ".control.MapTable");
			AbstractTableMap acm = (AbstractTableMap)classe.newInstance();
			// TODO voltar isso aqui
//			tableMap.putAll( acm.createList() );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getLabel(String str) throws Exception {
		String[] parts = getTokenJedi(str).split(" ");

		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < parts.length; j++) {
			String part = parts[j];
			for (int i = 0; i < nDict.size(); i++) {
				String word = nDict.get(i);
				if (word.equals(part) == false) continue;

				sb.append( oDict.get(i) );
				sb.append( " " );
			}
		}

		return sb.toString().trim();
	}

	public String getJavaUpper(String str) throws Exception {
		String[] parts = getTokenJedi(str).split(" ");
		String r = ""; 
		for (int i = 0; i < parts.length; i++) {
			String part = parts[i];
			r += upperFirstLetter(part);
		}

		return r.trim();
	}
	public String getJavaLower(String str) throws Exception {
		String[] parts = getTokenJedi(str).split(" ");
		String r = parts[0]; 
		for (int i = 1; i < parts.length; i++) {
			String part = parts[i];
			r += upperFirstLetter(part);
		}

		return r.trim();
	}

	private final String getTokenJedi(String str) throws Exception {
		int qtd = StringUtils.occurrencesCount(str, "_");
		
		String result = null; 
		if (qtd == 0) {
			result = token(str);
			
			qtd += (StringUtils.occurrencesCount(result, " ")-1);
		} else if (qtd == 1) {
			str = str.substring(str.indexOf("_")+1);
			result = token(str);

			qtd += (StringUtils.occurrencesCount(result, " ")-1);
		} else if (qtd == 2) {
			
			int pos1 = str.indexOf("_");
			int pos2 = str.lastIndexOf("_");
			str = str.substring(pos1 + 1, pos2);

			result = token(str);
			qtd += (StringUtils.occurrencesCount(result, " ")-1);
		}
		
		if ((StringUtils.hasValue(result) == false) || (result.length() < str.length()))
			throw new DeveloperException("Desconhecido \""+ str +"\". oDict.add(\""+ str +"\");");

		return result;
	}
	private final static String upperFirstLetter(String value) {
		String result = "";
		result = value.substring(0, 1).toUpperCase();
		result += value.substring(1);

		return result;
	}
	private final static String token(String str) {
		str = StringUtils.removeAccent(str.toLowerCase());

		StringBuilder ret = new StringBuilder();
		if (nDict.size() == 0) {
			Iterator iter = oDict.iterator();
			while (iter.hasNext()) {
				String x = iter.next().toString();
				nDict.add( StringUtils.removeAccent(x.toLowerCase()) );
			}
		}

		if (nDict.contains(str)) {
			return str;
		}

		int i, j, n;
		i = j = n = 0;

		while(j <= str.length()){
			String pre = str.substring(n, j);
			if(nDict.contains(pre)) {
				ret.append(pre);
				ret.append(" ");

				n = j;
			}
			// backtrack here
			if(j == str.length() && i < n) {
				i = j = n;
			}
			j++;
		}
		return ret.toString();
	}


	public String getTableBySigla(String sigla) throws Exception {
		for (String table : tableMap.keySet()) {
			if (tableMap.get(table).equals(sigla) == false) continue;
			return table;
		}

		throw new Exception("Nao foi mapeado a tabela com a sigla: " + sigla);
	}
	public String getTableByName(String name) throws Exception {
		String result = tableMap.get(name);

		if (result == null)
			throw new Exception("Nao foi mapeado a tabela com nome: " + name);

		return name;
	}
	public String getSiglaByName(String name) throws Exception {
		String result = tableMap.get(name);

		if (result == null)
			throw new Exception("Nao foi mapeado a tabela com nome: " + name);

		return result.trim();
	}
}

