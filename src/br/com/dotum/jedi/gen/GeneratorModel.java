package br.com.dotum.jedi.gen;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.ConnectionDB;
import br.com.dotum.jedi.core.db.DBHelper;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.TypeDB;
import br.com.dotum.jedi.core.db.bean.PessoaBean;
import br.com.dotum.jedi.core.db.bean.PessoaTarefaBean;
import br.com.dotum.jedi.core.db.bean.UnidadeBean;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.gen.AbstractTableMap.TypePKEnum;
import br.com.dotum.jedi.util.ClassUtils;
import br.com.dotum.jedi.util.FileUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

public class GeneratorModel {

	public static final String TAB = "\t";
	public static final String PL = "\n";
	public static final String TERMINAL = "/*dotum*/";

	public static void genBean(String pathProject, TransactionDB trans, String filter) throws Exception {
		//		trans.createFunction();

		String path = getPathDotumModel(pathProject);

		List<TableMD> list = selectTable(trans, filter);
		StringBuilder fkToCreate = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			TableMD t = list.get(i);
			TableC tableC = TableC.getTableByName(t.getTableName());
			List<ConstraintMD> constraintList = selectConstraint(trans, t.getTableName());

			StringBuilder sb = new StringBuilder();
			sb.append("package "+ TransactionDB.PACKAGE_BEAN +";" + PL);
			sb.append("" + PL);

			sb.append("import java.io.Serializable;" + PL);
			sb.append("import java.util.ArrayList;" + PL);
			sb.append("import java.util.Arrays;" + PL);
			sb.append("import java.util.Date;" + PL);
			sb.append("import java.util.List;" + PL);
			sb.append("import java.math.BigDecimal;" + PL);

			sb.append("import br.com.dotum.jedi.core.table.Blob;" + PL);
			sb.append("import br.com.dotum.jedi.core.table.Bean;" + PL);
			sb.append("import br.com.dotum.jedi.core.table.TableFieldEditType;" + PL);
			sb.append("import br.com.dotum.jedi.core.db.AbstractTableEnum;" + PL);
			sb.append("import br.com.dotum.jedi.core.db.TableDB;" + PL);
			sb.append("import br.com.dotum.jedi.core.db.ColumnDB;" + PL);
			sb.append("import br.com.dotum.jedi.core.db.FieldDB;" + PL);
			sb.append("" + PL);


			String sequenceName = "";
			if (tableC.getTypePK() == TypePKEnum.SEQUENCE) sequenceName = ", sequence=\"s"+ tableC.getSigla() +"_id\"";
			String auditing = "";
			if (tableC.getAuditing()) auditing = ", auditing=true";


			sb.append("@TableDB(label=\""+ tableC.getLabel() +"\", schema=\"sys\", name=\""+ t.getTableName() +"\", acronym=\""+ tableC.getSigla() +"\", typePk=\""+ tableC.getTypePK().getValue() +"\""+ sequenceName +""+ auditing +")" + PL);			
			sb.append("public class "+ tableC.getClassName() +"Bean extends Bean {" + PL);
			sb.append("" + PL);


			//
			// ATRIBUTOS
			//
			List<String> pkList = new ArrayList<String>();
			for (ColumnMD c : t.getColumnList()) {
				ColumnC column = ColumnC.getColumnByName(c.getColumnSigla(), c.getJavaAtributo());
				String fieldType = getFieldType(tableC, c, column); 
				String javaType = getJavaType(tableC, c, column);
				String javaAtribute = getJavaAttribute(tableC, c, column);
				String chave = "";
				if (column.getChave() != null) chave = ", chave=\""+ column.getChave() +"\"";
				if ((StringUtils.hasValue(chave) == false) && (javaType.endsWith("Bean"))) chave = ", chave=\"LV:"+ (javaType.substring(0, javaType.length()-4)).toLowerCase() +"\"";

				String fk = "";
				String xxx = getRelation(constraintList, tableC, c);
				if (xxx != null) fk = ", fk=\""+ xxx +"\"";


				TableC tableCTemp = null;
				String beanName = null;
				String nameField = javaAtribute.substring(0, 1).toUpperCase() + javaAtribute.substring(1);
				String fkName = "";
				String fkNameToBean = "";
				String column1Name = null;
				if (javaType.endsWith("Bean")) {
					beanName = javaType.substring(0, javaType.length()-4).toLowerCase();
					tableCTemp = TableC.getTableByBean(beanName);

					nameField += ".Id";
					fkName = (StringUtils.occurrencesCount(c.getColumnName(), "_") > 1 ? c.getColumnName() : tableCTemp.getSigla());
					column1Name = (StringUtils.occurrencesCount(fkName, "_") > 0 ? fkName : fkName + "_id");
					fkName = tableC.getSigla() +"_"+ fkName + "";
					fkNameToBean = ", fkName=\"fk_"+fkName + "\"";
				}
				if ((javaType.endsWith("Bean") && (StringUtils.hasValue(fk) == false))) {
					fkToCreate.append("alter table "+ t.getTableName() +" add constraint fk_"+ fkName +" foreign key ("+ column1Name +") references "+ beanName + " (,"+ tableCTemp.getSigla() +"_id);" + PL);
				}

				if (c.isColumnPk() == true) pkList.add(javaAtribute);
				sb.append(TAB + "@ColumnDB(name=\""+c.getColumnName()+"\", pk="+ c.isColumnPk() + fkNameToBean + fk + ", length="+ c.getColumnPrecision() +", precision="+ c.getColumnScale() +")" + PL);
				sb.append(TAB + "@FieldDB(name=\""+ nameField +"\", label=\""+ getLabel(tableC, c, column) +"\", required="+ c.isColumnRequired() +", type=TableFieldEditType."+ fieldType + chave + ")" + PL);
				sb.append(TAB + "private "+ javaType +" "+ javaAtribute +";" + PL);
				sb.append("" + PL);
			}
			validPk(tableC, pkList);


			//
			// CONSTRUTORES
			//
			String part1 = "";
			String part2 = "";

			String part3 = "";
			String part4 = "";

			String part5 = "";
			String part6 = "";

			String temp = "";
			for (ColumnMD c : t.getColumnList()) {
				ColumnC column = ColumnC.getColumnByName(c.getColumnSigla(), c.getJavaAtributo());
				String javaType = getJavaType(tableC, c, column);
				String javaAtribute = getJavaAttribute(tableC, c, column);

				if (c.isColumnPk() == true) {
					if (part1.length() > 0) part1 += temp;
					part1 += javaType + " " + javaAtribute;
					part2 += TAB + TAB + "this." + javaAtribute + " = " + javaAtribute + ";" + PL;
				}

				if (c.isColumnRequired() == true) {
					if (part3.length() > 0) part3 += temp;
					part3 += javaType + " " + javaAtribute;
					part4 += TAB + TAB + "this." + javaAtribute + " = " + javaAtribute + ";" + PL;
				}


				part5 += temp;
				part5 += javaType + " " + javaAtribute;
				part6 += TAB + TAB + "this." + javaAtribute + " = " + javaAtribute + ";" + PL;


				temp = ", ";

			}

			// Sem parametro
			sb.append(TAB + "/** " + PL);
			sb.append(TAB + " * Construtor sem parametros" + PL);
			sb.append(TAB + " *" + PL);
			sb.append(TAB + " */" + PL);
			sb.append(TAB + "public "+ tableC.getClassName() +"Bean() {" + PL);
			sb.append(TAB + TAB + "super();" + PL);
			sb.append(TAB + "}" + PL);
			sb.append("" + PL);

			// com parametro PK
			sb.append(TAB + "/** " + PL);
			sb.append(TAB + " * Construtor com os campos da PK" + PL);
			sb.append(TAB + " *" + PL);
			sb.append(TAB + " */" + PL);
			sb.append(TAB + "public "+ tableC.getClassName() +"Bean("+ part1 +") {" + PL);
			sb.append(TAB + TAB + "super();" + PL);
			sb.append( part2 );
			sb.append(TAB + "}" + PL);
			sb.append("" + PL);

			// com parametro REQUERIDO
			if (part1.equals(part3) == false) {
				sb.append(TAB + "/* " + PL);
				sb.append(TAB + " * Construtor com os campos obrigadorios" + PL);
				sb.append(TAB + " *" + PL);
				sb.append(TAB + " */" + PL);
				sb.append(TAB + "public "+ tableC.getClassName() +"Bean("+ part3 +") {" + PL);
				sb.append(TAB + TAB + "super();" + PL);
				sb.append( part4 );
				sb.append(TAB + "}" + PL);
				sb.append("" + PL);
			}

			// com todos parametros
			if ((part1.equals(part3) == false) && (part1.equals(part5) == false) && (part3.equals(part5) == false)) {
				sb.append(TAB + "/** " + PL);
				sb.append(TAB + " *" + PL);
				sb.append(TAB + " */" + PL);
				sb.append(TAB + "public "+ tableC.getClassName() +"Bean("+ part5 +") {" + PL);
				sb.append(TAB + TAB + "super();" + PL);
				sb.append( part6 );
				sb.append(TAB + "}" + PL);
				sb.append("" + PL);
			}
			// 
			// METODOS
			//
			for (ColumnMD c : t.getColumnList()) {
				ColumnC column = ColumnC.getColumnByName(c.getColumnSigla(), c.getJavaAtributo());

				String javaType = getJavaType(tableC, c, column);
				String javaAtribute = getJavaAttribute(tableC, c, column);
				String javaMethod = getJavaMethod(tableC, c, column);
				String newBeanIfNull = "";
				if (c.getColumnSigla().equals(tableC.getSigla()) == false) {
					newBeanIfNull = TAB+TAB+"if ("+ javaAtribute +" == null) " + javaAtribute + " = new " + javaType + "();" + PL;
				}

				sb.append(TAB+"public void set"+ javaMethod +"("+ javaType +" "+ javaAtribute +") {" + PL);
				sb.append(TAB+TAB+"this."+ javaAtribute +" = "+ javaAtribute +";" + PL);
				sb.append(TAB+"}" + PL);
				sb.append("" + PL);
				sb.append(TAB+"public "+ javaType +" get"+ javaMethod +"() {" + PL);
				sb.append( newBeanIfNull ); 
				sb.append(TAB+TAB+"return "+ javaAtribute +";" + PL);
				sb.append(TAB+"}" + PL);
				sb.append("" + PL);
			}

			// 
			// METODO HASHCODE 
			// 
			sb.append(TAB+"@Override" + PL);
			sb.append(TAB+"public int hashCode() {" + PL);
			sb.append(TAB+TAB+"final int prime = 31;" + PL);
			sb.append(TAB+TAB+"int result = 1;" + PL);
			for (String pk : pkList) {
				sb.append(TAB+TAB+"result = prime * result + (("+ pk +" == null) ? 0 : "+ pk +".hashCode());" + PL);
			}
			sb.append(TAB+TAB+"return result;" + PL);
			sb.append(TAB+"}" + PL);
			sb.append( PL);

			// 
			// METODO EQUALS 
			// 
			sb.append(TAB+"@Override" + PL);
			sb.append(TAB+"public boolean equals(Object obj) {" + PL);
			sb.append(TAB+TAB+"if (this == obj)" + PL);
			sb.append(TAB+TAB+TAB+"return true;" + PL);
			sb.append(TAB+TAB+"if (obj == null)" + PL);
			sb.append(TAB+TAB+TAB+"return false;" + PL);
			sb.append(TAB+TAB+"if (getClass() != obj.getClass())" + PL);
			sb.append(TAB+TAB+TAB+"return false;" + PL);
			sb.append(TAB+TAB+tableC.getClassName() +"Bean other = ("+ tableC.getClassName() +"Bean) obj;" + PL);

			for (String pk : pkList) {
				sb.append(TAB+TAB+"if ("+ pk +" == null) {" + PL);
				sb.append(TAB+TAB+TAB+"if (other."+ pk +" != null)" + PL);
				sb.append(TAB+TAB+TAB+TAB+"return false;" + PL);
				sb.append(TAB+TAB+"} else if (!"+ pk +".equals(other."+ pk +"))" + PL);
				sb.append(TAB+TAB+TAB+"return false;" + PL);
			}
			sb.append(TAB+TAB+"return true;" + PL);
			sb.append(TAB+"}" + PL);

			sb.append("}" + PL);

			FileUtils.createDirectory(path);
			BufferedWriter f = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path + tableC.getClassName() + "Bean.java"), "UTF-8"));
			f.append(sb.toString());
			f.flush();
			f.close();
			System.out.println("["+ (i+1) +"/"+ list.size() +"] Gerado a classe " + tableC.getClassName() + "Bean em: " + path);
		}

		System.out.println( "==========================================" );
		System.out.println( "Criar as FK a seguir e rodar o GEN de novo" );
		System.out.println( "==========================================" );
		System.out.println( fkToCreate.toString() );
	}

	public static void printScript(TransactionDB trans, String owner, String filtro) throws Exception {
		String result = genScript(trans, owner, filtro, ";");
		System.out.println(result);
	}
	public static String genScript(TransactionDB trans, String owner, String filtro, String terminal) throws Exception {
		List<Class<?>> list = getClassesBean();
		if ((list == null) || (list.size() == 0))
			throw new DeveloperException("Não foi encontrado nenhuma classe no pacote " + TransactionDB.PACKAGE_BEAN);

		StringBuilder sb = new StringBuilder();
		Map<String, List<String>> commandList = genScriptCreateTable(trans, owner, filtro, list);
		for (String key : commandList.keySet()) {
			List<String> command = commandList.get(key);
			for (String c : command) {
				sb.append(c + terminal + PL );
			}

			sb.append( PL );
		}

		return sb.toString();
	}

	public static Map<String, List<String>> genScriptCreateTable(TransactionDB trans, String owner, String filtro, List<Class<?>> list) throws Exception {
		String typeDB = trans.getTypeDB();
		Map<String, List<String>> commandMap = new LinkedHashMap<String, List<String>>();
		if (owner == null) owner = "";
		if (StringUtils.hasValue(owner)) owner = owner + ".";
		if (owner.endsWith("..")) owner = owner.substring(0, owner.length()-1);


		// create table
		List<String> tableList = new ArrayList<String>();
		for (Class<?> classe : list) {
			StringBuilder sb = new StringBuilder();
			TableDB table = (TableDB)classe.getAnnotation(TableDB.class);
			if (table == null) continue;

			if (StringUtils.hasValue(filtro))
				if (table.name().equalsIgnoreCase(filtro) == false) continue;

			sb.append("create table " + owner + table.name() + "("+ PL);

			Field[] fieldArray = classe.getDeclaredFields();
			String temp = "";
			for (Field field : fieldArray) {
				ColumnDB column = (ColumnDB)field.getAnnotation(ColumnDB.class);
				FieldDB f = (FieldDB)field.getAnnotation(FieldDB.class);
				if (column == null) continue;
				sb.append(temp);


				String required = "";
				if (f.required()) required = " not null";
				String type = getTypeDBOracle(classe, field, column);
				if (typeDB.equalsIgnoreCase(TypeDB.MYSQL)) type = getTypeDBMySQL(field, column);


				sb.append("    " + column.name() + " " + type + required);
				temp = "," + PL;
			}
			sb.append( PL );
			sb.append(")");

			tableList.add(sb.toString());
		}
		commandMap.put("Table", tableList);

		// Cria PK
		List<String> pkList = new ArrayList<String>();
		for (Class<?> classe : list) {
			TableDB table = (TableDB)classe.getAnnotation(TableDB.class);
			if (table == null) continue;
			if (StringUtils.hasValue(filtro))
				if (table.name().equalsIgnoreCase(filtro) == false) continue;



			Field[] pkArray = DBHelper.getFieldPrimaryKey(classe);
			String pk = "";
			String temp = "";
			for (Field field : pkArray) {
				ColumnDB column = (ColumnDB)field.getAnnotation(ColumnDB.class);
				if (column == null) continue;

				pk += temp;
				pk += column.name();
				temp = ", ";
			}
			String command = "alter table "+ owner + table.name() +" add constraint pk_"+ table.acronym() + " primary key ("+ pk +")";
			pkList.add(command);
		}
		commandMap.put("PK", pkList);

		// Cria fK
		List<String> fkList = new ArrayList<String>();
		for (Class<?> classe : list) {
			TableDB table = (TableDB)classe.getAnnotation(TableDB.class);
			if (table == null) continue;
			if (StringUtils.hasValue(filtro))
				if (table.name().equalsIgnoreCase(filtro) == false) continue;

			Field[] pkArray = DBHelper.getFieldForingKey(classe);

			for (Field field : pkArray) {
				ColumnDB column = (ColumnDB)field.getAnnotation(ColumnDB.class);
				if (column == null) continue;
				if (StringUtils.hasValue(column.fk()) == false) continue;

				String javaType = field.getType().getSimpleName();
				String tabelaReferencia = javaType.substring(0, javaType.length()-4);
				TableC tableCTemp = TableC.getTableByName(tabelaReferencia);

				String fkName = (StringUtils.occurrencesCount(column.name(), "_") > 1 ? column.name() : tableCTemp.getSigla());
				String[] joinArray = column.fk().split(" and ");

				String part1 = "";
				String part2 = "";

				String temp = "";
				for (String join : joinArray) {
					String[] parts = join.split(" = ");

					part1 += temp + parts[0]; 
					part2 += temp + parts[1];
					temp = ", ";
				}


				String command = "alter table "+ owner + table.name() +" add constraint fk_"+ table.acronym() +"_"+ fkName +" foreign key ("+ part1 +") references "+ owner + tabelaReferencia.toLowerCase() + " ("+ part2 +")";
				fkList.add(command);
			}
			commandMap.put("FK", fkList);
		}

		return commandMap;

	}
	private static String getPathDotumModel(String pathProject) {
		return pathProject + "/src/" + TransactionDB.PACKAGE_BEAN.replaceAll("\\.", "/") + "/";
	}
	public static void diffDB(TransactionDB trans) throws Exception {


		Map<String, List<String>> map = diffDB(trans, null, ";");
		for (String key : map.keySet()) {
			System.out.println("=======================================================");
			System.out.println("=== "+ key +" ===");
			System.out.println("=======================================================");



			for (String comando : map.get(key)) {
				System.out.println(comando);
			}
			System.out.println(PL);
		}

	}
	public static Map<String, List<String>> diffDB(TransactionDB trans, String owner, String terminal) throws Exception {
		Map<String, List<String>> map = new LinkedHashMap<String, List<String>>();


		StringBuilder sb = new StringBuilder();
		if (owner == null) owner = "";
		if (StringUtils.hasValue(owner) == true) owner = owner + ".";

		// lista do arquivo
		List<Class<?>> classList = new ArrayList<Class<?>>();
		List<Class<?>> tempList = getClassesBean();
		for (Class<?> c : tempList) {
			TableDB tableAnn = (TableDB)c.getAnnotation(TableDB.class);
			if (tableAnn == null) continue;

			classList.add(c);
		}




		// lista do Banco de dados
		List<TableMD> tableMDList = selectTable(trans, "%", false);
		List<ConstraintMD> constraintList = selectConstraint(trans, "%");


		// ver os bean que existem e nao existe tabela
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		for (Class<?> _class : classList) {
			TableDB tableAnn = (TableDB)_class.getAnnotation(TableDB.class);
			list1.add(tableAnn.name());

			for (TableMD tableMD : tableMDList) {
				if (tableAnn.name().equalsIgnoreCase(tableMD.getTableName()) == false) continue;
				if (list2.contains(tableMD.getTableName())) continue;

				list2.add(tableMD.getTableName());
				break;
			}
		}

		//
		// TABELA QUE NAO EXISTE
		//
		List<String> commando = new ArrayList<String>();
		map.put("CREATE TABLE", commando);
		if (list1.size() != list2.size()) {
			for (String var : list1) {
				if (list2.contains(var)) continue;

				String result = GeneratorModel.genScript(trans, owner, var, TERMINAL);
				commando.add( result );
			}
		}

		list1 = new ArrayList<String>();
		list2 = new ArrayList<String>();
		for (TableMD tableMD : tableMDList) {
			list1.add(tableMD.getTableName());

			for (Class<?> bean : classList) {
				TableDB tableAnn = (TableDB)bean.getAnnotation(TableDB.class);
				if (tableAnn.name().equalsIgnoreCase(tableMD.getTableName()) == false) continue;
				if (list2.contains(tableAnn.name())) continue;

				list2.add(tableAnn.name());
				break;
			}
		}



		//
		// BEAN QUE NAO EXISTE
		//
		commando = new ArrayList<String>();
		map.put("DROP TABLE", commando);
		if (list1.size() != list2.size()) {
			for (String var : list1) {
				if (list2.contains(var)) continue; 

				commando.add( "drop table " + owner + var + terminal);
			}
		}


		//
		// BEAN QUE NAO EXISTE
		//
		for (Class<?> bean : classList) {
			TableDB tableAnn = (TableDB)bean.getAnnotation(TableDB.class);

			boolean foundTable = false;
			for (TableMD tableMD : tableMDList) {
				if (tableAnn.name().equalsIgnoreCase(tableMD.getTableName()) == false) continue;

				// achei a tabela que igual dela
				// agora comparar se ta tudo igual...
				// >> colunas
				// >> tipo
				// >> tamanho
				// >> required
				// >> fk
				// >> ...

				commando = new ArrayList<String>();
				map.put("ALTER TABLE " + tableAnn.label().toUpperCase(), commando);

				Field[] fieldArray = null;
				try {
					fieldArray = bean.getDeclaredFields();
				} catch (Exception e) {

					e.printStackTrace();
				}

				for (Field field : fieldArray) {
					ColumnDB columnAnn = (ColumnDB)field.getAnnotation(ColumnDB.class);
					if (columnAnn == null) continue;

					boolean nome = false;
					boolean tipo = false;
					boolean pk = false;
					boolean fk = false;
					boolean precision = false;
					boolean scale = false;

					String type = "";
					String constraintName = null;
					String constraintRelation = null;
					List<ColumnMD> columnMDList = tableMD.getColumnList();
					for (ColumnMD columnMD : columnMDList) {

						if (columnAnn.name().equalsIgnoreCase(columnMD.getColumnName()) == false) continue;
						if (columnAnn.pk() == columnMD.isColumnPk()) pk = true;

						for (ConstraintMD temp : constraintList) {
							if (columnAnn.fk().equalsIgnoreCase( temp.getRelation() ) == false) continue;
							fk = true;
							break;
						}


						if (columnAnn.length() == columnMD.getColumnPrecision()) precision = true;
						if (columnAnn.precision() == columnMD.getColumnScale()) scale = true;
						if (field.getType().getSimpleName().equalsIgnoreCase(columnMD.getJavaType())) tipo = true;
						if ((field.getType().getSimpleName().endsWith("Bean") && (columnMD.getJavaType().equalsIgnoreCase("Integer"))))  tipo = true; 
						nome = true;

						type = getTypeDBOracle(bean, field, columnAnn);
						constraintName = columnAnn.fkName();
						constraintRelation = columnAnn.fk();
						break;
					}

					if (nome == true) {
						if (pk == false) {
							sb = new StringBuilder();
							sb.append( tableAnn.name().toUpperCase() + ".");
							sb.append( columnAnn.name() + ":");
							sb.append(" pk diferente");

							commando.add( sb.toString() + terminal);
						}
						if (precision == false) {
							commando.add("alter table " + owner + tableAnn.name().toUpperCase() + " modify " + columnAnn.name() + " " + type + terminal);
						}
						if (scale == false) { 
							sb.append("alter table " + owner + tableAnn.name().toUpperCase() + " modify " + columnAnn.name() + " " + type);
						}
						if (tipo == false) { 
							sb = new StringBuilder();
							sb.append( tableAnn.name().toUpperCase() + ".");
							sb.append( columnAnn.name() + ":");
							sb.append(" tipo diferente");

							commando.add( sb.toString() + terminal);
						}
						if ((StringUtils.hasValue(columnAnn.fk()) && (fk == false))) {
							//alter table bemveiculo add constraint fk_bemv_cec foreign key (cec_id) references centrocusto (cec_id)

							sb = new StringBuilder();
							sb.append( "alter table ");
							sb.append( tableAnn.name().toUpperCase() );
							sb.append(" add constraint ");
							sb.append( constraintName );
							sb.append(" foreign key (");

							String[] parts = constraintRelation.split(" and ");
							String temp = "";
							for (String part : parts) {
								String[] xxx = part.split(" = ");
								sb.append( temp );
								sb.append( xxx[0] );
								temp = ",";
							}
							sb.append(") references ");

							String javaType = field.getType().getSimpleName();
							String tabelaReferencia = javaType.substring(0, javaType.length()-4);
							TableC tableCTemp = TableC.getTableByName(tabelaReferencia);
							sb.append(tableCTemp.getName());
							
							sb.append(" (");

							temp = "";
							for (String part : parts) {
								String[] xxx = part.split(" = ");
								sb.append( temp );
								sb.append( xxx[1] );
								temp = ",";
							}
							sb.append(" )");

							commando.add( sb.toString() + terminal);
						}
					}

				}



				commando = new ArrayList<String>();
				map.put("ADD COLUMN " + tableAnn.label().toUpperCase(), commando);

				// agora vou ver se no Bean tem item a mais que na tabela
				list1 = new ArrayList<String>();
				list2 = new ArrayList<String>();
				ArrayList<String> typeList = new ArrayList<String>();
				for (Field field : bean.getDeclaredFields()) {
					ColumnDB columnAnn = (ColumnDB)field.getAnnotation(ColumnDB.class);
					if (columnAnn == null) continue;
					list1.add(columnAnn.name());
					typeList.add( getTypeDBOracle(bean, field, columnAnn) );

					List<ColumnMD> columnMDList = tableMD.getColumnList();
					for (ColumnMD columnMD : columnMDList) {
						if (columnAnn.name().equalsIgnoreCase(columnMD.getColumnName()) == false) continue;
						if (list2.contains(columnMD.getColumnName())) continue;

						list2.add(columnMD.getColumnName());

						break;
					}

				}
				if (list1.size() != list2.size()) {


					for (int i = 0; i < list1.size(); i++) {
						String campo1 = list1.get(i);
						if (list2.contains(campo1)) continue; 

						String type = typeList.get(i);
						commando.add("alter table " + owner + tableAnn.name().toUpperCase() + " add " + campo1 + " " + type + terminal);
					}
				}



				commando = new ArrayList<String>();
				map.put("DROP COLUMN " + tableAnn.label().toUpperCase(), commando);

				// agora vou ver se na tabela tem coluna que nao tem no Bean
				list1 = new ArrayList<String>();
				list2 = new ArrayList<String>();
				List<ColumnMD> columnMDList = tableMD.getColumnList();
				for (ColumnMD columnMD : columnMDList) {
					list1.add(columnMD.getColumnName());

					for (Field field : bean.getDeclaredFields()) {
						ColumnDB columnAnn = (ColumnDB)field.getAnnotation(ColumnDB.class);
						if (columnAnn == null) continue;
						if (columnAnn.name().equalsIgnoreCase(columnMD.getColumnName()) == false) continue;
						if (list2.contains(columnAnn.name())) continue;

						list2.add(columnAnn.name());
						break;
					}

				}

				if (list1.size() != list2.size()) {
					for (String campo1 : list1) {
						if (list2.contains(campo1)) continue; 
						commando.add("alter table " + owner + tableAnn.name().toUpperCase() + " drop column " + campo1 + terminal);
					}
				}


				// agora vou ver se no Bean tem item a menos que na tabela
				foundTable = true;
			}


			if (foundTable == false) {
				// caso nao exista a tabela no banco criar o scritp para tal.
			}
		}

		return map;
	}


	private static List<Class<?>> getClassesBean() throws ClassNotFoundException, IOException {
		List<Class<?>> tempList = ClassUtils.getClasses(TransactionDB.PACKAGE_BEAN);
		List<Class<?>> temp2List = ClassUtils.getClasses(TransactionDB.PACK_MODEL_DEFAULT);
		temp2List.remove(UnidadeBean.class);
		temp2List.remove(PessoaBean.class);
		tempList.addAll( temp2List );
		
		return tempList;
	}

	// HELPER
	private static List<TableMD> selectTable(TransactionDB trans, String filter) throws Exception {
		return selectTable(trans, filter, true);
	}
	private static List<TableMD> selectTable(TransactionDB trans, String filter, boolean valid) throws Exception {
		String typeDB = PropertiesUtils.getString(PropertiesConstants.DATABASE_TYPE);
		if (StringUtils.hasValue(typeDB) == false) typeDB = "ORACLE";

		String sql = getSqlTableOracle(filter);
		if (typeDB.equalsIgnoreCase("POSTGRES")) sql = getSqlTablePostgres(filter);
		ResultSet rs = trans.execQuery(sql, new Object[] {filter.toUpperCase()});


		String lastTable = null;
		TableMD t = null;
		List<TableMD> result = new ArrayList<TableMD>();
		while (rs.next()) {
			String tableName = rs.getString("TABLE_NAME");
			if (tableName.equals(lastTable) == false) {
				t = new TableMD();
				t.setTableName(tableName);
				result.add(t);

				lastTable = tableName;
			}

			ColumnMD c = new ColumnMD();
			c.setColumnPk( StringUtils.hasValue( rs.getString("pk_name") ) );
			c.setColumnRequired( rs.getString("nullable").equals("N") );
			c.setColumnSigla(rs.getString("COLUMN_SIGLA"));
			c.setColumnName(rs.getString("COLUMN_NAME"));
			c.setColumnPrecision(rs.getInt("COLUMN_PRECISION"));
			c.setColumnScale(rs.getInt("COLUMN_SCALE"));
			c.setJavaType(rs.getString("JAVA_TIPO"));
			c.setJavaAtributo(rs.getString("JAVA_ATRIBUTO"));
			t.addColumn(c);
		}
		trans.closeQuery();

		if (result.size() == 0) {
			System.out.println("O filtro utilizado ("+ filter +") não encontrou nenhuma tabela");
			System.exit(-1);
		}
		if (valid == true)
			valid(result);

		return result;
	}
	private static String getSqlTableOracle(String filter) {
		StringBuilder sb = new StringBuilder();
		sb.append("          select lower(u.table_name) as TABLE_NAME,                                                                                       ");
		sb.append("                 lower(substr(u.column_name, 1, (INSTR(u.column_name, '_')-1))) as COLUMN_SIGLA,                                          ");
		sb.append("                 lower(u.column_name) as COLUMN_NAME,                                                                                     ");
		sb.append("                 (case when u.data_type = 'VARCHAR2' then to_number(u.char_col_decl_length)                                               ");
		sb.append("                       when u.data_type = 'NVARCHAR2' then to_number(u.char_col_decl_length)                                              ");
		sb.append("                       when u.data_type = 'CLOB' then to_number(999999)                                                                   ");
		sb.append("                       when u.data_type = 'NUMBER' then to_number(u.data_precision)                                                       ");
		sb.append("                       when u.data_type = 'DATE' then to_number(0)                                                                        ");
		sb.append("                       when u.data_type = 'BLOB' then to_number(0)                                                                        ");
		sb.append("                       else -1                                                                                                            ");
		sb.append("                 end) as COLUMN_PRECISION,                                                                                                ");
		sb.append("                 (case when u.data_type = 'VARCHAR2' then to_number(0)                                                                    ");
		sb.append("                       when u.data_type = 'NVARCHAR2' then to_number(0)                                                                   ");
		sb.append("                       when u.data_type = 'CLOB' then to_number(0)                                                                        ");
		sb.append("                       when u.data_type = 'NUMBER' then to_number(u.data_scale)                                                           ");
		sb.append("                       when u.data_type = 'DATE' then to_number(0)                                                                        ");
		sb.append("                       when u.data_type = 'BLOB' then to_number(0)                                                                        ");
		sb.append("                       else -1                                                                                                            ");
		sb.append("                 end) as COLUMN_SCALE,                                                                                                    ");
		sb.append("                 (case when (u.data_type = 'VARCHAR2') then 'String'                                                                      ");
		sb.append("                       when (u.data_type = 'NVARCHAR2') then 'String'                                                                     ");
		sb.append("                       when (u.data_type = 'CLOB') then 'String'                                                                          ");
		sb.append("                       when (u.data_type = 'DATE') then 'Date'                                                                            ");
		sb.append("                       when (u.data_type = 'NUMBER' and u.data_scale = 0) then 'Integer'                                                  ");
		sb.append("                       when (u.data_type = 'NUMBER' and u.data_scale > 0) then 'BigDecimal'                                                   ");
		sb.append("                       when (u.data_type = 'BLOB') then 'Blob'                                                                            ");
		sb.append("                       else 'DESCONHECIDO=' || u.data_type                                                                                ");
		sb.append("                  end) as JAVA_TIPO,                                                                                                      ");
		sb.append("                 lower(substr(u.column_name, (INSTR(u.column_name, '_') + 1))) as JAVA_ATRIBUTO,                                          ");
		sb.append("                 u.nullable,                                                                                                              ");
		sb.append("                 u.COLUMN_ID,                                                                                                             ");
		sb.append("                 tb1.constraint_name as pk_name                                                                                           ");
		sb.append("            from USER_TAB_COLUMNS u                                                                                                       ");
		sb.append(" left outer join (select uc.table_name,                                                                                                   ");
		sb.append("                         uc.constraint_name,                                                                                              ");
		sb.append("                         ucc.COLUMN_NAME                                                                                                  ");
		sb.append("                    from USER_CONSTRAINTS uc,                                                                                             ");
		sb.append("                         USER_CONS_COLUMNS ucc                                                                                            ");
		sb.append("                   where uc.CONSTRAINT_TYPE = 'P'                                                                                         ");
		sb.append("                     and uc.CONSTRAINT_NAME = ucc.CONSTRAINT_NAME) tb1 on (tb1.table_name = u.table_name                                  ");
		sb.append("                                                                       AND tb1.COLUMN_NAME = u.COLUMN_NAME)                               ");
		sb.append("           WHERE u.TABLE_NAME LIKE ?                                                                                                      ");
		sb.append("             and u.TABLE_NAME not like 'SYS_%'                                                                                            ");
		sb.append("        ORDER BY TABLE_NAME, COLUMN_ID                                                                                                    ");

		return sb.toString();
	}
	private static String getSqlTablePostgres(String filter) {
		StringBuilder sb = new StringBuilder();
		sb.append("          select s.table_name,                                                                                                            ");
		sb.append("                 lower(substr(s.column_name, 1, (strpos(s.column_name, '_')-1))) as COLUMN_SIGLA,                                         ");
		sb.append("                 lower(s.column_name) as COLUMN_NAME,                                                                                     ");

		sb.append("                 (case when s.data_type = 'character varying' then to_number(s.character_maximum_length, '')                                ");
		sb.append("                       when s.data_type = 'numeric' then to_number(s.numeric_precision)                                                     ");
		sb.append("                       else 0                                                                                                            ");
		sb.append("                  end) as COLUMN_PRECISION,                                                                                               ");

		sb.append("                 (case when s.data_type = 'character varying' then to_number(s.character_maximum_length, '')                                ");
		sb.append("                       when s.data_type = 'numeric' then to_number(s.numeric_scale)                                                         ");
		sb.append("                       else 0                                                                                                            ");
		sb.append("                  end) as COLUMN_SCALE,                                                                                                   ");

		sb.append("                 (case when (s.data_type = 'character varying') then 'String'                                                             ");
		sb.append("                       when (s.data_type = 'date') then 'Date'                                                                            ");
		sb.append("                       when (s.data_type = 'numeric' and s.numeric_scale = 0) then 'Integer'                                              ");
		sb.append("                       when (s.data_type = 'numeric' and s.numeric_scale > 0) then 'Double'                                               ");
		sb.append("                       else 'DESCONHECIDO'                                                                                                ");
		sb.append("                  end) as JAVA_TIPO,                                                                                                      ");
		sb.append("                 lower(substr(s.column_name, (strpos(s.column_name, '_') + 1))) as JAVA_ATRIBUTO,                                         ");
		sb.append("                 substr(is_nullable, 1, 1) as nullable,                                                                                   ");
		sb.append("                 s.ordinal_position as COLUMN_ID,                                                                                         ");
		sb.append("                 c.constraint_name as pk_name                                                                                             ");
		sb.append("            FROM information_schema.columns s                                                                                             ");
		sb.append(" left outer join information_schema.constraint_column_usage c on (s.table_name = c.table_name                                             ");
		sb.append("                                                              and s.column_name = c.column_name                                           ");
		sb.append("                                                              and c.constraint_name like 'pk%')                                           ");
		sb.append("           WHERE upper(s.table_name) LIKE ?                                                                           ");
		sb.append("        ORDER BY column_id                                                                                                                ");

		return sb.toString();
	}
	private static List<ConstraintMD> selectConstraint(TransactionDB trans, String filter) throws Exception {
		String typeDB = PropertiesUtils.getString(PropertiesConstants.DATABASE_TYPE);
		if (StringUtils.hasValue(typeDB) == false) typeDB = "ORACLE";

		String sql = getSqlConstraintOracle(filter);
		if (typeDB.equalsIgnoreCase("POSTGRES")) sql = getSqlConstraintPostgres(filter);
		ResultSet rs = trans.execQuery(sql, new Object[] {filter.toUpperCase()});


		ConstraintMD c = null;
		List<ConstraintMD> result = new ArrayList<ConstraintMD>();
		while (rs.next()) {
			c = new ConstraintMD();
			result.add(c);

			c.setName(rs.getString("CONSTRAINT_NAME"));
			c.setTable(rs.getString("TABLE_NAME"));
			c.setRelation(rs.getString("RELATION"));
		}
		trans.closeQuery();
		return result;
	}
	private static String getSqlConstraintPostgres(String filter) throws DeveloperException {
		throw new DeveloperException("Ainda nao implementado para postgres");
	}
	private static String getSqlConstraintOracle(String filter) {
		StringBuilder sb = new StringBuilder();

		sb.append("          select uc.constraint_name,                                                                                              ");
		sb.append("                 ucc2.table_name,                                                                                                 ");
		sb.append("                 listagg(ucc1.column_name || ' = ' ||ucc2.column_name, ' and ') within group (order by ucc1.position) as relation ");
		sb.append("                                                                                                                                  ");
		sb.append("            from user_constraints uc,                                                                                             ");  
		sb.append("                 user_cons_columns ucc1,                                                                                          ");  
		sb.append("                 user_cons_columns ucc2                                                                                           ");  
		sb.append("           where uc.constraint_name = ucc1.constraint_name                                                                        ");  
		sb.append("             and uc.r_constraint_name = ucc2.constraint_name                                                                      ");  
		sb.append("             and ucc1.position        = ucc2.position                                                                             ");  
		sb.append("             and uc.constraint_type   = 'R'                                                                                       ");  
		sb.append("             and ucc1.table_name like ?                                                                                              ");
		sb.append("        group by uc.constraint_name,                                                                                              ");
		sb.append("                 ucc2.table_name                                                                                                  ");

		return sb.toString();
	}
	// 
	private static void validPk(TableC tableC, List<String> pk) throws DeveloperException {
		if (pk.size() > 1) {
			for (int i = 1; i < pk.size(); i++) {
				if (pk.get(i).equalsIgnoreCase("unidade") == false)  continue;
				throw new DeveloperException("A PK da tabela "+ tableC.getName() +" esta montado errado. A unidade tem que ser primeiro");
			}
		}
	}
	private static String getRelation(List<ConstraintMD> constraintList, TableC tableC, ColumnMD c) throws Exception {
		if (c.getColumnSigla().equals(tableC.getSigla()) == false) {
			TableC tableCTemp = TableC.getTableBySigla(c.getColumnSigla());
			for (ConstraintMD constraintBean : constraintList) {
				if (constraintBean.getTable().equalsIgnoreCase(tableCTemp.getName()) == false) continue;

				String[] joins = constraintBean.getRelation().split(" and ");
				for (String join : joins) {
					String[] parts = join.split(" = ");
					if (c.getColumnName().trim().equalsIgnoreCase(parts[0].trim()) == false) continue;

					return constraintBean.getRelation().toLowerCase();
				}
			}
		}

		return null;
	}
	private static void valid(List<TableMD> list) throws Exception {
		boolean faltaMapear = false;

		DicionarioGen d = new DicionarioGen();

		StringBuilder sb = new StringBuilder();
		List<TableMD> listTemp = new ArrayList<TableMD>();

		List<String> tables = new ArrayList<String>();
		for (TableMD t : list) {
			try {
				TableC.getTableByName(t.getTableName());
				listTemp.add(t);
			} catch (Exception e) {
				faltaMapear = true;

				String tableName = t.getTableName();
				String label = t.getTableName();
				try {
					tableName = d.getJavaUpper(t.getTableName());
					label = d.getLabel(t.getTableName());
				} catch (Exception iu) {
				}
				String error = 	"list.add(new TableC(\""+ t.getTableName().toLowerCase() +"\",					false, 		\""+ "\",			\""+ tableName +"\",				\""+ label +"\"));";
				if (tables.contains(error) == false)
					tables.add(error);
			}
		}

		for (TableMD t : list) {
			for (ColumnMD c : t.getColumnList()) {
				try {
					TableC.getTableBySigla(c.getColumnSigla());
				} catch (Exception e) {
					faltaMapear = true;
					String error = 	"list.add(new TableC(\""+ c.getColumnSigla() +"\",					false, 		\""+ "\",			\""+ c.getColumnSigla() +"\",				\""+ c.getColumnSigla() +"\"));";
					if (tables.contains(error) == false)
						tables.add(error);
				}
			}
		}
		sb.append(PL);
		sb.append("=================================" + PL);
		sb.append("Falta mapear as seguintes TABELAS" + PL);
		sb.append("=================================" + PL);
		for (String table : tables) {
			sb.append(table + PL);
		}


		List<String> colunas = new ArrayList<String>();
		for (TableMD t : list) {
			for (ColumnMD c : t.getColumnList()) {
				try {
					ColumnC.getColumnByName(c.getColumnSigla(), c.getJavaAtributo());
				} catch (Exception e) {
					faltaMapear = true;



					String attribute = c.getJavaAtributo();
					String label = c.getJavaAtributo();
					try {
						attribute = d.getJavaLower(c.getJavaAtributo());
						label = d.getLabel(c.getJavaAtributo());
					} catch (Exception iu) {
					}

					String error = "list.add(new ColumnC(\""+ c.getColumnSigla() + "_" + c.getJavaAtributo() +"\",						null,               \""+ attribute +"\",					\""+ label +"\",						null,						null,						null));";
					if (colunas.contains(error) == false)
						colunas.add(error);
				}
			}
		}
		sb.append(PL);
		sb.append("=================================" + PL);
		sb.append("Falta mapear as seguintes COLUNAS" + PL);
		sb.append("=================================" + PL);
		for (String coluna : colunas) {
			sb.append(coluna + PL);
		}


		if (faltaMapear == true)
			throw new DeveloperException(sb.toString());
	}
	private static String getFieldType(TableC tableC, ColumnMD c, ColumnC column) throws Exception {
		String result = column.getFieldType();
		if (result == null) {
			String javaType = getJavaType(tableC, c, column);
			if (javaType.equals("String")) {
				result = "TEXTFIELD";
			} else if (javaType.equals("Integer")) {
				result = "INTEGERFIELD";
			} else if (javaType.equals("BigDecimal")) {
				result = "DECIMALFIELD";
			} else if (javaType.equals("Double")) {
				result = "DECIMALFIELD";
			} else if (javaType.equals("Date")) {
				result = "DATEFIELD";
			} else if (javaType.equals("Long")) {
				result = "LONGFIELD";
			} else if (javaType.equals("Blob")) {
				result = "FILEUPLOAD";
			} else if (c.getColumnSigla().equals(tableC.getSigla()) == false) {
				result = "COMBOBOXDBFIELD";
			} else {
				throw new DeveloperException("Tipo de campo ("+ javaType  +") nao detectado para coluna \"" + column.getName() + "\".");
			}
		}

		return result;
	}
	private static String getLabel(TableC tableC, ColumnMD c, ColumnC column) throws Exception {
		String result = column.getLabel();
		if (c.getColumnSigla().equals(tableC.getSigla()) == false) {
			TableC tableCTemp = TableC.getTableBySigla(c.getColumnSigla());
			result = tableCTemp.getLabel();
		}
		return result;
	}
	private static String getJavaType(TableC tableC, ColumnMD c, ColumnC column) throws Exception {
		String result = column.getJavaType();
		if (result == null) result = c.getJavaType();
		if (c.getColumnSigla().equals(tableC.getSigla()) == false) {
			TableC tableCTemp = TableC.getTableBySigla(c.getColumnSigla());
			result = tableCTemp.getClassName() + "Bean";
		}
		return result;
	}
	private static String getJavaAttribute(TableC tableC, ColumnMD c, ColumnC column) throws Exception {
		String result = column.getAtribute();
		if (c.getColumnSigla().equals(tableC.getSigla()) == false) {
			TableC tableCTemp = TableC.getTableBySigla(c.getColumnSigla());
			if (StringUtils.occurrencesCount(c.getColumnName(), "_") == 1) {
				result = tableCTemp.getClassName().substring(0, 1).toLowerCase() + tableCTemp.getClassName().substring(1);
			}
		}

		return result;
	}
	private static String getJavaMethod(TableC tableC, ColumnMD c, ColumnC column) throws Exception {

		String javaAtribute = getJavaAttribute(tableC, c, column);
		String result = null;
		if (c.getColumnSigla().equals(tableC.getSigla()) == false) {
			TableC tableCTemp = TableC.getTableBySigla(c.getColumnSigla());
			if (c.getColumnName().contains("_")) {
				result = javaAtribute.split("_")[0].substring(0, 1).toUpperCase() + javaAtribute.split("_")[0].substring(1);
			} else {
				result = tableCTemp.getClassName().substring(0, 1).toUpperCase() + tableCTemp.getClassName().substring(1);
			}
		} else {
			result = javaAtribute.substring(0, 1).toUpperCase() + javaAtribute.substring(1); 
		}

		return result;
	}
	private static String getTypeDBOracle(Class<?> bean, Field field, ColumnDB column) throws DeveloperException {
		String result = null;
		if (field.getType().equals(Integer.class)) {
			result = "number("+ column.length() +"," + column.precision() +")";
		} else if (field.getType().equals(BigDecimal.class)) {
			result = "number("+ column.length() +"," + column.precision() +")";
		} else if (field.getType().equals(Double.class)) {
			result = "number("+ column.length() +"," + column.precision() +")";
		} else if (field.getType().equals(Long.class)) {
			result = "number("+ column.length() +")";
		} else if ((field.getType().equals(String.class)) && (column.length() < 5000)) {
			result = "varchar2("+ column.length() +")";
		} else if (field.getType().equals(String.class)) {
			result = "clob";
		} else if (field.getType().equals(Blob.class)) {
			result = "blob";
		} else if (field.getType().equals(Clob.class)) {
			result = "clob";
		} else if (field.getType().equals(Date.class)) {
			result = "date";
		} else if (field.getType().getSuperclass().equals(Bean.class)) {
			result = "number(10)";
		} else {
			throw new DeveloperException("Tipo desconhecido para o campo \"" + field.getName() + "\" do tipo \"" + field.getType() + "\" na classe \""+ bean.getSimpleName() +"\".");
		}
		return result;
	}

	private static String getTypeDBMySQL(Field field, ColumnDB column) throws DeveloperException {
		String result = null;
		if (field.getType().equals(Integer.class)) {
			result = "numeric("+ column.length() +"," + column.precision() +")";
		} else if (field.getType().equals(Double.class)) {
			result = "numeric("+ column.length() +"," + column.precision() +")";
		} else if (field.getType().equals(Long.class)) {
			result = "numeric("+ column.length() +")";
		} else if ((field.getType().equals(String.class)) && (column.length() < 5000)) {
			result = "varchar("+ column.length() +")";
		} else if (field.getType().equals(String.class)) {
			result = "text";
		} else if (field.getType().equals(Blob.class)) {
			result = "blob";
		} else if (field.getType().equals(Clob.class)) {
			result = "MEDIUMTEXT";
		} else if (field.getType().equals(Date.class)) {
			result = "date";
		} else if (field.getType().getSuperclass().equals(Bean.class)) {
			result = "numeric(15)";
		} else {
			throw new DeveloperException("Tipo desconhecido para o campo " + field.getName() + " do tipo " + field.getType());
		}
		return result;
	}

	/**
	 * Ainda esta +/- pronto
	 * @param conn
	 * @param userName
	 */
	public static Map<String, String> createUser(ConnectionDB conn, String userName) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("TableSpace",  "create bigfile tablespace ts"+userName+" datafile '/u01/app/oradata/dotum/df"+userName+".dbf' size 512m autoextend on next 256m maxsize 10g;");
		map.put("User",  "create user "+userName+" identified by \"2345vc78\" default tablespace ts"+userName+" quota unlimited on ts"+userName+"");
		map.put("Grant",  "grant connect, resource to "+userName+"");
		map.put("Grant",  "grant read, write on directory backup_dir to "+userName+"");

		return map;
	}
}


class TableMD extends Bean {
	private static final long serialVersionUID = 1L;

	private String tableName;
	private List<ColumnMD> columnList = new ArrayList<ColumnMD>();

	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public List<ColumnMD> getColumnList() {
		return columnList;
	}
	public void addColumn(ColumnMD column) {
		this.columnList.add(column);
	}
}

class ColumnMD extends Bean {
	private static final long serialVersionUID = 1L;

	private boolean columnPk;
	private boolean columnRequired;
	private String columnSigla;
	private String columnName;
	private Integer columnPrecision;
	private Integer columnScale;
	private String javaType;
	private String javaAtributo;

	public boolean isColumnPk() {
		return columnPk;
	}
	public void setColumnPk(boolean columnPk) {
		this.columnPk = columnPk;
	}
	public boolean isColumnRequired() {
		return columnRequired;
	}
	public void setColumnRequired(boolean columnRequired) {
		this.columnRequired = columnRequired;
	}
	public String getColumnSigla() {
		return columnSigla;
	}
	public String getColumnName() {
		return columnName;
	}
	public String getJavaType() {
		return javaType;
	}
	public String getJavaAtributo() {
		return javaAtributo;
	}
	public void setColumnSigla(String columnSigla) {
		this.columnSigla = columnSigla;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}
	public void setJavaAtributo(String javaAtributo) {
		this.javaAtributo = javaAtributo;
	}
	public Integer getColumnPrecision() {
		return columnPrecision;
	}
	public void setColumnPrecision(Integer columnPrecision) {
		this.columnPrecision = columnPrecision;
	}
	public Integer getColumnScale() {
		return columnScale;
	}
	public void setColumnScale(Integer columnScale) {
		this.columnScale = columnScale;
	}
}

class ConstraintMD {
	private String name;
	private String table;
	private String relation;
	private boolean usado;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public boolean isUsado() {
		return usado;
	}
	public void setUsado(boolean usado) {
		this.usado = usado;
	}
}