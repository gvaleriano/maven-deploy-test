package br.com.dotum.jedi.gen;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.db.bean.ComponenteBean;
import br.com.dotum.jedi.gen.AbstractTableMap.TypePKEnum;
import br.com.dotum.jedi.util.PropertiesUtils;


public class TableC {

	public static List<TableC> list = new ArrayList<TableC>();
	
	static {
		try {
			String pack = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
			Class classe = Class.forName(pack + ".control.MapTable");
			AbstractTableMap acm = (AbstractTableMap)classe.newInstance();
			list.addAll( acm.createList() );
			
			list.add(new TableC("alerta",					false, 		"ale",			"Alerta",					"Alerta"));
			list.add(new TableC("alertaunidade",			false, 		"alun",			"AlertaUnidade",			"Alerta da Unidade"));
			
			list.add(new TableC("tutorial",					false, 		"tut",			"Tutorial",					"Tutorial"));
			list.add(new TableC("tutorialpasso",			false, 		"tutp",			"TutorialPasso",			"TutorialPasso"));
			list.add(new TableC("tutorialpessoa",			false, 		"tupe",			"TutorialPessoa",			"TutorialPessoa"));
			
			list.add(new TableC("tarefa",					false, 		"tar",			"Tarefa",					"Tarefa"));
			list.add(new TableC("auditoria",				false, 		"aud",			"Auditoria",				"Auditoria"));
			list.add(new TableC("auditoriatipo",			false, 		"audt",			"AuditoriaTipo",			"Tipo de auditoria"));
			list.add(new TableC("fwrelease",				false, 		"fwr",			"FwRelease",				"Release"));
			list.add(new TableC("componentetipo",			false, 		"cptt",			"ComponenteTipo",			"Tipo de componente"));
			list.add(new TableC("componente",				false, 		"cpt",			"Componente",				"Componente"));
			list.add(new TableC("componenteestatistica",	false, 		"cpte",			"ComponenteEstatistica",	"Estatisticas de componente"));
			list.add(new TableC("pessoatarefa",				false, 		"peta",			"PessoaTarefa",				"Tarefas da pessoa"));
			list.add(new TableC("grupoacesso",				false, 		"grua",			"GrupoAcesso",				"Grupo de acesso"));
			list.add(new TableC("grupoacessotarefa",		false, 		"grta",			"GrupoAcessoTarefa",		"Tarefas dos Grupo de acesso"));

			list.add(new TableC("unidade",					false, 		"uni",			"Unidade",					"Unidade"));
			list.add(new TableC("pessoa",					false, 		"pes",			"Pessoa",					"Pessoa"));

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static TableC getTableBySigla(String sigla) throws Exception {
		for (TableC table : list) {
			if (table.getSigla().equals(sigla) == false) continue;
			return table;
		}

		throw new Exception("Nao foi mapeado a tabela com a sigla: " + sigla);
	}

	public static TableC getTableByName(String name) throws Exception {
		for (TableC table : list) {
			if (table.getName().equalsIgnoreCase(name) == false) continue;
			return table;
		}
		
		throw new Exception("Nao foi mapeado a tabela com nome: " + name);
	}
	
	public static TableC getTableByBean(String name) throws Exception {
		for (TableC table : list) {
			if (table.getClassName().equalsIgnoreCase(name) == false) continue;
			return table;
		}
		
		throw new Exception("Nao foi mapeado o bean com nome: " + name);
	}
	
	private String name;
	private boolean auditing;
	private TypePKEnum typePK;
	private String sigla;
	private String className;
	private String label;
	private String sequence;

	public TableC(String name, boolean auditing, String sigla, String className, String label) {
		super();
		this.name = name;
		this.auditing = auditing;
		this.typePK = TypePKEnum.SEQUENCE;
		this.sigla = sigla;
		this.className = className;
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public boolean getAuditing() {
		return auditing;
	}

	public String getSigla() {
		return sigla;
	}

	public String getClassName() {
		return className;
	}

	public String getLabel() {
		return label;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public TypePKEnum getTypePK() {
		return typePK;
	}

	public void setTypePK(TypePKEnum typePK) {
		this.typePK = typePK;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
}





