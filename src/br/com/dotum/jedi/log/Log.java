package br.com.dotum.jedi.log;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

public class Log {
	private Logger log;

	public Log(Class<?> klass) {
		log = Logger.getLogger(klass.getName());


		String pathLog = PropertiesUtils.getString(PropertiesConstants.APPLICATION_LOG_PATH);
		String levelLog = PropertiesUtils.getString(PropertiesConstants.APPLICATION_LOG_LEVEL);
		if (StringUtils.hasValue(levelLog) == false) levelLog = "error";

		try {
			PatternLayout layout = new PatternLayout();
			String conversionPattern = "%-7p %d [%t] %c %x - %m%n";
			layout.setConversionPattern(conversionPattern);

			ConsoleAppender consoleAppender = new ConsoleAppender();
			consoleAppender.setLayout(layout);
			consoleAppender.activateOptions();
			log.addAppender(consoleAppender);

			if (StringUtils.hasValue(pathLog) == true) {
				FileAppender fileAppender = new FileAppender();
				fileAppender.setLayout(layout);
				fileAppender.setFile(pathLog);
				fileAppender.activateOptions();
				log.addAppender(fileAppender);
			}

			Level level = null;
			if (levelLog.equalsIgnoreCase("debug")) {
				level = Level.DEBUG;
			} else if (levelLog.equalsIgnoreCase("info")) {
				level = Level.INFO;
			} else if (levelLog.equalsIgnoreCase("warn")) {
				level = Level.WARN;
			} else if (levelLog.equalsIgnoreCase("error")) {
				level = Level.ERROR;
			}

			log.setLevel(level);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//
	// ERROR
	//
	public void error(UserContext userContext, String msg, Throwable thrown){
		msg = formatMsg(userContext, msg);
		log.log(Level.ERROR, msg, thrown);
	}

	@Deprecated
	public void error(String msg, Throwable thrown){
		error(null, msg, thrown);
	}

	public void error(UserContext userContext, String msg){
		error(userContext, msg, null);
	}

	@Deprecated
	public void error(String msg){
		error(null, msg, null);
	}

	//
	// WARN
	//
	public void warn(UserContext userContext, String msg, Throwable thrown){
		msg = formatMsg(userContext, msg);
		log.log(Level.WARN, msg, thrown);
	}

	@Deprecated
	public void warn(String msg, Throwable thrown){
		warn(null, msg, thrown);
	}

	public void warn(UserContext userContext, String msg){
		warn(userContext, msg, null);
	}

	@Deprecated
	public void warn(String msg){
		warn(null, msg, null);
	}


	//
	// INFO
	//
	public void info(UserContext userContext, String msg, Throwable thrown){
		msg = formatMsg(userContext, msg);
		log.log(Level.INFO, msg, thrown);
	}

	@Deprecated
	public void info(String msg, Throwable thrown){
		info(null, msg, thrown);
	}

	public void info(UserContext userContext, String msg){
		info(userContext, msg, null);
	}

	@Deprecated
	public void info(String msg){
		info(null, msg, null);
	}

	//
	// DEBUG
	//
	public void debug(UserContext userContext, String msg, Throwable thrown){
		msg = formatMsg(userContext, msg);
		log.log(Level.DEBUG, msg, thrown);
	}

	@Deprecated
	public void debug(String msg, Throwable thrown){
		debug(null, msg, thrown);
	}

	public void debug(UserContext userContext, String msg){
		debug(userContext, msg, null);
	}

	@Deprecated
	public void debug(String msg){
		debug(null, msg, null);
	}

	private String formatMsg(UserContext userContext, String msg) {
		String part = "";
		if (userContext != null) part = "[" + userContext.getUser().getLogin() + "] "; 
		String xxx =  part + msg;
		return xxx;
	}
}




