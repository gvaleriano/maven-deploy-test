package br.com.dotum.jedi.log;

public class LogFactory {

	private static Log log;
	
	public static Log getLog(Class<?> klass){
		if (log == null) log = new Log(klass); 
		return log;
	}
	
}