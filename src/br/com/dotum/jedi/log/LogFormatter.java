package br.com.dotum.jedi.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import br.com.dotum.jedi.util.FormatUtils;


public final class LogFormatter extends Formatter {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();

        Date data = new Date(record.getMillis());
        
        sb.append("[");
        sb.append(FormatUtils.formatDate( data ) + " " + FormatUtils.formatHour( data ));
        sb.append("] ");
        sb.append( record.getLevel().getLocalizedName() + ":");
        sb.append(" ");
        sb.append(formatMessage(record));
        sb.append(LINE_SEPARATOR);

        if (record.getThrown() != null) {
            try {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                record.getThrown().printStackTrace(pw);
                pw.close();
                sb.append(sw.toString());
            } catch (Exception ex) {
                // ignore
            }
        }

        return sb.toString();
    }
}




