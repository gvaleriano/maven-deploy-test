
package br.com.dotum.jedi.protocol;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;


public class EmailAuthenticator extends Authenticator {
	private PasswordAuthentication autentic;
	
	public EmailAuthenticator(String username, String password) {
		autentic = new PasswordAuthentication(username, password);
	}

	public PasswordAuthentication getPasswordAuthentication() {
		return autentic;
	}
}


