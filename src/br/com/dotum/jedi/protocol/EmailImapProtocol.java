
package br.com.dotum.jedi.protocol;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import br.com.dotum.jedi.util.FileUtils;

public class EmailImapProtocol {
	private String host;
	private Integer port;
	private EmailSecurityType securityType;
	private String username;
	private String password;
	private List<File> attach = new ArrayList<File>();

	private Session session;
	private Store store;

	public EmailImapProtocol(String imap, Integer port, String username, String password, EmailSecurityType securityType) throws Exception {
		this.host = imap;
		this.port = port;
		if (port == null) {
			if (securityType.equals(EmailSecurityType.TLS)) {
				this.port = 143;
			} else if (securityType.equals(EmailSecurityType.SSL)) {
				this.port = 993;
			}
		}
		this.securityType = securityType;
		this.username = username;
		this.password = password;

		prepare();
	}

	private void prepare() throws Exception {
		String protocolo = "imap";

		Properties props = new Properties();
			com.sun.mail.util.MailSSLSocketFactory sf = new com.sun.mail.util.MailSSLSocketFactory();
			sf.setTrustAllHosts(true);
			props.put("mail.imap.ssl.socketFactory", sf);
		props.put("mail.imap.starttls.enable", "true");
		props.put("mail.imap.ssl.enable", "true");
		if (securityType.equals(EmailSecurityType.SSL)) {
			protocolo = "imaps";
		}

		session = Session.getInstance(props, null);
		//session.setDebug(true);
		store = session.getStore(protocolo);
		store.connect(host, username, password);
	}


	public void sendMessage(String subject, String content) throws Exception {
		sendMessage(null, subject, content);
	}

	public void sendMessage(Folder folder, String subject, String content) throws Exception {
		if (folder == null) folder = getFolderInbox();

		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(this.username));
		message.setSender(new InternetAddress(this.username));
		message.setSubject(subject);
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(this.username));

		// Parte 1, a mensagem
		Multipart mp = new MimeMultipart();
		MimeBodyPart part1 = new MimeBodyPart();
		part1.setText(content);
		mp.addBodyPart(part1);

		// Parte 2, o anexo
		MimeBodyPart mbPart = new MimeBodyPart();

		for (File f : attach) {
			mbPart = new MimeBodyPart();
			DataSource ds1 = new ByteArrayDataSource(FileUtils.getBytesFromFile(f), FileUtils.getFileMime(f));
			DataHandler dh1 = new DataHandler(ds1);
			mbPart.setDataHandler(dh1);
			mbPart.setFileName(f.getName());
			mbPart.setDisposition("attachment; filename=" + "\""+ f.getName() + "\"");
			mp.addBodyPart(mbPart);
		}
		
		MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);

		message.setContent(mp);
		folder.appendMessages(new Message[]{message});
	}

	public void addAttach(File attach) {
		this.attach.add(attach);
	}

	public void cleanAttach(){
		this.attach = new ArrayList<File>();
	}

	public Store getStore() throws MessagingException{
		return store;
	}

	public Folder getFolderInbox() throws MessagingException {
		return getFolder("Inbox");
	}
	public Folder getFolderTrash() throws MessagingException {
		Folder result = null;
		try {
			result = getFolder("INBOX.Trash");
		} catch (Exception e) {
			result = getFolder("Trash");
		}
		
		return result;
	}

	public Folder getFolder(String pasta) throws MessagingException {
		Store store = getStore();
		Folder folder = store.getFolder(pasta);
		folder.open(Folder.READ_WRITE);
		return folder;
	}

	public Message[] getMessages(Folder folder) throws MessagingException {
		Message[] messages = folder.getMessages();
		return messages;
	}

	public void delete(Folder folder, Message message) throws MessagingException {
		message.setFlag(Flags.Flag.DELETED, true);
		folder.expunge();
	}
	
	public void moveMessage(Folder folderSource, Message message, Folder folderTarget) throws MessagingException {
		Message[] messages = new Message[] {message};
		folderSource.copyMessages(messages, folderTarget);
		folderSource.setFlags(messages, new Flags(Flags.Flag.DELETED), true);
		folderTarget.close(true);
	}

	public void deleteBySubject(String subject) throws MessagingException {
		Folder folder = getFolderInbox();
		Message[] messages = getMessages(folder);
		if (messages.length > 0) {
			for (Message message : messages) {
				if (message.getSubject().equals(subject) == false) continue;
				delete(folder, message);
			}
		}
		folder.expunge();
		folder.close(true);
	}

	/**
	 * Return the map key=FileName value=FileContent
	 * @param message
	 * @return
	 * @throws Exception 
	 */
	public HashMap<String, String> getZipFileAttachmentContent(Message message) throws Exception {
		HashMap<String, String> fileMap = new HashMap<String, String>();
		Multipart multipart = (Multipart) message.getContent();
		for (int i = 0; i < multipart.getCount(); i++) {
			Part part = multipart.getBodyPart(i);
			if (part.getContentType().toLowerCase().contains("zip") == false) continue;
			HashMap<String, String> map = getZipFileContent(part);
			fileMap.putAll(map);
		}
		return fileMap;
	}

	private HashMap<String, String> getZipFileContent(Part part) throws Exception {
		HashMap<String, String> fileMap = new HashMap<String, String>();
		InputStream inputZip = part.getInputStream();
		ZipInputStream zip = new ZipInputStream(inputZip);

		byte buf[] = new byte[100 * 1024];  
		ZipEntry entry;  
		while ((entry = zip.getNextEntry()) != null) {  
			//        	 StringWriter sw = new StringWriter();
			int read = 0;  
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while ((read = zip.read(buf, 0, buf.length)) != -1)  {  
				baos.write(buf, 0, read);
			}  
			baos.flush();  
			baos.close();

			//             sw.write(new String(baos.toByteArray()));
			fileMap.put(entry.getName(), baos.toString("UTF-8"));
			// 			 sw.close(); 
		}                 
		zip.close();  

		return fileMap;
	}

	/**
	 * Return the map key=FileName value=FileContent
	 * @param message
	 * @return
	 * @throws IOException
	 * @throws MessagingException
	 */
	public HashMap<String, String> getFileAttachmentContent(Message message) throws IOException, MessagingException {
		HashMap<String, String> fileMap = new HashMap<String, String>();
		Multipart multipart = (Multipart) message.getContent();
		for (int i = 0; i < multipart.getCount(); i++) {
			Part part = multipart.getBodyPart(i);
			String fileName = part.getFileName();
			if (fileName == null) continue;
			String content = getTxtFileAttachmentContent(part);
			fileMap.put(fileName, content);
		}
		return fileMap;
	}

	public static String getTxtFileAttachmentContent(Part part) throws IOException, MessagingException {
		StringWriter sw = new StringWriter();
		Object obj = part.getInputStream();
		if (obj instanceof InputStream) {
			byte[] bytes = FileUtils.getBytes((InputStream)obj);
			sw.write(new String(bytes, "UTF-8"));
		}
		sw.close();
		return sw.toString();
	}

	public void close() throws MessagingException {
		if (store != null) store.close();
	}

	public class ByteArrayDataSource implements DataSource {
		private byte[] data;
		private String type;

		public ByteArrayDataSource(byte[] data, String type) {
			super();
			this.data = data;
			this.type = type;
		}

		public ByteArrayDataSource(byte[] data) {
			super();
			this.data = data;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getContentType() {
			if (type == null)
				return "application/octet-stream";
			else
				return type;
		}

		public InputStream getInputStream() throws IOException {
			return new ByteArrayInputStream(data);
		}

		public String getName() {
			return "ByteArrayDataSource";
		}

		public OutputStream getOutputStream() throws IOException {
			throw new IOException("Not Supported");
		}
	}
}

