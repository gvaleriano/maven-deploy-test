
package br.com.dotum.jedi.protocol;



public enum EmailSecurityType {
	NONE("none"),
	SMTPS("smtps"),
	TLS("TLS"),
	SSL("SSL"),
	STARTTLS("STARTTLS");
	private String descricao; 

	EmailSecurityType(String descricao) {
		this.descricao = descricao;
	}

	public String toString() {
		return descricao;
	}
}

