

package br.com.dotum.jedi.protocol;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.util.StringUtils;

public class EmailSmtpProtocol {
	private Properties props = new Properties();
	private String host;
	private Integer port;
	private String sender;
	private String from;

	private EmailSecurityType securityType;
	private String username;
	private String password;
	// isso daqui é por causa da merda do SAV 
	// e outros sistemas lixo que tem nessa merda de mercado
	// pqp
	private boolean quotesInNameFile = false;

	private List<String> to = new ArrayList<String>();
	private EmailContentTypeEnum type;
	private List<FileMemory> attach = new ArrayList<FileMemory>();


	public EmailSmtpProtocol(String host, Integer port, String username, String password, EmailSecurityType securityType) {
		this.host = host;
		if (port == null) {
			if (securityType.equals(EmailSecurityType.TLS)) {
				this.port = 25;
			} else if (securityType.equals(EmailSecurityType.SSL)) {
				this.port = 465;
			}
		} else {
			this.port = port;
		}

		this.securityType = securityType;
		this.username = username;
		this.password = password;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setQuotesInNameFile(boolean quotesInNameFile) {
		this.quotesInNameFile = quotesInNameFile;
	}
	public boolean isQuotesInNameFile() {
		return quotesInNameFile;
	}
	public EmailContentTypeEnum getType() {
		return type;
	}
	public void setType(EmailContentTypeEnum type) {
		this.type = type;
	}
	public void setFrom(String from) { this.from = from; }

	public List<FileMemory> getAttach() {
		return attach;
	}
	public void addAttach(FileMemory attach) {
		this.attach.add(attach);
	}
	public void addTo(String email) {
		this.to.add(email);
	}

	public Properties getProperties() {
		return props;
	}

	public void sendMessage(String subject, String content) throws Exception {
		String CHARACTER_SET = "utf-8"; 

		com.sun.mail.util.MailSSLSocketFactory sf = new com.sun.mail.util.MailSSLSocketFactory();
		sf.setTrustAllHosts(true);
		props.put("mail.smtp.ssl.socketFactory", sf);

		// Get system properties
		if (host != null) props.put("mail.smtp.host", host);
		if (port != null) props.put("mail.smtp.port", port);
		props.put("mail.smtp.starttls.enable", "true");

		if (securityType.equals(EmailSecurityType.SSL) == true) {
			props.put("mail.smtp.ssl.enable", "true");
		}

		props.setProperty("mail.smtp.auth", "true");
		EmailAuthenticator auth = new EmailAuthenticator(username, password);
		Session session = Session.getInstance(props, auth);
//		session.setDebug(true);

		// Define message
		MimeMessage message = new MimeMessage(session);

		//
		if (StringUtils.hasValue(this.from)) {
			message.setFrom(new InternetAddress(this.from));
		} else {
			message.setFrom(new InternetAddress(username));
		}

		InternetAddress[] address = new InternetAddress[to.size()]; 
		for (int i = 0; i < to.size(); i++) {
			address[i] = new InternetAddress(to.get(i));
		}
		message.addRecipients(Message.RecipientType.TO, address);

		// Set the subject
		message.setSubject(subject, CHARACTER_SET);

		// Parte 1, a mensagem
		Multipart mp = new MimeMultipart();
		MimeBodyPart part1 = new MimeBodyPart();
		if ((type == null) || (type.equals(EmailContentTypeEnum.TEXT))) {
			part1.setText(content, CHARACTER_SET);
		} else if (type.equals(EmailContentTypeEnum.HTML)) {
			part1.setContent(content, "text/html; charset=" + CHARACTER_SET);
		}
		mp.addBodyPart(part1);

		// Parte 2, o anexo
		MimeBodyPart mbPart = null;
		for (FileMemory fm : attach) {
			mbPart = new MimeBodyPart();
			
			InputStream is = fm.getStream();
			DataSource ds1 = new ByteArrayDataSource(is, fm.getContentType());
			is.close();
			
			DataHandler dh1 = new DataHandler(ds1);
			mbPart.setDataHandler(dh1);
			mbPart.setFileName(fm.getName());
			if (isQuotesInNameFile() == true) {
				mbPart.setDisposition("attachment; filename=" + "\"" + fm.getName() + "\"");
			}
			mp.addBodyPart(mbPart);
		}

		MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
		mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
		mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
		mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
		mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
		mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
		CommandMap.setDefaultCommandMap(mc);

		// Set the content			
		message.setContent(mp);
		Transport.send(message);			
	}
}



