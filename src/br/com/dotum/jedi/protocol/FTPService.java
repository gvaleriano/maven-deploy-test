package br.com.dotum.jedi.protocol;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import br.com.dotum.jedi.util.PropertiesUtils;

public class FTPService {

	private FTPClient ftp = new FTPClient();
	private String server;
	private String username;
	private String password;

	{
			this.server = PropertiesUtils.getString("ftp.server");
			this.username = PropertiesUtils.getString("ftp.username");
			this.password = PropertiesUtils.getString("ftp.password");
	}
	

	public FTPService() {
		super();
	}

	public FTPService(String server, String username, String password) {
		this.server = server;
		this.username = username;
		this.password = password;
	}

	public void connect() throws SocketException, IOException {
		ftp.connect(server);
		ftp.login(username, password);
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
	}

	public void disconnect() throws IOException {
		ftp.disconnect();
	}


	public void upload(String sourceFile) throws FileNotFoundException, IOException {
		upload(sourceFile, null);
	}
	public void upload(String sourceFile, String fileTarget) throws FileNotFoundException, IOException {
		File f = new File(sourceFile);
		upload(f, fileTarget);	
	}
	public void upload(File sourceFile, String fileTarget) throws FileNotFoundException, IOException {
		if (fileTarget == null) fileTarget = sourceFile.getName();
		FileInputStream fis = new FileInputStream(sourceFile);
		upload(fis, fileTarget);
		fis.close();
	}
	public void upload(InputStream souceFile, String fileTarget) throws FileNotFoundException, IOException {
		ftp.deleteFile(fileTarget);
		ftp.appendFile(fileTarget, souceFile);

	}

	public List<String> getListFiles() throws IOException {
		return getListFiles(null);
	}
	public List<String> getListFiles(String path) throws IOException {
		List<String> list = new ArrayList<String>();

		FTPFile[] files = null;
		if (path == null) {
			files = ftp.listFiles();
		} else {
			files = ftp.listFiles(path);
		}

		for (FTPFile file : files) {
			list.add(file.getName());
		}

		return list;
	}

	public List<String> getListAllFiles() throws IOException {
		return getListAllFiles(null, "");
	}
	private List<String> getListAllFiles(List<String> list, String path) throws IOException {
		if (list == null) list = new ArrayList<String>();

		FTPFile[] files = ftp.listFiles(path);
		path = path + File.separator;
		for (FTPFile file : files) {
			list.add(path + file.getName());
			if (file.isDirectory()) {
				getListAllFiles(list, path + file.getName());
			}
		}

		return list;
	}

	public void changeDirectory(String path) throws FileNotFoundException, IOException {
		ftp.changeWorkingDirectory(path);
	}
	public void createDiretory(String path) throws IOException {
		ftp.mkd(path);
	}

	public void download(String nameSource, String fileTarget) throws IOException {
		download(nameSource, new File(fileTarget));
	}
	public void download(String fileSource, File fileTarget) throws IOException {
		OutputStream out = new FileOutputStream(fileTarget);
		ftp.retrieveFile(fileSource, out);
		out.flush();
		out.close();
	}
	public void download(String fileSource, OutputStream out) throws IOException {
		ftp.retrieveFile(fileSource, out);
	}
	public void delete(String fileSource) throws IOException {
		ftp.deleteFile(fileSource);
	}
}



