
package br.com.dotum.jedi.protocol;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;


// essa classe servirá como interface entre
//o sistema e o mecanismo de transferencia
public class FileTransferProtocol {	
	private static Log LOG = LogFactory.getLog(FileTransferProtocol.class);

	public static final String BINARY_MODE = "BINARY";
	public static final String ASCII_MODE = "ASCII";

	
	
	private String host;
	private String username;
	private String password;
	private String mode = BINARY_MODE;
	private FTPClient ftp;	
	private boolean isConnected = false;

	public FileTransferProtocol(String host, String username, String password) {
		super();
		this.host = host;
		this.username = username;
		this.password = password;
	}

	public void disconnect() throws Exception {
		if (ftp != null) {
			ftp.logout();
			ftp.disconnect();
		}
	}
	public void connect() throws Exception {
		if (ftp != null) {
			disconnect();
		}
		ftp = new FTPClient();
		ftp.connect(host);
		showServerReply(ftp);
		LOG.debug("ftp conectado");
		if (FTPReply.isPositiveCompletion(ftp.getReplyCode()) == false) {
			ftp.disconnect();
			throw new Exception("Não foi possivel se conectar no servidor FTP");
		} else {
			boolean logado = ftp.login(this.username,this.password);
			if (logado == false) {
				throw new Exception("Não foi possivel se conectar no servidor FTP com o host " + host + ". Verifique usuario e senha (Usuario: "+ this.username +")");
			}
			
			showServerReply(ftp);
			LOG.debug("ftp usuario e senha informados");
			ftp.enterLocalPassiveMode();
			LOG.debug("ftp: modo passivo");
		}
		isConnected = true;
	}

	public void createDir(String directory) throws Exception {
		if (directory != null) {
			ftp.mkd(directory);
			if (FTPReply.isPositiveCompletion(ftp.getReplyCode()) == false) {
				throw new Exception("Não foi possível acessar diretorio "+directory+" no servidor de FTP: "+this.host);
			}		
			showServerReply(ftp);
		}
	}
	public void changeDir(String directory) throws Exception {
		if (directory != null) {
			ftp.changeWorkingDirectory(directory);
			if (FTPReply.isPositiveCompletion(ftp.getReplyCode()) == false) {
				throw new Exception("Não foi possível acessar diretorio "+directory+" no servidor de FTP: "+this.host);
			}
			showServerReply(ftp);
		}
	}
	
	public String[] list(String pathname) throws Exception {
		FTPFile[] files = ftp.listFiles();			
		String[] result = new String[files.length];
		for (int i = 0; i < files.length; i++) {
			result[i] = files[i].getName();				
		}
		return result;
	}

	public FileMemory receiveFile(String fileName) throws Exception {
		if (isConnected == false) throw new Exception("Primeiro chame o metodo connect()");			
		
		if (mode.equals(BINARY_MODE)) ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
		if (mode.equals(ASCII_MODE)) ftp.setFileType(FTPClient.ASCII_FILE_TYPE);

		InputStream is = ftp.retrieveFileStream(fileName);
		FileMemory fm = new FileMemory(is);
		fm.setName(fileName);
		ftp.completePendingCommand();

		return fm;
	}
	public void receiveFile(String fileName, String store) throws Exception {
		if (isConnected == false) throw new Exception("Primeiro chame o metodo connect()");			
		FileOutputStream fos = new FileOutputStream(new File(store+"/"+fileName));

		if (mode.equals(BINARY_MODE)) ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
		if (mode.equals(ASCII_MODE)) ftp.setFileType(FTPClient.ASCII_FILE_TYPE);

		ftp.retrieveFile(fileName, fos);
		fos.flush();
		fos.close();			
	}

	public void sendFile(FileMemory fm, String mode) throws Exception {
		if (isConnected == false) throw new Exception("Primeiro chame o metodo connect()");
		// seta o modo do arquivo que vai ser transferido
		if (mode.equals(BINARY_MODE)) ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
		if (mode.equals(ASCII_MODE))  ftp.setFileType(FTPClient.ASCII_FILE_TYPE);

		InputStream is = null;
		try {
			is = fm.getStream();
			boolean conseguiu = ftp.storeFile(fm.getName(), is);
			
			if (conseguiu == false)
				throw new Exception("Não foi possivel enviar o arquivo para o servidor de FTP. Verifique o owner da pasta.");
			
		} catch (IOException e) {
			throw new IOException(e.getMessage() + " Check server disk space.", e);
		} finally {
			if (is != null)
				is.close();
		}
	}
	public void sendFile(String fileNameAndPath, String mode) throws Exception {
		FileMemory fm = new FileMemory(fileNameAndPath);
		sendFile(fm, mode);
	}

	@Deprecated
	public void send(String fileNameAndPath) throws Exception {
		sendFile(fileNameAndPath, getMode());
	}
	public void sendFile(String fileNameAndPath) throws Exception {
		sendFile(fileNameAndPath, getMode());
	}
	public void sendFile(FileMemory fm) throws Exception {
		sendFile(fm, getMode());
	}

	public String getMode() {
		return mode;
	}
	public void setMode(String mode) throws Exception {
		this.mode = mode;
	}


	// HELPER
	private static void showServerReply(FTPClient ftpClient) {
        String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                LOG.debug("FTP: " + aReply);
            }
        }
    }

}


