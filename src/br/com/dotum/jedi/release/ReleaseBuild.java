package br.com.dotum.jedi.release;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCommitClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatusClient;
import org.tmatesoft.svn.core.wc.SVNWCClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.protocol.FileTransferProtocol;
import br.com.dotum.jedi.util.ArrayUtils;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.FileUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;


public class ReleaseBuild {
	static Log LOG = LogFactory.getLog(ReleaseBuild.class);

	public static final String NOVO = "[N]";
	public static final String MODIFICADO = "[M]";
	public static final String CONFLITO = "[C]";
	public static final String REMOVIDO = "[R]";

	private static final String PL = "\n";
	private static final String PLA = "\r\n";
	private static final String PREFIX_FILE_DELETE = "filetodelete";
	private static final String EXTENSION_FILE_DELETE = ".txt";
	private static final String PREFIX_FILE_SCRIPT = "scripts";
	private static final String EXTENSION_FILE_SCRIPT = ".sql";
	private static final String PREFIX_FILE_MD5 = "md5";
	private static final String EXTENSION_FILE_MD5 = ".txt";
	private static final String[] FILE_ACCEPT = {".java", ".dwxml", 
			".ddbxml", ".dsrxml", ".drxml", ".dmxml", ".jasper", ".jrxml", ".properties", 
			".xsd", ".css", ".js", ".gif", ".png", ".jpg", ".swf", ".xml", ".jar"};

	private String pathBaseProject;
	private String pathBaseRelease;
	private String pathProjectScript;
	private String fileOfScript;
	private String yearOfPackage = ""+DateUtils.getYear(new Date());
	private List<SvnArquivoBean> svnArquivoList = new ArrayList<SvnArquivoBean>();
	private String scriptFileName;
	private HashMap<String, String> hashMD5 = new HashMap<String, String>();
	private String pathZip;

	private String typeRelease;
	private String versionRelease;
	private String developerRelease;
	private String applicationRelease;
	private String description;
	private String observation;
	private String[] projects;
	private Integer revision;
	private String releaseDepentende = "";
	private String time = "";
	private boolean visible;

	private String ftpHost;			
	private String ftpUsername;			
	private String ftpPassword;			
	private String ftpFolder;

	private String svnRepositoryUrl;
	private String svnUser;
	private String svnPassword;

	//
	// GET's e SET's
	//
	public void setPath(String pathBaseProject, String pathBaseRelease, String pathProjectScript) {
		this.pathBaseProject = pathBaseProject;
		this.pathBaseRelease = pathBaseRelease;
		this.pathProjectScript = pathProjectScript;
	}
	public void setProjectsRelease(String ... projects) throws DeveloperException {
		this.projects = projects;
	}
	public void setFileOfScript(String code) {
		this.fileOfScript = code;
	}
	public void setTypeRelease(String typeRelease) {
		this.typeRelease = typeRelease;
	}
	public void setVersionRelease(String versionRelease) {
		this.versionRelease = versionRelease;
	}
	public void setDeveloperRelease(String developerRelease) {
		this.developerRelease = developerRelease;
	}
	public void setApplicationRelease(String applicationRelease) {
		this.applicationRelease = applicationRelease;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public Integer getRelease() {
		return this.revision;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public List<SvnArquivoBean> getSvnArquivoList() {
		return svnArquivoList;
	}

	public void definePropertiesFtp(String ftpHost, String ftpUsername, String ftpPassword, String ftpFolder){
		this.ftpHost = ftpHost;
		this.ftpUsername = ftpUsername;
		this.ftpPassword = ftpPassword;
		this.ftpFolder = ftpFolder;
	}

	public void definePropertiesSvn(String repositoryUrl, String user, String password){
		this.svnRepositoryUrl = repositoryUrl;
		this.svnUser = user;
		this.svnPassword = password;
	}
	
	public void setInformation(String description, String observation) {
		this.description = description;
		this.observation = observation;
	}


	// 
	// VALIDATORS 
	// 
	public static boolean isValid(String extensao){
		int i = ArrayUtils.search(FILE_ACCEPT, extensao, true);
		if (i != -1) return true;
		return false;
	}
	private void validFileScript(File in) throws Exception {
		StringBuilder sb = new StringBuilder();
		FileMemory fm = new FileMemory(in);
		String[] sql = fm.getText().split("/\\*dotum\\*/");
		for (int i = 0; i < sql.length; i++) {
			String sql_part = sql[i];
			if (sql_part == null) continue;

			sql_part = sql_part.trim();
			if (StringUtils.hasValue( sql_part ) == false) continue;
			if (sql_part.toLowerCase().startsWith("execute")) {
			} else if (sql_part.toLowerCase().startsWith("declare")) {
			} else {
				if (sql_part.endsWith(";")) sb.append("Comando "+ (i+1) +": ("+ sql_part +") - Esta terminando com a extensão ponto e virgula (;). Remova-o!" + PL);
			}
		}
		Integer myNumber = Integer.parseInt(fileOfScript);
		String[] files = FileUtils.getFileList(getPachFileScript());
		for (String file : files) {
			if (file.startsWith("_")) continue;

			String numberFile = file.substring(PREFIX_FILE_SCRIPT.length(), PREFIX_FILE_SCRIPT.length()+3);
			Integer number = Integer.parseInt(numberFile);
			if (number <= myNumber) continue;  
			sb.append("** Existe arquivo de SCRIPT mais recente. Verifique o numero do seu." + PL);
			break;
		}
		if (sb.length() > 5)  {
			String part = PL + "O arquivo de scripts (scripts" + fileOfScript + EXTENSION_FILE_SCRIPT + ") esta com inconsistencias:" + PL;
			throw new DeveloperException(part + sb.toString());
		}
	}

	// 
	// STEP's 
	// 
	public void prepareFiles() throws Exception {
		svnArquivoList.clear();

		DefaultSVNOptions options = SVNWCUtil.createDefaultOptions(true);
		SVNClientManager clientManager = SVNClientManager.newInstance(options, svnUser, svnPassword);
		SVNStatusClient statusClient = clientManager.getStatusClient();
		for (String project : this.projects) {
			File projeto = new File(pathBaseProject + project);
			
			SvnStatusEventHandler statusHandler = new SvnStatusEventHandler();
			statusHandler.setPathProject(pathBaseProject + project);
			statusClient.doStatus(projeto, SVNRevision.HEAD, SVNDepth.INFINITY, true, false, false, false, statusHandler, null); 
			svnArquivoList.addAll(statusHandler.getSvnArquivoList());
		}
		
		clientManager.dispose();
	}

	public void compileClass() throws Exception {
		if (this.svnArquivoList.size() == 0)
			throw new WarningException("Não há arquivos na lista foi encontrado nenhum arquivo para continuar");


		try {
			FileUtils.deleteDirectory(pathBaseRelease);
		} catch (IOException e) {
		}
		FileUtils.createDirectory(pathBaseRelease);

		//
		// COPIANDO PARA UM LUGAR ESPECIFICO
		//
		for (int i = 0; i < svnArquivoList.size(); i++) {
			SvnArquivoBean svnArquivoBean = svnArquivoList.get(i);
			String status = svnArquivoBean.getSvnStatus();
			if (status.equals(REMOVIDO)) continue;

			String pathFileStr = svnArquivoBean.getPathFile();
			String sourcePath = getSourcePath(pathFileStr);
			String targetPath = getTargetPath(pathFileStr, true);
			String[] nomeCompletoDoArquivo = getNomeCompletoDoArquivo(sourcePath, pathFileStr);

			// criando a estrutura de diretorio caso nao exista
			FileUtils.createDirectory(targetPath);

			for (String file : nomeCompletoDoArquivo) {
				//copiando os arquivos da release para uma pasta temporaria
				File in = new File(sourcePath + file);
				File out = new File(targetPath + file);
				FileUtils.copyFile(in, out);
				//
				criarHashMD5(out);
			}
		}
	}

	private void criarHashMD5(File file) throws Exception{
		FileInputStream fis = new FileInputStream(file.getAbsolutePath());
		String md5 = DigestUtils.md5Hex(fis);
		fis.close();
		//
		String pathFileMD5 = file.getAbsolutePath().replaceAll("\\\\", "/");
		pathFileMD5 = pathFileMD5.substring(pathFileMD5.indexOf(pathBaseRelease.replaceAll("\\\\", "/")) + pathBaseRelease.length());
		hashMD5.put(pathFileMD5, md5);
	}

	private String[] getNomeCompletoDoArquivo(String path, String fileStr) {
		String[] result = null;

		Integer posicaoDaUltimaBarra = fileStr.lastIndexOf("/")+1;
		String nomeCompletoDoArquivo = fileStr.substring(posicaoDaUltimaBarra);
		if (nomeCompletoDoArquivo.endsWith(".java")) {
			List<String> list = new ArrayList<String>();
			if (path != null) {
				String[] files = FileUtils.getFileList(path);

				for (String file : files) {
					if (file.startsWith(nomeCompletoDoArquivo.split("\\.")[0] + "$") == false) continue;
					if (file.endsWith(".class") == false) continue;

					list.add(file);
				}
			}
			nomeCompletoDoArquivo = nomeCompletoDoArquivo.replace(".java", ".class");
			list.add(nomeCompletoDoArquivo);
			result = list.toArray(new String[list.size()]);
		} else {
			result = new String[] {nomeCompletoDoArquivo};
		}
		return result;
	}
	private String getSourcePath(String fileStr) {
		Integer posicaoDaUltimaBarra = fileStr.lastIndexOf("/")+1;
		String diretorioCompletoDoArquivo = fileStr.substring(0, posicaoDaUltimaBarra);

		String srcTemp = "/src/main/java/";
		String binTemp = "/target/classes/";
		String webContentTemp = "/WebContent/";
		String libTemp = "/lib/";
		boolean arquivoJava = fileStr.contains(srcTemp);
		boolean arquivoStatic = fileStr.contains(webContentTemp);
		boolean arquivoJar = fileStr.contains(libTemp);


		String sourcePath = null;
		if (arquivoJava) {
			diretorioCompletoDoArquivo = diretorioCompletoDoArquivo.replace(srcTemp, binTemp);
			sourcePath = diretorioCompletoDoArquivo;
		} else if (arquivoJar) {
			sourcePath = diretorioCompletoDoArquivo;
		} else {
			sourcePath = diretorioCompletoDoArquivo;
		}

		return sourcePath;
	}
	private String getTargetPath(String fileStr, boolean fullPath) {
		Integer posicaoDaUltimaBarra = fileStr.lastIndexOf("/")+1;
		String nomeCompletoDoArquivo = (getNomeCompletoDoArquivo(null, fileStr))[0];
		String diretorioCompletoDoArquivo = fileStr.substring(0, posicaoDaUltimaBarra);

		String srcTemp = "/src/main/java/";
		String binTemp = "/target/classes/";
		String webContentTemp = "/WebContent/";
		String libTemp = "/lib/";
		String webInfTemp = "/WEB-INF/";
		boolean arquivoJava = fileStr.contains(srcTemp);
		boolean arquivoStatic = fileStr.contains(webContentTemp);
		boolean arquivoJar = fileStr.contains(libTemp);

		String targetPath = null;
		if (arquivoJava) {
			String diretorio = diretorioCompletoDoArquivo.substring(fileStr.indexOf(srcTemp) + srcTemp.length());
			diretorioCompletoDoArquivo = diretorioCompletoDoArquivo.replace(srcTemp, binTemp);
			if (nomeCompletoDoArquivo.endsWith(".java")) nomeCompletoDoArquivo = nomeCompletoDoArquivo.replace(".java", ".class");
			targetPath = (fullPath == true ? pathBaseRelease : "" ) + webInfTemp + "classes/" + diretorio;
		} else if (arquivoJar) {
			String diretorio = diretorioCompletoDoArquivo.substring(fileStr.indexOf(libTemp) + libTemp.length());
			targetPath = (fullPath == true ? pathBaseRelease : "" ) + webInfTemp + "lib/"  + diretorio;
		} else {
			targetPath = (fullPath == true ? pathBaseRelease : "" );
		}

		return targetPath;
	}
	public void copiaArquivoScript() throws Exception {
		//
		// copia o arquivo de script caso tenha
		//
		if ((StringUtils.hasValue(yearOfPackage)) && 
				(StringUtils.hasValue(fileOfScript))) {
			File in = getFileScript();
			scriptFileName = in.getAbsolutePath().replaceAll("\\\\", "/");

			validFileScript(in);

			File out = new File(pathBaseRelease + "script" + EXTENSION_FILE_SCRIPT);
			FileUtils.copyFile(in, out);
		}
	}
	public void criaArquivoDelete() throws Exception {

		StringBuilder sb = new StringBuilder();
		for(SvnArquivoBean svnArquivoBean : svnArquivoList) {
			String status = (String) svnArquivoBean.getSvnStatus();
			if (status.equals(REMOVIDO) == false) continue;

			String pathFileStr = (String) svnArquivoBean.getPathFile();
			String nomeCompletoDoArquivo2 = (getNomeCompletoDoArquivo(null, pathFileStr))[0];
			String targetPath = getTargetPath(pathFileStr, false);
			if (targetPath.startsWith("/")) targetPath = targetPath.substring(1);
			sb.append(targetPath + nomeCompletoDoArquivo2 + PLA);

			//	enviar arquivo Action XML para se deletada tbm
			if(nomeCompletoDoArquivo2.endsWith(".class") && nomeCompletoDoArquivo2.length() > 14){
				//Fin005Action030Lancar.java
				String managerCode = nomeCompletoDoArquivo2.substring(0, 6).toUpperCase();
				String code = nomeCompletoDoArquivo2.substring(12, 15).toUpperCase();
				code = StringUtils.getNumberOnly(code);
				if(code.length() > 2 && code.startsWith("0")) code = code.substring(1, 3);
				//
				if(StringUtils.hasValue(code)){
					String path = targetPath.replace("/action/", "/");
					sb.append(path+managerCode+"."+code+".daxml" + PLA);
				}
			}
		}

		String conteudoArquivo = sb.toString();
		if(StringUtils.hasValue(conteudoArquivo) == true){
			File fileOut = new File(pathBaseRelease + PREFIX_FILE_DELETE + EXTENSION_FILE_DELETE); 
			FileUtils.createFileFromString(fileOut, conteudoArquivo);
		}
	}

	public void criaArquivoMD5() throws Exception {
		StringBuilder sb = new StringBuilder();
		for(String key : hashMD5.keySet()){
			sb.append(key);
			sb.append("=");
			sb.append(hashMD5.get(key));
			sb.append(PLA);
		}
		if(sb.toString().isEmpty() == false){
			File fileOut = new File(pathBaseRelease + PREFIX_FILE_MD5 + EXTENSION_FILE_MD5); 
			FileUtils.createFileFromString(fileOut, sb.toString());
		}
	}

	public void commitarArquivos() throws Exception {
		DefaultSVNOptions options = SVNWCUtil.createDefaultOptions(true);
		SVNClientManager clientManager = SVNClientManager.newInstance(options, svnUser, svnPassword);
		List<File> fileModifiedList = new ArrayList<File>();
		List<File> fileAddList = new ArrayList<File>();
		List<SVNURL> fileDeletedList = new ArrayList<SVNURL>();
		String msgRetorno = "";

		//
		//ADICIONA O ARQUIVO DE SCRIPT NA LISTA
		if(scriptFileName != null){
			fileAddList.add(new File(scriptFileName));
			fileModifiedList.add(new File(scriptFileName));
		}


		List<String> releaseDepentendeList = new ArrayList<String>();
		for (SvnArquivoBean svnArquivoBean : svnArquivoList) {
			String status = svnArquivoBean.getSvnStatus();
			String pathFile = svnArquivoBean.getPathFile();
			String remotePathFile = svnArquivoBean.getRemotePathFile();
			String author = svnArquivoBean.getAuthor();
			Integer lastCommitedRevision = svnArquivoBean.getLastCommittedRevision();

			if(status.equals(NOVO)){
				fileAddList.add(new File(pathFile));
				fileModifiedList.add(new File(pathFile));

			} else if(status.equals(MODIFICADO)){
				fileModifiedList.add(new File(pathFile));
				//

				if(lastCommitedRevision != null && lastCommitedRevision > 0) {
					String temp = StringUtils.coalesce(author, "")+":"+lastCommitedRevision+", ";
					if (releaseDepentendeList.contains(temp) == false) {
						releaseDepentendeList.add(temp);
						releaseDepentende += temp;
					}
				}

			} else if(status.equals(REMOVIDO)){
				fileDeletedList.add(SVNURL.parseURIEncoded(svnRepositoryUrl+remotePathFile));
				//
				if(lastCommitedRevision != null && lastCommitedRevision > 0) {
					String temp = StringUtils.coalesce(author, "")+":"+lastCommitedRevision+", ";
					if (releaseDepentendeList.contains(temp) == false) {
						releaseDepentendeList.add(temp);
						releaseDepentende += temp;
					}
				}
			}
		}

		//
		// ADICIONADO ARQUIVOS NOVOS PARA O SVN 
		//
		if(fileAddList.isEmpty() == false){
			SVNWCClient wcClient = clientManager.getWCClient();
			wcClient.setCommitHandler(new SvnCommitEventHandler());
			wcClient.doAdd(fileAddList.toArray(new File[fileAddList.size()]), 
					false, 
					false, 
					false, 
					SVNDepth.INFINITY, 
					false, 
					false, 
					false);
		}

		//
		// DELETANDO NO SVN
		//
		SVNCommitClient commitClient = clientManager.getCommitClient();
		commitClient.setCommitHandler(new SvnCommitEventHandler());
		if(fileDeletedList.isEmpty() == false){
			SVNCommitInfo commitInfo = commitClient.doDelete(fileDeletedList.toArray(new SVNURL[fileDeletedList.size()]), this.observation);

			this.revision = new Long(commitInfo.getNewRevision()).intValue();
			if(commitInfo.getErrorMessage() != null){
				msgRetorno = commitInfo.getErrorMessage().getFullMessage();
			}
		}

		//
		// COMMITANDO NO SVN
		//
		if(fileModifiedList.isEmpty() == false){
			SVNCommitInfo commitInfo = commitClient.doCommit(fileModifiedList.toArray(new File[fileModifiedList.size()]), 
					false, 
					this.observation, 
					null, 
					null, 
					false, 
					false, 
					SVNDepth.INFINITY);
			
			this.revision = new Long(commitInfo.getNewRevision()).intValue();
			if(commitInfo.getErrorMessage() != null){
				msgRetorno = commitInfo.getErrorMessage().getFullMessage();
			}
		}

		// aqui é para dar um clear e ver se vai rolar!!
		for (String proj : projects) {
			File f = new File(pathBaseProject+proj);
			clientManager.getWCClient().doCleanup(f);
		}
		
		
		// apos terminar de comitar as coisas.. 
		// tem que chamar este metodo para terminar as conexoes
		clientManager.dispose();

		

		if (StringUtils.hasValue(msgRetorno))
			throw new Exception(msgRetorno);
		
		if(this.revision == null || this.revision <= 0)
			throw new Exception("Não foi possível obter o número da release."+ PL +"Verifique:\"+ PL +\"Se existe algum arquivo bloqueado(locked)."+StringUtils.coalesce(msgRetorno, ""));
	}

	private String prepareStr(String value) {
		if (StringUtils.hasValue(value) == false) return "";
		return value.replaceAll("\"", "").replaceAll("'", "").replaceAll("<", "").replaceAll(">", "");
	}
	
	public void criaArquivoXml() throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("<dotumrelease>" + PL);
		sb.append("  <release>"+ this.revision +"</release>" + PL);
		sb.append("  <application>"+ prepareStr(applicationRelease) +"</application>" + PL);
		sb.append("  <description>"+ prepareStr(this.description) +"</description>" + PL);
		sb.append("  <observation>"+ prepareStr(this.observation) +"</observation>" + PL);
		sb.append("  <type>"+ this.typeRelease +"</type>" + PL);
		sb.append("  <version>"+ this.versionRelease +"</version>" + PL);
		sb.append("  <createStr>"+ FormatUtils.formatDate(new Date(), "yyyy-MM-dd") +"</createStr>" + PL);
		sb.append("  <developerBy>"+ prepareStr(this.developerRelease) +"</developerBy>" + PL);

		if (releaseDepentende.length() > 2000) 
			releaseDepentende = "especial";
		sb.append("  <dependentRelease>"+  releaseDepentende +"</dependentRelease>" + PL);
		//			sb.append("  <changedFile>"+ arquivo[0] +"</changedFile>" + PL);
		//			sb.append("  <deletedFile>"+ arquivo[1] +"</deletedFile>" + PL);
		//			sb.append("  <scriptFile>"+  arquivo[2] +"</scriptFile>" + PL);

		int visivel = 0;
		if (this.visible == true) {
			visivel = 1;
		}
		sb.append("  <time>"+ this.time +" h</time>" + PL);
		sb.append("  <visible>"+ visivel +"</visible>" + PL);
		sb.append("</dotumrelease>" + PL);


		String nameFile = "release.xml";
		File releaseFile = new File(pathBaseRelease + nameFile);
		FileUtils.createFileFromString(releaseFile, sb.toString());
	}
	public void compactarArquivos() throws Exception {
		//
		// COMPACTA TUDO
		//
		int position = -1;
		if (pathBaseRelease.endsWith("/")) {
			position = pathBaseRelease.lastIndexOf("/");
			position = pathBaseRelease.substring(0, position).lastIndexOf("/");
		} else {
			position = pathBaseRelease.lastIndexOf("/");
		}
		pathZip = pathBaseRelease.substring(0, position);
		pathZip += "/release"+ revision +".zip";
		FileUtils.compressFolderZip(pathBaseRelease, pathZip);
	}
	public void enviarArquivoServidorFtp() throws Exception {
		// conectando no ftp ára pegar os releases disponiveis
		FileTransferProtocol ftp = new FileTransferProtocol(ftpHost, ftpUsername, ftpPassword);			
		ftp.connect();
		ftp.changeDir(ftpFolder);
		
		ftp.sendFile(pathZip);
		ftp.disconnect();
	}


	// 
	// HELPER 
	// 
	public String getPachFileScript() {
		return pathBaseProject + pathProjectScript + "/docs/release/scripts"+ yearOfPackage +"/";
	}
	public File getFileScript() {
		if (fileOfScript == null) return null;
		return new File(getPachFileScript() + PREFIX_FILE_SCRIPT + fileOfScript + EXTENSION_FILE_SCRIPT);
	}
}