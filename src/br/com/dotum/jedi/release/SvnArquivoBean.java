package br.com.dotum.jedi.release;

public class SvnArquivoBean {

	private boolean directory = false;
	private String pathFile;
	private String remotePathFile;
	private String svnStatus;
	private String author;
	private Integer lastCommittedRevision;
	private Boolean locked;
	
	public String getPathFile() {
		return pathFile;
	}
	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}
	public String getRemotePathFile() {
		return remotePathFile;
	}
	public void setRemotePathFile(String remotePathFile) {
		this.remotePathFile = remotePathFile;
	}
	public String getSvnStatus() {
		return svnStatus;
	}
	public void setSvnStatus(String svnStatus) {
		this.svnStatus = svnStatus;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Integer getLastCommittedRevision() {
		return lastCommittedRevision;
	}
	public void setLastCommittedRevision(Integer lastCommittedRevision) {
		this.lastCommittedRevision = lastCommittedRevision;
	}
	public Boolean getLocked() {
		return locked;
	}
	public void setLocked(Boolean locked) {
		this.locked = locked;
	}
	public boolean isDirectory() {
		return directory;
	}
	public void setDirectory(boolean directory) {
		this.directory = directory;
	}
}
