package br.com.dotum.jedi.release;


import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.wc.ISVNCommitHandler;
import org.tmatesoft.svn.core.wc.SVNCommitItem;

public class SvnCommitEventHandler implements ISVNCommitHandler {

	@Override
	public String getCommitMessage(String message, SVNCommitItem[] commitables) throws SVNException {
		return message;
	}

	@Override
	public SVNProperties getRevisionProperties(String message, SVNCommitItem[] commitables, SVNProperties revisionProperties) throws SVNException {
		return revisionProperties;
	}

}
