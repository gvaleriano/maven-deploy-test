package br.com.dotum.jedi.release;

import java.util.ArrayList;
import java.util.List;

import org.tmatesoft.svn.core.wc.ISVNStatusHandler;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusType;

import br.com.dotum.jedi.util.StringUtils;

public class SvnStatusEventHandler implements ISVNStatusHandler {

	private String pathProject;
	private List<SvnArquivoBean> svnDirectoryList = new ArrayList<SvnArquivoBean>();
	private List<SvnArquivoBean> svnArquivoList = new ArrayList<SvnArquivoBean>();

	public void handleStatus(SVNStatus status) {
		SVNStatusType contentsStatus = status.getContentsStatus();
		SVNStatusType propertiesStatus = status.getPropertiesStatus();
		long lastCommittedRevision = status.getCommittedRevision().getNumber();
		long remoteRevision = status.getRemoteRevision().getNumber();
		String pathFile = status.getFile().getAbsolutePath().replaceAll("\\\\", "/");
		String repositoryRelativePath =  StringUtils.coalesce(status.getRepositoryRelativePath(), "").replaceAll("\\\\", "/");

		if(status.getFile().isDirectory()) {
			// tem que ver se é diretorio novo e pegar os arquivos de dentro para gravar tbm
			String pathFileTemp = pathFile.substring(pathFile.indexOf("/"));
			if (pathProject == null) return;
			if (pathFileTemp.startsWith( pathProject + "/src/") == false) return;
			
			String st = getStatus(status, contentsStatus, propertiesStatus, lastCommittedRevision, remoteRevision);
			if(st.isEmpty() == false){
				SvnArquivoBean bean = new SvnArquivoBean();
				bean.setPathFile(pathFile);
				bean.setRemotePathFile(repositoryRelativePath);
				bean.setSvnStatus(st);
				bean.setLastCommittedRevision(new Long(lastCommittedRevision).intValue());
				bean.setAuthor(status.getAuthor());
				bean.setLocked(status.isLocked());
				bean.setDirectory(true);

				svnDirectoryList.add(bean);
			}
		} else {
			int i = pathFile.lastIndexOf('.');
			String extension = "";
			if (i > 0) {
				extension = pathFile.substring(i);
			}
			if(ReleaseBuild.isValid(extension) == false) return;

			String st = getStatus(status, contentsStatus, propertiesStatus, lastCommittedRevision, remoteRevision);
			if(st.isEmpty() == false){
				SvnArquivoBean bean = new SvnArquivoBean();
				bean.setPathFile(pathFile);
				bean.setRemotePathFile(repositoryRelativePath);
				bean.setSvnStatus(st);
				bean.setLastCommittedRevision(new Long(lastCommittedRevision).intValue());
				bean.setAuthor(status.getAuthor());
				bean.setLocked(status.isLocked());

				svnArquivoList.add(bean);
			}
		}

	}

	private String getStatus(SVNStatus status, SVNStatusType contentsStatus, SVNStatusType propertiesStatus, long lastCommittedRevision, long remoteRevision) {
		String st = "";
		if(contentsStatus.toString().equals(SVNStatusType.STATUS_MODIFIED.toString())  && lastCommittedRevision < remoteRevision){
			st = ReleaseBuild.CONFLITO;
		} else if(status.isVersioned() == false && lastCommittedRevision <= 0){
			st = ReleaseBuild.NOVO;
		} else if(status.getNodeStatus().toString().equals(SVNStatusType.STATUS_DELETED.toString())){
			st = ReleaseBuild.REMOVIDO;
		}
		
		if(st.isEmpty()){
			if(contentsStatus.toString().equals(SVNStatusType.STATUS_MODIFIED.toString())) {
				st = ReleaseBuild.MODIFICADO;
			} else if(contentsStatus.toString().equals(SVNStatusType.STATUS_CONFLICTED.toString())) {
				st = ReleaseBuild.CONFLITO;
			} else if(contentsStatus.toString().equals(SVNStatusType.STATUS_ADDED.toString())) {
				st = ReleaseBuild.MODIFICADO;
			} else if(contentsStatus.toString().equals(SVNStatusType.STATUS_DELETED.toString())) {
				st = ReleaseBuild.REMOVIDO;
			} else if(contentsStatus.toString().equals(SVNStatusType.STATUS_UNVERSIONED.toString())) {
				st = ReleaseBuild.NOVO;
			}

			if(propertiesStatus.toString().equals(SVNStatusType.STATUS_MODIFIED.toString())) {
				st = ReleaseBuild.MODIFICADO;
			} else if(propertiesStatus.toString().equals(SVNStatusType.STATUS_CONFLICTED.toString())) {
				st = ReleaseBuild.CONFLITO;
			}
		}
		
		return st;
	}

	public List<SvnArquivoBean> getSvnArquivoList(){
		return svnArquivoList;
	}
	public List<SvnArquivoBean> getSvnDiretorioList(){
		return svnDirectoryList;
	}

	public String getPathProject() {
		return pathProject;
	}

	public void setPathProject(String pathProject) {
		this.pathProject = pathProject;
	}
}
