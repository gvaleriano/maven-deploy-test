package br.com.dotum.jedi.security;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import br.com.dotum.jedi.core.PropertiesConstants;
import br.com.dotum.jedi.util.PropertiesUtils;

public class DESEncrypter {
	
	
	private static final String METHOD_ENCRYPT_DES = "DES"; // default 1977
	private static final String METHOD_ENCRYPT_AES = "AES"; // advanced 2000
	private static String METHOD_ENCRYPT;
	private Cipher ecipher;
	private Cipher dcipher;	

	static {
		String tipo = PropertiesUtils.getString(PropertiesConstants.SYSTEM_PACKAGE_BASE);
		if (tipo == null) tipo = "dotum";
		if (tipo.equals("sicoob")) {
			METHOD_ENCRYPT = METHOD_ENCRYPT_AES;
		} else {
			METHOD_ENCRYPT = METHOD_ENCRYPT_DES;
		}
	}

	
	public DESEncrypter(byte[] chave) throws Exception {
        SecretKey key = new SecretKeySpec(chave, METHOD_ENCRYPT);

		ecipher = Cipher.getInstance(METHOD_ENCRYPT);
		dcipher = Cipher.getInstance(METHOD_ENCRYPT);
		ecipher.init(Cipher.ENCRYPT_MODE, key);
		dcipher.init(Cipher.DECRYPT_MODE, key);
	}

	public String encrypt(String str) {
		try {
			// Encode the string into bytes using utf-8
			byte[] utf8 = str.getBytes("UTF8");

			// Encrypt
			byte[] enc = ecipher.doFinal(utf8);

			// Encode bytes to base64 to get a string
			
			Base64 base64 = new Base64(2000, new byte[]{});
			return base64.encodeToString(enc);
		} catch (javax.crypto.BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

	public String decrypt(String str) {
		try {
			// Decode base64 to get bytes
			Base64 base64 = new Base64(2000, new byte[]{});
			byte[] dec = base64.decode(str);

			// Decrypt
			byte[] utf8 = dcipher.doFinal(dec);

			// Decode using utf-8
			return new String(utf8, "UTF8");
		} catch (javax.crypto.BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}
}





