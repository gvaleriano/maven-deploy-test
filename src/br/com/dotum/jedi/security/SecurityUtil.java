package br.com.dotum.jedi.security;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.HashMap;

import br.com.dotum.jedi.core.exceptions.JediException;
import br.com.dotum.jedi.core.exceptions.LoginException;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;

public class SecurityUtil {
	private static Log LOG = LogFactory.getLog(SecurityUtil.class);

	private static HashMap<Integer, DESEncrypter> map = new HashMap<Integer, DESEncrypter>();
	public static final Integer MOD1 = 1;
	public static final Integer MOD2 = 2;
	public static final Integer MOD3 = 3;
	public static final Integer MOD4 = 4;
	public static final Integer MOD5 = 5;

	
	
	private static byte[] getByteByString(String chave) {
		if (StringUtils.hasValue(chave) == false) 
			LOG.error("Chave nao existente no arquivo de propriedade: " + chave);

		String[] fraseBite = chave.split(",");
		byte[] byteArray = new byte[fraseBite.length]; 
		BigInteger val = null;
		for (int i = 0; i < fraseBite.length; i++) {
			val = new BigInteger(fraseBite[i].trim());
			byteArray[i]= val.byteValue();
		}

		return byteArray;
	}


	static {
		for (int i = 1; ; i++) {
			String chave = PropertiesUtils.getString("security.key"+i);
			if (chave == null) break;
			
			try {
				DESEncrypter des1 = new DESEncrypter(getByteByString(chave));
				map.put(i, des1);
			} catch (Exception e) {
				LOG.error("Erro na chave de segurança "+ i +". Veja o arquivo de propriedade");
			}
		}
	}

	public static String encrypt(Integer key, String value) throws LoginException {
		try {
			DESEncrypter result = map.get(key);
			if (result == null)
				throw new LoginException("Não existe a chave de segurança com a key " + key);
			return result.encrypt(value);
		} catch (Exception ex) {
			throw new LoginException(ex);
		}		
	}

	public static final String decrypt(Integer key, String value) throws LoginException {
		try {
			DESEncrypter result = map.get(key);
			if (result == null)
				throw new LoginException("Não existe a chave de segurança com a key " + key);
			
			return result.decrypt(value);
		} catch (Exception ex) {
			throw new LoginException(ex);
		}
	}

	public static String encrypt(String value) throws LoginException {
		try {
			return map.get(1).encrypt(value);
		} catch (Exception ex) {
			throw new LoginException(ex);
		}		
	}

	public static final String decrypt(String value) throws LoginException {
		try {
			return map.get(1).decrypt(value);
		} catch (Exception ex) {
			throw new LoginException(ex);
		}
	}

	public static String encryptMD5(String word) throws JediException {
		try {
			if (StringUtils.hasValue(word) == false) return null;
			
			MessageDigest md = MessageDigest.getInstance("MD5");
			BigInteger hash = new BigInteger(1, md.digest(word.getBytes()));
			String s = hash.toString(16);
			if (s.length() %2 != 0)
				s = "0" + s;
			return s;
		} catch (Exception e) {
			throw new JediException(e.getMessage(), e);
		}
	}

}
