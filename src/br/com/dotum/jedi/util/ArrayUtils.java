


package br.com.dotum.jedi.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.JPopupMenu.Separator;

import br.com.dotum.jedi.core.table.Bean;

public final class ArrayUtils {

	public final static int search(String[] array, String value, boolean ignoreCase) {

		for (int i = 0; i < array.length; i++) {
			if (ignoreCase == true) {
				if (array[i].equalsIgnoreCase(value)) {
					return i;
				}
			} else {
				if (array[i].equals(value)) {
					return i;
				}
			}
		}
		return -1;        
	}

	public final static int search(Integer[] array, Integer value) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(value)) {
				return i;
			}
		}
		return -1;        
	}

	public final static Integer[] splitToInt(String value, String regex){
		if ((value == null) || (value.isEmpty() == true)) return null;
		try {
			String[] values = value.split(regex);
			Integer[] result = new Integer[values.length];

			for (int i = 0; i < values.length; i++) {
				if (values[i].equals("null")) {
					result[i] = null;
				} else {
					result[i] = Integer.parseInt(values[i]); 
				}
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public final static Integer[] parseToInt(String[] array) throws ParseException {
		Integer[] result = null;

		if (array == null) {
			result = new Integer[0]; 
		} else {
			result = new Integer[array.length]; 
			for (int i = 0; i < array.length; i++) {
				if (array[i] == null) {
					result[i] = null;
				} else {
					result[i] = FormatUtils.parseInt(array[i].trim());
				}
			}
		}

		return result;
	}

	public final static Integer[] parseToInt(Object[] oArray) throws ParseException {
		Integer[] result = null;

		if (oArray == null) {
			result = new Integer[0]; 
		} else {
			result = new Integer[oArray.length]; 
			for (int i = 0; i < oArray.length; i++) {
				if (oArray[i] instanceof Integer) {
					result[i] = (Integer)oArray[i];
				} else {
					result[i] = FormatUtils.parseInt((String)oArray[i]);

				}
			}
		}

		return result;
	}

	/**
	 * Transforma um array de <b>String</b> que esta no formato dd/MM/yyyy para um array de <b>Date</b>.<br/>
	 * Exemplo:<br/>
	 * <blockquote><pre>
	 * String[] sArray = new String[] {"10/08/2017", "19/08/2017", "21/08/2017"};
	 * Date[] dArray =  ArrayUtils.parseToDate(sArray);
	 * </pre></blockquote>
	 * <br/> 
	 * @param sArray
	 * @return Date[]
	 */
	public final static Date[] parseToDate(String[] sArray) throws ParseException {
		Date[] result = null;

		if (sArray == null) {
			result = new Date[0]; 
		} else {
			result = new Date[sArray.length]; 
			for (int i = 0; i < sArray.length; i++) {
				result[i] = FormatUtils.parseDate(sArray[i]);
			}
		}

		return result;
	}

	public final static Date[] parseToDate(Object[] oArray) throws ParseException {
		Date[] result = null; 

		if (oArray == null) {
			result = new Date[0]; 

		} else {
			result = new Date[oArray.length];
			for (int i = 0; i < oArray.length; i++) {
				if (oArray[i] == null) {
					result[i] = null;
				} else {
					result[i] = (Date)oArray[i];
				}
			}
		}

		return result;
	}

	public final static Double[] parseToDouble(String[] sArray, String separator) throws ParseException {
		Double[] result = null;

		if (sArray == null) {
			result = new Double[0]; 
		} else {
			result = new Double[sArray.length]; 
			for (int i = 0; i < sArray.length; i++) {
				String temp = sArray[i];
				temp = temp.replaceAll(separator, ".");
				result[i] = FormatUtils.parseDouble(temp);
			}
		}

		return result;
	}

	public final static Double[] parseToDouble(String[] sArray) throws ParseException {
		Double[] result = null;

		if (sArray == null) {
			result = new Double[0]; 
		} else {
			result = new Double[sArray.length]; 
			for (int i = 0; i < sArray.length; i++) {
				if (sArray[i] == null) {
					result[i] = null;
				} else {
					result[i] = FormatUtils.parseDouble((String) sArray[i]);
				}
			}
		}

		return result;
	}

	public final static Double[] parseToDouble(Object[] oArray) throws ParseException {
		Double[] result = null;

		if (oArray == null) {
			result = new Double[0]; 
		} else {
			result = new Double[oArray.length]; 
			for (int i = 0; i < oArray.length; i++) {
				if (oArray[i] == null) {
					result[i] = null;
				} else {
					result[i] = (Double) oArray[i];
				}
			}
		}

		return result;
	}

	public final static BigDecimal[] parseToBigDecimal(Object[] oArray) throws ParseException {
		BigDecimal[] result = null;

		if (oArray == null) {
			result = new BigDecimal[0]; 
		} else {
			result = new BigDecimal[oArray.length]; 
			for (int i = 0; i < oArray.length; i++) {
				if (oArray[i] == null) {
					result[i] = null;
				} else {
					result[i] = (BigDecimal) oArray[i];
				}
			}
		}

		return result;
	}

	/**
	 * Transforma um array de <b>Object</b> (podendo ser um tipo especifico) para um array de <b>String</b>.<br/>
	 * Exemplo:<br/>
	 * <blockquote><pre>
	 * Integer[] iArray = new Integer[] {"abc", "def", "ghi", "jkl"};
	 * String in =  ArrayUtils.stringToIn(iArray);
	 * </pre></blockquote>
	 * Equivalente a:
	 * <blockquote><pre>
	 * String in = "("abc", "def", "ghi", "jkl");"<br/>
	 * </pre></blockquote>
	 * <br/> 
	 * @param sArray
	 * @return String
	 */
	public final static String[] parseToString(Object[] value) throws Exception {
		if (value == null) 
			return null;

		String[] result = new String[value.length];
		for (int i = 0; i < value.length; i++) {
			if (value[i] == null) {
				result[i] = null;
			} else if (value[i] instanceof String) {
				result[i] = (String)value[i];
			} else if (value[i] instanceof Double) {
				result[i] = FormatUtils.formatDouble((Double)value[i]);
			} else if (value[i] instanceof Integer) {
				result[i] = "" + (Integer)value[i];
			} else if (value[i] instanceof Date) {
				result[i] = FormatUtils.formatDate((Date)value[i]);
			} else {
				throw new Exception("Tipo de campo com valor desconhecido.");
			}
		}
		return result;
	}

	/**
	 * Transforma um array de <b>String</b> em uma string entre parenteses separado por vincula.<br/>
	 * Exemplo:<br/>
	 * <blockquote><pre>
	 * Integer[] iArray = new Integer[] {"abc", "def", "ghi", "jkl"};
	 * String in =  ArrayUtils.stringToIn(iArray);
	 * </pre></blockquote>
	 * Equivalente a:
	 * <blockquote><pre>
	 * String in = "("abc", "def", "ghi", "jkl");"<br/>
	 * </pre></blockquote>
	 * <br/> 
	 * @param sArray
	 * @return String
	 */
	public final static String stringToIn(String[] sArray) {
		return stringToIn(sArray, ",", "(", ")", true);
	}
	
	public final static String join(Object[] oArray, String sepatator) {
		return stringToIn(oArray, sepatator, null, null, false);
	}

	public final static String stringToIn(Object[] sArray, String separator, String charStart, String charEnd, boolean isEncapsulated) {
		if(sArray == null || sArray.length == 0) return null;
		
		StringBuilder sb = new StringBuilder();

		String temp = "";
		if (StringUtils.hasValue(charStart)) {
			sb.append(charStart);
		}

		for(Object node : sArray) {
			if(node == null) continue;

			sb.append( temp );
			
			if(isEncapsulated == true) 
				sb.append( "\"");
			
			
			sb.append(node);
			
			
			if(isEncapsulated == true) 
				sb.append( "\"");

			temp = separator;
		}

		if (StringUtils.hasValue(charEnd)) {
			sb.append(charEnd);
		}

		return sb.toString();
	}

	/**
	 * Transforma um array de <b>Integer</b> em uma string entre parenteses separado por vincula.<br/>
	 * Exemplo:<br/>
	 * <blockquote><pre>
	 * Integer[] iArray = new Integer[] {10, 20, 15, 37};
	 * String in =  ArrayUtils.stringToIn(iArray);
	 * </pre></blockquote>
	 * Equivalente a:
	 * <blockquote><pre>
	 * String in = "(10, 20, 15, 37);"<br/>
	 * </pre></blockquote>
	 * <br/> 
	 * @param iArray
	 * @return String
	 */
	public final static String stringToIn(Integer[] iArray) {
		return stringToIn(iArray, ",", "(", ")", false);
	}

	public static Integer[] contem(List<Integer> a, List<Integer> b) {
		return contem(a.toArray(new Integer[a.size()]), b.toArray(new Integer[b.size()]));
	}
	public static Integer[] contem(Integer[] a, Integer[] b) {
		List<Integer> result = new ArrayList<Integer>(); 

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b.length; j++) {
				if (a[i] != null && b[j] != null && a[i].equals(b[j]) == false) continue;
				result.add(a[i]);
			}
		}

		return result.toArray(new Integer[result.size()]);
	}


	public static Integer[] naoContem(List<Integer> a, List<Integer> b) {
		return naoContem(a.toArray(new Integer[a.size()]), b.toArray(new Integer[b.size()]));
	}
	public static Integer[] naoContem(Integer[] a, Integer[] b) {
		List<Integer> result = new ArrayList<Integer>(); 

		for (int i = 0; i < a.length; i++) {
			boolean tem = false;

			for (int j = 0; j < b.length; j++) {
				if (a[i] != null && b[j] != null && a[i].equals(b[j])) {
					tem = true;
				}
			}

			if (tem == false)
				result.add(a[i]);
		}

		return result.toArray(new Integer[result.size()]);
	}

	public static void orderBy(List<? extends Bean> list, String attribute) {
		Collections.sort(list, new Comparator<Bean>() {
			public int compare(Bean o1, Bean o2) {
				if(o1.get(attribute) instanceof Integer) {
					return ((Integer)o1.get(attribute)).compareTo(o2.get(attribute));
				} else if(o1.get(attribute) instanceof String) {
					return ((String)o1.get(attribute)).compareTo(o2.get(attribute));
				} else if(o1.get(attribute) instanceof BigDecimal) {
					return ((BigDecimal)o1.get(attribute)).compareTo(o2.get(attribute));
				} else if(o1.get(attribute) instanceof Double) {
					return ((Double)o1.get(attribute)).compareTo(o2.get(attribute));
				} else {
					return o1.get(attribute);
				}

			}
		});
	}

}