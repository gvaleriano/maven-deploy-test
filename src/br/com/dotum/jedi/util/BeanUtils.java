package br.com.dotum.jedi.util;

import java.util.ArrayList;
import java.util.List;

import br.com.dotum.jedi.core.table.Bean;

public class BeanUtils {

	public static Integer[] getPropertieByListToArray(List<? extends Bean> list, String propertie) {
		List<Integer> array = new ArrayList<Integer>();
		for (Bean bean : list) {
			Integer value = (Integer)bean.get(propertie);
			array.add(value);
		}
		
		return array.toArray(new Integer[array.size()]);
	}

	
}
