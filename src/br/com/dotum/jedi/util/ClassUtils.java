/*

 * JForms(TM) - The Open-Source JForms API.
 */
package br.com.dotum.jedi.util;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import com.itextpdf.text.log.SysoCounter;


/**
 * 
 * @author <a href="mailto:fabio@jforms.org">Fabio Patricio</a>
 * @version 0.1
 * @since 0.1
 */
public final class ClassUtils {

	private static Log LOG = LogFactory.getLog(ClassUtils.class);
	/**
	 * 
	 * @param object
	 * @param methodName
	 * @return
	 */
	public final static Object invokeMethodGet(Object object, String methodName) throws Exception {
		Object result = null;
		Method method = null;
		try {

			if (existsMethod(object, methodName)) {
				method = getMethodToExecute(object, methodName, method, null);
				result = invokeMethod(method, object, null);
			}
		} catch (Exception e) {
			LOG.error(e.toString());
			throw new Exception(e.getMessage());
		}
		return result;
	}
	/**
	 * 
	 * @param object
	 * @param methodName
	 * @param methodValue
	 */
	public final static void invokeMethodSet(Object object, String methodName, Object methodValue) throws Exception {
		//Object result = null;
		Object[] param = null;
		Method method = null;
		Class<?>[] partypes = null;
		//
		param = new Object[1];
		partypes = new Class[1];
		param[0] = methodValue;
		getPartypes(methodValue, partypes);
		try {

			if (existsMethod(object, methodName)) {
				method = getMethodToExecute(object, methodName, method, partypes);
				invokeMethod(method, object, param);
			}
		} catch (Exception e) {
			LOG.error(e.toString());
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * 
	 * @param object
	 * @param methodName
	 * @param methodValue
	 */
	public final static void invokeMethodSetNull(Object object, String methodName, Class<?> klass) throws Exception {
		//Object result = null;
		Object[] param = null;
		Method method = null;
		Class<?>[] partypes = null;
		//
		param = new Object[1];
		partypes = new Class[] {klass};
		param[0] = null;
		try {
			if (existsMethod(object, methodName)) {
				method = getMethodToExecute(object, methodName, method, partypes);
				invokeMethod(method, object, param);
			}
		} catch (Exception e) {
			LOG.error(e.toString());
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * 
	 * @param Object to find all fields.
	 * @return Array of Fields of this Object.
	 */
	public final static Field[] getAttributes(Object object) {
		return getAttributes(object.getClass());        
	}
	/**
	 * 
	 * @param Object to find all fields.
	 * @return Array of Fields of this Object.
	 */
	public final static Field[] getAttributes(Class<?> clazz) {
		return clazz.getDeclaredFields();        
	}
	/**
	 * 
	 * @param Object to find all methods
	 * @return Array of Methods of this Object.
	 */
	public final static Method[] getMethods(Object object) {
		return getMethods(object.getClass());        
	}
	/**
	 * 
	 * @param Object to find all methods
	 * @return Array of Methods of this Object.
	 */
	public final static Method[] getMethods(Class<?> clazz) {
		return clazz.getMethods();        
	}
	/**
	 * 
	 * @param Object to find field with name equal parameter name.
	 * @param Name of the Field
	 * @return Field with name equal parameter name.
	 */
	public final static Field getAttribute(Object obj, String name) throws Exception {
		Field field = null;
		try {
			field = obj.getClass().getDeclaredField(name);
		} catch (Exception e) {
			LOG.error(e.toString());
			throw new Exception(e.getMessage());
		}
		return field;
	}
	/**
	 * 
	 * @param obj
	 * @return result
	 */
	public final static String[] getNameAttributes(Object obj) {
		Field[] fields = ClassUtils.getAttributes(obj);
		String[] result = new String[fields.length];
		for (int i=0; i < fields.length; i++) {
			result[i] = fields[i].getName();
		}
		return result;       
	}
	/**
	 * 
	 * @param clazz
	 * @return
	 */
	public final static Object buildObject(Class<?> clazz) {
		return getInstance(clazz, null, null);
	}
	/**
	 * 
	 * @param className
	 * @param partypes
	 * @param parameters
	 * @return
	 */
	public final static Object buildObject(String className) {
		return getInstance(getClass(className), null, null);
	}
	/**
	 * 
	 * @param className
	 * @param partypes
	 * @param parameters
	 * @return
	 */
	public final static Object buildObject(String className, Class<?>[] partypes, Object[] parameters) {
		return getInstance(getClass(className), partypes, parameters);
	}
	/**
	 * 
	 * @param clazz
	 * @param partypes
	 * @param parameters
	 * @return
	 */
	public final static Object getInstance(Class<?> clazz, Class<?>[] partypes, Object[] parameters) {
		Object obj = null;
		try {
			Constructor<?> constructor = getConstructor(partypes, clazz);
			obj = getInstance(parameters, constructor);
		} catch (InstantiationException e) {
			LOG.error(e.toString());
		} catch (IllegalAccessException e) {
			LOG.error(e.toString());
		}
		return obj;
	}
	/**
	 * 
	 * @param object
	 * @param methodName
	 * @return
	 * @throws Exception
	 */
	public final static Method getMethod(Object object, String methodName) throws Exception {
		return getMethod(object,methodName,null);
	}
	/**
	 * @param obj
	 * @param methodName
	 * @param partypes
	 * @return method
	 */
	public final static Method getMethod(Object obj, String methodName, Class<?>[] partypes) throws Exception {
		Method method = null;
		try {
			method = obj.getClass().getDeclaredMethod(methodName, partypes);
		} catch (Exception e) {
			LOG.error(e.toString());
			throw new Exception(e.getMessage());
		}
		return method;
	}
	/*
	 * Private and auxiliar methods.
	 */
	/**
	 * @param obj
	 * @param methodName
	 * @param partypes
	 * @return method
	 */
	public final static Method getMethod(Class<?> clazz, String methodName, Class<?>[] partypes) throws Exception {
		Method method = null;
		method = clazz.getDeclaredMethod(methodName, partypes);
		return method;
	}
	/**
	 * @param className
	 * @return
	 */
	public final static Class<?> getClass(String className) {
		Class<?> classDef = null;
		try {
			classDef = Class.forName(className);
		} catch (ClassNotFoundException e) {
			LOG.error(e.toString());
		}
		return classDef;
	}
	/**
	 * @param parameters
	 * @param obj
	 * @param constructor
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private static Object getInstance(Object[] parameters, Constructor<?> constructor) throws InstantiationException, IllegalAccessException {
		Object obj = null;
		try {
			obj = constructor.newInstance(parameters);
		} catch (IllegalArgumentException e) {
			LOG.error(e.toString());
		} catch (InvocationTargetException e) {
			LOG.error(e.toString());
		}
		return obj;
	}
	/**
	 * @param partypes
	 * @param classDef
	 * @return
	 */
	private static Constructor<?> getConstructor(Class<?>[] partypes, Class<?> classDef) {
		Constructor<?> constructor = null;
		try {
			constructor = classDef.getConstructor(partypes);
		} catch (SecurityException e) {
			LOG.error(e.toString());
		} catch (NoSuchMethodException e) {
			LOG.error(e.toString());
		}
		return constructor;
	}
	/*
	 * 
	 * 
	 */
	/**
	 * @param object
	 * @param result
	 * @param method
	 * @return
	 */
	private static Object invokeMethod(Method method, Object object, Object[] param) throws Exception {
		Object result = null;
		try {
			result = method.invoke(object, param);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new Exception(e);
		}
		return result;
	}
	/**
	 * @param object
	 * @param methodName
	 * @param method
	 * @param partypes
	 * @return
	 */
	private static Method getMethodToExecute(Object object, String methodName, Method method, Class<?>[] partypes) throws Exception {
		method = getMethod(object, methodName, partypes);
		if (method == null) {
			method = getMethodByName(object, methodName);
		}
		return method;
	}
	/**
	 * 
	 * @param object
	 * @param methodName
	 * @param partypes
	 * @return exists
	 */
	private static Method getMethodByName(Object object, String methodName) {
		Method methods[] = null;
		Method method = null;
		//
		methods = getMethods(object);
		for (int i = 0; i < methods.length; i++) {
			if (methodName.equals(methods[i].getName())) {
				method = methods[i];
				break;
			}
		}
		if (method == null) {
			LOG.warn("Method " + methodName + " does not exists in class " + object.getClass().getName() + ".");
		}
		return method;
	}
	/**
	 * 
	 * @param obj
	 * @param methodName
	 * @param partypes
	 * @return exists
	 */
	public final static boolean existsMethod(Object obj, String methodName) {
		Method method[] = null;
		boolean exists = false;
		if (obj != null) {
			method = getMethods(obj);
			for (int i = 0; i < method.length; i++) {
				if (methodName.equals(method[i].getName())) {
					exists = true;
					break;
				}
			}
			if (!exists) {
				LOG.warn("Method " + methodName + " does not exists in class " + obj.getClass().getName() + ".");
			}
		}
		return exists;
	}
	/**
	 * @param methodValue
	 * @param partypes
	 */
	private static void getPartypes(Object methodValue, Class<?>[] partypes) {
		if (methodValue != null) {
			partypes[0] = methodValue.getClass();
		}
	}

	/**
	 * Scans all classes accessible from the context class loader which belong
	 * to the given package and subpackages.
	 * 
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public final static List<Class<?>> getClasses(String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			String fileName = resource.getFile();
			String fileNameDecoded = URLDecoder.decode(fileName, "UTF-8");
			dirs.add(new File(fileNameDecoded));
		}
		List<Class<?>> classes = new ArrayList<Class<?>>();

		StringBuilder sb = new StringBuilder();
		for (File directory : dirs) {
			if (directory.getPath().indexOf('!') > 0) {
				classes.addAll(findClassesInsideJar(directory, packageName));
			}
			else {
				classes.addAll(findClasses(directory, packageName, sb));
			}
		}

		String msgError = sb.toString();
		if (msgError.length() > 0) {
			throw new ClassNotFoundException(msgError);
		}

		classes = orderAlphaNumeric(classes);
		return classes;
	}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * subdirs.
	 * 
	 * @param classPathJar
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	private static List<Class<?>> findClassesInsideJar(File classPathJar, String packageName)
			throws ClassNotFoundException {

		System.out.println(" veio aqui simmmmmmmmmmmmmmmmmmmmmm ");
		String path = classPathJar.getPath();
		path = path.replace("file:\\", "");
		path = path.replace("file:", "");
		String jarFilePath = path.substring(0, path.indexOf('!'));
		List<Class<?>> classes = new ArrayList<Class<?>>();

		JarFile jarFile = null;
		try {
			jarFile = new JarFile(jarFilePath);
		} catch (IOException e1) {
			LOG.error(e1.getMessage(), e1);
		}
		Enumeration<JarEntry> entries = jarFile.entries();
		while (entries.hasMoreElements()) {
			JarEntry jarEntry = (JarEntry) entries.nextElement();

			String fileName = jarEntry.getName();
			if (fileName.endsWith(".class") && !fileName.contains("$")) {
				fileName = fileName.replaceAll("/", ".");
				Class<?> clazz = Class.forName(fileName.substring(0, fileName.length() - 6));
				classes.add(clazz);
			}
		}
		return classes;
	}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * subdirs.
	 * 
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	private static List<Class<?>> findClasses(File directory, String packageName, StringBuilder sb)
			throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists()) {
			return classes;
		}

		File[] files = directory.listFiles();
		for (File file : files) {
			String fileName = file.getName();
			if (file.isDirectory()) {
				assert !fileName.contains(".");
				classes.addAll(findClasses(file, packageName + "." + fileName, sb));
			} else if (fileName.endsWith(".class") && !fileName.contains("$")) {
				Class<?> clazz = null;
				String classe = packageName + '.' + fileName.substring(0, fileName.length() - 6);
				try {
					clazz = Class.forName(classe,
							false,
							Thread.currentThread().getContextClassLoader());
					classes.add(clazz);
				} catch (ClassNotFoundException e) {
					sb.append("A classe " + classe + " não existe (" + e.getMessage() + ").\n");
				} catch (NoClassDefFoundError e) {
					sb.append("A dependencia \""+ e.getMessage() +"\" da classe \"" + classe + "\" não existe.\n");
				}
			}
		}

		return classes;
	}


	public final static List<Class<?>> removeEquals(List<Class<?>> list) {
		List<Class<?>> result = new ArrayList<Class<?>>();
		for (Class<?> item : list) {
			if (result.contains(item)) continue;

			result.add(item);
		}
		return result;
	}



	public final static List<Class<?>> orderAlphaNumeric(List<Class<?>> classes) {
		List<Class<?>> result = new ArrayList<Class<?>>();
		List<String> classesStr = new ArrayList<String>();
		for (Class<?> classe : classes) {
			try {			
				classesStr.add(classe.getPackage().getName() + "." + classe.getSimpleName());
			} catch (NullPointerException e) {
				throw new NullPointerException(e.getMessage() + ". ClasseStr:" + classe + " - Classe: " + classe.getName());
			}
		}

		Collections.sort(classesStr);
		for (String classeName : classesStr) {
			for (Class<?> classe : classes) {
				String classeOrdenada = classe.getPackage().getName() + "." + classe.getSimpleName();

				if (classeName.equals(classeOrdenada) == false) continue;
				result.add(classe);
				break;
			}
		}

		return result;
	}
	public final static Method getMethodNameByAnnotation(Class<?> class1, Class class2) {
		Method[] methodArray = class1.getMethods();

		for (Method xxx : methodArray) {
			Annotation annList = xxx.getDeclaredAnnotation(class2);
			if (annList == null) continue;
			return xxx;
		}

		return null;
	}

	public static String getCodeTask(Class<?> class1) {
		return getCodeTask(class1.getName());
	}
	public static String getCodeTask(String class1) {
		String key = class1;
		if (class1.startsWith("1") == false) {
			Integer pos = class1.lastIndexOf(".");

			String part1 = "1.";
			String part2 = StringUtils.getNumberOnly(class1.substring(0, pos));
			String part3 = StringUtils.getNumberOnly(class1.substring(pos));

			key = part1  
					+ part2 
					+ (StringUtils.hasValue(part3) ? "." : "")
					+ part3;
		}
		return key;
	}

}

