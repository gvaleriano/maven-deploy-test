
package br.com.dotum.jedi.util;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

public final class DateUtils {
	public static final String ISO_8601 = "yyyy-MM-dd HH:mm:ss";
	public static final String ISO_8601_TIME = "yyyy-MM-dd'T'HH:mm:ssZ";
	private static Log LOG = LogFactory.getLog(DateUtils.class);

	private DateUtils() {};

	/**
	 * Retorna a data truncada, sem os valores de hora, minuto, segundo, etc.
	 * @param data Data original.
	 * @return Data truncada.
	 */
	public final static Date getDate(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		// cria uma nova data sem as horas, minutos, etc 
		GregorianCalendar retDate = new GregorianCalendar();
		retDate.set(GregorianCalendar.YEAR, origDate.get(GregorianCalendar.YEAR));
		retDate.set(GregorianCalendar.MONTH, origDate.get(GregorianCalendar.MONTH));
		retDate.set(GregorianCalendar.DAY_OF_MONTH, origDate.get(GregorianCalendar.DAY_OF_MONTH));
		//
		return retDate.getTime();
	}

	public final static Date getDateWithoutTime(Date data) throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.parse( dateFormat.format(data) );
	}

	public final static String getDateAndTime(Date data) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dateFormat.format(data);
	}

	public final static int getHours(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.HOUR_OF_DAY);
	}

	public final static int getMinutes(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.MINUTE);
	}

	public final static int getSeconds(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.SECOND);
	}

	public final static int getDayOfMonth(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.DAY_OF_MONTH);
	}

	public final static int getDayOfWeek(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.DAY_OF_WEEK);
	}

	public final static int getDayOfYear(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.DAY_OF_YEAR);
	}

	public final static int getMonth(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.MONTH)+1;
	}

	public final static int getYear(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.YEAR);
	}

	public final static Date getFirstWeekOfMonth(int mes, int week) {
		Date dataAtual = new Date();
		GregorianCalendar data = new GregorianCalendar();
		data.set(Calendar.YEAR, DateUtils.getYear(dataAtual));
		data.setFirstDayOfWeek(Calendar.SUNDAY);
		data.set(Calendar.WEEK_OF_MONTH, week);
		data.set(Calendar.MONTH, mes);
		data.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		
		return data.getTime();
	}
	public final static int getWeekOfMonth(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.WEEK_OF_MONTH);
	}
	public final static int getWeekOfYear(Date data) {
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(data);
		//
		return origDate.get(GregorianCalendar.WEEK_OF_YEAR);
	}
	
	/**
	 * Retorna o primeiro da dia da semana correspondente a Domingo;
	 * Ex: data = 24/10/2018; retorno = 43;
	 * @param data
	 * @return data
	 * @throws ParseException
	 */
	public final static Date getFirstDateOfWeek(Date data) {
		int year = getYear(data);
		int week = getWeekOfYear(data);
		
		return getFirstDateOfWeek(year, week);
	}
	public final static Date getFirstDateOfWeek(int year, int week) {
		GregorianCalendar date = new GregorianCalendar();
		date.set(Calendar.YEAR, year);
		date.setFirstDayOfWeek(Calendar.SUNDAY);
		date.set(Calendar.WEEK_OF_YEAR, week);
		date.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return date.getTime();
	}
	/**
	 * Retorna o ultimo da dia da semana correspondente a Sábado;
	 * @param data
	 * @return data
	 * @throws ParseException
	 */
	public final static Date getLastDateOfWeek(Date data) {
		int year = getYear(data);
		int week = getWeekOfYear(data);
		
		return getLastDateOfWeek(year, week);
	}
	public final static Date getLastDateOfWeek(int year, int week) {

		GregorianCalendar date = new GregorianCalendar();
		date.set(Calendar.YEAR, year);
		date.setFirstDayOfWeek(Calendar.SUNDAY);
		date.set(Calendar.WEEK_OF_YEAR, week);
		date.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		return date.getTime();
	}
	
	public final static Date firstDayOfMonth(Date date) {
		int month = getMonth(date);
		int year = getYear(date);
		return firstDayOfMonth(month, year);
	}
	public final static Date firstDayOfMonth(int month, int year) {
		GregorianCalendar retDate = new GregorianCalendar(year, (month-1), 1);
		return retDate.getTime();
	}

	public final static Date firstDayOfYear(String year) {
		return firstDayOfYear(Integer.parseInt(year));
	}
	public final static Date firstDayOfYear(int year) {
		GregorianCalendar retDate = new GregorianCalendar(year, 0, 1);
		return retDate.getTime();
	}

	public final static Date lastDayOfMonth(Date date) {
		int month = getMonth(date);
		int year = getYear(date);
		return lastDayOfMonth(month, year);
	}
	public final static Date lastDayOfMonth(int month, int year) {
		// cria a data para o retorno (o mes comeca no 0)
		GregorianCalendar retDate = new GregorianCalendar(year, (month-1), 1);
		retDate.set(GregorianCalendar.DAY_OF_MONTH, retDate.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));
		//
		return retDate.getTime();
	}

	/**
	 * Acrescenta a quantidade informada de dias a uma data.
	 * @param data Data original.
	 * @param days Quantidade de dias a adicionar.
	 * @return
	 */
	public final static Date addDay(Date data, int days) {
		GregorianCalendar tempDate = new GregorianCalendar();
		tempDate.setTime(data);
		// adiciona os dias a data
		tempDate.add(GregorianCalendar.DAY_OF_MONTH, days);
		// retorna a nova data
		return tempDate.getTime();
	}

	/**
	 * Acrescenta a quantidade informada de meses a uma data.
	 * @param data Data original.
	 * @param months Quantidade de meses a adicionar.
	 * @return
	 */
	public final static Date addYear(Date data, int year) {
		return addMonth(data, year*12);
	}
	public final static Date addMonth(Date data, int months) {
		GregorianCalendar tempDate = new GregorianCalendar();
		tempDate.setTime(data);
		// adiciona os meses a data
		tempDate.add(GregorianCalendar.MONTH, months);
		// retorna a nova data
		return tempDate.getTime();
	}

	public final static int hoursBetween(Date d1, Date d2) {
		return secundsBetween(d1, d2) / 60 / 60;
	}
	public final static int minutesBetween(Date d1, Date d2) {
		return secundsBetween(d1, d2) / 60;
	}
	public final static int secundsBetween(Date d1, Date d2) {
		long x = ((d2.getTime() - d1.getTime()) / 1000);
		return (int)(x > 0 ? x : 0);
	}
	public final static int millisecundsBetween(Date d1, Date d2) {
		long x = (d2.getTime() - d1.getTime());
		return (int)(x > 0 ? x : 0);
	}

	public final static int daysBetween(Date d1, Date d2) {
		if (d1.equals(d2)) {
			return 0;
		}
		GregorianCalendar startTime = new GregorianCalendar();
		GregorianCalendar endTime = new GregorianCalendar();
		//
		GregorianCalendar curTime = new GregorianCalendar();
		GregorianCalendar baseTime = new GregorianCalendar();

		int dif_multiplier = 1;

		// Verifica a ordem de inicio das datas
		if (d1.compareTo(d2) < 0) {
			baseTime.setTime(d2);
			curTime.setTime(d1);
			dif_multiplier = 1;

			// seta as datas (truncadas)
			startTime.setTime(getDate(d1));
			endTime.setTime(getDate(d2));
		}else{
			baseTime.setTime(d1);
			curTime.setTime(d2);
			dif_multiplier = -1;

			// seta as datas (truncadas)
			startTime.setTime(getDate(d2));
			endTime.setTime(getDate(d1));
		}

		int daysInMonths = 0;

		// Para cada mes e ano, vai de mes em mes pegar o ultimo dia para import acumulando
		// no total de dias. Ja leva em consideracao ano bissexto
		while (	curTime.get(GregorianCalendar.YEAR) < baseTime.get(GregorianCalendar.YEAR) ||
				curTime.get(GregorianCalendar.MONTH) < baseTime.get(GregorianCalendar.MONTH)) {
			int max_day = curTime.getActualMaximum( GregorianCalendar.DAY_OF_MONTH );
			daysInMonths += max_day;
			curTime.add(GregorianCalendar.MONTH, 1);
		}

		// Marca que eh um saldo negativo ou positivo
		daysInMonths = daysInMonths * dif_multiplier;

		// Retira a diferenca de dias do total dos meses
		int days = (endTime.get(GregorianCalendar.DAY_OF_MONTH) - curTime.get(GregorianCalendar.DAY_OF_MONTH));

		// Se eh anterior ou posterior
		days = (days * dif_multiplier);

		// Acrescenta um dia, sempre falta
		//days++;

		return daysInMonths+days;
	}

	public final static int monthsBetween(Date d1, Date d2) {
		return monthsBetween(d1, d2, false);
	}
	public final static int monthsBetween(Date d1, Date d2, boolean ignorDay) {
		GregorianCalendar startTime = new GregorianCalendar();
		GregorianCalendar endTime = new GregorianCalendar();
		//
		GregorianCalendar curTime = new GregorianCalendar();
		GregorianCalendar baseTime = new GregorianCalendar();
		//
		startTime.setTime(d1);
		endTime.setTime(d2);

		int dif_multiplier = 1;

		// Verifica a ordem de inicio das datas
		if (d1.compareTo(d2) < 0) {
			baseTime.setTime(d2);
			curTime.setTime(d1);
			dif_multiplier = 1;
		}else{
			baseTime.setTime(d1);
			curTime.setTime(d2);
			dif_multiplier = -1;
		}

		int months = 0;

		// Para cada mes e ano, vai de mes em mes pegar o ultimo dia para import acumulando
		// no total de dias. Ja leva em consideracao ano bissesto
		curTime.add(GregorianCalendar.MONTH, 1);
		while (	(curTime.get(GregorianCalendar.YEAR) < baseTime.get(GregorianCalendar.YEAR)) ||
				((curTime.get(GregorianCalendar.YEAR) == baseTime.get(GregorianCalendar.YEAR)) &&
						(curTime.get(GregorianCalendar.MONTH) < baseTime.get(GregorianCalendar.MONTH))) ||
				((curTime.get(GregorianCalendar.MONTH) == baseTime.get(GregorianCalendar.MONTH)) )) {
			if(ignorDay == false) {
				if (curTime.get(GregorianCalendar.DAY_OF_MONTH) <= baseTime.get(GregorianCalendar.DAY_OF_MONTH))continue;
			} else {
				curTime.add(GregorianCalendar.MONTH, 1);
				months++;
			}
			//
		}

		//		// se for o ultimo dia do mes, acrescenta mais um mes
		// esse +1 abaixo boicota quando o dia é 31 de cada mes até dia 31 do mesmo mes
		//		int lastDayInMonth = endTime.getActualMaximum( GregorianCalendar.DAY_OF_MONTH );
		//		if (endTime.get(GregorianCalendar.DAY_OF_MONTH) == lastDayInMonth) {
		//			months += 1;
		//		}

		// Marca que eh um saldo negativo ou positivo
		months = months * dif_multiplier;

		return months;
	}

	public final static int yearsBetween(Date d1, Date d2) {
		GregorianCalendar startTime = new GregorianCalendar();
		GregorianCalendar endTime = new GregorianCalendar();
		//
		GregorianCalendar curTime = new GregorianCalendar();
		GregorianCalendar baseTime = new GregorianCalendar();
		//
		startTime.setTime(d1);
		endTime.setTime(d2);

		int dif_multiplier = 1;

		// Verifica a ordem de inicio das datas
		if (d1.compareTo(d2) < 0) {
			baseTime.setTime(d2);
			curTime.setTime(d1);
			dif_multiplier = 1;
		}else{
			baseTime.setTime(d1);
			curTime.setTime(d2);
			dif_multiplier = -1;
		}

		int years = 0;

		// Para cada mes e ano, vai de mes em mes pegar o ultimo dia para import acumulando
		// no total de dias. Ja leva em consideracao ano bissesto
		curTime.add(GregorianCalendar.YEAR, 1);
		while (	(curTime.get(GregorianCalendar.YEAR) < baseTime.get(GregorianCalendar.YEAR)) ||
				((curTime.get(GregorianCalendar.YEAR) == baseTime.get(GregorianCalendar.YEAR)) &&
						(curTime.get(GregorianCalendar.DAY_OF_YEAR) <= baseTime.get(GregorianCalendar.DAY_OF_YEAR))) ) {
			//
			years += 1;
			curTime.add(GregorianCalendar.YEAR, 1);
		}

		// Marca que eh um saldo negativo ou positivo
		years = years * dif_multiplier;

		return years;
	}

	public final static String getHourMinutesSecond(Date data) {
		GregorianCalendar curTime = new GregorianCalendar();

		curTime.setTime(data);

		StringBuilder horaMinutoSegundo = new StringBuilder();
		int hora = curTime.get(GregorianCalendar.HOUR_OF_DAY); 
		horaMinutoSegundo.append((hora < 10 ? "0"+hora : hora));
		horaMinutoSegundo.append(":");
		int minuto = curTime.get(GregorianCalendar.MINUTE); 
		horaMinutoSegundo.append((minuto < 10 ? "0"+minuto : minuto));
		horaMinutoSegundo.append(":");
		int segundo = curTime.get(GregorianCalendar.SECOND); 
		horaMinutoSegundo.append((segundo < 10 ? "0"+segundo : segundo));

		return horaMinutoSegundo.toString();
	}

	public final static Double getHourDouble(Date hourDate) {
		// converte para decimal
		int valueHours = DateUtils.getHours(hourDate);
		int valueMinutes = DateUtils.getMinutes(hourDate);
		int valueSeconds = DateUtils.getSeconds(hourDate);
		// converte para um decimal com 4 casas, aredondando os centavos
		double hourDouble = new Double(valueHours);
		hourDouble = NumberUtils.roundDouble((hourDouble + NumberUtils.roundDouble((valueMinutes / 60d), 4)), 4);
		hourDouble = NumberUtils.roundDouble((hourDouble + NumberUtils.roundDouble((valueSeconds / 60d / 60d), 4)), 4);
		// retorna
		return hourDouble;
	}

	public final static Date getHour(Double hourDouble) {
		// pega as horas, parte inteira
		Double valueHours = NumberUtils.truncDouble(hourDouble, 0);
		Double valueMinutes = NumberUtils.truncDouble(((hourDouble - valueHours.doubleValue()) * 60d), 0);
		Double valueSeconds = NumberUtils.roundDouble(((((hourDouble - valueHours.doubleValue()) * 60d) - valueMinutes.doubleValue()) * 60d), 0);
		// se passar dos 59 minutos ou segundos, distribui
		if (valueSeconds.doubleValue() < 0d) { valueMinutes -= 1d; valueSeconds += 60d; }
		if (valueSeconds.doubleValue() > 59d) { valueMinutes += 1d; valueSeconds -= 60d; }
		if (valueMinutes.doubleValue() < 0d) { valueHours -= 1d; valueMinutes += 60d; }
		if (valueMinutes.doubleValue() > 59d) { valueHours += 1d; valueMinutes -= 60d; }
		// monta no formato de data
		Date hourDate = DateUtils.getHour(valueHours.intValue(), valueMinutes.intValue(), valueSeconds.intValue());
		//
		return hourDate;
	}

	public final static Date getHour(int hours, int minutes, int seconds) {
		// cria uma nova data sem as horas, minutos, etc 
		GregorianCalendar retDate = new GregorianCalendar();
		retDate.set(GregorianCalendar.HOUR_OF_DAY, hours);
		retDate.set(GregorianCalendar.MINUTE, minutes);
		retDate.set(GregorianCalendar.SECOND, seconds);
		//
		return retDate.getTime();
	}

	public final static long getCurrentTimeMillis() {
		return System.currentTimeMillis();
	}


	public final static Date coalesce(Date d1, Date d2) {
		if (d1 == null) return d2;
		return d1;
	}

	public final static String dateExtension(Date d) throws Exception {
		int dia = getDayOfMonth(d);
		String mes = mothExtension( getMonth( d ) );
		int ano = getYear( d );

		return dia +" de "+ mes + " de " + ano;
	}

	public final static String mothExtension(String value) throws Exception {
		return mothExtension(FormatUtils.parseInt(value));
	}

	public final static String mothExtension(int value) throws Exception {
		if ((value < 1) || (value > 12)) 
			throw new Exception("o mes tem que star entre 1 e 12");

		value -= 1;
		String[] month = new String[12];
		month[0] = "Janeiro";
		month[1] = "Fevereiro";
		month[2] = "Março";
		month[3] = "Abril";
		month[4] = "Maio";
		month[5] = "Junho";
		month[6] = "Julho";
		month[7] = "Agosto";
		month[8] = "Setembro";
		month[9] = "Outubro";
		month[10] = "Novembro";
		month[11] = "Dezembro";

		return month[value];
	}

	public final static Date getFirstYear() {
		try {
			return FormatUtils.parseDate("01/01/1900");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	public final static Date getLastYear() {
		try {
			return FormatUtils.parseDate("31/12/2100");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public final static int getMinutesOfDay(String horaStr) throws ParseException {
		String[] parts = horaStr.split(":");
		Integer hora = FormatUtils.parseInt(parts[0]);
		Integer minutes = FormatUtils.parseInt(parts[1]);
		Integer result = (hora * 60) + minutes;
		return result;
	}

	/**
	 * Pega o dia util subsequente ao fim de semana
	 * @param value
	 * @return
	 */
	public final static Date getUsefulDay(Date value) {
		int diaDaSemana = DateUtils.getDayOfWeek(value);
		if ((diaDaSemana == GregorianCalendar.SATURDAY)) {
			value = DateUtils.addDay(value, 2);
		} else if ((diaDaSemana == GregorianCalendar.SUNDAY)) {
			value = DateUtils.addDay(value, 1);
		}
		return value;
	}

	public final static void diffTime(String preText, Date d1) {
		Date dFim = new Date();
		String resultado  = diffTime(d1, dFim);
		System.out.println("[Performance] " + preText + " -> " + resultado);
	}

	public final static String diffTimeSimple(Date d1, Date d2) {
		String resultado = "";
		Integer milesimos = millisecundsBetween(d1, d2);
		Integer segundos = DateUtils.secundsBetween(d1, d2);
		Integer minutos = DateUtils.minutesBetween(d1, d2);
		Integer horas = DateUtils.hoursBetween(d1, d2);
		Integer dias = DateUtils.daysBetween(d1, d2);
		Integer meses = DateUtils.monthsBetween(d1, d2);


		if (meses > 0) {
			resultado = meses + " meses";
		} else if (dias > 0) {
			resultado = dias + " dias";
		} else if (horas > 0) {
			Integer restoMinutos = minutos - (horas*60);
			resultado = horas + ":" + StringUtils.fillLeft(restoMinutos, '0', 2) + " h";
		} else if (minutos > 0) {
			Integer restoSegundos = segundos - (minutos*60);
			resultado = minutos + ":" + StringUtils.fillLeft(restoSegundos, '0', 2) + " m";
		} else if (segundos > 0) {
			Integer restoMilesimos = milesimos - (segundos*1000);
			resultado = segundos + "," + StringUtils.fillLeft(restoMilesimos, '0', 3) + " s";
		} else {
			resultado = "0," + milesimos + " s";
		}

		return resultado;

	}
	public final static String diffTime(Date d1, Date d2) {
		String resultado = "";
		Integer segundos = DateUtils.secundsBetween(d1, d2);
		Integer minutos = DateUtils.minutesBetween(d1, d2);
		Integer horas = DateUtils.hoursBetween(d1, d2);
		Integer milesimos = millisecundsBetween(d1, d2);

		if (horas > 0) {
			Integer restoMinutos = minutos - (horas*60);
			Integer restoSegundos = segundos - (minutos*60);
			Integer restoMilesimos = milesimos - (segundos*1000);
			resultado = StringUtils.fillLeft(horas, '0', 2) + ":" + StringUtils.fillLeft(restoMinutos, '0', 2) + ":" + StringUtils.fillLeft(restoSegundos, '0', 2) + " " + restoMilesimos;
		} else if (minutos > 0) {
			Integer restoSegundos = segundos - (minutos*60);
			Integer restoMilesimos = milesimos - (segundos*1000);
			resultado = "00:" + StringUtils.fillLeft(minutos, '0', 2) + ":" + StringUtils.fillLeft(restoSegundos, '0', 2) + " " + restoMilesimos;
		} else if (segundos > 0) {
			Integer restoMilesimos = milesimos - (segundos*1000);
			resultado = "00:00:" + StringUtils.fillLeft(segundos, '0', 2) + " " + restoMilesimos;
		} else {
			resultado = "00:00:00 " + milesimos;
		}

		return resultado;
	}

	public final static String formatHour(Date date) {
		return formatDate(date, "HH:mm");
	}

	public final static String formatDate(Date date) {
		return formatDate(date, "dd/MM/yyyy");
	}
	public final static String formatDate(Date date, String format) {
		if (date == null) return null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public final static Date parseDateWithHour(Date data, String hora) throws ParseException {
		return parseDate(formatDate(data) + " " + hora, "dd/MM/yyyy HH:mm:ss");
	}
	public final static Date parseDateWithHour(String data, String hora) throws ParseException {
		return parseDate(data+ " " + hora, "dd/MM/yyyy HH:mm:ss");
	}

	public final static Date parseDate(String data) throws ParseException {
		return parseDate(data, "dd/MM/yyyy");
	}
	public final static Date parseDate(String data, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(data);
	}
	
	public final static String getDayOfWeekExtension(Date value) {
		String[] DIA_SEMANA_EXTENSO = new DateFormatSymbols(new Locale("pt", "BR")).getShortWeekdays();
		
		GregorianCalendar origDate = new GregorianCalendar();
		origDate.setTime(value);

		return DIA_SEMANA_EXTENSO[origDate.get(Calendar.DAY_OF_WEEK)];
	}
}





