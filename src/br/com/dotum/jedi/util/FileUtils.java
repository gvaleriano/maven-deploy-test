
package br.com.dotum.jedi.util;

import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopyFields;
import com.itextpdf.text.pdf.PdfReader;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

public final class FileUtils {
	private static Log LOG = LogFactory.getLog(FileUtils.class);

	public final static int TAMANHO_BUFFER = 1024;
	public static enum FileType {DIR,FILE,IMAGE,TEXT};


	public final static String jpeg = "jpeg";
	public final static String jpg = "jpg";
	public final static String gif = "gif";
	public final static String tiff = "tiff";
	public final static String tif = "tif";
	public final static String png = "png";
	public final static String txt = "txt";
	public final static String rtf = "rtf";

	/**
	 * Get the extension of a file.
	 */  
	public final static String getName(File f) {
		return f.getName().substring(0, f.getName().lastIndexOf('.'));
	}
	public final static String getExtension(File f) {
		String ext = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');

		if (i < s.length() - 1) {
			ext = s.substring(i+1).toLowerCase();
		}
		return ext;
	}

	public final static String getExtension(String f) {
		int position = f.lastIndexOf(".")+1;
		return f.substring(position).toLowerCase();
	}

	/**
	 * Get a file by user choice.
	 * @param parent
	 * @param fileType
	 * @return
	 * @throws Exception
	 */
	public final static String fileChooser(Component parent, FileType fileType) throws Exception {
		try {
			JFileChooser chooser = new JFileChooser();
			if (fileType == FileType.IMAGE) {
				chooser.setFileFilter(new ImageFilter());
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			} else if (fileType == FileType.TEXT) {
				chooser.setFileFilter(new TextFilter());
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			} else if (fileType == FileType.TEXT) {
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			} else {
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			}
			int returnVal = chooser.showOpenDialog(parent);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				return chooser.getSelectedFile().getPath();
			}
			return null;
		} catch (Exception ex) {
			throw new Exception("Ocorreu um erro ao carregar o arquivo!", ex);
		}
	}

	/**
	 * Abre a janela se seleção de arquivos baseado em uma lista de filtros.
	 * fileFilter - devera ser sempre um par como segue:
	 * 
	 * 				new String[] {"Arquivos PDF", "pdf"}
	 * 
	 * 		      - para passar mais de um tipo de arquivo
	 * 				basta separar por ';'
	 * 
	 * 				new String[] {"Arquivos PDF", "pdf","Imagens","gif;jpg;jpeg"}
	 * 
	 * @param parent
	 * @param fileFilter
	 * @return
	 * @throws Exception
	 */
	public final static String fileChooser(Component parent, String[] fileFilter) throws Exception {
		try {
			JFileChooser chooser = new JFileChooser();
			if (fileFilter != null) {	    	
				chooser.removeChoosableFileFilter(chooser.getFileFilter());
				for (int i = 0; i < fileFilter.length; i = i + 2) {
					String ext = fileFilter[i+1];
					String[] extensions = ext.split(";");
					FileExtensionFilter filter = new FileExtensionFilter(fileFilter[i], extensions);
					chooser.addChoosableFileFilter(filter);
				}
			}
			int returnVal = chooser.showOpenDialog(parent);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				return chooser.getSelectedFile().getPath();
			}
			return null;
		} catch (Exception ex) {
			throw new Exception("Ocorreu um erro ao carregar o arquivo!", ex);
		}
	}

	public final static void copyFile(File in, File out) throws Exception {
		FileInputStream fis = new FileInputStream(in);
		FileOutputStream fos = new FileOutputStream(out);
		byte[] buf = new byte[1024];
		int i = 0;
		while((i=fis.read(buf))!=-1) {
			fos.write(buf, 0, i);
		}
		fis.close();
		fos.close();
	}

	public final static void copyDirectory(File srcDir, File dstDir) throws Exception {
		if (srcDir.isDirectory()) {
			if (!dstDir.exists()) {
				dstDir.mkdir();
			}

			String[] children = srcDir.list();
			for (int i=0; i<children.length; i++) {
				copyDirectory(new File(srcDir, children[i]),
						new File(dstDir, children[i]));
			}
		} else {
			// This method is implemented in Copying a File
			copyFile(srcDir, dstDir);
		}
	}

	public final static boolean existDirectory(String directory) {
		File file = new File(directory);
		return file.exists();
	}

	public final static boolean existFile(String FileNameAndPath) {
		File file = new File(FileNameAndPath);
		return file.exists();
	}

	public final static void createDirectory(String directory) throws Exception {

		if (directory.contains("/") && directory.contains("\\"))
			throw new Exception("O nome do diretório não pode conter os dois tipos de barras '\\' e '/'");

		String token = "\\\\";
		if (directory.contains("/")) token = "/";

		String[] parts = directory.split(token);
		String concat = "";
		String primeiro = directory.substring(0,1);
		if (primeiro.equals("/") || primeiro.equals("\\")) {
			concat += primeiro;
		}
		String temp = "";
		for (int i = 0; i < parts.length; i++) {
			if (parts[i].equals("")) continue;
			concat += temp + parts[i];
			if (existDirectory(concat) == false) {
				File file = new File(concat);
				file.mkdir();
			}
			temp = "/";
		}
	}

	/*
	 * Get a imagem from a file name.
	 */
	public final static Image getImageFromFile(String fileName) throws Exception {
		try {
			// se nao passou o nome do arquivo
			if (fileName == null) return null;
			// carrega a imagem
			Image image = null;
			// Lendo um arquivo
			File sourceimage = new File(fileName);
			image = ImageIO.read(sourceimage);
			//
			return image;
		} catch (IOException ex) {
			throw new Exception("Não foi possível acessar o arquivo informado! [Arquivo: "+ fileName +"]", ex);
		} catch (Exception ex) {
			throw new Exception("Não foi possível carregar a imagem do arquivo! [Arquivo: "+ fileName +"]", ex);
		}
	}

	public final static Image getImageFromFileStream(String fileName) throws Exception {
		try {
			// se nao passou o nome do arquivo
			if (fileName == null) return null;
			// carrega a imagem
			Image image = null;
			// Lendo de um input stream
			InputStream is = new BufferedInputStream(
					new FileInputStream(fileName));
			image = ImageIO.read(is);
			//
			return image;
		} catch (IOException ex) {
			throw new Exception("Não foi possível acessar o arquivo informado! [Arquivo: "+ fileName +"]", ex);
		} catch (Exception ex) {
			throw new Exception("Não foi possível carregar a imagem do arquivo! [Arquivo: "+ fileName +"]", ex);
		}
	}

	public final static Image getImageFromURL(String url) throws Exception {
		try {
			// se nao passou a url do arquivo
			if (url == null) return null;
			// carrega a imagem
			Image image = null;
			// Lendo de uma URL
			image = ImageIO.read(new URL(url));
			//
			return image;
		} catch (IOException ex) {
			throw new Exception("Não foi possível acessar a imagem informada! [URL: "+ url +"]", ex);
		} catch (Exception ex) {
			throw new Exception("Não foi possível carregar a imagem do endereço informado! [URL: "+ url +"]", ex);
		}
	}

	static class ImageFilter extends FileFilter {
		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) {
				return true;
			}
			//
			String extension = getExtension(f);
			if (extension != null) {
				if (extension.equals(tiff) ||
						extension.equals(tif) ||
						extension.equals(gif) ||
						extension.equals(jpeg) ||
						extension.equals(jpg) ||
						extension.equals(png)) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}

		public String getDescription() {
			return "Imagens (JPG, PNG, GIF, ...)";
		}
	}

	static class TextFilter extends FileFilter {
		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) {
				return true;
			}
			//
			String extension = getExtension(f);
			if (extension != null) {
				if (extension.equals(tiff) ||
						extension.equals(tif) ||
						extension.equals(gif) ||
						extension.equals(jpeg) ||
						extension.equals(jpg) ||
						extension.equals(png)) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}

		public String getDescription() {
			return "Arquivos texto (TXT, RTF, ...)";
		}
	}

	public final static InputStream getStreamFromBytes(byte[] bytes) throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		return in;
	}
	public final static byte[] getBytesFromStream(InputStream is) throws IOException {
		// Get the size of the file
		long length = 1024;

		if (length > Integer.MAX_VALUE) {
			throw new IOException("Arquivo grande demais!!!");
		}

		// Create the byte array to hold the data
		byte[] bytes = new byte[(int)length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
			offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file.");
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;
	}
	public final static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		// Get the size of the file
		long length = file.length();

		if (length > Integer.MAX_VALUE) {
			throw new IOException("Arquivo grande demais!!!");
		}

		// Create the byte array to hold the data
		byte[] bytes = new byte[(int)length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
			offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file "+file.getName());
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;
	}

	public final static byte[] getBytes(String text) throws IOException {
		return text.getBytes("UTF-8");
	}

	public final static byte[] getBytes(InputStream is) throws IOException {
		int len;
		int size = 1024;
		byte[] buf;
		if (is instanceof ByteArrayInputStream) {
			size = is.available();
			buf = new byte[size];
			len = is.read(buf, 0, size);
		} else {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			buf = new byte[size];
			while ((len = is.read(buf, 0, size)) != -1)
				bos.write(buf, 0, len);
			buf = bos.toByteArray();
		}
		return buf;
	}

	public final static boolean createFileFromInputStream(File fileOut, InputStream is) throws Exception {
		FileUtils.createDirectory(fileOut.getParent());

		// transforma o stream em arquivo fisico
		FileOutputStream fos = new FileOutputStream(fileOut);
		byte[] b = new byte[1024];
		while (is.read(b) != -1) {
			fos.write(b);
		}
		fos.flush();
		fos.close();
		fos = null;
		return true;
	}

	public final static boolean createFileFromString(File fileOut, String text) throws Exception {
		FileUtils.createDirectory(fileOut.getParent());

		// transforma o stream em arquivo fisico
		FileOutputStream fos = new FileOutputStream(fileOut);
		DataOutputStream dos = new DataOutputStream(fos);
		dos.write( text.getBytes(Charset.forName("UTF-8")) );
		dos.flush();
		dos.close();
		return true;
	}

	@Deprecated
	public final static boolean createFileFromBytes(File fileOut, byte[] bytes) throws Exception {
		FileUtils.createDirectory(fileOut.getParent());

		// transforma o stream em arquivo fisico
		FileOutputStream fos = new FileOutputStream(fileOut);
		fos.write(bytes);
		fos.flush();
		fos.close();
		fos = null;
		return true;
	}

	public final static boolean createFileFromRows(File fileOut, String[] rows) throws Exception {
		StringBuilder sb = new StringBuilder();
		for (String str : rows) {
			sb.append(str + "\n");
		}

		return createFileFromString(fileOut, sb.toString());
	}

	public final static int imageWidth(String file) throws Exception {
		Image imagem = Toolkit.getDefaultToolkit().getImage(file);
		return imagem.getWidth(null);
	}

	public final static int imageHeight(String file) throws Exception {
		Image imagem = Toolkit.getDefaultToolkit().getImage(file);
		return imagem.getHeight(null);
	}

	public final static boolean deleteFile(String file) {
		File f = new File(file);
		return deleteFile(f);
	}
	public final static boolean deleteFile(File f) {
		if (f.exists()) {
			LOG.info("Apagado o arquivo " + f.getAbsolutePath());
		} else {
			LOG.info("O arquivo " + f.getAbsolutePath() + " não existe. Impossivel apagar!");
		}
		return f.delete();
	}

	public final static void deleteDirectory(String dir) throws IOException {
		deleteDirectory(new File(dir));
	}

	public final static void deleteDirectory(File dir) throws IOException {
		if (!dir.isDirectory()) {
			throw new IOException("Not a directory " + dir);
		}

		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			if (file.isDirectory()) {
				deleteDirectory(file);
			}
			else {
				boolean deleted = file.delete();
				if (!deleted) {
					throw new IOException("Unable to delete file " + file);
				}
			}
		}

		dir.delete();
	}

	/**
	 * Metodo retorna um array com os nomes de diretorios de um determinado diretorio
	 * @param path
	 * @return
	 */
	public final static String[] getDirectoryList(File path) {
		List<String> result = new ArrayList<String>();
		String[] list = path.list();
		if (list != null) {
			for (String str : list) {
				File f = new File(path + "/" + str);
				if (f.isDirectory()) {
					result.add(str);
				}
			}
			return result.toArray(new String[result.size()]);
		} else {
			return null;
		}
	}

	/**
	 * Metodo retorna um array com os nomes de diretorios de um determinado diretorio
	 * @param path
	 * @return
	 */
	public final static String[] getDirectoryList(String path) {
		File f = new File(path);
		return getDirectoryList(f);
	}

	/**
	 * Metodo retorna um array com os nomes de arquivos de um determinado diretorio 
	 * @param path
	 * @return
	 */
	public final static String[] getFileList(String path) {
		List<String> result = new ArrayList<String>();

		if ( (path.endsWith("\\") == false) &&
				(path.endsWith("/") == false) ) {
			path = path + "/";
		}

		File temp = new File(path);
		String[] list = temp.list();
		if (list != null) {
			for (String str : list) {
				File f = new File(path + str);
				if (f.isFile()) {
					result.add(str);
				}
			}
			return result.toArray(new String[result.size()]);
		} else {
			return null;
		}
	}

	/**
	 * Metodo retorna o Mime do arquivo
	 * @param file
	 * @return
	 */
	public final static String getFileMime(String file) {
		File f = new File(file);
		return getFileMime(f);
	}

	/**
	 * Metodo retorna o Mime do arquivo
	 * @param file
	 * @return
	 */
	public final static String getFileMime(File file) {
		return new MimetypesFileTypeMap().getContentType(file);
	}

	public final static byte[] createByteZip(List<FileMemory> fmList) throws IOException, DeveloperException{
		Map<String, byte[]> aquivoMap = new HashMap<String, byte[]>();
		for (FileMemory fm : fmList) {
			aquivoMap.put(fm.getName(), fm.getBytes());
		}

		return createByteZip(aquivoMap);
	}
	public final static byte[] createByteZip(Map<String, byte[]> aquivoMap) throws IOException{
		ByteArrayOutputStream memory = new ByteArrayOutputStream();
		ByteArrayInputStream lerArquivoByte = null;
		ZipOutputStream zip = null;   

		zip = new ZipOutputStream(memory);
		byte[] buffer = new byte[TAMANHO_BUFFER];

		for (String key : aquivoMap.keySet()) {
			byte[] arq = aquivoMap.get(key);

			ZipEntry entry = new ZipEntry(key);
			lerArquivoByte = new ByteArrayInputStream(arq);
			zip.putNextEntry(entry);


			int nBytes;
			while ((nBytes = lerArquivoByte.read(buffer)) > 0){
				zip.write(buffer, 0, nBytes);
			}
		}
		zip.closeEntry();
		zip.close();
		lerArquivoByte.close();

		return memory.toByteArray(); 
	}

	public final static File createFileZip(String zipFile, List<String> files) throws Exception {
		byte[] buffer = new byte[TAMANHO_BUFFER];
		FileOutputStream fos = new FileOutputStream(zipFile);
		ZipOutputStream zos = new ZipOutputStream(fos);

		for (String file : files) {
			String[] temp = file.split("/");
			int i = temp.length;
			String nameFile = temp[i-1];

			ZipEntry ze = new ZipEntry(nameFile);
			zos.putNextEntry(ze);
			FileInputStream in = new FileInputStream(file);

			int len;
			while ((len = in.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}

			in.close();
		}
		zos.closeEntry();
		zos.close();

		return new File(zipFile);
	}

	public final static void addFilesToExistingZip(File zipFile, File[] files) throws Exception {
		// get a temp file
		String tempDir = zipFile.getParent().replace("\\", "/") + "/temp/";
		FileUtils.createDirectory(tempDir);
		File tempFile = File.createTempFile(zipFile.getName(), null, new File(tempDir));
		// delete it, otherwise you cannot rename your existing zip to it.
		tempFile.delete();

		boolean renameOk=zipFile.renameTo(tempFile);
		if (!renameOk)
		{
			throw new RuntimeException("could not rename the file "+zipFile.getAbsolutePath()+" to "+tempFile.getAbsolutePath());
		}
		byte[] buf = new byte[1024];

		ZipInputStream zin = new ZipInputStream(new FileInputStream(tempFile));
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));

		ZipEntry entry = zin.getNextEntry();
		while (entry != null) {
			String name = entry.getName();
			boolean notInFiles = true;
			for (File f : files) {
				if (f.getName().equals(name)) {
					notInFiles = false;
					break;
				}
			}
			if (notInFiles) {
				// Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry(name));
				// Transfer bytes from the ZIP file to the output file
				int len;
				while ((len = zin.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
			}
			entry = zin.getNextEntry();
		}
		// Close the streams		
		zin.close();
		// Compress the files
		for (int i = 0; i < files.length; i++) {
			InputStream in = new FileInputStream(files[i]);
			// Add ZIP entry to output stream.
			out.putNextEntry(new ZipEntry(files[i].getName()));
			// Transfer bytes from the file to the ZIP file
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			// Complete the entry
			out.closeEntry();
			in.close();
		}
		// Complete the ZIP file
		out.close();
		tempFile.delete();
	}

	public final static List<File> listPath(List<File> result, File path) {
		return listPath(result, path, null);

	}
	public final static List<File> listPath(List<File> result, File path, String extensionFilter) {
		if (result == null) result = new ArrayList<File>();
		File[] files = path.listFiles();
		if (files == null) return result;
		Arrays.sort(files);

		for (File file : files) {

			if (file.isDirectory()) {
				listPath(result, file, extensionFilter);
			} else {
				if ((extensionFilter != null) && (file.getName().endsWith(extensionFilter) == false)) continue;
				result.add(file);
			}
		}
		return result;
	}

	/** 
	 * Compacta determindado arquivo ou diretório para o arquivo ZIP 
	 * especificado 
	 *  
	 * @param input 
	 *            O arquivo ou diretório de entrada 
	 * @param output 
	 *            O arquivo ZIP de saída 
	 */  
	public final static boolean compressFolderZip(String endEntrada, String endSaida) throws IOException {  
		String dirInterno = "";
		boolean retorno = true;
		try {
			File file = new File(endEntrada);

			//Verifica se o arquivo ou diretório existe
			if (!file.exists()) {
				return false;
			}

			ZipOutputStream zipDestino = new ZipOutputStream(new FileOutputStream(endSaida));

			//se é um arquivo a ser zipado
			//zipa e retorna
			if (file.isFile()) {
				compressFileZip(file, dirInterno + File.separator, zipDestino);


			} else {
				//senão lista o que tem no diretório e zipa
				dirInterno = file.getName();

				//Verifica se é diretório ou

				File[] files = file.listFiles();



				for (int i = 0; i < files.length; i++) {

					compressFileZip(files[i], dirInterno + File.separator, zipDestino);

				}
			}
			zipDestino.close();

		} catch (IOException ex) {
			retorno = false;
		}

		return retorno;

	}


	/**
	 * Zipa o arquivo ou diretório passado Verifica se é diretório executa recursão para adicionar os arquivos
	 * contidos dentro do mesmo no zip senão somente adiciona o arquivo no zip criado
	 * @param file arquivo ou diretório a ser adicionado no zip
	 * @param dirInterno diretório interno do zip
	 * @param zipDestino zip em que está sendo adicionado os arquivos e diretórios
	 * @throws java.io.IOException exeção que pode ser gerada na adição de arquivos no zip
	 */
	private static void compressFileZip(File file, String dirInterno, ZipOutputStream zipDestino) throws IOException {

		byte data[] = new byte[1024];

		//Verifica se a file é um diretório, então faz a recursão
		if (file.isDirectory()) {


			File[] files = file.listFiles();



			for (int i = 0; i < files.length; i++) {
				compressFileZip(files[i], dirInterno + File.separator + file.getName(), zipDestino);
			}


			return;

		}

		dirInterno = dirInterno.substring(dirInterno.indexOf(File.separator) + 1);
		if (dirInterno.startsWith(File.separator)) dirInterno = dirInterno.substring(1);
		if ((dirInterno.equals("") == false) && (dirInterno.endsWith(File.separator) == false)) dirInterno = dirInterno + File.separator;

		FileInputStream fi = new FileInputStream(file.getAbsolutePath());
		ZipEntry entry = new ZipEntry(dirInterno + file.getName());
		zipDestino.putNextEntry(entry);
		int count;
		while ((count = fi.read(data)) > 0) {
			zipDestino.write(data, 0, count);
		}
		zipDestino.closeEntry();
		fi.close();

	}

	/** 
	 * Extrai um arquivo ZIP para o diretório especificado 
	 *  
	 * @param input 
	 *            O arquivo ZIP de entrada 
	 * @param output 
	 *            O diretório de saída 
	 */  
	public final static void extractZip(final File input, final File output) throws IOException {  
		if (input.exists()) {  
			if (input.isDirectory()) {  
				throw new IllegalArgumentException("\"" + input.getAbsolutePath() + "\" não é um arquivo!");  
			}  
		} else {  
			throw new IllegalArgumentException("\"" + input.getAbsolutePath() + "\" não existe!");  
		}  
		if (output.exists()) {  
			if (output.isFile()) {  
				throw new IllegalArgumentException("\"" + output.getAbsolutePath() + "\" não é um diretório!");  
			}  
		}  
		final ZipFile zip = new ZipFile(input);  
		extract(zip, output);  
		zip.close();  
	}  

	// Copia o conteúdo do stream de entrada para o stream de saída  
	private static void copy(final InputStream in, final OutputStream out) throws IOException {  
		final int n = 4096;  
		final byte[] b = new byte[n];  
		for (int r = -1; (r = in.read(b, 0, n)) != -1; out.write(b, 0, r)) {}  
		out.flush();  
	}  

	// Retira determinado elemento do arquivo ZIP  
	private static void extract(final ZipFile zip, final File pasta) throws IOException {  
		InputStream entrada = null;  
		OutputStream saida = null;  
		String nome = null;  
		File arquivo = null;  
		ZipEntry elemento = null;  
		final Enumeration<?> elementos = zip.entries();  
		while (elementos.hasMoreElements()) {  
			elemento = (ZipEntry) elementos.nextElement();  
			nome = elemento.getName();  
			nome = nome.replace('/', File.separatorChar);  
			nome = nome.replace('\\', File.separatorChar);  
			arquivo = new File(pasta, nome);  
			if (elemento.isDirectory()) {  
				arquivo.mkdirs();  
			} else {  
				if (!arquivo.exists()) {  
					final File parent = arquivo.getParentFile();  
					if (parent != null) {  
						parent.mkdirs();  
					}  
					arquivo.createNewFile();  
				}  
				saida = new FileOutputStream(arquivo);  
				entrada = zip.getInputStream(elemento);  
				copy(entrada, saida);  
				saida.flush();  
				saida.close();  
				entrada.close();  
			}  
			arquivo.setLastModified(elemento.getTime());  
		}  
	}

	public final static HashMap<String, String> getZipFileContent(InputStream inputZip) throws Exception {
		HashMap<String, String> fileMap = new HashMap<String, String>();
		ZipInputStream zip = new ZipInputStream(inputZip);

		byte buf[] = new byte[100 * 1024];  
		ZipEntry entry;  
		while ((entry = zip.getNextEntry()) != null) { 
			int read = 0;  
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while ((read = zip.read(buf, 0, buf.length)) != -1)  {  
				baos.write(buf, 0, read); 
			}
			baos.toString("UTF-8");
			baos.toString("utf-8");
			baos.toString("UTF8");
			baos.toString("utf8");
			baos.toString("ASCII");
			baos.toString("ISO8859-1");
			baos.toString("ISO-8859-1");
			baos.flush();  
			baos.close();  

			byte[] b = baos.toByteArray();
			StringWriter sw = new StringWriter();
			for(int i = 0; i < b.length; i++){
				sw.write(byteToInt(b[i]));
			}
			fileMap.put(entry.getName(), sw.toString());
		}                 
		zip.close();  

		return fileMap;
	}

	static int byteToInt(byte b){
		return (b & 0xff);
	}

	public final static void unzip(String zipFileName, String target) throws IOException {
		FileUtils.extractZip(new File(zipFileName), new File(target));
	}

	/**
	 * Esse metodo recebe um FileMemory do tipo imagem e retorna um fileMemory do tipo imagem já resuzido o tamanho para 8bit.
	 * O arquivo de retorno já está com o nome e a extensão do arquivo passado.
	 * @param fm
	 * @return FileMemory
	 * @throws Exception
	 */
	public final static FileMemory resizeImg(FileMemory fm) throws Exception {
		BufferedImage src = ImageIO.read(fm.getStream());
		src.getScaledInstance(100, 100, BufferedImage.TYPE_INT_RGB);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ImageIO.write(src, FileUtils.jpg, byteArrayOutputStream);

		FileMemory fmNovo = new FileMemory(byteArrayOutputStream.toByteArray());
		fmNovo.setName(fm.getName());
		fmNovo.setExtension(fm.getExtension());

		return fmNovo;
	}

	public static byte[] getBytesFromFiles(List<FileMemory> fmList) throws DocumentException, IOException {
		ByteArrayOutputStream  file = new ByteArrayOutputStream();
		PdfCopyFields copy = new PdfCopyFields(file);
		
		for(FileMemory f : fmList) {
			PdfReader reader = new PdfReader(f.getStream());
			copy.addDocument(reader); 
		}
		copy.close();

		byte[] bb = file.toByteArray();
		return bb;
	}
}
