


package br.com.dotum.jedi.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public final class FormatUtils {	
	private static final DecimalFormat df = new DecimalFormat("###,##0.00");
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat sdf_dt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static final SimpleDateFormat sdfH = new SimpleDateFormat("HH:mm:ss"); 

	/**
	 * Formata uma data para dd/MM/yyyy
	 */
	public final static String formatDate(Date date) {
		if(date == null) return null;
		
		return sdf.format(date);
	}

	public final static String formatDateWithHour(Date date) throws ParseException {
		return sdf_dt.format(date);
	}
	public final static String formatDateWithHour(Date date, String hora) throws ParseException {
		Date d = FormatUtils.parseDateWithFormat(FormatUtils.formatDate(date) + " " + hora, "dd/MM/yyyy HH:mm:ss");
		
		return sdf_dt.format(d);
	}
	
	/**
	 * Formata uma data
	 */
	public final static String formatDate(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * Transforma uma data a partir de uma string
	 * @param date
	 * @return
	 */
	public final static Date parseDate(String value) throws ParseException {
		if (StringUtils.hasValue(value) == false) return null;
		return sdf.parse(value);
	}

	/**
	 * Transforma uma data a partir de uma string
	 * @param date
	 * @return
	 */
	public final static Date parseDateWithFormat(String value, String format) throws ParseException {
		if (StringUtils.hasValue(value) == false) return null;
		
		SimpleDateFormat form = new SimpleDateFormat(format);
		return form.parse(value);
	}

	public final static Date parseDate(String data, String hora) throws ParseException {
		if (StringUtils.hasValue(data) == false) return null;
		if (StringUtils.hasValue(hora) == false) return null;
		
		return sdf_dt.parse(data+" "+hora);
	}

	/**
	 * Transforma um numero a partir de uma string com pontos
	 * @param number
	 * @return
	 */
	public final static Integer parseIntWithDot(String value) throws ParseException {
		if (value == null) return null;
		value = value.trim();
		if ((StringUtils.hasValue(value) == false) || (value.equalsIgnoreCase("null")) || (value.equals("-"))) return null;
		
		value = value.replaceAll("\\.", "");
		value = value.replaceAll(",", "");
		return Integer.parseInt(value);
	}

	public final static Long parseLongWithDot(String value) throws ParseException {
		if ((StringUtils.hasValue(value) == false) || (value.equalsIgnoreCase("null")) || (value.equals("-"))) return null;
		
		value = value.replaceAll("\\.", "");
		value = value.replaceAll(",", "");
		return Long.parseLong(value);
	}

	/**
	 * Transforma um numero a partir de uma string
	 * @param number
	 * @return
	 */
	public final static Integer parseInt(String value) throws ParseException {
		return parseIntWithDot(value);
	}

	/**
	 * Nunca usar esse metodo.. lixo
	 * NUNCA!!!!!!!!!!!!!!!!
	 * NUNCA!!!!!!!!!!!!!!!!
	 * NUNCA!!!!!!!!!!!!!!!!
	 * @param value
	 * @return
	 * @throws ParseException
	 */
	@Deprecated
	public final static Long parseLong(String value) throws ParseException {
		return parseLongWithDot(value);
	}
	
	/**
	 * Transforma um Double a partir de uma string
	 * @param number
	 * @return
	 */
	public final static Double parseDouble(String value) throws ParseException {
		if ((StringUtils.hasValue(value) == false) || (value.equalsIgnoreCase("null"))) return null;

		int l0 = value.lastIndexOf(",");
		int l1 = value.lastIndexOf(".");

		// entao o ultimo separador eh uma virgula
		if (l0 > l1) {
			value = value.replaceAll("\\.", "");
			value = value.replaceAll(",", ".");
			return Double.parseDouble(value);
			// ultimo separador eh um ponto
		} else {
			value = value.replaceAll(",", "");
			return Double.parseDouble(value);
		}    
	}

	/**
	 * Formata uma data para HH:mm:ss
	 */
	@Deprecated
	public final static String formatHour(Date data) {
		if(data == null) return null;
		
		return sdfH.format(data);
	}

	@Deprecated
	/**
	 * Ao inves deste usar o FormatUtils.formatDoubleBR(Double value)
	 */
	public final static String formatDouble(Double value) {
		if(value == null) return null;
		
		return df.format(value);
	}

	public final static String formatDouble(Double value, int decimalPlaces) {
		if(value == null) return "";
		
		NumberFormat dff = new DecimalFormat ("###,##0.00", new DecimalFormatSymbols (new Locale ("pt", "BR")));
		dff.setMinimumFractionDigits(decimalPlaces);
		dff.setMaximumFractionDigits(decimalPlaces);
		return dff.format(value);
	}
	
	public final static String formatBigDecimal(BigDecimal value, int decimalPlaces) {
		if(value == null) return "";
		
		NumberFormat dff = new DecimalFormat("###,##0.00", new DecimalFormatSymbols (new Locale ("pt", "BR")));
		dff.setMinimumFractionDigits(decimalPlaces);
		dff.setMaximumFractionDigits(decimalPlaces);
		return dff.format(value);
	}
	
	public final static String formatBigDecimal(BigDecimal value) {
		return formatBigDecimal(value, 2);
	}
	
	@Deprecated
	/**
	 * Ao inves deste usar o FormatUtils.formatDoubleBR(Double value)
	 */
	public final static String formatDouble(Double value, String format) {
		if(value == null)
			return "";
		
		DecimalFormat dff = new DecimalFormat(format);
//		dff.setMinimumFractionDigits(decimalPlaces);
//		dff.setMaximumFractionDigits(decimalPlaces);
		String result = dff.format(value);
		result = result.replaceAll(",", "\\.");
		return result;
	}

	
	
	
	public final static String formatDoubleBR(Double value) {
		Locale locate = new Locale("pt", "BR");
		NumberFormat nf = NumberFormat.getInstance(locate);
		return nf.format((Double) value);
	}

	/*
	 * realiza a formatacao do valor de acordo com a mascara enviada
	 */
	public final static String format(String valor, String mascara) {
		if (StringUtils.hasValue(valor) == false) return null;
		
		StringBuffer dado = new StringBuffer();
		// remove caracteres nao numericos
		for (int i = 0; i < valor.length(); i++) {
			char c = valor.charAt(i);
			if (Character.isDigit(c)) {
				dado.append(c);
			}
		}

		int indMascara = mascara.length();
		int indCampo = dado.length();

		for (; indCampo > 0 && indMascara > 0;) {
			if (mascara.charAt(--indMascara) == '#') {
				indCampo--;
			}
		}

		StringBuffer saida = new StringBuffer();
		for (; indMascara < mascara.length(); indMascara++) {
			saida.append(((mascara.charAt(indMascara) == '#') ? dado.charAt(indCampo++) : mascara.charAt(indMascara)));
		}
		return saida.toString();
	}

	/**
	 * Formata um CPF.
	 */
	private final static String formatCpf(String value) {
		if (StringUtils.hasValue(value) == false) return null;
		
		while (value.length() < 11) {
			value = "0" + value;
		}
		return format(value, "###.###.###-##");
	}

	public final static String formatTelefone(String value) {
		return format(value, " (##) ####-####");
	}
	
	
	public final static String formatCpfCnpj(String value) {
		if (StringUtils.hasValue(value) == false) return null;
		
		value = StringUtils.getNumberOnly(value);
		if (value.length() == 11) {
			return formatCpf(value);
		} else {
			return formatCnpj(value);
		}
	}
	/**
	 * Formata um CNPJ.
	 */
	private final static String formatCnpj(String value) {
		if (StringUtils.hasValue(value) == false) return null;
		
		while (value.length() < 14) {
			value = "0" + value;
		}
		return format(value, "##.###.###/####-##");
	}

	public final static String formatSimNao(Integer value) {
		if (value == null) 
			return "Não";

		if (value.equals(1))
			return "Sim";

		return "Não";
	}

	/**
	 * Metodo que retorna o tamanho de caracteres que o objeto tem
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public final static int lenght(Object value) throws Exception {
		if (value == null) 
			return 0;

		if (value instanceof String) {
			return ((String) value).length();
		} else if (value instanceof Integer) {
			return (""+(Integer) value).length();
		} else {
			throw new Exception("Objeto do tipo desconhecido, atualize o metodo.");
		}
	}

}