
package br.com.dotum.jedi.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.WarningException;


public final class MathUtils {

	private static final int CASAS_DECIMAIS = 10;
	private static final int TIPO_ARREDONDAMENTO = BigDecimal.ROUND_HALF_DOWN;//BigDecimal.ROUND_CEILING;
	//
	
	public final static BigDecimal add(Number ... v) {
		for (int i = 0; i < v.length; i++) {
			if (v[i] == null) v[i] = BigDecimal.ZERO;
		}

		BigDecimal result = BigDecimal.ZERO;
		for (int i = 0; i < v.length; i++) {
			result = result.add( new BigDecimal(v[i].toString()) );
		}
		
		return result;
	}
	
	public final static BigDecimal subtract(Number ... v) {
		for (int i = 0; i < v.length; i++) {
			if (v[i] == null) v[i] = BigDecimal.ZERO;
		}

		BigDecimal result = new BigDecimal(v[0].toString());
		for (int i = 1; i < v.length; i++) {
			result = result.subtract( new BigDecimal(v[i].toString()) );
		}
		
		return result;
	}
	
	@Deprecated
	/**
	 * Pode passar como numeral normal que vai dar certo<br>
	 * utilize o metodo que os parametros varargs<br>
	 * multiply(Number ... values) <br>
	 * @param v1
	 * @param v
	 * @return
	 */
	public final static BigDecimal multiply(Number v1, String v) {
		BigDecimal v2 = null;
		if (v1 == null) v1 = BigDecimal.ZERO;
		if (v == null) {
			v2 = BigDecimal.ONE;
		} else {
			v2 = new BigDecimal(v);
		}
		
		return new BigDecimal(v1.toString()).multiply(v2);
	}

	
	public static void main(String[] args) throws DeveloperException {
		BigDecimal xxx = multiply(1.2345, "100");
		System.out.println(xxx);
	}
	
	public final static BigDecimal multiply(Number ... values) {
		BigDecimal result = BigDecimal.ONE;
		for (Number v : values) {
			if (v == null) v = BigDecimal.ZERO;
			
			result = multiply(result, v);
		}
		return result;
	}
	
	
	private final static BigDecimal multiply(Number v1, Number v) {
		BigDecimal v2 = null;
		if (v1 == null) v1 = BigDecimal.ZERO;
		if (v == null) {
			v2 = BigDecimal.ONE;
		} else {
			v2 = new BigDecimal(v.toString());
		}
		
		BigDecimal result = new BigDecimal(v1.toString()).multiply(v2);
		if (MathUtils.equalsZero(result))
			return BigDecimal.ZERO;
			
		return result;
	}
	
	public final static BigDecimal divide(Number v1, Number v) {
		return divide(v1, v, CASAS_DECIMAIS);
	}
	public final static BigDecimal divide(Number v1, Number v, int scale) {
		BigDecimal v2 = null;
		if (v1 == null) v1 = BigDecimal.ZERO;
		if (v == null) {
			v2 = BigDecimal.ONE;
		} else {
			v2 = new BigDecimal(v.toString());
		}
		v1 = new BigDecimal(v1.toString()).setScale(CASAS_DECIMAIS, TIPO_ARREDONDAMENTO);
		v2 = v2.setScale(scale, TIPO_ARREDONDAMENTO);
		
		BigDecimal result = new BigDecimal(v1.toString()).divide(v2, TIPO_ARREDONDAMENTO);
		if (MathUtils.equalsZero(result))
			return BigDecimal.ZERO;
			
		return result;
	}

	public final static BigDecimal max(Number v1, Number v2) {
		if (v1 == null) return null;
		if (v2 == null) return null;
		
		return new BigDecimal(v1.toString()).max(new BigDecimal(v2.toString()));
	}

	public final static BigDecimal min(Number v1, Number v2) {
		if (v1 == null) return null;
		if (v2 == null) return null;

		return new BigDecimal(v1.toString()).min(new BigDecimal(v2.toString()));
	}
	
	public final static Boolean equals(Number v1, Number v2) {
		if (v1 == null) return null;
		if (v2 == null) return null;

		return new BigDecimal(v1.toString()).compareTo(new BigDecimal(v2.toString())) == 0;
	}

	public final static Boolean equalsCoalesse(Number v1, Number v2) {
		if (v1 == null) v1 = BigDecimal.ZERO;
		if (v2 == null) v2 = BigDecimal.ZERO;
		
		return new BigDecimal(v1.toString()).compareTo(new BigDecimal(v2.toString())) == 0;
	}
	
	public final static Boolean less(Number v1, Number v2) {
		if (v1 == null) return null;
		if (v2 == null) return null;

		return new BigDecimal(v1.toString()).compareTo(new BigDecimal(v2.toString())) < 0;
	}

	public final static Boolean great(Number v1, Number v2) {
		if (v1 == null) return null;
		if (v2 == null) return null;

		return new BigDecimal(v1.toString()).compareTo(new BigDecimal(v2.toString())) > 0;
	}

	public final static Boolean lessEquals(Number v1, Number v2) {
		if (v1 == null) return null;
		if (v2 == null) return null;

		return new BigDecimal(v1.toString()).compareTo(new BigDecimal(v2.toString())) <= 0;
	}
	
	public final static Boolean greatEquals(Number v1, Number v2) {
		if (v1 == null) return null;
		if (v2 == null) return null;

		return new BigDecimal(v1.toString()).compareTo(new BigDecimal(v2.toString())) >= 0;
	}
	
	public final static Boolean equalsZero(Number v1) {
		if (v1 == null) return false;

		return new BigDecimal(v1.toString()).compareTo(BigDecimal.ZERO) == 0;
	}
	
	public final static Boolean notEqualsZero(Number v1) {
		if (v1 == null) return false;

		return new BigDecimal(v1.toString()).compareTo(BigDecimal.ZERO) != 0;
	}
	
	public final static Boolean lessZero(Number v1) {
		if (v1 == null) return false;
	
		return new BigDecimal(v1.toString()).compareTo(BigDecimal.ZERO) < 0;
	}
	
	public final static Boolean greatZero(Number v1) {
		if (v1 == null) return false;

		return new BigDecimal(v1.toString()).compareTo(BigDecimal.ZERO) > 0;
	}
	
	public final static Boolean lessEqualsZero(Number v1) {
		if (v1 == null) return false;

		return new BigDecimal(v1.toString()).compareTo(BigDecimal.ZERO) <= 0;
	}
	
	public final static Boolean greatEqualsZero(Number v1) {
		if (v1 == null) return false;

		return new BigDecimal(v1.toString()).compareTo(BigDecimal.ZERO) >= 0;
	}

	public final static BigDecimal round(Number v1, int size) {
		return new BigDecimal(v1.toString()).setScale(size, TIPO_ARREDONDAMENTO);
	}

	public final static BigDecimal trunk(Number v1, int size) {
		// esse é sempre assim pq nao é para arredondar e sim para truncar
		return new BigDecimal(v1.toString()).setScale(size, RoundingMode.DOWN); 
	}
	
	public final static boolean validDiff(Number v1, String v2) {
		BigDecimal vt1 = new BigDecimal(v2).negate();
		BigDecimal vt2 = new BigDecimal(v2);
		
		if ((lessEquals(v1, vt1)) || (greatEquals(v1, vt2))) {
			return true;
		}
		return false;
	}
	
	public final static BigDecimal[] portion(Number v1, Number[] pArray) {
		return portion(v1, pArray, 2);
	}
	
	public final static BigDecimal[] portion(Number v1, Number[] pArray, int casas) {
		BigDecimal[] paArray = new BigDecimal[pArray.length];
		BigDecimal t1 = BigDecimal.ZERO;
		for (int i = 0; i < pArray.length; i++) {
			BigDecimal p = new BigDecimal(pArray[i].toString());
			paArray[i] = p;
			t1 = add(t1, p);
		}
		BigDecimal t2 = BigDecimal.ZERO;
		for (int i = 0; i < pArray.length; i++) {
			BigDecimal p = new BigDecimal(pArray[i].toString());
			paArray[i] = divide(p, 100);
			t2 = add(t2, p);
		}
		
		if ((equals(t1, BigDecimal.ONE) == false) && equals(t1, new BigDecimal("100.0")) == false) {
			throw new WarningException("A proporcao tem que resultar 1.0"); 
		}
		
		
		BigDecimal sobra = BigDecimal.ZERO;
		BigDecimal[] result = new BigDecimal[paArray.length];
		for (int i = 0; i < paArray.length; i++) {
			BigDecimal r1 = multiply(v1, paArray[i]);
			BigDecimal r2 = trunk(r1, casas);
			BigDecimal r3 = subtract(r1, r2);
			sobra = add(sobra, r3);
			
			result[i] = r2;
		}
		int position = result.length-1;
		result[position] = add(sobra, result[position]); 
		
		return result;
	}
	
	public final static BigDecimal[] portion(Number v1, Integer parcelas) {
		BigDecimal r1 = divide(BigDecimal.ONE, new BigDecimal(parcelas.toString()));
		BigDecimal[] proporcao = new BigDecimal[parcelas];
		
		BigDecimal totalTemp = BigDecimal.ZERO;
		for (int i = 0; i < parcelas;i ++) {
			proporcao[i] = r1;
			
			totalTemp = add(totalTemp, r1);
		}
		BigDecimal restoTemp = subtract(BigDecimal.ONE, totalTemp); 
		proporcao[parcelas-1] = add(proporcao[parcelas-1], restoTemp);
		
		return portion(v1, proporcao);
	}
	
	public static BigDecimal newBigDecimal(Double valor) {
		return new BigDecimal(""+valor);
	}

	public static Double[] parse(BigDecimal[] valueArray) {
		Double[] x = new Double[valueArray.length];
		for (int i = 0; i < valueArray.length; i++) {
			x[i] = valueArray[i].doubleValue();
		}

		return x;
	}
	
}




