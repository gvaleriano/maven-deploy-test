
package br.com.dotum.jedi.util;

import java.math.BigDecimal;
import java.util.Arrays;

import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

public final class NumberUtils {
	private static Log LOG = LogFactory.getLog(NumberUtils.class);

	// para que a classe nao seja instanciada
	private NumberUtils() {};

	
	
	/** Metodo para arredondar um valor decimal em x casas
	 * 
	 * @param value - representa o valor original
	 * @param decimalPlaces - quantidade de casas decimais 
	 *                        em que o valor sera convertido
	 * @return valor arredondado para n casas
	 */
	public final static double roundDouble(double value, int decimalPlaces) {
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	/** Metodo para truncar um valor decimal em x casas
	 * 
	 * @param value - representa o valor original
	 * @param decimalPlaces - quantidade de casas decimais 
	 *                        em que o valor sera convertido
	 * @return valor truncado para n casas
	 */
	public final static double truncDouble(double value, int decimalPlaces) {
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_DOWN);
		return bd.doubleValue();
	}

	public final static Double coalesce(Double n1, Double n2) {
		if (n1 == null) return n2;
		return n1;
	}

	public final static BigDecimal coalesce(BigDecimal n1, BigDecimal n2) {
		if (n1 == null) return n2;
		return n1;
	}
	
	public final static Integer coalesce(Integer n1, Integer n2) {
		if (n1 == null) return n2;
		return n1;
	}

	public final static Double truncMoney(double value, int decimalPlaces) {
		String s = value+"";
		int pos_ponto = s.indexOf(".");
		String p1 = s.substring(0, pos_ponto);

		String p2 = s.substring(pos_ponto+1, s.length());
		if (p2.length() < decimalPlaces) {
			p2 = StringUtils.fillRight(p2, '0', decimalPlaces);
		} else {
			p2 = p2.substring(0,decimalPlaces);
		}

		Double result = new Double(p1+"."+p2);
		return result;
	}

	public final static Integer double2Integer(Double value, int decimalPlaces) {
		Double valor = value;
		String s = valor+"";
		int pos_ponto = s.indexOf(".");
		String p1 = s.substring(0, pos_ponto);
		String p2 = s.substring(pos_ponto+1, s.length());
		if (p2.length() < decimalPlaces) {
			p2 = StringUtils.fillRight(p2, '0', decimalPlaces);
		} else {
			p2 = p2.substring(0,decimalPlaces);
		}
		Integer result = new Integer(p1+p2);
		return result;
	}

	public final static String convertToStr( final double param, final int tamanho, final int casasdec ) {
		//
		final StringBuffer str = new StringBuffer();
		str.append( String.valueOf( param ) );
		//
		final char[] strTochar = str.toString().toCharArray();
		final int index = str.indexOf( "." );
		final int indexDesc = casasdec - ( ( strTochar.length - 1 ) - index );
		//
		str.delete( 0, strTochar.length );
		//
		for ( int i = 0; i < strTochar.length; i++ ) {
			if ( i != index ) {
				str.append( strTochar[ i ] );
			}
		}
		//
		for ( int i = 0; i < indexDesc; i++ ) {
			str.append( 0 );
		}
		//
		return StringUtils.fillZeroLeft( str.toString(), tamanho );
	}

	public final static double parse(Double value) {
		if (value == null) return 0.00;
		return value.doubleValue();
	}

	public final static int somaDoisDigitos(int dezena) {
		int a = Integer.parseInt(String.valueOf(dezena).substring(0, 1));
		int b = Integer.parseInt(String.valueOf(dezena).substring(1));
		return a + b;
	}

	public final static boolean hasValue(Integer value) {
		if ((value == null) || (value.equals(0))) 
			return false;
		return true;
	}

	public final static boolean hasValue(Double value) {
		if ((value == null) || (value.equals(0.0))) 
			return false;
		return true;
	}

	public final static boolean hasValue(BigDecimal value) {
		if ((value == null) || (value.equals(BigDecimal.ZERO))) 
			return false;
		return true;
	}

	/**
	 * Método que faz a distribuicao de um numero em X elementos
	 * @param numero
	 * @param parcelas
	 * @return
	 */
	public final static Integer[] distribuir(Integer numero, Integer parcelas) {
		Integer[] result = new Integer[parcelas];
		Integer resto = (numero % parcelas);
		if (resto == 0) {
			for (int i = 0; i < parcelas; i++) {
				result[i] = (numero / parcelas);
			}
		} else {
			Integer total = 0;
			Integer elemento = numero / parcelas;
			for (int i = 0; i < parcelas; i++) {
				result[i] = elemento;
				total += elemento;
			}
			result[result.length-1] += (numero-total);
		}
		return result;
	}

	public final static Integer[] distribuir2(Integer numero, Integer parcelas) {
		Integer[] result = new Integer[parcelas];
		Arrays.fill(result, 0);
		Integer resto = (numero % parcelas);
		if (resto == 0) {
			for (int i = 0; i < parcelas; i++) {
				result[i] = (numero / parcelas);
			}
		} else {
			Integer total = 0;
			Integer bloco = 0;
			for (int i = 0; i < numero; i++) {
				result[bloco] += 1;
				total += 1;
				bloco++;
				if (bloco >= parcelas) bloco = 0;
			}
		}
		return result;
	}

	public final static Integer random(int size) {
		if (size > 10)
			size = 10;
		
		String[] carct = {"0","1","2","3","4","5","6","7","8","9"}; 
		String result = ""; 
		for (int x = 0; x < size; x++){ 
			int j = (int) (Math.random() * carct.length); 
			result += carct[j]; 
		}
		return Integer.parseInt(result);
	}

	public final static Integer parseInt(String value) {
		value = StringUtils.getNumberOnly(value, "0123456789-");
		if (StringUtils.hasValue(value) == false) return null;
		return Integer.parseInt(value);
	}

	public final static BigDecimal parseBigDecimal(String value, char separator) {
		if ((StringUtils.hasValue(value) == false) || (value.equalsIgnoreCase("null"))) return null;
		
		String sep = "";
		if (separator == '.') sep = ",";
		if (separator == ',') sep = "\\.";
		
		value = value.replaceAll(sep, "");
		
		return parseBigDecimal(value);
	}
	
	/**
	 * Este metodo esta depreciado pois é preciso passar o caracter padrao que usará na conversão das casas decimais<br>
	 * Como é impossivel definir a origem dos dados (Brasil/EUA) é preciso informar o separador de decimal (ponto ou virgula)<br>
	 * Nos 4 primeiros casos abaixo é possivel identificar porem no caso 5 e 6 não sabemos se é um decimal ou um inteiro considerando a origem dos dados;<br> 
	 * String x1 = "123.456,789";<br>
	 * String x2 = "123,456.789";<br>
	 * String x3 = "123,456,789";<br>
	 * String x4 = "123.456.789";<br>
	 * // problema aqui!!! pq nao sei se é separador de milhar ou decimal!!<br>
	 * String x5 = "123,456";<br>
	 * String x6 = "123.456";<br>
	 *  
	 *  Por tanto começe a usar o metodo que passa um char com o caracter informativo
	 * @param value
	 * @return
	 */
	@Deprecated
	public final static BigDecimal parseBigDecimal(String value) {
		if ((StringUtils.hasValue(value) == false) || (value.equalsIgnoreCase("null"))) return null;

		int c0 = StringUtils.occurrencesCount(value, ",");
		int c1 = StringUtils.occurrencesCount(value, ".");
		if (c0 > 1) value = value.replaceAll(",", "");
		if (c1 > 1) value = value.replaceAll("\\.", "");

		
		int l0 = value.lastIndexOf(",");
		int l1 = value.lastIndexOf(".");

		// entao o ultimo separador eh uma virgula
		if (l0 > l1) {
			value = value.replaceAll("\\.", "");
			value = value.replaceAll(",", ".");
			return new BigDecimal(value);
			// ultimo separador eh um ponto
		} else {
			value = value.replaceAll(",", "");
			return new BigDecimal(value);
		}    
	}

	public final static Integer mod11(String value) {
		String basecalculo = value;
		int tamanho = basecalculo.length();
		int multiplicador = 2;
		int acumulador = 0;
		Integer result = null;
		String um = null;

		for (int i = tamanho; i > 0; i--) {
			um = basecalculo.substring(i-1, i);
			acumulador = acumulador + (Integer.parseInt(um)*multiplicador); 
			multiplicador++;
			if (multiplicador > 9) {
				multiplicador = 2;
			}
		}

		int digito = 11 - (acumulador % 11); 

		if ((digito == 0) || (digito == 10) || (digito == 11)) {
			result = 1;
		} else {
			result = digito;
		}
		return result;
	}

	public final static double round(double value, Integer scale){
		BigDecimal vlr = new BigDecimal(value);
		vlr = vlr.setScale(scale, BigDecimal.ROUND_HALF_EVEN);
		return vlr.doubleValue();
	}

}


