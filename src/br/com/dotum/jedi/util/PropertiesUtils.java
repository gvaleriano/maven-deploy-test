
package br.com.dotum.jedi.util;



import java.io.InputStream;
import java.util.Properties;

import br.com.dotum.jedi.core.exceptions.WarningException;

public final class PropertiesUtils {

	private static final String FILE_NAME = "/config.properties";
	private static Properties props;

	public final static void iniciaAplicacao() {
		InputStream is = null;
		try {
			is = PropertiesUtils.class.getResourceAsStream(FILE_NAME);

			props = new Properties();
			if (is != null) props.load(is);
		} catch (Exception e) {
			throw new WarningException(e.getMessage(), e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					throw new WarningException(e.getMessage(), e);
				}
			}
		}
	}

	public final static boolean getBoolean(String propertie) {
		String propStr = PropertiesUtils.get(propertie, false);
		if (StringUtils.hasValue(propStr) == false) propStr = "false";  
		boolean prop = new Boolean(propStr);
		return prop;
	}

	public final static String getString(String propertie) {
		return get(propertie, false);
	}

	public final static String getString(String propertie, boolean reload) {
		return get(propertie, reload);
	}

	public final static String get(String propertie, boolean reload) {
		if ((props == null) || (reload == true)) iniciaAplicacao();
		return props.getProperty(propertie);
	}

}