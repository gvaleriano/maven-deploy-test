package br.com.dotum.jedi.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.dotum.jedi.core.security.UserContext;
import bsh.Interpreter;


public final class SQLUtils {

	public final static String intToIn(Integer[] array) throws Exception {
		if (array == null) throw new Exception("Array está nulo");
		StringBuilder tps = new StringBuilder();
		String temp = "";
		tps.append("(");
		for (int i = 0; i < array.length; i++) {
			tps.append(temp);
			tps.append(array[i]);
			temp = ", ";
		}
		tps.append(")");
		return tps.toString();
	}
	
	
	public final static String intToIn(ArrayList<Integer> array) throws Exception {
		if (array == null) throw new Exception("Array está nulo");
		StringBuilder tps = new StringBuilder();
		String temp = "";
		tps.append("(");
		for (int i = 0; i < array.size(); i++) {
			tps.append(temp);
			tps.append(array.get(i));
			temp = ", ";
		}
		tps.append(")");
		return tps.toString();
	}
	
	public final static String strToIn(String[] array) throws Exception {
		if (array == null) throw new Exception("Array está nulo");
		StringBuilder tps = new StringBuilder();
		String temp = "";
		tps.append("(");
		for (int i = 0; i < array.length; i++) {
			tps.append(temp);
			tps.append(array[i]);
			temp = ", ";
		}
		tps.append(")");
		return tps.toString();
	}

	public final static String parseQuery(String sql, Map<String, Object> parmsValue) throws Exception {
		if (parmsValue == null) return sql;

		for (String key : parmsValue.keySet()) {
			Object value = parmsValue.get(key);
			if (value == null) value = "null";

			String prefix = "\\$P!\\{";
			String posfix = "}";
			String search = prefix+key;
			if (value.equals("null")) {
				sql = sql.replaceAll( search+posfix, (String)value);
				sql = sql.replaceAll( search+"\\[0\\]"+posfix, (String)value);
				sql = sql.replaceAll( search+"\\[1\\]"+posfix, (String)value);
				
			} else if ( value instanceof String) {
				sql = sql.replaceAll( search+posfix, (String)value);
			} else if ( value instanceof String[]) {
				sql = sql.replaceAll( search+posfix, SQLUtils.strToIn((String[])value));
			} else if ( value instanceof Integer[]) {
				sql = sql.replaceAll( search+posfix, SQLUtils.intToIn((Integer[])value));
			} else if ( value instanceof Integer) {
				sql = sql.replaceAll( search+posfix, ""+value);
			} else if ( value instanceof Date) {
				sql = sql.replaceAll( search+posfix, FormatUtils.formatDate((Date)value));
			} else if ( value instanceof Date[]) {
				Date[] values = (Date[])value;
				for (int i = 0; i < values.length; i++) {
					Date data = values[i];
					sql = sql.replaceAll( search+"\\["+i+"\\]"+posfix, "'"+FormatUtils.formatDate(data)+"'");
				}
			} else if ( value instanceof Double) {
				sql = sql.replaceAll( search+posfix, FormatUtils.formatDouble((Double)value));
			}
		}	
		
		for (String key : parmsValue.keySet()) {
			Object value = parmsValue.get(key);
			if (value == null) value = "null";

			// PARAMETRO COM ESQUEMA DE TIPO DE VALORES PREPARADO PARA A QUERY
			String prefix = "\\$P\\{";
			String posfix = "}";
			String search = prefix+key;
			if (value.equals("null")) {
				sql = sql.replaceAll( search+posfix, (String)value);
				sql = sql.replaceAll( search+"\\[0\\]"+posfix, (String)value);
				sql = sql.replaceAll( search+"\\[1\\]"+posfix, (String)value);

			} else if ( value instanceof String) {
				if (key.equals("PARAM_ORDER")) {
					sql = sql.replaceAll( search+posfix, (String)value );
				} else {
					sql = sql.replaceAll( search+posfix, "'"+(String)value+"'");
				}
			} else if ( value instanceof String[]) {
				sql = sql.replaceAll( search+posfix, SQLUtils.strToIn((String[])value));
			} else if ( value instanceof Integer) {
				sql = sql.replaceAll( search+posfix, ""+value);
			} else if ( value instanceof Integer[]) {
				sql = sql.replaceAll( search+posfix, SQLUtils.intToIn((Integer[])value));
			} else if ( value instanceof Date) {
				sql = sql.replaceAll( search+posfix, "'"+FormatUtils.formatDate((Date)value) +"'");
			} else if ( value instanceof Date[]) {
				Date[] values = (Date[])value;
				for (int i = 0; i < values.length; i++) {
					Date data = values[i];
					sql = sql.replaceAll( search+"\\["+i+"\\]"+posfix, "'"+FormatUtils.formatDate(data)+"'");
				}
			} else if ( value instanceof Double) {
				sql = sql.replaceAll( search+posfix, FormatUtils.formatDouble((Double)value));
			}
		}		
		return sql;
	}

	public final static String parseQueryParamDefault(String sql, UserContext userContext) {
		if (sql == null) return null;
		if (userContext == null) return sql;
		
		sql = sql.replaceAll("\\$V\\{usuarioId}", ""+userContext.getUser().getId());
		sql = sql.replaceAll("\\$V\\{unidadeId}", ""+userContext.getUnidade().getId());
		sql = sql.replaceAll("\\$V\\{unidadesId}", ""+userContext.getUnidades());
		//
		Date dataBasePagamento = (Date) userContext.getValues().get("dataBasePagamento");
		if (dataBasePagamento == null) dataBasePagamento = userContext.getDataAtual();
		sql = sql.replaceAll("\\$V\\{dataBasePagamento}", FormatUtils.formatDate(dataBasePagamento));
		//
		return sql;
	}
	
	public final static String parseDinamicQuery(String sql) throws Exception{
		Interpreter it = new Interpreter();
		
		int x = 0;
        int y = 0;
        do{
	        x = sql.indexOf("/*[dinamic]");
	        y = sql.indexOf("[/dinamic]*/");
	        	
	        if(x != -1){
	        	if(y == -1) throw new Exception("Não foi encontrado a tag de fechamento [/dinamic]*/");
	        	
	        	String result = (String) it.eval(sql.substring(x+11, y));
	        	sql = sql.substring(0, x)+result+sql.substring(y+12, sql.length());
	        }
        }while(x != -1);
        
        return sql;
	}

	public final static List<String> getFields(int i, String query, boolean repetir) {
		List<String> fields = new ArrayList<String>();
		Pattern pt = Pattern.compile("\\$P\\{([^}]*)\\}");
		Matcher m = pt.matcher(query);
		while (m.find()) {
			String xxx = m.group(i);
			if ((repetir == false) && (fields.contains(xxx))) continue;
			fields.add(xxx);
		}

		pt = Pattern.compile("\\$P!\\{([^}]*)\\}");
		m = pt.matcher(query);
		while (m.find()) {
			String xxx = m.group(i);
			if ((repetir == false) && (fields.contains(xxx))) continue;
			fields.add(xxx);
		}

		pt = Pattern.compile("\\$V\\{([^}]*)\\}");
		m = pt.matcher(query);
		while (m.find()) {
			String xxx = m.group(i);
			if ((repetir == false) && (fields.contains(xxx))) continue;
			fields.add(xxx);
		}

		pt = Pattern.compile("\\$V!\\{([^}]*)\\}");
		m = pt.matcher(query);
		while (m.find()) {
			String xxx = m.group(i);
			if ((repetir == false) && (fields.contains(xxx))) continue;
			fields.add(xxx);
		}
		return fields;
	}
	
	public static String formatPartStringToShow(String value) {
		return formatPartStringToShow(value, null);
	}
	public static String formatPartStringToShow(String value, Integer size) {
		if (StringUtils.hasValue(value) == false) return "";
		if (size == null) size = 500;
		
		String sqlTemp = value.replaceAll("\r\n", " ").replaceAll("\n\r", " ").replaceAll("\r", " ").replaceAll("\n", " ").replaceAll("\t", " ");
		while (sqlTemp.contains("  ")) sqlTemp = sqlTemp.replaceAll("  ", " ");
		sqlTemp = (sqlTemp.length() > size ? sqlTemp.substring(0, size) + "..." : sqlTemp);
		
		return sqlTemp;
	}
	
}