

package br.com.dotum.jedi.util;

import java.text.ParseException;
import java.util.Date;

public final class SQLiteUtils {

	/**
	 * Formata a data do SQLite, padrao yyyy-MM-dd para dd/MM/yyyy retorna uma
	 * string para ser concatenada ao select
	 * 
	 * @param column
	 * @return
	 */
	public final static String functionFormatDate(String column) {
		return functionFormatDate(column, "%d/%m/%Y");
	}
	

	/**
	 * Formata a data do SQLite, padrao yyyy-MM-dd para dd/MM/yyyy retorna uma
	 * string para ser concatenada ao select
	 * 
	 * @param column
	 * @return
	 */
	public final static String functionFormatDate(String column, String format) {
		StringBuilder result = new StringBuilder();
		result.append("strftime('");
		result.append(format);
		result.append("', ");
		result.append(column);
		result.append(") as ");
		if (column.indexOf(".") != -1) {
			result.append(column.substring(column.indexOf(".") + 1,
					column.length()));
		} else {
			result.append(column);
		}
		return result.toString();
	}

	public final static String formatDate(Date date) {
		return FormatUtils.formatDate(date, "yyyy-MM-dd");
	}

	public final static Date parseDate(String dateStr) throws ParseException {
		return FormatUtils.parseDateWithFormat(dateStr, "yyyy-MM-dd");
	}

}


