package br.com.dotum.jedi.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class StringUtils {

	
	public final static String brokenStrings(String value, String token, int count, String separator) {
		StringBuilder sb = new StringBuilder();

		String[] str = value.split(token);
		for (int i = 0; i < str.length; i++) {
			sb.append(str[i]);
			if ((i % count) == 0) {
				sb.append("\n");
			} else {
				if (i < str.length - 1) sb.append(separator);
			}
		}

		
		return sb.toString();
	}

	public final static String brokenStrings(String value, int max) {
		StringBuilder sb = new StringBuilder();

		while (true) {
			if (value.length() >= max) {
				String temp = value.substring(0, max);
				sb.append(temp);
				sb.append("\n");
				value = value.substring(max);
				continue;
			} else {
				sb.append(value);				
			}
			break;
		}
		return sb.toString();
	}

	public final static String[] partString(String value, int max) {
		List<String> result = new ArrayList<String>();
		while (true) {
			if (value.length() >= max) {
				String temp = value.substring(0, max);
				result.add(temp);
				value = value.substring(max);
				continue;
			} else {
				result.add(value);				
			}
			break;
		}
		return result.toArray(new String[result.size()]);
	}

	/**
	 * Metodo que retorna a primeira palavra do parametro passado.
	 * <br />
	 * <br />
	 * @param word
	 * @return
	 */
	public final static String getFirstWord(String word) {
		if (word == null) return null;
		int pos = word.indexOf(" ");
		if (pos != -1) {
			return word.substring(0, pos);
		}
		return word;
	}

	public final static String getLastWord(String word) {
		if (word == null) return null;
		int pos = word.lastIndexOf(" ");
		if (pos != -1) {
			return word.substring(pos);
		}
		return word;
	}

	/**
	 * Metodo que retorna se existe ou não numeros em uma palavra
	 * <br />
	 * <br />
	 * @param word
	 * @return
	 */
	public final static boolean isContainsNumbers(String word) {
		if (word == null) return false;
		//
		String numbers = "0123456789";
		for (int i = 0; i < word.length(); i++) {
			String str = word.substring(i,i+1);
			if (numbers.contains(str) == true) {
				return true;
			}
		}
		return false;
	}

	public final static boolean isContainsLetters(String word) {
		if (word == null) return false;
		//
		String letters = "abcdefghijklmnopqrstuvxzwykABCDEFGHIJKLMNOPQRSTUVXZWYK";
		for (int i = 0; i < word.length(); i++) {
			String str = word.substring(i,i+1);
			if (letters.contains(str) == true) {
				return true;
			}
		}
		return false;
	}

	public final static boolean isNumberOnly(String word) {
		if (word == null) return false;
		//
		String numbers = "0123456789";
		for (int i = 0; i < word.length(); i++) {
			String str = word.substring(i,i+1);
			if (numbers.contains(str) == false) {
				return false;
			}
		}
		return true;
	}

	public final static boolean isDateValid(String value) {
		return isDateValid(value, "dd/MM/yyyy");
	}

	public final static boolean isDateValid(String value, String format) {
		try {
			if (value == null) return false;
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			sdf.parse(value);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	/**
	 * Metodo que retorna apenas os numeros de uma palavra
	 * <br />
	 * <br />
	 * @param word
	 * @return String
	 */
	public final static String getNumberOnly(String word, String numbers) {
		if (word == null) return null;
		//
		StringBuffer sb = new StringBuffer(); 
		for (int i = 0; i < word.length(); i++) {
			String str = word.substring(i,i+1);
			if (numbers.contains(str)) {
				sb.append(str);
			}
		}
		return sb.toString();
	}

	public final static String getNumberOnly(String word) {
		return getNumberOnly(word, "0123456789");
	}

	public final static String getLettersOnly(String word) {
		if (word == null) return null;
		//
		String numbers = "abcdefghijklmnopqrstuvyxwzABCDEFGHIJKLMNOPQRSTUVYXWZ";
		StringBuffer sb = new StringBuffer(); 
		for (int i = 0; i < word.length(); i++) {
			String str = word.substring(i,i+1);
			if (numbers.contains(str)) {
				sb.append(str);
			}
		}
		return sb.toString();
	}
	public final static String getLettersAndNumberOnly(String word) {
		if (word == null) return null;
		//
		String numbers = "abcdefghijklmnopqrstuvyxwzABCDEFGHIJKLMNOPQRSTUVYXWZ0123456789";
		StringBuffer sb = new StringBuffer(); 
		for (int i = 0; i < word.length(); i++) {
			String str = word.substring(i,i+1);
			if (numbers.contains(str)) {
				sb.append(str);
			}
		}
		return sb.toString();
	}


	/**
	 * Metodo que retorna apenas as letras de um texto
	 * <br />
	 * <br />
	 * @param word
	 * @return String
	 */
	public final static String formatWordForNameFile(String word) {
		if (word == null) return null;
		//

		word = StringUtils.removeAccent(word);

		String letters = "abcdefghijklmnopqrstuvyxwzABCDEFGHIJKLMNOPQRSTUVYXWZ._1234567890";
		StringBuffer sb = new StringBuffer(); 
		for (int i = 0; i < word.length(); i++) {
			String str = word.substring(i,i+1);
			if (letters.contains(str)) {
				sb.append(str);
			}
		}
		return sb.toString();
	}

	public final static String reverse(String str) {
		StringBuilder sb = new StringBuilder();
		sb.append(str);
		sb.reverse();
		return sb.toString();		
	}

	public final static long getNumber(String word) {
		StringBuffer sb = new StringBuffer(); 
		for (int i = 0; i < word.length(); i++) {
			try {
				String str = word.substring(i,i+1);
				Long.parseLong(str);
				sb.append(str);
			} catch (Exception ex) {
			}
		}
		Long result = new Long(sb.toString());
		return result;
	}

	public final static String fillZeroLeft(String str, int count) {
		return fillLeft(str, '0', count);
	}

	public final static String fillZeroRight(String str, int count) {
		return fillRight(str, '0', count);
	}

	public final static String limitString(String str, int limit) {
		if (str.length() > limit) return str.substring(0, limit);
		return str;

	}

	public final static String fillLeft(String str, char charFill, int length) {
		StringBuffer result = new StringBuffer();
		if (str.length() >= length) {
			return result.append(str.substring(0, length)).toString();
		} else {
			result.append(str);
			int qtde = length - str.length();

			for (int i = 0; i < qtde; i++) {
				result.insert(0, charFill);
			}
			return result.toString();
		}
	}

	public final static String fillLeft(long str, char charFill, int length) {
		return fillLeft(str+"", charFill, length);
	}

	public final static String fillLeft(int str, char charFill, int length) {
		return fillLeft(str+"", charFill, length);
	}

	public final static String fillLeft(double str, char charFill, int length) {
		return fillLeft(str+"", charFill, length);
	}

	public final static String fillRight(String str, char charFill, int length) {
		StringBuffer result = new StringBuffer();
		if (str.length() >= length) {
			return result.append(str.substring(0, length)).toString();
		} else {
			result.append(str);

			for (int i = result.length(); i < length; i++) {
				result.insert(i, charFill);
			}
			return result.toString();
		}
	}

	public final static String fillCenter(String str, char charFill, int length) {
		String newstr = new String(str);
		if (str.length() < length) {
			int left = ((length - newstr.length()) / 2);
			newstr = fillLeft(newstr, charFill, (length-left));
			newstr = fillRight(newstr, charFill, length);
		}
		return newstr;
	}

	public final static String centerText(String text, int length) {
		return fillCenter(text, ' ', length);
	}

	public final static String addStringAfter(String target, String token, String value) throws Exception {
		int pos = target.indexOf(token);
		int len = token.length();
		if (pos == -1) {
			throw new Exception("String "+token+" não encontrada em "+target);          
		}
		StringBuilder sb = new StringBuilder();
		sb.append(target.substring(0,pos+len));
		sb.append(value);
		sb.append(target.substring(pos+len));
		return sb.toString();
	}

	public final static String addStringBefore(String target, String token, String value) throws Exception {
		int pos = target.indexOf(token);
		if (pos == -1) {
			throw new Exception("String "+token+" não encontrada em "+target);          
		}
		StringBuilder sb = new StringBuilder();
		sb.append(target.substring(0,pos));
		sb.append(value);
		sb.append(target.substring(pos));
		return sb.toString();
	}

	/**
	 * Metodo que remove os acentos de uma palavra
	 * <br />
	 * <br />
	 * @param word
	 * @return
	 */
	public final static String removeBreakLine(String text) {
		if (text == null) return null;
		return text.replaceAll("\n", "").replaceAll("\r", "").replaceAll("\r\n", "");
	}
	public final static String removeAccent(String word) {
		if (word == null) return null;

		String result = word;
		result = result.replaceAll("[äàáâã]","a");
		result = result.replaceAll("[ëèéê]","e");
		result = result.replaceAll("[ïìíî]","i");
		result = result.replaceAll("[öòóôõ]","o");
		result = result.replaceAll("[üùúû]","u");

		result = result.replaceAll("[ÄÀÁÂÃ]","A");
		result = result.replaceAll("[ËÈÉÊ]","E");
		result = result.replaceAll("[ÏÌÍÎ]","I");
		result = result.replaceAll("[ÖÒÓÔÕ]","O");
		result = result.replaceAll("[ÜÙÚÛ]","U");

		result = result.replaceAll("ç","c");
		result = result.replaceAll("Ç","C");

		result = result.replaceAll("ñ","n");
		result = result.replaceAll("Ñ","N");

		result = result.replaceAll("[ýÿ]","y");
		result = result.replaceAll("[Ý]","Y");

		result = result.replaceAll("º",".");
		result = result.replaceAll("ª",".");

		return result;
	}

	public final static String removeSpecial(String word) {
		if (word == null) return null;

		String result = word;
		result = result.replaceAll("&", "e");
		result = result.replaceAll("/", " ");
		result = result.replaceAll("\\\\", " ");
		result = result.replaceAll("%", " ");
		result = result.replaceAll("@", " ");
		result = result.replaceAll("!", " ");
		result = result.replaceAll("\\(", " ");
		result = result.replaceAll("\\)", " ");
		result = result.replaceAll("\\*", " ");
		result = result.replaceAll("¹", " ");
		result = result.replaceAll("²", " ");
		result = result.replaceAll("³", " ");

		return result;
	}

	public final static String toUpperCaseFirstLetter(String word) {
		StringBuilder sb = new StringBuilder();
		sb.append(word.substring(0, 1).toUpperCase());
		sb.append(word.substring(1).toLowerCase());
		return sb.toString();
	}

	public final static boolean isValidCpfCnpj(String cpfCnpj) {
		// se estiver nulo, retorna verdadeiro
		if (cpfCnpj == null) return false;
		// remove os espacos, pontos, tracos, etc
		String cpfCnpjTratado = StringUtils.getNumberOnly(cpfCnpj);
		// se nao tiver 11 ou 14 digitos
		// nao eh nenhum dos dois, incorreto
		if (cpfCnpjTratado.length() == 11) {
			if (cpfCnpjTratado.equals("00000000000") || cpfCnpjTratado.equals("11111111111") || cpfCnpjTratado.equals("22222222222") ||
					cpfCnpjTratado.equals("33333333333") || cpfCnpjTratado.equals("44444444444") || cpfCnpjTratado.equals("55555555555") ||
					cpfCnpjTratado.equals("66666666666") || cpfCnpjTratado.equals("77777777777") || cpfCnpjTratado.equals("88888888888") ||
					cpfCnpjTratado.equals("99999999999")) {
				return false;
			}
			// converte para um numero
			long cpfNumero = Long.parseLong(cpfCnpjTratado);
			cpfNumero += 100000000000L;
			//
			int calculo = 0;
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(1, 2)) * 10);
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(2, 3)) * 9);
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(3, 4)) * 8);
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(4, 5)) * 7);
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(5, 6)) * 6);
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(6, 7)) * 5);
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(7, 8)) * 4);
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(8, 9)) * 3);
			calculo += (Integer.parseInt(String.valueOf(cpfNumero).substring(9, 10)) * 2);
			//
			int calculoAux = (calculo / 11);
			//
			int digitoAux = (calculo - 11 * calculoAux);
			int digitoTmp = 0;
			if ((digitoAux != 1) && (digitoAux != 0)) {
				digitoTmp = (11 - digitoAux);
			}
			int digitoOk = (digitoTmp * 10);
			//
			int calculo2 = 0;
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(1, 2)) * 11);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(2, 3)) * 10);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(3, 4)) * 9);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(4, 5)) * 8);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(5, 6)) * 7);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(6, 7)) * 6);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(7, 8)) * 5);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(8, 9)) * 4);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(9, 10)) * 3);
			calculo2 += (Integer.parseInt(String.valueOf(cpfNumero).substring(10, 11)) * 2);
			//
			int calculoAux2 = (calculo2 / 11);
			//
			int digitoAux2 = (calculo2 - 11 * calculoAux2);
			int digitoTmp2 = 0;
			if ((digitoAux2 != 1) && (digitoAux2 != 0)) {
				digitoTmp2 = (11 - digitoAux2);
			}
			//
			digitoOk = (digitoOk + digitoTmp2);
			//
			if (Integer.parseInt(String.valueOf(cpfNumero).substring(10, 12)) != digitoOk) {
				return false;
			}
			// senao esta ok
			return true;
		} else if ((cpfCnpjTratado.length() == 14) || (cpfCnpjTratado.length() == 15)) {
			if (cpfCnpjTratado.equals("000000000000000") || cpfCnpjTratado.equals("111111111111111") || cpfCnpjTratado.equals("222222222222222") ||
					cpfCnpjTratado.equals("333333333333333") || cpfCnpjTratado.equals("444444444444444") || cpfCnpjTratado.equals("555555555555555") ||
					cpfCnpjTratado.equals("666666666666666") || cpfCnpjTratado.equals("777777777777777") || cpfCnpjTratado.equals("888888888888888") || 
					cpfCnpjTratado.equals("9999999999999999")) {
				return false;
			}
			// converte para um numero
			long cnpjNumero = Long.parseLong(cpfCnpjTratado);
			cnpjNumero += 100000000000000L;
			//
			int calculo = 0;
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(1, 2)) * 5);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(2, 3)) * 4);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(3, 4)) * 3);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(4, 5)) * 2);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(5, 6)) * 9);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(6, 7)) * 8);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(7, 8)) * 7);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(8, 9)) * 6);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(9, 10)) * 5);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(10, 11)) * 4);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(11, 12)) * 3);
			calculo += (Integer.parseInt(String.valueOf(cnpjNumero).substring(12, 13)) * 2);
			//
			int calculoAux = (calculo / 11);
			//
			int digitoAux = (calculo - 11 * calculoAux);
			int digitoTmp = 0;
			if ((digitoAux != 0) && (digitoAux != 1)) {
				digitoTmp = (11 - digitoAux);
			}
			//
			int digitoOk = (digitoTmp * 10);
			//
			int calculo2 = 0;
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(1, 2)) * 6);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(2, 3)) * 5);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(3, 4)) * 4);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(4, 5)) * 3);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(5, 6)) * 2);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(6, 7)) * 9);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(7, 8)) * 8);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(8, 9)) * 7);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(9, 10)) * 6);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(10, 11)) * 5);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(11, 12)) * 4);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(12, 13)) * 3);
			calculo2 += (Integer.parseInt(String.valueOf(cnpjNumero).substring(13, 14)) * 2);
			//
			int calculoAux2 = (calculo2 / 11);
			//
			int digitoAux2 = (calculo2 - 11 * calculoAux2);
			int digitoTmp2 = 0;
			if ((digitoAux2 != 0) && (digitoAux2 != 1)) {
				digitoTmp2 = (11 - digitoAux2);
			}
			//
			digitoOk = digitoOk + digitoTmp2;
			//
			if (Integer.parseInt(String.valueOf(cnpjNumero).substring(13, 15)) != digitoOk) {
				return false;
			}
			// senao esta ok
			return true;
		}
		// se nao entrou em uma das condicoes
		// acima eh porque a quantidade de digitos
		// esta incorreta, deve ser 11 ou 14
		return false;
	}

	/**
	 *if ((value != null) && (value.equals("") == false)) return true;<br/>
	 *return false;
	 * @param value
	 * @return
	 */
	public final static boolean hasValue(String value) {
		if ((value != null) && (value.equals("") == false))
			return true;

		return false;
	}

	public final static boolean hasValueWithTrim(String value) {
		if ((value != null) && (value.equals("") == false) && (value.trim().equals("") == false))
			return true;

		return false;
	}

	public final static String getDigitoModulo11(String value) {
		return getDigitoModulo11(value, 9);
	}

	public final static String getDigitoModulo11(String value, int limite) {
		String basecalculo = value;
		int tamanho = basecalculo.length();
		int acumulador = 0;
		String result = null;
		String um = null;

		int multiplicador = 2;
		for (int i = tamanho; i > 0; i--) {
			um = basecalculo.substring(i-1, i);
			acumulador = acumulador + (Integer.parseInt(um) * multiplicador); 
			multiplicador++;
			if (multiplicador > limite) {
				multiplicador = 2;
			}
		}

		// multiplica por 10 e guarda o resto da divisao por 11
		result = String.valueOf( ((acumulador * 10) % 11) );

		return result;
	}

	public static int getModulo10(String num){  
		//variáveis de instancia
		int soma = 0;
		int resto = 0;
		int dv = 0;
		String[] numeros = new String[num.length()+1];
		int multiplicador = 2;
		String aux;
		String aux2;
		String aux3;
		for (int i = num.length(); i > 0; i--) {  	    	
			//Multiplica da direita pra esquerda, alternando os algarismos 2 e 1
			if(multiplicador%2 == 0){
				// pega cada numero isoladamente  
				numeros[i] = String.valueOf(Integer.valueOf(num.substring(i-1,i))*2);
				multiplicador = 1;
			}else{
				numeros[i] = String.valueOf(Integer.valueOf(num.substring(i-1,i))*1);
				multiplicador = 2;
			}
		}  
		// Realiza a soma dos campos de acordo com a regra
		for(int i = (numeros.length-1); i > 0; i--){
			aux = String.valueOf(Integer.valueOf(numeros[i]));
			if(aux.length()>1){
				aux2 = aux.substring(0,aux.length()-1);	    		
				aux3 = aux.substring(aux.length()-1,aux.length());
				numeros[i] = String.valueOf(Integer.valueOf(aux2) + Integer.valueOf(aux3));	    		
			}
			else{
				numeros[i] = aux;    		
			}
		}
		//Realiza a soma de todos os elementos do array e calcula o digito verificador
		//na base 10 de acordo com a regra.	    
		for(int i = numeros.length; i > 0 ; i--){
			if(numeros[i-1] != null){
				soma += Integer.valueOf(numeros[i-1]);
			}
		}
		resto = soma%10;
		dv = 10 - resto;
		//retorna o digito verificador
		return dv;
	}

	public final static int occurrencesCount(String value, String token) {

		int result = 0;
		String temp = value;
		while (true) {
			int pos = temp.indexOf(token);
			if (pos >= 0) {
				temp = temp.substring(pos+token.length());
				result++;
			} else {
				break;
			}
		}

		return result;
	}

	public final static String fillBetween(String str, char charFill, int between) {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < str.length(); i+=between) {
			if ( (i+between) < str.length()) {
				result.append(str.substring(i, i+between)+charFill);
			} else {
				result.append(str.substring(i)+charFill);
			}
		}
		return result.toString();
	}


	public final static String coalesce(String s1, String s2) {
		if (s1 == null) return s2;
		return s1;
	}

	public final static String coalesce(Integer s1, String s2) {
		if (s1 == null) return ((StringUtils.hasValue(s2) == true) ? s2 : "");
		return ""+s1;
	}

	public final static boolean search(String value, String[] array) {
		if (array == null) return false;
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(value)) return true;
		}
		return false;
	}

	public final static String parse(Object value) {
		if (value == null) return null;
		if (value instanceof String) return (String)value;
		return null;
	}

	public final static String stream2str(InputStream is) throws IOException {	 
		if (is != null) {
			Writer writer = new StringWriter();
			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		}
		else {
			return "";
		}
	}

	public final static String exceptionStack2Str(Exception ex) {
		StringBuffer result = new StringBuffer();
		StringWriter sw = new StringWriter(); 
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		result = sw.getBuffer();
		return result.toString();
	}

	public final static String getTag(String xml, String tag) {
		int pos1 = xml.indexOf("<"+tag);
		int pos2 = xml.indexOf("</"+tag);

		if ((pos1 == -1) || (pos2 == -1)) return null;

		int len = tag.length();
		return xml.substring(pos1, pos2+len+3);
	}

	public final static String getTagValue(String xml, String tag) {
		try {
			String tagFull = getTag(xml, tag); // isso retorna a tag completa <tag id="zz">xxx</tag>
			int pos1 = tagFull.indexOf(">");
			int pos2 = tagFull.indexOf("</"+tag);

			if ((pos1 == -1) || (pos1 == -1)) return null;
			return tagFull.substring(pos1+1, pos2);
		} catch (Exception ex) {
			return null;
		}
	}


	public final static String getNumeroExtenso(Long num) {
		return getNumeroExtenso(num, null);
	}
	public final static String getNumeroExtenso(Long num, Integer maxlen) {
		String nome[] = {"um bi-lhão", " bi-lhões", "um mi-lhão", " mi-lhões"};
		String s = "";
		long n = (long)num;
		long mil_milhoes;
		long milhoes;
		long milhares;
		long unidades;
		int rp;
		int last;
		int p;
		int len;


		if (num == 0) return "zero";

		mil_milhoes = (n - n % 1000000000) / 1000000000;
		n -= mil_milhoes * 1000000000;
		milhoes = (n - n % 1000000) / 1000000;
		n -= milhoes * 1000000;
		milhares = (n - n % 1000) / 1000;
		n -= milhares * 1000;
		unidades = n;

		//                      s = "\0";
		//s[0] = '\0' ; //??
		if (mil_milhoes > 0) {
			if (mil_milhoes == 1) {
				s += nome[0];
			} else {
				s += numero(mil_milhoes);
				s += nome[1];
			}
			if ((unidades == 0) && ((milhares == 0) && (milhoes > 0))) {
				s += " e ";
			} else if ((unidades != 0) || ((milhares != 0) || (milhoes != 0))) {
				s += ", ";
			}
		}
		if (milhoes > 0) {
			if (milhoes == 1) {
				s += nome[2];
			} else {
				s += numero(milhoes);
				s += nome[3];
			}
			if ((unidades == 0) && (milhares > 0)) {
				s += " e ";
			} else if ((unidades != 0) || (milhares != 0)) {
				s += ", ";
			}
		}
		if (milhares > 0) {
			if (milhares != 1) {
				s += numero(milhares);
			}
			s += " mil";
			if (unidades > 0) {
				if ((milhares > 100) && (unidades > 100)) {
					s += ", ";
				} else if (((unidades % 100) != 0) || ((unidades % 100 == 0) &&
						(milhares < 10))) {
					s += " e ";
				} else {
					s += " ";
				}
			}
		}
		s += numero(unidades);

		len = s.length();
		StringBuffer sar = new StringBuffer(s);
		StringBuffer l = new StringBuffer();
		last = 0;
		rp = 0;

		for (p = 0; p < len; ++p) {
			if (sar.charAt(p) != '-') {
				rp++;
			}
			if (((maxlen != null)) && (rp > maxlen)) {
				if (sar.charAt(last) == ' ') {
					sar.replace(last, last + 1, "\n");
				} else {
					sar.insert(last + 1, '\n');
				}
				rp -= maxlen;
			} else {

			}
			if ((sar.charAt(p) == ' ') || (sar.charAt(p) == '-')) {
				last = p;
			}
		} //for
		rp = 0;
		len = sar.length();

		for (p = 0; p < len; ++p) {
			if (!((sar.charAt(p) == '-') && (sar.charAt(p + 1) != '\n'))) {
				l.insert(rp++, sar.charAt(p));
			}
		} //for

		s = l.toString();
		return s;
	}

	public final static String formatArrayToString(Object[] values, String token) {
		StringBuilder sb = new StringBuilder();

		String temp = "";
		for (Object xxx : values) {
			sb.append(temp);
			sb.append(xxx);
			temp = token;
		}

		return sb.toString();
	}

	private final static String numero(long n) {
		String u[] = {"", "um", "dois", "tres", "qua-tro", "cin-co", "seis",
				"se-te", "oi-to", "no-ve", "dez", "on-ze", "do-ze", "tre-ze", "ca-tor-ze",
				"quin-ze", "de-zas-seis", "de-zas-sete", "de-zoi-to", "de-za-no-ve"};
		String d[] = {"", "", "vin-te", "trin-ta", "qua-ren-ta", "cin-quen-ta",
				"ses-sen-ta", "se-ten-ta", "oi-ten-ta", "no-ven-ta"};
		String c[] = {"", "cen-to", "du-zen-tos", "tre-zen-tos",
				"qua-tro-cen-tos", "qui-nhen-tos", "seis-cen-tos", "se-te-cen-tos",
				"oi-to-cen-tos", "no-ve-cen-tos"};
		String extenso_do_numero = new String();
		//                      extenso_do_numero  = "\0" ;
		if ((n < 1000) && (n != 0)) {
			if (n == 100) {
				extenso_do_numero = "cem";
			} else {
				if (n > 99) {
					extenso_do_numero += c[(int)(n / 100)];
					if (n % 100 > 0) {
						extenso_do_numero += " e ";
					}
				}
				if (n % 100 < 20) {
					extenso_do_numero += u[(int)n % 100];
				} else {
					extenso_do_numero += d[((int)n % 100) / 10];
					if ((n % 10 > 0) && (n > 10)) {
						extenso_do_numero += " e ";
						extenso_do_numero += u[(int)n % 10];
					}
				}
			}
		} else if (n > 999) {
			extenso_do_numero = "<<ERRO: NUMERO > 999>>";
		}
		return extenso_do_numero;
	}

	public static String arrayToString(Object[] array) {
		return Arrays.toString(array).replace("{", "").replace("}", "");
	}

	public static String setSize(String value, int size, char c, int leftRight) throws Exception{
		String result = value;

		if(value.length()>size){
			result = value.substring(0, size);
		}else if(value.length()<size){
			if(leftRight==1)result = putCharacterLeft(value, c, size);
			else if(leftRight==2)result = putCharacterRight(value, c, size);
			else throw new Exception("Parametro incorreto.");
		}

		return result;
	}
	public static String putCharacterLeft(String value, char character, int size) {
		int length = value.length();
		int diff = size - length;
		if (diff < 0) return value;

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < diff; i++) {
			sb.append(character);
		}
		sb.append(value);


		return sb.toString();
	}

	public static String putCharacterRight(String value, char character, int size) {
		int length = value.length();
		int diff = size - length;
		if (diff < 0) return value;

		StringBuilder sb = new StringBuilder();
		sb.append(value);
		for (int i = 0; i < diff; i++) {
			sb.append(character);
		}

		return sb.toString();
	}

	public static String parseStrToHtml(String value) {
		value = (value != null ? value.replaceAll("\r\n", "<br />") : "");
		value = (value != null ? value.replaceAll("\n", "<br />") : "");
		value = (value != null ? value.replaceAll("\r", "<br />") : "");

		return value;
	}


	public static String parseHtmlToStr(String value) {
		value = (value != null ? value.replaceAll("<br />", "\r\n") : "");
		value = (value != null ? value.replaceAll("<br>", "\r\n") : "");
		value = (value != null ? value.replaceAll("<br />", "\n") : "");
		value = (value != null ? value.replaceAll("<br>", "\n") : "");
		return value;
	}

	public static String unescapeXML( final String xml )
	{
		Pattern xmlEntityRegex = Pattern.compile( "&(#?)([^;]+);" );
		//Unfortunately, Matcher requires a StringBuffer instead of a StringBuilder
		StringBuffer unescapedOutput = new StringBuffer( xml.length() );

		Matcher m = xmlEntityRegex.matcher( xml );
		Map<String,String> builtinEntities = null;
		String entity;
		String hashmark;
		String ent;
		int code;
		while ( m.find() ) {
			ent = m.group(2);
			hashmark = m.group(1);
			if ( (hashmark != null) && (hashmark.length() > 0) ) {
				code = Integer.parseInt( ent );
				entity = Character.toString( (char) code );
			} else {
				//must be a non-numerical entity
				if ( builtinEntities == null ) {
					builtinEntities = buildBuiltinXMLEntityMap();
				}
				entity = builtinEntities.get( ent );
				if ( entity == null ) {
					//not a known entity - ignore it
					entity = "&" + ent + ';';
				}
			}
			m.appendReplacement( unescapedOutput, entity );
		}
		m.appendTail( unescapedOutput );

		return unescapedOutput.toString();
	}

	private static Map<String,String> buildBuiltinXMLEntityMap() {
		Map<String,String> entities = new HashMap<String,String>(10);
		entities.put( "lt", "<" );
		entities.put( "gt", ">" );
		entities.put( "amp", "&" );
		entities.put( "apos", "'" );
		entities.put( "quot", "\"" );
		return entities;
	}


	public static String escapeXML(String s) {
		Pattern ESCAPE_XML_CHARS = Pattern.compile("[\"&'<>]");

		Matcher m = ESCAPE_XML_CHARS.matcher(s);
		StringBuffer buf = new StringBuffer();
		while (m.find()) {
			switch (m.group().codePointAt(0)) {
			case '"':
				m.appendReplacement(buf, "&quot;");
				break;
			case '&':
				m.appendReplacement(buf, "&amp;");
				break;
			case '\'':
				m.appendReplacement(buf, "&apos;");
				break;
			case '<':
				m.appendReplacement(buf, "&lt;");
				break;
			case '>':
				m.appendReplacement(buf, "&gt;");
				break;
			}
		}
		m.appendTail(buf);
		return buf.toString();
	}

	public static String gerarToken(int size) {
		return gerarToken(size, false);
	}
	public static String gerarToken(int size, boolean withCharSpecial) {
		String[] carct = null;
		if (withCharSpecial == true) {
			carct = new String[]{"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","!","@","#","$","%","&","*","(",")","-","+","{","}","]","[","]","_"};
		} else {
			carct = new String[]{"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
		}
		
		String result = ""; 
		for (int x = 0; x < size; x++){ 
			int j = (int) (Math.random() * carct.length); 
			result += carct[j]; 
		}
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println( getLettersOnly("ração para todos") );
	}

}



