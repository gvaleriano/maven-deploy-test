
package br.com.dotum.jedi.util;


import java.text.ParseException;
import java.util.Date;

public final class TokenService {

	public final static boolean validToken(String cnpj, String aplicacao, String seuId, String token) throws ParseException {
		String[] result = reversoToken(token);
		int size = result[1].length();
		if (result[0].equals(prepareCnpj(cnpj)) == false) return false;
		if (result[1].substring(size-2, size).equals(aplicacao) == false) return false;
		if (result[3].equals(prepareSeuId(seuId)) == false) return false;
		
		Date data = FormatUtils.parseDateWithFormat("20"+result[4], "yyyyMMdd");
		int diff = DateUtils.daysBetween(data, new Date()); 
		if (diff > 0) return false; 
		
		return true;
	}

	public final static String[] reversoToken(String token) {
		Integer numero = Integer.parseInt(token.substring(1, 2));
		String[] parts = token.split("-");
		int[] posicao = getPosicao(numero);
		String xxx = parts[0] + "-"+ parts[posicao[0]] + "-"+ parts[posicao[1]] + "-"+ parts[posicao[2]] + "-"+ parts[posicao[3]] + "-"+ parts[posicao[4]];
		String[] yyy = xxx.split("-");

		String[] result = new String[]{"", "", "", "", ""};
		for (int i = 0; i < 6; i++) {
			if ((i % 2) != 0) {
				result[0] += yyy[i].substring(4,5);
				result[1] += yyy[i].substring(3,4);
				result[2] += yyy[i].substring(2,3);
				result[3] += yyy[i].substring(1,2);
				result[4] += yyy[i].substring(0,1);
			} else {
				result[0] += yyy[i].substring(0,1);
				result[1] += yyy[i].substring(1,2);
				result[2] += yyy[i].substring(2,3);
				result[3] += yyy[i].substring(3,4);
				result[4] += yyy[i].substring(4,5);
			}
		}

		return result;
	}


	public final static String gerarToken(String cnpj, String aplicacao, String licenca, String seuId, String validade) {
		cnpj = prepareCnpj(cnpj);
		for (int i = 0; i < 4; i++) {aplicacao = randomico() + aplicacao;}
		for (int i = 0; i < 4; i++) {licenca = randomico() + licenca;}
		seuId = prepareSeuId(seuId);


		int x = 0;
		int y = 0;

		String[] parts = new String[8];
		parts[y++] = cnpj.substring(x, x+1) + aplicacao.substring(x, x+1) + licenca.substring(x, x+1) + seuId.substring(x, x+1) + validade.substring(x, x+1);
		x++;
		parts[y++] = validade.substring(x, x+1) + seuId.substring(x, x+1) + licenca.substring(x, x+1) + aplicacao.substring(x, x+1) + cnpj.substring(x, x+1);
		x++;
		parts[y++] = cnpj.substring(x, x+1) + aplicacao.substring(x, x+1) + licenca.substring(x, x+1) + seuId.substring(x, x+1) + validade.substring(x, x+1);
		x++;
		parts[y++] = validade.substring(x, x+1) + seuId.substring(x, x+1) + licenca.substring(x, x+1) + aplicacao.substring(x, x+1) + cnpj.substring(x, x+1);
		x++;
		parts[y++] = cnpj.substring(x, x+1) + aplicacao.substring(x, x+1) + licenca.substring(x, x+1) + seuId.substring(x, x+1) + validade.substring(x, x+1);
		x++;
		parts[y++] = validade.substring(x, x+1) + seuId.substring(x, x+1) + licenca.substring(x, x+1) + aplicacao.substring(x, x+1) + cnpj.substring(x, x+1);

		Integer numero = Integer.parseInt(parts[0].substring(1,2));
		int[] posicao = getPosicao(numero);
		return parts[0] + "-"+ parts[posicao[0]] + "-"+ parts[posicao[1]] + "-"+ parts[posicao[2]] + "-"+ parts[posicao[3]] + "-"+ parts[posicao[4]];
	}

	private final static String prepareSeuId(String seuId) {
		int size = seuId.length();
		String seuId1 = seuId.substring(0, 3);
		String seuId2 = seuId.substring(size - 3, size);

		return seuId1 + seuId2;
	}


	private final static String prepareCnpj(String cnpj) {
		int size = cnpj.length();
		String cnpj1 = cnpj.substring(0, 3);
		String cnpj2 = cnpj.substring(size-5, size-2);
		return cnpj1 + cnpj2;
	}


	private final static int[] getPosicao(int numero) {
		int[] posicao = {1, 2, 3, 4, 5};
		if (numero == 1) posicao = new int[]{5, 4, 3, 2, 1};
		if (numero == 2) posicao = new int[]{1, 2, 3, 5, 4};
		if (numero == 3) posicao = new int[]{2, 1, 5, 4, 3};
		if (numero == 4) posicao = new int[]{5, 4, 3, 2, 1};
		if (numero == 5) posicao = new int[]{3, 2, 1, 5, 4};
		if (numero == 6) posicao = new int[]{4, 3, 2, 1, 5};
		if (numero == 7) posicao = new int[]{5, 4, 3, 2, 1};
		if (numero == 8) posicao = new int[]{1, 5, 3, 4, 2};
		if (numero == 9) posicao = new int[]{2, 3, 5, 4, 1};
		return posicao;
	}

	public final static Integer randomico() {
		return ((Double)(Math.random() * 9.0)).intValue();
	}
}

