package capital.api;

import br.com.dotum.jedi.core.exceptions.JediException;

public class API400Exception extends JediException {

	private String code;
	private String message;
	
	public API400Exception(String code, String message) {
		this.code = code;
		this.message = message;		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
