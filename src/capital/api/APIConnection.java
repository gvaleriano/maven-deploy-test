package capital.api;

import br.com.dotum.jedi.core.table.Bean;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.HashMap;

public class APIConnection {

    public static String post(String URL, HashMap<String, String> header, JSONObject body) throws Exception {

        StringEntity entity = new StringEntity(body.toString(),
                ContentType.APPLICATION_JSON);

        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(URL);

        httpPost.setHeader("Content-Type", "application/json");

        if (header != null) {
            for (String key : header.keySet()) {
                String value = header.get(key);
                httpPost.setHeader(key, value);
            }
        }

        httpPost.setEntity(entity);
        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity respEntity = response.getEntity();

        if (respEntity != null) {
            String content =  EntityUtils.toString(respEntity);
            return content;
        }

        return null;
    }

    public static JSONObject get(String URL) throws Exception {
        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(URL);
        httpGet.setHeader("Content-Type", "application/json");
        HttpResponse response = httpClient.execute(httpGet);
        HttpEntity respEntity = response.getEntity();
        if (respEntity != null) {
            String content =  EntityUtils.toString(respEntity);
            JSONObject result = new JSONObject(content);
            return result;
        }
        return null;
    }

    public static Bean post(String URL, Bean body) throws Exception {
    	
    	StringEntity entity = new StringEntity(body.toJson2(),
    			ContentType.APPLICATION_JSON);
    	
    	HttpClient httpClient = HttpClients.createDefault();
    	HttpPost httpPost = new HttpPost(URL);
    	httpPost.setHeader("Content-Type", "application/json");
    	httpPost.setEntity(entity);
    	
    	HttpResponse response = httpClient.execute(httpPost);
    	HttpEntity respEntity = response.getEntity();
    	
    	if (respEntity != null) {
    		String content =  EntityUtils.toString(respEntity);
    		Bean result = new Bean();
    		result.toObject(content);
    		return result;
    	}
    	
    	return null;
    }
}