package capital.api;

import br.com.dotum.jedi.core.db.TransactionDB;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AbstractAction {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private JSONObject body;
    private String pathServer;
    private TransactionDB trans;

    public AbstractAction() {

    }

    public AbstractAction(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, JSONObject body, String pathServer) {
        this.request = request;
        this.response = response;
        this.trans = trans;
        this.body = body;
        this.pathServer = pathServer;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public Object getBodyParameter(String name) {
        return body.get(name);
    }

    public void addMessage(HttpServletResponse response, boolean success, String message) throws IOException {
        JSONObject json = new JSONObject();
        json.put("success", success);
        json.put("message", message);
        response.getWriter().print( json );
    }

    public String getPathServer() {
        return this.pathServer;
    }

    public void setTransaction(TransactionDB trans) {
        this.trans = trans;
    }
    public TransactionDB getTrans() { return this.trans; }
}