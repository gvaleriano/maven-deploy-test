package capital.api;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.ErrorException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.CarteiraBean;
import capital.model.bean.PessoaBean;
import capital.model.bean.RequestBean;
import capital.service.MsgService;
import com.payu.sdk.PayU;
import com.payu.sdk.PayUPayments;
import com.payu.sdk.PayUTokens;
import com.payu.sdk.exceptions.ConnectionException;
import com.payu.sdk.exceptions.InvalidParametersException;
import com.payu.sdk.exceptions.PayUException;
import com.payu.sdk.model.CreditCardToken;
import com.payu.sdk.model.Language;
import com.payu.sdk.model.PaymentCountry;
import com.payu.sdk.model.TransactionResponse;
import com.payu.sdk.utils.LoggerUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;

public class BrasPagService {

    public static final String NAME = "BRASPAG";
    private static Log LOG = LogFactory.getLog(BrasPagService.class);
    private static Properties props;
    private static final String FILE_NAME= "/braspag-config.properties";
    private static String merchant_id;
    private static String merchant_key;
    private static String api_service;

    static {
        iniciaProps();
    }

    public static Properties  iniciaProps() {
        InputStream is = null;
        try {
            is = PropertiesUtils.class.getResourceAsStream(FILE_NAME);

            props = new Properties();
            if (is != null) props.load(is);

            LOG.debug("Carregando configuração da BrasPag...");
            api_service = get("api_service");
            merchant_id = get("merchant_id");
            merchant_key = get("merchant_key");
            LOG.debug("merchant_id => "+merchant_id );
            LOG.debug("merchant_key => "+merchant_key);

            LOG.debug("Carregando configuração da BrasPag (finish)");

        } catch (Exception e) {
            throw new WarningException(e.getMessage(), e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    throw new WarningException(e.getMessage(), e);
                }
            }
        }
        return props;
    }

    public final static String get(String propertie) {
        if ((props == null)) iniciaProps();
        return props.getProperty(propertie);
    }

    public static Bean inclusaoDeCartao(String serverPath, TransactionDB trans, PessoaBean pesBean, HttpServletRequest request, CreditCardBean ccBean) throws Exception {

        Bean result = new Bean();
        String ano_str = ccBean.exp_year;
        if (ano_str.length() == 2) {
            ano_str = "20"+ano_str;
        }

        String validade = StringUtils.fillLeft( ccBean.exp_month+"", '0', 2 ) + "/" + ano_str ;
        String numero = StringUtils.getNumberOnly(ccBean.numero);

        if ((StringUtils.hasValue(numero) == false) || (numero.length() < 8)) {
            throw new API400Exception("numero_cartao_invalido","O número do cartão de crédito informado é inválido");
        }

        long start_time = System.currentTimeMillis();
        RequestBean gwRequestBean = new RequestBean();
        gwRequestBean.setData(trans.getDataAtual());
        gwRequestBean.setHora(trans.getHoraAtual());
        gwRequestBean.setEndpoint("criarToken");
        gwRequestBean.setHost( request.getLocalAddr() );

        // Payment JSON

        JSONObject MerchantOrderId = new JSONObject();
        MerchantOrderId.put("MerchantOrderId", trans.getId());

        JSONObject Customer = new JSONObject();
        Customer.put("Name",ccBean.nome);

        JSONObject Payment = new JSONObject();
        Payment.put("Provider", "Simulado");
        Payment.put("Type", "CreditCard");
        Payment.put("Type", "CreditCard");
        Payment.put("Amount", 1);
        Payment.put("Capture", false);
        Payment.put("Country", "BRA");
        Payment.put("Authenticate", false);
        Payment.put("Recurrent", false);
        Payment.put("Installments", 1);
        Payment.put("Currency", "BRL");

        JSONObject CreditCard = new JSONObject();
        CreditCard.put("CardNumber", numero);
        CreditCard.put("Holder", ccBean.nome);
        CreditCard.put("SaveCard", true);
        CreditCard.put("ExpirationDate", validade);

        Payment.put("CreditCard", CreditCard);

        JSONObject json_request = new JSONObject();
        json_request.put("MerchantOrderId", MerchantOrderId);
        json_request.put("Customer", Customer);
        json_request.put("Payment", Payment);
        String endpoint = "/v2/sales";

        gwRequestBean.setRequest(json_request.toString());

        // header
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("MerchantId", merchant_id);
        header.put("MerchantKey", merchant_key);

        // merchantOrderId vai na url
        String URL = api_service+endpoint;

        String json = APIConnection.post(URL, header, json_request);
        json = json.replace("\n", "");
        json = json.replace("\r", "");
        LOG.debug( "Result> "  + json);
        StringBuilder messages = new StringBuilder();
        if (json.startsWith("[")) {
            JSONArray array = new JSONArray(json);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = (JSONObject)array.get(i);
                messages.append( "["+obj.get("Message")+"] " );
            }
        } else {
            result.toObject(json);
        }

        String idCartao = null;
        if ((result.get("Status") != null) && ((int)result.get("Status") == 1)) {
            idCartao = result.get("CardToken");
            result.set("idCartao", idCartao);
            gwRequestBean.setGatewaySucesso(1);
        } else {
            gwRequestBean.setGatewaySucesso(0);
            gwRequestBean.setGatewayMensagem(messages.toString());
        }

        long end_time = System.currentTimeMillis();
        gwRequestBean.getPessoaCliente().setId(pesBean.getId());
        gwRequestBean.setTime(new Long(end_time - start_time).intValue());
        gwRequestBean.setGateway(BrasPagService.NAME);


        LOG.debug("Inserindo LOG Gateway...");
        TransactionDB trans2 = TransactionDB.getInstance(null);
        trans2.insert(gwRequestBean);
        trans2.commit();
        trans2.close();
        LOG.debug("LOG Gateway (inserido)");

//        if (idCartao.equals("") || idCartao == null) {
//            throw new ErrorException(MsgService.get(serverPath, "msg.pinbankservice.inclusaocartao.01", "" + result.get("descricaoRetorno")));
//        }
        return result;
    }

    public static Bean efetuarTransacaoWithCreditCard(TransactionDB trans, PessoaBean pesBean, CreditCardBean ccBean, Integer nsu, BigDecimal valor, HttpServletRequest request) throws Exception {

        BigDecimal novoValor = valor.multiply(new BigDecimal("100"));
        Integer valorInteiro = novoValor.intValue();

        String reference = nsu.toString();
        Bean result = new Bean();

        long start_time = System.currentTimeMillis();
        RequestBean gwRequestBean = new RequestBean();
        gwRequestBean.setData(trans.getDataAtual());
        gwRequestBean.setHora(trans.getHoraAtual());
        gwRequestBean.setEndpoint("efetuarTransacaoWithCreditCard");
        gwRequestBean.setHost( request.getLocalAddr() );

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put(PayU.PARAMETERS.ACCOUNT_ID, get("account_id"));
        parameters.put(PayU.PARAMETERS.REFERENCE_CODE, ""+reference);
        parameters.put(PayU.PARAMETERS.DESCRIPTION, "Compra");
        parameters.put(PayU.PARAMETERS.LANGUAGE, "Language.pt");
        parameters.put(PayU.PARAMETERS.VALUE, ""+valorInteiro);
        parameters.put(PayU.PARAMETERS.CURRENCY, "BRL");
        parameters.put(PayU.PARAMETERS.BUYER_ID, pesBean.getId()+"");
        parameters.put(PayU.PARAMETERS.BUYER_NAME, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.BUYER_EMAIL, pesBean.getEmail());
        parameters.put(PayU.PARAMETERS.BUYER_CONTACT_PHONE, pesBean.getTelefone1());
        parameters.put(PayU.PARAMETERS.BUYER_DNI, StringUtils.getNumberOnly( pesBean.getCpfCnpj() ));

        //Coloque aqui o endereço do comprador.
//        parameters.put(PayU.PARAMETERS.BUYER_STREET, "calle 100");
//        parameters.put(PayU.PARAMETERS.BUYER_STREET_2, "5555487");
//        parameters.put(PayU.PARAMETERS.BUYER_CITY, "Sao paulo");
//        parameters.put(PayU.PARAMETERS.BUYER_STATE, "SP");
//        parameters.put(PayU.PARAMETERS.BUYER_COUNTRY, "BR");
//        parameters.put(PayU.PARAMETERS.BUYER_POSTAL_CODE, "01019-030");
//        parameters.put(PayU.PARAMETERS.BUYER_PHONE, "(11)756312633");

        // -- Pagador --
        //Coloque aqui o nome do pagador.
        parameters.put(PayU.PARAMETERS.PAYER_NAME, ccBean.nome);

        // -- Dados do cartão de crédito --
        parameters.put(PayU.PARAMETERS.CREDIT_CARD_NUMBER, StringUtils.getNumberOnly(ccBean.numero));
        parameters.put(PayU.PARAMETERS.CREDIT_CARD_EXPIRATION_DATE, "2014/12");
        parameters.put(PayU.PARAMETERS.CREDIT_CARD_SECURITY_CODE, ccBean.cvv+"");
        parameters.put(PayU.PARAMETERS.PAYMENT_METHOD, ccBean.bandeira);
        parameters.put(PayU.PARAMETERS.INSTALLMENTS_NUMBER, "1");
        parameters.put(PayU.PARAMETERS.COUNTRY, PaymentCountry.BR.name());
        parameters.put(PayU.PARAMETERS.IP_ADDRESS, request.getRemoteAddr());

        // "Authorization and capture" request
        TransactionResponse response = PayUPayments.doAuthorizationAndCapture(parameters);

        long end_time = System.currentTimeMillis();
        gwRequestBean.getPessoaCliente().setId(pesBean.getId());
        gwRequestBean.setTime(new Long(end_time - start_time).intValue());
        gwRequestBean.setGateway(BrasPagService.NAME);
        gwRequestBean.setGatewaySucesso(0);

        LOG.debug("Inserindo LOG Gateway...");
        TransactionDB trans2 = TransactionDB.getInstance(null);
        trans2.insert(gwRequestBean);
        trans2.commit();
        trans2.close();
        LOG.debug("LOG Gateway (inserido)");

        LOG.debug( "order_id => "+response.getOrderId() );
        LOG.debug( "transaction_id => "+response.getTransactionId() );
        LOG.debug( "response_code => "+response.getTransactionId() );
        LOG.debug( "response_message => "+response.getTransactionId() );
        LOG.debug( "error_code => "+response.getErrorCode() );
        LOG.debug( "error_message => "+response.getPendingReason() );

        result.set("nsu_gw", response.getTransactionId());

        // resposta
        if (response != null){
            result.set("order_id", response.getOrderId());
            result.set("transaction_id", response.getTransactionId());
            result.set("response_code", response.getResponseCode());
            result.set("response_message", response.getResponseMessage());
            result.set("error_code", response.getErrorCode());
            result.set("error_message", response.getPendingReason());
            if(response.getState().toString().equalsIgnoreCase("PENDING")){
                response.getPendingReason();
            }
        }

        return result;
    }

    public static Bean efetuarTransacao(TransactionDB trans, PessoaBean pesBean, BigDecimal valor, HttpServletRequest request, Integer nsu, CarteiraBean carBean) throws Exception {

        LOG.debug("PayuLatam (efetuarTransacao) starting ");

        BigDecimal novoValor = valor.multiply(new BigDecimal("100"));
        Integer valorInteiro = novoValor.intValue();

        String reference = nsu.toString();
        Bean result = new Bean();

        long start_time = System.currentTimeMillis();
        RequestBean gwRequestBean = new RequestBean();
        gwRequestBean.setData(trans.getDataAtual());
        gwRequestBean.setHora(trans.getHoraAtual());
        gwRequestBean.setEndpoint("efetuarTransacao");
        gwRequestBean.setHost( request.getLocalAddr() );

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put(PayU.PARAMETERS.ACCOUNT_ID, get("account_id"));
        parameters.put(PayU.PARAMETERS.REFERENCE_CODE, ""+reference);
        parameters.put(PayU.PARAMETERS.DESCRIPTION, "Compra");
        parameters.put(PayU.PARAMETERS.LANGUAGE, "Language.pt");
        //parameters.put(PayU.PARAMETERS.VALUE, ""+valorInteiro);
        parameters.put(PayU.PARAMETERS.VALUE, ""+valorInteiro);
        parameters.put(PayU.PARAMETERS.CURRENCY, "BRL");
        parameters.put(PayU.PARAMETERS.BUYER_ID, pesBean.getId()+"");
        parameters.put(PayU.PARAMETERS.BUYER_NAME, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.BUYER_EMAIL, pesBean.getEmail());
        parameters.put(PayU.PARAMETERS.BUYER_CONTACT_PHONE, pesBean.getTelefone1());
        parameters.put(PayU.PARAMETERS.BUYER_DNI, StringUtils.getNumberOnly( pesBean.getCpfCnpj() ));

        //Coloque aqui o endereço do comprador.
        parameters.put(PayU.PARAMETERS.BUYER_STREET, "calle 100");
        parameters.put(PayU.PARAMETERS.BUYER_STREET_2, "5555487");
        parameters.put(PayU.PARAMETERS.BUYER_CITY, "Sao paulo");
        parameters.put(PayU.PARAMETERS.BUYER_STATE, "SP");
        parameters.put(PayU.PARAMETERS.BUYER_COUNTRY, "BR");
        parameters.put(PayU.PARAMETERS.BUYER_POSTAL_CODE, "01019-030");
        parameters.put(PayU.PARAMETERS.BUYER_PHONE, "(11)756312633");

        parameters.put(PayU.PARAMETERS.PAYER_NAME, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.INSTALLMENTS_NUMBER, "1");
        parameters.put(PayU.PARAMETERS.TOKEN_ID, carBean.getTokenPayU());
        parameters.put(PayU.PARAMETERS.IP_ADDRESS, request.getRemoteAddr());
        parameters.put(PayU.PARAMETERS.PAYMENT_METHOD, carBean.getBandeira() );

        // "Authorization and capture" request
        LOG.debug("PayuLatam PayUPayments.doAuthorizationAndCapture... ");

        int http_status = 0;
        boolean venda_realizada = false;
        try {
            TransactionResponse response = PayUPayments.doAuthorizationAndCapture(parameters);

            LOG.debug( "getAuthorizationCode => "+response.getAuthorizationCode() );
            LOG.debug( "getPaymentNetworkResponseCode => "+response.getPaymentNetworkResponseCode() );
            LOG.debug( "order_id => "+response.getOrderId() );
            LOG.debug( "transaction_id => "+response.getTransactionId() );
            LOG.debug( "response_code => "+response.getResponseCode() );
            LOG.debug( "pending_reason => "+response.getPendingReason() );
            LOG.debug( "error_code => "+response.getErrorCode() );
            LOG.debug( "error_message => "+response.getPendingReason() );

            result.set("nsu_gw", response.getTransactionId());

            // resposta
            if (response != null){
                result.set("order_id", response.getOrderId());
                result.set("transaction_id", response.getTransactionId());
                result.set("response_code", response.getResponseCode());
                result.set("response_message", response.getResponseMessage());
                result.set("error_code", response.getErrorCode());
                result.set("error_message", response.getPendingReason());

                // venda só foi realizar quando STATE = APPROVED
                if (response.getState().toString().equalsIgnoreCase("APPROVED")){
                    venda_realizada = true;
                }
            }


        } catch (Exception ex) {
            gwRequestBean.setResponse(ex.getMessage());
            LOG.error(ex.getMessage());
        }
        LOG.debug("PayuLatam PayUPayments.doAuthorizationAndCapture (done) ");

        long end_time = System.currentTimeMillis();
        gwRequestBean.getPessoaCliente().setId(pesBean.getId());
        gwRequestBean.setTime(new Long(end_time - start_time).intValue());
        gwRequestBean.setGateway(BrasPagService.NAME);
        gwRequestBean.setGatewaySucesso(0);
        gwRequestBean.setHttpStatus(http_status);

        LOG.debug("Inserindo LOG Gateway...");
        TransactionDB trans2 = TransactionDB.getInstance(null);
        trans2.insert(gwRequestBean);
        trans2.commit();
        trans2.close();
        LOG.debug("LOG Gateway (inserido)");

        if (venda_realizada == false)
            throw new API400Exception("banco_reprovou", "Banco não autorizou esta transação");

        return result;
    }

    public static Bean exclusaoDeCartao(String serverPath, TransactionDB trans, PessoaBean pesBean, String token, HttpServletRequest request) throws Exception {

        Bean result = new Bean();
        long start_time = System.currentTimeMillis();
        RequestBean gwRequestBean = new RequestBean();
        gwRequestBean.setData(trans.getDataAtual());
        gwRequestBean.setHora(trans.getHoraAtual());
        gwRequestBean.setEndpoint("excluirToken");
        gwRequestBean.setHost( request.getLocalAddr() );

        // -- Operação "Eliminar token" --
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(PayU.PARAMETERS.PAYER_ID, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.TOKEN_ID, token);
        CreditCardToken response = PayUTokens.remove(parameters);
        LoggerUtil.info("{0}", response);

        long end_time = System.currentTimeMillis();
        gwRequestBean.getPessoaCliente().setId(pesBean.getId());
        gwRequestBean.setTime(new Long(end_time - start_time).intValue());
        gwRequestBean.setGateway(BrasPagService.NAME);
        gwRequestBean.setGatewaySucesso(0);

        LOG.debug("Inserindo LOG Gateway...");
        TransactionDB trans2 = TransactionDB.getInstance(null);
        trans2.insert(gwRequestBean);
        trans2.commit();
        trans2.close();
        LOG.debug("LOG Gateway (inserido)");

        return result;
    }

    public static void listarCartoes() throws Exception {

        // -- Operação "Consultar token" --
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(PayU.PARAMETERS.PAYER_ID, "64626");
        parameters.put(PayU.PARAMETERS.TOKEN_ID, "cd9b536d-4952-41ab-bb80-660339b1c080");
        parameters.put(PayU.PARAMETERS.START_DATE, "2010-01-01T12:00:00");
        parameters.put(PayU.PARAMETERS.END_DATE, "2020-01-01T12:00:00");

        List<CreditCardToken> response = null;
        try {
            response = PayUTokens.find(parameters);
        } catch (PayUException e) {
            e.printStackTrace();
        } catch (InvalidParametersException e) {
            e.printStackTrace();
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
        Iterator<CreditCardToken> tokens_iterator = response.iterator();

    }
}