package capital.api;


import br.com.dotum.core.servlet.ServletHelper;
import br.com.dotum.jedi.core.db.ORM;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.db.WhereDB.Condition;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.ErrorException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.*;
import capital.model.dd.EtaDD;
import capital.model.dd.SitDD;
import capital.model.dd.UniDD;
import capital.model.dd.VerDD;
import capital.service.FirebaseService;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import capital.service.MsgService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CapitalService {
	private static Log LOG = LogFactory.getLog(CapitalService.class);

	final static Integer EXP_MINUTES = 60 * 24 * 5;

	public static EtapaBean doDataEtapaVendaAtual(String pathServer, TransactionDB trans) throws SQLException, Exception {
		return doDataEtapaData(pathServer, trans, "eta_datalimitevenda", "eta_horalimitevenda");
	}
	public static EtapaBean doDataEtapaData(String pathServer, TransactionDB trans, String columnData, String columnHora) throws SQLException, Exception {

		StringBuilder sb = new StringBuilder();
		sb.append("         select eta.eta_id,                                                                                                            ");
		sb.append("                eta.uni_id,                                                                                                            ");
		sb.append("                eta.eta_descricao,                                                                                                     ");
		sb.append("                eta.eta_datasorteio,                                                                                                   ");
		sb.append("                eta.eta_horasorteio,                                                                                                   ");
		sb.append("                eta.eta_datalimitevenda,                                                                                               ");
		sb.append("                eta.eta_horalimitevenda,                                                                                               ");
		sb.append("                eta.eta_edicao,                                                                                                        ");
		sb.append("                eta.eta_urlvideo,                                                                                                      ");
		sb.append("                eta.eta_urlimagem,                                                                                                     ");
		sb.append("                eta.eta_numero,                                                                                                        ");
		sb.append("                eta.eta_tipocertificado                                                                                                ");
		sb.append("           from etapa eta                                                                                                              ");
		sb.append("          where to_date(eta."+columnData+" || ' ' || eta."+columnHora+", 'DD/MM/YYYY HH24:MI') = (select min(tb1.dataproxima)              ");
		sb.append("        From                                                                                                                           ");
		sb.append("      (select to_date(eta."+columnData+" || ' ' || eta."+columnHora+", 'DD/MM/YYYY HH24:MI') dataProxima                               ");
		sb.append("        from etapa eta                                                                                                                 ");
		sb.append("        where to_date(eta."+columnData+" || ' ' || eta."+columnHora+", 'DD/MM/RR HH24:MI') >= TO_DATE(TO_CHAR(current_date,'DD/MM/RR HH24:MI'), 'DD/MM/RR HH24:MI')) tb1)                      ");

		ORM orm = new ORM();
		orm.add(Integer.class,                             "Id",                          "eta_id");
		orm.add(Integer.class,                             "unidade.Id",                  "uni_id");
		orm.add(Integer.class,                             "numero",                      "eta_numero");
		orm.add(Date.class,                                "dataSorteio",                 "eta_datasorteio");
		orm.add(String.class,                              "horaSorteio",                 "eta_horasorteio");
		orm.add(String.class,                              "urlVideo",                    "eta_urlvideo");
		orm.add(String.class,                              "urlImagem",                    "eta_urlimagem");
		orm.add(Date.class,                                "dataLimiteVenda",             "eta_datalimitevenda");
		orm.add(String.class,                              "horaLimiteVenda",             "eta_horalimitevenda");
		orm.add(String.class,                              "descricao",                   "eta_descricao");
		orm.add(Integer.class,                             "edicao",                      "eta_edicao");
		orm.add(Integer.class,                             "tipoCertificado",             "eta_tipocertificado");

		EtapaBean etaBean = null;
		try {
			List<EtapaBean> etaList = trans.select(EtapaBean.class, orm, sb.toString());
			etaBean = etaList.get(0);
		} catch (NotFoundException e) {
			throw new WarningException(MsgService.get(pathServer, "msg.capitalservice.doDataEtapaData.01"));
		}

		return etaBean;
	}

	public static List<Bean> doDataCarteira(TransactionDB trans, PessoaBean pesBean) throws SQLException, Exception {
		List<Bean> carList = new ArrayList<Bean>();
		try {
			Integer pesId = pesBean.getId();

			StringBuilder sb = new StringBuilder();
			sb.append("               select car.*,              																		");
			sb.append("                      sit.sit_descricao                                                                          ");
			sb.append("                 from carteira car                                                                               ");
			sb.append("            left join situacao sit on (sit.sit_id = car.sit_id)                                                  ");

			WhereDB where = new WhereDB();
			where.add("Pessoa.Id", Condition.EQUALS, pesId);

			ORM orm = new ORM();
			orm.add(String.class, "NomeApelido", "car_nomeapelido");
			orm.add(Integer.class,"Pessoa.Id", "pes_id");
			orm.add(Integer.class,"Situacao.Id", "sit_id");
			orm.add(String.class, "Situacao.Descricao", "sit_descricao");
			orm.add(String.class, "NumeroCartao", "car_numerocartao");
			orm.add(String.class, "DataValidade", "car_datavalidade");
			orm.add(String.class, "Bandeira",     "car_bandeira");
			orm.add(String.class, "tokenValit",   "car_token");
			orm.add(String.class, "gateway",      "car_gateway");

			List<CarteiraBean> list = trans.select(CarteiraBean.class, orm, sb.toString(), where);

			for (CarteiraBean carBean : list) {
				Bean b = new Bean();
				b.set("nome_cartao",        carBean.get("NomeApelido"));
				b.set("numero_cartao",      carBean.get("NumeroCartao"));
				if(carBean.get("Situacao.Id").equals(SitDD.CONCLUIDO)) {
					b.set("situacao_id",        1);
					b.set("situacao_descricao", "ATIVO");
				}else {
					b.set("situacao_id",        2);
					b.set("situacao_descricao", "INATIVO");
				}
				String dataValidade =  carBean.get("DataValidade");
				String[] dataSplit = dataValidade.split("\\/");
				String mes = dataSplit[0];
				String ano = dataSplit[1];

				// TODO garantir que os anos estao padronizados no banco de dados "DD/YY";
				if(mes.length() < 2) mes = "0" + mes;
				if(ano.length() > 2) ano = ano.substring(2, 4);

				dataValidade = mes + "/" + ano;
				b.set("bandeira_cartao",    carBean.get("Bandeira"));
				b.set("data_validade",      dataValidade );
				b.set("token_valit",        carBean.get("tokenValit"));
				b.set("gateway",            carBean.get("gateway"));
				carList.add(b);
			}
		} catch (NotFoundException e) {
		}

		return carList;
	}

	public static List<Bean> doDataVendaPorPeriodo(String pathServer, TransactionDB trans, PessoaBean pesBean, Integer etapaId, Date inicio, Date fim) throws SQLException, Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("          select ven.ven_id,                                                                      ");
		sb.append("                 ven.pes_cliente_id,                                                              ");
		sb.append("                 ven.ven_data,                                                                    ");
		sb.append("                 ven.ven_valor,                                                                   ");
		sb.append("                 ven.sit_id,                                                                      ");
		sb.append("                 cer.cer_id,                                                                      ");
		sb.append("                 cer.cer_numero1,                                                                  ");
		sb.append("                 cer.cer_numero2,                                                                  ");
		sb.append("                 cer.cer_numero3,                                                                  ");
		sb.append("                 cer.cer_sequencia1,                                                              ");
		sb.append("                 cer.cer_sequencia2,                                                              ");
		sb.append("                 cer.cer_sequencia3,                                                              ");
		sb.append("                 eta.eta_id,                                                                      ");
		sb.append("                 eta.eta_tipocertificado,                                                                      ");
		sb.append("                 eta.eta_descricao,                                                               ");
		sb.append("                 eta.eta_edicao                                                                   ");
		sb.append("            from venda ven                                                                        ");
		sb.append("            join certificado cer on (ven.cer_id = cer.cer_id)                                     ");
		sb.append("            join etapa eta on (cer.eta_id = eta.eta_id)                                           ");
		sb.append("                                                                                                  ");
		sb.append("                                                                                                  ");

		ORM orm = new ORM();
		orm.add(Integer.class,                 "venda.id",                                            "ven_id");
		orm.add(Integer.class,                 "etapa.id",                                            "eta_id");
		orm.add(Integer.class,                 "tipoCertificado",                                     "eta_tipocertificado");
		orm.add(Integer.class,                 "etapa.edicao",                                        "eta_edicao");
		orm.add(Integer.class,                 "cliente.id",                                          "pes_cliente_id");
		orm.add(Integer.class,                 "situacao.id",                                         "sit_id");
		orm.add(Integer.class,                 "certificado.id",                                      "cer_id");
		orm.add(Date.class,                    "data",                                                "ven_data");
		orm.add(BigDecimal.class,              "valor",                                               "ven_valor");
		orm.add(Integer.class,                 "numero1",                                              "cer_numero1");
		orm.add(Integer.class,                 "numero2",                                              "cer_numero2");
		orm.add(Integer.class,                 "numero3",                                              "cer_numero3");
		orm.add(Integer.class,                 "sequencia1",                                          "cer_sequencia1");
		orm.add(Integer.class,                 "sequencia2",                                          "cer_sequencia2");
		orm.add(Integer.class,                 "sequencia3",                                          "cer_sequencia3");
		orm.add(String.class,                  "etapa.descricao",                                     "eta_descricao");

		if ((etapaId == null) && (inicio == null) && (fim == null)) {
			EtapaBean etaBean = CapitalService.doDataEtapaVendaAtual(pathServer, trans);
			etapaId = etaBean.getId();
		}


		WhereDB where = new WhereDB();
		where.add("cliente.id", Condition.EQUALS, pesBean.getId());
		where.add("Situacao.id", Condition.EQUALS, SitDD.CONCLUIDO);

		if(etapaId != null)
			where.add("etapa.id", Condition.EQUALS,etapaId);
		if(inicio != null)
			where.add("data", Condition.GREATOREQUALS,  inicio);
		if(fim != null)
			where.add("data", Condition.LESSOREQUALS, fim);

		List<Bean> beanList = null;
		try {
			beanList = trans.execQuery(Bean.class, orm, sb.toString(), where);
		} catch (NotFoundException e) {
			beanList = new ArrayList<Bean>();
		}

		return beanList;
	}

	public static String generateTokenOnLogin(String pathServer, TransactionDB trans, String user, String pass) throws NotFoundException, Exception {
		WhereDB where = new WhereDB();
		where.add("CpfCnpj", Condition.EQUALS, user);
		where.add("Senha", Condition.EQUALS, pass);
		where.add("Ativo", Condition.EQUALS, 1);

		PessoaBean pesBean = null;

		try {
			List<PessoaBean> list = trans.select(PessoaBean.class, where);
			pesBean = list.get(0);
		} catch (NotFoundException e) {
			throw new WarningException(MsgService.get(pathServer, "msg.capitalservice.generateTokenOnLogin.01"));
		}

		Bean b = new Bean();
		b.set("exp", org.apache.commons.lang.time.DateUtils.addMinutes(new Date(), EXP_MINUTES).getTime());
		b.set("iat", new Date().getTime());
		b.set("pes_id", pesBean.getId());
		b.set("uni_id", pesBean.getUnidade().getId());
		b.set("pes_nome", StringUtils.getFirstWord(pesBean.getNomeRazao()));

		return Jwt.generate(b);
	}

	public static PessoaBean getPessoaBytoken(HttpServletResponse res, String pathServer, TransactionDB trans, String tokenAuth) throws Exception {
		Bean b = null;
		PessoaBean pesBean = null;
		try {
			b = Jwt.token2bean(tokenAuth);
		} catch (Exception e) {
			res.setStatus(401);
			throw new WarningException(e.getMessage());
		}
		try {
			pesBean = trans.selectById(PessoaBean.class, (Integer)b.get("pes_id"));
		} catch (NotFoundException e) {
			LOG.error(e.getMessage());
			throw new WarningException(MsgService.get(pathServer, "msg.capitalservice.getPessoaBytoken.01"));
		}

		return pesBean;
	}

	public static String reservaVenda(String pathServer, TransactionDB trans, Integer[] certificadoArray, Integer etapaId,
									  Integer pesId, Integer uniId, Date dataAtual,
									  Integer situacao, String canal) throws NotFoundException, Exception {

		WhereDB where = new WhereDB();
		where.add("Numero1", Condition.IN, certificadoArray);
		where.add("Etapa.id", Condition.EQUALS, etapaId);
		List<CertificadoBean> cerList = trans.select(CertificadoBean.class, where);

		List<VendaBean> venFinalList = new ArrayList<VendaBean>();

		String transId = null;
		for (CertificadoBean cerBean : cerList) {
			BigDecimal cerValor = cerBean.getValor();
			Integer etapaAtual = cerBean.getEtapa().getId();

			VendaBean venBean = null;
			try {
				where = new WhereDB();
				where.add("Certificado.Id", Condition.EQUALS, cerBean.getId());
				// Se achar algum resultado ele pula e NÃO insere como uma nova venda
				List<VendaBean> venList = trans.select(VendaBean.class, where);
				venBean = venList.get(0);
				transId = venBean.getTransacao();
				if (venBean.getSituacao().getId().equals(SitDD.CONCLUIDO)) {

					throw new ErrorException(MsgService.get(pathServer, "msg.capitalservice.reservaVenda.01"));

				} else if (venBean.getPessoaCliente().getId().equals(pesId) == false) {

					throw new ErrorException(MsgService.get(pathServer, "msg.capitalservice.reservaVenda.01"));

				}

			} catch (NotFoundException e) {

				if (transId == null) {
					transId = trans.getId();
				}

				// Adiciona uma como uma nova venda
				venBean = new VendaBean();
				venBean.setTransacao(transId);
				venBean.getSituacao().setId(situacao);
				venBean.getEtapa().setId(etapaAtual);
				venBean.getPessoaCliente().setId(pesId);
				venBean.getCertificado().setId(cerBean.getId());
				venBean.getUnidade().setId(uniId);
				venBean.setData(dataAtual);
				venBean.setHora(DateUtils.formatHour(dataAtual));
				venBean.setValor(cerValor);

				if (StringUtils.hasValue(canal) == false)
					venBean.setCanalTag("Mobile");
				else
					venBean.setCanalTag(canal);

				venBean.setPinbankNsu("");
				venBean.setNsu(CapitalService.getNsu(trans));

				venFinalList.add(venBean);
			}
		}

		for (VendaBean venBean : venFinalList) {
			trans.merge(venBean);
		}

		// A partir daqui pode acorrer um erro na compra mas a compra ainda deve ficar registrada como pendente
		trans.commit();

		return transId;
	}

	public static CarteiraBean getWalletByToken(String pathServer, TransactionDB trans, PessoaBean pesBean, String tokenValit) throws Exception {
		CarteiraBean carBean = new CarteiraBean();

		WhereDB where = new WhereDB();
		where.add("Pessoa.Id", Condition.EQUALS, pesBean.getId());
		where.add("token", Condition.EQUALS, tokenValit);
		try {
			carBean = ((List<CarteiraBean>) trans.select(CarteiraBean.class, where)).get(0);
		} catch (NotFoundException e) {
			throw new WarningException(MsgService.get(pathServer, "msg.capitalservice.getWalletByToken.01", tokenValit));
		}

		return carBean;
	}

	public static String esconderNumerosCartao(String numero) {
		String result = null;
		try {
			String comeco = numero.substring(0, 4);
			String fim = numero.substring(12, numero.length());
			result = comeco + "********" + fim;
		} catch (Exception e) {
			result = numero;
		}
		return result;
	}

	public static final String SEPARADOR_RG = " ";

	public static Bean sendResponse(HttpServletResponse res, String pathServer, TransactionDB trans, Bean b, String oldToken) {
		try {
			b.set("auth_token", newTokenSession(res, pathServer, trans, oldToken));
			b.set("status", "success");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			b = new Bean();
			b.set("auth_token", oldToken);
			b.set("status", "warning");
			b.set("message", "Erro ao revalidar token, varifique a expiração do token.");
		}
		return b;
	}

	public static String newTokenSession(HttpServletResponse res, String pathServer, TransactionDB trans, String oldToken) throws Exception {
		PessoaBean pesBean = CapitalService.getPessoaBytoken(res, pathServer, trans, oldToken);

		Bean b = new Bean();
		b.set("exp", org.apache.commons.lang.time.DateUtils.addMinutes(new Date(), EXP_MINUTES).getTime());
		b.set("iat", new Date().getTime());
		b.set("pes_id", pesBean.getId());
		b.set("uni_id", pesBean.getUnidade().getId());
		b.set("pes_nome", StringUtils.getFirstWord(pesBean.getNomeRazao()));
		return Jwt.generate(b);
	}

	public static Integer getNsu(TransactionDB trans) throws SQLException, Exception {

		String sql = "select sven_nsu.nextval nsu from dual";
		ORM orm = new ORM();
		orm.add(Integer.class, "nsu","nsu");
		List<Bean> list = trans.execQuery(Bean.class, orm, sql);
		Bean b = list.get(0);
		return b.get("nsu");
	}

	public static int deleteVendasByNSU(TransactionDB trans, PessoaBean pesBean, Integer nsu) throws SQLException, Exception {
		String sql = "delete from venda where pes_cliente_id = "+pesBean.getId()+" ven_nsu = "+nsu;
		return trans.execUpdate(sql);
	}

	public static void syncCartoes(TransactionDB trans, PessoaBean pesBean, HttpServletRequest request) throws Exception {
		JSONArray cartoes = null;
		try {
			Bean list = PinbankService.consultaDeCartoes(trans, pesBean, pesBean.getPinbankId(), null, request);
			cartoes = list.get("cartoes");
		} catch (Exception e) {
			return;
		}
		if(cartoes == null) return;


		List<CarteiraBean> carList = null;
		try {
			WhereDB where = new WhereDB();
			where.add("Pessoa.Id", Condition.EQUALS, pesBean.getId());
			carList = trans.select(CarteiraBean.class, where);
		} catch (NotFoundException e) {
			carList = new ArrayList<CarteiraBean>(); 
		}


		a:
			for (Object json : cartoes) {
				Bean b = new Bean();
				b.toObject(json.toString());
				String dataValidade = Integer.toString((Integer) b.get("dataValidade"));
				String ano = dataValidade.substring(0, 4);
				String mes = dataValidade.substring(4, 6);
				dataValidade = mes + "/" + ano;


				for (CarteiraBean carTempBean : carList) {
					if (carTempBean.getToken().equals(b.get("idCartao"))) {

						continue a;
					}

				}


				CarteiraBean carBean = new CarteiraBean();
				carBean.setPessoa(pesBean);
				carBean.setUnidade(pesBean.getUnidade());
				carBean.setBandeira((String) b.get("descricaoBandeira"));
				//carBean.setNomeApelido((String) b.get("apelidoCartao"));
				carBean.setToken((String) b.get("idCartao"));
				//carBean.setGateway("PINBANK");
				carBean.setNumeroCartao((String) b.get("numeroCartaoTruncado"));
				carBean.setData(trans.getDataAtual());
				carBean.setHora(trans.getHoraAtual());
				//carBean.setDataValidade(dataValidade);

				if(Integer.valueOf((String) b.get("codigoStatusCartao")) == 2) {
					carBean.getSituacao().setId(SitDD.PENDENTE);;
				}else if(Integer.valueOf((String) b.get("codigoStatusCartao")) == 1) {
					carBean.getSituacao().setId(SitDD.CONCLUIDO);
				}

				trans.insert(carBean);
			}

	}

	public static CarteiraBean getCartaoCreditoDaCarteira(TransactionDB trans, PessoaBean pesBean, String token) throws Exception {
		WhereDB where = new WhereDB();
		where.add("Pessoa.Id", Condition.EQUALS, pesBean.getId());
		where.add("Token", Condition.EQUALS, token);
		List<CarteiraBean> list = trans.select(CarteiraBean.class, where);
		return list.get(0);
	}

	public static CarteiraBean adicionarCartaoNaCarteira(TransactionDB trans, HttpServletRequest request, PessoaBean pesBean, String pathServer, CreditCardBean ccBean) throws Exception {

		// gerar um token interno
		MessageDigest salt = MessageDigest.getInstance("SHA-256");
		salt.update(UUID.randomUUID().toString().getBytes("UTF-8"));
		String token_wallet = bytesToHex(salt.digest());

		String ano = ccBean.exp_year;
		if (ano.length() == 2) ano = "20" + ano;
		String mes = ccBean.exp_month;
		if (mes.length() == 1) mes = "0" + mes;

		// inserir o cartão na carteira
		CarteiraBean carBean = new CarteiraBean();
		carBean.setPessoa(pesBean);
		carBean.getUnidade().setId(1);
		carBean.setNomeApelido( "Cartão do "+ccBean.nome+"/"+trans.getId());
		carBean.setNumeroCartao(CapitalService.esconderNumerosCartao(ccBean.numero));
		carBean.setBandeira(ccBean.bandeira);
		carBean.setData(trans.getDataAtual());
		carBean.getSituacao().setId(SitDD.CONCLUIDO);
		carBean.setToken(token_wallet);
		carBean.setDataValidade(mes+"/"+ano);
		carBean.setGateway("VARIOS");

		Integer pinbank_priority_int = 1;
		Integer payulatam_priority_int = 0;
		Integer braspag_priority_int = 0;
		try {
			// pegar as prioridades dos gateways
			//String pinbank_priority = (String) FirebaseService.getProperty("gateway_pinbank_priority");
			//pinbank_priority_int = Integer.parseInt(pinbank_priority);
			//String payulatam_priority = (String) FirebaseService.getProperty("gateway_payulatam_priority");
			//payulatam_priority_int = Integer.parseInt(payulatam_priority);
			//String braspag_priority = (String) FirebaseService.getProperty("gateway_braspag_priority");
			//braspag_priority_int = Integer.parseInt(braspag_priority);
		} catch (Exception ex) {
			LOG.error("Não foi possível encontrar prioridade dentro das configurações; erro crítico");
			throw new DeveloperException("Não foi possível encontrar prioridade dentro das configurações; Erro crítico");
		}

		// pinbank
		if (pinbank_priority_int > 0) {
			// Se não tem pinbankid cadastra o cliente lá
			// a pinbank exige um cadastro de cliente no banco para liberar o token do cartão de crédito
			if (pesBean.getPinbankId() == null) {
				WhereDB where = new WhereDB();
				where.add("Pessoa.Id", WhereDB.Condition.EQUALS, pesBean.getId());
				PessoaEnderecoBean peseBean = ((List<PessoaEnderecoBean>) trans.select(PessoaEnderecoBean.class, where)).get(0);
				CidadeBean cidBean = peseBean.getCidade();
				EstadoBean estBean = trans.selectById(EstadoBean.class, cidBean.getEstado().getId());

				String telefoneFull = StringUtils.getNumberOnly(pesBean.getTelefone1());
				String pesDDD = telefoneFull.substring(0, 2);
				String pesTelefone = telefoneFull.substring(2, telefoneFull.length());
				String sexo = pesBean.getSexo();

				if (StringUtils.hasValue(sexo) == false) {
					String nome = StringUtils.getFirstWord(pesBean.getNomeRazao());
					if (nome.substring((nome.length() - 1)).equalsIgnoreCase("A")) {
						sexo = "F";
					} else {
						sexo = "M";
					}
				}

				// alguns dados que não pedimos mas a pinbank exige
				String rg = "" + NumberUtils.random(7);
				String emissorRg = "SSP";
				String bairro = "Centro";
				Date dataEmissaoRg = DateUtils.addYear(new Date(), -17);
				pesBean.setNascimentoFundacao(DateUtils.addYear(new Date(), -28));
				Bean res = PinbankService.inclusaoDeCliente(trans, pesBean, ccBean.nome, pesBean.getNascimentoFundacao(), sexo, pesBean.getCpfCnpj(), rg, emissorRg, dataEmissaoRg, pesBean.getEmail(), pesDDD, pesTelefone, peseBean.getCep(), estBean.getSigla(), cidBean.getDescricao(), bairro, peseBean.getLogradouro(), peseBean.getNumero(), request);
				Integer clientePinBankId = res.get("mml");
				pesBean.setPinbankId(clientePinBankId);

				//
				// É preciso desse commit pq o cliente pode ou nao
				// ja esta cadastrado para seguir em exceções para frente.
				//
				trans.update(pesBean);
				trans.commit();
			}

			// pinbank - gerar token

			Integer codigoCliente = pesBean.getPinbankId(); // esse é o id que retorno do método addCliente
			String nomeImpresso = pesBean.getNomeRazao();
			String cpfCnpj = pesBean.getCpfCnpj();
			cpfCnpj = StringUtils.getNumberOnly(cpfCnpj);
			String expMonthStr = StringUtils.fillLeft(ccBean.exp_month, '0', 2);

			Bean cartao = PinbankService.inclusaoDeCartao(pathServer, trans, pesBean, codigoCliente, ccBean.numero, nomeImpresso, ccBean.nome, expMonthStr, ccBean.exp_year, ccBean.cvv, request);
			String idCartao = cartao.get("idCartao");

			Bean car = new Bean();
			car.set("token", idCartao);
			car.set("numeroCartao", CapitalService.esconderNumerosCartao(ccBean.numero));
			car.set("bandeiraCartao", ccBean.bandeira);
			car.set("validade", expMonthStr + "/" + ccBean.exp_year);
			car.set("situacao.id", SitDD.CONCLUIDO);
			car.set("gateway", PinbankService.NAME);
			carBean.setTokenPinbank(idCartao);

		}

		// agora vou cadastrar na payu
		if (payulatam_priority_int > 0) {
			Bean cartao = PayuService.inclusaoDeCartao(pathServer, trans, pesBean, request, ccBean);
			String idCartao = cartao.get("idCartao");
			carBean.setTokenPayU(idCartao);
		}

		// agora vou cadastrar na braspag
		if (braspag_priority_int > 0) {
			Bean cartao = BrasPagService.inclusaoDeCartao(pathServer, trans, pesBean, request, ccBean);
			String idCartao = cartao.get("idCartao");
			carBean.setTokenBrasPag(idCartao);
		}
		trans.insert(carBean);

		return carBean;
	}

	public static void excluirCartaoDaCarteira(TransactionDB trans, String pathServer, HttpServletRequest request, PessoaBean pesBean, String token_valit) throws Exception {
		CarteiraBean carBean = CapitalService.getWalletByToken(pathServer, trans, pesBean, token_valit);

		// apagar os tokens antigos
		//if (carBean.getGateway().equalsIgnoreCase("pinbank")) {

			if (carBean.getGateway().equalsIgnoreCase("VARIOS") ) {
				PinbankService.exclusaoDeCartao(pathServer, trans, pesBean, pesBean.getPinbankId(), carBean.getTokenPinbank(), request);
			} else {
				PinbankService.exclusaoDeCartao(pathServer, trans, pesBean, pesBean.getPinbankId(), carBean.getToken(), request);
			}
		//}

		// se der certo ate aqui update do banco tbm
		trans.delete(carBean);
		trans.commit();
	}

	public static void cepLimiteCampra(String pathServer, TransactionDB trans, PessoaBean pesBean) throws Exception {
		UnidadeBean uniBean = pesBean.getUnidade();
		PessoaEnderecoBean peseBean = null;
		try {
			WhereDB where = new WhereDB();
			where.add("Pessoa.Id", Condition.EQUALS, pesBean.getId());
			peseBean = ((List<PessoaEnderecoBean>)trans.select(PessoaEnderecoBean.class, where)).get(0);

		} catch (NotFoundException e) {
			throw new WarningException(MsgService.get(pathServer, "msg.capitalservice.cepLimiteCampra.01"));
		}

		Boolean isValid = false;
		try {
			List<Bean> listCepfaixa = doDataCepFaixa(uniBean);
			for (Bean bean : listCepfaixa) {
				Integer f1 = null;
				Integer f2 = null;
				Object cep1 = bean.get("cep_inicial");
				Object cep2 = bean.get("cep_final");

				if(cep1.getClass().equals(String.class)) {
					f1 = Integer.valueOf((String) cep1);
				} else {
					f1 = (Integer) cep1;
				}
				if(cep2.getClass().equals(String.class)) {
					f2 = Integer.valueOf((String) cep2);
				} else {
					f2 = (Integer) cep2;
				}

				Integer pesCep = Integer.valueOf(peseBean.getCep().substring(0,cep1.toString().length()));

				// Se não estiver na faixa continua o loop
				if((pesCep < f1) || (pesCep > f2)) continue;

				isValid = true;
				break;
			}
		} catch (Exception e) {
			isValid = true;
			LOG.error(e.getMessage());
		}

		if(isValid == false) {
			throw new WarningException(MsgService.get(pathServer, "msg.capitalservice.cepLimiteCampra.02"));
		}

	}

	private static List<Bean> doDataCepFaixa(UnidadeBean uniBean) {
		List<Bean> list = new ArrayList<Bean>();
		JSONArray listJson = null;
		try {
			listJson = new JSONArray(uniBean.getCepLimite());
		} catch (Exception e) {
			return null;
		}

		for (Object json : listJson) {
			Bean b = new Bean();
			try {
				b.toObject(json.toString());
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			list.add(b);
		}

		return list;
	}

	public static EtapaBean doDataEtapaSorteioAtual(String pathServer, TransactionDB trans) throws SQLException, Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("         select eta.eta_id,                                                                                                            ");
		sb.append("                eta.uni_id,                                                                                                            ");
		sb.append("                eta.eta_descricao,                                                                                                     ");
		sb.append("                eta.eta_datasorteio,                                                                                                   ");
		sb.append("                eta.eta_horasorteio,                                                                                                   ");
		sb.append("                eta.eta_datalimitevenda,                                                                                               ");
		sb.append("                eta.eta_horalimitevenda,                                                                                               ");
		sb.append("                eta.eta_edicao,                                                                                                        ");
		sb.append("                eta.eta_urlvideo,                                                                                                      ");
		sb.append("                eta.eta_numero,                                                                                                        ");
		sb.append("                eta.eta_tipocertificado                                                                                                ");
		sb.append("           from etapa eta                                                                                                              ");
		sb.append("          where eta.eta_datasorteio = (                                                                                                ");
		sb.append("                                 select min(eta_datasorteio)                                                                           ");
		sb.append("                                   from etapa xx                                                                                       ");
		sb.append("                                   join etapapremio etap on (etap.eta_id = xx.eta_id)                                                  ");
		sb.append("                                   where to_date(eta_datasorteio, 'DD/MM/RR') >= to_date(current_date, 'DD/MM/RR')                     ");
		sb.append("                                   and etap.sit_id != " + SitDD.CONCLUIDO + "                                                          ");
		sb.append("                                   )                                                                                                   ");

		ORM orm = new ORM();
		orm.add(Integer.class,                             "Id",                          "eta_id");
		orm.add(Integer.class,                             "unidade.Id",                  "uni_id");
		orm.add(Integer.class,                             "numero",                      "eta_numero");
		orm.add(Date.class,                                "dataSorteio",                 "eta_datasorteio");
		orm.add(String.class,                              "horaSorteio",                 "eta_horasorteio");
		orm.add(String.class,                              "urlVideo",                    "eta_urlvideo");
		orm.add(Date.class,                                "dataLimiteVenda",             "eta_datalimitevenda");
		orm.add(String.class,                              "horaLimiteVenda",             "eta_horalimitevenda");
		orm.add(String.class,                              "descricao",                   "eta_descricao");
		orm.add(Integer.class,                             "edicao",                      "eta_edicao");
		orm.add(Integer.class,                             "tipoCertificado",             "eta_tipocertificado");

		EtapaBean etaBean = null;
		try {
			List<Bean> etaList = trans.select(Bean.class, orm, sb.toString());
			Bean bean = etaList.get(0); 
			Integer id = bean.get("Id");
			etaBean = trans.selectById(EtapaBean.class, id);
		} catch (NotFoundException e) {
			//			e.printStackTrace();
			throw new WarningException(MsgService.get(pathServer, "msg.capitalservice.doDataSorteioData.01"));
		}

		return etaBean;
	}

	public static Integer doDataNumComprados(TransactionDB trans, PessoaBean pesBean, Integer etapaAtualId) throws SQLException, Exception {
		Integer numComprado = 0;
		try {

			StringBuilder sb = new StringBuilder();
			sb.append("       select count(1) numComprado                            ");
			sb.append("         From venda                                           ");
			sb.append("         where sit_id = "+ SitDD.CONCLUIDO +"                 ");
			sb.append("           and eta_id = "+ etapaAtualId +"                    ");
			sb.append("           and pes_cliente_id = "+ pesBean.getId() +"         ");
			sb.append("                                                              ");

			ORM orm = new ORM();
			orm.add(Integer.class, "numComprado", "numComprado");

			List<Bean> bList = trans.select(Bean.class, orm, sb.toString());
			numComprado = bList.get(0).get("numComprado");

		} catch (NotFoundException e) {

		}
		return numComprado;
	}

	public static Integer doDataNumImagensPorEtapa(TransactionDB trans, Integer id) throws SQLException, Exception {
		// 
		StringBuilder sb = new StringBuilder();
		sb.append("        select count(1) as numImg    ");
		sb.append("          from etapaimagem           ");
		sb.append("         where eta_id = " + id        );

		ORM orm = new ORM();
		orm.add(Integer.class, "numImg", "numImg");

		List<Bean> list = trans.select(Bean.class, orm, sb.toString());

		return list.get(0).get("numImg");
	}

	public static boolean validarJson(String cepLimite) {

		try {
			JSONArray arr = new JSONArray(cepLimite);
			for (Object json : arr) {
				JSONObject j = (JSONObject) json;
			}
		} catch (JSONException e) {
			return false;
		}
		return true;
	}

	public static Boolean validVersaoApp(TransactionDB trans, String versaoApp) throws NotFoundException, Exception {
		Boolean b = false;
		VersaoBean verBean = null;
		if(versaoApp == null) return true;
		verBean = trans.selectById(VersaoBean.class, VerDD.PRINCIPAL);

		String v1 = StringUtils.getNumberOnly(verBean.getVersaoApp());
		if (v1.length() < 3) v1 = v1 + "0";

		String v2 = StringUtils.getNumberOnly(versaoApp);
		if (v2.length() < 3) v2 = v2 + "0";

		Integer verAppSys = new Integer(v1);
		Integer verAppCell = new Integer(v2);

		int res = verAppCell.compareTo(verAppSys);
		if(res == -1) {
			// versao anterior
			return true;
		}
		return b;
	}

	public static List<EtapaBean> doDataEtapaCompra(TransactionDB trans, Integer pesId) throws SQLException, Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("         select eta.eta_id,                                                                                       ");
		sb.append("                eta.eta_descricao,                                                                                ");
		sb.append("                eta.eta_tipocertificado,                                                                          ");
		sb.append("                eta.eta_datasorteio,                                                                               ");
		sb.append("                eta.eta_edicao,                                                                                    ");
		sb.append("                eta.eta_urlimagem                                                                                  ");
		sb.append("           from venda  ven                                                                                        ");
		sb.append("           join etapa eta on (eta.eta_id = ven.eta_id)                                                            ");
		sb.append("          where ven.pes_cliente_id = "+ pesId +"                                                                  ");
		sb.append("            and ven.sit_id= "+ SitDD.CONCLUIDO+"                                                                  ");
		sb.append("       group by eta.eta_id,                                                                                       ");
		sb.append("                eta.eta_descricao,                                                                                ");
		sb.append("                eta.eta_tipocertificado,                                                                          ");
		sb.append("                eta.eta_datasorteio,                                                                              ");
		sb.append("                eta.eta_edicao,                                                                                   ");
		sb.append("                eta.eta_urlimagem                                                                                 ");
		sb.append("       order by eta.eta_edicao desc                                                                               ");


		ORM orm = new ORM();
		orm.add(Integer.class,                           "Id",                                                 "eta_id");
		orm.add(String.class,                            "Descricao",                                          "eta_descricao");
		orm.add(Date.class,                              "DataSorteio",                                        "eta_datasorteio");
		orm.add(Integer.class,                           "TipoCertificado",                                    "eta_tipocertificado");
		orm.add(String.class,                           "UrlImagem",                                    		  "eta_urlimagem");


		return trans.select(EtapaBean.class, orm, sb.toString());
	}

	public static Bean doDataGanhadorByPremio(TransactionDB trans, Integer etapId, Integer pesId) throws SQLException, Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("           select pes.pes_id,                                                                                             ");
		sb.append("                  ven.ven_id,                                                                                             ");
		sb.append("                  cer.cer_id,                                                                                             ");
		sb.append("                  pes.pes_nomerazao,                                                                                      ");
		sb.append("                  cid.cid_descricao,                                                                                      ");
		sb.append("                  est.est_sigla,                                                                                          ");
		sb.append("                  cer.cer_numero1,                                                                                        ");
		sb.append("                  cer.cer_numero2,                                                                                        ");
		sb.append("                  cer.cer_numero3                                                                                         ");
		sb.append("             from venda ven                                                                                               ");
		sb.append("             join certificado cer on (cer.cer_id = ven.cer_id)                                                            ");
		sb.append("             join pessoa pes on (pes.pes_id = ven.pes_cliente_id)                                                         ");
		sb.append("             join pessoaendereco pese on (pese.pes_id = pes.pes_id)                                                       ");
		sb.append("             join cidade cid on (cid.cid_id = pese.cid_id)                                                                ");
		sb.append("             join estado est on (est.est_id = cid.est_id)                                                                 ");
		sb.append("            where ven.etap_id = " + etapId  + "                                                                           ");
		sb.append("              and ven.pes_cliente_id = " + pesId  + "                                                                     ");

		ORM orm = new ORM();
		orm.add(String.class,                  "nome"                                    ,"pes_nomerazao");
		orm.add(String.class,                  "cidade"                                  ,"cid_descricao");
		orm.add(String.class,                  "estado"                                  ,"est_sigla");
		orm.add(Integer.class,                 "certificadonumero"                       ,"cer_numero1");
		orm.add(Integer.class,                 "certificadonumero2"                      ,"cer_numero2");
		orm.add(Integer.class,                 "certificadonumero3"                      ,"cer_numero3");

		List<Bean> list = trans.select(Bean.class, orm, sb.toString());
		return list.get(0);
	}

	public static Date getDataRange(int start, int end) {
		int idade = -1;
		while ((idade < start) || (idade > end)) {
			idade = NumberUtils.random(2);
		}
		Date nasci = DateUtils.addYear(new Date(), (idade*-1));
		return nasci;
	}

	public static PessoaEnderecoBean getEnderecoPessoa(TransactionDB trans, Integer pessoaId) throws Exception {
		PessoaEnderecoBean peseBean = null;

		// Endereço
		try {
			WhereDB where = new WhereDB();
			where.add("Pessoa.Id", Condition.EQUALS, pessoaId);
			List<PessoaEnderecoBean> peseList = trans.select(PessoaEnderecoBean.class, where);
			peseBean = peseList.get(0);

			CidadeBean cidBean = peseBean.getCidade();
			EstadoBean estBean = trans.selectById(EstadoBean.class, cidBean.getEstado().getId());

			peseBean.getCidade().setEstado(estBean);
		} catch (NotFoundException e) {
			peseBean = new PessoaEnderecoBean();
		}

		return peseBean;
	}

	/**
	 * Metodo que valida se tem o CPF com outra pessoa
	 * @param serverPath
	 * @param trans
	 * @param cpf
	 * @throws Exception
	 */
	public static void validaPessoaCpfCnpjUnico(String serverPath, TransactionDB trans, Integer pessoaId, String cpf) throws Exception {
		try {
			// Cpf Unico
			WhereDB where = new WhereDB();
			where.add("CpfCnpj", Condition.EQUALS, StringUtils.getNumberOnly(cpf));
			if (pessoaId != null) where.add("Id", Condition.NOTEQUALS, pessoaId);
			trans.select(PessoaBean.class, where);
			throw new WarningException(MsgService.get(serverPath, "msg.005.cadastrooficial.01"));
		} catch (NotFoundException e) {
		}	

	}
	/**
	 * Metodo que valida se tem o EMAIL com outra pessoa
	 * @param serverPath
	 * @param trans
	 * @param email
	 * @throws Exception
	 */
	public static void validaPessoaEmailUnico(String serverPath, TransactionDB trans, Integer pessoaId, String email) throws Exception {
		try {
			WhereDB where = new WhereDB();
			where.add("Email", Condition.EQUALS, email);
			if (pessoaId != null) where.add("Id", Condition.NOTEQUALS, pessoaId);
			trans.select(PessoaBean.class, where);
			throw new WarningException(MsgService.get(serverPath, "msg.005.cadastrooficial.02"));
		} catch (NotFoundException e) {
		}	
	}

	/**
	 * Metodo que valida se tem o TELEFONE com outra pessoa
	 * @param serverPath
	 * @param trans
	 * @param telefone
	 * @throws Exception
	 */
	public static void validaPessoaTelefoneUnico(String serverPath, TransactionDB trans, Integer pessoaId, String telefone) throws Exception {
		try {
			WhereDB where = new WhereDB();
			where.add("Telefone1", Condition.EQUALS, StringUtils.getNumberOnly(telefone));
			if (pessoaId != null) where.add("Id", Condition.NOTEQUALS, pessoaId);
			trans.select(PessoaBean.class, where);
			throw new WarningException(MsgService.get(serverPath, "msg.005.cadastrooficial.03"));
		} catch (NotFoundException e) {
		}

	}

	public static Date getDataAtual() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		//Here you say to java the initial timezone. This is the secret
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		//Will print in UTC
		//System.out.println(sdf.format(calendar.getTime()));

		//Here you set to your timezone
		sdf.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
		//Will print on your default Timezone
		//System.out.println(sdf.format(calendar.getTime()));

		return calendar.getTime();
	}

	public static void debug( HttpServletRequest request ) throws IOException {
		StringBuffer result = new StringBuffer();
		result.append( DateUtils.formatDate( new Date(), "dd/MM/yyyy HH:mm:ss") );

		Map<String, Object> map = new HashMap<String, Object>();
		result.append( ServletHelper.showAllParameter(request, map) );

		Enumeration<String> xxx = request.getParameterNames();
		while (xxx.hasMoreElements()) {
			String temp = xxx.nextElement();
			result.append( temp );
		}

		result.append( "/"+ request.getPathInfo() );

		result.append( "\nFORM-DATA>" );
		try {
			ServletFileUpload upload = new ServletFileUpload();
			FileItemIterator iter = upload.getItemIterator(request);

			while (iter.hasNext()) {
				FileItemStream item = iter.next();
				result.append( item.getFieldName() + " =>" );

				if (item.isFormField()) {
					result.append( Streams.asString(item.openStream()) );
				}
			}
		} catch (Exception ex) {}

		result.append( "\nPARAMS>" );

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			result.append( paramName + " => ");
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) {
				String paramValue = paramValues[i];
				result.append( paramValue );
			}
			result.append( "|" );
		}

		result.append( "\nBODY>" );
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				result.append(line);
		} catch (Exception e) { /*report an error*/ }

		System.out.println( result.toString() );
	}

	private static String bytesToHex(byte[] hashInBytes) {
		StringBuilder sb = new StringBuilder();
		for (byte b : hashInBytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

}