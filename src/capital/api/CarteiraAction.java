package capital.api;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.CarteiraBean;
import capital.model.bean.DebitoTokenBean;
import capital.model.bean.PessoaBean;
import capital.model.dd.SitDD;
import capital.service.FirebaseService;
import com.payu.sdk.PayU;
import com.payu.sdk.PayUTokens;
import com.payu.sdk.exceptions.ConnectionException;
import com.payu.sdk.exceptions.InvalidParametersException;
import com.payu.sdk.exceptions.PayUException;
import com.payu.sdk.model.CreditCardToken;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class CarteiraAction extends AbstractAction {

    private static Log LOG = LogFactory.getLog(CarteiraAction.class);

    public CarteiraAction(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, JSONObject body, String pathServer) {
        super(request, response, trans, body, pathServer);
    }

    public Bean adicionarCartao() throws Exception{

        Date dataAtual = CapitalService.getDataAtual();

        String tokenAuth = (String)getBodyParameter("token_auth");

        String cvv = (String)getBodyParameter("cvv");
        String nomeImpresso = (String)getBodyParameter("nome_impresso");
        String numero = (String)getBodyParameter("numero_cartao");
        String mesValidade = (String)getBodyParameter("exp_month");
        String anoValidade = (String)getBodyParameter("exp_year");

        ValidatorContract validator = new ValidatorContract();
        validator.isRequiredString(nomeImpresso, "O nome_impresso é um campo requerido para adicionar um novo cartão");
        validator.isRequiredString(numero, "O numero_cartao é um campo requerido para adicionar um novo cartão");
        validator.isRequiredString(mesValidade, "O exp_month é um campo requerido para adicionar um novo cartão");
        validator.isRequiredString(anoValidade, "O exp_year é um campo requerido para adicionar um novo cartão");
        validator.isRequiredString(cvv, "O cvv é um campo requerido para adicionar um novo cartão");
        numero = StringUtils.getNumberOnly(numero);
        int cardId = CCUtils.getCardID(numero);
        validator.isValid( (cardId > -1), "O número do cartão de crédito enviado é inválido");
        String bandeira = CCUtils.getCardName(cardId);
        validator.isRequiredString( bandeira, "Não foi possível determinar a bandeira desse cartão de crédito");

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();
        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        CreditCardBean ccBean = new CreditCardBean(nomeImpresso, numero, anoValidade, mesValidade, cvv, bandeira);
        CarteiraBean carBean = CapitalService.adicionarCartaoNaCarteira(trans, super.getRequest(), pesBean, getPathServer(), ccBean);

        Bean b = new Bean();
        b.set("situacao_id", SitDD.ATIVO);
        b.set("token_valit", carBean.getToken());

        return CapitalService.sendResponse(getResponse(),getPathServer(), trans, b, tokenAuth);
    }

    public Bean listarCartoes() throws Exception {
        TransactionDB trans = getTrans();

        String tokenAuth = (String)getBodyParameter("token_auth");

        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        Bean res = new Bean();
        res.set("carteiras", CapitalService.doDataCarteira(trans, pesBean).toArray());


        /////////////////////////
        //PayuService.listarCartoes();
        //////////////////////////////////////



        return CapitalService.sendResponse(getResponse(), getPathServer(), trans, res, tokenAuth);
    }

    public Bean excluirCartao() throws Exception{
        TransactionDB trans = getTrans();

        String tokenAuth = (String)getBodyParameter("token_auth");

        String token_valit = (String)getBodyParameter("token_valit");

        ValidatorContract validator = new ValidatorContract();
        validator.isRequiredString(token_valit, "O token_valit é um campo requerido para excluir o cartão");
        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        // apagar em todos os gateways
        CapitalService.excluirCartaoDaCarteira(trans, getPathServer(), getRequest(), pesBean, token_valit);

        Bean result = new Bean();
        return CapitalService.sendResponse(getResponse(), getPathServer(), trans, result, tokenAuth);
    }


    public Bean registrarTokenDebito() throws Exception {
        String tokenAuth = (String)getBodyParameter("token_auth");

        String token = (String)getBodyParameter("token");
        Long data = (Long)getBodyParameter("data");
        String status = (String)getBodyParameter("status");

        ValidatorContract validator = new ValidatorContract();
        validator.isRequiredString(token, "O token é um campo requerido");
        validator.isRequired(data, "A data é um campo requerido");
        validator.isRequiredString(status, "O status é um campo requerido");

        String[] values = new String[] {"pendente", "aprovado", "reprovado"};
        validator.isValid( StringUtils.search(status, values) , "O status deve ser um dos valores "+values.toString() );

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();
        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        DebitoTokenBean debtBean = new DebitoTokenBean();
        debtBean.setDataCadastro(trans.getDataAtual());
        debtBean.setPessoa(pesBean);
        debtBean.setToken(token);
        debtBean.setStatus(status);
        debtBean.setData(data.longValue());
        trans.insert(debtBean);
        trans.commit();

        Bean result = new Bean();
        result.set("status", "success");
        result.set("token", token);
        result.set("message", "Token Registrado com sucesso");

        return result;

    }

    public Bean capturarTokenDebito() throws Exception {

        String tokenAuth = (String)getBodyParameter("token_auth");
        String token = (String)getBodyParameter("token");

        ValidatorContract validator = new ValidatorContract();
        validator.isRequiredString(token, "O token é um campo requerido");

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();
        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        Bean result = new Bean();
        try {
            WhereDB where = new WhereDB();
            where.add("Token", WhereDB.Condition.EQUALS, token);
            List<DebitoTokenBean> lista = trans.select(DebitoTokenBean.class, where);

            DebitoTokenBean bean = lista.get(0);

            result.set("status", "success");
            result.set("token", bean.getToken());
            result.set("status", bean.getStatus());
            result.set("data", bean.getData() );
            result.set("message", "Token Registrado com sucesso");

        } catch (NotFoundException nfe) {
            result.set("status", "not_found");
            result.set("message", "Token não foi encontrado");
        }

        return result;

    }

    public Bean atualizarTokenDebito() throws Exception {
        String tokenAuth = (String)getBodyParameter("token_auth");

        String token = (String)getBodyParameter("token");
        String status = (String)getBodyParameter("status");

        ValidatorContract validator = new ValidatorContract();
        validator.isRequiredString(token, "O token é um campo requerido");
        validator.isRequiredString(status, "O status é um campo requerido");

        String[] values = new String[] {"pendente", "aprovado", "reprovado", "concluido"};
        validator.isValid( StringUtils.search(status, values) , "O status deve ser um dos valores "+StringUtils.formatArrayToString(values, ",") );

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();
        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        Bean result = new Bean();
        try {

            WhereDB where = new WhereDB();
            where.add("Token", WhereDB.Condition.EQUALS, token);
            List<DebitoTokenBean> lista = trans.select(DebitoTokenBean.class, where);

            DebitoTokenBean debtBean = lista.get(0);
            debtBean.setStatus(status);
            trans.update(debtBean);
            trans.commit();

            result.set("status", "success");
            result.set("message", "Token atualizado com sucesso");

        } catch (NotFoundException nfe) {
            result.set("status", "not_found");
            result.set("message", "Token Não foi encontrado");
        }

        return result;

    }
}