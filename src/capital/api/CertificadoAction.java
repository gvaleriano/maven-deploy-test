package capital.api;

import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.ErrorException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.*;
import capital.model.bean.*;
import capital.model.dd.EtaDD;
import capital.model.dd.SitDD;
import capital.service.FirebaseService;
import capital.service.MsgService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class CertificadoAction extends AbstractAction {

    private static Log LOG = LogFactory.getLog(CertificadoAction.class);

    public CertificadoAction(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, JSONObject body, String pathServer) {
        super(request, response, trans, body, pathServer);
    }

    public Bean certificados() throws Exception {

        TransactionDB trans = TransactionDB.getInstance(null);
        PreparedStatement ps = null;
        ResultSet rs = null;
        Integer count = 0;
        Date data_sorteio = null;
        String hora_sorteio = null;
        String descricao = null;
        Integer etapaId = null;
        Integer tipoCertificado = null;
        Connection conn = trans.getConnection();
        try {
            // para saber quantos certificados tem
            StringBuilder sql = new StringBuilder();
            sql.append("select eta.eta_id, eta.eta_descricao descricao, eta.eta_datasorteio data_sorteio, eta.eta_horasorteio hora_sorteio, eta.eta_tipocertificado tipo_certificado, count(*) qtde ");
            sql.append("  from certificado cer, etapa eta ");
            sql.append(" where cer.eta_id = eta.eta_id ");
            sql.append("   and not exists (select 1 from venda vv where vv.cer_id = cer.cer_id) ");
            sql.append(" and to_date(eta.eta_datalimitevenda || ' ' || eta.eta_horalimitevenda, 'DD/MM/YYYY HH24:MI') = ");
            sql.append(" (select min(tb1.dataproxima) from (select to_date(eta.eta_datalimitevenda || ' ' || eta.eta_horalimitevenda, 'DD/MM/YYYY HH24:MI') dataProxima ");
            sql.append(" from etapa eta ");
            sql.append(" where to_date(eta.eta_datalimitevenda || ' ' || eta.eta_horalimitevenda, 'DD/MM/RR HH24:MI') >= to_date(to_char(current_date,'DD/MM/RR HH24:MI'), 'DD/MM/RR HH24:MI')) tb1)");
            sql.append(" group by eta.eta_id, eta.eta_descricao, eta.eta_datasorteio, eta.eta_horasorteio, eta.eta_tipocertificado ");

            LOG.debug( sql.toString() );
            ps = conn.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            if (rs.next()) {
                etapaId = rs.getInt("eta_id");
                count = rs.getInt("qtde");
                tipoCertificado = rs.getInt("tipo_certificado");
                descricao = rs.getString("descricao");
                data_sorteio = rs.getDate("data_sorteio");
                hora_sorteio = rs.getString("hora_sorteio");
            } else {
                throw new WarningException( MsgService.get(getPathServer(), "msg.capitalservice.doDataCertificadoLivreAleatorio.01") );
            }
        } finally {
            rs.close();
            ps.close();
        }

        LOG.debug( "Etapa ID> " + etapaId + " -> Count("+count+")");

        //EtapaBean etaBean = CapitalService.doDataEtapaVendaAtual( getPathServer(), trans);

        EtapaBean etaBean = new EtapaBean();
        etaBean.setId(etapaId);
        etaBean.setDescricao(descricao);
        etaBean.setDataSorteio(data_sorteio);
        etaBean.setHoraSorteio(hora_sorteio);

        // pegar 5 números aleatórios entre 1 e 50k e trazer apenas eles
        Random random = new Random();
        int row1 = random.nextInt(count);
        int row2 = random.nextInt(count);
        int row3 = random.nextInt(count);
        int row4 = random.nextInt(count);
        int row5 = random.nextInt(count);

        LOG.debug("Certificados Rows: ["+row1+","+row2+","+row3+","+row4+","+row5+"]");

        List<CertificadoBean> cerList = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select * from (select rownum linha, cer_id, ");
            sb.append("eta_id, ");
            sb.append("uni_id, ");
            sb.append("cer_numero1, ");
            sb.append("cer_numero2, ");
            sb.append("cer_numero3, ");
            sb.append("cer_sequencia1, ");
            sb.append("cer_sequencia2, ");
            sb.append("cer_sequencia3, ");
            sb.append("cer_valor ");
            sb.append("from certificado ");
            sb.append("where eta_id = ? ");
            sb.append("and not exists (select 1 from venda ven where ven.cer_id = certificado.cer_id)) ");
            sb.append("where linha in ( ?, ?, ?, ?, ? ) ");

            LOG.debug("SQL> "+sb.toString());

            ps = conn.prepareStatement(sb.toString());
            ps.setInt(1, etapaId);
            ps.setInt(2, row1);
            ps.setInt(3, row2);
            ps.setInt(4, row3);
            ps.setInt(5, row4);
            ps.setInt(6, row5);
            rs = ps.executeQuery();

            // quebramos isso para melhorar a performance
            if (tipoCertificado.equals(EtaDD.SIMPLES)) {
                cerList = CertificadoActionHelper.getResultSet1(rs, ps);
            } else if (tipoCertificado.equals(EtaDD.DUPLO)) {
                cerList = CertificadoActionHelper.getResultSet2(rs, ps);
            } else if (tipoCertificado.equals(EtaDD.TRIPLO)) {
                cerList = CertificadoActionHelper.getResultSet3(rs, ps);
            }
        } finally {
            rs.close();
            ps.close();
        }

        Bean result = new Bean();
        Bean[] list = new Bean[cerList.size()];
        for (int i = 0; i < cerList.size(); i++) {
            CertificadoBean cerBean = cerList.get(i);

            String[] sequencia1 = StringUtils.coalesce(cerBean.getSequencia1(), "").split("-");
            String[] sequencia2 = StringUtils.coalesce(cerBean.getSequencia2(), "").split("-");
            String[] sequencia3 = StringUtils.coalesce(cerBean.getSequencia3(), "").split("-");

            Bean bean = new Bean();
            bean.set("valor", cerBean.getValor());
            bean.set("cartela1", sequencia1);
            bean.set("numerosorte1", cerBean.getNumero1());
            bean.set("certificado_id", cerBean.getId());

            if (tipoCertificado >= EtaDD.DUPLO) {
                bean.set("numerosorte2", Integer.parseInt(cerBean.getNumero2()));
                bean.set("cartela2", sequencia2);
            }

            if (tipoCertificado >= EtaDD.TRIPLO) {
                bean.set("numerosorte3", cerBean.getNumero3());
                bean.set("cartela3", sequencia3);
            }

            list[i] = bean;
        }

        Bean etapa = new Bean();
        etapa.set("id", etaBean.getId());
        etapa.set("descricao", etaBean.getDescricao());
        etapa.set("data_sorteio", FormatUtils.formatDate((Date) etaBean.getDataSorteio()) + " " + etaBean.getHoraSorteio());

        result.set("etapa", etapa);
        result.set("list", list);

        return result;
    }

    public Bean comprarCertificado() throws Exception{

        ValidatorContract validator = new ValidatorContract();

        String canal_tag = (String)getBodyParameter("canal_tag");
        String tokenValit = (String)getBodyParameter("token_valit");
        String tokenAuth = (String)getBodyParameter("token_auth");
        String salvar_token_cartao = (String)getBodyParameter("salvar_token_cartao");

        if (StringUtils.hasValue(salvar_token_cartao) == false)
            salvar_token_cartao = "1";

        if (StringUtils.hasValue(canal_tag) == false)
            canal_tag = "android";

        validator.isRequiredString(tokenAuth, "O tokenAuth é um campo requerido");
        //validator.isRequiredString(canal_tag, "O canal_tag é um campo requerido");

        //String[] values = new String[] {"android", "web", "ios"};
        //validator.isValid( StringUtils.search(canal_tag, values) , "O canal_tag deve ser um dos valores 'android' 'ios' 'web' " );

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        String methodPayment = "PINBANK";

        // Numero da sorte
        JSONArray certificados = (JSONArray)getBodyParameter("certificados");
        Integer[] certificadoArray = new Integer[certificados.size()];
        for (int i = 0; i < certificados.size(); i++) {
            certificadoArray[i] = ((Long) certificados.get(i)).intValue();
        }

        String nomeCartao = (String)getBodyParameter("nome_cartao");
        String numeroCartao = (String)getBodyParameter("numero_cartao");
        String bandeiraCartao = (String)getBodyParameter("bandeira_cartao");
        String expYear = (String)getBodyParameter("exp_year");
        String expMonth = (String)getBodyParameter("exp_month");
        String expMonthStr = StringUtils.fillLeft( expMonth+"", '0', 2 );
        String cvv = (String)getBodyParameter("cvv");

        CreditCardBean ccBean = new CreditCardBean(nomeCartao, numeroCartao, expYear, expMonth, cvv, bandeiraCartao);

        String authNumber = "";

        PessoaBean pesBean = null;

        // Acho que deve ser somente pelo token
        TransactionDB trans = getTrans();
        pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        Integer pesId = pesBean.getId();
        UnidadeBean uniBean = pesBean.getUnidade();
        Integer uniId = uniBean.getId();
        Date dataAtual = CapitalService.getDataAtual();
        EtapaBean etaBean = CapitalService.doDataEtapaVendaAtual(getPathServer(), trans);

        Bean[] beanArray = null;

        //
        // COMPRA APENAS PELO CEP DA UNIDADE
        //

        CapitalService.cepLimiteCampra(getPathServer(), trans, pesBean);

        //
        // LIMITE DE COMPRA POR USUARIO
        //

        Integer numComprados = CapitalService.doDataNumComprados(trans, pesBean, etaBean.getId());
        if(((uniBean.getCompraLimite() != null) && (numComprados + certificadoArray.length) > uniBean.getCompraLimite()))
            throw new WarningException(MsgService.get(super.getPathServer(), "msg.015.comprarcertificado.01"));

        //
        // CERTIFICADO
        //

        LOG.debug("Reservando certificados ... "+StringUtils.arrayToString(certificadoArray));
        Integer nsu = reservaVenda( getPathServer(), trans, certificadoArray, etaBean.getId(), pesId, uniId, dataAtual, SitDD.PENDENTE, canal_tag );
        LOG.debug("Certificados reservados (done)");

        CarteiraBean carBean = null;
        try {
            // se passou o token do cartão vou procurar pelo token e faz a venda
            if (tokenValit != null) {
                WhereDB where = new WhereDB();
                where.add("Token", WhereDB.Condition.EQUALS, tokenValit);
                where.add("Pessoa.Id", WhereDB.Condition.EQUALS, pesBean.getId());
                carBean = ((List<CarteiraBean>)trans.select(CarteiraBean.class, where)).get(0);
            }
        } catch (NotFoundException e) {
            LOG.warn("Tentativa de comprar um certificado com um token inválido");
            throw new API400Exception("token_not_found","Seu banco não quis autorizar essa transação. Por favor, dá uma ligadinha lá e veja o que pode estar acontecendo e tente novamente mais tarde!");
        } finally {
            if (carBean == null) {
                carBean = new CarteiraBean();
            }
        }

        beanArray = pinbankVenda(trans, nsu, pesBean, nomeCartao, nomeCartao, etaBean.getId(), carBean.getTokenPinbank(), numeroCartao, bandeiraCartao, expYear, expMonth, cvv, true);

        //
        // Carteira novamente
        //
        Bean carteira = beanArray[0];
        // somente se for pra salvar cartão
        //if ( salvar_token_cartao.equalsIgnoreCase("1")) {
            if (StringUtils.hasValue((String) carteira.get("token"))) {

                try {
                    LOG.debug("Procurando cartão na carteira...");
                    tokenValit = (String) carteira.get("token");
                    // verificar se o token já existe, pq se já existir não faz sentido cadastrar um novo
                    // isso na tabela carteira, pq na pinbank já vai existir.
                    // a pinbank sempre cadastra o token

                    WhereDB where = new WhereDB();
                    where.add("Token", WhereDB.Condition.EQUALS, tokenValit);
                    where.add("Pessoa.Id", WhereDB.Condition.EQUALS, pesBean.getId());
                    carBean = ((List<CarteiraBean>) trans.select(CarteiraBean.class, where)).get(0);

                } catch (NotFoundException nfe) {

                    LOG.debug("Cartão não encontrado na carteira (incluindo)...");
                    CapitalService.adicionarCartaoNaCarteira(trans, super.getRequest(), pesBean, getPathServer(), ccBean);
                    LOG.debug("Cartão incluido na carteira com sucesso.");

//                    String dataValidade = expMonthStr + "/" + expYear;
//                    carBean.setPessoa(pesBean);
//                    carBean.setGateway("VARIOS");
//                    carBean.getUnidade().setId(uniId);
//                    carBean.setNomeApelido("Cartão do "+ nomeCartao + "/ " + trans.getId());
//                    carBean.setDataValidade(dataValidade);
//                    carBean.setNumeroCartao((String) carteira.get("numeroCartao"));
//                    carBean.setBandeira(bandeiraCartao);
//                    carBean.setData(dataAtual);
//                    carBean.getSituacao().setId((Integer) carteira.get("situacao.id"));
//                    carBean.setToken((String) carteira.get("token"));
//                    carBean.setToken((String) carteira.get("token"));
//                    trans.insert(carBean);

                }
            }
        //  }

        trans.commit();

        Bean b = new Bean();
        Bean venda = beanArray[1];
        if (venda.get("situacao.id") != null && venda.get("situacao.id").equals(SitDD.CONCLUIDO)){
            b.set("situacao_id", SitDD.ATIVO);
            b.set("status", "success");
            b.set("codigo_autorizacao", authNumber);
            b.set("message", venda.get("message"));
            b.set("token_valit", carBean.getToken());
            b.set("token_auth", tokenAuth);
        } else {
            throw new API400Exception("venda_não_autorizada", "Seu banco não autorizou esta transação. Por favor, dá uma ligadinha lá ou tente novamente mais tarde");
        }

        return b;
    }

    public Bean comprarCertificado2() throws Exception{

        ValidatorContract validator = new ValidatorContract();

        String canal_tag = (String)getBodyParameter("canal_tag");
        String token_valit = (String)getBodyParameter("token_valit");
        String token_auth = (String)getBodyParameter("token_auth");
        String salvar_token_cartao = (String)getBodyParameter("salvar_token_cartao");
        if (StringUtils.hasValue(salvar_token_cartao) == false)
            salvar_token_cartao = "1";
        boolean salvar_na_carteira = salvar_token_cartao.equals("1");

        if (StringUtils.hasValue(canal_tag) == false)
            canal_tag = "android";

        validator.isRequiredString(token_auth, "O token_auth é um campo requerido");
        //validator.isRequiredString(canal_tag, "O canal_tag é um campo requerido");

        //String[] values = new String[] {"android", "web", "ios"};
        //validator.isValid( StringUtils.search(canal_tag, values) , "O canal_tag deve ser um dos valores 'android' 'ios' 'web' " );

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        // Numero da sorte
        JSONArray certificados = (JSONArray)getBodyParameter("certificados");
        Integer[] certificadoArray = new Integer[certificados.size()];
        for (int i = 0; i < certificados.size(); i++) {
            certificadoArray[i] = ((Long) certificados.get(i)).intValue();
        }

        String nomeEscrito = (String)getBodyParameter("nome_escrito");
        String nomeCartao = (String)getBodyParameter("nome_cartao");
        String numeroCartao = (String)getBodyParameter("numero_cartao");
        String bandeiraCartao = (String)getBodyParameter("bandeira_cartao");
        String expYear = (String)getBodyParameter("exp_year");
        String expMonth = (String)getBodyParameter("exp_month");
        String expMonthStr = StringUtils.fillLeft( expMonth+"", '0', 2 );
        String cvv = (String)getBodyParameter("cvv");

        // isso eh pra saber se está enviando apenas o token ou dados do cartão
        boolean enviou_dados_do_cartao = StringUtils.hasValue(numeroCartao);

        // se enviar os dados do cartão e enviar o token valit tem que dar erro
        if ((enviou_dados_do_cartao == true) && (StringUtils.hasValue(token_valit))) {
            throw new API400Exception("request_invalid", "Não é possível processar essa venda, ou informa o token do cartão ou informa os dados do cartão");
        }

        PessoaBean pesBean = null;

        // Acho que deve ser somente pelo token
        TransactionDB trans = getTrans();
        pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, token_auth);

        if(StringUtils.hasValue(nomeEscrito) == false) {
            nomeEscrito = pesBean.getNomeRazao();
        }

        Integer pesId = pesBean.getId();
        UnidadeBean uniBean = pesBean.getUnidade();
        Integer uniId = uniBean.getId();
        Date dataAtual = CapitalService.getDataAtual();
        EtapaBean etaBean = CapitalService.doDataEtapaVendaAtual(getPathServer(), trans);

        Bean[] beanArray = null;

        //
        // COMPRA APENAS PELO CEP DA UNIDADE
        //

        CapitalService.cepLimiteCampra(getPathServer(), trans, pesBean);

        //
        // LIMITE DE COMPRA POR USUARIO
        //

        Integer numComprados = CapitalService.doDataNumComprados(trans, pesBean, etaBean.getId());
        if (((uniBean.getCompraLimite() != null) && (numComprados + certificadoArray.length) > uniBean.getCompraLimite()))
            throw new WarningException(MsgService.get(super.getPathServer(), "msg.015.comprarcertificado.01"));

        //
        // CERTIFICADO
        //

        LOG.debug("Reservando certificados ... "+StringUtils.arrayToString(certificadoArray));
        List<VendaBean> venList = reservaVenda2( getPathServer(), trans, certificadoArray, etaBean.getId(), pesId, uniId, dataAtual, SitDD.PENDENTE, canal_tag );
        Integer nsu = venList.get(0).getNsu();
        LOG.debug("Certificados reservados. NSU => "+nsu+"(done)");

        Integer pinbank_priority = 1;
        Integer payulatam_priority = 0;
        try {
            // pegar as prioridades dos gateways
            String pinbank_priority_str = (String) FirebaseService.getProperty("gateway_pinbank_priority");
            pinbank_priority = Integer.parseInt(pinbank_priority_str);
            String payulatam_priority_str = (String) FirebaseService.getProperty("gateway_payulatam_priority");
            payulatam_priority = Integer.parseInt(payulatam_priority_str);
        } catch (Exception ex) {
            LOG.error("Não foi possível encontrar prioridade dentro das configurações; erro crítico");
            throw new DeveloperException("Não foi possível encontrar prioridade dentro das configurações; Erro crítico");
        }

        LOG.debug("pinbank_priority => "+pinbank_priority);
        LOG.debug("payulatam_priority => "+payulatam_priority);

        CarteiraBean carBean = null;
        if (enviou_dados_do_cartao == true) {
            CreditCardBean ccBean = new CreditCardBean(nomeCartao, numeroCartao, expYear, expMonth, cvv, bandeiraCartao);
            LOG.debug("Recuperando cartão da carteira...");
            LOG.debug("Cadastrando tokens do cartão...");
            carBean = CapitalService.adicionarCartaoNaCarteira(trans, getRequest(), pesBean, getPathServer(), ccBean);
            LOG.debug("Cadastrando tokens do cartão (done)");
        }

        try {
            if (token_valit != null) {
                carBean = CapitalService.getCartaoCreditoDaCarteira(trans, pesBean, token_valit);
            }
        } catch (NotFoundException e) {
            // apaga a reserva das vendas
            CapitalService.deleteVendasByNSU(trans, pesBean, nsu);
            LOG.warn("Tentativa de comprar um certificado com um token inválido");
            throw new API400Exception("token_not_found","Seu banco não quis autorizar essa transação. Por favor, dá uma ligadinha lá e veja o que pode estar acontecendo e tente novamente mais tarde!");
        }

        Boolean tem_token_pinBank = StringUtils.hasValue( carBean.getTokenPinbank() );
        Boolean tem_token_payU = StringUtils.hasValue( carBean.getTokenPayU() );
        Boolean tem_token_pagSeguro = StringUtils.hasValue( carBean.getTokenPagSeguro() );

        // preciso saber se é um cadastro antigo e só tem token na pinbank
        // se tiver vou tentar o fluxo normal, apenas vender
        boolean tem_token_somente_na_pinbank = true;
        tem_token_somente_na_pinbank = ((tem_token_payU || tem_token_pagSeguro) == false);

        LOG.debug("Cliente tem token somente na pinbank => "+tem_token_somente_na_pinbank);
        LOG.debug("Token na Pinbank => "+tem_token_pinBank);
        LOG.debug("Token na PayU => "+tem_token_payU);
        LOG.debug("Token na PagSeguro => "+tem_token_pagSeguro);

        // se está enviando os dados do cartão, fluxo é (se usuário autorizar claro) cadastrar tokens em todos os gateways
        Bean venda = null;
        // PRIORIDADE 1
        if (pinbank_priority == 1) {
            venda = pinbankVenda2(trans, venList, pesBean, carBean.getTokenPinbank());
        } else if (payulatam_priority == 1) {
            venda = payuVenda(trans, venList, pesBean, carBean);
        }

        // se a venda não foi realizada, tenta a prioridade 2
        boolean venda_realizada = (boolean)venda.get("venda_realizada");
        if (venda_realizada == false) {
            // PRIORIDADE 2
            if (pinbank_priority == 2) {
                venda = pinbankVenda2(trans, venList, pesBean, carBean.getTokenPinbank());
            } else if (payulatam_priority == 2) {
                venda = payuVenda(trans, venList, pesBean, carBean);
            }
        }

        trans.commit();

        if (venda_realizada == true) {
            return CertificadoActionHelper.compraFinalizadaComSucesso(venda, token_auth);
        } else {
            throw new API400Exception("venda_não_autorizada", "Seu banco não autorizou esta transação. Por favor, dá uma ligadinha lá ou tente novamente mais tarde");
        }
    }

    public Bean cancelarCompraCertificado() throws Exception{

        ValidatorContract validator = new ValidatorContract();

        // TODO: criar autenticação para API
        // mais segurança
        String tokenAuth = (String)getBodyParameter("token_auth");
        validator.isRequiredString(tokenAuth, "O token_auth é um campo requerido");

        Long nsu = (Long)getBodyParameter("nsu");
        validator.isRequired(nsu, "O nsu é um campo requerido");

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();
        PessoaBean pesBean = null;
        BigDecimal total = new BigDecimal(0.00);
        String nsu_gateway = null;
        try {
            WhereDB where = new WhereDB();
            //where.add("PessoaCliente.Id", WhereDB.Condition.EQUALS, pesBean.getId());
            where.add("Nsu", WhereDB.Condition.EQUALS, nsu);
            List<VendaBean> list = trans.select(VendaBean.class, where);
            for (VendaBean venBean: list) {
                total = total.add(venBean.getValor());
                nsu_gateway = venBean.getNsuGw();
                pesBean = venBean.getPessoaCliente();
            }
        } catch (NotFoundException ex) {
            throw new API400Exception("not_found", "NSU não encontrado");
        }

        String methodPayment = "PINBANK";
        if (methodPayment.equalsIgnoreCase("pinbank")) {
            PinbankService.cancelarTransacao(getPathServer(), trans, nsu_gateway, pesBean, total, getRequest());
        }

        trans.commit();

        Bean b = new Bean();
        b.set("token_auth", tokenAuth);

        return b;
    }

    public Bean comprarCertificadoNoDebito() throws Exception{

        String methodPayment = "PINBANK";
        String tokenAuth = (String)getBodyParameter("token_auth");
        ValidatorContract validator = new ValidatorContract();

        String status = (String)getBodyParameter("status");
        String codigo_autorizacao = (String)getBodyParameter("codigo_autorizacao");
        String nsu_gw = (String)getBodyParameter("nsu_gw");
        String nsu_adquirente = (String)getBodyParameter("nsu_adquirente");
        String canal_tag = (String)getBodyParameter("canal_tag");

        validator.isRequiredString(tokenAuth, "O tokenAuth é um campo requerido");
        validator.isRequiredString(status, "O status é um campo requerido");
        validator.isRequiredString(codigo_autorizacao, "O codigo_autorizacao é um campo requerido");
        validator.isRequiredString(nsu_gw, "O nsu_gw é um campo requerido");
        validator.isRequiredString(nsu_adquirente, "O nsu_adquirente é um campo requerido");
        validator.isRequiredString(canal_tag, "O canal_tag é um campo requerido");

        String[] values = new String[] {"android", "web", "ios"};
        validator.isValid( StringUtils.search(canal_tag, values) , "O canal_tag deve ser um dos valores 'android' 'ios' 'web' " );

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        // Numero da sorte
        JSONArray certificados = (JSONArray)getBodyParameter("certificados");
        Integer[] certificadoArray = new Integer[certificados.size()];
        for (int i = 0; i < certificados.size(); i++) {
            certificadoArray[i] = ((Long) certificados.get(i)).intValue();
        }

        TransactionDB trans = getTrans();

        PessoaBean pesBean = null;

        // Acho que deve ser somente pelo token
        pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        Integer pesId = pesBean.getId();
        UnidadeBean uniBean = pesBean.getUnidade();
        Integer uniId = uniBean.getId();
        Date dataAtual = CapitalService.getDataAtual();
        EtapaBean etaBean = CapitalService.doDataEtapaVendaAtual(getPathServer(), trans);

        Bean[] beanArray = null;

        //
        // COMPRA APENAS PELO CEP DA UNIDADE
        //
        CapitalService.cepLimiteCampra(getPathServer(), trans, pesBean);

        //
        // LIMITE DE COMPRA POR USUARIO
        //
        Integer numComprados = CapitalService.doDataNumComprados(trans, pesBean, etaBean.getId());
        if(((uniBean.getCompraLimite() != null) && (numComprados + certificadoArray.length) > uniBean.getCompraLimite()))
            throw new WarningException(MsgService.get(super.getPathServer(), "msg.015.comprarcertificado.01"));

        //
        // CERTIFICADO
        //
        String transIdDaVendaPendente = CapitalService.reservaVenda( getPathServer(), trans, certificadoArray, etaBean.getId(), pesId, uniId, dataAtual, SitDD.CONCLUIDO, canal_tag );
        trans.commit();

        Bean b = new Bean();
        b.set("situacao_id", SitDD.ATIVO);
        b.set("status", "success");
        b.set("token_auth", tokenAuth);

        return b;
    }

    public Bean historicoCompras() throws Exception{
        TransactionDB trans = getTrans();

        String tokenAuth = (String)getBodyParameter("token_auth");
        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        // Todas as vendas do usuario - Lista etapas
        List<EtapaBean> etaList = null;
        try {
            etaList = CapitalService.doDataEtapaCompra(trans, pesBean.getId());
        } catch (NotFoundException e) {
            Bean result = new Bean();
            result.set("message", MsgService.get(getPathServer(), "msg.015.historicocompra.01"));
            result.set("rows", 0);
            return result;
        }

        Integer[] etaIdArray = BeanUtils.getPropertieByListToArray(etaList, "Id");

        List<EtapaPremioBean> etapFullList = null;
        // Lista premios
        try {
            WhereDB where = new WhereDB();
            where.add("Etapa.Id", WhereDB.Condition.IN, etaIdArray);
            etapFullList = trans.select(EtapaPremioBean.class, where);
        }catch(NotFoundException e) {
            Bean result = new Bean();
            result.set("message", MsgService.get(super.getPathServer(), "msg.015.historicocompra.02"));
            result.set("rows", 0);
            return result;
        }

        Bean result = new Bean();
        List<Bean> list = new ArrayList<Bean>();
        for (EtapaBean etaBean : etaList) {
            Bean etapa = new Bean();
            List<Bean> premioList = new ArrayList<Bean>();
            List<Bean> certificados = new ArrayList<Bean>();
            list.add(etapa);

            etapa.set("etapa_id", etaBean.getId());
            etapa.set("data", DateUtils.formatDate(etaBean.getDataSorteio()));
            etapa.set("edicao", etaBean.getEdicao());
            etapa.set("descricao", etaBean.getDescricao());
            etapa.set("url_imagem", etaBean.getUrlImagem());

            List<EtapaPremioBean> etapList = new ArrayList<EtapaPremioBean>();
            for (EtapaPremioBean etapBean : etapFullList) {
                if (etapBean.getEtapa().getId().equals(etaBean.getId()) == false) continue;
                etapList.add(etapBean);
            }

            ArrayUtils.orderBy(etapList, "Ordem");

            for (EtapaPremioBean etapBean : etapList) {
                String ordem = "Prêmio " + etapBean.getOrdem();
                String descricao = etapBean.getDescricao();

                //	Lista numeros chamados
                String[] bolaArr = null;
                try {
                    bolaArr = etapBean.getBolas().split("-");
                } catch (Exception e) {
                }
                Bean ganhador = null;
                try {
                    //Lista ganhador
                    ganhador = CapitalService.doDataGanhadorByPremio(trans, etapBean.getId(), pesBean.getId());
                } catch (NotFoundException e) {
                }
                Bean premio = new Bean();
                premio.set("premio_descricao", ordem);
                premio.set("premio", descricao);
                premio.set("numeros_chamados", bolaArr);
                premio.set("ganhador", ganhador);
                premioList.add(premio);
            }

            List<VendaBean> compraList = null;
            try {
                WhereDB where = new WhereDB();
                where.add("Etapa.Id", WhereDB.Condition.EQUALS, etaBean.getId());
                where.add("PessoaCliente.Id", WhereDB.Condition.EQUALS, pesBean.getId());
                where.add("Situacao.Id", WhereDB.Condition.EQUALS, SitDD.CONCLUIDO);
                compraList = trans.select(VendaBean.class, where);
            } catch (NotFoundException e) {
                result = new Bean();
                result.set("message", MsgService.get(super.getPathServer(), "msg.015.historicocompra.03"));
                result.set("rows", 0);
                return result;
            }

            // Lista certificados comprados
            for (VendaBean compraBean : compraList) {
                Bean certificado = new Bean();
                CertificadoBean cerBean = compraBean.getCertificado();

                String numero1 = cerBean.getNumero1() + "";
                String[] sequencia1 = cerBean.getSequencia1().split("-");
                certificado.set("data_compra", DateUtils.formatDate(compraBean.getData()));
                certificado.set("valor", compraBean.getValor() );
                certificado.set("numerosorte1", numero1);
                certificado.set("cartela1", sequencia1);

                if (etaBean.getTipoCertificado() >= EtaDD.DUPLO) {
                    String numero2 = cerBean.getNumero2() + "";
                    String[] sequencia2 = cerBean.getSequencia2().split("-");
                    certificado.set("numerosorte2", numero2);
                    certificado.set("cartela2", sequencia2);
                }

                if (etaBean.getTipoCertificado() >= EtaDD.TRIPLO) {

                    String numero3 = cerBean.getNumero3() + "";
                    String[] sequencia3 = cerBean.getSequencia3().split("-");
                    certificado.set("cartela3", sequencia3);
                    certificado.set("numerosorte3", numero3);
                }

                certificados.add(certificado);
            }
            etapa.set("premios", premioList.toArray());
            etapa.set("certificados_comprados", certificados.toArray());
        }

        result.set("list", list.toArray());

        return result;
    }

    public Bean certificadosVigentes() throws Exception{
        TransactionDB trans = getTrans();

        String tokenAuth = (String)getBodyParameter("token_auth");

        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);
        EtapaBean etaBean = CapitalService.doDataEtapaSorteioAtual( getPathServer(), trans);

        List<Bean> beanList = CapitalService.doDataVendaPorPeriodo(getPathServer(), trans, pesBean, etaBean.getId(), null, null);

        List<Bean> list = new ArrayList<Bean>();
        for (Bean bean : beanList) {
            Integer tipoCertificado = bean.get("tipoCertificado");
            Bean cerBean = new Bean();
            cerBean.set("etapa_id", bean.get("etapa.id"));
            cerBean.set("etapa_descricao", bean.get("etapa.descricao"));
            cerBean.set("numerosorte1", bean.get("numero1"));
            cerBean.set("valor", bean.get("valor"));

            String[] sequencia1 = StringUtils.coalesce( (String) bean.get("sequencia1"), "").split("-");
            cerBean.set("cartela1", sequencia1);


            if( tipoCertificado >= EtaDD.DUPLO) {
                String[] sequencia2 = null;
                sequencia2 = StringUtils.coalesce( (String) bean.get("sequencia2"), "").split("-");
                cerBean.set("numerosorte2", bean.get("numero2"));
                cerBean.set("cartela2", sequencia2);
            }

            if( tipoCertificado >= EtaDD.TRIPLO) {
                String[] sequencia3 = null;
                sequencia3 = StringUtils.coalesce( (String) bean.get("sequencia3"), "").split("-");
                cerBean.set("numerosorte3", bean.get("numero3"));
                cerBean.set("cartela3", sequencia3);
            }
            list.add(cerBean);
        }

        String scheme = super.getRequest().getScheme();             // http
        String serverName = super.getRequest().getServerName();     // hostname.com
        int serverPort = super.getRequest().getServerPort();        // 80
        String contextPath = super.getRequest().getContextPath();   // /mywebapp
        StringBuilder url = new StringBuilder();
        url.append(scheme).append("://").append(serverName);
        if (serverPort != 80 && serverPort != 443) {
            url.append(":").append(serverPort);
        }
        url.append(contextPath).append( "/api2/1.100.020/andamentosorteio");

        Bean sorteio_atual = new Bean();
        sorteio_atual.set("id", etaBean.get("Id"));
        sorteio_atual.set("url_video", etaBean.getUrlVideo());
        sorteio_atual.set("descricao", etaBean.get("descricao"));
        sorteio_atual.set("sorteio", FormatUtils.formatDate((Date) etaBean.getDataSorteio()) + " " + etaBean.getHoraSorteio());
        sorteio_atual.set("data_servidor", FormatUtils.formatDate(CapitalService.getDataAtual(), "dd/MM/yyyy HH:mm") );
        sorteio_atual.set("limite_venda", FormatUtils.formatDate((Date) etaBean.getDataLimiteVenda()) + " " + etaBean.getHoraLimiteVenda());
        sorteio_atual.set("url_acompanhamento", url.toString());

        Bean b = new Bean();
        if(list.size() != 0)
            b.set("list", list.toArray());
        else
            b.set("list", new Object[] {});

        b.set("sorteio_atual", sorteio_atual);

        return CapitalService.sendResponse(getResponse(), getPathServer(), trans, b, tokenAuth);
    }

    private Bean[] pinbankVenda(TransactionDB trans, Integer nsu, PessoaBean pesBean, String nomeEscrito, String nomeCartao, Integer etapaAtual, String tokenValit, String numeroCartao, String bandeiraCartao, String expYear, String expMonth, String cvv, Boolean salvar_cartao ) throws Exception {

        // Se não tem pinbankid cadastra o cliente lá
        if (pesBean.getPinbankId() == null) {
            WhereDB where = new WhereDB();
            where.add("Pessoa.Id", WhereDB.Condition.EQUALS, pesBean.getId());
            PessoaEnderecoBean peseBean = ((List<PessoaEnderecoBean>) trans.select(PessoaEnderecoBean.class, where)).get(0);
            CidadeBean cidBean = peseBean.getCidade();
            EstadoBean estBean = trans.selectById(EstadoBean.class,cidBean.getEstado().getId());

            String telefoneFull = StringUtils.getNumberOnly(pesBean.getTelefone1());
            String pesDDD = telefoneFull.substring(0, 2);
            String pesTelefone = telefoneFull.substring(2, telefoneFull.length());
            String sexo = pesBean.getSexo();

            if(StringUtils.hasValue(sexo) == false) {
                String nome = StringUtils.getFirstWord(pesBean.getNomeRazao());
                nome = nome.toLowerCase();
                if(nome.substring((nome.length() -1)).equals("a")) {
                    sexo = "F";
                }else {
                    sexo = "M";
                }
            }

            /*
             * Esta incorreto esta forma de cadastrar um usuario no sistema da pinbank
             */
            String rg = "" + NumberUtils.random(7);
            String emissorRg = StringUtils.gerarToken(6);
            String bairro = "Centro";
            Date dataEmissaoRg = DateUtils.addYear(new Date(), -15);
            if(pesBean.getNascimentoFundacao() == null) pesBean.setNascimentoFundacao(DateUtils.addYear(new Date(), -25));

            Integer clientePinBankId = null;
            if (pesBean.getNascimentoFundacao() == null) pesBean.setNascimentoFundacao(new Date());
            Bean res = PinbankService.inclusaoDeCliente(trans, pesBean, nomeEscrito, pesBean.getNascimentoFundacao(), sexo, pesBean.getCpfCnpj(), rg, emissorRg, dataEmissaoRg, pesBean.getEmail(), pesDDD, pesTelefone, peseBean.getCep(), estBean.getSigla(), cidBean.getDescricao(), bairro, peseBean.getLogradouro(), peseBean.getNumero(), super.getRequest());
            clientePinBankId = res.get("mml");
            pesBean.setPinbankId(clientePinBankId);

            //
            // É preciso desse commit pq o cliente pode ou nao
            // ja esta cadastrado para seguir em exceções para frente.
            //
            trans.update(pesBean);
            trans.commit();
        }


        //
        // Cartao gerar token
        //
        Bean car = null;
        if (tokenValit == null) {

            Integer codigoCliente = pesBean.getPinbankId(); // esse é o id que retorno do método addCliente
            String nomeImpresso = pesBean.getNomeRazao();
            String cpfCnpj = pesBean.getCpfCnpj();
            cpfCnpj = StringUtils.getNumberOnly(cpfCnpj);
            String expMonthStr = StringUtils.fillLeft( expMonth+"", '0', 2 );

            Bean cartao = PinbankService.inclusaoDeCartao( getPathServer(), trans, pesBean, codigoCliente, numeroCartao, nomeImpresso, nomeCartao, expMonthStr, expYear, cvv, getRequest());
            String idCartao = cartao.get("idCartao");

            car = new Bean();
            car.set("token", idCartao);
            car.set("numeroCartao",  CapitalService.esconderNumerosCartao(numeroCartao));
            car.set("bandeiraCartao",	bandeiraCartao);
            car.set("validade", expMonthStr + "/" + expYear);
            car.set("situacao.id", SitDD.CONCLUIDO);
            car.set("gateway", PinbankService.NAME);
            tokenValit = idCartao;
        } else {
            car = new Bean();
        }

        //
        // venda
        //
        Bean venda = new Bean();
        venda.set("venda_realizada", false);
        if (tokenValit != null) {
            // Ativar venda
            OrderDB order = new OrderDB();
            order.add("Id");

            WhereDB where = new WhereDB();
            where.add("PessoaCliente.Id", WhereDB.Condition.EQUALS, pesBean.getId());
            where.add("Situacao.Id", WhereDB.Condition.EQUALS, SitDD.PENDENTE);
            where.add("Etapa.Id", WhereDB.Condition.EQUALS, etapaAtual);
            where.add("Nsu", WhereDB.Condition.EQUALS, nsu);

            List<VendaBean> venList = null;
            try {
                venList = trans.select(VendaBean.class, where, order);
            } catch (NotFoundException nfe) {
                throw new API400Exception("certificado_ja_vendido", "Esse certificado não está disponível para venda nesta etapa");
            }

            String nsuTransacao = ""+venList.get(0).getNsu();

            BigDecimal valor = BigDecimal.ZERO;
            for (VendaBean venBean : venList) {
                valor = MathUtils.add(valor, venBean.getValor());
            }

            try {

                Bean response = PinbankService.efetuarTransacao(getPathServer(), trans, nsu.toString(), pesBean, pesBean.getPinbankId(), tokenValit, valor, nsuTransacao, super.getRequest());

                // isso aqui é uma POG para facilitar a homologação
                // no caso da pinbank, não tem cartão de crédito que provoque um erro
                // então se eu quiser 'simular' um erro na pinbank, vou espear que no nome do cliente tenha uma string ' POG-PINBANK-DE-ERRO-NA-TRANSACAO '
                // se isso conter no nome do cliente, vai retornar como se a pinbank tivesse dado erro na transação da venda
                if ( pesBean.getNomeRazao().contains("POG-PINBANK-DE-ERRO-NA-TRANSACAO")) {
                    throw new ErrorException("Banco " + MsgService.get(getPathServer(), "msg.pinbankservice.formatresponse.01"));
                }

                for (VendaBean venBean : venList) {
                    venBean.getSituacao().setId(SitDD.CONCLUIDO);
                    venBean.setPinbankNsu((String) response.get("nsuTiPagos"));
                    venBean.setNsuGw((String) response.get("nsuTiPagos"));
                    trans.update(venBean);
                }

                venda.set("venda_realizada", true);
                venda.set("situacao.id", SitDD.CONCLUIDO);
                venda.set("message", response.get("descricaoRetorno"));
            } catch (Exception e) {
                venda.set("venda_realizada", false);
                venda.set("message", e.getMessage());
            }
        }

        Bean[] b = {car, venda};
        return b;
    }

    private Bean pinbankVenda2(TransactionDB trans, List<VendaBean> venList, PessoaBean pesBean, String tokenValit) {

        Bean venda = new Bean();
        venda.set("venda_realizada", false);

        String nsuTransacao = ""+venList.get(0).getNsu();
        BigDecimal valor = BigDecimal.ZERO;
        for (VendaBean venBean : venList) {
            valor = MathUtils.add(valor, venBean.getValor());
        }

        try {

            Bean response = PinbankService.efetuarTransacao(getPathServer(), trans, nsuTransacao, pesBean, pesBean.getPinbankId(), tokenValit, valor, nsuTransacao, super.getRequest());

            // isso aqui é uma POG para facilitar a homologação
            // no caso da pinbank, não tem cartão de crédito que provoque um erro
            // então se eu quiser 'simular' um erro na pinbank, vou espear que no nome do cliente tenha uma string ' POG-PINBANK-DE-ERRO-NA-TRANSACAO '
            // se isso conter no nome do cliente, vai retornar como se a pinbank tivesse dado erro na transação da venda
            if ( pesBean.getNomeRazao().contains("POG-PINBANK-DE-ERRO-NA-TRANSACAO")) {
                throw new ErrorException("Banco " + MsgService.get(getPathServer(), "msg.pinbankservice.formatresponse.01"));
            }

            for (VendaBean venBean : venList) {
                venBean.getSituacao().setId(SitDD.CONCLUIDO);
                venBean.setNsuGw((String) response.get("nsuTiPagos"));
                trans.update(venBean);
            }

            venda.set("venda_realizada", true);
            venda.set("situacao.id", SitDD.CONCLUIDO);
            venda.set("message", response.get("descricaoRetorno"));
        } catch (Exception e) {
            venda.set("venda_realizada", false);
            venda.set("message", e.getMessage());
        }


        return venda;
    }

    private Bean payuVenda(TransactionDB trans, List<VendaBean> venList, PessoaBean pesBean, CarteiraBean carBean) {

        LOG.debug("PayuVenda (starting)... ");

        Bean venda = new Bean();
        venda.set("venda_realizada", false);

        Integer nsuTransacao = venList.get(0).getNsu();
        BigDecimal valor = BigDecimal.ZERO;
        for (VendaBean venBean : venList) {
            valor = MathUtils.add(valor, venBean.getValor());
        }
        try {
            Bean response = PayuService.efetuarTransacao(trans, pesBean, valor, super.getRequest(), nsuTransacao, carBean);
            for (VendaBean venBean : venList) {
                venBean.getSituacao().setId(SitDD.CONCLUIDO);
                venBean.setNsuGw((String) response.get("transaction_id"));
                trans.update(venBean);
            }
            venda.set("venda_realizada", true);
            venda.set("situacao.id", SitDD.CONCLUIDO);
            venda.set("message", response.get("descricaoRetorno"));
        } catch (Exception e) {
            venda.set("venda_realizada", false);
            venda.set("message", e.getMessage());
        }

        LOG.debug("venda_realizada = "+venda.get("venda_realizada"));
        LOG.debug("PayuVenda (done)... ");
        return venda;
    }

    private Integer reservaVenda(String pathServer, TransactionDB trans, Integer[] certificadoArray, Integer etapaId, Integer pesId, Integer uniId, Date dataAtual, Integer situacao, String canal) throws NotFoundException, Exception {

        List<CertificadoBean> cerList = null;
        WhereDB where;
        try {
            LOG.debug("Buscando certificado... " + StringUtils.arrayToString(certificadoArray));
            where = new WhereDB();
            where.add("Numero1", WhereDB.Condition.IN, certificadoArray);
            where.add("Etapa.id", WhereDB.Condition.EQUALS, etapaId);
            cerList = trans.select(CertificadoBean.class, where);
        } catch (NotFoundException nfe) {
            throw new API400Exception("bad_request", "Tentativa de comprar um certificado de uma etapa diferente da etapa atual");
        }

        List<VendaBean> venFinalList = new ArrayList<VendaBean>();

        String transId = null;
        Integer nsu = null;
        for (CertificadoBean cerBean : cerList) {
            BigDecimal cerValor = cerBean.getValor();
            Integer etapaAtual = cerBean.getEtapa().getId();

            VendaBean venBean = null;
            try {
                where = new WhereDB();
                where.add("Certificado.Id", WhereDB.Condition.EQUALS, cerBean.getId());
                // Se achar algum resultado ele pula e NÃO insere como uma nova venda
                List<VendaBean> venList = trans.select(VendaBean.class, where);
                venBean = venList.get(0);
                transId = venBean.getTransacao();
                if (venBean.getSituacao().getId().equals(SitDD.CONCLUIDO)) {
                    throw new API400Exception("certificado_ja_vendido", MsgService.get(pathServer, "msg.capitalservice.reservaVenda.01"));

                } else if (venBean.getPessoaCliente().getId().equals(pesId) == false) {
                    throw new API400Exception("certificado_ja_vendido", MsgService.get(pathServer, "msg.capitalservice.reservaVenda.01"));
                }

                nsu = venBean.getNsu();
            } catch (NotFoundException e) {

                if (transId == null) {
                    transId = trans.getId();
                }

                // Adiciona uma como uma nova venda
                venBean = new VendaBean();
                venBean.setTransacao(transId);
                venBean.getSituacao().setId(situacao);
                venBean.getEtapa().setId(etapaAtual);
                venBean.getPessoaCliente().setId(pesId);
                venBean.getCertificado().setId(cerBean.getId());
                venBean.getUnidade().setId(uniId);
                venBean.setData(dataAtual);
                venBean.setHora(DateUtils.formatHour(dataAtual));
                venBean.setValor(cerValor);
                venBean.setPinbankNsu("");

                if (StringUtils.hasValue(canal) == false)
                    venBean.setCanalTag("Mobile");
                else
                    venBean.setCanalTag(canal);

                // isso faz com que as vendas fiquem com o mesmo nsu
                if (nsu == null) {
                    nsu = CapitalService.getNsu(trans);
                }
                venBean.setNsu(nsu);

                venFinalList.add(venBean);
            }
        }

        for (VendaBean venBean : venFinalList) {
            trans.merge(venBean);
        }

        LOG.debug("Gravando (commit) reserva ...");
        // A partir daqui pode acorrer um erro na compra mas a compra ainda deve ficar registrada como pendente
        trans.commit();

        return nsu;
    }

    private List<VendaBean> reservaVenda2(String pathServer, TransactionDB trans, Integer[] certificadoArray, Integer etapaId, Integer pesId, Integer uniId, Date dataAtual, Integer situacao, String canal) throws NotFoundException, Exception {

        List<CertificadoBean> cerList = null;
        WhereDB where;
        try {
            LOG.debug("Buscando certificado... " + StringUtils.arrayToString(certificadoArray));
            where = new WhereDB();
            where.add("Numero1", WhereDB.Condition.IN, certificadoArray);
            where.add("Etapa.id", WhereDB.Condition.EQUALS, etapaId);
            cerList = trans.select(CertificadoBean.class, where);
        } catch (NotFoundException nfe) {
            throw new API400Exception("bad_request", "Tentativa de comprar um certificado de uma etapa diferente da etapa atual");
        }

        List<VendaBean> venFinalList = new ArrayList<VendaBean>();

        String transId = null;
        Integer nsu = null;
        for (CertificadoBean cerBean : cerList) {
            BigDecimal cerValor = cerBean.getValor();
            Integer etapaAtual = cerBean.getEtapa().getId();

            VendaBean venBean = null;
            try {

                if (transId == null) {
                    transId = trans.getId();
                }

                // Adiciona uma como uma nova venda
                venBean = new VendaBean();
                venBean.setTransacao(transId);
                venBean.getSituacao().setId(situacao);
                venBean.getEtapa().setId(etapaAtual);
                venBean.getPessoaCliente().setId(pesId);
                venBean.getCertificado().setId(cerBean.getId());
                venBean.getUnidade().setId(uniId);
                venBean.setData(dataAtual);
                venBean.setHora(DateUtils.formatHour(dataAtual));
                venBean.setValor(cerValor);
                venBean.setPinbankNsu("");

                if (StringUtils.hasValue(canal) == false)
                    venBean.setCanalTag("Mobile");
                else
                    venBean.setCanalTag(canal);

                // isso faz com que as vendas fiquem com o mesmo nsu
                if (nsu == null) {
                    nsu = CapitalService.getNsu(trans);
                }
                venBean.setNsu(nsu);
                trans.insert(venBean);

                venFinalList.add(venBean);
            } catch (Exception ex) {
                trans.rollback();
                throw new API400Exception("bad_request", "Tentativa de comprar um certificado já vendido pra outro cliente");
            }
        }

        LOG.debug("Gravando (commit) reserva ...");
        // A partir daqui pode acorrer um erro na compra mas a compra ainda deve ficar registrada como pendente
        trans.commit();

        return venFinalList;
    }

}
