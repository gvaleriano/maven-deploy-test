package capital.api;

import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.exceptions.ErrorException;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.MathUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.*;
import capital.model.dd.SitDD;
import capital.service.MsgService;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CertificadoActionHelper {

    public static List<CertificadoBean> getResultSet1(ResultSet rs, PreparedStatement ps) throws SQLException {
        List<CertificadoBean> cerList2 = new ArrayList<CertificadoBean>();
        while (rs.next()) {
            CertificadoBean cerBean = new CertificadoBean();

            cerBean.setId( rs.getInt("cer_id") );
            cerBean.getEtapa().setId( rs.getInt("eta_id") );
            cerBean.getUnidade().setId( rs.getInt("uni_id"));
            cerBean.setNumero1( rs.getInt("cer_numero1"));
            cerBean.setSequencia1( rs.getString("cer_sequencia1"));
            cerBean.setValor( rs.getBigDecimal("cer_valor"));

            cerList2.add(cerBean);
        }
        return cerList2;
    }

    public static List<CertificadoBean> getResultSet2(ResultSet rs, PreparedStatement ps) throws SQLException {
        List<CertificadoBean> cerList2 = new ArrayList<CertificadoBean>();
        while (rs.next()) {
            CertificadoBean cerBean = new CertificadoBean();

            cerBean.setId( rs.getInt("cer_id") );
            cerBean.getEtapa().setId( rs.getInt("eta_id") );
            cerBean.getUnidade().setId( rs.getInt("uni_id"));
            cerBean.setNumero1( rs.getInt("cer_numero1"));
            cerBean.setSequencia1( rs.getString("cer_sequencia1"));

            cerBean.setNumero2( rs.getString("cer_numero2"));
            cerBean.setSequencia2( rs.getString("cer_sequencia2"));
            cerBean.setValor( rs.getBigDecimal("cer_valor"));

            cerList2.add(cerBean);
        }
        return cerList2;
    }

    public static List<CertificadoBean> getResultSet3(ResultSet rs, PreparedStatement ps) throws SQLException {
        List<CertificadoBean> cerList2 = new ArrayList<CertificadoBean>();
        while (rs.next()) {
            CertificadoBean cerBean = new CertificadoBean();
            cerBean.setId( rs.getInt("cer_id") );
            cerBean.getEtapa().setId( rs.getInt("eta_id") );
            cerBean.getUnidade().setId( rs.getInt("uni_id"));
            cerBean.setNumero1( rs.getInt("cer_numero1"));
            cerBean.setSequencia1( rs.getString("cer_sequencia1"));
            cerBean.setNumero2( rs.getString("cer_numero2"));
            cerBean.setSequencia2( rs.getString("cer_sequencia2"));
            cerBean.setNumero3(rs.getInt("cer_numero3"));
            cerBean.setSequencia3(rs.getString("cer_sequencia3"));
            cerBean.setValor( rs.getBigDecimal("cer_valor"));

            cerList2.add(cerBean);
        }
        return cerList2;
    }

    public static Bean compraFinalizadaComSucesso(Bean venda, String tokenAuth) {
        Bean b = new Bean();
        b.set("status", "success");
        b.set("message", venda.get("message"));
        b.set("token_auth", tokenAuth);
        return b;
    }

    public static Bean compraFinalizadaSemSucesso(Bean venda, String tokenAuth) {
        Bean b = new Bean();
        b.set("status", "error");
        b.set("message", venda.get("message"));
        b.set("token_auth", tokenAuth);
        return b;
    }

    public static String getTokenValit(List<Bean> cartoes, String gateway) {
        for (Bean bean: cartoes) {
            if ((bean.get("gateway") != null) && bean.get("gateway").equals(gateway)) {
                return bean.get("token");
            }
        }
        return null;
    }

}
