package capital.api;

public class CreditCardBean {

    public String nome;
    public String numero;
    public String exp_year;
    public String exp_month;
    public String cvv;
    public String bandeira;
    public String token;
    public String doc;
    public String endereco;

    public CreditCardBean(String nome, String numero, String exp_year, String exp_month, String cvv, String bandeira) {
        this.nome = nome;
        this.numero = numero;
        this.exp_year = exp_year;
        this.exp_month = exp_month;
        this.cvv = cvv;
        this.bandeira = bandeira;
    }
}
