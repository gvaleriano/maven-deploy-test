package capital.api;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

public abstract class EndPointHelper {
	
	public static String getToken(HttpServletRequest httpRequest) {
		String token = null;
		Enumeration<String> headerNames = httpRequest.getHeaderNames();

		// Bearer flamengo_eh_o_maior, sqn!
		if (headerNames != null) {
			while (headerNames.hasMoreElements()) {
				String name = headerNames.nextElement();
				if (name.equals("authorization")) {
					String aux = httpRequest.getHeader(name);
					int p = aux.indexOf(" ");
					token = aux.substring(p+1);
				}
			}
		}
		return token;
	}

}
