package capital.api;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.RequestBean;
import capital.service.FirebaseService;
import capital.service.MsgService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(name = "EndpointGetServlet")
public class EndpointServlet extends HttpServlet {

    private static Log LOG = LogFactory.getLog(EndpointServlet.class);

    private void setAccessControlHeaders(HttpServletResponse response) throws IOException {

        String app_mode = System.getenv("DIX_ENV");
        LOG.debug("DIX_ENV => "+app_mode);
        if (StringUtils.hasValue(app_mode) == false) {
            LOG.warn("A variável de ambiente DIX_ENV não foi definida!");
            throw new IOException("A variável de ambiente DIX_ENV não foi definida! ");
        }

        if (app_mode.equalsIgnoreCase("PRODUCTION")) {
            //response.addHeader("Access-Control-Allow-Origin", "loja.capitaldepremios.com.br");
            response.addHeader("Access-Control-Allow-Origin", "*");
        } else {
            response.addHeader("Access-Control-Allow-Origin", "*");
        }
        response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type");
        response.addHeader("Access-Control-Max-Age", "86400");

        LOG.debug( "setAccessControlHeaders invoke (finish)");
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setAccessControlHeaders(resp);
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doExecute("post", request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doExecute("get", request, response);
    }

    protected void doExecute(String method, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean version2 = false;
        TransactionDB trans = null;
        RequestBean myReqBean = new RequestBean();
        myReqBean.setHost( request.getRemoteAddr() );
        long start_time = System.currentTimeMillis();
        try {
            setAccessControlHeaders(response);

            LOG.info("Iniciou o servlet para o host "+request.getRemoteAddr());

            try {
                request.setCharacterEncoding("UTF-8");
                response.setContentType("application/json; charset=UTF-8");
            } catch (Exception ex) {
                JSONObject json = new JSONObject();
                json.put("success", false);
                json.put("message", "Nao foi possivel determinar ContentType para a resposta");
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print( json );
                return;
            }

            // imprimir parms request
            printQueryParameters(request);

            StringBuffer body = new StringBuffer();
            JSONObject jsonObject = null;
            try {
                // imprimir body request (once)
                String line = null;

                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null)
                    body.append(line);

                if ( StringUtils.hasValue(body.toString()) == true ) {
                    JSONParser parser = new JSONParser();
                    jsonObject = (JSONObject) parser.parse(body.toString());
                }
                LOG.debug("BODY> "+body.toString());

            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
                JSONObject json = new JSONObject();
                json.put("success", false);
                json.put("message", "Tem algum erro no envio dos parâmetros do BODY. Verifique! JSON => "+body.toString());
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print( json );
                return;
            }

            // se não for body
            if ((jsonObject == null) && ( method.equalsIgnoreCase("get") )) {
                jsonObject = new JSONObject();
                saveQueryParameters(request, jsonObject);
            }

            LOG.debug("BODY> "+jsonObject);

            ValidatorContract validator = new ValidatorContract();

            String api_key = request.getParameter("command");
            if (api_key == null ) api_key = (String)jsonObject.get("api_key");
            if (api_key != null) version2 = (StringUtils.hasValue(api_key));

            LOG.debug("api_key > "+api_key);
            LOG.debug("version2 > "+version2);
            String command = request.getParameter("command");
            if (command == null ) command = (String)jsonObject.get("command");

            if (command == null) {
                throw new API400Exception("command_missing", "Não foi informado o parâmetro 'command' na requisição, ou o 'command' não vou encontrado no servidor");
            }

            LOG.info("command> "+command);
            if (jsonObject.get("token_auth") != null) LOG.debug("token_auth> "+jsonObject.get("token_auth"));

            trans = TransactionDB.getInstance(null);
            String serverPath = request.getRealPath("/").replaceAll("\\\\", "/");
            String versaoApp = (String)jsonObject.get("versao_app");

            myReqBean.setData( trans.getDataAtual() );
            myReqBean.setHora( trans.getHoraAtual() );
            myReqBean.setEndpoint( command );
            myReqBean.setVersao( versaoApp );
            myReqBean.setRequest( jsonObject.toString() );

            if (versaoApp == null) {
                throw new API400Exception("versao_app_missing", "Não foi informado o parâmetro 'versao_app' na requisição.");
            }

            if (!versaoApp.startsWith("web.vue") && !versaoApp.startsWith("ios")) {
                if (CapitalService.validVersaoApp(trans, versaoApp.toString())) {

                    LOG.info("Versão Desatualizada - Precisando atualizar.. ");

                    Bean b = new Bean();
                    b.set("precisa_atualizar", true);

                    Bean ret = new Bean();
                    ret.set("success", false);
                    b.set("message", MsgService.get( serverPath, "msg.capitalservice.responseversaoapp.01"));
                    ret.set("result", b);
                    response.getWriter().print(ret.toJson2());
                    return;
                }
            }

            LOG.info(command+ " starting... ");
            Bean result = null;

            if (command.equals("reloadConfig")) {
                FirebaseService.clean();
                result = new Bean();
                result.set("success", true);
            }

            if (command.equals("etapaAtual")) {
                EtapaAction action = new EtapaAction(request, response, trans, jsonObject, serverPath);
                result = action.etapaAtual();
            }

            if (command.equals("sorteioAtual")) {
                EtapaAction action = new EtapaAction(request, response, trans, jsonObject, serverPath);
                result = action.sorteioAtual();
            }

            if (command.equals("etapaAtiva")) {
                EtapaAction action = new EtapaAction(request, response, trans, jsonObject, serverPath);
                result = action.etapaAtiva();
            }

            if (command.equals("historicoSorteio")) {
                EtapaAction action = new EtapaAction(request, response, trans, jsonObject, serverPath);
                result = action.historicoSorteio();
            }

            if (command.equals("login")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.login();
            }

            if (command.equals("loginWithFacebook")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.loginWithFacebook();
            }

            if (command.equals("meusDados")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.meusDados();
            }

            if (command.equals("cadastro")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.cadastro();
                if (version2 == true) response.setStatus(HttpServletResponse.SC_CREATED);
            }

            if (command.equals("atualiza")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.atualiza();
            }


            if (command.equals("atualiza2")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.atualiza2();
            }

            if (command.equals("feedback")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.feedback();
            }

            if (command.equals("buscaCpf")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.buscaCpf();
            }

            if (command.equals("recuperarSenhaPasso1")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.recuperarSenhaPasso1();
            }

            if (command.equals("recuperarSenhaPasso2")) {
                PessoaAction action = new PessoaAction(request, response, trans, jsonObject, serverPath);
                result = action.recuperarSenhaPasso2();
            }

            if (command.equals("certificados")) {
                CertificadoAction action = new CertificadoAction(request, response, trans, jsonObject, serverPath);
                result = action.certificados();
            }

            if (command.equals("comprarCertificado")) {
                CertificadoAction action = new CertificadoAction(request, response, trans, jsonObject, serverPath);
                result = action.comprarCertificado();
            }

            if (command.equals("comprarCertificado2")) {
                CertificadoAction action = new CertificadoAction(request, response, trans, jsonObject, serverPath);
                result = action.comprarCertificado2();
            }


            if (command.equals("comprarCertificadoNoDebito")) {
                CertificadoAction action = new CertificadoAction(request, response, trans, jsonObject, serverPath);
                result = action.comprarCertificadoNoDebito();
            }

            if (command.equals("cancelarCompraCertificado")) {
                CertificadoAction action = new CertificadoAction(request, response, trans, jsonObject, serverPath);
                result = action.cancelarCompraCertificado();
            }

            if (command.equals("historicoCompras")) {
                CertificadoAction action = new CertificadoAction(request, response, trans, jsonObject, serverPath);
                result = action.historicoCompras();
            }

            if (command.equals("certificadosVigentes")) {
                CertificadoAction action = new CertificadoAction(request, response, trans, jsonObject, serverPath);
                result = action.certificadosVigentes();
            }

            if (command.equals("adicionarCartao")) {
                CarteiraAction action = new CarteiraAction(request, response, trans, jsonObject, serverPath);
                result = action.adicionarCartao();
                if (version2 == true) response.setStatus(HttpServletResponse.SC_CREATED);
            }

            if (command.equals("listarCartoes")) {
                CarteiraAction action = new CarteiraAction(request, response, trans, jsonObject, serverPath);
                result = action.listarCartoes();
            }

            if (command.equals("excluirCartao")) {
                CarteiraAction action = new CarteiraAction(request, response, trans, jsonObject, serverPath);
                result = action.excluirCartao();
            }

            if (command.equals("encerrarPremio")) {
                EtapaAction action = new EtapaAction(request, response, trans, jsonObject, serverPath);
                result = action.encerrarPremio();
            }

            if (command.equals("registrarTokenDebito")) {
                CarteiraAction action = new CarteiraAction(request, response, trans, jsonObject, serverPath);
                result = action.registrarTokenDebito();
            }

            if (command.equals("capturarTokenDebito")) {
                CarteiraAction action = new CarteiraAction(request, response, trans, jsonObject, serverPath);
                result = action.capturarTokenDebito();
            }

            if (command.equals("atualizarTokenDebito")) {
                CarteiraAction action = new CarteiraAction(request, response, trans, jsonObject, serverPath);
                result = action.atualizarTokenDebito();
            }

            LOG.info(command+ " finish ");

            if (result == null)
                throw new API400Exception("command_not_found","Nenhuma requisição foi executada, provavelmente o 'command' não está mapeado");

            Bean ret = new Bean();
            ret.set("success", true);
            ret.set("result", result);
            response.getWriter().print( ret.toJson2() );
        } catch (API400Exception ex1) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            myReqBean.setResponse(ex1.getMessage());
            ex1.printStackTrace();
            Bean result = new Bean();
            result.set("message", ex1.getMessage());
            result.set("error", "400");
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", ex1.getMessage());
            result2.set("result", result);
            try {
                if (version2) response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                LOG.error(exx.getMessage(), exx);
            }
        } catch (API401Exception ex1) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            myReqBean.setResponse(ex1.getMessage());
            ex1.printStackTrace();
            Bean result = new Bean();
            result.set("message", ex1.getMessage());
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", ex1.getMessage());
            result2.set("result", result);
            try {
                if (version2 == true) response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                LOG.error(exx.getMessage(), exx);
            }
        } catch (API403Exception ex401) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            myReqBean.setResponse(ex401.getMessage());
            ex401.printStackTrace();
            Bean result = new Bean();
            result.set("message", ex401.getMessage());
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", ex401.getMessage());
            result2.set("result", result);
            try {
                if (version2 == true) response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                LOG.error(exx.getMessage(), exx);
            }
        } catch (WarningException ex2) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            myReqBean.setResponse(ex2.getMessage());
            ex2.printStackTrace();
            Bean result = new Bean();
            result.set("message", ex2.getMessage());
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", ex2.getMessage());
            result2.set("result", result);
            try {
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                LOG.error(exx.getMessage(), exx);
            }
        } catch (Exception ex3) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            myReqBean.setResponse(ex3.getMessage());
            ex3.printStackTrace();
            Bean result = new Bean();
            result.set("message", "Erro inesperado no servidor.");
            result.set("internal_error", "Erro inesperado no ser3vidor.");
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", "Erro inesperado no servidor.");
            result2.set("internal_error", "Erro inesperado no servidor.");
            result2.set("result", result);
            try {
                if (version2 == true) response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                exx.printStackTrace();
            }
        } catch (Throwable th1) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            myReqBean.setResponse(th1.getMessage());
            th1.printStackTrace();
            Bean result = new Bean();
            result.set("message", "Erro inesperado no servidor.");
            result.set("internal_error", "Erro inesperado no servidor.");
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", "Erro inesperado no se4rvidor.");
            result2.set("internal_error", "Erro inesperado no servidor.");
            result2.set("result", result);
            try {
                if (version2 == true) response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                exx.printStackTrace();
            }
        } finally {
            try {
                if (trans == null) trans = TransactionDB.getInstance(null);

                long end_time = System.currentTimeMillis();
                myReqBean.setHttpStatus(response.getStatus());
                myReqBean.setTime(new Long(end_time - start_time).intValue());
                trans.insert(myReqBean);
                trans.commit();
                trans.close();

            } catch (Exception ex) {
                ex.printStackTrace();
                LOG.error(ex);
            }
        }
    }

    private void printQueryParameters(HttpServletRequest request) {
        LOG.debug("\nPARAMS>" );

        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            LOG.debug( paramName + " => ");
            String[] paramValues = request.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                LOG.debug( paramValue );
            }
            LOG.debug( "|" );
        }

    }

    private void saveQueryParameters(HttpServletRequest request, JSONObject jsonObject) {
        LOG.debug("\nPARAMS saving...>" );

        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            String[] paramValues = request.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                jsonObject.put( paramName, paramValue );
            }
            LOG.debug( "|" );
        }

    }
}
