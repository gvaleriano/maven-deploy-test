package capital.api;

import br.com.dotum.jedi.core.db.ORM;
import br.com.dotum.jedi.core.db.OrderDB;
import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.FormatUtils;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.*;
import capital.model.dd.SitDD;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import capital.service.MsgService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EtapaAction extends AbstractAction {

    private static Log LOG = LogFactory.getLog(EtapaAction.class);

    public EtapaAction(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, JSONObject body, String pathServer) {
        super(request, response, trans, body, pathServer);
    }

    public Bean etapaAtual() throws Exception {

        Bean result = new Bean();

        ValidatorContract validator = new ValidatorContract();
        TransactionDB trans = getTrans();

        EtapaBean etaBean = null;
        etaBean = CapitalService.doDataEtapaVendaAtual( getPathServer(), trans);

        result.set("id", etaBean.get("Id"));
        result.set("url_video", etaBean.getUrlVideo());
        result.set("descricao", etaBean.get("descricao"));
        result.set("sorteio", FormatUtils.formatDate((Date) etaBean.getDataSorteio()) + " " + etaBean.getHoraSorteio());
        result.set("data_servidor", FormatUtils.formatDate(CapitalService.getDataAtual(), "dd/MM/yyyy HH:mm") );
        result.set("limite_venda", FormatUtils.formatDate((Date) etaBean.getDataLimiteVenda()) + " " + etaBean.getHoraLimiteVenda());

        String scheme = super.getRequest().getScheme();             // http
        String serverName = super.getRequest().getServerName();     // hostname.com
        int serverPort = super.getRequest().getServerPort();        // 80
        String contextPath = super.getRequest().getContextPath();   // /mywebapp

        StringBuilder url = new StringBuilder();
//        if (serverPort == 443) {
//            scheme += "s"; // http's'
//        }
        url.append(scheme).append("://").append(serverName);
        if (serverPort != 80 && serverPort != 443) {
            url.append(":").append(serverPort);
        }
        url.append(contextPath).append( "/etapaatual/imagem"+etaBean.getId()+".jpg");
        result.set("imagem_url", etaBean.getUrlImagem() );

        return result;
    }

    public Bean sorteioAtual() throws Exception {

        TransactionDB trans = getTrans();
        String tokenAuth = (String)getBodyParameter("token_auth");

        EtapaBean etaBean = null;
        etaBean = CapitalService.doDataEtapaSorteioAtual( getPathServer(), trans);

        String scheme = super.getRequest().getScheme();             // http
        String serverName = super.getRequest().getServerName();     // hostname.com
        int serverPort = super.getRequest().getServerPort();        // 80
        String contextPath = super.getRequest().getContextPath();   // /mywebapp

        StringBuilder url = new StringBuilder();
        url.append(scheme).append("://").append(serverName);
        if (serverPort != 80 && serverPort != 443) {
            url.append(":").append(serverPort);
        }
        url.append(contextPath).append( "/api2/1.100.020/andamentosorteio");

        Bean result = new Bean();
        result.set("status", "success");
        result.set("id", etaBean.get("Id"));
        result.set("url_video", etaBean.getUrlVideo());
        result.set("descricao", etaBean.get("descricao"));
        result.set("sorteio", FormatUtils.formatDate((Date) etaBean.getDataSorteio()) + " " + etaBean.getHoraSorteio());
        result.set("data_servidor", FormatUtils.formatDate(CapitalService.getDataAtual(), "dd/MM/yyyy HH:mm") );
        result.set("limite_venda", FormatUtils.formatDate((Date) etaBean.getDataLimiteVenda()) + " " + etaBean.getHoraLimiteVenda());
        result.set("url_acompanhamento", url.toString());

        WhereDB where = new WhereDB();
        where.add("Etapa.Id", WhereDB.Condition.EQUALS ,etaBean.getId());
        OrderDB order = new OrderDB();
        order.add("Ordem", OrderDB.Orientation.ASC);
        List<EtapaPremioBean> premios = trans.select(EtapaPremioBean.class, where, order);
        List<Bean> premios_json = new ArrayList<Bean>();

        for (EtapaPremioBean bean: premios) {
            Bean b = new Bean();
            b.set("id", bean.getId());
            b.set("descricao", bean.getDescricao());
            b.set("ordem", bean.getOrdem());
            premios_json.add( b );
        }

        result.set("premios", premios_json.toArray());

        return result;
    }

    public Bean etapaAtiva() throws Exception {

        TransactionDB trans = TransactionDB.getInstance(null);
        Integer etapaId = ((Long)getBodyParameter("etapa_id")).intValue();

        Bean result = new Bean();

        EtapaBean etapaBean = null;
        try {
            etapaBean = trans.selectById(EtapaBean.class, etapaId);
        } catch (NotFoundException e) {
            throw new API400Exception("invalid_etapa", "O número da etapa informada não foi encontrado.");
        }

        Date horaLimite = null;
        if (etapaBean.getHoraLimiteVenda() == null) {
            result.set("message", MsgService.get( getPathServer(), "msg.020.etapaativa.01"));
        } else {
            if(etapaBean.getHoraLimiteVenda().length() < 8) {
                horaLimite = DateUtils.parseDateWithHour(etapaBean.getDataLimiteVenda(), etapaBean.getHoraLimiteVenda() + ":00");
            } else {
                horaLimite = DateUtils.parseDateWithHour(etapaBean.getDataLimiteVenda(), etapaBean.getHoraLimiteVenda());
            }
            Date horaAtual = trans.getDataAtual();
            if (horaLimite != null) {
                if (DateUtils.secundsBetween(horaAtual, horaLimite) <= 0) {
                    throw new WarningException( MsgService.get( getPathServer(), "msg.020.etapaativa.02") );
                }
            }
        }

        return result;
    }

    public Bean historicoSorteio() throws Exception {

        TransactionDB trans = getTrans();
        String tokenAuth = (String)getBodyParameter("token_auth");

        PessoaBean pesBean = null;
        if (StringUtils.hasValue(tokenAuth)) {
            pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);
        }

        //
        // Regra de Negocio
        //
        List<Bean> listBean = new ArrayList<Bean>();

        try {
            StringBuilder sb = new StringBuilder();
            sb.append("                 select eta.eta_id                                                        ");
            sb.append("                   from etapa eta                                                         ");
            sb.append("                   join etapapremio etap on (eta.eta_id  = etap.eta_id)                   ");
            sb.append("                  where etap.sit_id = "+ SitDD.CONCLUIDO+"                                 ");
            sb.append("               group by eta.eta_id                                                        ");

            ORM orm = new ORM();
            orm.add(Integer.class, "id", "eta_id");

            List<Bean> ids = trans.execQuery(Bean.class, orm, sb.toString());

            Bean etapaJson = new Bean();

            for (Bean idBean : ids) {

                Integer etapaId = idBean.get("id");
                EtapaBean etaBean = trans.selectById(EtapaBean.class, etapaId);

                WhereDB where = new WhereDB();
                where.add("Etapa.Id", WhereDB.Condition.EQUALS, etapaId);
                where.add("Situacao.Id", WhereDB.Condition.EQUALS, SitDD.CONCLUIDO);
                List<EtapaPremioBean> preList = trans.select(EtapaPremioBean.class,where);

                etapaJson.set("etapa_id", etaBean.getId());
                etapaJson.set("edicao", etaBean.getEdicao());
                etapaJson.set("descricao", etaBean.getDescricao());
                etapaJson.set("data", DateUtils.formatDate(etaBean.getDataSorteio()));

                Bean[] premioList = new Bean[preList.size()];
                etapaJson.set("premios", premioList);

                int cont = 0;
                for (EtapaPremioBean etapBean : preList) {
                    Bean premio = new Bean();
                    Bean ganhador = new Bean();

                    String numeros = etapBean.getBolas();
                    String[] numerosArr = numeros.split("-");

                    where = new WhereDB();
                    where.add("EtapaPremio.Id", WhereDB.Condition.EQUALS, etapBean.getId());
                    where.add("Ganhador", WhereDB.Condition.EQUALS, 1); // 1 = Venda premiada
                    VendaBean venBean = ((List<VendaBean>) trans.select(VendaBean.class, where)).get(0);
                    PessoaBean cliBean = trans.selectById(PessoaBean.class, venBean.getPessoaCliente().getId());

                    where = new WhereDB();
                    where.add("Pessoa.id", WhereDB.Condition.EQUALS, cliBean.getId());
                    PessoaEnderecoBean pese = ((List<PessoaEnderecoBean>) trans.select(PessoaEnderecoBean.class, where)).get(0);
                    CidadeBean cidBean = pese.getCidade();
                    EstadoBean estBean = trans.selectById(EstadoBean.class, cidBean.getEstado().getId());

                    premio.set("premio_descricao", etapBean.getOrdem() + "º Prêmio");
                    premio.set("premio", etapBean.getDescricao());
                    premio.set("numeros_chamados", numerosArr);
                    premio.set("ganhador", ganhador);
                    ganhador.set("cidade", cidBean.getDescricao());
                    ganhador.set("estado", estBean.getSigla());
                    ganhador.set("certificadonumero1", venBean.getCertificado().getNumero1());
                    //					if(etaBean.getTipoCertificado().equals(EtaDD.DUPLO)){
                    //						ganhador.set("certificadonumero2", venBean.getCertificado().getNumero2());
                    //					}
                    ganhador.set("nome", cliBean.getNomeRazao());
                    ganhador.set("vendedor", "Fixo");
                    premioList[cont] = premio;
                    cont++;
                }

            }

            listBean.add(etapaJson);

        }catch (NotFoundException e) {
        }

        Bean result = new Bean();
        result.set("list", listBean.toArray());

        return result;
    }

    public Bean encerrarPremio() throws Exception {

        ValidatorContract validator = new ValidatorContract();

        validator.isRequired(getBodyParameter("etapa_id"), "A etapa_id é requerida");
        validator.isRequired(getBodyParameter("ordem"), "A ordem é requerida");
        validator.isRequiredString((String)getBodyParameter("bolas"), "As bolas são requeridas");
        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        Integer etapaId = ((Long)getBodyParameter("etapa_id")).intValue();
        Integer ordem = ((Long)getBodyParameter("ordem")).intValue();
        String bolas = (String)getBodyParameter("bolas");

        Bean result = new Bean();

        TransactionDB trans = getTrans();

        EtapaBean etapaBean = null;
        try {
            etapaBean = trans.selectById(EtapaBean.class, etapaId);
        } catch (NotFoundException e) {
            throw new API400Exception("invalid_etapa", "O número da etapa informada não foi encontrado.");
        }

        WhereDB where = new WhereDB();
        where.add("Etapa.Id", WhereDB.Condition.EQUALS, etapaId);
        where.add("Ordem", WhereDB.Condition.EQUALS, ordem);
        List<EtapaPremioBean> list = trans.select(EtapaPremioBean.class, where);

        for (EtapaPremioBean b: list) {
            LOG.debug( b.getDescricao() + "/" + b.getOrdem() );
        }

        EtapaPremioBean etapBean = list.get(0);
        etapBean.setBolas(bolas);
        etapBean.getSituacao().setId(SitDD.CONCLUIDO);
        trans.update(etapBean);
        trans.commit();

        return result;
    }

    private static List<String> montarLinkImagem(TransactionDB trans, EtapaBean etaBean) throws SQLException, Exception {
        List<String> imagemUrlArray =  new ArrayList<String>();
        Integer numImg = CapitalService.doDataNumImagensPorEtapa(trans, etaBean.getId());
        UnidadeBean uniBean = etaBean.getUnidade();

        for(int i = 0; i < numImg; i++){
            imagemUrlArray.add(uniBean.getUrlBaseImage() + "/etapa_" + etaBean.getNumero() + "_" + (i+1) + ".jpg");
        }
        return imagemUrlArray;
    }
}
