package capital.api;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.net.util.Base64;

import br.com.dotum.jedi.core.exceptions.ErrorException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.DateUtils;

public class Jwt {
	
	private static final String KEY = "7187b9fd61603900250a55d23cfbee77";
	private static final String CHARSET = "UTF-8";
	
	public static void main(String[] args) throws Exception  {
		
		Date data = new Date();
		Date iet = data;
		Date exp = DateUtils.parseDateWithHour(data, "10:11:00");
		data.getHours();
		
		String xx = DateUtils.diffTime(iet, exp);
		int zz = DateUtils.hoursBetween(iet, exp);
		System.out.println(zz);
		 zz = DateUtils.minutesBetween(iet, exp);
		System.out.println(zz);
		zz = DateUtils.secundsBetween(iet, exp);
		System.out.println(zz);
		
		Bean body = new Bean();
		body.set("sub", "Dotum");
		body.set("iet", iet);
		body.set("exp", exp);
		body.set("pes_id", 64);
		
		String res  = null;
		try {
			res = Jwt.generate(body);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Bean b = Jwt.token2bean(res);
		
//		System.out.println(b.toJson2());
		
	}
	
	public static String generate(Bean header , Bean body) throws Exception {
		if(header == null) {
			header = new Bean();
			header.set("alg", "HS256");
		}
		
		String h = header.toJson2();
		String b = body.toJson2();
		
		String encodeHeader = base64encode(h.getBytes(CHARSET));
		String encodeBody = base64encode(b.getBytes(CHARSET));
		
		String concatenated = encodeHeader + "." + encodeBody;
		
		byte[] signature = hmacSha256(KEY, concatenated);
		
		String jwt = concatenated + "." + base64encode(signature);
		return jwt;
	}

	public static String generate(Bean body) throws Exception {
		return Jwt.generate(null, body);
	}
	
	public static String[] authToken(String token) throws Exception {
		String[] parts = token.split("\\.");
		String header64 = parts[0];
		String body64 = parts[1];
		String signature64 = parts[2];
		
		String concatenated = header64 + "." + body64;
		
		byte[] signature = hmacSha256(KEY, concatenated);
		
		if(base64encode(signature).equals(signature64)) {
			return new String[]{parts[0], parts[1]};
		}else {
			throw new WarningException("Token não reconhecido");
		}
		
	}
	
	public static Bean token2bean(String jwt) throws Exception {
		return token2bean(jwt, true);
	}
	public static Bean token2bean(String jwt, Boolean valid) throws Exception {
		String[] xx = null; 
		
		if(jwt == null) throw new WarningException("Token não encontrado");
		
		if(valid == true)
			xx = Jwt.authToken(jwt);
		else
			xx = jwt.split("\\.");
		
		String body = new String(Base64.decodeBase64(xx[1]),CHARSET);
		Bean b = new Bean();
		b.toObject(body);
		
		if(valid == true) {
			validData(b);
		}
		
		return b;
	}
	
	public static byte[] hmacSha256(String key, String data) throws Exception {
		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
		SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(CHARSET), "HmacSHA256");
		sha256_HMAC.init(secret_key);

		return Hex.encodeHexString(sha256_HMAC.doFinal(data.getBytes(CHARSET))).getBytes();
	}
	private static String base64encode(byte[] binaryData) throws UnsupportedEncodingException {
		
		byte[] xx = Base64.encodeBase64URLSafe(binaryData);
		return new String(xx, CHARSET);
	}
	
	private static Boolean validData(Bean b) throws ErrorException {
		
//		Long iatLong = b.get("iat");
//		if(iatLong == null) throw new ErrorException("iat não encontrado para validar expiração do token");
		Long expLong = b.get("exp");
		if(expLong == null) 
			throw new ErrorException("exp não encontrado para validar expiração do token");
		
//		Date iatDate = new Date(iatLong);
		Date expDate = new Date(expLong);
		Date nowDate = new Date();
		
		Integer xx = DateUtils.millisecundsBetween(nowDate, expDate);

		if(xx <= 0) {
			throw new ErrorException("token expirado");
			
		}
		
		return true;
	}
	
	public static String addExpDate() {
		return "";
	}
}
