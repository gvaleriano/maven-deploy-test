package capital.api;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.RequestBean;
import capital.service.FirebaseService;
import capital.service.MsgService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(name = "NotificacaoServlet")
public class NotificacaoServlet extends HttpServlet {

    private static Log LOG = LogFactory.getLog(NotificacaoServlet.class);

    private void setAccessControlHeaders(HttpServletResponse response) throws IOException {

        String app_mode = System.getenv("DIX_ENV");
        LOG.debug("DIX_ENV => "+app_mode);
        if (StringUtils.hasValue(app_mode) == false) {
            LOG.warn("A variável de ambiente DIX_ENV não foi definida!");
            throw new IOException("A variável de ambiente DIX_ENV não foi definida! ");
        }

        if (app_mode.equalsIgnoreCase("PRODUCTION")) {
            //response.addHeader("Access-Control-Allow-Origin", "loja.capitaldepremios.com.br");
            response.addHeader("Access-Control-Allow-Origin", "*");
        } else {
            response.addHeader("Access-Control-Allow-Origin", "*");
        }
        response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type");
        response.addHeader("Access-Control-Max-Age", "86400");

        LOG.debug( "setAccessControlHeaders invoke (finish)");
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setAccessControlHeaders(resp);
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doExecute("post", request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doExecute("get", request, response);
    }

    protected void doExecute(String method, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean version2 = false;
        TransactionDB trans = null;
        long start_time = System.currentTimeMillis();
        try {
            setAccessControlHeaders(response);

            LOG.info("Iniciou o servlet para o host "+request.getRemoteAddr());

            try {
                request.setCharacterEncoding("UTF-8");
                response.setContentType("application/json; charset=UTF-8");
            } catch (Exception ex) {
                JSONObject json = new JSONObject();
                json.put("success", false);
                json.put("message", "Nao foi possivel determinar ContentType para a resposta");
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print( json );
                return;
            }

            // imprimir parms request
            printQueryParameters(request);

            StringBuffer body = new StringBuffer();
            JSONObject jsonObject = null;
            try {
                // imprimir body request (once)
                String line = null;

                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null)
                    body.append(line);

                if ( StringUtils.hasValue(body.toString()) == true ) {
                    JSONParser parser = new JSONParser();
                    jsonObject = (JSONObject) parser.parse(body.toString());
                }
                LOG.debug("BODY> "+body.toString());

            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
                JSONObject json = new JSONObject();
                json.put("success", false);
                json.put("message", "Tem algum erro no envio dos parâmetros do BODY. Verifique! JSON => "+body.toString());
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print( json );
                return;
            }

            // se não for body
            if ((jsonObject == null) && ( method.equalsIgnoreCase("get") )) {
                jsonObject = new JSONObject();
                saveQueryParameters(request, jsonObject);
            }

            LOG.debug("BODY> "+jsonObject);

            trans = TransactionDB.getInstance(null);

            Bean ret = new Bean();
            ret.set("success", true);
            response.getWriter().print( ret.toJson2() );
        } catch (API400Exception ex1) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            ex1.printStackTrace();
            Bean result = new Bean();
            result.set("message", ex1.getMessage());
            result.set("error", "400");
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", ex1.getMessage());
            result2.set("result", result);
            try {
                if (version2) response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                LOG.error(exx.getMessage(), exx);
            }
        } catch (API401Exception ex1) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            ex1.printStackTrace();
            Bean result = new Bean();
            result.set("message", ex1.getMessage());
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", ex1.getMessage());
            result2.set("result", result);
            try {
                if (version2 == true) response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                LOG.error(exx.getMessage(), exx);
            }
        } catch (API403Exception ex401) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            ex401.printStackTrace();
            Bean result = new Bean();
            result.set("message", ex401.getMessage());
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", ex401.getMessage());
            result2.set("result", result);
            try {
                if (version2 == true) response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                LOG.error(exx.getMessage(), exx);
            }
        } catch (WarningException ex2) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            ex2.printStackTrace();
            Bean result = new Bean();
            result.set("message", ex2.getMessage());
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", ex2.getMessage());
            result2.set("result", result);
            try {
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                LOG.error(exx.getMessage(), exx);
            }
        } catch (Exception ex3) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            ex3.printStackTrace();
            Bean result = new Bean();
            result.set("message", "Erro inesperado no servidor.");
            result.set("internal_error", "Erro inesperado no ser3vidor.");
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", "Erro inesperado no servidor.");
            result2.set("internal_error", "Erro inesperado no servidor.");
            result2.set("result", result);
            try {
                if (version2 == true) response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                exx.printStackTrace();
            }
        } catch (Throwable th1) {

            try {
                trans.rollback();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            th1.printStackTrace();
            Bean result = new Bean();
            result.set("message", "Erro inesperado no servidor.");
            result.set("internal_error", "Erro inesperado no servidor.");
            Bean result2 = new Bean();
            result2.set("success", false);
            result2.set("message", "Erro inesperado no se4rvidor.");
            result2.set("internal_error", "Erro inesperado no servidor.");
            result2.set("result", result);
            try {
                if (version2 == true) response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print(result2.toJson2());
            } catch (Exception exx) {
                exx.printStackTrace();
            }
        } finally {
            try {
                trans.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void printQueryParameters(HttpServletRequest request) {
        LOG.debug("\nPARAMS>" );

        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            LOG.debug( paramName + " => ");
            String[] paramValues = request.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                LOG.debug( paramValue );
            }
            LOG.debug( "|" );
        }

    }

    private void saveQueryParameters(HttpServletRequest request, JSONObject jsonObject) {
        LOG.debug("\nPARAMS saving...>" );

        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            String[] paramValues = request.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                jsonObject.put( paramName, paramValue );
            }
            LOG.debug( "|" );
        }

    }
}
