package capital.api;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.ErrorException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.CarteiraBean;
import capital.model.bean.PessoaBean;
import capital.model.bean.RequestBean;
import capital.service.MsgService;
import com.payu.sdk.PayU;
import com.payu.sdk.PayUPayments;
import com.payu.sdk.PayUTokens;
import com.payu.sdk.exceptions.ConnectionException;
import com.payu.sdk.exceptions.InvalidParametersException;
import com.payu.sdk.exceptions.PayUException;
import com.payu.sdk.model.CreditCardToken;
import com.payu.sdk.model.Language;
import com.payu.sdk.model.PaymentCountry;
import com.payu.sdk.model.TransactionResponse;
import com.payu.sdk.utils.LoggerUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;

public class PayuService {

    public static final String NAME = "PAYULATAM";
    private static Log LOG = LogFactory.getLog(PayuService.class);
    private static Properties props;
    private static final String FILE_NAME= "/payu-config.properties";

    static {
        iniciaProps();
    }

    public static Properties  iniciaProps() {
        InputStream is = null;
        try {
            is = PropertiesUtils.class.getResourceAsStream(FILE_NAME);

            props = new Properties();
            if (is != null) props.load(is);

            LOG.debug("Carregando configuração da PayU...");
            PayU.merchantId = get("merchant_id");
            // agora setar na sdk da payu
            PayU.apiKey = get("api_key");
            LOG.debug("api_key => "+PayU.apiKey);
            PayU.apiLogin = get("api_login");
            LOG.debug("api_login => "+PayU.apiLogin);
            PayU.language = Language.pt;
            PayU.isTest = get("is_test").equalsIgnoreCase("true");
            LoggerUtil.setLogLevel(Level.ALL); //Incluí-lo somente se deseja ver todo o rastreio do log; se somente desejar ver a resposta, pode apagar.
            PayU.paymentsUrl = get("payments_url");
            PayU.reportsUrl = get("reports_url");
            LOG.debug("Carregando configuração da PayU (finish)");

        } catch (Exception e) {
            throw new WarningException(e.getMessage(), e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    throw new WarningException(e.getMessage(), e);
                }
            }
        }
        return props;
    }

    public final static String get(String propertie) {
        if ((props == null)) iniciaProps();
        return props.getProperty(propertie);
    }


    public static Bean inclusaoDeCartao(String serverPath, TransactionDB trans, PessoaBean pesBean,  HttpServletRequest request, CreditCardBean ccBean) throws Exception {

        Bean result = new Bean();
        String validade = ccBean.exp_year+"/"+ StringUtils.fillLeft( ccBean.exp_month+"", '0', 2 );
        if (validade.length() <= 4) {
            validade = "20"+validade;
        }
        String numero = StringUtils.getNumberOnly(ccBean.numero);

        if ((StringUtils.hasValue(numero) == false) || (numero.length() < 8)) {
            throw new API400Exception("numero_cartao_invalido","O número do cartão de crédito informado é inválido");
        }

        long start_time = System.currentTimeMillis();
        RequestBean gwRequestBean = new RequestBean();
        gwRequestBean.setData(trans.getDataAtual());
        gwRequestBean.setHora(trans.getHoraAtual());
        gwRequestBean.setEndpoint("criarToken");
        gwRequestBean.setHost( request.getLocalAddr() );

        // chamar SDK da PayU
        // -- Operação "Criar Token" --
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(PayU.PARAMETERS.PAYER_NAME, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.PAYER_ID, pesBean.getId().toString());
        parameters.put(PayU.PARAMETERS.PAYER_DNI, StringUtils.getNumberOnly(pesBean.getCpfCnpj()));
        parameters.put(PayU.PARAMETERS.CREDIT_CARD_NUMBER, numero);
        parameters.put(PayU.PARAMETERS.CREDIT_CARD_EXPIRATION_DATE, validade);
        parameters.put(PayU.PARAMETERS.PAYMENT_METHOD, ccBean.bandeira);
        CreditCardToken response = PayUTokens.create(parameters);
        String idCartao = null;
        if (response != null){
            idCartao = response.getTokenId();
            result.set("idCartao", idCartao);
            result.set("maskedNumber", CapitalService.esconderNumerosCartao(numero));
            //response.getPayerId();
            //response.getIdentificationNumber();
            //response.getPaymentMethod();
            gwRequestBean.setGatewaySucesso(0);
        } else {
            gwRequestBean.setGatewaySucesso(0);
        }

        long end_time = System.currentTimeMillis();
        gwRequestBean.getPessoaCliente().setId(pesBean.getId());
        gwRequestBean.setTime(new Long(end_time - start_time).intValue());
        gwRequestBean.setGateway(PayuService.NAME);
        gwRequestBean.setGatewayMensagem(response.getErrorDescription());

        LOG.debug("Inserindo LOG Gateway...");
        TransactionDB trans2 = TransactionDB.getInstance(null);
        trans2.insert(gwRequestBean);
        trans2.commit();
        trans2.close();
        LOG.debug("LOG Gateway (inserido)");

        if (idCartao.equals("") || idCartao == null) {
            throw new ErrorException(MsgService.get(serverPath, "msg.pinbankservice.inclusaocartao.01", "" + result.get("descricaoRetorno")));
        }
        return result;
    }

    public static Bean efetuarTransacaoWithCreditCard(TransactionDB trans, PessoaBean pesBean, CreditCardBean ccBean, Integer nsu, BigDecimal valor, HttpServletRequest request) throws Exception {

        BigDecimal novoValor = valor.multiply(new BigDecimal("100"));
        Integer valorInteiro = novoValor.intValue();

        String reference = nsu.toString();
        Bean result = new Bean();

        long start_time = System.currentTimeMillis();
        RequestBean gwRequestBean = new RequestBean();
        gwRequestBean.setData(trans.getDataAtual());
        gwRequestBean.setHora(trans.getHoraAtual());
        gwRequestBean.setEndpoint("efetuarTransacaoWithCreditCard");
        gwRequestBean.setHost( request.getLocalAddr() );

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put(PayU.PARAMETERS.ACCOUNT_ID, get("account_id"));
        parameters.put(PayU.PARAMETERS.REFERENCE_CODE, ""+reference);
        parameters.put(PayU.PARAMETERS.DESCRIPTION, "Compra");
        parameters.put(PayU.PARAMETERS.LANGUAGE, "Language.pt");
        parameters.put(PayU.PARAMETERS.VALUE, ""+valorInteiro);
        parameters.put(PayU.PARAMETERS.CURRENCY, "BRL");
        parameters.put(PayU.PARAMETERS.BUYER_ID, pesBean.getId()+"");
        parameters.put(PayU.PARAMETERS.BUYER_NAME, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.BUYER_EMAIL, pesBean.getEmail());
        parameters.put(PayU.PARAMETERS.BUYER_CONTACT_PHONE, pesBean.getTelefone1());
        parameters.put(PayU.PARAMETERS.BUYER_DNI, StringUtils.getNumberOnly( pesBean.getCpfCnpj() ));

        //Coloque aqui o endereço do comprador.
//        parameters.put(PayU.PARAMETERS.BUYER_STREET, "calle 100");
//        parameters.put(PayU.PARAMETERS.BUYER_STREET_2, "5555487");
//        parameters.put(PayU.PARAMETERS.BUYER_CITY, "Sao paulo");
//        parameters.put(PayU.PARAMETERS.BUYER_STATE, "SP");
//        parameters.put(PayU.PARAMETERS.BUYER_COUNTRY, "BR");
//        parameters.put(PayU.PARAMETERS.BUYER_POSTAL_CODE, "01019-030");
//        parameters.put(PayU.PARAMETERS.BUYER_PHONE, "(11)756312633");

        // -- Pagador --
        //Coloque aqui o nome do pagador.
        parameters.put(PayU.PARAMETERS.PAYER_NAME, ccBean.nome);

        // -- Dados do cartão de crédito --
        parameters.put(PayU.PARAMETERS.CREDIT_CARD_NUMBER, StringUtils.getNumberOnly(ccBean.numero));
        parameters.put(PayU.PARAMETERS.CREDIT_CARD_EXPIRATION_DATE, "2014/12");
        parameters.put(PayU.PARAMETERS.CREDIT_CARD_SECURITY_CODE, ccBean.cvv+"");
        parameters.put(PayU.PARAMETERS.PAYMENT_METHOD, ccBean.bandeira);
        parameters.put(PayU.PARAMETERS.INSTALLMENTS_NUMBER, "1");
        parameters.put(PayU.PARAMETERS.COUNTRY, PaymentCountry.BR.name());
        parameters.put(PayU.PARAMETERS.IP_ADDRESS, request.getRemoteAddr());

        // "Authorization and capture" request
        TransactionResponse response = PayUPayments.doAuthorizationAndCapture(parameters);

        long end_time = System.currentTimeMillis();
        gwRequestBean.getPessoaCliente().setId(pesBean.getId());
        gwRequestBean.setTime(new Long(end_time - start_time).intValue());
        gwRequestBean.setGateway(PayuService.NAME);
        gwRequestBean.setGatewaySucesso(0);

        LOG.debug("Inserindo LOG Gateway...");
        TransactionDB trans2 = TransactionDB.getInstance(null);
        trans2.insert(gwRequestBean);
        trans2.commit();
        trans2.close();
        LOG.debug("LOG Gateway (inserido)");

        LOG.debug( "order_id => "+response.getOrderId() );
        LOG.debug( "transaction_id => "+response.getTransactionId() );
        LOG.debug( "response_code => "+response.getTransactionId() );
        LOG.debug( "response_message => "+response.getTransactionId() );
        LOG.debug( "error_code => "+response.getErrorCode() );
        LOG.debug( "error_message => "+response.getPendingReason() );

        result.set("nsu_gw", response.getTransactionId());

        // resposta
        if (response != null){
            result.set("order_id", response.getOrderId());
            result.set("transaction_id", response.getTransactionId());
            result.set("response_code", response.getResponseCode());
            result.set("response_message", response.getResponseMessage());
            result.set("error_code", response.getErrorCode());
            result.set("error_message", response.getPendingReason());
            if(response.getState().toString().equalsIgnoreCase("PENDING")){
                response.getPendingReason();
            }
        }

        return result;
    }

    public static Bean efetuarTransacao(TransactionDB trans, PessoaBean pesBean, BigDecimal valor, HttpServletRequest request, Integer nsu, CarteiraBean carBean) throws Exception {

        LOG.debug("PayuLatam (efetuarTransacao) starting ");

        BigDecimal novoValor = valor.multiply(new BigDecimal("100"));
        Integer valorInteiro = novoValor.intValue();

        String reference = nsu.toString();
        Bean result = new Bean();

        long start_time = System.currentTimeMillis();
        RequestBean gwRequestBean = new RequestBean();
        gwRequestBean.setData(trans.getDataAtual());
        gwRequestBean.setHora(trans.getHoraAtual());
        gwRequestBean.setEndpoint("efetuarTransacao");
        gwRequestBean.setHost( request.getLocalAddr() );

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put(PayU.PARAMETERS.ACCOUNT_ID, get("account_id"));
        parameters.put(PayU.PARAMETERS.REFERENCE_CODE, ""+reference);
        parameters.put(PayU.PARAMETERS.DESCRIPTION, "Compra");
        parameters.put(PayU.PARAMETERS.LANGUAGE, "Language.pt");
        //parameters.put(PayU.PARAMETERS.VALUE, ""+valorInteiro);
        parameters.put(PayU.PARAMETERS.VALUE, ""+valorInteiro);
        parameters.put(PayU.PARAMETERS.CURRENCY, "BRL");
        parameters.put(PayU.PARAMETERS.BUYER_ID, pesBean.getId()+"");
        parameters.put(PayU.PARAMETERS.BUYER_NAME, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.BUYER_EMAIL, pesBean.getEmail());
        parameters.put(PayU.PARAMETERS.BUYER_CONTACT_PHONE, pesBean.getTelefone1());
        parameters.put(PayU.PARAMETERS.BUYER_DNI, StringUtils.getNumberOnly( pesBean.getCpfCnpj() ));

        //Coloque aqui o endereço do comprador.
        parameters.put(PayU.PARAMETERS.BUYER_STREET, "calle 100");
        parameters.put(PayU.PARAMETERS.BUYER_STREET_2, "5555487");
        parameters.put(PayU.PARAMETERS.BUYER_CITY, "Sao paulo");
        parameters.put(PayU.PARAMETERS.BUYER_STATE, "SP");
        parameters.put(PayU.PARAMETERS.BUYER_COUNTRY, "BR");
        parameters.put(PayU.PARAMETERS.BUYER_POSTAL_CODE, "01019-030");
        parameters.put(PayU.PARAMETERS.BUYER_PHONE, "(11)756312633");

        parameters.put(PayU.PARAMETERS.PAYER_NAME, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.INSTALLMENTS_NUMBER, "1");
        parameters.put(PayU.PARAMETERS.TOKEN_ID, carBean.getTokenPayU());
        parameters.put(PayU.PARAMETERS.IP_ADDRESS, request.getRemoteAddr());
        parameters.put(PayU.PARAMETERS.PAYMENT_METHOD, carBean.getBandeira() );

        // "Authorization and capture" request
        LOG.debug("PayuLatam PayUPayments.doAuthorizationAndCapture... ");

        int http_status = 0;
        boolean venda_realizada = false;
        try {
            TransactionResponse response = PayUPayments.doAuthorizationAndCapture(parameters);

            LOG.debug( "getAuthorizationCode => "+response.getAuthorizationCode() );
            LOG.debug( "getPaymentNetworkResponseCode => "+response.getPaymentNetworkResponseCode() );
            LOG.debug( "order_id => "+response.getOrderId() );
            LOG.debug( "transaction_id => "+response.getTransactionId() );
            LOG.debug( "response_code => "+response.getResponseCode() );
            LOG.debug( "pending_reason => "+response.getPendingReason() );
            LOG.debug( "error_code => "+response.getErrorCode() );
            LOG.debug( "error_message => "+response.getPendingReason() );

            result.set("nsu_gw", response.getTransactionId());

            // resposta
            if (response != null){
                result.set("order_id", response.getOrderId());
                result.set("transaction_id", response.getTransactionId());
                result.set("response_code", response.getResponseCode());
                result.set("response_message", response.getResponseMessage());
                result.set("error_code", response.getErrorCode());
                result.set("error_message", response.getPendingReason());

                // venda só foi realizar quando STATE = APPROVED
                if (response.getState().toString().equalsIgnoreCase("APPROVED")){
                    venda_realizada = true;
                }
            }


        } catch (Exception ex) {
            gwRequestBean.setResponse(ex.getMessage());
            LOG.error(ex.getMessage());
        }
        LOG.debug("PayuLatam PayUPayments.doAuthorizationAndCapture (done) ");

        long end_time = System.currentTimeMillis();
        gwRequestBean.getPessoaCliente().setId(pesBean.getId());
        gwRequestBean.setTime(new Long(end_time - start_time).intValue());
        gwRequestBean.setGateway(PayuService.NAME);
        gwRequestBean.setGatewaySucesso(0);
        gwRequestBean.setHttpStatus(http_status);

        LOG.debug("Inserindo LOG Gateway...");
        TransactionDB trans2 = TransactionDB.getInstance(null);
        trans2.insert(gwRequestBean);
        trans2.commit();
        trans2.close();
        LOG.debug("LOG Gateway (inserido)");

        if (venda_realizada == false)
            throw new API400Exception("banco_reprovou", "Banco não autorizou esta transação");

        return result;
    }

    public static Bean exclusaoDeCartao(String serverPath, TransactionDB trans, PessoaBean pesBean, String token, HttpServletRequest request) throws Exception {

        Bean result = new Bean();
        long start_time = System.currentTimeMillis();
        RequestBean gwRequestBean = new RequestBean();
        gwRequestBean.setData(trans.getDataAtual());
        gwRequestBean.setHora(trans.getHoraAtual());
        gwRequestBean.setEndpoint("excluirToken");
        gwRequestBean.setHost( request.getLocalAddr() );

        // -- Operação "Eliminar token" --
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(PayU.PARAMETERS.PAYER_ID, pesBean.getNomeRazao());
        parameters.put(PayU.PARAMETERS.TOKEN_ID, token);
        CreditCardToken response = PayUTokens.remove(parameters);
        LoggerUtil.info("{0}", response);

        long end_time = System.currentTimeMillis();
        gwRequestBean.getPessoaCliente().setId(pesBean.getId());
        gwRequestBean.setTime(new Long(end_time - start_time).intValue());
        gwRequestBean.setGateway(PayuService.NAME);
        gwRequestBean.setGatewaySucesso(0);

        LOG.debug("Inserindo LOG Gateway...");
        TransactionDB trans2 = TransactionDB.getInstance(null);
        trans2.insert(gwRequestBean);
        trans2.commit();
        trans2.close();
        LOG.debug("LOG Gateway (inserido)");

        return result;
    }

    public static void listarCartoes() throws Exception {

        // -- Operação "Consultar token" --
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(PayU.PARAMETERS.PAYER_ID, "64626");
        parameters.put(PayU.PARAMETERS.TOKEN_ID, "cd9b536d-4952-41ab-bb80-660339b1c080");
        parameters.put(PayU.PARAMETERS.START_DATE, "2010-01-01T12:00:00");
        parameters.put(PayU.PARAMETERS.END_DATE, "2020-01-01T12:00:00");

        List<CreditCardToken> response = null;
        try {
            response = PayUTokens.find(parameters);
        } catch (PayUException e) {
            e.printStackTrace();
        } catch (InvalidParametersException e) {
            e.printStackTrace();
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
        Iterator<CreditCardToken> tokens_iterator = response.iterator();

    }
}