package capital.api;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.db.WhereDB;
import br.com.dotum.jedi.core.exceptions.NotFoundException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.security.SecurityUtil;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.NumberUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.dd.PesetDD;
import capital.model.dd.PestDD;
import capital.model.dd.UniDD;
import capital.service.EmailService;
import capital.service.MsgService;
import capital.service.SMSService;
import capital.model.bean.CidadeBean;
import capital.model.bean.PessoaBean;
import capital.model.bean.PessoaEnderecoBean;
import capital.model.bean.PessoaTipoPessoaBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PessoaAction extends AbstractAction {

    private static Log LOG = LogFactory.getLog(PessoaAction.class);

    public PessoaAction(HttpServletRequest request, HttpServletResponse response, TransactionDB trans, JSONObject body, String pathServer) {
        super(request, response, trans, body, pathServer);
    }

    public Bean login() throws Exception {

        TransactionDB trans = getTrans();

        String cpf = (String)getBodyParameter("cpf");
        String senha = (String)getBodyParameter("senha");
        String versao = (String)getBodyParameter("versao_app");

        // se versão for ios, vem sem cifrar então enho que criptografar
        if (versao.startsWith("ios-")) {
            senha = SecurityUtil.encryptMD5(senha);
        }

        LOG.info("Usuário "+cpf+" efetuando login...");

        String tokenAuth = CapitalService.generateTokenOnLogin( getPathServer(), trans, cpf, senha);
        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);
        PessoaEnderecoBean peseBean = CapitalService.getEnderecoPessoa(trans, pesBean.getId());

        // Carteira
        List<Bean> carList = CapitalService.doDataCarteira(trans, pesBean);

        Bean result = new Bean();
        result.set("status", "success");
        result.set("token_auth", tokenAuth);

        Bean perfilBean = new Bean();
        perfilBean.set("id", null);
        perfilBean.set("nome", pesBean.getNomeRazao());
        perfilBean.set("cpf", pesBean.getCpfCnpj());
        perfilBean.set("data_nascimento", DateUtils.formatDate(pesBean.getNascimentoFundacao()));
        perfilBean.set("email", pesBean.getEmail());
        perfilBean.set("telefone", pesBean.getTelefone1());
        perfilBean.set("endereco", peseBean.getLogradouro());
        perfilBean.set("bairro", peseBean.getBairro());
        perfilBean.set("endereco_numero", peseBean.getNumero());
        perfilBean.set("cidade", peseBean.getCidade().getDescricao());
        perfilBean.set("cidade_ibge", peseBean.getCidade().getIbge());
        perfilBean.set("estado", peseBean.getCidade().getEstado().getSigla());
        perfilBean.set("cep", peseBean.getCep());
        perfilBean.set("carteiras", carList.toArray());
        result.set("perfil", perfilBean);

        return result;
    }

    public Bean loginWithFacebook() throws Exception {

        TransactionDB trans = getTrans();

        String facebook_token = (String)getBodyParameter("facebook_token");
        String email = (String)getBodyParameter("email");

        LOG.info("Usuário "+facebook_token+" efetuando login with facebook...");

        String URL = "https://graph.facebook.com/me?access_token="+facebook_token+"&fields=email,name";
        org.json.JSONObject response = APIConnection.get(URL);
        String facebook_email = response.getString("email");

        LOG.debug( "Response>" + response.toString() );
        LOG.debug( "email> " + facebook_email );

        if (facebook_email == null)
            throw new API401Exception("email_invalid","Login inválido");

        if (facebook_email.equalsIgnoreCase(email) == false)
            throw new API401Exception("email_invalid","Login inválido");

        Bean result = new Bean();
        try {
            WhereDB where = new WhereDB();
            where.add("Email", WhereDB.Condition.EQUALS, email);
            List<PessoaBean> lista = trans.select(PessoaBean.class, where);
            PessoaBean pesBean = lista.get(0);

            String tokenAuth = CapitalService.generateTokenOnLogin( getPathServer(), trans, pesBean.getCpfCnpj(), pesBean.getSenha());
            PessoaEnderecoBean peseBean = CapitalService.getEnderecoPessoa(trans, pesBean.getId());

            // Carteira
            List<Bean> carList = CapitalService.doDataCarteira(trans, pesBean);

            result.set("status", "success");
            result.set("token_auth", tokenAuth);

            Bean perfilBean = new Bean();
            perfilBean.set("id", null);
            perfilBean.set("nome", pesBean.getNomeRazao());
            perfilBean.set("cpf", pesBean.getCpfCnpj());
            perfilBean.set("data_nascimento", DateUtils.formatDate(pesBean.getNascimentoFundacao()));
            perfilBean.set("email", pesBean.getEmail());
            perfilBean.set("telefone", pesBean.getTelefone1());
            perfilBean.set("endereco", peseBean.getLogradouro());
            perfilBean.set("bairro", peseBean.getBairro());
            perfilBean.set("endereco_numero", peseBean.getNumero());
            perfilBean.set("cidade", peseBean.getCidade().getDescricao());
            perfilBean.set("cidade_ibge", peseBean.getCidade().getIbge());
            perfilBean.set("estado", peseBean.getCidade().getEstado().getSigla());
            perfilBean.set("cep", peseBean.getCep());
            perfilBean.set("carteiras", carList.toArray());
            result.set("perfil", perfilBean);
            return result;

        } catch (NotFoundException ex) {
            throw new API401Exception("email_invalid","Login inválido");
        }

    }

    public Bean meusDados() throws Exception{

        TransactionDB trans = getTrans();

        //
        // pegando os parametros
        //
        String tokenAuth = (String)getBodyParameter("token_auth");

        //
        // busca os dados
        //
        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);
        PessoaEnderecoBean peseBean = CapitalService.getEnderecoPessoa(trans, pesBean.getId());
        List<Bean> carList = CapitalService.doDataCarteira(trans, pesBean);

        Bean perfilBean = new Bean();
        perfilBean.set("id", null);
        perfilBean.set("nome", pesBean.getNomeRazao());
        perfilBean.set("cpf", pesBean.getCpfCnpj());
        perfilBean.set("data_nascimento", DateUtils.formatDate(pesBean.getNascimentoFundacao()));
        perfilBean.set("email", pesBean.getEmail());
        perfilBean.set("telefone", pesBean.getTelefone1());
        perfilBean.set("endereco", peseBean.getLogradouro());
        perfilBean.set("bairro", peseBean.getBairro());
        perfilBean.set("endereco_numero", peseBean.getNumero());
        perfilBean.set("cidade", peseBean.getCidade().getDescricao());
        perfilBean.set("cidade_ibge", peseBean.getCidade().getIbge());
        perfilBean.set("estado", peseBean.getCidade().getEstado().getSigla());
        perfilBean.set("cep", peseBean.getCep());
        perfilBean.set("carteiras", carList.toArray());

        perfilBean.set("token_auth", CapitalService.newTokenSession(getResponse(), getPathServer(), trans, tokenAuth));
        perfilBean.set("status", "success");

        return perfilBean;
    }

    public Bean cadastro() throws Exception {

        LOG.debug("Cadastro...");

        ValidatorContract validator = new ValidatorContract();

        Boolean facebook  = (Boolean)getBodyParameter("facebook");
        if (facebook == null) facebook = false;

        String nome = (String)getBodyParameter("nome");
        validator.isRequiredString(nome, "O nome é um campo requerido para o cadastro");

        String email = (String)getBodyParameter("email");
        validator.isRequiredString(email, "O email é um campo requerido para o cadastro");

        String cpf = (String)getBodyParameter("cpf");
        cpf = StringUtils.getNumberOnly(cpf);
        validator.isRequiredString(cpf, "O CPF é um campo requerido para o cadastro");
        validator.isValidCpf(cpf, "O CPF informado é inválido");

        String dataNascimentoStr = (String)getBodyParameter("data_nascimento");
        Date dataNascimento = null;

        if (dataNascimentoStr != null) {
            validator.isValidDate(dataNascimentoStr, "dd/MM/yyyy", "A data de nascimento ("+dataNascimentoStr+") está em um formato inválida. Ex: dd/mm/yyyy");
        }

        String telefone = (String)getBodyParameter("telefone");
        telefone = StringUtils.getNumberOnly(telefone);
        validator.isRequiredString(telefone, "O telefone é um campo requerido para o cadastro");
        validator.hasLenght(telefone, 11, "O telefone informado ("+telefone+") está num formato inválido, precisa ter exatamente 11 números. Ex: (99)99999-9999");

        // se não é um cadastro pelo facebook, a senha é obrigatória
        String senha = null;
        if (facebook == false) {
            senha = (String) getBodyParameter("senha");
            validator.isRequiredString(senha, "A senha é um campo requerido para o cadastro");
        } else {
            senha = "facebook";
        }

        String cep = (String)getBodyParameter("cep");
        cep = StringUtils.getNumberOnly(cep);
        validator.isRequiredString(cep, "O cep é um campo requerido para o cadastro");
        validator.hasLenght(cep, 8, "O cep informado ("+cep+") está num formato inválido, precisa ter exatamente 8 números. Ex: 999999-999");

        String endereco = (String)getBodyParameter("endereco");
        validator.isRequiredString(endereco, "O endereco é um campo requerido para o cadastro");
        String enderecoNumero = (String)getBodyParameter("endereco_numero");
        String cidadeIbge = (String)getBodyParameter("codigo_ibge");
        validator.isRequiredString(cidadeIbge, "O cidadeIbge é um campo requerido para o cadastro");
        String bairro = (String)getBodyParameter("bairro");
        String sexo = (String)getBodyParameter("sexo");

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();
        /////
        Bean b = new Bean();
        cep = StringUtils.getNumberOnly(cep);
        cpf = StringUtils.getNumberOnly(cpf);
        cidadeIbge = StringUtils.getNumberOnly(cidadeIbge);

        try {
            LOG.debug("Validando Cadastro...");
            CapitalService.validaPessoaCpfCnpjUnico(getPathServer(), trans, null, cpf);
            CapitalService.validaPessoaEmailUnico(getPathServer(), trans, null, email);
            CapitalService.validaPessoaTelefoneUnico(getPathServer(), trans, null, telefone);
            LOG.debug("Cadastro válido!");
        } catch (WarningException ex) {
            throw new API400Exception("duplicate", ex.getMessage());
        }

        //
        // PESSOA
        //
        PessoaBean pesBean = new PessoaBean();
        pesBean.getUnidade().setId(UniDD.PRINCIPAL);
        pesBean.setNomeRazao(nome);
        pesBean.setEmail(email);
        pesBean.setTelefone1(StringUtils.getNumberOnly(telefone));
        pesBean.setCpfCnpj(cpf);
        pesBean.setSenha(SecurityUtil.encryptMD5(senha));

        // smart Nascimento
        if (dataNascimento == null) {
            dataNascimento = CapitalService.getDataRange(18, 65);
        }
        pesBean.setNascimentoFundacao(dataNascimento);

        // smartSexo
        if (StringUtils.hasValue(sexo) == false) {
            sexo = (nome.toLowerCase().substring((nome.length())).equals("a"))? "F" :"M";
        }
        pesBean.setSexo(sexo);

        // smart RG
        String rg = "" + NumberUtils.random(7);
        String emissorRg = "SSP";
        pesBean.setRgInscricao(rg + CapitalService.SEPARADOR_RG + emissorRg);
        LOG.debug("Inserindo Pessoa...");
        trans.insert(pesBean);
        LOG.debug("Pessoa inserida!");

        //
        // PESSOA ENDERECO
        //
        CidadeBean cidBean = null;
        PessoaEnderecoBean peseBean = new PessoaEnderecoBean();

        try {
            LOG.debug("Buscando cidade pelo código do IBGE ["+cidadeIbge+"]");
            WhereDB where = new WhereDB();
            where.add("Ibge", WhereDB.Condition.EQUALS, cidadeIbge);
            List<CidadeBean> cidList = trans.select(CidadeBean.class, where);
            cidBean = cidList.get(0);
        } catch (NotFoundException e) {
            LOG.error("Tentativa de cadastrar a cidade "+cidadeIbge+" que não foi encontrada na base de ceps");
            LOG.error("CEP: "+cep);
            throw new API400Exception("ibge_invalid", "O código do IBGE informado não foi encontrado");
        }
        peseBean.setCep(cep);
        peseBean.setCidade(cidBean);
        peseBean.setPessoa(pesBean);
        peseBean.getPessoaEnderecoTipo().setId(PesetDD.RESIDENCIAL);
        peseBean.setLogradouro(endereco);
        peseBean.setNumero(enderecoNumero);
        peseBean.setBairro(StringUtils.hasValue(bairro) ? bairro : "centro");
        LOG.debug("Inserindo PessoaEndereco...");
        trans.insert(peseBean);
        LOG.debug("PessoaEndereco inserido!");

        //
        // Pessoa tipo pessoa : VINCULO
        //
        LOG.debug("Inserindo PessoaTipoPessoa...");
        PessoaTipoPessoaBean ptp = new PessoaTipoPessoaBean();
        ptp.setPessoa(pesBean);
        ptp.getPessoaTipo().setId(PestDD.MOBILE);
        trans.insert(ptp);
        LOG.debug("PessoaTipoPessoa inserido!");

        // TODO: tirar o cadastro do gateway de pagamento daqui, isso faz com que percamos cadastros
        LOG.debug("Cadastro na PinBank...");
        //CapitalService.cadastroClientePinbank(trans, pesBean, peseBean, cidBean, super.getRequest());
        LOG.debug("Cadastro na PinBank inserido!");

        LOG.debug("Gerando token...");
        String tokenAuth = CapitalService.generateTokenOnLogin( getPathServer(), trans, pesBean.getCpfCnpj(), pesBean.getSenha());
        b.set("token_auth", tokenAuth);
        LOG.debug("Token Gerado!");

        trans.commit();

        return b;
    }

    public Bean atualiza() throws Exception {

        String tokenAuth = (String)getBodyParameter("token_auth");

        String nome = (String)getBodyParameter("nome");
        String data_nascimentoStr = (String)getBodyParameter("data_nascimento");
        Date dataNascimento = null;
        if (data_nascimentoStr != null) {
            try {
                dataNascimento = DateUtils.parseDate(data_nascimentoStr, "dd/MM/yyyy");
            } catch (Exception ex ) { }
        }

        String telefone = (String)getBodyParameter("telefone");
        telefone = StringUtils.getNumberOnly(telefone);
        String email = (String)getBodyParameter("email");
        String cep = (String)getBodyParameter("cep");
        cep = StringUtils.getNumberOnly(cep);
        String endereco = (String)getBodyParameter("endereco");
        String enderecoNumero = (String)getBodyParameter("endereco_numero");
        String cidadeIbge = (String)getBodyParameter("codigo_ibge");
        cidadeIbge = StringUtils.getNumberOnly(cidadeIbge);
        String bairro = (String)getBodyParameter("bairro");
        String senha = (String)getBodyParameter("senha");

        TransactionDB trans = getTrans();

        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        CapitalService.validaPessoaEmailUnico(getPathServer(), trans, pesBean.getId(), email);
        CapitalService.validaPessoaTelefoneUnico(getPathServer(), trans, pesBean.getId(), telefone);

        if (StringUtils.hasValue(nome)) pesBean.setNomeRazao(nome);
        if (StringUtils.hasValue(email)) pesBean.setEmail(email);
        if (StringUtils.hasValue(telefone)) pesBean.setTelefone1(telefone);
        if (StringUtils.hasValue(senha)) pesBean.setSenha(senha);
        if (StringUtils.hasValue(DateUtils.formatDate(dataNascimento))) pesBean.setNascimentoFundacao(dataNascimento);
        trans.update(pesBean);

        PessoaEnderecoBean peseBean = null;
        WhereDB where;
        try {
            where = new WhereDB();
            where.add("Pessoa.Id", WhereDB.Condition.EQUALS, pesBean.getId());
            where.add("PessoaEnderecoTipo.Id", WhereDB.Condition.EQUALS, PesetDD.RESIDENCIAL);
            List<PessoaEnderecoBean> peseList = trans.select(PessoaEnderecoBean.class, where);
            peseBean = peseList.get(0);

        } catch (NotFoundException e) {
            peseBean = new PessoaEnderecoBean();
        }

        CidadeBean cidBean = null;
        if((cidadeIbge != null)) {
            try {
                where = new WhereDB();
                where.add("Ibge", WhereDB.Condition.EQUALS, cidadeIbge);
                List<CidadeBean> cidList = trans.select(CidadeBean.class, where);
                cidBean = cidList.get(0);
            } catch (Exception e) {
                throw new Exception(MsgService.get(getPathServer(), "msg.005.atualizacadastro.01"));
            }
            peseBean.setCep(cep);
            peseBean.setCidade(cidBean);
        }

        peseBean.setPessoa(pesBean);
        peseBean.getPessoaEnderecoTipo().setId(PesetDD.RESIDENCIAL);
        peseBean.setLogradouro(endereco);
        peseBean.setNumero(enderecoNumero);
        peseBean.setBairro(StringUtils.hasValue(bairro) == false ? "Centro" : bairro);
        peseBean.setCep(cep);
        trans.merge(peseBean);

        Bean result = new Bean();

        try {
            result.set("auth_token", CapitalService.newTokenSession(getResponse(), getPathServer(), trans, tokenAuth));
        } catch (Exception e) {
            result = new Bean();
            result.set("auth_token", tokenAuth);
            result.set("message", "Erro ao revalidar token, varifique a expiração do token.");
        }

        trans.commit();

        return result;
    }

    public Bean atualiza2() throws Exception {

        ValidatorContract validator = new ValidatorContract();

        String tokenAuth = (String)getBodyParameter("token_auth");
        validator.isRequiredString(tokenAuth, "o token_auth é um campo obrigatório");

        String fields = (String)getBodyParameter("fields");
        validator.isRequiredString(fields, "o fields é um campo obrigatório");

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        String[] parts;
        try {
            parts = fields.split(",");
            LOG.debug("Fields para atualizar => " + StringUtils.arrayToString(parts));
        } catch (Exception ex) {
            throw new API400Exception("validation", validator.getMessages());
        }

        String nome = null;
        if ( StringUtils.search("nome", parts) ) {
            nome = (String)getBodyParameter("nome");
            validator.isRequiredString(nome, "O campo nome é um campo obrigatório");
        }

        String email = null;
        if ( StringUtils.search("email", parts) ) {
            email = (String)getBodyParameter("email");
            validator.isRequiredString(email, "O campo email é um campo obrigatório");
        }

        String telefone = null;
        if ( StringUtils.search("telefone", parts) ) {
            telefone = (String)getBodyParameter("telefone");
            telefone = StringUtils.getNumberOnly(telefone);
            validator.isRequiredString(telefone, "O campo telefone é um campo obrigatório");
        }

        String cep = null;
        if ( StringUtils.search("cep", parts) ) {
            cep = (String)getBodyParameter("cep");
            validator.isRequiredString(cep, "O campo cep é um campo obrigatório");
        }

        String cidade_ibge = null;
        if ( StringUtils.search("cidade_ibge", parts) ) {
            cidade_ibge = (String)getBodyParameter("cidade_ibge");
            cidade_ibge = StringUtils.getNumberOnly(cidade_ibge);
            validator.isRequiredString(cidade_ibge, "O campo cidade_ibge é um campo obrigatório");
        }

        String endereco = null;
        if ( StringUtils.search("endereco", parts) ) {
            endereco = (String)getBodyParameter("endereco");
            validator.isRequiredString(endereco, "O campo endereco é um campo obrigatório");
        }

        String endereco_numero = null;
        if ( StringUtils.search("endereco_numero", parts) ) {
            endereco_numero = (String)getBodyParameter("endereco_numero");
            validator.isRequiredString(endereco_numero, "O campo endereco_numero é um campo obrigatório");
        }

        String bairro = null;
        if ( StringUtils.search("bairro", parts) ) {
            bairro = (String)getBodyParameter("bairro");
            validator.isRequiredString(bairro, "O campo bairro é um campo obrigatório");
        }

        String senha = null;
        if ( StringUtils.search("senha", parts) ) {
            senha = (String)getBodyParameter("senha");
            validator.isRequiredString(senha, "O campo senha é um campo obrigatório");
        }

        Date dataNascimento = null;
        if ( StringUtils.search("data_nascimento", parts) ) {
            String data_nascimentoStr = (String)getBodyParameter("data_nascimento");
            LOG.debug("Data de nascimento => "+data_nascimentoStr);
            validator.isRequiredString(data_nascimentoStr, "O campo data_nascimento é um campo obrigatório");
            validator.isValid( isValidDate(data_nascimentoStr, "dd/MM/yyyy"), "O campo data_nascimento está num formato inválido. Formato aceito dd/mm/yyyy");

            if (!validator.isValid()) {
                throw new API400Exception("validation", validator.getMessages());
            }

            if (data_nascimentoStr != null) {
                dataNascimento = DateUtils.parseDate(data_nascimentoStr, "dd/MM/yyyy");
            }
        }

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();

        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        CapitalService.validaPessoaEmailUnico(getPathServer(), trans, pesBean.getId(), email);
        CapitalService.validaPessoaTelefoneUnico(getPathServer(), trans, pesBean.getId(), telefone);

        if (StringUtils.hasValue(nome)) pesBean.setNomeRazao(nome);
        if (StringUtils.hasValue(email)) pesBean.setEmail(email);
        if (StringUtils.hasValue(telefone)) pesBean.setTelefone1(telefone);
        if (StringUtils.hasValue(senha)) pesBean.setSenha(senha);
        if ( dataNascimento != null ) pesBean.setNascimentoFundacao(dataNascimento);
        trans.update(pesBean);

        PessoaEnderecoBean peseBean = null;
        WhereDB where;
        try {
            where = new WhereDB();
            where.add("Pessoa.Id", WhereDB.Condition.EQUALS, pesBean.getId());
            where.add("PessoaEnderecoTipo.Id", WhereDB.Condition.EQUALS, PesetDD.RESIDENCIAL);
            List<PessoaEnderecoBean> peseList = trans.select(PessoaEnderecoBean.class, where);
            peseBean = peseList.get(0);

        } catch (NotFoundException e) {
            peseBean = new PessoaEnderecoBean();
        }

        CidadeBean cidBean = null;
        if((cidade_ibge != null)) {
            try {
                where = new WhereDB();
                where.add("Ibge", WhereDB.Condition.EQUALS, cidade_ibge);
                List<CidadeBean> cidList = trans.select(CidadeBean.class, where);
                cidBean = cidList.get(0);
            } catch (Exception e) {
                throw new Exception(MsgService.get(getPathServer(), "msg.005.atualizacadastro.01"));
            }
            peseBean.setCep(cep);
            peseBean.setCidade(cidBean);
        }

        peseBean.setPessoa(pesBean);
        peseBean.getPessoaEnderecoTipo().setId(PesetDD.RESIDENCIAL);
        peseBean.setLogradouro(endereco);
        peseBean.setNumero(endereco_numero);
        peseBean.setBairro(StringUtils.hasValue(bairro) == false ? "Centro" : bairro);
        peseBean.setCep(cep);
        trans.merge(peseBean);

        Bean result = new Bean();

        try {
            result.set("auth_token", CapitalService.newTokenSession(getResponse(), getPathServer(), trans, tokenAuth));
        } catch (Exception e) {
            result = new Bean();
            result.set("auth_token", tokenAuth);
            result.set("message", "Erro ao revalidar token, varifique a expiração do token.");
        }

        trans.commit();

        return result;
    }

    public Bean feedback() throws Exception {

        ValidatorContract validator = new ValidatorContract();
        TransactionDB trans = getTrans();
        String tokenAuth = (String)getBodyParameter("token_auth");
        validator.isRequiredString(tokenAuth, "O token_auth é um campo requerido");
        PessoaBean pesBean = CapitalService.getPessoaBytoken(getResponse(), getPathServer(), trans, tokenAuth);

        String feedback = (String)getBodyParameter("feedback");

        validator.isRequiredString(feedback, "O feedback é um campo requerido");

        StringBuilder message = new StringBuilder();
        message.append(" Oie! <br>");
        message.append(" Acabamos de receber um feedback pelo nosso aplicativo!  <br>");
        message.append(" Enviado por:  <br> <br>");
        message.append(" Nome: "+pesBean.getNomeRazao());
        message.append("  <br>Telefone: "+pesBean.getTelefone1());
        message.append("  <br>Email: "+pesBean.getEmail());
        message.append("  <br>Cpf: "+pesBean.getCpfCnpj());
        message.append("  <br>Feedback: "+feedback);

        String destinatarios = PropertiesUtils.get("feedback.destinatarios", false);
        String subject = PropertiesUtils.get("feedback.subject", false);
        String[] email = destinatarios.split(",");

        //String[] email = {"kuesley@gmail.com", "renatosaladino@gmail.com", "renato.quickit@gmail.com"};

        EmailService.enviarEmail( email,  subject, message.toString(), null, pesBean.getEmail());

        Bean result = new Bean();

        try {
            result.set("auth_token", CapitalService.newTokenSession(getResponse(), getPathServer(), trans, tokenAuth));
        } catch (Exception e) {
            result = new Bean();
            result.set("auth_token", tokenAuth);
            result.set("message", "Erro ao revalidar token, varifique a expiração do token.");
        }

        return result;
    }

    public Bean buscaCpf() throws Exception {
        TransactionDB trans = getTrans();

        String cpf = (String)getBodyParameter("cpf");
        cpf = StringUtils.getNumberOnly(cpf);

        if (StringUtils.isValidCpfCnpj(cpf) == false )
            throw new WarningException(MsgService.get( getPathServer(), "msg.005.buscacpf.01", cpf));

        Bean result = new Bean();
        try {
            WhereDB where = new WhereDB();
            where.add("CpfCnpj", WhereDB.Condition.EQUALS, cpf);
            List<PessoaBean> pesList = trans.select(PessoaBean.class, where);
            PessoaBean pesBean = pesList.get(0);

            result.set("message", MsgService.get( getPathServer(), "msg.005.buscacpf.02"));
            result.set("rows", 1);
        } catch (NotFoundException e) {
            result.set("message", MsgService.get( getPathServer(), "msg.005.buscacpf.03", cpf) );
            result.set("rows", 0);
        }

        return result;
    }

    public Bean recuperarSenhaPasso1() throws Exception {
        ValidatorContract validator = new ValidatorContract();

        String cpf = (String)getBodyParameter("cpf");
        cpf = StringUtils.getNumberOnly(cpf);
        validator.isRequiredString(cpf, "O CPF é um campo requerido para o recuperar a senha");
        validator.isValidCpf(cpf, "O CPF informado é inválido");

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();

        Bean b = new Bean();

        try {
            WhereDB where = new WhereDB();
            where.add("cpfCnpj", WhereDB.Condition.EQUALS, cpf );
            PessoaBean pesBean = ((List<PessoaBean>) trans.select(PessoaBean.class, where)).get(0);
            String telefone = pesBean.getTelefone1();
            Integer codigo = NumberUtils.random(6);
            String hash = SecurityUtil.encryptMD5( Integer.toString(codigo) );

            String message = MsgService.get(getPathServer(), "msg.005.recuperarsenhasms.01", String.valueOf(codigo));

            // envia um sms
            String status = SMSService.sendSMS(telefone, message);
            try {
                EmailService.enviarEmail(pesBean.getEmail(), "Recuperação de Senha", message);
                b.set("status", "success");
                b.set("code", hash );
                b.set("message", MsgService.get(getPathServer(), "msg.005.recuperarsenhasms.02", "*****-"+telefone.substring(6)));

                Bean b1 = new Bean();
                b1.set("exp", org.apache.commons.lang.time.DateUtils.addMinutes(new Date(), 5).getTime());
                b1.set("iat", new Date().getTime());
                b1.set("pes_id", pesBean.getId());
                b1.set("pes_nome", StringUtils.getFirstWord(pesBean.getNomeRazao()));
                b1.set("codigo", hash);
                String token = Jwt.generate(b1);
                b.set("token_recovery", token);

            } catch (Exception ex) {
                throw new WarningException(MsgService.get(getPathServer(), "msg.005.recuperarsenhasms.03"));
            }

        } catch (NotFoundException e) {
            throw new WarningException(MsgService.get(getPathServer(), "msg.005.recuperarsenhasms.04"));
        }

        return b;
    }

    public Bean recuperarSenhaPasso2() throws Exception {
        ValidatorContract validator = new ValidatorContract();

        String cpf = (String)getBodyParameter("cpf");
        String codigo = (String)getBodyParameter("codigo");
        String nova_senha = (String)getBodyParameter("nova_senha");
        String token_recovery = (String)getBodyParameter("token_recovery");

        cpf = StringUtils.getNumberOnly(cpf);
        validator.isRequiredString(cpf, "O CPF é um campo requerido para o recuperar a senha");
        validator.isValidCpf(cpf, "O CPF informado é inválido");
        validator.isRequiredString(codigo, "O campo codigo é um campo requerido para o atualizar a senha");
        validator.isRequiredString(nova_senha, "O campo nova_senha é um campo requerido para o atualizar a senha");
        validator.isRequiredString(token_recovery, "O campo token_recovery é um campo requerido para o atualizar a senha");

        if (!validator.isValid()) {
            throw new API400Exception("validation", validator.getMessages());
        }

        TransactionDB trans = getTrans();

        try {
            Bean bt = Jwt.token2bean(token_recovery);
            Integer pesID = bt.get("pes_id");
            String codigo_gerado = bt.get("codigo");
            codigo = SecurityUtil.encryptMD5( codigo );
            if (!codigo.equalsIgnoreCase(codigo_gerado))
                throw new API403Exception("code_invalid","Código inválido");

            PessoaBean pesBean = trans.selectById(PessoaBean.class, pesID);
            pesBean.setSenha(nova_senha);

            trans.execUpdate("update pessoa set pes_senha = ? where pes_id = ?", new Object[]{ nova_senha, pesID});
            trans.commit();

            Bean b = new Bean();
            b.set("status", "success");
            b.set("message", "Senha alterada com sucesso");

            return b;

        } catch (Exception e) {
            throw new API403Exception("not_authorized", e.getMessage());
        }

    }

    public static boolean isValidDate(String date, String formato) {
        SimpleDateFormat sdf = new SimpleDateFormat(formato);
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }
        catch (ParseException e) {
            return false;
        }
        if (!sdf.format(testDate).equals(date)) {
            return false;
        }
        return true;
    }
}
