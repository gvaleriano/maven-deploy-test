package capital.api;

import br.com.dotum.jedi.core.db.TransactionDB;
import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.ErrorException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.DateUtils;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import capital.model.bean.PessoaBean;
import capital.model.bean.RequestBean;
import capital.service.MsgService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Properties;

public class PinbankService {
	private static Log LOG = LogFactory.getLog(PinbankService.class);

	private static Properties props;
	public static final String NAME = "PINBANK";
	private static final String REQUEST = "REQUEST";
	private static final String RESPONSE= "RESPONSE";
	private static final String FILE_NAME= "/pinbank-config.properties";

	public static final Boolean VALIDAR_CARTAO = Boolean.valueOf(get("validar_cartao"));
	private static final String CPFCNPJ_LOJA = get("cpfcnpj_loja");
	private static final Integer PRODUTO_LOJA = Integer.valueOf(get("produto_loja"));
	private static final Integer CODIGO_PRODUTO = Integer.valueOf(get("codigo_produto"));

	public static Properties  iniciaProps() {
		InputStream is = null;
		try {
			is = PropertiesUtils.class.getResourceAsStream(FILE_NAME);

			props = new Properties();
			if (is != null) props.load(is);
		} catch (Exception e) {
			throw new WarningException(e.getMessage(), e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					throw new WarningException(e.getMessage(), e);
				}
			}
		}
		return props;
	}

	public static Bean inclusaoDeCliente(TransactionDB trans, PessoaBean pesBean, String nome, Date dataNascimento, String sexo, String cpf, String rg, String emissorRg, Date dataEmissaoRg,
										 String email, String dddCelular, String numeroCelular, String cep, String estado,
										 String cidade, String bairro, String logradouro, String numero, HttpServletRequest request) throws Exception {

		String codigo_concatenar_nome = get("codigo_concatenar_nome");
		if (StringUtils.hasValue(codigo_concatenar_nome) == false)
			throw new DeveloperException("Precisa definir 'codigo_concatenar_nome' no arquivo de propriedades da pinbank");

		JSONObject obj = new JSONObject();
		obj.put("codigoProduto",CODIGO_PRODUTO);
		obj.put("nome", codigo_concatenar_nome + pesBean.getNomeRazao());

		obj.put("dataNascimento", DateUtils.formatDate(dataNascimento));
		obj.put("sexo", sexo);
		obj.put("cpf", cpf);
		obj.put("rg", rg);
		obj.put("emissor", emissorRg);
		obj.put("dataEmissao", DateUtils.formatDate(dataEmissaoRg));
		obj.put("email", email);
		obj.put("ddiCelular", "55");
		obj.put("dddCelular", dddCelular);
		obj.put("numeroCelular", numeroCelular);
		obj.put("cepResidencial", cep);
		obj.put("estadoResidencia", estado);
		obj.put("cidadeResidencia", cidade);
		obj.put("bairroResidencia", bairro);
		obj.put("logradouroResidencia", logradouro);
		obj.put("numeroResidencia", numero);
		String nameMethod = new Object(){}.getClass().getEnclosingMethod().getName();

		long start_time = System.currentTimeMillis();
		RequestBean gwRequestBean = new RequestBean();
		gwRequestBean.getPessoaCliente().setId(pesBean.getId());
		gwRequestBean.setData(trans.getDataAtual());
		gwRequestBean.setHora(trans.getHoraAtual());
		gwRequestBean.setRequest( obj.toString() );
		gwRequestBean.setEndpoint(nameMethod);
		gwRequestBean.setHost( request.getLocalAddr() );

		String URL = "https://www.ti-pagos.com/Ws/Integracao/WCF/REST/Cadastro.svc/cadastrarClienteExterno";
		Bean result  = new Bean(); 

		// faz a requisição
		LOG.debug("Chamando Endpoint do Gateway ... ");
		result.toObject(APIConnection.post(URL, null, obj).toString());
		Integer mml = (Integer)result.get("mml");

		long end_time = System.currentTimeMillis();
		gwRequestBean.setTime(new Long(end_time - start_time).intValue());
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGateway(PinbankService.NAME);
		gwRequestBean.setHttpStatus( result.get("status_code"));
		gwRequestBean.setResponse( result.toJson2() );

		gwRequestBean.setGatewaySucesso(0);
		if (mml.equals(0) == false) gwRequestBean.setGatewaySucesso(1);

		gwRequestBean.setGatewayCodigoRetorno( result.get("codigoretorno").toString() );
		gwRequestBean.setGatewayMensagem(result.get("descricaoRetorno"));

		LOG.debug("Inserindo LOG Gateway...");
		TransactionDB trans2 = TransactionDB.getInstance(null);
		trans2.insert(gwRequestBean);
		trans2.commit();
		trans2.close();
		LOG.debug("LOG Gateway (inserido)");

		if (mml.equals(0) == true) {
			throw new API400Exception("pinbank_exception","Pinbank: " + result.get("descricaoRetorno"));
		}

		return result;
	}

	public static Bean efetuarTransacao(String serverPath, TransactionDB trans, String transIdDaVendaPendente, PessoaBean pesBean, Integer codigoCliente, String idCartao, BigDecimal valor, String nsuTransacao, HttpServletRequest request) throws Exception {
		BigDecimal novoValor = valor.multiply(new BigDecimal("100"));
		Integer valorInteiro = novoValor.intValue();

		JSONObject obj = new JSONObject();
		obj.put("ddiCelular", 0);
		obj.put("dddCelular", 0);
		obj.put("numeroCelular", 0);
		obj.put("codigoCliente", codigoCliente);
		obj.put("codigoProduto", CODIGO_PRODUTO);
		obj.put("cpfCnpjLoja", CPFCNPJ_LOJA);
		obj.put("produtoLoja", PRODUTO_LOJA);
		obj.put("nsuTransacao", nsuTransacao);
		obj.put("idCartao", idCartao);
		obj.put("valor", valorInteiro);
		obj.put("numParcelas", 1);
		obj.put("formaPagamento", 1);
		obj.put("transacaoCapturada", true);
		String nameMethod = new Object(){}.getClass().getEnclosingMethod().getName();

		long start_time = System.currentTimeMillis();
		RequestBean gwRequestBean = new RequestBean();
		gwRequestBean.setData(trans.getDataAtual());
		gwRequestBean.setHora(trans.getHoraAtual());
		gwRequestBean.setRequest( obj.toString() );
		gwRequestBean.setEndpoint(nameMethod);
		gwRequestBean.setHost( request.getLocalAddr() );

		String URL = "https://www.ti-pagos.com/Ws/Integracao/WCF/REST/Financeiro.svc/efetuarTransacao";
		Bean result = new Bean();
		result.toObject(APIConnection.post(URL, null, obj).toString());

		long end_time = System.currentTimeMillis();
		gwRequestBean.getPessoaCliente().setId(pesBean.getId());
		gwRequestBean.setTime(new Long(end_time - start_time).intValue());
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGateway(PinbankService.NAME);
		gwRequestBean.setHttpStatus( result.get("status_code"));
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGatewayCodigoRetorno( result.get("codigoretorno").toString() );
		gwRequestBean.setGatewayMensagem(result.get("descricaoRetorno"));

		gwRequestBean.setGatewaySucesso(0);
		Integer codigo_retorno = (Integer)result.get("codigoretorno");
		if ( codigo_retorno.equals(0) ) gwRequestBean.setGatewaySucesso(1);

		LOG.debug("Inserindo LOG Gateway...");
		TransactionDB trans2 = TransactionDB.getInstance(null);
		trans2.insert(gwRequestBean);
		trans2.commit();
		trans2.close();
		LOG.debug("LOG Gateway (inserido)");

		return formatResponse(serverPath, result);
	}

	public static Bean cancelarTransacao(String serverPath, TransactionDB trans, String nsuTiPagos, PessoaBean pesBean, BigDecimal valor, HttpServletRequest request) throws Exception {
		BigDecimal novoValor = valor.multiply(new BigDecimal("100"));
		Integer valorInteiro = novoValor.intValue();

		JSONObject obj = new JSONObject();
		obj.put("ddiCelular", 0);
		obj.put("dddCelular", 0);
		obj.put("numeroCelular", 0);
		obj.put("codigoCliente", pesBean.getPinbankId());
		obj.put("codigoProduto", CODIGO_PRODUTO);
		obj.put("cpfCnpjLoja", CPFCNPJ_LOJA);
		obj.put("nsuTiPagos", nsuTiPagos);
		obj.put("valor", valorInteiro);
		String nameMethod = new Object(){}.getClass().getEnclosingMethod().getName();

		long start_time = System.currentTimeMillis();
		RequestBean gwRequestBean = new RequestBean();
		gwRequestBean.setData(trans.getDataAtual());
		gwRequestBean.setHora(trans.getHoraAtual());
		gwRequestBean.setRequest( obj.toString() );
		gwRequestBean.setEndpoint(nameMethod);
		gwRequestBean.setHost( request.getLocalAddr() );

		String URL = "https://www.ti-pagos.com/Ws/Integracao/WCF/REST/Financeiro.svc/cancelarTransacao";
		Bean result = new Bean();
		result.toObject(APIConnection.post(URL, null, obj).toString());

		long end_time = System.currentTimeMillis();
		gwRequestBean.getPessoaCliente().setId(pesBean.getId());
		gwRequestBean.setTime(new Long(end_time - start_time).intValue());
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGateway(PinbankService.NAME);
		gwRequestBean.setHttpStatus( result.get("status_code"));
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGatewayCodigoRetorno( result.get("codigoretorno").toString() );
		gwRequestBean.setGatewayMensagem(result.get("descricaoRetorno"));

		gwRequestBean.setGatewaySucesso(0);
		Integer codigo_retorno = (Integer)result.get("codigoretorno");
		if ( codigo_retorno.equals(0) ) gwRequestBean.setGatewaySucesso(1);

		LOG.debug("Inserindo LOG Gateway...");
		TransactionDB trans2 = TransactionDB.getInstance(null);
		trans2.insert(gwRequestBean);
		trans2.commit();
		trans2.close();
		LOG.debug("LOG Gateway (inserido)");

		return formatResponse(serverPath, result);
	}

	private static Bean formatResponse(String pathServer, Bean result) throws Exception {

		Integer codigoRetorno = result.get("codigoRetorno");
		if (codigoRetorno.equals(0) == false) {
			throw new API400Exception("gateway_error","Banco " + MsgService.get(pathServer, "msg.pinbankservice.formatresponse.01"));
		}
		return result;
	}

	public static Bean consultaDeCartoes(TransactionDB trans, PessoaBean pesBean, Integer codigoCliente, Integer statusCartao, HttpServletRequest request) throws Exception {

		JSONObject obj = new JSONObject();
		obj.put("ddiCelular", 0);
		obj.put("dddCelular", 0);
		obj.put("numeroCelular", 0);
		obj.put("codigoCliente", codigoCliente);
		obj.put("codigoProduto", CODIGO_PRODUTO);
		String nameMethod = new Object(){}.getClass().getEnclosingMethod().getName();

		long start_time = System.currentTimeMillis();
		RequestBean gwRequestBean = new RequestBean();
		gwRequestBean.setData(trans.getDataAtual());
		gwRequestBean.setHora(trans.getHoraAtual());
		gwRequestBean.setRequest( obj.toString() );
		gwRequestBean.setEndpoint(nameMethod);
		gwRequestBean.setHost( request.getLocalAddr() );

		if (statusCartao != null) obj.put("statusCartao", statusCartao);

		String URL = "https://www.ti-pagos.com/Ws/Integracao/WCF/REST/Cadastro.svc/consultaDeCartoes";
		Bean result = new Bean();
		result.toObject(APIConnection.post(URL, null, obj).toString());

		long end_time = System.currentTimeMillis();
		gwRequestBean.getPessoaCliente().setId(pesBean.getId());
		gwRequestBean.setTime(new Long(end_time - start_time).intValue());
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGateway(PinbankService.NAME);
		gwRequestBean.setHttpStatus( result.get("status_code"));
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGatewayCodigoRetorno( result.get("codigoretorno").toString() );
		gwRequestBean.setGatewayMensagem(result.get("descricaoRetorno"));
		LOG.debug("Inserindo LOG Gateway...");
		TransactionDB trans2 = TransactionDB.getInstance(null);
		trans2.insert(gwRequestBean);
		trans2.commit();
		trans2.close();
		LOG.debug("LOG Gateway (inserido)");

		return result;
	}

	public static Bean exclusaoDeCartao(String serverPath, TransactionDB trans, PessoaBean pesBean, Integer codigoCliente, String idCartao, HttpServletRequest request) throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("ddiCelular", 0);
		obj.put("dddCelular", 0);
		obj.put("numeroCelular", 0);
		obj.put("codigoCliente", codigoCliente);
		obj.put("codigoProduto", CODIGO_PRODUTO);
		obj.put("idCartao", idCartao);
		String nameMethod = new Object(){}.getClass().getEnclosingMethod().getName();

		long start_time = System.currentTimeMillis();
		RequestBean gwRequestBean = new RequestBean();
		gwRequestBean.setData(trans.getDataAtual());
		gwRequestBean.setHora(trans.getHoraAtual());
		gwRequestBean.setRequest( obj.toString() );
		gwRequestBean.setEndpoint(nameMethod);
		gwRequestBean.setHost( request.getLocalAddr() );

		String URL = "https://www.ti-pagos.com/Ws/Integracao/WCF/REST/Cadastro.svc/exclusaoDeCartao";
		Bean result = new Bean();
		result.toObject(APIConnection.post(URL, null, obj).toString());

		long end_time = System.currentTimeMillis();
		gwRequestBean.getPessoaCliente().setId(pesBean.getId());
		gwRequestBean.setTime(new Long(end_time - start_time).intValue());
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGateway(PinbankService.NAME);
		gwRequestBean.setHttpStatus( result.get("status_code"));
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGatewayCodigoRetorno( result.get("codigoretorno").toString() );
		gwRequestBean.setGatewayMensagem(result.get("descricaoRetorno"));

		gwRequestBean.setGatewaySucesso(0);
		Integer codigo_retorno = (Integer)result.get("codigoretorno");
		if ( codigo_retorno.equals(0) ) gwRequestBean.setGatewaySucesso(1);

		LOG.debug("Inserindo LOG Gateway...");
		TransactionDB trans2 = TransactionDB.getInstance(null);
		trans2.insert(gwRequestBean);
		trans2.commit();
		trans2.close();
		LOG.debug("LOG Gateway (inserido)");

		return formatResponse(serverPath, result);
	}
	public static Bean inclusaoDeCartao(String serverPath, TransactionDB trans, PessoaBean pesBean, Integer codigoCliente, String numero, String nomeImpresso, String apelido, String mesValidade,
			String anoValidade, String cvv, HttpServletRequest request) throws Exception {
		String fullData = anoValidade+""+ StringUtils.fillLeft( mesValidade+"", '0', 2 );
		if(fullData.length() <= 4) {
			fullData = "20"+fullData;
		}
		apelido = StringUtils.hasValue(apelido)? apelido:StringUtils.getFirstWord(nomeImpresso);
		apelido += new Date().getTime();
		numero = StringUtils.getNumberOnly(numero);
		
		if ((StringUtils.hasValue(numero) == false) || (numero.length() < 8)) {
			throw new WarningException(MsgService.get(serverPath, "msg.015.payzen.01"));
		}

		JSONObject obj = createCartaoJson(codigoCliente, numero, nomeImpresso, apelido, cvv, fullData);
		JSONObject truncado = createCartaoJson(codigoCliente, CapitalService.esconderNumerosCartao(numero), nomeImpresso, apelido, "xxx", fullData);
		String nameMethod = new Object(){}.getClass().getEnclosingMethod().getName();

		long start_time = System.currentTimeMillis();
		RequestBean gwRequestBean = new RequestBean();
		gwRequestBean.setData(trans.getDataAtual());
		gwRequestBean.setHora(trans.getHoraAtual());
		gwRequestBean.setRequest( obj.toString() );
		gwRequestBean.setEndpoint(nameMethod);
		gwRequestBean.setHost( request.getLocalAddr() );

		String URL = "https://www.ti-pagos.com/Ws/Integracao/WCF/REST/Cadastro.svc/inclusaoDeCartao";
		Bean result = new Bean();
		result.toObject(APIConnection.post(URL, null, obj));

		long end_time = System.currentTimeMillis();
		gwRequestBean.getPessoaCliente().setId(pesBean.getId());
		gwRequestBean.setTime(new Long(end_time - start_time).intValue());
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGateway(PinbankService.NAME);
		gwRequestBean.setHttpStatus( result.get("status_code"));
		gwRequestBean.setResponse( result.toJson2() );
		gwRequestBean.setGatewayCodigoRetorno( result.get("codigoretorno").toString() );
		gwRequestBean.setGatewayMensagem(result.get("descricaoRetorno"));

		gwRequestBean.setGatewaySucesso(0);
		Integer codigo_retorno = (Integer)result.get("codigoretorno");
		if ( codigo_retorno.equals(0) ) gwRequestBean.setGatewaySucesso(1);

		LOG.debug("Inserindo LOG Gateway...");
		TransactionDB trans2 = TransactionDB.getInstance(null);
		trans2.insert(gwRequestBean);
		trans2.commit();
		trans2.close();
		LOG.debug("LOG Gateway (inserido)");

		// Valida se retornar algo no id do cartão ja registra na carteira novamente
		Integer codigoRetorno = result.get("codigoRetorno");
		String idCartao = result.get("idcartao");
		if (idCartao.equals("") || idCartao == null) {
			throw new ErrorException(MsgService.get(serverPath, "msg.pinbankservice.inclusaocartao.01", "" + result.get("descricaoRetorno")));
		}
		result.set("gateway", PinbankService.NAME);
		result.set("maskedNumber", CapitalService.esconderNumerosCartao(numero));
		return result;

	}
	private static JSONObject createCartaoJson(Integer codigoCliente, String numero, String nomeImpresso,
			String apelido, String cvv, String fullData) {
		JSONObject obj = new JSONObject();
		obj.put("ddiCelular", 0);
		obj.put("dddCelular", 0);
		obj.put("numeroCelular", 0);
		obj.put("codigoProduto", CODIGO_PRODUTO);
		obj.put("codigoCliente", codigoCliente);
		obj.put("validarCartao", VALIDAR_CARTAO);
		obj.put("apelido", apelido);
		obj.put("nomeImpresso", nomeImpresso);
		obj.put("numero", numero);
		obj.put("dataValidade", fullData);
		obj.put("cvv", cvv);
		return obj;
	}

	public final static String get(String propertie) {
		if ((props == null)) iniciaProps();
		return props.getProperty(propertie);
	}
}
