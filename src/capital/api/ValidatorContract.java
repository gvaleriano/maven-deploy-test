package capital.api;

import br.com.dotum.jedi.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ValidatorContract {

    private List<String> errors = new ArrayList<String>();

    public void isRequired(Object value, String message) {
        if (value == null)
            errors.add( message );
    }

    public void isRequiredString(String value, String message) {
        if (value == null || value.isEmpty())
            errors.add( message );
    }
    public void hasMinLength(String value, int min, String message) {
        if ( (StringUtils.hasValue(value) == false) || value.length() <= min)
            errors.add( message );
    }

    public void hasLenght(String value, int size, String message) {
        if ( (StringUtils.hasValue(value) == false) || value.length() != size)
            errors.add( message );
    }

    public void equalsAny(String value, String[] values, String message) {
        for (int i = 0; i < values.length; i++) {
            if ( values[i].equalsIgnoreCase(value) ) return;
        }
        errors.add( message );
    }

    public void isValid(boolean isValid, String message) {
        if (isValid == false) {
            errors.add( message );
        }
    }

    public void isValidCpf(String cpf, String message) {
        if (StringUtils.isValidCpfCnpj(cpf) == false) {
            errors.add(message);
        }
    }

    public void isValidDate(String date, String format, String message) {
        if (StringUtils.isDateValid(date, format) == false) {
            errors.add(message);
        }
    }

    public boolean isValid() {
        return errors.size() == 0;
    }

    public String getMessages() {
        StringBuilder result = new StringBuilder();
        for (String str: errors) {
            result.append( str );
            result.append( " | ");
        }
        return result.toString();
    }
}