package capital.control;

import br.com.dotum.jedi.gen.AbstractColumnMap;
import br.com.dotum.jedi.gen.ColumnC;

import java.util.ArrayList;
import java.util.List;

public class MapColumn implements AbstractColumnMap {
	public List<ColumnC> createList() {
		List<ColumnC> list = new ArrayList<ColumnC>();
		
		// AAA
		list.add(new ColumnC("authtoken",					null,               "authToken",					"Auth Token",						null,						null,						null));
		list.add(new ColumnC("resposta",					null,               "resposta",					"Resposta",						null,						null,						null));

		list.add(new ColumnC("aplicacao",					null,               "aplicacao",					"Aplicação",						null,						null,						null));
		list.add(new ColumnC("ativo",						null, 				"ativo",						"Ativo",								null,						null,						null));
		list.add(new ColumnC("aliquotaipi",				    null,				"aliquotaIpi",					"Aliquota ipi",							null,						null,						null));
		list.add(new ColumnC("aliquotapis",				    null,				"aliquotaPis",					"Aliquota pis",							null,						null,						null));
		list.add(new ColumnC("aliquotacofins",			    null,				"aliquotaCofins",				"Aliquota cofins",						null,						null,						null));
		list.add(new ColumnC("aliquotaicms",			    null,				"aliquotaIcms",					"Aliquota icms",						null,						null,						null));
		list.add(new ColumnC("atualizado",				    null,				"atualizado",					"Atualizado",							null,						null,						null));
		list.add(new ColumnC("entradasaida",				null, 				"entradaSaida",					"Entrada/Saída",						null,						null,						null));
		list.add(new ColumnC("atualizado",					null, 				"atualizado",					"Atualizado",							null,						null,						null));
		list.add(new ColumnC("apelidofantasia",				null,				"apelidoFantasia",				"Apelido/Fantasia",						null,						null,						null));
		list.add(new ColumnC("aniversario",					null,				"aniversario",					"Aniversario",							null,						null,						null));
		list.add(new ColumnC("agencia",						null, 				"agencia",						"Agencia",								null,						null,						null));
		list.add(new ColumnC("agenciadigito",				null, 				"agenciaDigito",				"Digito da agencia",					null,						null,						null));
		list.add(new ColumnC("arquivo",						null, 				"arquivo",						"Arquivo",								null,						null,						null));
		list.add(new ColumnC("aliquotaicmsst",				null, 				"aliquotaIcmsSt",				"Aliquota icms st",						null,						null,						null));
		list.add(new ColumnC("aliquitaipi",					null, 				"aliquitaIpi",					"Aliquita ipi",							null,						null,						null));
		list.add(new ColumnC("ambiente",					null, 				"ambiente",						"Ambiente",								null,						null,						null));
		list.add(new ColumnC("analiticosintetico",			null, 				"analiticoSintetico",			"Analitico Sintetico",					null,						null,						null));
		list.add(new ColumnC("autorizacao",					null,               "autorizacao",					"Autorização",							null,						null,						null));
		list.add(new ColumnC("area",						null,               "area",							"Área",									null,						null,						null));
		
		list.add(new ColumnC("tempoprocessamento",			null,               "tempoProcessamento",			"Tempo processamento",					null,						null,						null));
		list.add(new ColumnC("foto",						null,               "foto",							"Foto",									null,						null,						null));
		list.add(new ColumnC("tipoassociacao",				null,               "tipoAssociacao",				"Tipo de associação",					null,						null,						null));
		list.add(new ColumnC("telefonecelular",				null,               "telefoneCelular",				"Telefone Celular",						null,						null,						null));
		list.add(new ColumnC("telefoneresidencial",			null,               "telefoneResidencia",			"Telefone Residência",					null,						null,						null));
		list.add(new ColumnC("tituloeleitor",				null,               "tituloeleitor",				"Titulo eleitor",						null,						null,						null));
		list.add(new ColumnC("nit",							null,               "nit",							"NIT",									null,						null,						null));
		list.add(new ColumnC("cnh",							null,               "cnh",							"CNH",									null,						null,						null));
		list.add(new ColumnC("mei",							null,               "mei",							"MEI",									null,						null,						null));
		list.add(new ColumnC("marcacarro",					null,               "marcaCarro",					"Marca carro",							null,						null,						null));
		list.add(new ColumnC("categoriacarro",				null,               "categoriaCarro",				"Categoria carro",						null,						null,						null));
		list.add(new ColumnC("placacarro",					null,               "placaCarro",					"Placa carro",							null,						null,						null));
		list.add(new ColumnC("fichaantiga",					null,               "fichaAntiga",					"Ficha antiga",							null,						null,						null));

		
		// BBB
		list.add(new ColumnC("bairro",						null,				"bairro",						"Bairro",								null,						null,						null));
		list.add(new ColumnC("bcicms",						null, 				"bcIcms",						"Bc icms",								null,						null,						null));
		list.add(new ColumnC("bcicmsst",					null, 				"bcIcmsSt",						"Bc icms st",							null,						null,						null));
		list.add(new ColumnC("bcpis",						null, 				"bcPis",						"Bc pis",								null,						null,						null));
		list.add(new ColumnC("bccofins",					null, 				"bcCofins",						"Bc cofins",							null,						null,						null));
		list.add(new ColumnC("bcipi",						null, 				"bcIpi",						"Bc ipi",								null,						null,						null));
		list.add(new ColumnC("bandeira",					null,               "bandeira",						"Bandeira",								null,						null,						null));
		list.add(new ColumnC("banco",						null,               "banco",						"Banco",								null,						null,						null));
		list.add(new ColumnC("bandeiracartao",				null,               "bandeiraCartao",				"Bandeira Cartão",						null,						null,						null));

		
		// CCC
		list.add(new ColumnC("cadastro",					null, 				"cadastro",						"Cadastro",								null,						null,						null));
		list.add(new ColumnC("cest",						null, 				"cest",							"Cest",									null,						null,						null));
		list.add(new ColumnC("cfopestadualtag",				null, 				"cfopEstadualTag",				"Cfop estadual tag",					null,						null,						null));
		list.add(new ColumnC("cfopinterestadualtag",		null, 				"cfopInterestadualTag",			"Cfop interestadual tag",				null,						null,						null));
		list.add(new ColumnC("codigo",						null, 				"codigo",						"Codigo",								null,						null,						null));
		list.add(new ColumnC("cep",							null,				"cep",							"Cep",									null,						null,						null));
		list.add(new ColumnC("complemento",					null,				"complemento",					"Complemento",							null,						null,						null));
		list.add(new ColumnC("campo",						null, 				"campo",						"Campo",								null,						null,						null));
		list.add(new ColumnC("chave",						null, 				"chave",						"Chave",								null,						null,						null));
		list.add(new ColumnC("cpfcnpj",						null,				"cpfCnpj",						"CpfCnpj",								null,						null,						null));
		list.add(new ColumnC("cidade",						null, 				"cidade",						"Cidade",								null,						null,						null));
		list.add(new ColumnC("codigobarra",				    null,				"codigoBarra",					"Codigo barra",							null,						null,						null));
		list.add(new ColumnC("cfop",						null, 				"cfop",							"Cfop",									null,						null,						null));
		list.add(new ColumnC("cliente",						null,				"cliente",						"Cliente",								null,						null,						null));
		list.add(new ColumnC("comentario",					null, 				"comentario",					"Comentario",							null,						null,						null));
		list.add(new ColumnC("caixa",						null, 				"caixa",						"Caixa",								null,						null,						null));
		list.add(new ColumnC("conta",						null, 				"conta",						"Conta",								null,						null,						null));
		list.add(new ColumnC("contadigito",					null, 				"contaDigito",					"Digito da conta",						null,						null,						null));
		list.add(new ColumnC("chaveacesso",					null, 				"chaveAcesso",					"Chave de acesso",						null,						null,						null));
		list.add(new ColumnC("conteudo",					null, 				"conteudo",						"Conteudo",								null,						null,						null));
		list.add(new ColumnC("codigoreduzido",				null, 				"codigoReduzido",				"Codigo reduzido",						null,						null,						null));
		list.add(new ColumnC("cid_transp_id",				null, 				"transpCidade",					"Transp Cidade",						null,						null,						null));
		list.add(new ColumnC("codigocooperativa",			null, 				"codigoCooperativa",			"Codigo cooperativa",					null,						null,						null));
		list.add(new ColumnC("codigocedente",				null, 				"codigoCedente",				"Codigo cedente",						null,						null,						null));
		list.add(new ColumnC("codigocedentedigito",			null, 				"codigoCedenteDigito",			"Codigo cedente digito",				null,						null,						null));
		list.add(new ColumnC("codigoconvenio",				null, 				"codigoConvenio",				"Codigo convenio",						null,						null,						null));
		list.add(new ColumnC("codigobarra1",				null, 				"codigoBarra1",					"Codigo barra1",						null,						null,						null));
		list.add(new ColumnC("codigobarra2",				null, 				"codigoBarra2",					"Codigo barra2",						null,						null,						null));
		list.add(new ColumnC("cid_origem_id",				null, 				"origemCidade",					"Origem cidade",						null,						null,						null));
		list.add(new ColumnC("cid_destino_id",				null, 				"destinoCidade",				"Destino Cidade",						null,						null,						null));
		list.add(new ColumnC("chipid",						null, 				"chipId",						"Id do Chip",							null,						null,						null));

		
		// DDD
		list.add(new ColumnC("data",						null, 				"data",							"Data",									null,						null,						null));
		list.add(new ColumnC("descricao",					null, 				"descricao",					"Descrição",							null,						null,						null));
		list.add(new ColumnC("destinotag",					null,				"destinoTag",					"Destino tag",							null,						null,						null));
		list.add(new ColumnC("dataemissao",					null, 				"dataEmissao",					"Data emissao",							null,						null,						null));
		list.add(new ColumnC("debitocredito",				null, 				"debitoCredito",				"Debito/Credito",						null,						null,						null));
		list.add(new ColumnC("dataprocessamento",			null, 				"dataProcessamento",			"Data processamento",					null,						null,						null));
		list.add(new ColumnC("datacadastro",				null,               "dataCadastro",					"Data Cadastro",						null,						null,						null));
		list.add(new ColumnC("dataagendada",				null,               "dataAgendada",					"Data Agendada",						null,						null,						null));
		list.add(new ColumnC("dreordem",					null, 				"dreOrdem",						"Ordem do DRE",							null,						null,						null));
		list.add(new ColumnC("destino",						null,               "destino",						"Destino",								null,						null,						null));
		list.add(new ColumnC("destinonomerazao",			null, 				"destinoNomeRazao",				"Destino nomerazao",					null,						null,						null));
		list.add(new ColumnC("destinocpfcnpj",				null, 				"destinoCpfCnpj",				"Destino cpfcnpj",						null,						null,						null));
		list.add(new ColumnC("destinoie",					null, 				"destinoIe",					"Destino ie",							null,						null,						null));
		list.add(new ColumnC("destinologradouro",			null, 				"destinoLogradouro",			"Destino logradouro",					null,						null,						null));
		list.add(new ColumnC("destinonumero",				null, 				"destinoNumero",				"Destino numero",						null,						null,						null));
		list.add(new ColumnC("destinobairro",				null, 				"destinoBairro",				"Destino bairro",						null,						null,						null));
		list.add(new ColumnC("destinocep",					null, 				"destinoCep",					"Destino cep",							null,						null,						null));
		list.add(new ColumnC("desconto",					null,               "desconto",						"Desconto",								null,						null,						null));
		list.add(new ColumnC("dataregistroemp",				null,               "dataRegistroEmp",				"Data Registro Emp",					null,						null,						null));

		
		// EEE
		list.add(new ColumnC("empresa",						null,               "empresa",						"Empresa",								null,						null,						null));
		list.add(new ColumnC("estoque",					    null,				"estoque",						"Estoque",								null,						null,						null));
		list.add(new ColumnC("emissao",						null,				"emissao",						"Emissão",								null,						null,						null));
		list.add(new ColumnC("enviadorecebido",				null, 				"enviadoRecebido",				"EnviadoRecebido",						null,						null,						null));
		list.add(new ColumnC("estado",						null, 				"estado",						"Estado",								null,						null,						null));
		list.add(new ColumnC("especificacao1",				null, 				"especificacao1",				"Especificação 1",						null,						null,						null));
		list.add(new ColumnC("especificacao2",				null, 				"especificacao2",				"Especificação 2",						null,						null,						null));
		list.add(new ColumnC("especificacao3",				null, 				"especificacao3",				"Especificação 3",						null,						null,						null));
		list.add(new ColumnC("endereco",					null, 				"endereco",						"Endereço",								null,						null,						null));
		list.add(new ColumnC("email1",						null,				"email1",						"Email 1",								null,						null,						null));
		list.add(new ColumnC("email2",						null,				"email2",						"Email 2",								null,						null,						null));
		list.add(new ColumnC("email3",						null,				"email3",						"Email 3",								null,						null,						null));
		list.add(new ColumnC("email",						null,				"email",						"Email",								null,						null,						null));
		list.add(new ColumnC("extensao",					null, 				"extensao",						"Extensao",								null,						null,						null));

		// FFF
		list.add(new ColumnC("finalidade",					null, 				"finalidade",					"Finalidade",							null,						null,						null));
		list.add(new ColumnC("formapagamentotag",			null, 				"formaPagamentoTag",			"Forma de Pagamento tag",				null,						null,						null));
		list.add(new ColumnC("fechado",						null,               "fechado",						"Fechado",								null,						null,						null));
		list.add(new ColumnC("faturamento",					null,               "faturamento",					"Faturamento",							null,						null,						null));

		// GGG
		list.add(new ColumnC("gcm",						    null, 				"gcm",					   		"Gcm",									null,						null,						null));
		list.add(new ColumnC("genero",						null, 				"genero",						"Genero",								null,						null,						null));
		list.add(new ColumnC("grupo",						null, 				"grupo",						"Grupo",								null,						null,						null));

		// HHH
		list.add(new ColumnC("hora",						null, 				"hora",							"Hora",									"HOURFIELD",				null,						null));
		list.add(new ColumnC("horaagendada",				null,               "horaAgendada",					"Hora Agendada",						null,						null,						null));
		list.add(new ColumnC("horacadastro",				null,               "horaCadastro",					"Hora Cadastro",						null,						null,						null));
		list.add(new ColumnC("horaemissao",					null, 				"horaEmissao",					"Hora emissao",							null,						null,						null));
		list.add(new ColumnC("horario",						null,               "horario",						"Horário",								null,						null,						null));

		// III
		list.add(new ColumnC("icone",						null,               "icone",						"Icone",						null,						null,						null));
		list.add(new ColumnC("ibge",						null,             	"ibge",							"Ibge",									null,						null,						null));
		list.add(new ColumnC("id",							null, 				"id",							"Id",									null,						null,						null));
		list.add(new ColumnC("ie",							null,				"ie",							"Inscrição Estadual",					null,						null,						null));
		list.add(new ColumnC("imei",						null, 				"imei",					    	"Imei",				   					null,						null,						null));
		list.add(new ColumnC("im",							null,				"im",							"Inscrição Municipal",					null,						null,						null));
		list.add(new ColumnC("informacaocomplementar",		null, 				"informacaoComplementar",		"Informações complementar",				null,						null,						null));
		list.add(new ColumnC("instrucao",					null, 				"instrucao",					"Instrução",							null,						null,						null));

		// LLL
		list.add(new ColumnC("latitude",					null, 				"latitude",						"Latitude",								null,						null,						null));
		list.add(new ColumnC("limitequantidade",			null,				"limiteQuantidade",				"Limite de quantidade",					null,						null,						null));
		list.add(new ColumnC("limitedata",					null,				"limiteData",					"Limite de data",						null,						null,						null));
		list.add(new ColumnC("liberadoate",					null, 				"liberadoAte",					"Liberado ate",							null,						null,						null));
		list.add(new ColumnC("linhadigitavel",				null, 				"linhaDigitavel",				"Linha digitavel",						null,						null,						null));
		list.add(new ColumnC("longitude",					null, 				"longitude",					"Longitude",							null,						null,						null));
		list.add(new ColumnC("log",							null, 				"log",							"Log",									null,						null,						null));
		list.add(new ColumnC("login",						null, 				"login",						"Login",								null,						null,						null));
		list.add(new ColumnC("logradouro",					null,				"logradouro",					"Logradouro",							null,						null,						null));
		list.add(new ColumnC("latitude1",					null,               "latitude1",					"Latitude1",							null,						null,						null));
		list.add(new ColumnC("longitude1",					null,               "longitude1",					"Longitude1",							null,						null,						null));
		list.add(new ColumnC("latitude2",					null,               "latitude2",					"Latitude2",							null,						null,						null));
		list.add(new ColumnC("longitude2",					null,               "longitude2",					"Longitude2",							null,						null,						null));
		list.add(new ColumnC("limite",						null,               "limite",						"Limite",								null,						null,						null));

		
		// MMM
		list.add(new ColumnC("mensagem",					null, 				"mensagem",						"Mensagem",								null,						null,						null));
		list.add(new ColumnC("matriz",						null,				"matriz",						"Matriz",								null,						null,						null));
		list.add(new ColumnC("modelo",						null, 				"modelo",						"Modelo",								null,						null,						null));
		list.add(new ColumnC("mensalidade",					null, 				"mensalidade",					"Mensalidade",							null,						null,						null));
		list.add(new ColumnC("messageid",					null, 				"messageId",					"ID message",							null,						null,						null));
		list.add(new ColumnC("mime",						null, 				"mime",							"Mime",									null,						null,						null));
		list.add(new ColumnC("modalidade",					null, 				"modalidade",					"Modalidade",							null,						null,						null));

		
		// NNN
		list.add(new ColumnC("numero",						null, 				"numero",						"Numero",								null,						null,						null));
		list.add(new ColumnC("nome",						null, 				"nome",							"Nome",									null,						null,						null));
		list.add(new ColumnC("nomerazao",					null,				"nomeRazao",					"Nome/Razao",							null,						null,						null));
		list.add(new ColumnC("nascimento",					null, 				"nascimento",					"Nascimento",							null,						null,						null));
		list.add(new ColumnC("natureza",					null, 				"natureza",						"Natureza",								null,						null,						null));
		list.add(new ColumnC("numerolote",					null, 				"numeroLote",					"Numero do lote",						null,						null,						null));
		list.add(new ColumnC("nossonumero",					null, 				"nossoNumero",					"Nosso numero",							null,						null,						null));
		list.add(new ColumnC("nossonumerodigito",			null, 				"nossoNumeroDigito",			"Nosso numero digito",					null,						null,						null));
		
		


		
		// OOO
		list.add(new ColumnC("origem",					    null,				"origem",						"Origem",								null,						null,						null));
		list.add(new ColumnC("origemtag",					null,				"origemTag",					"Origem tag",							null,						null,						null));
		list.add(new ColumnC("original",					null, 				"original",						"Original",								null,						null,						null));
		list.add(new ColumnC("observacao",					null, 				"observacao",					"Observação",							null,						null,						null));
		list.add(new ColumnC("obrigatorio",					null, 				"obrigatorio",					"Obrigatorio",							null,						null,						null));
		list.add(new ColumnC("origemnomerazao",				null, 				"origemNomeRazao",				"Origem nome razao",					null,						null,						null));
		list.add(new ColumnC("origemcpfcnpj",				null, 				"origemCpfCnpj",				"Origem cpfcnpj",						null,						null,						null));
		list.add(new ColumnC("origemie",					null, 				"origemIe",						"Origem ie",							null,						null,						null));
		list.add(new ColumnC("origemlogradouro",			null, 				"origemLogradouro",				"Origem logradouro",					null,						null,						null));
		list.add(new ColumnC("origemnumero",				null, 				"origemNumero",					"Origem numero",						null,						null,						null));
		list.add(new ColumnC("origembairro",				null, 				"origemBairro",					"Origem bairro",						null,						null,						null));
		list.add(new ColumnC("origemcep",					null, 				"origemCep",					"Origem cep",							null,						null,						null));

		
		
		// PPP
		list.add(new ColumnC("precoordem",					null,				"precoOrdem",					"Ordem do preço",						null,						null,						null));
		list.add(new ColumnC("pagamento",					null, 				"pagamento",					"Pagamento",							null,						null,						null));
		list.add(new ColumnC("pontuacao",					null, 				"pontuacao",					"Pontuação",							null,						null,						null));
		list.add(new ColumnC("poligono",					null, 				"poligono",						"Poligono",								null,						null,						null));
		list.add(new ColumnC("proprietario",				null, 				"proprietario",					"Proprietario",							null,						null,						null));
		list.add(new ColumnC("parcelamentotag",				null,				"parcelamentoTag",				"Parcelamento tag",						null,						null,						null));
		list.add(new ColumnC("precocusto",				    null,				"precoCusto",					"Preço custo",							null,						null,						null));
		list.add(new ColumnC("precovenda",				    null,				"precoVenda",					"Preço venda",							null,						null,						null));
		list.add(new ColumnC("precopromocao",			    null,				"precoPromocao",				"Preço promoção",						null,						null,						null));
		list.add(new ColumnC("pesoliquido",				    null,				"pesoLiquido",					"Peso líquido",							null,						null,						null));
		list.add(new ColumnC("pesobruto",				    null,				"pesoBruto",					"Peso bruto",							null,						null,						null));
		list.add(new ColumnC("pes_vendedor_id",				null, 				"pessoaVendedor",				"Pessoa vendedor",						null,						null,						null));
		list.add(new ColumnC("pes_atendente_id",			null,				"pessoaAtendente",				"Pessoa atendente",						null,						null,						null));
		list.add(new ColumnC("pes_transportador_id",		null, 				"pessoaTransportador",			"Pessoa transportador",					null,						null,						null));
		list.add(new ColumnC("pes_cancelou_id",				null, 				"pessoaCancelou",				"Pessoa cancelo",						null,						null,						null));
		list.add(new ColumnC("pes_taxista_id",				null,               "taxista",						"Taxista",								null,						null,						null));
		list.add(new ColumnC("pes_promotora_id",			null,               "promotora",					"Promotora",							null,						null,						null));

		list.add(new ColumnC("reg_origem_id",				null,               "regiaoOrigem",					"Região origem",						null,						null,						null));
		list.add(new ColumnC("reg_destino_id",				null,               "regiaoDestino",				"Região destino",						null,						null,						null));

		
		list.add(new ColumnC("pes_cadastrou_id",			null,               "pessoaCadastrou",				"Pessoa cadastrou",						null,						null,						null));
		list.add(new ColumnC("pes_representante_id",		null,               "pessoaRepresentante",			"Pessoa representante",					null,						null,						null));
		list.add(new ColumnC("principal",					null,  				"principal",					"Principal",							null,						null,						null));
		list.add(new ColumnC("protocolo",					null, 				"protocolo",					"Protocolo",							null,						null,						null));
		list.add(new ColumnC("posicao",						null, 				"posicao",						"Posição",								null,						null,						null));
		list.add(new ColumnC("programador",					null,				"programador",					"Programador",							null,						null,						null));
		list.add(new ColumnC("protestodias",				null, 				"protestoDias",					"Protesto dias",						null,						null,						null));
		list.add(new ColumnC("perccomissao",				null, 				"percComissao",					"Perc comissão",						null,						null,						null));
		list.add(new ColumnC("placa",						null, 				"placa",						"Placa",								null,						null,						null));
		list.add(new ColumnC("placauf",						null, 				"placauf",						"Placa Uf",								null,						null,						null));
		list.add(new ColumnC("paymentid",					null,               "paymentId",					"Payment ID",							null,						null,						null));


		// QQQ
		list.add(new ColumnC("quantidade",					null, 				"quantidade",					"Quantidade",							null,						null,						null));
		list.add(new ColumnC("qtdusuario",					null, 				"qtdUsuario",					"Qtd usuario",							null,						null,						null));
		list.add(new ColumnC("qtdnfe",						null, 				"qtdNfe",						"Qtd nfe",								null,						null,						null));
		list.add(new ColumnC("qtdboleto",					null, 				"qtdBoleto",					"Qtd boleto",							null,						null,						null));

		// RRR
		list.add(new ColumnC("rota",					    null,				"rota",							"Rota",									null,						null,						null));
		list.add(new ColumnC("rg",							null,				"rg",							"Rg",									null,						null,						null));
		list.add(new ColumnC("referencia",					null, 				"referencia",					"Referencia",							null,						null,						null));
		list.add(new ColumnC("receitadespesa",				null, 				"receitaDespesa",				"receitaDespesa",						null,						null,						null));
		list.add(new ColumnC("rginscricao",					null,               "rgInscricao",					"Rg/Inscricao",							null,						null,						null));

		// SSS
		list.add(new ColumnC("sigla",						null, 				"sigla",						"Sigla",								null,						null,						null));
		list.add(new ColumnC("senha",						null,				"senha",						"Senha",								null,						null,						null));
		list.add(new ColumnC("saldosms",					null, 				"saldosms",						"SaldoSms",								null,						null,						null));
		list.add(new ColumnC("situacao",					null, 				"situacao",						"Situação",								null,						null,						null));
		list.add(new ColumnC("saldoinicial",				null, 				"saldoInicial",					"Saldoinicial",							null,						null,						null));
		list.add(new ColumnC("saldo",						null, 				"saldo",						"Saldo",								null,						null,						null));
		list.add(new ColumnC("serie",						null, 				"serie",						"Serie",								null,						null,						null));
		list.add(new ColumnC("seletor",						null, 				"seletor",						"Seletor",								null,						null,						null));
		list.add(new ColumnC("status",						null, 				"status",						"Status",								null,						null,						null));
		list.add(new ColumnC("sql",							null,               "sql",							"Sql",									null,						null,						null));
		list.add(new ColumnC("shopping",					null,               "shopping",						"É Shopping",							null,						null,						null));

		
		// TTT
		list.add(new ColumnC("telefone1",					null,				"telefone1",					"Telefone1",							null,						null,						null));
		list.add(new ColumnC("telefone2",					null,				"telefone2",					"Telefone2",							null,						null,						null));
		list.add(new ColumnC("telefone3",					null,				"telefone3",					"Telefone3",							null,						null,						null));
		list.add(new ColumnC("telefone",					null, 				"telefone",						"Telefone",								null,						null,						null));
		list.add(new ColumnC("tag",							null, 				"tag",							"Tag",									null,						null,						null));
		list.add(new ColumnC("texto",						null, 				"texto",						"Texto",								null,						null,						null));
		list.add(new ColumnC("tipoemissao",					null, 				"tipoEmissao",					"Tipo de emissao",						null,						null,						null));
		list.add(new ColumnC("tipopagamento",				null,               "tipoPagamento",				"Tipo de Pagamento",					null,						null,						null));
		list.add(new ColumnC("tamanho",						null, 				"tamanho",						"Tamanho",								null,						null,						null));
		list.add(new ColumnC("transacao",					null, 				"transacao",					"Transação",							null,						null,						null));
		list.add(new ColumnC("titulo",						null, 				"titulo",						"Titulo",								null,						null,						null));
		list.add(new ColumnC("tipo",						null, 				"tipo",							"Tipo",									null,						null,						null));
		list.add(new ColumnC("transpnomerazao",				null, 				"transpNomeRazao",				"Transp nomerazao",						null,						null,						null));
		list.add(new ColumnC("transpcpfcnpj",				null, 				"transpCpfCnpj",				"Transp cpfcnpj",						null,						null,						null));
		list.add(new ColumnC("transpie",					null, 				"transpIe",						"Transp ie",							null,						null,						null));
		list.add(new ColumnC("transplogradouro",			null, 				"transpLogradouro",				"Transp logradouro",					null,						null,						null));
		list.add(new ColumnC("transpnumero",				null, 				"transpNumero",					"Transp numero",						null,						null,						null));
		list.add(new ColumnC("transpbairro",				null, 				"transpBairro",					"Transp bairro",						null,						null,						null));
		list.add(new ColumnC("transpcep",					null, 				"transpCep",					"Transp cep",							null,						null,						null));
		list.add(new ColumnC("transpmodalidade",			null, 				"transpModalidade",				"Transp modalidade",					null,						null,						null));
		list.add(new ColumnC("transpplaca",					null, 				"transpPlaca",					"Transp placa",							null,						null,						null));
		list.add(new ColumnC("transpplacauf",				null, 				"transpPlacaUf",				"Transp placauf",						null,						null,						null));
		list.add(new ColumnC("transpquantidade",			null, 				"transpQuantidade",				"Transp quantidade",					null,						null,						null));
		list.add(new ColumnC("transpespecie",				null, 				"transpEspecie",				"Transp especie",						null,						null,						null));
		list.add(new ColumnC("transpmarca",					null, 				"transpMarca",					"Transp marca",							null,						null,						null));
		list.add(new ColumnC("transpnumeracao",				null, 				"transpNumeracao",				"Transp numeração",						null,						null,						null));
		list.add(new ColumnC("transppeso",					null, 				"transpPeso",					"Transp peso",							null,						null,						null));
		list.add(new ColumnC("tipoassociado",				null,               "tipoAssociado",				"Tipo do Associado",					null,						null,						null));

		
		// UUU
		list.add(new ColumnC("usado",						null, 				"usado",						"Usado",								null,						null,						null));
		list.add(new ColumnC("usuario",						null, 				"usuario",						"Usuario",								null,						null,						null));
		list.add(new ColumnC("uni_cliente_id",				null,               "unidadeCliente",				"Unidade Cliente",						null,						null,						null));
		list.add(new ColumnC("uf",							null,               "uf",							"UF",									null,						null,						null));

		// VVV
		list.add(new ColumnC("valorpago",					null,				"valorPago",					"Valor pago",							null,						null,						null));
		list.add(new ColumnC("valor1",						null, 				"valor1",						"Valor 1",								null,						null,						null));
		list.add(new ColumnC("valor2",						null, 				"valor2",						"Valor 2",								null,						null,						null));
		list.add(new ColumnC("valor3",						null, 				"valor3",						"Valor 3",								null,						null,						null));
		list.add(new ColumnC("valordescontototal",			null,				"valorDescontoTotal",			"Valor desconto total",					null,						null,						null));
		list.add(new ColumnC("valorunitario",				null, 				"valorUnitario",				"Valor unitario",						null,						null,						null));
		list.add(new ColumnC("valortotal",					null, 				"valorTotal",					"Valor total",							null,						null,						null));
		list.add(new ColumnC("valornormal",					null,				"valorNormal",					"Valor normal",							null,						null,						null));
		list.add(new ColumnC("valoroferta",					null,				"valorOferta",					"Valor da oferta",						null,						null,						null));
		list.add(new ColumnC("valorliquido",				null,				"valorLiquido",					"Valor liquido",						null,						null,						null));
		list.add(new ColumnC("valor",						null, 				"valor",						"Valor",								null,						null,						null));
		list.add(new ColumnC("vencimento",					null,				"vencimento",					"Vencimento",							null,						null,						null));
		list.add(new ColumnC("visivel",						null, 				"visivel",						"Visivel",								null,						null,						null));
		list.add(new ColumnC("validade",					null, 				"validade",						"Validade",								null,						null,						null));
		list.add(new ColumnC("valordescontounitario",		null,				"valorDescontoUnitario",		"Valor desconto unitário",				null,						null,						null));
		list.add(new ColumnC("versao",						null, 				"versao",						"Versao",								null,						null,						null));
		list.add(new ColumnC("valormensal",					null, 				"valorMensal",					"Valor mensal",							null,						null,						null));
		list.add(new ColumnC("valoranual",					null, 				"valorAnual",					"Valor anual",							null,						null,						null));
		//
		
		

		

		list.add(new ColumnC("valoricms",					null, 				"valorIcms",					"valor icms",							null,						null,						null));
		list.add(new ColumnC("valoricmsst",					null, 				"valorIcmsSt",					"Valor icms st",						null,						null,						null));
		list.add(new ColumnC("valorpis",					null, 				"valorPis",						"Valor pis",							null,						null,						null));
		list.add(new ColumnC("valorcofins",					null, 				"valorCofins",					"Valor cofins",							null,						null,						null));
		list.add(new ColumnC("valoripi",					null, 				"valorIpi",						"Valor ipi",							null,						null,						null));
		list.add(new ColumnC("valorunitariocusto",			null, 				"valorUnitarioCusto",			"Valor unitario custo",					null,						null,						null));
		list.add(new ColumnC("valortotalcusto",				null,  				"valorTotalCusto",				"Valor total custo",					null,						null,						null));
		list.add(new ColumnC("valorcomissao",				null, 				"valorComissao",				"Valor comissão",						null,						null,						null));

		

		// XXX
		list.add(new ColumnC("xml",							null, 				"xml",							"Xml",									null,						null,						null));

		

		list.add(new ColumnC("pes_dizimista_id",			null,               "pessoaDizimista",				"Pessoa Dizimista",						null,						null,						null));
		list.add(new ColumnC("pes_usuario_id",				null,               "pessoaUsuario",				"Pessoa Usuario",						null,						null,						null));
		list.add(new ColumnC("descmensalidade",				null,               "descontoMensalidade",			"Desconto Mensalidade",					null,						null,						null));
		list.add(new ColumnC("dataatualizado",				null,               "dataAtualizado",				"Data Atualizado",						null,						null,						null));
		list.add(new ColumnC("horaatualizado",				null,               "horaAtualizado",				"Hora Atualizado",						null,						null,						null));
		
		
		
		list.add(new ColumnC("usuarioativo",				null,               "usuarioAtivo",					"Usuario Ativo",						null,						null,						null));
		list.add(new ColumnC("datanascimento",				null,               "dataNascimento",				"Data Nascimento",						null,						null,						null));
		list.add(new ColumnC("profissao",					null,               "profissao",					"Profissao",							null,						null,						null));
		list.add(new ColumnC("datacasamento",				null,               "dataCasamento",				"Data casamento",						null,						null,						null));
		list.add(new ColumnC("datareferencia",				null,               "dataReferencia",				"Data referencia",						null,						null,						null));

		
		list.add(new ColumnC("localtrabalhotag",			null,               "localTrabalhoTag",				"Local Trabalho Tag",					null,						null,						null));
		list.add(new ColumnC("tamanhocamiseta",				null,               "tamanhocamiseta",				"tamanho camiseta",						null,						null,						null));
		list.add(new ColumnC("periodo",						null,               "periodo",						"Periodo",								null,						null,						null));
		list.add(new ColumnC("valorinscricaoequipe",		null,               "valorInscricaoEquipe",			"Valor inscricao equipe",				null,						null,						null));
		list.add(new ColumnC("valorinscricaoretirante",		null,               "valorInscricaoRetirante",		"Valor inscricao retirante",			null,						null,						null));
		list.add(new ColumnC("coordenador",					null,               "coordenador",					"Coordenador",							null,						null,						null));

		
		
		list.add(new ColumnC("batismo",						null,               "batismo",						"Batismo",								null,						null,						null));
		list.add(new ColumnC("eucaristia",					null,               "eucaristia",					"Eucaristia",							null,						null,						null));
		list.add(new ColumnC("crisma",						null,               "crisma",						"Crisma",								null,						null,						null));
		list.add(new ColumnC("matrimonio",					null,               "matrimonio",					"Matrimonio",							null,						null,						null));
		list.add(new ColumnC("usamedicamento",				null,               "usaMedicamento",				"Usa medicamento",						null,						null,						null));
		list.add(new ColumnC("alergia",						null,               "alergia",						"Alergia",								null,						null,						null));
		list.add(new ColumnC("intoleranciaalimento",		null,               "intoleranciaAlimento",			"Intolerancia alimento",				null,						null,						null));
		list.add(new ColumnC("comunidade",					null,               "comunidade",					"Comunidade",							null,						null,						null));
		list.add(new ColumnC("frequencia",					null,               "frequencia",					"Frequencia",							null,						null,						null));
		list.add(new ColumnC("casamento",					null,               "casamento",					"Casamento",							null,						null,						null));
		list.add(new ColumnC("usaalianca",					null,               "usaAlianca",					"Usa alianca",							null,						null,						null));
		list.add(new ColumnC("chavecasa",					null,               "chavecasa",					"Chave casa",							null,						null,						null));
		list.add(new ColumnC("musica",						null,               "musica",						"Musica",								null,						null,						null));
		list.add(new ColumnC("servo",						null,               "servo",						"Servo",								null,						null,						null));
		list.add(new ColumnC("interna",						null,               "interna",						"Interna",								null,						null,						null));
		list.add(new ColumnC("sexo",						null,               "sexo",							"Sexo",									null,						null,						null));
		list.add(new ColumnC("vidareligiosa",				null,               "vidaReligiosa",				"Vida Religiosa",						null,						null,						null));
		list.add(new ColumnC("idade",						null,               "idade",						"Idade",								null,						null,						null));

		list.add(new ColumnC("pes_marido_id",				null,               "pessoaMarido",					"P. Marido",							null,						null,						null));
		list.add(new ColumnC("pes_esposa_id",				null,               "pessoaEsposa",					"P. Esposa",							null,						null,						null));
		list.add(new ColumnC("datamatrimonio",				null,               "dataMatrimonio",				"Matrimonio",							null,						null,						null));
		list.add(new ColumnC("base64",						null,               "base64",						"Base64",								null,						null,						null));
		list.add(new ColumnC("ordem",						null,               "ordem",						"Ordem",								null,						null,						null));
		list.add(new ColumnC("moracom",						null,               "moraCom",						"Mora com",								null,						null,						null));
		list.add(new ColumnC("jafezretiro",					null,               "jaFezRetiro",					"Ja Fez Retiro",						null,						null,						null));
		list.add(new ColumnC("padrao",						null,               "padrao",						"Padrão",								null,						null,						null));
		list.add(new ColumnC("dataultimoacesso",			null,               "dataUltimoAcesso",				"Data ultimo acesso",					null,						null,						null));

		list.add(new ColumnC("numerosocio",					null,               "numeroSocio",					"Numero socio",							null,						null,						null));
		list.add(new ColumnC("dataassociacao",				null,               "dataAssociacao",				"Data associação",						null,						null,						null));
		list.add(new ColumnC("pai",							null,               "pai",							"Pai",									null,						null,						null));
		list.add(new ColumnC("mae",							null,               "mae",							"Mãe",									null,						null,						null));
		list.add(new ColumnC("pis",							null,               "pis",							"Pis",									null,						null,						null));
		list.add(new ColumnC("numeroautorizacao",			null,               "numeroAutorizacao",			"Numero autorização",					null,						null,						null));
		list.add(new ColumnC("categoria",					null,               "categoria",					"Categoria",							null,						null,						null));
		list.add(new ColumnC("numeroantigo",				null,               "numeroAntigo",					"Numero antigo",						null,						null,						null));

		
		list.add(new ColumnC("cpf",							null,               "cpf",							"Cpf",									null,						null,						null));
		list.add(new ColumnC("dataatualizacao",				null,               "dataAtualizacao",				"Data atualizacao",						null,						null,						null));

		list.add(new ColumnC("fichaantigafrente",			null,               "fichaAntigaFrente",			"Ficha Antiga (frente)",				null,						null,						null));
		list.add(new ColumnC("fichaantigaverso",			null,               "fichaAntigaVerso",				"Ficha Antiga (verso)",					null,						null,						null));
		list.add(new ColumnC("anocarro",					null,               "anoCarro",						"Ano do Carro",							null,						null,						null));

		list.add(new ColumnC("nascimentofundacao",			null,               "nascimentoFundacao",			"Nascimento/Abertura",					null,						null,						null));
		list.add(new ColumnC("salarioreceita",				null,               "salarioReceita",				"Salário/Receita",						null,						null,						null));


		list.add(new ColumnC("nacionalidade",				null,               "Nacionalidade",				"Nacionalidade",						null,						null,						null));
		list.add(new ColumnC("ramoatividade",				null,               "RamoAtividade",				"Ramo de Atividade",						null,						null,						null));
		list.add(new ColumnC("tipoconta",					null,               "TipoConta",					"Tipo Conta",						null,						null,						null));
		list.add(new ColumnC("portalnome",					null,               "PortalNome",					"Portal Nome",						null,						null,						null));
		list.add(new ColumnC("ehshopping",					null,               "EhShopping",					"Eh Shopping",						null,						null,						null));
		list.add(new ColumnC("nomeshopping",				null,               "NomeShopping",					"Nome Shopping",						null,						null,						null));
		list.add(new ColumnC("outros",						null,               "Outros",						"Outros",						null,						null,						null));
		list.add(new ColumnC("tecnologia",					null,               "Tecnologia",					"Tecnologia",						null,						null,						null));
		list.add(new ColumnC("aluguel",						null,               "Aluguel",						"Aluguel",						null,						null,						null));
		list.add(new ColumnC("venda",						null,               "Venda",						"Venda",						null,						null,						null));
		list.add(new ColumnC("debito",						null,               "Debito",						"Debito",						null,						null,						null));
		list.add(new ColumnC("creditoavista",				null,               "CreditoAvista",				"Credito à Vista",				null,						null,						null));
		list.add(new ColumnC("condicao1",					null,               "Condicao1",					"Condição 2 a 6",				null,						null,						null));
		list.add(new ColumnC("condicao2",					null,               "Condicao2",					"Condição 7 a 12",				null,						null,						null));
		list.add(new ColumnC("antecipacao",					null,               "Antecipacao",					"Antecipação",					null,						null,						null));
		list.add(new ColumnC("vendedor",					null,               "Vendedor",						"Vendedor",						null,						null,						null));
		list.add(new ColumnC("fabricante",					null,               "Fabricante",					"Fabricante",					null,						null,						null));
		list.add(new ColumnC("numeroserie",					null,               "NumeroSerie",					"Número de Série",				null,						null,						null));

		list.add(new ColumnC("fisicojuridico",				null,               "fisicoJuridico",				"Físico ou Jurídico",			null,						null,						null));
		list.add(new ColumnC("vendaaluguel",				null,               "vendaAluguel",					"Venda Aluguel",				null,						null,						null));
		list.add(new ColumnC("valorvenda",					null,               "valorVenda",					"Valor de Venda",				null,						null,						null));
		list.add(new ColumnC("pes_cliente_id",				null,               "pessoaCliente",				"Cliente",						null,						null,						null));
		list.add(new ColumnC("capital",						null,               "capital",						"Capital",						null,						null,						null));
		list.add(new ColumnC("regiao",						null,               "regiao",						"Região",						null,						null,						null));
		list.add(new ColumnC("ddd",							null,               "ddd",							"DDD",							null,						null,						null));

		list.add(new ColumnC("pes_distribuidor_id",			null,               "pessoaDistribuidor",			"Distribuidor",					null,						null,						null));
		list.add(new ColumnC("pes_pontovenda_id",			null,               "pessoaPontoDeVenda",			"Ponto de Venda",				null,						null,						null));
		list.add(new ColumnC("edicao",						null,               "edicao",						"Edição",						null,						null,						null));
		list.add(new ColumnC("premios",						null,               "premios",						"Prêmios",						null,						null,						null));
		list.add(new ColumnC("imagem",						null,               "imagem",						"Imagem",						null,						null,						null));
		list.add(new ColumnC("premio",						null,               "premio",						"Prêmio",						null,						null,						null));
		list.add(new ColumnC("bolas",						null,               "bolas",						"Bolas",						null,						null,						null));
		list.add(new ColumnC("sequencia1",					null,               "sequencia1",					"Sequência 1",					null,						null,						null));
		list.add(new ColumnC("sequencia2",					null,               "sequencia2",					"Sequência 2",					null,						null,						null));
		list.add(new ColumnC("canaltag",					null,               "canalTag",						"Canal Tag",					null,						null,						null));
		list.add(new ColumnC("token",						null,               "token",						"Token",						null,						null,						null));
		list.add(new ColumnC("numerocartao",				null,               "numeroCartao",					"Número Cartão",				null,						null,						null));
		list.add(new ColumnC("pes_cliente_id",				null,               "pessoaCliente",				"Cliente",						null,						null,						null));
		list.add(new ColumnC("pes_vendedor_id",				null,               "pessoaVendedor",				"Vendedor",						null,						null,						null));
		list.add(new ColumnC("ven_ganhador",				null,               "ganhador",					    "Ganhador",						null,						null,						null));
		list.add(new ColumnC("eta_datasorteio",				null,               "dataSorteio",					"Data Sorteio",					null,						null,						null));
		list.add(new ColumnC("eta_horasorteio",				null,               "horaSorteio",					"Hora Sorteio",					null,						null,						null));
		list.add(new ColumnC("eta_datalimitevenda",			null,               "dataLimiteVenda",				"data Limite Venda",			null,						null,						null));
		list.add(new ColumnC("eta_horalimitevenda",			null,               "horaLimiteVenda",				"hora Limite Venda",			null,						null,						null));
		list.add(new ColumnC("peto_datavalidade",			null,               "dataValidade",					"Data Validade",				null,						null,						null));
		list.add(new ColumnC("peto_horavalidade",			null,               "horaValidade",					"Hora Validade",				null,						null,						null));
		list.add(new ColumnC("pes_rgdatainscricao",			null,               "rgDataInscricao",				"Rg Data Inscricao",			null,						null,						null));
		list.add(new ColumnC("pes_pinbankid",				null,               "pinbankId",					"Pinbank Id",					null,						null,						null));
		list.add(new ColumnC("ven_pinbanknsu",				null,               "pinbankNsu",					"Pinbank Nsu",					null,						null,						null));
		list.add(new ColumnC("car_datavalidade",			null,               "dataValidade",					"Data Validade",				null,						null,						null));
		list.add(new ColumnC("car_nomeapelido",				null,               "nomeApelido",					"Nome Apelido",					null,						null,						null));
		list.add(new ColumnC("car_gateway",					null,               "gateway",				    	"Gateway",						null,						null,						null));
		list.add(new ColumnC("nsu",						    null,               "nsu",					        "Nsu",					     	null,						null,						null));
		list.add(new ColumnC("tipocertificado",		    	null,               "tipoCertificado",				"Tipo Certificado",				null,						null,						null));
		list.add(new ColumnC("numero2",						null,               "numero2",					    "Numero2",						null,						null,						null));
		list.add(new ColumnC("numero1",						null,               "numero1",					    "Numero1",						null,						null,						null));
		list.add(new ColumnC("urlvideo",					null,               "urlVideo",					     "Url Video",					null,						null,						null));
		list.add(new ColumnC("ceplimite",					null,               "cepLimite",					"Cep Limite",					null,						null,						null));
		list.add(new ColumnC("compralimite",				null,               "compraLimite",					"Compra Limite",				null,						null,						null));
		list.add(new ColumnC("urlbaseimage",				null,               "urlBaseImage",					"Url Base Image",				null,						null,						null));
		list.add(new ColumnC("datareserva",					null,               "dataReserva",					"Data Reserva",					null,						null,						null));
		list.add(new ColumnC("horareserva",					null,               "horaReserva",					"Hora Reserva",					null,						null,						null));
		list.add(new ColumnC("sequencia3",					null,               "sequencia3",					"Sequencia 3",					null,						null,						null));
		list.add(new ColumnC("numero3",						null,               "numero3",					     "Número 3",					null,						null,						null));
		list.add(new ColumnC("versaoapp",					null,               "versaoApp",					"Versão App",					null,						null,						null));

		list.add(new ColumnC("bloqueadoate",				null,               "bloqueadoAte",					"Bloqueado ate",				null,						null,						null));
		list.add(new ColumnC("bloqueadoem",					null,               "bloqueadoEm",					"Bloqueado em",					null,						null,						null));
		list.add(new ColumnC("tanocarrinho",				null,               "taNoCarrinho",					"Ta no carrinho",				null,						null,						null));


		list.add(new ColumnC("validogateway",				null,               "validoGateway",				"Valido no gateway",						null,						null,						null));

		list.add(new ColumnC("gg",							null,               "gg",							"GG",						null,						null,						null));
		list.add(new ColumnC("forma_pagamento",				null,               "pagamento",					"Pagamento",						null,						null,						null));
		list.add(new ColumnC("codigo_autorizacao",			null,               "autorizacao",					"Autorização",						null,						null,						null));
		list.add(new ColumnC("nsu_gw",						null,               "nsuGw",						"Nsu Gw",						null,						null,						null));
		list.add(new ColumnC("nsu_adquirente",				null,               "nsuAdquirente",				"Nsu Adquirente",						null,						null,						null));
		list.add(new ColumnC("mensagem_gateway",				null,               "mensagemGateway",				"Mensagem Gateway",						null,						null,						null));
		list.add(new ColumnC("debt_data",					"Long",             "data",						"Data",						null,						null,						null));

		list.add(new ColumnC("gateway",						null,               "gateway",					"Gateway",						null,						null,						null));
		list.add(new ColumnC("sucesso",						null,               "sucesso",					"Sucesso",						null,						null,						null));
		list.add(new ColumnC("ip",							null,               "ip",						"IP",							null,						null,						null));
		list.add(new ColumnC("codigo_retorno",				null,               "codigoRetorno",			"Retorno",						null,						null,						null));
		list.add(new ColumnC("delay",						null,               "delay",					"Delay",						null,						null,						null));
		list.add(new ColumnC("request_response",				null,               "requestResponse",					"request_response",						null,						null,						null));
		list.add(new ColumnC("metodo",						null,               "metodo",					"Método",						null,						null,						null));

		list.add(new ColumnC("host",						null,               "host",					"host",						null,						null,						null));
		list.add(new ColumnC("versao",						null,               "versao",					"versao",						null,						null,						null));
		list.add(new ColumnC("endpoint",						null,               "endpoint",					"endpoint",						null,						null,						null));
		list.add(new ColumnC("request",						null,               "request",					"request",						null,						null,						null));
		list.add(new ColumnC("response",						null,               "response",					"response",						null,						null,						null));
		list.add(new ColumnC("http_status",						null,               "httpStatus",					"Http Status",						null,						null,						null));
		list.add(new ColumnC("time",						null,               "time",					"time",						null,						null,						null));
		list.add(new ColumnC("gateway",						null,               "gateway",					"Gateway",						null,						null,						null));
		list.add(new ColumnC("gw_sucesso",						null,               "GatewaySucesso",					"Gateway Sucesso",						null,						null,						null));
		list.add(new ColumnC("gw_codigo_retorno",						null,               "GatewayCodigoRetorno",					"Código",						null,						null,						null));
		list.add(new ColumnC("gw_mensagem",						null,               "GatewayMensagem",					"Mensagem",						null,						null,						null));
		list.add(new ColumnC("numerocartao",						null,               "numerocartao",					"numerocartao",						null,						null,						null));
		list.add(new ColumnC("bandeira",						null,               "bandeira",					"bandeira",						null,						null,						null));
		list.add(new ColumnC("data_validade",						null,               "validade",					"Validade",						null,						null,						null));
		list.add(new ColumnC("datavalidade",						null,               "dataValidade",					"Data Validade",						null,						null,						null));
		list.add(new ColumnC("nomeapelido",						null,               "nomeApelido",					"Nome Apelido",						null,						null,						null));
		list.add(new ColumnC("car_token_pinbank",						null,               "tokenPinbank",					"Token Pinbank",						null,						null,						null));
		list.add(new ColumnC("car_token_payu",						null,               "tokenPayU",					"Token Payu",						null,						null,						null));
		list.add(new ColumnC("car_token_pagseguro",						null,               "tokenPagSeguro",					"Token PagSeguro",						null,						null,						null));
		list.add(new ColumnC("car_token_braspag",						null,               "tokenBrasPag",					"Token BrasPag",						null,						null,						null));


		return list;
	}
}