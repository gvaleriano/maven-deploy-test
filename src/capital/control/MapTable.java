package capital.control;

import br.com.dotum.jedi.gen.AbstractTableMap;
import br.com.dotum.jedi.gen.TableC;

import java.util.ArrayList;
import java.util.List;

public class MapTable implements AbstractTableMap {

	public List<TableC> createList() {
		List<TableC> list = new ArrayList<TableC>();

		// AAA
		
		list.add(new TableC("arquivo",							false, 		"arq",			"Arquivo",						"Arquivo"));
		list.add(new TableC("arquivotipo",						false, 		"arqt",			"ArquivoTipo",					"Arquivo Tipo"));
		list.add(new TableC("pessoagateway",					false, 		"pesg",			"PessoaGateway",				"Pessoa Gateway"));

		list.add(new TableC("auditoria",						false,		"aud",			"Auditoria",					"Auditoria"));
		list.add(new TableC("banco",							false,		"ban",			"Banco",						"Banco"));
		list.add(new TableC("bairro",							false,		"bai",			"Bairro",						"Bairro"));
		list.add(new TableC("complemento",						false, 		"com",			"Complemento",					"Complemento"));
		list.add(new TableC("componenteestatistica",			false,		"cpte",			"ComponenteEstatistica",		"Componente Estatistica"));
		list.add(new TableC("pessoa",							false,		"pes",			"Pessoa",						"Pessoa"));
		list.add(new TableC("situacao",							false,		"sit",			"Situacao",						"Situação"));
		list.add(new TableC("unidade",							false,		"uni",			"Unidade",						"Unidade"));
		list.add(new TableC("pessoatarefa",						false,		"peta",			"PessoaTarefa",					"Pessoa Tarefa"));
		list.add(new TableC("unidade",							false, 		"uni",			"Unidade",						"Unidade"));
		list.add(new TableC("estado",							false, 		"est",			"Estado",						"Estado"));
		list.add(new TableC("cidade",							false, 		"cid",			"Cidade",						"Cidade"));
		list.add(new TableC("tarefa",							false, 		"tar",			"Tarefa",						"Tarefa"));
		list.add(new TableC("usuario",							false, 		"usu",			"Usuario",						"Usuário"));
		list.add(new TableC("componente",						false, 		"cpt",			"Componente",					"Componente"));
		list.add(new TableC("componentetipo",					false, 		"cptt",			"ComponenteTipo",				"Tipo de componente"));
		list.add(new TableC("regiao",							false, 		"reg",			"Regiao",						"Região"));
		list.add(new TableC("auditoriatipo",					false, 		"audt",			"AuditoriaTipo",				"Auditoria Tipo"));
		list.add(new TableC("pessoatipo",						false, 		"pest",			"PessoaTipo",					"Tipo de Pessoa"));
		list.add(new TableC("pessoaenderecotipo",				false, 		"peset",		"PessoaEnderecoTipo",				"Pessoa Endereço Tipo"));
		list.add(new TableC("pessoaendereco",					false, 		"pese",			"PessoaEndereco",				"Pessoa Endereço"));
		list.add(new TableC("pessoatipopessoa",					false, 		"ptp",			"PessoaTipoPessoa",				"Pessoa Tipo Pessoa"));
		list.add(new TableC("pessoaarquivo",					false, 		"pesa",			"PessoaArquivo",				"Pessoa Arquivo"));
		list.add(new TableC("pessoaarquivotipo",				false, 		"pesat",		"PessoaArquivoTipo",			"Pessoa Arquivo Tipo"));
		list.add(new TableC("logradouro",						false, 		"log",			"Logradouro",					"Logradouro"));
		list.add(new TableC("pessoadistribuidor",				false, 		"pesd",			"PessoaDistribuidor",			"PessoaDistribuidor"));
		list.add(new TableC("etapa",							false, 		"eta",			"Etapa",						"Etapa"));
		list.add(new TableC("etapasorteio",						false, 		"etas",			"EtapaSorteio",						"Etapa"));
		list.add(new TableC("etapaimagem",						false, 		"etai",			"EtapaImagem",						"Etapa"));
		list.add(new TableC("certificado",						false, 		"cer",			"Certificado",						"Etapa"));
		list.add(new TableC("certificadopessoa",				false, 		"cerp",			"CertificadoPessoa",						"Etapa"));
		list.add(new TableC("carteira",							false, 		"car",			"Carteira",						"Etapa"));
		list.add(new TableC("venda",							false, 		"ven",			"Venda",						"Etapa"));

		list.add(new TableC("etapapremio",						false, 		"etap",			"EtapaPremio",					"Premio da etapa"));
		list.add(new TableC("pessoatoken",					    false, 		"peto",			"PessoaToken",				    "Pessoa Token"));
		list.add(new TableC("versao",					        false, 		"ver",			"Versao",			         	"Versão"));
		list.add(new TableC("messageui",						false, 		"msg",			"MessageUI",				    "Message UI"));
		list.add(new TableC("debitotoken",						false, 		"debt",			"DebitoToken",				    "Débito Token"));
		list.add(new TableC("request",					false, 		"req",			"Request",				"Request"));

		return list;
	}
	
	
}

