package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Arquivo", schema="sys", name="arquivo", acronym="arq", typePk="S", sequence="sarq_id")
public class ArquivoBean extends Bean {

	@ColumnDB(name="arq_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="arqt_id", pk=false, fkName="fk_arq_arqt", fk="arqt_id = arqt_id", length=10, precision=0)
	@FieldDB(name="ArquivoTipo.Id", label="Arquivo Tipo", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:arquivotipo")
	private ArquivoTipoBean arquivoTipo;

	@ColumnDB(name="arq_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="arq_extensao", pk=false, length=10, precision=0)
	@FieldDB(name="Extensao", label="Extensao", required=false, type= TableFieldEditType.TEXTFIELD)
	private String extensao;

	@ColumnDB(name="arq_arquivo", pk=false, length=999999, precision=0)
	@FieldDB(name="Arquivo", label="Arquivo", required=false, type= TableFieldEditType.TEXTFIELD)
	private String arquivo;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public ArquivoBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public ArquivoBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public ArquivoBean(Integer id, ArquivoTipoBean arquivoTipo) {
		super();
		this.id = id;
		this.arquivoTipo = arquivoTipo;
	}

	/** 
	 *
	 */
	public ArquivoBean(Integer id, ArquivoTipoBean arquivoTipo, String descricao, String extensao, String arquivo) {
		super();
		this.id = id;
		this.arquivoTipo = arquivoTipo;
		this.descricao = descricao;
		this.extensao = extensao;
		this.arquivo = arquivo;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setArquivoTipo(ArquivoTipoBean arquivoTipo) {
		this.arquivoTipo = arquivoTipo;
	}

	public ArquivoTipoBean getArquivoTipo() {
		if (arquivoTipo == null) arquivoTipo = new ArquivoTipoBean();
		return arquivoTipo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}

	public String getExtensao() {
		return extensao;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

	public String getArquivo() {
		return arquivo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArquivoBean other = (ArquivoBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
