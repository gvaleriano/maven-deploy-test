package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Arquivo Tipo", schema="sys", name="arquivotipo", acronym="arqt", typePk="S", sequence="sarqt_id")
public class ArquivoTipoBean extends Bean {

	@ColumnDB(name="arqt_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="arqt_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="arqt_tag", pk=false, length=100, precision=0)
	@FieldDB(name="Tag", label="Tag", required=false, type= TableFieldEditType.TEXTFIELD)
	private String tag;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public ArquivoTipoBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public ArquivoTipoBean(Integer id) {
		super();
		this.id = id;
	}
	

	public ArquivoTipoBean(Integer id, String descricao, String tag) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.tag = tag;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTag() {
		return tag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArquivoTipoBean other = (ArquivoTipoBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
