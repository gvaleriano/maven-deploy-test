package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Bairro", schema="sys", name="bairro", acronym="bai", typePk="S", sequence="sbai_id")
public class BairroBean extends Bean {

	@ColumnDB(name="bai_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="cid_id", pk=false, fkName="fk_bai_cid", fk="cid_id = cid_id", length=10, precision=0)
	@FieldDB(name="Cidade.Id", label="Cidade", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:cidade")
	private CidadeBean cidade;

	@ColumnDB(name="bai_nome", pk=false, length=250, precision=0)
	@FieldDB(name="Nome", label="Nome", required=false, type= TableFieldEditType.TEXTFIELD)
	private String nome;

	@ColumnDB(name="bai_latitude", pk=false, length=50, precision=0)
	@FieldDB(name="Latitude", label="Latitude", required=false, type= TableFieldEditType.TEXTFIELD)
	private String latitude;

	@ColumnDB(name="bai_longitude", pk=false, length=50, precision=0)
	@FieldDB(name="Longitude", label="Longitude", required=false, type= TableFieldEditType.TEXTFIELD)
	private String longitude;

	public BairroBean() {
		super();
	}

	public BairroBean(Integer id) {
		super();
		this.id = id;
	}

	public BairroBean(Integer id, CidadeBean cidade, String nome, String latitude, String longitude) {
		super();
		this.id = id;
		this.cidade = cidade;
		this.nome = nome;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setCidade(CidadeBean cidade) {
		this.cidade = cidade;
	}

	public CidadeBean getCidade() {
		if (cidade == null) cidade = new CidadeBean();
		return cidade;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLongitude() {
		return longitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BairroBean other = (BairroBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
