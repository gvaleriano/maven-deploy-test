package capital.model.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;
import br.com.dotum.jedi.core.db.AbstractTableEnum;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;

@TableDB(label="Etapa", schema="sys", name="carteira", acronym="car", typePk="S", sequence="scar_id")
public class CarteiraBean extends Bean {

	@ColumnDB(name="car_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_car_pes", fk="pes_id = pes_id", length=15, precision=0)
	@FieldDB(name="Pessoa.Id", label="Pessoa", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoa;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_car_uni", fk="uni_id = uni_id", length=15, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="car_data", pk=false, length=0, precision=0)
	@FieldDB(name="Data", label="Data", required=true, type=TableFieldEditType.DATEFIELD)
	private Date data;

	@ColumnDB(name="car_hora", pk=false, length=8, precision=0)
	@FieldDB(name="Hora", label="Hora", required=true, type=TableFieldEditType.HOURFIELD)
	private String hora;

	@ColumnDB(name="car_token", pk=false, length=250, precision=0)
	@FieldDB(name="Token", label="Token", required=false, type=TableFieldEditType.TEXTFIELD)
	private String token;

	@ColumnDB(name="car_numerocartao", pk=false, length=50, precision=0)
	@FieldDB(name="NumeroCartao", label="Número Cartão", required=true, type=TableFieldEditType.TEXTFIELD)
	private String numeroCartao;

	@ColumnDB(name="car_bandeira", pk=false, length=250, precision=0)
	@FieldDB(name="Bandeira", label="Bandeira", required=false, type=TableFieldEditType.TEXTFIELD)
	private String bandeira;

	@ColumnDB(name="car_data_validade", pk=false, length=250, precision=0)
	@FieldDB(name="Validade", label="Validade", required=false, type=TableFieldEditType.TEXTFIELD)
	private String validade;

	@ColumnDB(name="sit_id", pk=false, fkName="fk_car_sit", fk="sit_id = sit_id", length=10, precision=0)
	@FieldDB(name="Situacao.Id", label="Situação", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:situacao")
	private SituacaoBean situacao;

	@ColumnDB(name="car_datavalidade", pk=false, length=100, precision=0)
	@FieldDB(name="DataValidade", label="Data Validade", required=false, type=TableFieldEditType.TEXTFIELD)
	private String dataValidade;

	@ColumnDB(name="car_nomeapelido", pk=false, length=100, precision=0)
	@FieldDB(name="NomeApelido", label="Nome Apelido", required=false, type=TableFieldEditType.TEXTFIELD)
	private String nomeApelido;

	@ColumnDB(name="car_gateway", pk=false, length=50, precision=0)
	@FieldDB(name="Gateway", label="Gateway", required=false, type=TableFieldEditType.TEXTFIELD)
	private String gateway;

	@ColumnDB(name="car_transacao", pk=false, length=500, precision=0)
	@FieldDB(name="Transacao", label="Transação", required=false, type=TableFieldEditType.TEXTFIELD)
	private String transacao;

	@ColumnDB(name="car_validogateway", pk=false, length=1, precision=0)
	@FieldDB(name="ValidoGateway", label="Valido no gateway", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer validoGateway;

	@ColumnDB(name="car_token_pinbank", pk=false, length=100, precision=0)
	@FieldDB(name="TokenPinbank", label="Token Pinbank", required=false, type=TableFieldEditType.TEXTFIELD)
	private String tokenPinbank;

	@ColumnDB(name="car_token_payu", pk=false, length=100, precision=0)
	@FieldDB(name="TokenPayU", label="Token Payu", required=false, type=TableFieldEditType.TEXTFIELD)
	private String tokenPayU;

	@ColumnDB(name="car_token_pagseguro", pk=false, length=100, precision=0)
	@FieldDB(name="TokenPagSeguro", label="Token PagSeguro", required=false, type=TableFieldEditType.TEXTFIELD)
	private String tokenPagSeguro;

	@ColumnDB(name="car_token_braspag", pk=false, length=100, precision=0)
	@FieldDB(name="TokenBrasPag", label="Token BrasPag", required=false, type=TableFieldEditType.TEXTFIELD)
	private String tokenBrasPag;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public CarteiraBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public CarteiraBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public CarteiraBean(Integer id, PessoaBean pessoa, UnidadeBean unidade, Date data, String hora, String numeroCartao) {
		super();
		this.id = id;
		this.pessoa = pessoa;
		this.unidade = unidade;
		this.data = data;
		this.hora = hora;
		this.numeroCartao = numeroCartao;
	}

	/** 
	 *
	 */
	public CarteiraBean(Integer id, PessoaBean pessoa, UnidadeBean unidade, Date data, String hora, String token, String numeroCartao, String bandeira, String validade, SituacaoBean situacao, String dataValidade, String nomeApelido, String gateway, String transacao, Integer validoGateway, String tokenPinbank, String tokenPayU, String tokenPagSeguro, String tokenBrasPag) {
		super();
		this.id = id;
		this.pessoa = pessoa;
		this.unidade = unidade;
		this.data = data;
		this.hora = hora;
		this.token = token;
		this.numeroCartao = numeroCartao;
		this.bandeira = bandeira;
		this.validade = validade;
		this.situacao = situacao;
		this.dataValidade = dataValidade;
		this.nomeApelido = nomeApelido;
		this.gateway = gateway;
		this.transacao = transacao;
		this.validoGateway = validoGateway;
		this.tokenPinbank = tokenPinbank;
		this.tokenPayU = tokenPayU;
		this.tokenPagSeguro = tokenPagSeguro;
		this.tokenBrasPag = tokenBrasPag;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPessoa(PessoaBean pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaBean getPessoa() {
		if (pessoa == null) pessoa = new PessoaBean();
		return pessoa;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getData() {
		return data;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getHora() {
		return hora;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	public String getBandeira() {
		return bandeira;
	}

	public void setValidade(String validade) {
		this.validade = validade;
	}

	public String getValidade() {
		return validade;
	}

	public void setSituacao(SituacaoBean situacao) {
		this.situacao = situacao;
	}

	public SituacaoBean getSituacao() {
		if (situacao == null) situacao = new SituacaoBean();
		return situacao;
	}

	public void setDataValidade(String dataValidade) {
		this.dataValidade = dataValidade;
	}

	public String getDataValidade() {
		return dataValidade;
	}

	public void setNomeApelido(String nomeApelido) {
		this.nomeApelido = nomeApelido;
	}

	public String getNomeApelido() {
		return nomeApelido;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getGateway() {
		return gateway;
	}

	public void setTransacao(String transacao) {
		this.transacao = transacao;
	}

	public String getTransacao() {
		return transacao;
	}

	public void setValidoGateway(Integer validoGateway) {
		this.validoGateway = validoGateway;
	}

	public Integer getValidoGateway() {
		return validoGateway;
	}

	public void setTokenPinbank(String tokenPinbank) {
		this.tokenPinbank = tokenPinbank;
	}

	public String getTokenPinbank() {
		return tokenPinbank;
	}

	public void setTokenPayU(String tokenPayU) {
		this.tokenPayU = tokenPayU;
	}

	public String getTokenPayU() {
		return tokenPayU;
	}

	public void setTokenPagSeguro(String tokenPagSeguro) {
		this.tokenPagSeguro = tokenPagSeguro;
	}

	public String getTokenPagSeguro() {
		return tokenPagSeguro;
	}

	public void setTokenBrasPag(String tokenBrasPag) {
		this.tokenBrasPag = tokenBrasPag;
	}

	public String getTokenBrasPag() {
		return tokenBrasPag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarteiraBean other = (CarteiraBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
