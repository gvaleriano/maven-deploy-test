package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

import java.math.BigDecimal;

@TableDB(label="Etapa", schema="sys", name="certificado", acronym="cer", typePk="S", sequence="scer_id")
public class CertificadoBean extends Bean {

	@ColumnDB(name="cer_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="eta_id", pk=false, fkName="fk_cer_eta", fk="eta_id = eta_id", length=15, precision=0)
	@FieldDB(name="Etapa.Id", label="Etapa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:etapa")
	private EtapaBean etapa;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_cer_uni", fk="uni_id = uni_id", length=15, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="cer_numero1", pk=false, length=10, precision=0)
	@FieldDB(name="Numero1", label="Numero1", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer numero1;

	@ColumnDB(name="cer_sequencia1", pk=false, length=250, precision=0)
	@FieldDB(name="Sequencia1", label="Sequência 1", required=true, type= TableFieldEditType.TEXTFIELD)
	private String sequencia1;

	@ColumnDB(name="cer_sequencia2", pk=false, length=250, precision=0)
	@FieldDB(name="Sequencia2", label="Sequência 2", required=false, type= TableFieldEditType.TEXTFIELD)
	private String sequencia2;

	@ColumnDB(name="cer_valor", pk=false, length=15, precision=2)
	@FieldDB(name="Valor", label="Valor", required=true, type= TableFieldEditType.DECIMALFIELD)
	private BigDecimal valor;

	@ColumnDB(name="cer_numero2", pk=false, length=250, precision=0)
	@FieldDB(name="Numero2", label="Numero2", required=false, type= TableFieldEditType.TEXTFIELD)
	private String numero2;

	@ColumnDB(name="cer_sequencia3", pk=false, length=250, precision=0)
	@FieldDB(name="Sequencia3", label="Sequencia 3", required=false, type= TableFieldEditType.TEXTFIELD)
	private String sequencia3;

	@ColumnDB(name="cer_numero3", pk=false, length=10, precision=0)
	@FieldDB(name="Numero3", label="Número 3", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer numero3;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public CertificadoBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public CertificadoBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public CertificadoBean(Integer id, EtapaBean etapa, UnidadeBean unidade, Integer numero1, String sequencia1, BigDecimal valor) {
		super();
		this.id = id;
		this.etapa = etapa;
		this.unidade = unidade;
		this.numero1 = numero1;
		this.sequencia1 = sequencia1;
		this.valor = valor;
	}

	/** 
	 *
	 */
	public CertificadoBean(Integer id, EtapaBean etapa, UnidadeBean unidade, Integer numero1, String sequencia1, String sequencia2, BigDecimal valor, String numero2, String sequencia3, Integer numero3) {
		super();
		this.id = id;
		this.etapa = etapa;
		this.unidade = unidade;
		this.numero1 = numero1;
		this.sequencia1 = sequencia1;
		this.sequencia2 = sequencia2;
		this.valor = valor;
		this.numero2 = numero2;
		this.sequencia3 = sequencia3;
		this.numero3 = numero3;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setEtapa(EtapaBean etapa) {
		this.etapa = etapa;
	}

	public EtapaBean getEtapa() {
		if (etapa == null) etapa = new EtapaBean();
		return etapa;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setNumero1(Integer numero1) {
		this.numero1 = numero1;
	}

	public Integer getNumero1() {
		return numero1;
	}

	public void setSequencia1(String sequencia1) {
		this.sequencia1 = sequencia1;
	}

	public String getSequencia1() {
		return sequencia1;
	}

	public void setSequencia2(String sequencia2) {
		this.sequencia2 = sequencia2;
	}

	public String getSequencia2() {
		return sequencia2;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setNumero2(String numero2) {
		this.numero2 = numero2;
	}

	public String getNumero2() {
		return numero2;
	}

	public void setSequencia3(String sequencia3) {
		this.sequencia3 = sequencia3;
	}

	public String getSequencia3() {
		return sequencia3;
	}

	public void setNumero3(Integer numero3) {
		this.numero3 = numero3;
	}

	public Integer getNumero3() {
		return numero3;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CertificadoBean other = (CertificadoBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
