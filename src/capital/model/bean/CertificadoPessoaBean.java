package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

import java.util.Date;

@TableDB(label="Etapa", schema="sys", name="certificadopessoa", acronym="cerp", typePk="S", sequence="scerp_id")
public class CertificadoPessoaBean extends Bean {

	@ColumnDB(name="cerp_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="cer_id", pk=false, fkName="fk_cerp_cer", fk="cer_id = cer_id", length=15, precision=0)
	@FieldDB(name="Certificado.Id", label="Etapa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:certificado")
	private CertificadoBean certificado;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_cerp_uni", fk="uni_id = uni_id", length=15, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="pes_cliente_id", pk=false, fkName="fk_cerp_pes_cliente_id", fk="pes_cliente_id = pes_id", length=15, precision=0)
	@FieldDB(name="PessoaCliente.Id", label="Pessoa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoaCliente;

	@ColumnDB(name="sit_id", pk=false, fkName="fk_cerp_sit", fk="sit_id = sit_id", length=15, precision=0)
	@FieldDB(name="Situacao.Id", label="Situação", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:situacao")
	private SituacaoBean situacao;

	@ColumnDB(name="pes_vendedor_id", pk=false, fkName="fk_cerp_pes_vendedor_id", fk="pes_vendedor_id = pes_id", length=15, precision=0)
	@FieldDB(name="PessoaVendedor.Id", label="Pessoa", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoaVendedor;

	@ColumnDB(name="cerp_bloqueadoate", pk=false, length=15, precision=0)
	@FieldDB(name="BloqueadoAte", label="Bloqueado ate", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer bloqueadoAte;

	@ColumnDB(name="cerp_bloqueadoem", pk=false, length=0, precision=0)
	@FieldDB(name="BloqueadoEm", label="Bloqueado em", required=false, type= TableFieldEditType.DATEFIELD)
	private Date bloqueadoEm;

	@ColumnDB(name="cerp_datareserva", pk=false, length=0, precision=0)
	@FieldDB(name="DataReserva", label="Data Reserva", required=false, type= TableFieldEditType.DATEFIELD)
	private Date dataReserva;

	@ColumnDB(name="cerp_horareserva", pk=false, length=10, precision=0)
	@FieldDB(name="HoraReserva", label="Hora Reserva", required=false, type= TableFieldEditType.TEXTFIELD)
	private String horaReserva;

	@ColumnDB(name="eta_id", pk=false, fkName="fk_cerp_eta", fk="eta_id = eta_id", length=10, precision=0)
	@FieldDB(name="Etapa.Id", label="Etapa", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:etapa")
	private EtapaBean etapa;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public CertificadoPessoaBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public CertificadoPessoaBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public CertificadoPessoaBean(Integer id, CertificadoBean certificado, UnidadeBean unidade, PessoaBean pessoaCliente, SituacaoBean situacao) {
		super();
		this.id = id;
		this.certificado = certificado;
		this.unidade = unidade;
		this.pessoaCliente = pessoaCliente;
		this.situacao = situacao;
	}

	/** 
	 *
	 */
	public CertificadoPessoaBean(Integer id, CertificadoBean certificado, UnidadeBean unidade, PessoaBean pessoaCliente, SituacaoBean situacao, PessoaBean pessoaVendedor, Integer bloqueadoAte, Date bloqueadoEm, Date dataReserva, String horaReserva, EtapaBean etapa) {
		super();
		this.id = id;
		this.certificado = certificado;
		this.unidade = unidade;
		this.pessoaCliente = pessoaCliente;
		this.situacao = situacao;
		this.pessoaVendedor = pessoaVendedor;
		this.bloqueadoAte = bloqueadoAte;
		this.bloqueadoEm = bloqueadoEm;
		this.dataReserva = dataReserva;
		this.horaReserva = horaReserva;
		this.etapa = etapa;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setCertificado(CertificadoBean certificado) {
		this.certificado = certificado;
	}

	public CertificadoBean getCertificado() {
		if (certificado == null) certificado = new CertificadoBean();
		return certificado;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setPessoaCliente(PessoaBean pessoaCliente) {
		this.pessoaCliente = pessoaCliente;
	}

	public PessoaBean getPessoaCliente() {
		if (pessoaCliente == null) pessoaCliente = new PessoaBean();
		return pessoaCliente;
	}

	public void setSituacao(SituacaoBean situacao) {
		this.situacao = situacao;
	}

	public SituacaoBean getSituacao() {
		if (situacao == null) situacao = new SituacaoBean();
		return situacao;
	}

	public void setPessoaVendedor(PessoaBean pessoaVendedor) {
		this.pessoaVendedor = pessoaVendedor;
	}

	public PessoaBean getPessoaVendedor() {
		if (pessoaVendedor == null) pessoaVendedor = new PessoaBean();
		return pessoaVendedor;
	}

	public void setBloqueadoAte(Integer bloqueadoAte) {
		this.bloqueadoAte = bloqueadoAte;
	}

	public Integer getBloqueadoAte() {
		return bloqueadoAte;
	}

	public void setBloqueadoEm(Date bloqueadoEm) {
		this.bloqueadoEm = bloqueadoEm;
	}

	public Date getBloqueadoEm() {
		return bloqueadoEm;
	}

	public void setDataReserva(Date dataReserva) {
		this.dataReserva = dataReserva;
	}

	public Date getDataReserva() {
		return dataReserva;
	}

	public void setHoraReserva(String horaReserva) {
		this.horaReserva = horaReserva;
	}

	public String getHoraReserva() {
		return horaReserva;
	}

	public void setEtapa(EtapaBean etapa) {
		this.etapa = etapa;
	}

	public EtapaBean getEtapa() {
		if (etapa == null) etapa = new EtapaBean();
		return etapa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CertificadoPessoaBean other = (CertificadoPessoaBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
