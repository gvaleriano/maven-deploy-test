package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Cidade", schema="sys", name="cidade", acronym="cid", typePk="S")
public class CidadeBean extends Bean {

	@ColumnDB(name="cid_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="cid_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=true, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="est_id", pk=false, fkName="fk_cid_est", fk="est_id = est_id", length=10, precision=0)
	@FieldDB(name="Estado.Id", label="Estado", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:estado")
	private EstadoBean estado;

	@ColumnDB(name="cid_ibge", pk=false, length=10, precision=0)
	@FieldDB(name="Ibge", label="Ibge", required=true, type= TableFieldEditType.TEXTFIELD)
	private String ibge;

	public CidadeBean() {
		super();
	}

	public CidadeBean(Integer id) {
		super();
		this.id = id;
	}

	public CidadeBean(Integer id, EstadoBean estado, String descricao, String ibge) {
		super();
		this.id = id;
		this.estado = estado;
		this.descricao = descricao;
		this.ibge = ibge;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setEstado(EstadoBean estado) {
		this.estado = estado;
	}

	public EstadoBean getEstado() {
		if (estado == null) estado = new EstadoBean();
		return estado;
	}

	public void setIbge(String ibge) {
		this.ibge = ibge;
	}

	public String getIbge() {
		return ibge;
	}

}
