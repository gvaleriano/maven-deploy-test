package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Complemento", schema="sys", name="complemento", acronym="com", typePk="S", sequence="scom_id")
public class ComplementoBean extends Bean {

	@ColumnDB(name="com_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="com_cep", pk=false, length=9, precision=0)
	@FieldDB(name="Cep", label="Cep", required=true, type= TableFieldEditType.TEXTFIELD)
	private String cep;

	@ColumnDB(name="com_complemento", pk=false, length=250, precision=0)
	@FieldDB(name="Complemento", label="Complemento", required=false, type= TableFieldEditType.TEXTFIELD)
	private String complemento;

	public ComplementoBean() {
		super();
	}

	public ComplementoBean(Integer id) {
		super();
		this.id = id;
	}

	public ComplementoBean(Integer id, String cep, String complemento) {
		super();
		this.id = id;
		this.cep = cep;
		this.complemento = complemento;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCep() {
		return cep;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getComplemento() {
		return complemento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComplementoBean other = (ComplementoBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
