package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

import java.util.Date;

@TableDB(label="Débito Token", schema="sys", name="debitotoken", acronym="debt", typePk="S", sequence="sdebt_id")
public class DebitoTokenBean extends Bean {

	@ColumnDB(name="debt_id", pk=true, length=15, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="debt_datacadastro", pk=false, length=0, precision=0)
	@FieldDB(name="DataCadastro", label="Data Cadastro", required=true, type= TableFieldEditType.DATEFIELD)
	private Date dataCadastro;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_debt_pes", fk="pes_id = pes_id", length=15, precision=0)
	@FieldDB(name="Pessoa.Id", label="Pessoa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoa;

	@ColumnDB(name="debt_token", pk=false, length=2000, precision=0)
	@FieldDB(name="Token", label="Token", required=true, type= TableFieldEditType.TEXTFIELD)
	private String token;

	@ColumnDB(name="debt_status", pk=false, length=50, precision=0)
	@FieldDB(name="Status", label="Status", required=true, type= TableFieldEditType.TEXTFIELD)
	private String status;

	@ColumnDB(name="debt_data", pk=false, length=15, precision=0)
	@FieldDB(name="Data", label="Data", required=true, type= TableFieldEditType.LONGFIELD)
	private Long data;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public DebitoTokenBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public DebitoTokenBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public DebitoTokenBean(Integer id, Date dataCadastro, PessoaBean pessoa, String token, String status, Long data) {
		super();
		this.id = id;
		this.dataCadastro = dataCadastro;
		this.pessoa = pessoa;
		this.token = token;
		this.status = status;
		this.data = data;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setPessoa(PessoaBean pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaBean getPessoa() {
		if (pessoa == null) pessoa = new PessoaBean();
		return pessoa;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setData(Long data) {
		this.data = data;
	}

	public Long getData() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DebitoTokenBean other = (DebitoTokenBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
