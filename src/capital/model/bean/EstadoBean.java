package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Estado", schema="sys", name="estado", acronym="est", typePk="S")
public class EstadoBean extends Bean {

	@ColumnDB(name="est_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="est_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=true, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="est_sigla", pk=false, length=10, precision=0)
	@FieldDB(name="Sigla", label="Sigla", required=true, type= TableFieldEditType.TEXTFIELD)
	private String sigla;

	@ColumnDB(name="est_timezone", pk=false, length=10, precision=0)
	@FieldDB(name="TimeZone", label="TimeZone", required=true, type= TableFieldEditType.TEXTFIELD)
	private String timeZone;
	
	public EstadoBean() {
		super();
	}

	public EstadoBean(Integer id) {
		super();
		this.id = id;
	}

	public EstadoBean(Integer id, String descricao, String sigla, String timeZone) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.sigla = sigla;
		this.timeZone = timeZone;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getSigla() {
		return sigla;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	

}
