package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

import java.util.Date;

@TableDB(label="Etapa", schema="sys", name="etapa", acronym="eta", typePk="S", sequence="seta_id")
public class EtapaBean extends Bean {

	@ColumnDB(name="eta_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_eta_uni", fk="uni_id = uni_id", length=15, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="eta_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=true, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="eta_datasorteio", pk=false, length=0, precision=0)
	@FieldDB(name="DataSorteio", label="Data Sorteio", required=true, type= TableFieldEditType.DATEFIELD)
	private Date dataSorteio;

	@ColumnDB(name="eta_horasorteio", pk=false, length=8, precision=0)
	@FieldDB(name="HoraSorteio", label="Hora Sorteio", required=true, type= TableFieldEditType.TEXTFIELD)
	private String horaSorteio;

	@ColumnDB(name="eta_edicao", pk=false, length=10, precision=0)
	@FieldDB(name="Edicao", label="Edição", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer edicao;

	@ColumnDB(name="eta_numero", pk=false, length=10, precision=0)
	@FieldDB(name="Numero", label="Numero", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer numero;

	@ColumnDB(name="eta_datalimitevenda", pk=false, length=0, precision=0)
	@FieldDB(name="DataLimiteVenda", label="data Limite Venda", required=false, type= TableFieldEditType.DATEFIELD)
	private Date dataLimiteVenda;

	@ColumnDB(name="eta_horalimitevenda", pk=false, length=8, precision=0)
	@FieldDB(name="HoraLimiteVenda", label="hora Limite Venda", required=false, type= TableFieldEditType.TEXTFIELD)
	private String horaLimiteVenda;

	@ColumnDB(name="eta_tipocertificado", pk=false, length=10, precision=0)
	@FieldDB(name="TipoCertificado", label="Tipo Certificado", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer tipoCertificado;

	@ColumnDB(name="eta_urlvideo", pk=false, length=2000, precision=0)
	@FieldDB(name="UrlVideo", label="Url Video", required=false, type= TableFieldEditType.TEXTFIELD)
	private String urlVideo;

	@ColumnDB(name="eta_urlimagem", pk=false, length=2000, precision=0)
	@FieldDB(name="UrlImagem", label="Url Imagem", required=false, type= TableFieldEditType.TEXTFIELD)
	private String urlImagem;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public EtapaBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public EtapaBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public EtapaBean(Integer id, UnidadeBean unidade, String descricao, Date dataSorteio, String horaSorteio, Integer edicao, Integer numero) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.descricao = descricao;
		this.dataSorteio = dataSorteio;
		this.horaSorteio = horaSorteio;
		this.edicao = edicao;
		this.numero = numero;
	}

	/** 
	 *
	 */
	public EtapaBean(Integer id, UnidadeBean unidade, String descricao, Date dataSorteio, String horaSorteio, Integer edicao, Integer numero, Date dataLimiteVenda, String horaLimiteVenda, Integer tipoCertificado, String urlVideo, String urlImagem) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.descricao = descricao;
		this.dataSorteio = dataSorteio;
		this.horaSorteio = horaSorteio;
		this.edicao = edicao;
		this.numero = numero;
		this.dataLimiteVenda = dataLimiteVenda;
		this.horaLimiteVenda = horaLimiteVenda;
		this.tipoCertificado = tipoCertificado;
		this.urlVideo = urlVideo;
		this.urlImagem = urlImagem;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDataSorteio(Date dataSorteio) {
		this.dataSorteio = dataSorteio;
	}

	public Date getDataSorteio() {
		return dataSorteio;
	}

	public void setHoraSorteio(String horaSorteio) {
		this.horaSorteio = horaSorteio;
	}

	public String getHoraSorteio() {
		return horaSorteio;
	}

	public void setEdicao(Integer edicao) {
		this.edicao = edicao;
	}

	public Integer getEdicao() {
		return edicao;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setDataLimiteVenda(Date dataLimiteVenda) {
		this.dataLimiteVenda = dataLimiteVenda;
	}

	public Date getDataLimiteVenda() {
		return dataLimiteVenda;
	}

	public void setHoraLimiteVenda(String horaLimiteVenda) {
		this.horaLimiteVenda = horaLimiteVenda;
	}

	public String getHoraLimiteVenda() {
		return horaLimiteVenda;
	}

	public void setTipoCertificado(Integer tipoCertificado) {
		this.tipoCertificado = tipoCertificado;
	}

	public Integer getTipoCertificado() {
		return tipoCertificado;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public String getUrlVideo() {
		return urlVideo;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EtapaBean other = (EtapaBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
