package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Etapa", schema="sys", name="etapaimagem", acronym="etai", typePk="S", sequence="setai_id")
public class EtapaImagemBean extends Bean {

	@ColumnDB(name="etai_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_etai_uni", fk="uni_id = uni_id", length=10, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="eta_id", pk=false, fkName="fk_etai_eta", fk="eta_id = eta_id", length=10, precision=0)
	@FieldDB(name="Etapa.Id", label="Etapa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:etapa")
	private EtapaBean etapa;

	@ColumnDB(name="arq_id", pk=false, fkName="fk_etai_arq", fk="arq_id = arq_id", length=10, precision=0)
	@FieldDB(name="Arquivo.Id", label="Arquivo", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:arquivo")
	private ArquivoBean arquivo;

	@ColumnDB(name="etai_tag", pk=false, length=100, precision=0)
	@FieldDB(name="Tag", label="Tag", required=false, type= TableFieldEditType.TEXTFIELD)
	private String tag;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public EtapaImagemBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public EtapaImagemBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public EtapaImagemBean(Integer id, UnidadeBean unidade, EtapaBean etapa, String tag) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.etapa = etapa;
		this.tag = tag;
	}

	/** 
	 *
	 */
	public EtapaImagemBean(Integer id, UnidadeBean unidade, EtapaBean etapa, ArquivoBean arquivo, String tag) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.etapa = etapa;
		this.arquivo = arquivo;
		this.tag = tag;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setEtapa(EtapaBean etapa) {
		this.etapa = etapa;
	}

	public EtapaBean getEtapa() {
		if (etapa == null) etapa = new EtapaBean();
		return etapa;
	}

	public void setArquivo(ArquivoBean arquivo) {
		this.arquivo = arquivo;
	}

	public ArquivoBean getArquivo() {
		if (arquivo == null) arquivo = new ArquivoBean();
		return arquivo;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTag() {
		return tag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EtapaImagemBean other = (EtapaImagemBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
