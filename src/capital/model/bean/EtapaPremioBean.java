package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Etapa Premio", schema="sys", name="etapapremio", acronym="etap", typePk="S", sequence="setap_id")
public class EtapaPremioBean extends Bean {

	@ColumnDB(name="etap_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="eta_id", pk=false, fkName="fk_etap_eta", fk="eta_id = eta_id", length=10, precision=0)
	@FieldDB(name="Etapa.Id", label="Etapa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:etapa")
	private EtapaBean etapa;

	@ColumnDB(name="sit_id", pk=false, fkName="fk_etap_sit", fk="sit_id = sit_id", length=10, precision=0)
	@FieldDB(name="Situacao.Id", label="Situação", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:situacao")
	private SituacaoBean situacao;

	@ColumnDB(name="etap_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="etap_ordem", pk=false, length=10, precision=0)
	@FieldDB(name="Ordem", label="Ordem", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer ordem;

	@ColumnDB(name="etap_bolas", pk=false, length=4000, precision=0)
	@FieldDB(name="Bolas", label="Bolas", required=false, type= TableFieldEditType.TEXTFIELD)
	private String bolas;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public EtapaPremioBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public EtapaPremioBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public EtapaPremioBean(Integer id, EtapaBean etapa, Integer ordem) {
		super();
		this.id = id;
		this.etapa = etapa;
		this.ordem = ordem;
	}

	/** 
	 *
	 */
	public EtapaPremioBean(Integer id, EtapaBean etapa, SituacaoBean situacao, String descricao, Integer ordem, String bolas) {
		super();
		this.id = id;
		this.etapa = etapa;
		this.situacao = situacao;
		this.descricao = descricao;
		this.ordem = ordem;
		this.bolas = bolas;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setEtapa(EtapaBean etapa) {
		this.etapa = etapa;
	}

	public EtapaBean getEtapa() {
		if (etapa == null) etapa = new EtapaBean();
		return etapa;
	}

	public void setSituacao(SituacaoBean situacao) {
		this.situacao = situacao;
	}

	public SituacaoBean getSituacao() {
		if (situacao == null) situacao = new SituacaoBean();
		return situacao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setBolas(String bolas) {
		this.bolas = bolas;
	}

	public String getBolas() {
		return bolas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EtapaPremioBean other = (EtapaPremioBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
