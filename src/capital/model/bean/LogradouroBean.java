package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Logradouro", schema="sys", name="logradouro", acronym="log", typePk="S", sequence="slog_id")
public class LogradouroBean extends Bean {

	@ColumnDB(name="log_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="log_cep", pk=false, length=9, precision=0)
	@FieldDB(name="Cep", label="Cep", required=false, type= TableFieldEditType.TEXTFIELD)
	private String cep;

	@ColumnDB(name="log_logradouro", pk=false, length=250, precision=0)
	@FieldDB(name="Logradouro", label="Logradouro", required=false, type= TableFieldEditType.TEXTFIELD)
	private String logradouro;

	@ColumnDB(name="log_tipo", pk=false, length=20, precision=0)
	@FieldDB(name="Tipo", label="Tipo", required=false, type= TableFieldEditType.TEXTFIELD)
	private String tipo;

	@ColumnDB(name="bai_id", pk=false, fkName="fk_log_bai", fk="bai_id = bai_id", length=10, precision=0)
	@FieldDB(name="Bairro.Id", label="Bairro", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:bairro")
	private BairroBean bairro;

	@ColumnDB(name="cid_id", pk=false, fkName="fk_log_cid", fk="cid_id = cid_id", length=10, precision=0)
	@FieldDB(name="Cidade.Id", label="Cidade", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:cidade")
	private CidadeBean cidade;

	@ColumnDB(name="log_latitude", pk=false, length=20, precision=0)
	@FieldDB(name="Latitude", label="Latitude", required=false, type= TableFieldEditType.TEXTFIELD)
	private String latitude;

	@ColumnDB(name="log_longitude", pk=false, length=20, precision=0)
	@FieldDB(name="Longitude", label="Longitude", required=false, type= TableFieldEditType.TEXTFIELD)
	private String longitude;

	public LogradouroBean() {
		super();
	}

	public LogradouroBean(Integer id) {
		super();
		this.id = id;
	}

	public LogradouroBean(Integer id, String cep, String logradouro, String tipo, BairroBean bairro, CidadeBean cidade, String latitude, String longitude) {
		super();
		this.id = id;
		this.cep = cep;
		this.logradouro = logradouro;
		this.tipo = tipo;
		this.bairro = bairro;
		this.cidade = cidade;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCep() {
		return cep;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setBairro(BairroBean bairro) {
		this.bairro = bairro;
	}

	public BairroBean getBairro() {
		if (bairro == null) bairro = new BairroBean();
		return bairro;
	}

	public void setCidade(CidadeBean cidade) {
		this.cidade = cidade;
	}

	public CidadeBean getCidade() {
		if (cidade == null) cidade = new CidadeBean();
		return cidade;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLongitude() {
		return longitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogradouroBean other = (LogradouroBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
