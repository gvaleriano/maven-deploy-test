package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Message UI", schema="sys", name="messageui", acronym="msg", typePk="S", sequence="smsg_id")
public class MessageUIBean extends Bean {

	@ColumnDB(name="msg_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="msg_chave", pk=false, length=500, precision=0)
	@FieldDB(name="Chave", label="Chave", required=false, type= TableFieldEditType.TEXTFIELD)
	private String chave;

	@ColumnDB(name="msg_descricao", pk=false, length=4000, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=false, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public MessageUIBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public MessageUIBean(Integer id) {
		super();
		this.id = id;
	}

	

	public MessageUIBean(Integer id, String chave, String descricao) {
		super();
		this.id = id;
		this.chave = chave;
		this.descricao = descricao;
	}
	public MessageUIBean(String chave, String descricao) {
		super();
		this.chave = chave;
		this.descricao = descricao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getChave() {
		return chave;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageUIBean other = (MessageUIBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
