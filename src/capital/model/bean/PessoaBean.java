package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

import java.math.BigDecimal;
import java.util.Date;

@TableDB(label="Pessoa", schema="sys", name="pessoa", acronym="pes", typePk="S", sequence="spes_id")
public class PessoaBean extends Bean {

	@ColumnDB(name="pes_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_pes_uni", fk="uni_id = uni_id", length=15, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="pes_ativo", pk=false, length=1, precision=0)
	@FieldDB(name="Ativo", label="Ativo", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer ativo;

	@ColumnDB(name="pes_usuario", pk=false, length=20, precision=0)
	@FieldDB(name="Usuario", label="Usuario", required=false, type= TableFieldEditType.TEXTFIELD)
	private String usuario;

	@ColumnDB(name="pes_senha", pk=false, length=250, precision=0)
	@FieldDB(name="Senha", label="Senha", required=false, type= TableFieldEditType.TEXTFIELD)
	private String senha;

	@ColumnDB(name="pes_datacadastro", pk=false, length=0, precision=0)
	@FieldDB(name="DataCadastro", label="Data Cadastro", required=true, type= TableFieldEditType.DATEFIELD)
	private Date dataCadastro;

	@ColumnDB(name="pes_dataatualizado", pk=false, length=0, precision=0)
	@FieldDB(name="DataAtualizado", label="Data Atualizado", required=true, type= TableFieldEditType.DATEFIELD)
	private Date dataAtualizado;

	@ColumnDB(name="pes_nomerazao", pk=false, length=250, precision=0)
	@FieldDB(name="NomeRazao", label="Nome/Razao", required=true, type= TableFieldEditType.TEXTFIELD)
	private String nomeRazao;

	@ColumnDB(name="pes_apelidofantasia", pk=false, length=250, precision=0)
	@FieldDB(name="ApelidoFantasia", label="Apelido/Fantasia", required=false, type= TableFieldEditType.TEXTFIELD)
	private String apelidoFantasia;

	@ColumnDB(name="pes_cpfcnpj", pk=false, length=21, precision=0)
	@FieldDB(name="CpfCnpj", label="CpfCnpj", required=false, type= TableFieldEditType.TEXTFIELD)
	private String cpfCnpj;

	@ColumnDB(name="pes_rginscricao", pk=false, length=20, precision=0)
	@FieldDB(name="RgInscricao", label="Rg/Inscricao", required=false, type= TableFieldEditType.TEXTFIELD)
	private String rgInscricao;

	@ColumnDB(name="pes_nascimentofundacao", pk=false, length=0, precision=0)
	@FieldDB(name="NascimentoFundacao", label="Nascimento/Abertura", required=false, type= TableFieldEditType.DATEFIELD)
	private Date nascimentoFundacao;

	@ColumnDB(name="pes_salarioreceita", pk=false, length=15, precision=2)
	@FieldDB(name="SalarioReceita", label="Salário/Receita", required=false, type= TableFieldEditType.DECIMALFIELD)
	private BigDecimal salarioReceita;

	@ColumnDB(name="pes_email", pk=false, length=50, precision=0)
	@FieldDB(name="Email", label="Email", required=false, type= TableFieldEditType.TEXTFIELD)
	private String email;

	@ColumnDB(name="pes_telefone1", pk=false, length=50, precision=0)
	@FieldDB(name="Telefone1", label="Telefone1", required=false, type= TableFieldEditType.TEXTFIELD)
	private String telefone1;

	@ColumnDB(name="pes_telefone2", pk=false, length=50, precision=0)
	@FieldDB(name="Telefone2", label="Telefone2", required=false, type= TableFieldEditType.TEXTFIELD)
	private String telefone2;

	@ColumnDB(name="pes_observacao", pk=false, length=500, precision=0)
	@FieldDB(name="Observacao", label="Observação", required=false, type= TableFieldEditType.TEXTFIELD)
	private String observacao;

	@ColumnDB(name="pes_imei", pk=false, length=250, precision=0)
	@FieldDB(name="Imei", label="Imei", required=false, type= TableFieldEditType.TEXTFIELD)
	private String imei;

	@ColumnDB(name="pes_authtoken", pk=false, length=250, precision=0)
	@FieldDB(name="AuthToken", label="Auth Token", required=false, type= TableFieldEditType.TEXTFIELD)
	private String authToken;

	@ColumnDB(name="sit_id", pk=false, fkName="fk_pes_sit", fk="sit_id = sit_id", length=10, precision=0)
	@FieldDB(name="Situacao.Id", label="Situação", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:situacao")
	private SituacaoBean situacao;

	@ColumnDB(name="pes_pinbankid", pk=false, length=10, precision=0)
	@FieldDB(name="PinbankId", label="Pinbank Id", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer pinbankId;

	@ColumnDB(name="pes_sexo", pk=false, length=1, precision=0)
	@FieldDB(name="Sexo", label="Sexo", required=false, type= TableFieldEditType.TEXTFIELD)
	private String sexo;

	@ColumnDB(name="pes_transacao", pk=false, length=500, precision=0)
	@FieldDB(name="Transacao", label="Transação", required=false, type= TableFieldEditType.TEXTFIELD)
	private String transacao;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public PessoaBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public PessoaBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public PessoaBean(Integer id, UnidadeBean unidade, Integer ativo, Date dataCadastro, Date dataAtualizado, String nomeRazao) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.ativo = ativo;
		this.dataCadastro = dataCadastro;
		this.dataAtualizado = dataAtualizado;
		this.nomeRazao = nomeRazao;
	}

	/** 
	 *
	 */
	public PessoaBean(Integer id, UnidadeBean unidade, Integer ativo, String usuario, String senha, Date dataCadastro, Date dataAtualizado, String nomeRazao, String apelidoFantasia, String cpfCnpj, String rgInscricao, Date nascimentoFundacao, BigDecimal salarioReceita, String email, String telefone1, String telefone2, String observacao, String imei, String authToken, SituacaoBean situacao, Integer pinbankId, String sexo, String transacao) {
		super();
		this.id = id;
		this.unidade = unidade;
		this.ativo = ativo;
		this.usuario = usuario;
		this.senha = senha;
		this.dataCadastro = dataCadastro;
		this.dataAtualizado = dataAtualizado;
		this.nomeRazao = nomeRazao;
		this.apelidoFantasia = apelidoFantasia;
		this.cpfCnpj = cpfCnpj;
		this.rgInscricao = rgInscricao;
		this.nascimentoFundacao = nascimentoFundacao;
		this.salarioReceita = salarioReceita;
		this.email = email;
		this.telefone1 = telefone1;
		this.telefone2 = telefone2;
		this.observacao = observacao;
		this.imei = imei;
		this.authToken = authToken;
		this.situacao = situacao;
		this.pinbankId = pinbankId;
		this.sexo = sexo;
		this.transacao = transacao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSenha() {
		return senha;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataAtualizado(Date dataAtualizado) {
		this.dataAtualizado = dataAtualizado;
	}

	public Date getDataAtualizado() {
		return dataAtualizado;
	}

	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	public String getNomeRazao() {
		return nomeRazao;
	}

	public void setApelidoFantasia(String apelidoFantasia) {
		this.apelidoFantasia = apelidoFantasia;
	}

	public String getApelidoFantasia() {
		return apelidoFantasia;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setRgInscricao(String rgInscricao) {
		this.rgInscricao = rgInscricao;
	}

	public String getRgInscricao() {
		return rgInscricao;
	}

	public void setNascimentoFundacao(Date nascimentoFundacao) {
		this.nascimentoFundacao = nascimentoFundacao;
	}

	public Date getNascimentoFundacao() {
		return nascimentoFundacao;
	}

	public void setSalarioReceita(BigDecimal salarioReceita) {
		this.salarioReceita = salarioReceita;
	}

	public BigDecimal getSalarioReceita() {
		return salarioReceita;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getImei() {
		return imei;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setSituacao(SituacaoBean situacao) {
		this.situacao = situacao;
	}

	public SituacaoBean getSituacao() {
		if (situacao == null) situacao = new SituacaoBean();
		return situacao;
	}

	public void setPinbankId(Integer pinbankId) {
		this.pinbankId = pinbankId;
	}

	public Integer getPinbankId() {
		return pinbankId;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getSexo() {
		return sexo;
	}

	public void setTransacao(String transacao) {
		this.transacao = transacao;
	}

	public String getTransacao() {
		return transacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaBean other = (PessoaBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
