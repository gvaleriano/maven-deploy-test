package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Pessoa Distribuidor", schema="capital", name="pessoadistribuidor", acronym="pesd", typePk="S", sequence="spesd_id")
public class PessoaDistribuidorBean extends Bean {

	@ColumnDB(name="pesd_id", pk=true, length=15, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="pes_distribuidor_id", pk=false, fkName="fk_pesd_distribuidor_pes", fk="pes_distribuidor_id = pes_id", length=15, precision=0)
	@FieldDB(name="PessoaDistribuidor.Id", label="Distribuidor", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean distribuidor;

	@ColumnDB(name="pes_pontovenda_id", pk=false, fkName="fk_pesd_pontovenda_pes", fk="pes_pontovenda_id = pes_id", length=15, precision=0)
	@FieldDB(name="PessoaPontoVenda.Id", label="Ponto de Venda", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pontoDeVenda;

	public PessoaDistribuidorBean() {
		super();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PessoaBean getDistribuidor() {
		if (distribuidor == null) distribuidor = new PessoaBean();
		return distribuidor;
	}



	public void setDistribuidor(PessoaBean distribuidor) {
		this.distribuidor = distribuidor;
	}



	public PessoaBean getPontoDeVenda() {
		return pontoDeVenda;
	}



	public void setPontoDeVenda(PessoaBean pontoDeVenda) {
		this.pontoDeVenda = pontoDeVenda;
	}



	public PessoaDistribuidorBean(Integer id) {
		super();
		this.id = id;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaDistribuidorBean other = (PessoaDistribuidorBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
