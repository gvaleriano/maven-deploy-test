package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Pessoa Endereço", schema="sys", name="pessoaendereco", acronym="pese", typePk="S", sequence="spese_id")
public class PessoaEnderecoBean extends Bean {

	@ColumnDB(name="pese_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="peset_id", pk=false, fkName="fk_pese_peset", fk="peset_id = peset_id", length=10, precision=0)
	@FieldDB(name="PessoaEnderecoTipo.Id", label="Pessoa Endereço Tipo", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoaenderecotipo")
	private PessoaEnderecoTipoBean pessoaEnderecoTipo;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_pese_pes", fk="pes_id = pes_id", length=10, precision=0)
	@FieldDB(name="Pessoa.Id", label="Pessoa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoa;

	@ColumnDB(name="cid_id", pk=false, fkName="fk_pese_cid", fk="cid_id = cid_id", length=10, precision=0)
	@FieldDB(name="Cidade.Id", label="Cidade", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:cidade")
	private CidadeBean cidade;

	@ColumnDB(name="pese_logradouro", pk=false, length=250, precision=0)
	@FieldDB(name="Logradouro", label="Logradouro", required=true, type= TableFieldEditType.TEXTFIELD)
	private String logradouro;

	@ColumnDB(name="pese_cep", pk=false, length=9, precision=0)
	@FieldDB(name="Cep", label="Cep", required=false, type= TableFieldEditType.TEXTFIELD)
	private String cep;

	@ColumnDB(name="pese_numero", pk=false, length=10, precision=0)
	@FieldDB(name="Numero", label="Numero", required=false, type= TableFieldEditType.TEXTFIELD)
	private String numero;

	@ColumnDB(name="pese_complemento", pk=false, length=20, precision=0)
	@FieldDB(name="Complemento", label="Complemento", required=false, type= TableFieldEditType.TEXTFIELD)
	private String complemento;

	@ColumnDB(name="pese_bairro", pk=false, length=250, precision=0)
	@FieldDB(name="Bairro", label="Bairro", required=false, type= TableFieldEditType.TEXTFIELD)
	private String bairro;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public PessoaEnderecoBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public PessoaEnderecoBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public PessoaEnderecoBean(Integer id, PessoaEnderecoTipoBean pessoaEnderecoTipo, PessoaBean pessoa, CidadeBean cidade, String logradouro) {
		super();
		this.id = id;
		this.pessoaEnderecoTipo = pessoaEnderecoTipo;
		this.pessoa = pessoa;
		this.cidade = cidade;
		this.logradouro = logradouro;
	}

	/** 
	 *
	 */
	public PessoaEnderecoBean(Integer id, PessoaEnderecoTipoBean pessoaEnderecoTipo, PessoaBean pessoa, CidadeBean cidade, String logradouro, String cep, String numero, String complemento, String bairro) {
		super();
		this.id = id;
		this.pessoaEnderecoTipo = pessoaEnderecoTipo;
		this.pessoa = pessoa;
		this.cidade = cidade;
		this.logradouro = logradouro;
		this.cep = cep;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPessoaEnderecoTipo(PessoaEnderecoTipoBean pessoaEnderecoTipo) {
		this.pessoaEnderecoTipo = pessoaEnderecoTipo;
	}

	public PessoaEnderecoTipoBean getPessoaEnderecoTipo() {
		if (pessoaEnderecoTipo == null) pessoaEnderecoTipo = new PessoaEnderecoTipoBean();
		return pessoaEnderecoTipo;
	}

	public void setPessoa(PessoaBean pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaBean getPessoa() {
		if (pessoa == null) pessoa = new PessoaBean();
		return pessoa;
	}

	public void setCidade(CidadeBean cidade) {
		this.cidade = cidade;
	}

	public CidadeBean getCidade() {
		if (cidade == null) cidade = new CidadeBean();
		return cidade;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCep() {
		return cep;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumero() {
		return numero;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getBairro() {
		return bairro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaEnderecoBean other = (PessoaEnderecoBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
