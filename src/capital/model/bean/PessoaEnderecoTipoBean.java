package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Pessoa Endereço Tipo", schema="sys", name="pessoaenderecotipo", acronym="peset", typePk="S", sequence="speset_id")
public class PessoaEnderecoTipoBean extends Bean {

	@ColumnDB(name="peset_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="peset_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=true, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public PessoaEnderecoTipoBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public PessoaEnderecoTipoBean(Integer id) {
		super();
		this.id = id;
	}

	public PessoaEnderecoTipoBean(Integer id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaEnderecoTipoBean other = (PessoaEnderecoTipoBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
