package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

import java.util.Date;

@TableDB(label="Pessoa Gateway", schema="sys", name="pessoagateway", acronym="pesg", typePk="S", sequence="spesg_id")
public class PessoaGatewayBean extends Bean {

	@ColumnDB(name="pesg_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_pesg_pes", fk="pes_id = pes_id", length=10, precision=0)
	@FieldDB(name="Pessoa.Id", label="Pessoa", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoa;

	@ColumnDB(name="pesg_resposta", pk=false, length=4000, precision=0)
	@FieldDB(name="Resposta", label="Resposta", required=false, type= TableFieldEditType.TEXTFIELD)
	private String resposta;

	@ColumnDB(name="pesg_transacao", pk=false, length=500, precision=0)
	@FieldDB(name="Transacao", label="Transação", required=false, type= TableFieldEditType.TEXTFIELD)
	private String transacao;

	@ColumnDB(name="pesg_gateway", pk=false, length=50, precision=0)
	@FieldDB(name="Gateway", label="Gateway", required=false, type= TableFieldEditType.TEXTFIELD)
	private String gateway;

	@ColumnDB(name="pesg_data", pk=false, length=0, precision=0)
	@FieldDB(name="Data", label="Data", required=false, type= TableFieldEditType.DATEFIELD)
	private Date data;

	@ColumnDB(name="pesg_hora", pk=false, length=8, precision=0)
	@FieldDB(name="Hora", label="Hora", required=false, type= TableFieldEditType.HOURFIELD)
	private String hora;

	@ColumnDB(name="pesg_sucesso", pk=false, length=1, precision=0)
	@FieldDB(name="Sucesso", label="Sucesso", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer sucesso;

	@ColumnDB(name="pesg_ip", pk=false, length=50, precision=0)
	@FieldDB(name="Ip", label="IP", required=false, type= TableFieldEditType.TEXTFIELD)
	private String ip;

	@ColumnDB(name="pesg_mensagem", pk=false, length=500, precision=0)
	@FieldDB(name="Mensagem", label="Mensagem", required=false, type= TableFieldEditType.TEXTFIELD)
	private String mensagem;

	@ColumnDB(name="pesg_codigo_retorno", pk=false, length=20, precision=0)
	@FieldDB(name="CodigoRetorno", label="Retorno", required=false, type= TableFieldEditType.TEXTFIELD)
	private String codigoRetorno;

	@ColumnDB(name="pesg_delay", pk=false, length=5, precision=0)
	@FieldDB(name="Delay", label="Delay", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer delay;

	@ColumnDB(name="pesg_request_response", pk=false, length=10, precision=0)
	@FieldDB(name="RequestResponse", label="request_response", required=false, type= TableFieldEditType.TEXTFIELD)
	private String requestResponse;

	@ColumnDB(name="pesg_metodo", pk=false, length=20, precision=0)
	@FieldDB(name="Metodo", label="Método", required=false, type= TableFieldEditType.TEXTFIELD)
	private String metodo;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public PessoaGatewayBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public PessoaGatewayBean(Integer id) {
		super();
		this.id = id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPessoa(PessoaBean pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaBean getPessoa() {
		if (pessoa == null) pessoa = new PessoaBean();
		return pessoa;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	public String getResposta() {
		return resposta;
	}

	public void setTransacao(String transacao) {
		this.transacao = transacao;
	}

	public String getTransacao() {
		return transacao;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getGateway() {
		return gateway;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getData() {
		return data;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getHora() {
		return hora;
	}

	public void setSucesso(Integer sucesso) {
		this.sucesso = sucesso;
	}

	public Integer getSucesso() {
		return sucesso;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIp() {
		return ip;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setCodigoRetorno(String codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}

	public String getCodigoRetorno() {
		return codigoRetorno;
	}

	public void setDelay(Integer delay) {
		this.delay = delay;
	}

	public Integer getDelay() {
		return delay;
	}

	public void setRequestResponse(String requestResponse) {
		this.requestResponse = requestResponse;
	}

	public String getRequestResponse() {
		return requestResponse;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getMetodo() {
		return metodo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaGatewayBean other = (PessoaGatewayBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
