package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Pessoa Tipo Pessoa", schema="sys", name="pessoatipopessoa", acronym="ptp", typePk="S", sequence="sptp_id")
public class PessoaTipoPessoaBean extends Bean {

	@ColumnDB(name="ptp_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="pest_id", pk=false, fkName="fk_ptp_pest", fk="pest_id = pest_id", length=10, precision=0)
	@FieldDB(name="PessoaTipo.Id", label="Tipo de Pessoa", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoatipo")
	private PessoaTipoBean pessoaTipo;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_ptp_pes", fk="pes_id = pes_id", length=10, precision=0)
	@FieldDB(name="Pessoa.Id", label="Pessoa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoa;

	public PessoaTipoPessoaBean() {
		super();
	}

	public PessoaTipoPessoaBean(Integer id) {
		super();
		this.id = id;
	}

	public PessoaTipoPessoaBean(Integer id, PessoaTipoBean pessoaTipo, PessoaBean pessoa) {
		super();
		this.id = id;
		this.pessoaTipo = pessoaTipo;
		this.pessoa = pessoa;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPessoaTipo(PessoaTipoBean pessoaTipo) {
		this.pessoaTipo = pessoaTipo;
	}

	public PessoaTipoBean getPessoaTipo() {
		if (pessoaTipo == null) pessoaTipo = new PessoaTipoBean();
		return pessoaTipo;
	}

	public void setPessoa(PessoaBean pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaBean getPessoa() {
		if (pessoa == null) pessoa = new PessoaBean();
		return pessoa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaTipoPessoaBean other = (PessoaTipoPessoaBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
