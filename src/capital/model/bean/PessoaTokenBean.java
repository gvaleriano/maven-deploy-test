package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

import java.util.Date;

@TableDB(label="Pessoa Token", schema="sys", name="pessoatoken", acronym="peto", typePk="S", sequence="speto_id")
public class PessoaTokenBean extends Bean {

	@ColumnDB(name="peto_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="pes_id", pk=false, fkName="fk_peto_pes", fk="pes_id = pes_id", length=10, precision=0)
	@FieldDB(name="Pessoa.Id", label="Pessoa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoa;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_peto_uni", fk="uni_id = uni_id", length=10, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="peto_codigo", pk=false, length=100, precision=0)
	@FieldDB(name="Codigo", label="Codigo", required=false, type= TableFieldEditType.TEXTFIELD)
	private String codigo;

	@ColumnDB(name="peto_datacadastro", pk=false, length=0, precision=0)
	@FieldDB(name="DataCadastro", label="Data Cadastro", required=false, type= TableFieldEditType.DATEFIELD)
	private Date dataCadastro;

	@ColumnDB(name="peto_horacadastro", pk=false, length=8, precision=0)
	@FieldDB(name="HoraCadastro", label="Hora Cadastro", required=false, type= TableFieldEditType.TEXTFIELD)
	private String horaCadastro;

	@ColumnDB(name="peto_datavalidade", pk=false, length=0, precision=0)
	@FieldDB(name="DataValidade", label="Data Validade", required=false, type= TableFieldEditType.DATEFIELD)
	private Date dataValidade;

	@ColumnDB(name="peto_horavalidade", pk=false, length=8, precision=0)
	@FieldDB(name="HoraValidade", label="Hora Validade", required=false, type= TableFieldEditType.TEXTFIELD)
	private String horaValidade;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public PessoaTokenBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public PessoaTokenBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public PessoaTokenBean(Integer id, PessoaBean pessoa, UnidadeBean unidade) {
		super();
		this.id = id;
		this.pessoa = pessoa;
		this.unidade = unidade;
	}

	/** 
	 *
	 */
	public PessoaTokenBean(Integer id, PessoaBean pessoa, UnidadeBean unidade, String codigo, Date dataCadastro, String horaCadastro, Date dataValidade, String horaValidade) {
		super();
		this.id = id;
		this.pessoa = pessoa;
		this.unidade = unidade;
		this.codigo = codigo;
		this.dataCadastro = dataCadastro;
		this.horaCadastro = horaCadastro;
		this.dataValidade = dataValidade;
		this.horaValidade = horaValidade;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPessoa(PessoaBean pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaBean getPessoa() {
		if (pessoa == null) pessoa = new PessoaBean();
		return pessoa;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setHoraCadastro(String horaCadastro) {
		this.horaCadastro = horaCadastro;
	}

	public String getHoraCadastro() {
		return horaCadastro;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setHoraValidade(String horaValidade) {
		this.horaValidade = horaValidade;
	}

	public String getHoraValidade() {
		return horaValidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaTokenBean other = (PessoaTokenBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
