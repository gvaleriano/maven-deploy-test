package capital.model.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import br.com.dotum.jedi.core.table.Blob;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;
import br.com.dotum.jedi.core.db.AbstractTableEnum;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;

@TableDB(label="Request", schema="sys", name="request", acronym="req", typePk="S", sequence="sreq_id")
public class RequestBean extends Bean {

	@ColumnDB(name="req_id", pk=true, length=15, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type=TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="req_data", pk=false, length=0, precision=0)
	@FieldDB(name="Data", label="Data", required=true, type=TableFieldEditType.DATEFIELD)
	private Date data;

	@ColumnDB(name="req_hora", pk=false, length=8, precision=0)
	@FieldDB(name="Hora", label="Hora", required=true, type=TableFieldEditType.HOURFIELD)
	private String hora;

	@ColumnDB(name="req_host", pk=false, length=50, precision=0)
	@FieldDB(name="Host", label="host", required=true, type=TableFieldEditType.TEXTFIELD)
	private String host;

	@ColumnDB(name="req_versao", pk=false, length=20, precision=0)
	@FieldDB(name="Versao", label="Versao", required=false, type=TableFieldEditType.TEXTFIELD)
	private String versao;

	@ColumnDB(name="req_endpoint", pk=false, length=100, precision=0)
	@FieldDB(name="Endpoint", label="endpoint", required=true, type=TableFieldEditType.TEXTFIELD)
	private String endpoint;

	@ColumnDB(name="req_request", pk=false, length=2000, precision=0)
	@FieldDB(name="Request", label="request", required=false, type=TableFieldEditType.TEXTFIELD)
	private String request;

	@ColumnDB(name="req_response", pk=false, length=2000, precision=0)
	@FieldDB(name="Response", label="response", required=false, type=TableFieldEditType.TEXTFIELD)
	private String response;

	@ColumnDB(name="req_http_status", pk=false, length=5, precision=0)
	@FieldDB(name="HttpStatus", label="Http Status", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer httpStatus;

	@ColumnDB(name="req_time", pk=false, length=6, precision=0)
	@FieldDB(name="Time", label="time", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer time;

	@ColumnDB(name="pes_cliente_id", pk=false, fkName="fk_req_pes_cliente_id", fk="pes_cliente_id = pes_id", length=15, precision=0)
	@FieldDB(name="PessoaCliente.Id", label="Pessoa", required=false, type=TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoaCliente;

	@ColumnDB(name="req_gateway", pk=false, length=50, precision=0)
	@FieldDB(name="Gateway", label="Gateway", required=false, type=TableFieldEditType.TEXTFIELD)
	private String gateway;

	@ColumnDB(name="req_gw_sucesso", pk=false, length=1, precision=0)
	@FieldDB(name="GatewaySucesso", label="Gateway Sucesso", required=false, type=TableFieldEditType.INTEGERFIELD)
	private Integer GatewaySucesso;

	@ColumnDB(name="req_gw_codigo_retorno", pk=false, length=100, precision=0)
	@FieldDB(name="GatewayCodigoRetorno", label="Código", required=false, type=TableFieldEditType.TEXTFIELD)
	private String GatewayCodigoRetorno;

	@ColumnDB(name="req_gw_mensagem", pk=false, length=2000, precision=0)
	@FieldDB(name="GatewayMensagem", label="Mensagem", required=false, type=TableFieldEditType.TEXTFIELD)
	private String GatewayMensagem;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public RequestBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public RequestBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public RequestBean(Integer id, Date data, String hora, String host, String endpoint) {
		super();
		this.id = id;
		this.data = data;
		this.hora = hora;
		this.host = host;
		this.endpoint = endpoint;
	}

	/** 
	 *
	 */
	public RequestBean(Integer id, Date data, String hora, String host, String versao, String endpoint, String request, String response, Integer httpStatus, Integer time, PessoaBean pessoaCliente, String gateway, Integer GatewaySucesso, String GatewayCodigoRetorno, String GatewayMensagem) {
		super();
		this.id = id;
		this.data = data;
		this.hora = hora;
		this.host = host;
		this.versao = versao;
		this.endpoint = endpoint;
		this.request = request;
		this.response = response;
		this.httpStatus = httpStatus;
		this.time = time;
		this.pessoaCliente = pessoaCliente;
		this.gateway = gateway;
		this.GatewaySucesso = GatewaySucesso;
		this.GatewayCodigoRetorno = GatewayCodigoRetorno;
		this.GatewayMensagem = GatewayMensagem;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getData() {
		return data;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getHora() {
		return hora;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getHost() {
		return host;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getVersao() {
		return versao;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getRequest() {
		return request;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getResponse() {
		return response;
	}

	public void setHttpStatus(Integer httpStatus) {
		this.httpStatus = httpStatus;
	}

	public Integer getHttpStatus() {
		return httpStatus;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public Integer getTime() {
		return time;
	}

	public void setPessoaCliente(PessoaBean pessoaCliente) {
		this.pessoaCliente = pessoaCliente;
	}

	public PessoaBean getPessoaCliente() {
		if (pessoaCliente == null) pessoaCliente = new PessoaBean();
		return pessoaCliente;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGatewaySucesso(Integer GatewaySucesso) {
		this.GatewaySucesso = GatewaySucesso;
	}

	public Integer getGatewaySucesso() {
		return GatewaySucesso;
	}

	public void setGatewayCodigoRetorno(String GatewayCodigoRetorno) {
		this.GatewayCodigoRetorno = GatewayCodigoRetorno;
	}

	public String getGatewayCodigoRetorno() {
		return GatewayCodigoRetorno;
	}

	public void setGatewayMensagem(String GatewayMensagem) {
		this.GatewayMensagem = GatewayMensagem;
	}

	public String getGatewayMensagem() {
		return GatewayMensagem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestBean other = (RequestBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
