package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Unidade", schema="sys", name="unidade", acronym="uni", typePk="S", sequence="suni_id")
public class UnidadeBean extends Bean {

	
	@ColumnDB(name="uni_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="uni_descricao", pk=false, length=250, precision=0)
	@FieldDB(name="Descricao", label="Descrição", required=true, type= TableFieldEditType.TEXTFIELD)
	private String descricao;

	@ColumnDB(name="uni_ceplimite", pk=false, length=500, precision=0)
	@FieldDB(name="CepLimite", label="Cep Limite", required=false, type= TableFieldEditType.TEXTFIELD)
	private String cepLimite;

	@ColumnDB(name="uni_compralimite", pk=false, length=10, precision=0)
	@FieldDB(name="CompraLimite", label="Compra Limite", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer compraLimite;

	@ColumnDB(name="uni_urlbaseimage", pk=false, length=500, precision=0)
	@FieldDB(name="UrlBaseImage", label="Url Base Image", required=false, type= TableFieldEditType.TEXTFIELD)
	private String urlBaseImage;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public UnidadeBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public UnidadeBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public UnidadeBean(Integer id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	/** 
	 *
	 */
	public UnidadeBean(Integer id, String descricao, String cepLimite, Integer compraLimite, String urlBaseImage) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.cepLimite = cepLimite;
		this.compraLimite = compraLimite;
		this.urlBaseImage = urlBaseImage;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setCepLimite(String cepLimite) {
		this.cepLimite = cepLimite;
	}

	public String getCepLimite() {
		return cepLimite;
	}

	public void setCompraLimite(Integer compraLimite) {
		this.compraLimite = compraLimite;
	}

	public Integer getCompraLimite() {
		return compraLimite;
	}

	public void setUrlBaseImage(String urlBaseImage) {
		this.urlBaseImage = urlBaseImage;
	}

	public String getUrlBaseImage() {
		return urlBaseImage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeBean other = (UnidadeBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
