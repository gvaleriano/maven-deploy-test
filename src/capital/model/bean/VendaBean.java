package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

import java.math.BigDecimal;
import java.util.Date;

@TableDB(label="Etapa", schema="sys", name="venda", acronym="ven", typePk="S", sequence="sven_id")
public class VendaBean extends Bean {

	@ColumnDB(name="ven_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="pes_cliente_id", pk=false, fkName="fk_ven_pes_cliente_id", fk="pes_cliente_id = pes_id", length=15, precision=0)
	@FieldDB(name="PessoaCliente.Id", label="Pessoa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:pessoa")
	private PessoaBean pessoaCliente;

	@ColumnDB(name="cer_id", pk=false, fkName="fk_ven_cer", fk="cer_id = cer_id", length=15, precision=0)
	@FieldDB(name="Certificado.Id", label="Etapa", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:certificado")
	private CertificadoBean certificado;

	@ColumnDB(name="uni_id", pk=false, fkName="fk_ven_uni", fk="uni_id = uni_id", length=15, precision=0)
	@FieldDB(name="Unidade.Id", label="Unidade", required=true, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:unidade")
	private UnidadeBean unidade;

	@ColumnDB(name="ven_data", pk=false, length=0, precision=0)
	@FieldDB(name="Data", label="Data", required=true, type= TableFieldEditType.DATEFIELD)
	private Date data;

	@ColumnDB(name="ven_hora", pk=false, length=8, precision=0)
	@FieldDB(name="Hora", label="Hora", required=true, type= TableFieldEditType.HOURFIELD)
	private String hora;

	@ColumnDB(name="ven_valor", pk=false, length=15, precision=2)
	@FieldDB(name="Valor", label="Valor", required=true, type= TableFieldEditType.DECIMALFIELD)
	private BigDecimal valor;

	@ColumnDB(name="ven_canaltag", pk=false, length=100, precision=0)
	@FieldDB(name="CanalTag", label="Canal Tag", required=true, type= TableFieldEditType.TEXTFIELD)
	private String canalTag;

	@ColumnDB(name="ven_ganhador", pk=false, length=1, precision=0)
	@FieldDB(name="Ganhador", label="Ganhador", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer ganhador;

	@ColumnDB(name="ven_gg", pk=false, length=1, precision=0)
	@FieldDB(name="Gg", label="GG", required=false, type= TableFieldEditType.INTEGERFIELD)
	private Integer gg;

	@ColumnDB(name="etap_id", pk=false, fkName="fk_ven_etap", fk="etap_id = etap_id", length=10, precision=0)
	@FieldDB(name="EtapaPremio.Id", label="Premio da etapa", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:etapapremio")
	private EtapaPremioBean etapaPremio;

	@ColumnDB(name="eta_id", pk=false, fkName="fk_ven_eta", fk="eta_id = eta_id", length=10, precision=0)
	@FieldDB(name="Etapa.Id", label="Etapa", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:etapa")
	private EtapaBean etapa;

	@ColumnDB(name="sit_id", pk=false, fkName="fk_ven_sit", fk="sit_id = sit_id", length=10, precision=0)
	@FieldDB(name="Situacao.Id", label="Situação", required=false, type= TableFieldEditType.COMBOBOXDBFIELD, chave="LV:situacao")
	private SituacaoBean situacao;



	@ColumnDB(name="ven_pinbanknsu", pk=false, length=200, precision=0)
	@FieldDB(name="PinbankNsu", label="Pinbank Nsu", required=false, type= TableFieldEditType.TEXTFIELD)
	private String pinbankNsu;

	@ColumnDB(name="ven_nsu", pk=false, length=15, precision=0)
	@FieldDB(name="Nsu", label="Nsu", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer nsu;

	@ColumnDB(name="ven_transacao", pk=false, length=500, precision=0)
	@FieldDB(name="Transacao", label="Transação", required=false, type= TableFieldEditType.TEXTFIELD)
	private String transacao;

	@ColumnDB(name="ven_forma_pagamento", pk=false, length=3, precision=0)
	@FieldDB(name="Pagamento", label="Pagamento", required=false, type= TableFieldEditType.TEXTFIELD)
	private String pagamento;

	@ColumnDB(name="ven_codigo_autorizacao", pk=false, length=50, precision=0)
	@FieldDB(name="Autorizacao", label="Autorização", required=false, type= TableFieldEditType.TEXTFIELD)
	private String autorizacao;

	@ColumnDB(name="ven_nsu_gw", pk=false, length=50, precision=0)
	@FieldDB(name="NsuGw", label="Nsu Gw", required=false, type= TableFieldEditType.TEXTFIELD)
	private String nsuGw;

	@ColumnDB(name="ven_nsu_adquirente", pk=false, length=50, precision=0)
	@FieldDB(name="NsuAdquirente", label="Nsu Adquirente", required=false, type= TableFieldEditType.TEXTFIELD)
	private String nsuAdquirente;

	@ColumnDB(name="ven_mensagem_gateway", pk=false, length=500, precision=0)
	@FieldDB(name="MensagemGateway", label="Mensagem Gateway", required=false, type= TableFieldEditType.TEXTFIELD)
	private String mensagemGateway;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public VendaBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public VendaBean(Integer id) {
		super();
		this.id = id;
	}

	/* 
	 * Construtor com os campos obrigadorios
	 *
	 */
	public VendaBean(Integer id, PessoaBean pessoaCliente, CertificadoBean certificado, UnidadeBean unidade, Date data, String hora, BigDecimal valor, String canalTag, Integer nsu) {
		super();
		this.id = id;
		this.pessoaCliente = pessoaCliente;
		this.certificado = certificado;
		this.unidade = unidade;
		this.data = data;
		this.hora = hora;
		this.valor = valor;
		this.canalTag = canalTag;
		this.nsu = nsu;
	}

	/** 
	 *
	 */
	public VendaBean(Integer id, PessoaBean pessoaCliente, CertificadoBean certificado, UnidadeBean unidade, Date data, String hora, BigDecimal valor, String canalTag, Integer ganhador, Integer gg, EtapaPremioBean etapaPremio, EtapaBean etapa, SituacaoBean situacao, String pinbankNsu, Integer nsu, String transacao, String pagamento, String autorizacao, String nsuGw, String nsuAdquirente, String mensagemGateway) {
		super();
		this.id = id;
		this.pessoaCliente = pessoaCliente;
		this.certificado = certificado;
		this.unidade = unidade;
		this.data = data;
		this.hora = hora;
		this.valor = valor;
		this.canalTag = canalTag;
		this.ganhador = ganhador;
		this.gg = gg;
		this.etapaPremio = etapaPremio;
		this.etapa = etapa;
		this.situacao = situacao;
		this.pinbankNsu = pinbankNsu;
		this.nsu = nsu;
		this.transacao = transacao;
		this.pagamento = pagamento;
		this.autorizacao = autorizacao;
		this.nsuGw = nsuGw;
		this.nsuAdquirente = nsuAdquirente;
		this.mensagemGateway = mensagemGateway;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPessoaCliente(PessoaBean pessoaCliente) {
		this.pessoaCliente = pessoaCliente;
	}

	public PessoaBean getPessoaCliente() {
		if (pessoaCliente == null) pessoaCliente = new PessoaBean();
		return pessoaCliente;
	}

	public void setCertificado(CertificadoBean certificado) {
		this.certificado = certificado;
	}

	public CertificadoBean getCertificado() {
		if (certificado == null) certificado = new CertificadoBean();
		return certificado;
	}

	public void setUnidade(UnidadeBean unidade) {
		this.unidade = unidade;
	}

	public UnidadeBean getUnidade() {
		if (unidade == null) unidade = new UnidadeBean();
		return unidade;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getData() {
		return data;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getHora() {
		return hora;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setCanalTag(String canalTag) {
		this.canalTag = canalTag;
	}

	public String getCanalTag() {
		return canalTag;
	}

	public void setGanhador(Integer ganhador) {
		this.ganhador = ganhador;
	}

	public Integer getGanhador() {
		return ganhador;
	}

	public void setGg(Integer gg) {
		this.gg = gg;
	}

	public Integer getGg() {
		return gg;
	}

	public void setEtapaPremio(EtapaPremioBean etapaPremio) {
		this.etapaPremio = etapaPremio;
	}

	public EtapaPremioBean getEtapaPremio() {
		if (etapaPremio == null) etapaPremio = new EtapaPremioBean();
		return etapaPremio;
	}

	public void setEtapa(EtapaBean etapa) {
		this.etapa = etapa;
	}

	public EtapaBean getEtapa() {
		if (etapa == null) etapa = new EtapaBean();
		return etapa;
	}

	public void setSituacao(SituacaoBean situacao) {
		this.situacao = situacao;
	}

	public SituacaoBean getSituacao() {
		if (situacao == null) situacao = new SituacaoBean();
		return situacao;
	}

	public void setPinbankNsu(String pinbankNsu) {
		this.pinbankNsu = pinbankNsu;
	}

	public String getPinbankNsu() {
		return pinbankNsu;
	}

	public void setNsu(Integer nsu) {
		this.nsu = nsu;
	}

	public Integer getNsu() {
		return nsu;
	}

	public void setTransacao(String transacao) {
		this.transacao = transacao;
	}

	public String getTransacao() {
		return transacao;
	}

	public void setPagamento(String pagamento) {
		this.pagamento = pagamento;
	}

	public String getPagamento() {
		return pagamento;
	}

	public void setAutorizacao(String autorizacao) {
		this.autorizacao = autorizacao;
	}

	public String getAutorizacao() {
		return autorizacao;
	}

	public void setNsuGw(String nsuGw) {
		this.nsuGw = nsuGw;
	}

	public String getNsuGw() {
		return nsuGw;
	}

	public void setNsuAdquirente(String nsuAdquirente) {
		this.nsuAdquirente = nsuAdquirente;
	}

	public String getNsuAdquirente() {
		return nsuAdquirente;
	}

	public void setMensagemGateway(String mensagemGateway) {
		this.mensagemGateway = mensagemGateway;
	}

	public String getMensagemGateway() {
		return mensagemGateway;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaBean other = (VendaBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
