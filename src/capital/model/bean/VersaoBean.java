package capital.model.bean;

import br.com.dotum.jedi.core.db.ColumnDB;
import br.com.dotum.jedi.core.db.FieldDB;
import br.com.dotum.jedi.core.db.TableDB;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.core.table.TableFieldEditType;

@TableDB(label="Versão", schema="sys", name="versao", acronym="ver", typePk="S", sequence="sver_id")
public class VersaoBean extends Bean {

	@ColumnDB(name="ver_id", pk=true, length=10, precision=0)
	@FieldDB(name="Id", label="Id", required=true, type= TableFieldEditType.INTEGERFIELD)
	private Integer id;

	@ColumnDB(name="ver_versaoapp", pk=false, length=20, precision=0)
	@FieldDB(name="VersaoApp", label="Versão App", required=false, type= TableFieldEditType.TEXTFIELD)
	private String versaoApp;

	/** 
	 * Construtor sem parametros
	 *
	 */
	public VersaoBean() {
		super();
	}

	/** 
	 * Construtor com os campos da PK
	 *
	 */
	public VersaoBean(Integer id) {
		super();
		this.id = id;
	}

	public VersaoBean(Integer id, String versaoApp) {
		this.id = id;
		this.versaoApp = versaoApp;

		}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setVersaoApp(String versaoApp) {
		this.versaoApp = versaoApp;
	}

	public String getVersaoApp() {
		return versaoApp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VersaoBean other = (VersaoBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
