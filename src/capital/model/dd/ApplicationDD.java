package capital.model.dd;

import br.com.dotum.jedi.core.db.ListDD;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class ApplicationDD extends ListDD {
	static Log LOG = LogFactory.getLog(ApplicationDD.class);
	
	public List<Class<?>> getFullList() throws Exception {
		List<Class<?>> classList = new ArrayList<Class<?>>();

		classList.add(UniDD.class);
		classList.add(SitDD.class);
		classList.add(TarDD.class);
		classList.add(PestDD.class);
		classList.add(PesetDD.class);
		classList.add(PesDD.class);
		classList.add(ArqtDD.class);
		classList.add(VerDD.class);
		classList.add(MsgDD.class);
		classList.add(EstDD.class);
		classList.add(CidDD.class);

		return classList;
	}
	
}	
