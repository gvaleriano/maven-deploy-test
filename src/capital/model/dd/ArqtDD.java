package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.ArquivoTipoBean;

public class ArqtDD extends AbstractDD {
	public ArqtDD() throws Exception {
		super(ArquivoTipoBean.class, true);
	}
	public static ArquivoTipoBean getById(Integer id) {
		return (ArquivoTipoBean) AbstractDD.getBy(ArquivoTipoBean.class, "id", id);
	}

	public static final Integer IMAGEM_CAPITAL_PREMIO_APP = 1;
	
	static {
		//                   ID,                        DESCRICAO      
		add( new ArquivoTipoBean(IMAGEM_CAPITAL_PREMIO_APP, "Imagem de premio", null));
	}
	
}
