package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.CidadeBean;

public class CidDD extends AbstractDD {
	
	public CidDD() throws Exception {
		super(CidadeBean.class, true);
	}
	
	static {
		//                       ID,                  ATIVO,        CADASTRO,        ESTID,                                       NOME,                                  CODIGOIBGE
		//add( new CidadeBean(5300108,             EstDD.getById(EstDD.DISTRITO_FEDERAL),       "Brasília",                            "5300108") );
	}
		

}
