package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.EstadoBean;

public class EstDD extends AbstractDD {

	public EstDD() throws Exception {
		super(EstadoBean.class, true);
	}
	public static EstadoBean getById(Integer id) {
		return (EstadoBean) AbstractDD.getBy(EstadoBean.class, "id", id);
	}
	
	public static Integer ACRE = 12;
	public static Integer ALAGOAS = 27;
	public static Integer AMAPA = 16;
	public static Integer AMAZONAS = 13;
	public static Integer BAHIA = 29;
	public static Integer CEARA = 23;
	public static Integer DISTRITO_FEDERAL= 53;
	public static Integer ESPIRITO_SANTO= 32;
	public static Integer GOIAS= 52;
	public static Integer MARANHAO = 21;
	public static Integer MATO_GROSSO = 51;
	public static Integer MATO_GROSSO_DO_SUL = 50;
	public static Integer MINAS_GERAIS = 31;
	public static Integer PARA = 15;
	public static Integer PARAIBA = 25;
	public static Integer PARANA = 41;
	public static Integer PERNAMBUCO = 26;
	public static Integer PIAUI = 22;
	public static Integer RIO_JANEIRO = 33;
	public static Integer RIO_GRANDE_DO_NORTE = 24;
	public static Integer RIO_GRANDE_DO_SUL = 43;
	public static Integer RONDONIA = 11;
	public static Integer RORAIMA = 14;
	public static Integer SANTA_CATARINA = 42;
	public static Integer SAO_PAULO = 35;
	public static Integer SERGIPE= 28;
	public static Integer TOCANTINS = 17;

	static {
		add( new EstadoBean(ACRE,        			"Acre",         			"AC",         "-04:00") );
		add( new EstadoBean(ALAGOAS,   	 		"Alagoas",      			"AL",         "-03:00") );
		add( new EstadoBean(AMAPA,       			"Amapá",        			"AP",         "-03:00") );
		add( new EstadoBean(AMAZONAS,    			"Amazonas",     			"AM",         "-04:00") );
		add( new EstadoBean(BAHIA,       			"Bahia",        			"BA",         "-03:00") );
		add( new EstadoBean(CEARA,       			"Ceará",        			"CE",         "-03:00") );
		add( new EstadoBean(DISTRITO_FEDERAL,      "Distrito Federal",         "DF",         "-03:00") );
		add( new EstadoBean(ESPIRITO_SANTO,       	"Espírito Santo",        	"ES",         "-03:00") );
		add( new EstadoBean(GOIAS,       			"Goiás",        			"GO",         "-03:00") );
		add( new EstadoBean(MARANHAO,       		"Maranhão",        			"MA",         "-03:00") );
		add( new EstadoBean(MATO_GROSSO,       	"Mato Grosso",        		"MT",         "-03:00") );
		add( new EstadoBean(MATO_GROSSO_DO_SUL,    "Mato Grosso do Sul",      	"MS",         "-03:00") );
		add( new EstadoBean(MINAS_GERAIS,       	"Minas Gerais",        		"MG",         "-03:00") );
		add( new EstadoBean(PARA,       			"Pará",        				"PA",         "-03:00") );
		add( new EstadoBean(PARANA,       			"Paraná",        			"PR",         "-03:00") );
		add( new EstadoBean(PARAIBA,     			"Paraíba",        			"PB",         "-03:00") );
		add( new EstadoBean(PERNAMBUCO,       		"Pernambuco",        		"PE",         "-03:00") );
		add( new EstadoBean(PIAUI,       			"Piauí",        			"PI",         "-03:00") );
		add( new EstadoBean(RIO_JANEIRO,       	"Rio de Janeiro",        	"RJ",         "-03:00") );
		add( new EstadoBean(RIO_GRANDE_DO_NORTE,   "Rio Grande do Norte",     	"RN",         "-03:00") );
		add( new EstadoBean(RIO_GRANDE_DO_SUL,     "Rio Grande do Sul",       	"RS",         "-03:00") );
		add( new EstadoBean(RONDONIA,       		"Rondônia",        			"RO",         "-04:00") );
		add( new EstadoBean(RORAIMA,       		"Roraima",        			"RR",         "-04:00") );
		add( new EstadoBean(SANTA_CATARINA,       	"Santa Catarina",        	"SC",         "-03:00") );
		add( new EstadoBean(SAO_PAULO,       		"São Paulo",        		"SP",         "-03:00") );
		add( new EstadoBean(SERGIPE,       		"Sergipe",        			"SE",         "-03:00") );
		add( new EstadoBean(TOCANTINS,       		"Tocantins",      			"TO",         "-03:00") );
	}
}	

