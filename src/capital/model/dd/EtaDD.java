package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.EtapaBean;

public class EtaDD extends AbstractDD {
	public EtaDD() throws Exception {
		super(EtapaBean.class, true);
	}
	public static EtapaBean getById(Integer id) {
		return (EtapaBean) AbstractDD.getBy(EtapaBean.class, "id", id);
	}

	public static final Integer SIMPLES = 1;
	public static final Integer DUPLO = 2;
	public static final Integer TRIPLO = 3;
	
	static {
		//                   ID,                        DESCRICAO      
	}
	
}
