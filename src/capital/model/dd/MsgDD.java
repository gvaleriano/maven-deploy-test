package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.ArquivoTipoBean;
import capital.model.bean.MessageUIBean;

public class MsgDD extends AbstractDD {
	public MsgDD() throws Exception {
		super(MessageUIBean.class, false);
	}
	public static ArquivoTipoBean getById(Integer id) {
		return (ArquivoTipoBean) AbstractDD.getBy(ArquivoTipoBean.class, "id", id);
	}

	public static final Integer IMAGEM_CAPITAL_PREMIO_APP = 1;

	static {
		//                       ID,                         CHAVE                                                           DESCRICAO      
		add( new MessageUIBean(-1, "msg.capitalservice.generatetokenonlogin.01"	                                             ,"Usuário ou senha inválidos"));
		add( new MessageUIBean(-2, "msg.025.ativarcartao.03"                                                                 ,"	Cartão pronto para compras."));
		add( new MessageUIBean(-3, "msg.025.ativarcartao.02"	                                                             ,"	Cartão não encontrado em sua carteira."));
		add( new MessageUIBean(-4, "msg.025.ativarcartao.01"	                                                             ,"	Digite o valor descontado em sua fatura do cartão."));
		add( new MessageUIBean(-5, "msg.capitalservice.dodataetapadata.01"	                                                 ,"	Não temos etapas ativa neste momento."));
		add( new MessageUIBean(-6, "msg.capitalservice.getwalletbytoken.01"	                                                 ,"	Não foi encontrado cartão com este token $P{0}"));
		add( new MessageUIBean(-7, "msg.015.comprarcertificado.01"	                                                         ,"	Número máximo de compras realizado."));
		add( new MessageUIBean(-8, "msg.015.ativarcatao.01"	                                                                 ,"	Digite o valor descontado em sua fatura do cartão."));
		add( new MessageUIBean(-9, "msg.015.ativarcatao.03"	                                                                 ,"	Compra realizada com sucesso!"));
		add( new MessageUIBean(-10, "msg.015.ativarcatao.02"	                                                             ,"	Cartão não encontrado em sua carteira."));
		add( new MessageUIBean(-11, "msg.015.payzen.06"	                                                                     ,"	Bandeira do cartão está vazio."));
		add( new MessageUIBean(-12, "msg.015.comprarcertificado.02"	                                                         ,"	Não foi encontrado cartão com o Id informado."));
		add( new MessageUIBean(-13, "msg.015.payzen.05"	                                                                     ,"	CVV está vazio"));
		add( new MessageUIBean(-14, "msg.015.payzen.04"	                                                                     ,"	Mês de validade está vazio."));
		add( new MessageUIBean(-15, "msg.015.payzen.03"	                                                                     ,"	Ano de validade está vazio"));
		add( new MessageUIBean(-16, "msg.015.payzen.02"	                                                                     ,"	Valor está vazio"));
		add( new MessageUIBean(-17, "msg.015.payzen.01"	                                                                     ,"	Número do cartão está vazio"));
		add( new MessageUIBean(-18, "msg.messagetest.main.01"	                                                             ,"	asd"));
		add( new MessageUIBean(-19, "msg.capitalservice.dodatacertificadolivrealeatorio.01"	                                 ,"	Nenhum certificado encontrado para esta etapa."));
		add( new MessageUIBean(-20, "msg.capitalservice.dodatasorteiodata.01"	                                             ,"	Não temos etapas ativa neste momento."));
		add( new MessageUIBean(-21, "msg.005.recuperarsenhasms.01"	                                                         ,"	$P{0} e o codigo do Capital de Premios. Nao o compartilhe."));
		add( new MessageUIBean(-22, "msg.005.recuperarsenhasms.02"	                                                         ,"	Mensagem de recuperação de senha foi enviado para $P{0}"));
		add( new MessageUIBean(-23, "msg.capitalservice.reservavenda.01"	                                                 ,"	Este certificado já foi comprado."));
		add( new MessageUIBean(-24, "msg.005.recuperarsenhasms.03"	                                                         ,"	Erro durante o envio do SMS de alteração de senha."));
		add( new MessageUIBean(-25, "msg.005.recuperarsenhasms.04"	                                                         ,"	Usuário não encontrado."));
		add( new MessageUIBean(-26, "msg.capitalservice.ceplimitecampra.01"	                                                 ,"	Não foi encontrado endereço, verifique seu cadastro"));
		add( new MessageUIBean(-27, "msg.capitalservice.ceplimitecampra.02"	                                                 ,"	Venda não permitida nessa região."));
		add( new MessageUIBean(-28, "msg.005.recuperarsenhacodigo.01"	                                                     ,"	Usuário não encontrado."));
		add( new MessageUIBean(-29, "msg.005.recuperarsenhacodigo.02"	                                                     ,"	Não e mais permitido a troca de senha com esse código"));
		add( new MessageUIBean(-30, "msg.005.recuperarsenhacodigo.03"	                                                     ,"	Senha alterada com sucesso."));
		add( new MessageUIBean(-31, "msg.005.recuperarsenhacodigo.04"	                                                     ,"	Código invalido."));
		add( new MessageUIBean(-32, "msg.005.cadastrooficial.04"	                                                         ,"	Endereço inválido."));
		add( new MessageUIBean(-33, "msg.005.cadastrooficial.02"	                                                         ,"	Já existe um cadastro com este EMAIL."));
		add( new MessageUIBean(-34, "msg.005.cadastrooficial.03"	                                                         ,"	Já existe um cadastro com este TELEFONE."));
		add( new MessageUIBean(-35, "msg.pinbankservice.formatresponse.01"	                                                 ,"	Ops! Algo deu errado na sua administradora de cartão. Por gentileza, dá uma ligadinha lá, veja o que aconteceu e tente novamente!"));
		add( new MessageUIBean(-36, "msg.005.recuperarsenhacodigo.05"	                                                     ,"	Código invalido."));
		add( new MessageUIBean(-38, "msg.005.buscacpf.03"	                                                                 ,"	CPF: $P{0} não encontrado"));
		add( new MessageUIBean(-39, "msg.005.cadastrooficial.01"	                                                         ,"	Já existe um cadastro com este CPF."));
		add( new MessageUIBean(-40, "msg.pinbankservice.inclusaocartao.01"	                                                 ,"	Ops! Algo deu errado na sua administradora de cartão. Por gentileza, dá uma ligadinha lá, veja o que aconteceu e tente novamente!"));
		add( new MessageUIBean(-41, "msg.005.buscacpf.02"	                                                                 ,"	Oie! Encontramos um cadastro com esse CPF, por gentileza, tente recuperar sua senha para obter acesso ao aplicativo."));
		add( new MessageUIBean(-42, "msg.005.buscacpf.01"	                                                                 ,"	$P{0} é um CPF invalido."));
		add( new MessageUIBean(-43, "msg.020.etapaativa.01"	                                                                 ,"	etapa sem data de validade para compra"));
		add( new MessageUIBean(-44, "msg.020.etapaativa.02"	                                                                 ,"	Não e mais permitido a compra de certificados para esta etapa"));
		add( new MessageUIBean(-45, "msg.025.syncpinbank.01"	                                                             ,"	cartões sincronizados com sucesso."));
		add( new MessageUIBean(-46, "msg.capitalservice.getpessoabytoken.01"	                                             ,"	Usuario não encontrado"));
		add( new MessageUIBean(-47, "msg.capitalservice.responseversaoapp.01"	                                             ,"	Oie! Preparamos uma nova versão pra você. Por favor, feche o aplicativo e atualize na Play Store. Ahhhh depois que atualizar, te desejamos uma boa sorte!"));
		add( new MessageUIBean(-48, "msg.020.etapaoficial.01"	                                                             ,"	Não foi possivel recuperar o IMEI do usuario"));

	}
}


