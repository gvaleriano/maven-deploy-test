package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.PessoaBean;

public class PesDD extends AbstractDD {
	
	public PesDD() throws Exception {
		super(PessoaBean.class, true);  
	}
	public static PessoaBean getById(Integer id) {
		return (PessoaBean) AbstractDD.getBy(PessoaBean.class, "id", id);
	}

	public static final Integer ADMIN = -1;
	
	static {
		//add( new PessoaBean(ADMIN,	UniDD.getById(UniDD.PRINCIPAL),         null,         null,           null,          "Principal"));
	}
	
}