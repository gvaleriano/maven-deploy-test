package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.PessoaEnderecoTipoBean;

public class PesetDD extends AbstractDD {
	public PesetDD() throws Exception {
		super(PessoaEnderecoTipoBean.class, true);
	}
	public static PessoaEnderecoTipoBean getById(Integer id) {
		return (PessoaEnderecoTipoBean) AbstractDD.getBy(PessoaEnderecoTipoBean.class, "id", id);
	}

	public static final Integer RESIDENCIAL = -1;
	public static final Integer COMERCIAL = -2;
	public static final Integer INSTALACAO = -3;
	
	static {
		//                   ID,                        DESCRICAO      
		add( new PessoaEnderecoTipoBean(RESIDENCIAL, "Residencial"));
		add( new PessoaEnderecoTipoBean(COMERCIAL, "Comercial"));
		add( new PessoaEnderecoTipoBean(INSTALACAO, "Instalação"));
	}
	
}
