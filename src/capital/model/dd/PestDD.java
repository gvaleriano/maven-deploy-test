package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.PessoaTipoBean;

public class PestDD extends AbstractDD {
	public PestDD() throws Exception {
		super(PessoaTipoBean.class, true);
	}
	public static PessoaTipoBean getById(Integer id) {
		return (PessoaTipoBean) AbstractDD.getBy(PessoaTipoBean.class, "id", id);
	}
	
	public static final Integer DEVELOPER = -1;
	public static final Integer SUPORTE = -2;
	public static final Integer ADMIN = -3;

	public static final Integer DISTRIBUIDOR = 1;
	public static final Integer VENDEDOR = 2;
	public static final Integer MOBILE  = 3;
	
	static {
		add( new PessoaTipoBean(DEVELOPER, "Developer"));
		add( new PessoaTipoBean(SUPORTE, "Suporte"));
		add( new PessoaTipoBean(ADMIN, "Admin"));
		add( new PessoaTipoBean(DISTRIBUIDOR, "Distribuidor"));
		add( new PessoaTipoBean(VENDEDOR, "Vendedor"));
		add( new PessoaTipoBean(MOBILE, "Mobile"));
	}
	
}