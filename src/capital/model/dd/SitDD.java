package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.SituacaoBean;

public class SitDD extends AbstractDD {
	public SitDD() throws Exception {
		super(SituacaoBean.class, true);
	}
	public static SituacaoBean getById(Integer id) {
		return (SituacaoBean) AbstractDD.getBy(SituacaoBean.class, "id", id);
	}

	public static final Integer ATIVO = 1;
	public static final Integer INATIVO = 2;
	public static final Integer CANCELADO = -1;
	public static final Integer EXCLUIDO = -2;
	public static final Integer PENDENTE = -3;
	public static final Integer CONCLUIDO = -4;
	public static final Integer ENVIADO = -5;
	public static final Integer VISUALIZADO = -6;
	public static final Integer REJEITADO = -7;
	public static final Integer ANDAMENTO = -8;
	
	static {
		//                   ID,                        DESCRICAO      
		add( new SituacaoBean(CANCELADO,				"Cancelado"));
		add( new SituacaoBean(EXCLUIDO,					"Excluido"));
		add( new SituacaoBean(PENDENTE,					"Pendente"));
		add( new SituacaoBean(CONCLUIDO,				"Concluido"));
		add( new SituacaoBean(ENVIADO,				    "Enviado"));
		add( new SituacaoBean(VISUALIZADO,				"Visualizado"));
		add( new SituacaoBean(REJEITADO,				"Rejeitado"));
		add( new SituacaoBean(ANDAMENTO,				"Andamento"));
	}
	
}
