package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import br.com.dotum.jedi.core.db.bean.TarefaBean;

public class TarDD extends AbstractDD {
	public TarDD() throws Exception {
		super(TarefaBean.class, true);
	}
	public static TarefaBean getById(Integer id) {
		return (TarefaBean) AbstractDD.getBy(TarefaBean.class, "id", id);
	}

	public static final Integer DEV = 1;
	public static final Integer ADMIN = 2;
	public static final Integer USER = 3;
	
	
	public static final String DEV_CODE = "FWD"; // apenas no momento de desenvolvimento
	public static final String ADMIN_CODE = "FWA"; // apenas administrador
	public static final String USER_CODE = "FWU"; // todos os usuarios terão acesso 

	
	
	static {
		//                   ID,                    DESCRICAO           CODIGO
		add( new TarefaBean(DEV,					"Dev", 				DEV_CODE));
		add( new TarefaBean(ADMIN,					"Admin", 			ADMIN_CODE));
		add( new TarefaBean(USER,					"User", 			USER_CODE));
	}
	
}
