package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.UnidadeBean;

public class UniDD extends AbstractDD {
	public UniDD() throws Exception {
		super(UnidadeBean.class, true);
	}
	public static UnidadeBean getById(Integer id) {
		return (UnidadeBean) AbstractDD.getBy(UnidadeBean.class, "id", id);
	}

	public static final Integer PRINCIPAL = 1;
	
	static {
		//                   ID,                        DESCRICAO      
		add( new UnidadeBean(PRINCIPAL,	"Capital de Prêmios"));
	}
	
}
