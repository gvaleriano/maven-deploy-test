package capital.model.dd;

import br.com.dotum.jedi.core.db.AbstractDD;
import capital.model.bean.VersaoBean;

public class VerDD extends AbstractDD {
	public VerDD() throws Exception {
		super(VersaoBean.class, true);
	}
	public static VersaoBean getById(Integer id) {
		return (VersaoBean) AbstractDD.getBy(VersaoBean.class, "id", id);
	}

	public static final Integer PRINCIPAL = 1;
	
	static {
		//                   ID,                        DESCRICAO      
		add( new VersaoBean(PRINCIPAL,	"1.0"));
	}
	
}
