package capital.service;

import br.com.dotum.jedi.core.security.UserContext;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.protocol.EmailContentTypeEnum;
import br.com.dotum.jedi.protocol.EmailSecurityType;
import br.com.dotum.jedi.protocol.EmailSmtpProtocol;
import br.com.dotum.jedi.util.PropertiesUtils;

import javax.servlet.http.HttpServletRequest;

public class EmailService {

	public static void enviarEmail(String[] to, String subject, String message) throws Exception {
		enviarEmail(to, subject, message, null, null );
	}

	public static void enviarEmail(String to, String subject, String message) throws Exception {
		enviarEmail(new String[] {to}, subject, message, null, null );
	}
	
	public static void enviarEmail(String to, String subject, String message, FileMemory attach) throws Exception {
		enviarEmail(new String[] {to}, subject, message, new FileMemory[] {attach}, null );
	}

	public static void enviarEmail(String[] recipients, String subject, String message, FileMemory[] attachs, String replyTo) throws Exception {

		String host = PropertiesUtils.get("email.host", false);
		String port_str = PropertiesUtils.get("email.port", false);
		Integer port = Integer.parseInt(port_str);
		String username = PropertiesUtils.get("email.username", false);
		String password = PropertiesUtils.get("email.password", false);

		EmailSmtpProtocol protocol = new EmailSmtpProtocol(host, port, username, password, EmailSecurityType.SSL);
		for (String to : recipients) {
			protocol.addTo(to);
		}
		if (attachs != null) {
			for (FileMemory file : attachs) {
				protocol.addAttach(file);
			}
		}
		protocol.setType(EmailContentTypeEnum.HTML);
		protocol.setFrom(replyTo);
		protocol.sendMessage(subject, message);
	}
	
	public static void agendarEnvioEmail() {
		
	}
	
//	public static String getURL(HttpServletRequest req) {
//
//	    String scheme = req.getScheme();             // http
//	    String serverName = req.getServerName();     // hostname.com
//	    int serverPort = req.getServerPort();        // 80
//	    String contextPath = req.getContextPath();   // /mywebapp
//	    String servletPath = req.getServletPath();   // /servlet/MyServlet
//	    String pathInfo = req.getPathInfo();         // /a/b;c=123
//	    String queryString = req.getQueryString();          // d=789
//
//	    // Reconstruct original requesting URL
//	    StringBuilder url = new StringBuilder();
//	    url.append(scheme).append("://").append(serverName);
//
//	    if (serverPort != 80 && serverPort != 443) {
//	        url.append(":").append(serverPort);
//	    }
//
//	    url.append(contextPath).append(servletPath);
//
//	    if (pathInfo != null) {
//	        url.append(pathInfo);
//	    }
//	    if (queryString != null) {
//	        url.append("?").append(queryString);
//	    }
//	    return url.toString();
//	}

}