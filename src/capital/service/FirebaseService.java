package capital.service;

import br.com.dotum.jedi.core.exceptions.DeveloperException;
import br.com.dotum.jedi.core.exceptions.WarningException;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.util.PropertiesUtils;
import br.com.dotum.jedi.util.StringUtils;
import capital.api.APIException;
import capital.api.PessoaAction;
import com.payu.sdk.PayU;
import com.payu.sdk.model.Language;
import com.payu.sdk.utils.LoggerUtil;
import net.thegreshams.firebase4j.error.FirebaseException;
import net.thegreshams.firebase4j.error.JacksonUtilityException;
import net.thegreshams.firebase4j.model.FirebaseResponse;
import net.thegreshams.firebase4j.service.Firebase;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.logging.Level;

public class FirebaseService {

    private static Log LOG = LogFactory.getLog(FirebaseService.class);
    private static HashMap<String, Object> config = new HashMap<String, Object>();

    public static void clean() {
        config = new HashMap<String, Object>();
    }

    public static void load() throws APIException {
        LOG.debug("Carregando configurações do Firebase...");
        config.put("gateway_pinbank_priority", (String)getProperty("config/gateway/pinbank", "priority"));
        config.put("gateway_payulatam_priority", (String)getProperty("config/gateway/payulatam", "priority"));
        config.put("gateway_braspag_priority", (String)getProperty("config/gateway/braspag", "priority"));
        LOG.debug("Carregando configurações do Firebase (done)");
    }

    public static Object getProperty(String param) throws APIException {
        if (config.containsKey(param) == false) load();
        load();
        return config.get(param);
    }

    private static Object getProperty(String path, String property) throws APIException  {
        try {

            String fb_url = PropertiesUtils.getString("firebase.database.realtime.url");
            if (StringUtils.hasValue(fb_url) == false)
                throw new DeveloperException("Falta definir o parâmetro 'firebase.database.realtime.url' no arquivo config.properties ");

            String token_auth = PropertiesUtils.getString("firebase.database.realtime.token");
            if (StringUtils.hasValue(token_auth) == false)
                throw new DeveloperException("Falta definir o parâmetro 'firebase.database.realtime.token' no arquivo config.properties ");

            StringBuilder url = new StringBuilder();
            url.append(fb_url);
            url.append("/");
            url.append(path);

            Firebase firebase = new Firebase(url.toString(), token_auth);
            FirebaseResponse response = firebase.get();
            LOG.debug("response firebase => "+response);
            return response.getBody().get(property);
        } catch (FirebaseException | UnsupportedEncodingException fbex) {
            throw new APIException("firebase_exception", fbex.getMessage());
        } catch (Exception e) {
            throw new APIException("firebase_exception", e.getMessage());
        }
    }

    public static void main(String[] args) throws APIException {
        Object limite_por_cliente = FirebaseService.getProperty("config/geral", "limite_por_cliente");
        ArrayList cep_liberado = (ArrayList)FirebaseService.getProperty("config/geral", "cep_liberado");
        LOG.debug( "limite_por_cliente => " + limite_por_cliente.toString() );
        LOG.debug( "cep_liberado =>"+cep_liberado.toString() );
        JSONArray listJson = new JSONArray(cep_liberado);
        List<Bean> list = new ArrayList<Bean>();
        for (Object json : listJson) {
            Bean b = new Bean();
            try {
                b.toObject(json.toString());
            } catch (Exception e) {
                LOG.error( e.getMessage() );
            }
            list.add(b);
        }
        for (Bean b: list) {
            String cep_i = b.get("cep_final");
            String cep_f = b.get("cep_inicial");
            LOG.debug("Faixa de cep => ["+cep_i+" até "+cep_f+"]");
        }
    }
}
