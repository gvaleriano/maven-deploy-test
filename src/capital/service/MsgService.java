package capital.service;

import br.com.dotum.core.constants.FWConstants;
import br.com.dotum.jedi.core.table.Bean;
import br.com.dotum.jedi.file.layout.FileMemory;
import br.com.dotum.jedi.log.Log;
import br.com.dotum.jedi.log.LogFactory;

import java.io.IOException;

public class MsgService {
	private static Log LOG = LogFactory.getLog(MsgService.class);
	private static final String FILE_NAME ="messageUI.json";
	private static String FILE_PATH = "D:/java/projetos/capital/webcapital/src/main/java/";
//	private static String FILE_PATH = "/usr/apache-tomcat-9.0.12/webapps/webcapital/WEB-INF/classes/";
	private static Bean json;
	private static FileMemory fm;
	
	public static Bean iniciaBean(String serverPath) throws Exception {
		FILE_PATH = serverPath + FWConstants.TOMCAT_PATH_CLASSES;
		try {
			fm = new FileMemory( FILE_PATH + FILE_NAME);
			fm.setCharSet("UTF-8");
		} catch (Exception e) {
			fm = new FileMemory("{}", FILE_NAME);
			fm.setPath(FILE_PATH);
			fm.setExtension("json");
			fm.setCharSet("UTF-8");
			fm.createFile();
		}
		json = new Bean();
		json.toObject(fm.getText());
		return json;
	}
	public static FileMemory getFileMemory() {
		return fm;
	}
	public static Bean getBean(String serverPath) throws Exception {
		if(json == null) iniciaBean(serverPath);
		return json;
	}
	public static void save(Bean jsonFile) throws IOException, Exception {
		FileMemory fm = new FileMemory(jsonFile.toJson2(), FILE_NAME);
//		if (fm.exists() == false) fm.createFile();
		fm.setPath(FILE_PATH);
		fm.createFile();
	}

	public static String get(String serverPath, String propertie) throws Exception {
		if (json == null) iniciaBean(serverPath);
		String msg = null;
			msg = json.get(propertie);
			if(msg == null) {
				msg = "Menssagem não personalizada para " + propertie;
				json.set(propertie, msg);
			}
		return msg;
	}
	public static String get(String serverPath, String propertie, String ...params) throws Exception {
		String msg = get(serverPath, propertie);
		for (int i = 0; i < params.length; i++) {
			msg = msg.replaceAll("\\$P\\{" + i + "}", params[i]);
		} 
		return  msg;
	}
}
